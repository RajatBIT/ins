﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
   public class BLCorporateInsuredRegistration
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public BLCorporateInsuredRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
            _sf = new SqlFunctions();
        }

        static BLCorporateInsuredRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public DataTable BindCorporateInsuredRegistration(string PkCInsdId, string name, string RegOffice, string Email)
        {
            DataTable dtReult = null;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PKCInsdId", PkCInsdId);
                dbManager.AddParameters("@Name", name);
                dbManager.AddParameters("@RegOffice", RegOffice);
                dbManager.AddParameters("@Email", Email);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewCorporateInsured, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public String AddCorporateInsuredRegistration(CSCorporateInsuredRegistration objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_InsdID", objProperty.PkCInsdId);
                dbManager.AddParameters("@Fk_State", objProperty.FKState);
                dbManager.AddParameters("@Fk_District", objProperty.FkDistricts);
                dbManager.AddParameters("@FK_InsrID ", objProperty.FkInsrID);
                dbManager.AddParameters("@Name", objProperty.Name);
                dbManager.AddParameters("@RegOffice", objProperty.RegOffice);
                dbManager.AddParameters("@UserName", objProperty.UserName);
                dbManager.AddParameters("@Email", objProperty.Email);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@AltMobile", objProperty.AltMobile);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@Pincode", objProperty.Pincode);
                dbManager.AddParameters("@Experience", objProperty.Experience);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                dbManager.AddParameters("@R_ID", objProperty.R_ID);
                dbManager.AddParameters("@CinNo", objProperty.CinNo);
                dbManager.AddParameters("@Password", objProperty.Password);
                dbManager.AddParameters("@ConfirmPassword", objProperty.ConfirmPassword);

                result = dbManager.ExecuteNonQuery(DBSP.uspCorporateInsuredRegistration, "@Flag", CommandType.StoredProcedure);
                if (result.Contains("Y"))
                {
                    result = "Your request for Insured Subscription has been successfully received. We will contact you soon.";
                }
                else if (result.Contains("U"))
                {
                    result = "User name Already Exists.";
                }
                else //if (result.Contains("E"))
                {
                    result = "Email Already Exists.";
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static String ApproveOrDiscardCorporateInsured(string ApproveStatus, string FK_UserID, string PK_CInsdID)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_UserID", FK_UserID);
                dbManager.AddParameters("@PK_CInsdID", PK_CInsdID);
                dbManager.AddParameters("@ApproveStatus", ApproveStatus);
                result = dbManager.ExecuteNonQuery(DBSP.uspApproveOrDiscardCInsured, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public DataTable CorporateInsuredRegisterValidation(CSCorporateInsuredRegistration c)
        {

            _param = new SqlParameter[] { new SqlParameter("@Email", c.Email), new SqlParameter("@Mobile", c.Mobile), new SqlParameter("@UserName", c.UserName) };
            return new SqlFunctions().returnDTWithProc_executeReader("uspCorporaterInsuredRegisterValidation", _param);

        }

        /////
    }
}
