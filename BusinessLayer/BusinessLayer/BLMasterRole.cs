﻿using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    static class BLMasterRole
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        static BLMasterRole() 
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public static Dictionary<int, string> BindMasterRole() 
        {
            Dictionary<int, string> _dictionary = new Dictionary<int, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT * FROM tblMasterRole");
                while (datareader.Read())
                {
                    _dictionary.Add(Convert.ToInt32(datareader["PKRoleID"]), datareader["RoleName"].ToString());
                }
            }
            catch (Exception ex)
            {
                _dictionary = null;
                throw ex;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        public static Boolean BindMasterRole(string controllers, string actions,string role)
        {
            bool result = false;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Controllers", controllers);
                dbManager.AddParameters("@Actions",actions);
                dbManager.AddParameters("@Role", role);
                IDataReader reader = dbManager.ExecuteReader(DBSP.uspCheckRole, CommandType.StoredProcedure);
                if (reader.Read())
                    result = true;
            }
            catch (Exception ex)
            {
                result = false;
                throw ex;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }
        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=*/
    }
}
