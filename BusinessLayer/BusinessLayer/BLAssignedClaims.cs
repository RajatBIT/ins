﻿using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BLAssignedClaims
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public BLAssignedClaims()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        static BLAssignedClaims()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public static DataTable BindTaskAssign(string mode, string FkSurId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Mode", mode);
                dbManager.AddParameters("@FK_SurID", FkSurId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspAssignedTask, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public string TaskAssign(CSAssignedClaims objProperty)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@XML", objProperty.Xml);
                dbManager.AddParameters("@Mode", objProperty.Mode);
                dbManager.AddParameters("@FK_SurID", objProperty.SurId);
                result = dbManager.ExecuteNonQuery(DBSP.uspAssignedTask, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable BindTraineeAssignedTask(string FkTurId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Mode", "MyAssignedTask");
                dbManager.AddParameters("@FK_TSURID", FkTurId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspAssignedTask, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }
    }
}