﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;
using System.Net;
using System.ComponentModel;
using System.Data;

namespace BusinessLayer
{
    public class BusinessHelper
    {
        public static string CreateAutoPassword(string value)
        {
            string password = "";
            char[] ch = value.ToLower().ToCharArray();
            foreach (char c in ch)
            {
                switch (c)
                {
                    case 'a':
                        password += "!";
                        break;
                    case 'b':
                        password += "w";
                        break;
                    case 'c':
                        password += "q";
                        break;
                    case 'd':
                        password += "j";
                        break;
                    case 'e':
                        password += "$";
                        break;
                    case 'f':
                        password += "b";
                        break;
                    case 'g':
                        password += "u";
                        break;
                    case 'h':
                        password += "a";
                        break;
                    case 'i':
                        password += "@";
                        break;
                    case 'j':
                        password += "d";
                        break;
                    case 'k':
                        password += "g";
                        break;
                    case 'l':
                        password += "o";
                        break;
                    case 'm':
                        password += "n";
                        break;
                    case 'n':
                        password += "m";
                        break;
                    case 'o':
                        password += "^";
                        break;
                    case 'p':
                        password += "r";
                        break;
                    case 'q':
                        password += "x";
                        break;
                    case 'r':
                        password += "f";
                        break;
                    case 's':
                        password += "y";
                        break;
                    case 't':
                        password += "p";
                        break;
                    case 'u':
                        password += "%";
                        break;
                    case 'v':
                        password += "z";
                        break;
                    case 'w':
                        password += "6";
                        break;
                    case 'x':
                        password += "9";
                        break;
                    case 'y':
                        password += "7";
                        break;
                    default:
                        password += "v";
                        break;
                }
            }

            if (password.Length >= 7)
                return password.Substring(0, 6);
            else
                return password;
        }

        public static string     GetIpAdress()
        {
            IPHostEntry host;
            string localIP = "?";
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily.ToString() == "InterNetwork")
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        public static DataTable ConvertToDataTable<T>(IList<T> data)
        {
            PropertyDescriptorCollection properties =
               TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

    }
}