﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DataAccessLayer;
using BusinessLayer;
using System.Data;
using BusinessLayer.Models;
using System.Data.SqlClient;
using BusinessLayer.DAL;
//using InsuranceSurvey.Models;
//using InsuranceSurvey;

namespace InsuranceSurvey.BusinessLayer
{
    public class BLSurveyorsRegistration
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion
        SqlParameter[] _param;
        SqlFunctions _sf;
        DataTable _dt;
        public BLSurveyorsRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
            _sf = new SqlFunctions();
        }

        static BLSurveyorsRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }


        public static String CheckUserExistance(string UserType, string UserName)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@UserType", UserType);
                dbManager.AddParameters("@UserName", UserName);
                result = dbManager.ExecuteScalar(DBSP.uspCheckUserExistance, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static String CheckSLANO(string SlaNo)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@SLANO", SlaNo);
                result = dbManager.ExecuteScalar(DBSP.uspCheckSLANO, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static String CheckEmailExistance(string UserType, string EmailId)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@UserType", UserType);
                dbManager.AddParameters("@Email", EmailId);
                result = dbManager.ExecuteScalar(DBSP.uspCheckEmailExistance, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static String CheckIniRefExistance(string IniRef)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@IniRef", IniRef);
                result = dbManager.ExecuteScalar(DBSP.uspCheckIniRefExistance, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String AddSurveyors(CSSurveyorsRegistration objProperty)
        {
            string result = "";
            string SurveyorDepartments = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Pk_SurId", objProperty.PkSurId);
                dbManager.AddParameters("@FirstName", objProperty.FirstName);
                dbManager.AddParameters("@LastName", objProperty.LastName);
                dbManager.AddParameters("@UserName", objProperty.UserName);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@AltMobile", objProperty.AltMobile);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@Pincode", objProperty.Pincode);
                dbManager.AddParameters("@PanNo", objProperty.PanNo);
                dbManager.AddParameters("@StaxNo", objProperty.StaxNo);
                dbManager.AddParameters("@SLANo", objProperty.SlanNo);
                dbManager.AddParameters("@ValidUpto", objProperty.ValidUpto);
                dbManager.AddParameters("@Email", objProperty.Email);
                dbManager.AddParameters("@AltEmail", objProperty.AltEmail);
                dbManager.AddParameters("@IslaNO", objProperty.IslaNo);
                dbManager.AddParameters("@IniRef", objProperty.IniRef);
                dbManager.AddParameters("@AnyOther", objProperty.AnyOther);
                dbManager.AddParameters("@Experience", objProperty.Experience);
                dbManager.AddParameters("@Fk_Membership", objProperty.FkMemberShip);
                dbManager.AddParameters("@Fk_State", objProperty.FkState);
                dbManager.AddParameters("@Fk_District", objProperty.FKDistricts);
                dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                dbManager.AddParameters("@R_ID", objProperty.R_ID);
                dbManager.AddParameters("@Password", objProperty.Password);
                dbManager.AddParameters("@ConfirmPassword", objProperty.ConfirmPassword);

                //     objProperty.IPAddress);

                //  SurveyorDepartments = "INSERT INTo tblSurveyorDepartment(Fk_DeptId,Fk_SurId) SELECT DeptId,# FROM tblMasterDepartments WHERE DeptName IN(" + SurveyorDepartments.TrimEnd(',') + ")";
                //  dbManager.AddParameters("@SurveyorDepartments", SurveyorDepartments);
                result = dbManager.ExecuteNonQuery(DBSP.uspSurveyorsRegistration, "@Flag", System.Data.CommandType.StoredProcedure);
                if (result.Contains("Y"))
                {
                    string pkId = result.Split('_')[1];
                    SurveyorDepartments = "INSERT INTO tblSurveyorDepartment (Fk_DeptId,Fk_SurId) VALUES";
                    foreach (string _dept in objProperty.ChDept)
                    {
                        SurveyorDepartments += "('" + _dept + "','" + pkId.ToString() + "'),";// "'" + _dept + "',";
                    }
                    //dbManager.ExecuteNonQuery(SurveyorDepartments.TrimEnd(','));

                    int status = new SqlFunctions().executeNonQueryWithQuery(SurveyorDepartments.TrimEnd(','));
                    //dbManager.ExecuteNonQuery(SurveyorDepartments);
                    result = "Your request for registering as a Surveyor received successfully. We will contact you shortly, Thank you.";
                }

            }
            catch (Exception ex)
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String AddSurveyorDocs(CSSurveyorsDocument objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 1);
                dbManager.AddParameters("@FkSurId", objProperty.FkSurId);
                dbManager.AddParameters("@PassportPhoto", objProperty.PassportPhotoPath);
                dbManager.AddParameters("@DigitalSign", objProperty.DigitalSignPath);
                dbManager.AddParameters("@StampSign", objProperty.StampSignPath);
                dbManager.AddParameters("@SenderSign", objProperty.SenderSign);
                dbManager.AddParameters("@LetterHead", objProperty.LetterHeadPath);
                result = dbManager.ExecuteNonQuery(DBSP.uspSurveyorDocs, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public DataTable BindSurveyorsDetails(int? FkUserID, string Status, int? FkSurId)
        {
            DataTable dtReult = null;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_UserID", FkUserID);
                dbManager.AddParameters("@Status", Status);
                dbManager.AddParameters("@PK_SURID", FkSurId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewPendingSurveyors, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static DataTable BindApproveSurveyorsDetails(string emailId)
        {
            DataTable dtReult = null;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@EmaiId", emailId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspApprovSurveyor, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static DataTable BindSurveyorsDocsDetails(int? FkSurId)
        {
            DataTable dtReult = null;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FkSurId", FkSurId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetSurveyorDocsDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }


        public static String ApproveOrDiscardSurveyor(string ApproveStatus, string FK_UserID, string FK_SURID)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_UserID", FK_UserID);
                dbManager.AddParameters("@FK_SURID", FK_SURID);
                dbManager.AddParameters("@ApproveStatus", ApproveStatus);
                result = dbManager.ExecuteNonQuery(DBSP.uspApproveOrDiscardSurveyor, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        //public static DataTable SurveyorLogin(string CallType, string LoginID, string UserPassword)
        //{
        //    DataTable dtResult = new DataTable();
        //    try
        //    {
        //        dbManager.ConnectionOpen();
        //        dbManager.AddParameters("@CallType", CallType);
        //        dbManager.AddParameters("@LoginID", LoginID);
        //        dbManager.AddParameters("@UserPassword", UserPassword);
        //        dtResult = dbManager.ExecuteDataTable(DBSP.uspLogin, CommandType.StoredProcedure);
        //    }
        //    catch
        //    {
        //        dtResult = null;
        //    }
        //    finally
        //    {
        //        dbManager.ConnectionClose();
        //    }
        //    return dtResult;
        //}

        public static DataTable SurveyorLogin(string UserName, string password)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@UserName", UserName);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspSurveyorLogin, CommandType.StoredProcedure);

                if (dtResult.Rows.Count > 0)
                {
                    if (Convert.ToString(dtResult.Rows[0]["Flag"]).ToLower() == "Y".ToLower())
                    {
                        if (Convert.ToString(dtResult.Rows[0]["Password"]) != password)
                        {
                            foreach (System.Data.DataColumn col in dtResult.Columns)
                                col.ReadOnly = false;

                            dtResult.Rows[0]["Flag"] = "N";
                            dtResult.Rows[0]["Password"] = "wwq90#%&(*&FGSDFSAT&T)%#@FSDSGFGHW213";
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable GetRegisteredInsuredDetails(string EmailId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Email", EmailId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetRegisteredInsuredDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        #region check surveyor validation
        public DataTable SurveyorRegisterValidation(CSSurveyorsRegistration s)
        {

            _param = new SqlParameter[] {new SqlParameter("@PK_SURID",s.PkSurId), new SqlParameter("@Email", s.Email), new SqlParameter("@Mobile", s.Mobile), new SqlParameter("@UserName", s.UserName), new SqlParameter("@PanNo", s.PanNo), new SqlParameter("@StaxNo", s.StaxNo), new SqlParameter("@SLANo", s.SlanNo), new SqlParameter("@ISLANO", s.IslaNo), new SqlParameter("@IniRef", s.IniRef) };
            return new SqlFunctions().returnDTWithProc_executeReader("uspSurveyorRegisterValidation", _param);

        }

        public int saveSurveyorsRegistrationNew(CSSurveyorsRegistration s)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_SURID",s.PkSurId),
                new SqlParameter("@Fk_Membership", s.FkMemberShip), new SqlParameter("@FK_State", s.FkState), new SqlParameter("@FK_Districts", s.FKDistricts), new SqlParameter("@FirstName", s.FirstName), new SqlParameter("@LastName", s.LastName), new SqlParameter("@UserName", s.UserName), new SqlParameter("@Mobile", s.Mobile), new SqlParameter("@AltMobile", s.AltMobile), new SqlParameter("@Address", s.Address), new SqlParameter("@Country", s.Country), new SqlParameter("@Pincode", s.Pincode), new SqlParameter("@PANNO", s.PanNo), new SqlParameter("@StaxNo", s.StaxNo), new SqlParameter("@SLANo", s.SlanNo), new SqlParameter("@ValidUpto", s.ValidUpto), new SqlParameter("@Email", s.Email), new SqlParameter("@AltEmail", s.AltEmail), new SqlParameter("@ISLANO", s.IslaNo), new SqlParameter("@IniRef", s.IniRef), new SqlParameter("@AnyOther", s.AnyOther), new SqlParameter("@Experience", s.Experience), new SqlParameter("@IPAddress", s.IPAddress), new SqlParameter("@R_ID", s.R_ID), new SqlParameter("@Password", s.Password), new SqlParameter("@ConfirmPassword", s.ConfirmPassword) };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspSurveyorRegistrationSaveNew", _param));
        }
        public CSSurveyorsRegistration getSurveyorRegistration_byPK_SURID(int pk_SURID)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_SURID",pk_SURID) };
            _dt = _sf.returnDTWithProc_executeReader("uspSurveyorRegistration_get_byPK_SURID", _param);
            CSSurveyorsRegistration s = new CSSurveyorsRegistration();
            if(_dt.Rows.Count>0)
            {
                s.PkSurId = Convert.ToInt32(_dt.Rows[0]["PK_SURID"]);
                s.FkMemberShip = Convert.ToString(_dt.Rows[0]["Fk_Membership"]);
                s.FkState = Convert.ToInt32(_dt.Rows[0]["FK_State"]);
                s.FKDistricts = Convert.ToInt32(_dt.Rows[0]["FK_Districts"]);
                s.FirstName = Convert.ToString(_dt.Rows[0]["FirstName"]);
                s.LastName = Convert.ToString(_dt.Rows[0]["LastName"]);
                s.UserName = Convert.ToString(_dt.Rows[0]["UserName"]);
                s.Mobile = Convert.ToString(_dt.Rows[0]["Mobile"]);
                s.AltMobile = Convert.ToString(_dt.Rows[0]["AltMobile"]);
                s.Address = Convert.ToString(_dt.Rows[0]["Address"]);
                s.Country = Convert.ToString(_dt.Rows[0]["Country"]);
                s.Pincode = Convert.ToString(_dt.Rows[0]["Pincode"]);
                s.PanNo = Convert.ToString(_dt.Rows[0]["PANNO"]);
                s.StaxNo = Convert.ToString(_dt.Rows[0]["StaxNo"]);
                s.SlanNo = Convert.ToString(_dt.Rows[0]["SLANo"]);
                s.ValidUpto = Convert.ToString(_dt.Rows[0]["ValidUpto"]);
                s.Email = Convert.ToString(_dt.Rows[0]["Email"]);
                s.AltEmail = Convert.ToString(_dt.Rows[0]["AltEmail"]);
                s.IslaNo = Convert.ToString(_dt.Rows[0]["ISLANO"]);
                s.IniRef = Convert.ToString(_dt.Rows[0]["IniRef"]);
                s.AnyOther = Convert.ToString(_dt.Rows[0]["AnyOther"]);
                s.Experience = Convert.ToString(_dt.Rows[0]["Experience"]);
                s.IPAddress = Convert.ToString(_dt.Rows[0]["IPAddress"]);
                s.ApprovedBy = Convert.ToInt32(_dt.Rows[0]["ApprovedBy"]);
                s.R_ID = Convert.ToInt32(_dt.Rows[0]["R_ID"]);
                s.Password = Convert.ToString(_dt.Rows[0]["Password"]);
                s.ConfirmPassword = Convert.ToString(_dt.Rows[0]["ConfirmPassword"]);

            }
            return s;
        }
        public int updateSurveyorsPassword(int pk_SURID,string newPassword)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_SURID", pk_SURID), new SqlParameter("@Password",newPassword) };
            return _sf.executeNonQueryWithProc("uspUpdateSurveyorsPassword",_param);
        }
        public List<CSSurveyorDepartment> getSurveyorDepartmentNew(int fk_SurId)
        {
            _param = new SqlParameter[] { new SqlParameter("@FK_SurId", fk_SurId) };
            _dt = _sf.returnDTWithProc_executeReader("uspSurveyorDepartmentGetNew", _param);
            List<CSSurveyorDepartment> listSurveyorDept = new List<CSSurveyorDepartment>();
            if (_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSSurveyorDepartment sd = new CSSurveyorDepartment();
                    sd.PK_Id =Convert.ToInt32( row["PK_Id"]);
                    sd.DeptId =Convert.ToInt32(row["DeptId"]);
                    sd.DeptName =Convert.ToString(row["DeptName"]);
                   sd.FK_SurId= Convert.ToInt32(row["FK_SurId"]);
                    sd.IsActive =Convert.ToBoolean(row["IsActive"]);
                    listSurveyorDept.Add(sd);
                }
            }
            return listSurveyorDept;
        }

        public int saveSurveyorDepartmentNew(CSSurveyorDepartment sd)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_Id",sd.PK_Id), new SqlParameter("@FK_Deptid", sd.DeptId), new SqlParameter("@FK_SurId", sd.FK_SurId),new SqlParameter("@IsActive",sd.IsActive)
            };
            return _sf.executeNonQueryWithProc("uspSurveyorDepartmentSaveNew", _param);
        }
        #endregion

    }
    }

//if (_dt.Rows.Count > 0)
//{
//    return _dt.AsEnumerable().Select(row => new CSSurveyorDepartment
//    {

//        PK_Id = row.Field<int>("PK_Id"),
//        DeptId = row.Field<int>("DeptId"),
//        DeptName = row.Field<string>("DeptName"),
//        IsChecked = row.Field<bool>("IsChecked")
//    }).ToList();

//}
//else
//{
//    return new List<CSSurveyorDepartment>();
//}


//    if (_dt.Rows.Count>0)
//{
//    return _dt.AsEnumerable().Select(row => new City
//    {
//        cityId=row.Field<int>("cityId"),
//        cityName=row.Field<string>("cityName")
//    }).ToList();
//}
//else
//{
//    return new List<City>();
//}