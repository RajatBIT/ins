﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BLUserMaster
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;

        public BLUserMaster()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
            _sf = new SqlFunctions();
        }

        static BLUserMaster()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public string AddUsers(CSUserMaster objProperty)
        {
            string result = "";
            try
            {
                string password = BusinessHelper.CreateAutoPassword(objProperty.Email);
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 1);
                dbManager.AddParameters("@FkUserId", objProperty.FkUserId);
                dbManager.AddParameters("@FkRoleId", objProperty.FkRoleID);
                dbManager.AddParameters("@FirstName", objProperty.FirstName);
                dbManager.AddParameters("@LastName", objProperty.LastName);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@Designation", objProperty.Designation);
                dbManager.AddParameters("@District", objProperty.DistrictCode);
                dbManager.AddParameters("@State", objProperty.StateCode);
                dbManager.AddParameters("@Email", objProperty.Email);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@Password", password);
                result = dbManager.ExecuteNonQuery(DBSP.uspUserMaster, "@LoginId", System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public Int32 UpdateUsers(CSUserMaster objProperty)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 2);
                dbManager.AddParameters("@PkUserId", objProperty.PkUserId);
                dbManager.AddParameters("@FkRoleId", objProperty.FkRoleID);
                dbManager.AddParameters("@FirstName", objProperty.FirstName);
                dbManager.AddParameters("@LastName", objProperty.LastName);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@Designation", objProperty.Designation);
                dbManager.AddParameters("@District", objProperty.DistrictCode);
                dbManager.AddParameters("@State", objProperty.StateCode);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                result = dbManager.ExecuteNonQuery(DBSP.uspUserMaster, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }
        public Int32 DeleteUsers(string PkUserId)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 3);
                dbManager.AddParameters("@PkUserId", PkUserId);
                result = dbManager.ExecuteNonQuery(DBSP.uspUserMaster, System.Data.CommandType.StoredProcedure);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }
        public static List<CSUserMaster> BindUsers(CSUserMaster objSearch)
        {
            List<CSUserMaster> objProperty = new List<CSUserMaster>();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 4);
                dbManager.AddParameters("@PkUserId", objSearch.PkUserId);
                dbManager.AddParameters("@FkRoleId", objSearch.FkRoleID);
                //dbManager.AddParameters("@FirstName", objSearch.FirstName);
                //dbManager.AddParameters("@LastName", objSearch.LastName);
                //dbManager.AddParameters("@Address", objSearch.Address);
                //dbManager.AddParameters("@Designation", objSearch.Designation);
                //dbManager.AddParameters("@District", objSearch.District);
                //dbManager.AddParameters("@State", objSearch.State);
                //dbManager.AddParameters("@Email", objSearch.Email);
                dbManager.AddParameters("@Mobile", objSearch.Mobile);
                dbManager.AddParameters("@DateFrom", objSearch.DateFrom);
                dbManager.AddParameters("@DateTo", objSearch.DateTo);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspUserMaster, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    CSUserMaster objResult = new CSUserMaster();
                    objResult.PkUserId = (int)datareader["PkUserId"];
                    objResult.FkUserId = (int)datareader["FkUserId"];
                    objResult.FirstName = (string)datareader["FirstName"];
                    objResult.LastName = (string)datareader["LastName"];
                    objResult.FullName = (string)datareader["FirstName"] + " " + (string)datareader["LastName"];
                    objResult.Address = (string)datareader["Address"];
                    objResult.Designation = (string)datareader["Designation"];
                    objResult.DistrictCode =Convert.ToInt32(datareader["DistrictCode"]);
                    objResult.District = (string)datareader["District"];
                    objResult.StateCode = Convert.ToInt32(datareader["StateCode"]);
                    objResult.State = (string)datareader["State"];
                    objResult.Email = (string)datareader["Email"];
                    objResult.Mobile = (string)datareader["Mobile"];
                    objResult.CreateDate = (DateTime)datareader["CreateDate"];
                    objResult.RoleName = (string)datareader["RoleName"];
                    objResult.FkRoleID = (int)datareader["PKRoleID"];
                    objProperty.Add(objResult);
                }
            }
            catch
            {
                objProperty = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }
        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
       public List<CSRole> getRole()
        {
            List<CSRole> listRole = new List<CSRole>();
            listRole.Add(new CSRole {PKRoleID=0,RoleName="Select" });
            _dt = _sf.returnDTWithQuery_executeReader("SELECT * FROM tblMasterRole WHERE IsActive='1' ORDER BY RoleName");

           if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSRole r = new CSRole();
                    r.PKRoleID =Convert.ToInt32(row["PKRoleID"]);
                    r.RoleName =Convert.ToString(row["RoleName"]);
                    listRole.Add(r);
                }
            }
            return listRole;
        }

        public DataTable UserMasterValidation(CSUserMaster u)
        {
            _param = new SqlParameter[] {new SqlParameter("@PkUserId",u.PkUserId), new SqlParameter("@Email", u.Email), new SqlParameter("@Mobile", u.Mobile), new SqlParameter("@UserName", u.UserName) };
            return _sf.returnDTWithProc_executeReader("uspUserMasterValidation", _param);
        }
        public int saveUserMaster(CSUserMaster u)
        {
            _param = new SqlParameter[] {new SqlParameter("@PkUserId", u.PkUserId), new SqlParameter("@FkUserId", u.FkUserId), new SqlParameter("@FirstName", u.FirstName), new SqlParameter("@LastName", u.LastName), new SqlParameter("@Address", u.Address), new SqlParameter("@Designation", u.Designation), new SqlParameter("@State", u.StateCode), new SqlParameter("@District", u.DistrictCode), new SqlParameter("@Email", u.Email), new SqlParameter("@Mobile", u.Mobile), new SqlParameter("@UserName", u.UserName), new SqlParameter("@Password", u.Password), new SqlParameter("@ConfirmPassword", u.ConfirmPassword), new SqlParameter("@RoleID", u.RoleID) };
            return _sf.executeNonQueryWithProc("uspUserMasterSave", _param);
        }

        public List<CSUserMaster> getUserMaster()
        {
            List<CSUserMaster> listUser = new List<CSUserMaster>();
            _dt = _sf.returnDTWithProc_executeReader("uspUserMasterGet");

            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSUserMaster u = new CSUserMaster();
                    u.PkUserId = Convert.ToInt32(row["PkUserId"]);
                    u.FkUserId = Convert.ToInt32(row["FkUserId"]);
                    u.FirstName = Convert.ToString(row["FirstName"]);
                    u.LastName = Convert.ToString(row["LastName"]);
                    u.Address = Convert.ToString(row["Address"]);
                    u.Designation = Convert.ToString(row["Designation"]);
                    u.StateCode = Convert.ToInt32(row["State"]);
                    u.State = Convert.ToString(row["StateName"]);
                    u.DistrictCode = Convert.ToInt32(row["District"]);
                    u.District = Convert.ToString(row["DistrictName"]);
                    u.Email = Convert.ToString(row["Email"]);
                    u.Mobile = Convert.ToString(row["Mobile"]);
                    u.UserName = Convert.ToString(row["UserName"]);
                    u.Password = Convert.ToString(row["Password"]);
                    u.ConfirmPassword = Convert.ToString(row["ConfirmPassword"]);
                    u.RoleID = Convert.ToInt32(row["RoleID"]);
                    listUser.Add(u);
                }
            }
            return listUser;
        }


      public DataTable authenticateUser(string userName,string password)
        {
            _param = new SqlParameter[] {new SqlParameter("@UserName",userName),new SqlParameter("@Password",password) };
            _dt = _sf.returnDTWithProc_executeReader("uspUserMasterLogin",_param);
            return _dt; 
        }

    }
}
