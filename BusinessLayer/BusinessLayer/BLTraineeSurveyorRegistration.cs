﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class BLTraineeSurveyorRegistration
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion
        SqlParameter[] _param;
        public BLTraineeSurveyorRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        static BLTraineeSurveyorRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public DataTable BindTraineeDetails(string Status, int? FkSurId)
        {
            DataTable dtReult = null;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Status", Status);
                dbManager.AddParameters("@PK_TSURID", FkSurId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewPendingTrainee, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public String AddTraineeSurveyorRegistration(CSTraineeSurveyorRegistration objProperty)
        {
            string result = "";
            string SurveyorDepartments = "";
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PkTraineId",);
                dbManager.AddParameters("@UserType", objProperty.UserType);
                dbManager.AddParameters("@Fk_State", objProperty.FkStateId);
                dbManager.AddParameters("@Fk_District", objProperty.FkDistrictsId);
                dbManager.AddParameters("@FirstName", objProperty.FirstName);
                dbManager.AddParameters("@LastName", objProperty.LastName);
                dbManager.AddParameters("@UserName", objProperty.UserName);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@Email", objProperty.EmailId);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@Pincode", objProperty.Pincode);
                dbManager.AddParameters("@SLANo", objProperty.SLANo);
                dbManager.AddParameters("@IRDANO", objProperty.IRDANo);
                dbManager.AddParameters("@IslaNO", objProperty.ISLANO);
                dbManager.AddParameters("@Education", objProperty.Education);
                dbManager.AddParameters("@InsQualification", objProperty.InsQualification);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                dbManager.AddParameters("@Password", objProperty.Password);
                dbManager.AddParameters("@ConfirmPassword", objProperty.ConfirmPassword);
                result = dbManager.ExecuteNonQuery(DBSP.uspTraineeSurveyorsRegistration, "@Flag", CommandType.StoredProcedure);
                if (result.Contains("Y"))
                {
                    string pkId = result.Split('_')[1];
                    SurveyorDepartments = "INSERT INTO tblTraineeDepartment (Fk_DeptId,FK_TraineeId) VALUES";
                    foreach (string _dept in objProperty.ChDept)
                    {
                        SurveyorDepartments += "('" + _dept + "','" + pkId.ToString() + "'),";// "'" + _dept + "',";
                    }
                    dbManager.ExecuteNonQuery(SurveyorDepartments.TrimEnd(','));
                    result = "Your request for registering as a Trainee Surveyor received successfully. We will contact you shortly, Thank you.";
                }
                else if (result.Contains("U"))
                {
                    result = "User name Already Exists";
                }
                else if (result.Contains("S"))
                {
                    result = "SLA No Already Exists";
                }
                else if (result.Contains("E"))
                {
                    result = "Email Already Exists";
                }
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static String ApproveOrDiscardTrainee(string ApproveStatus, string FK_UserID, string FK_TSURID)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_UserID", FK_UserID);
                dbManager.AddParameters("@FK_TSURID", FK_TSURID);
                dbManager.AddParameters("@ApproveStatus", ApproveStatus);
                result = dbManager.ExecuteNonQuery(DBSP.uspApproveOrDiscardTrainee, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }


        public static DataTable SurveyorTraineeInfo(CSTraineeSurveyorRegistration objProperty)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PkTraineId",);
                dbManager.AddParameters("@Mode", objProperty.Mode);
                dbManager.AddParameters("@EmailORSLA", objProperty.SLANo);
                dbManager.AddParameters("@FK_TSURID", objProperty.PkTsurId);
                dbManager.AddParameters("@FK_SURID", objProperty.FkSurId);
                dbManager.AddParameters("@FK_ReqID", objProperty.FkReqId);
                dbManager.AddParameters("@IsLinked", objProperty.IsLinked);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspSurveyorTraineeInfo, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }


        public DataTable TraineeSurveyorsRegisterValidation(CSTraineeSurveyorRegistration t)
        {

            _param = new SqlParameter[] { new SqlParameter("@Email", t.EmailId), new SqlParameter("@Mobile", t.Mobile), new SqlParameter("@UserName", t.UserName)};
            return new SqlFunctions().returnDTWithProc_executeReader("uspTraineeSurveyorsRegisterValidation", _param);

        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////
    }
}
