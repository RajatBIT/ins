﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Configuration;

namespace InsuranceSurvey
{
    public class DBSP
    {
        public static String ConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            }
        }

        public static String oleConnectionString
        {
            get
            {
                return System.Configuration.ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
            }
        }

        public static String uspUserMaster
        {
            get { return "uspUserMaster"; }
        }

        public static String uspCheckUserExistance
        {
            get { return "uspCheckUserExistance"; }
        }

        public static String uspCheckSLANO
        {
            get { return "uspCheckSLANO"; }
        }

        public static String uspCheckEmailExistance
        {
            get { return "uspCheckEmailExistance"; }
        }

        public static String uspCheckIniRefExistance
        {
            get { return "uspCheckIniRefExistance"; }
        }

        public static String uspSurveyorsRegistration
        {
            get { return "uspSurveyorsRegistration"; }
        }

        public static String uspSurveyorDocs
        {
            get { return "uspSurveyorDocs"; }
        }

        public static String uspViewPendingSurveyors
        {
            get { return "uspViewPendingSurveyors"; }
        }

        public static String uspApproveOrDiscardSurveyor
        {
            get { return "uspApproveOrDiscardSurveyor"; }
        }

        public static String uspSurveyorLogin
        {
            get { return "uspSurveyorLogin"; }
        }

        public static String uspGenrateClaim
        {
            get { return "uspGenrateClaim"; }
        }

        public static String uspViewMyClaims
        {
            get { return "uspViewMyClaims"; }
        }

        public static String uspMarineSubmitJIR
        {
            get { return "uspMarineSubmitJIR"; }
        }

        public static String uspMarineSubmitDamageDetails
        {
            get { return "uspMarineSubmitDamageDetails"; }
        }

        public static String uspMarineGetDamageDetails
        {
            get { return "uspMarineGetDamageDetails"; }
        }

        public static String uspMarineDeleteDamageDetails
        {
            get { return "uspMarineDeleteDamageDetails"; }
        }

        public static String uspMarineSubmitConsignmentDocs
        {
            get { return "uspMarineSubmitConsignmentDocs"; }
        }

        public static String uspMarineGetConsignmentDocs
        {
            get { return "uspMarineGetConsignmentDocs"; }
        }

        public static String uspMarineDeleteConsignmentDocs
        {
            get { return "uspMarineDeleteConsignmentDocs"; }
        }

        public static String uspMarineGetDescriptionConsignmentDocs
        {
            get { return "uspMarineGetDescriptionConsignmentDocs"; }
        }

        public static String uspInsertMarineRemark
        {
            get { return "uspInsertMarineRemark"; }
        }

        public static String uspInsertMarineLOR
        {
            get { return "uspInsertMarineLOR"; }
        }

        public static String uspGetConsignmentType
        {
            get { return "uspGetConsignmentType"; }
        }

        public static String uspMarineGetDescriptionConsignmentDocsEdit
        {
            get { return "uspMarineGetDescriptionConsignmentDocsEdit"; }
        }

        public static String uspMarineGetJIR
        {
            get { return "uspMarineGetJIR"; }
        }

        public static String uspPDFMarineJIR
        {
            get { return "uspPDFMarineJIR"; }
        }

        public static String uspPDFMarineISVR
        {
            get { return "uspPDFMarineISVR"; }
        }

        public static String uspMarineSubmitISVR
        {
            get { return "uspMarineSubmitISVR"; }
        }

        public static String uspUpdatePolicyParticulars
        {
            get { return "uspUpdatePolicyParticulars"; }
        }

        public static String uspGetPolicyParticulars
        {
            get { return "uspGetPolicyParticulars"; }
        }

        public static String uspMarineTranshipment
        {
            get { return "uspMarineTranshipment"; }
        }
        public static String uspMarineAssessmentDetails
        {
            get { return "uspMarineAssessmentDetails"; }
        }
        public static String uspMarineSurveyorVisits
        {
            get { return "uspMarineSurveyorVisits"; }
        }
        public static String uspInsuredRegistration
        {
            get { return "uspInsuredRegistration"; }
        }
        public static String uspViewAllInsured
        {
            get { return "uspViewAllInsured"; }
        }
        public static String uspApproveOrDiscardInsured
        {
            get { return "uspApproveOrDiscardInsured"; }
        }
        public static String uspMarineClaimedCalculation
        {
            get { return "uspMarineClaimedCalculation"; }
        }
        public static String uspGetMarineISVR
        {
            get { return "uspGetMarineISVR"; }
        }
        public static String uspGetMarineISVRPhotos
        {
            get { return "uspGetMarineISVRPhotos"; }
        }
        public static String uspMarineSubmitISVRPhotoDocument
        {
            get { return "uspMarineSubmitISVRPhotoDocument"; }
        }
        public static String uspGetMarineISVRDocument
        {
            get { return "uspGetMarineISVRDocument"; }
        }
        public static String uspLogin
        {
            get { return "uspLogin"; }
        }

        public static String uspPDFMarineFSR
        {
            get { return "uspPDFMarineFSR"; }
        }

        public static String uspMarineGetLORDetails
        {
            get { return "uspMarineGetLORDetails"; }
        }
        public static String uspGetRegisteredInsuredDetails
        {
            get { return "uspGetRegisteredInsuredDetails"; }
        }
        public static String uspGetSurveyorDocsDetails
        {
            get { return "uspGetSurveyorDocsDetails"; }
        }
        public static String uspInsuredLogin
        {
            get { return "uspInsuredLogin"; }
        }
        public static String uspViewInsuredClaims
        {
            get { return "uspViewInsuredClaims"; }
        }
        public static String uspTraineeSurveyorsRegistration
        {
            get { return "uspTraineeSurveyorsRegistration"; }
        }
        public static String uspViewPendingTrainee
        {
            get { return "uspViewPendingTrainee"; }
        }
        public static String uspApproveOrDiscardTrainee
        {
            get { return "uspApproveOrDiscardTrainee"; }
        }
        public static String uspPDFMarineClaimForm
        {
            get { return "uspPDFMarineClaimForm"; }
        }
        public static String uspMarineSubmitFSR
        {
            get { return "uspMarineSubmitFSR"; }
        }
        public static String uspMarineUpdatePlaceOfSurvey
        {
            get { return "uspMarineUpdatePlaceOfSurvey"; }
        }
        public static String uspGetMarineFSR
        {
            get { return "uspGetMarineFSR"; }
        }
        public static String uspSurveyorTraineeInfo
        {
            get { return "uspSurveyorTraineeInfo"; }
        }
        public static String uspAssignedTask
        {
            get { return "uspAssignedTask"; }
        }
        public static String uspCheckRole
        {
            get { return "uspCheckRole"; }
        }
        public static String uspCorporateInsuredRegistration
        {
            get { return "uspCorporateInsuredRegistration"; }
        }
        public static String uspViewCorporateInsured
        {
            get { return "uspViewCorporateInsured"; }
        }
        public static String uspApproveOrDiscardCInsured
        {
            get { return "uspApproveOrDiscardCInsured"; }
        }
        public static String uspAssignedClaims
        {
            get { return "uspAssignedClaims"; }
        }
        public static String uspApprovSurveyor
        {
            get { return "uspApprovSurveyor"; }
        }
        public static String uspViewTraineeClaims
        {
            get { return "uspViewTraineeClaims"; }
        }
        public static String uspMarineContactPersonJIR
        {
            get { return "uspMarineContactPersonJIR"; }
        }
        public static String uspUpdateMarineRemindersLOR
        {
            get { return "uspUpdateMarineRemindersLOR"; }
        }
        public static String uspGetInsuredDetails
        {
            get { return "uspGetInsuredDetails"; }
        }
        public static String uspGenerateOTP
        {
            get { return "uspGenerateOTP"; }
        }
        public static String uspGetClaimDetails
        {
            get { return "uspGetClaimDetails"; }
        }
        public static String uspMotorSubmitVehicleParticulars
        {
            get { return "uspMotorSubmitVehicleParticulars"; }
        }
        public static String uspMotorGetVehicleParticulars
        {
            get { return "uspMotorGetVehicleParticulars"; }
        }
        public static String uspMotorSubmitDLParticulars
        {
            get { return "uspMotorSubmitDLParticulars"; }
        }
        public static String uspMotorGETDLParticulars
        {
            get { return "uspMotorGETDLParticulars"; }
        }
        public static String uspMotorSubmitOccurence
        {
            get { return "uspMotorSubmitOccurence"; }
        }
        public static String uspMotorGetOccurence
        {
            get { return "uspMotorGetOccurence"; }
        }
        public static String uspMotorGetLORDetails
        {
            get { return "uspMotorGetLORDetails"; }
        }
        public static String uspInsertMotorRemark
        {
            get { return "uspInsertMotorRemark"; }
        }
        public static String uspMotorGetSurveyorRemarks
        {
            get { return "uspMotorGetSurveyorRemarks"; }
        }
        public static String uspPDFMotorJIR
        {
            get { return "uspPDFMotorJIR"; }
        }
        public static String uspPDFMotorClaimForm
        {
            get { return "uspPDFMotorClaimForm"; }
        }

        public static String uspMarineGetLORReminders
        {
            get { return "uspMarineGetLORReminders"; }
        }
        public static String uspMotorSubmitEstimateLoss
        {
            get { return "uspMotorSubmitEstimateLoss"; }
        }
        public static String uspGetLicenseType
        {
            get { return "uspGetLicenseType"; }
        }
        public static String uspGetLossType
        {
            get { return "uspGetLossType"; }
        }
        public static String uspGetDamageType
        {
            get { return "uspGetDamageType"; }
        }

        /// <summary>
        /// ////////////////// new added
        /// </summary>
        public static String uspMotorGetPolicyParticulars
        {
            get { return "uspMotorGetPolicyParticulars"; }
        }
        public static String uspMotorUpdatePolicyParticulars
        {
            get { return "uspMotorUpdatePolicyParticulars"; }
        }
        public static String uspMotorGetRouteParticulars
        {
            get { return "uspMotorGetRouteParticulars"; }
        }
        public static String uspMotorSubmitRouteParticulars
        {
            get { return "uspMotorSubmitRouteParticulars"; }
        }
        public static String usp_GetMotorPartsMaster
        {
            get { return "usp_GetMotorPartsMaster"; }
        }
        public static String usp_GetMotorPartsAffects
        {
            get { return "usp_GetMotorPartsAffects"; }
        }
        public static String usp_GetMotorPartsClaim
        {
            get { return "usp_GetMotorPartsClaim"; }
        }
        public static String usp_SubmitMotorPartsClaim
        {
            get { return "usp_SubmitMotorPartsClaim"; }
        }
        public static String usp_GetMotorParticularsRemarksClaim
        {
            get { return "usp_GetMotorParticularsRemarksClaim"; }
        }
        public static String usp_InsertMotorParticularsRemarksClaim
        {
            get { return "usp_InsertMotorParticularsRemarksClaim"; }
        }

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~ */
        #region MISC ISVR

        public static String usp_GetMIS_ISVR_PDF
        {
            get { return "usp_GetMIS_ISVR_PDF"; }
        }


        #endregion

    }
}