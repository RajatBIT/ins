﻿using BusinessLayer.BL;
using BusinessLayer.DAL;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class BLCommon
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion
        static BLCommon()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        #region Function Bind For State & Districts
        /// <summary>
        /// BindStates function use for bind a State 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindStates()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT StateCode,StateName FROM tblStates WHERE IsActive='1' ORDER BY StateName");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["StateCode"].ToString(), datareader["StateName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }



        ///// <summary>
        /// BindDistricts function use for bind a Districts list against of state
        /// </summary>
        /// <param name="pkStateId"></param>
        /// <returns></returns>
        public static Dictionary<string, string> BindDistricts(string pkStateId)
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT PkDistrictId,DistrictName FROM tblDistricts WHERE Statecode='" + pkStateId + "' AND IsActive='1' ORDER BY DistrictName");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["PkDistrictId"].ToString(), datareader["DistrictName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindRole()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT * FROM tblMasterRole WHERE IsActive='1' ORDER BY RoleName");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["PKRoleID"].ToString(), datareader["RoleName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindDepartments()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT * FROM tblMasterDepartments WHERE IsActive='1' ORDER BY DeptName");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["DeptId"].ToString(), datareader["DeptName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindMembershipType()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT * FROM tblMasterMembershipType WHERE IsActive='1' ORDER BY TypeName");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["TypeName"].ToString(), datareader["TypeName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindInsurersType()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT * FROM tblInsurers WHERE IsActive='1' ORDER BY Name");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["PK_InsrID"].ToString(), datareader["Name"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }


        public static Dictionary<string, string> BindNaturiofTransit()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT Value FROM tblNaturiofTransit ORDER BY ID");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["Value"].ToString(), datareader["Value"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }



        /// <summary>
        /// 
        /// </summary>
        /// <param name="FK_ClaimId"></param>
        /// <returns></returns>
        public static DataTable BindMarineRemarksMaster(string FK_ClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dtResult = dbManager.ExecuteDataTable("SELECT t1.PK_ID,t1.Name,dbo.CheckMarineRemarksClaimExists(t1.PK_ID," + FK_ClaimId + ") AS 'CheckMarineRemarksClaimExists' FROM tblMarineRemarksMaster t1 ORDER BY Name");
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="FK_ClaimId"></param>
        /// <returns></returns>
        public static DataTable BindMarineLORMaster(string FK_ClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dtResult = dbManager.ExecuteDataTable("SELECT * from dbo.GETMarineLORDetails(" + FK_ClaimId + ")");
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindCoverageType()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT Coverage FROM tblCoverage WHERE IsActive='1' ORDER BY Coverage ASC");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["Coverage"].ToString(), datareader["Coverage"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }
        #endregion

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> BindCalulationMaster(string task)
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader("SELECT CalDescription FROM tblMarineCalulationMaster where Task=case '" + task + "' when 'CL' then 'CL' else Task END ORDER BY CalDescription");
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["CalDescription"].ToString(), datareader["CalDescription"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        public static Dictionary<string, string> BindMotorClass()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                //IDataReader datareader = dbManager.ExecuteReader("SELECT StateCode,StateName FROM tblStates WHERE IsActive='1' ORDER BY StateName");
                _dictionary.Add("0", "Select");
                _dictionary.Add("A", "A");
                _dictionary.Add("B", "B");
                _dictionary.Add("C", "C");
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/

        /// <summary>
        /// Motor Work Starts here
        /// </summary>
        /// <param name="FK_ClaimId"></param>
        /// <returns></returns>
        public static DataTable BindMotorRemarksMaster(string FK_ClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dtResult = dbManager.ExecuteDataTable("SELECT t1.PK_ID,t1.Name,dbo.CheckMotorRemarksClaimExists(t1.PK_ID," + FK_ClaimId + ") AS 'CheckMotorRemarksClaimExists' FROM tblMotorRemarksMaster t1 ORDER BY Name");
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable BindMotorLORMaster(string FK_ClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dtResult = dbManager.ExecuteDataTable("SELECT * from dbo.GETMotorLORDetails(" + FK_ClaimId + ")");
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        // added by dhiraj
        public static Dictionary<string, string> BindMotorParts()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader(DBSP.usp_GetMotorPartsMaster, CommandType.StoredProcedure);
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["PK_ID"].ToString(), datareader["PartName"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }
        public static Dictionary<string, string> BindMotorPartsAffects()
        {
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            try
            {
                dbManager.ConnectionOpen();
                IDataReader datareader = dbManager.ExecuteReader(DBSP.usp_GetMotorPartsAffects, CommandType.StoredProcedure);
                _dictionary.Add("0", "Select");
                while (datareader.Read())
                {
                    _dictionary.Add(datareader["PK_ID"].ToString(), datareader["Affect"].ToString());
                }
            }
            catch
            {
                _dictionary = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dictionary;
        }

        public static List<CSMISCNatureofLoss> getNatureofLoss(int deptId)
        {
            SqlParameter[] param;
            param = new SqlParameter[] { new SqlParameter("@DeptId", deptId) };
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspNatureOfLossGet_byShortName", param);
            List<CSMISCNatureofLoss> listNL = new List<CSMISCNatureofLoss>();
            listNL.Insert(0, new CSMISCNatureofLoss { NatureOfLoss = "Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCNatureofLoss nl = new CSMISCNatureofLoss();
                    nl.PK_NL_ID = Convert.ToInt32(row["PK_NL_ID"]);
                    nl.NatureOfLoss = row["NatureofLoss"].ToString();
                    listNL.Add(nl);
                }

            }
            return listNL;
        }
        public static List<CSMarineExConditionPacking> getMarineExConditionPacking_JIR(string str)
        {
            if (str == null)
            {
                str = string.Empty;
            }
            string[] temp = str.Split(',');
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineExConditionPackingGet");
            List<CSMarineExConditionPacking> listECP = new List<CSMarineExConditionPacking>();
            //listECP.Insert(0,new CSMarineExConditionPacking { PK_EC_ID=0,ExConditionPacking="Select"});
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineExConditionPacking e = new CSMarineExConditionPacking();
                    e.PK_EC_ID = Convert.ToInt32(row["PK_EC_ID"]);
                    e.ExConditionPacking = row["ExConditionPacking"].ToString();
                    foreach (var item in temp)
                    {
                        if (Convert.ToString(e.PK_EC_ID) == item)
                        {
                            e.Selected = true;
                        }

                    }
                    //c.selected = true;
                    if (e.Selected != true)
                    {
                        e.Selected = false;
                    }

                    listECP.Add(e);
                }

            }
            return listECP;
        }
        public static List<CSMarineExConditionPacking> getMarineExConditionPacking()
        {
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineExConditionPackingGet");
            List<CSMarineExConditionPacking> listECP = new List<CSMarineExConditionPacking>();
            //listECP.Insert(0,new CSMarineExConditionPacking { PK_EC_ID=0,ExConditionPacking="Select"});
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineExConditionPacking e = new CSMarineExConditionPacking();
                    e.PK_EC_ID = Convert.ToInt32(row["PK_EC_ID"]);
                    e.ExConditionPacking = row["ExConditionPacking"].ToString();
                    listECP.Add(e);
                }

            }
            return listECP;
        }
        public static List<CSMarinePointOfDeliveryToConsignee> getPointOfDeliveryToConsigneeGet()
        {

            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarinePointOfDeliveryToConsigneeGet");
            List<CSMarinePointOfDeliveryToConsignee> listPD = new List<CSMarinePointOfDeliveryToConsignee>();
            listPD.Insert(0, new CSMarinePointOfDeliveryToConsignee { PointOfDeliveryToConsignee = "Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarinePointOfDeliveryToConsignee p = new CSMarinePointOfDeliveryToConsignee();

                    p.PointOfDeliveryToConsignee = row["PointOfDeliveryToConsignee"].ToString();
                    listPD.Add(p);
                }

            }
            return listPD;
        }
        public static List<CSMarineIncotermOfTheSale> getIncotermOfTheSale()
        {

            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineIncotermOfTheSaleGet");
            List<CSMarineIncotermOfTheSale> listIS = new List<CSMarineIncotermOfTheSale>();
            listIS.Insert(0, new CSMarineIncotermOfTheSale { IncotermSale = "Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineIncotermOfTheSale i = new CSMarineIncotermOfTheSale();
                    i.IncotermSale = row["IncotermSale"].ToString();
                    listIS.Add(i);
                }

            }
            return listIS;
        }
        public static List<CSMarineCauseOfLoss> getCauseOfLoss()
        {

            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineCauseOfLossGet");
            List<CSMarineCauseOfLoss> listCL = new List<CSMarineCauseOfLoss>();
            listCL.Insert(0, new CSMarineCauseOfLoss { CauseOfLoss = "Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineCauseOfLoss c = new CSMarineCauseOfLoss();
                    c.CauseOfLoss = row["CauseOfLoss"].ToString();
                    listCL.Add(c);
                }

            }
            return listCL;
        }
        public static List<CSMarineCommentForDeliveryReceipt> getCommentForDeliveryReceipt()
        {
            //string[] temp = { "1", "5" };
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineCommentForDeliveryReceiptGet");
            List<CSMarineCommentForDeliveryReceipt> listCD = new List<CSMarineCommentForDeliveryReceipt>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineCommentForDeliveryReceipt c = new CSMarineCommentForDeliveryReceipt();
                    c.PK_CD_ID = Convert.ToInt32(row["PK_CD_ID"]);
                    c.CommentForDeliveryReceipt = row["CommentForDeliveryReceipt"].ToString();
                    c.selected = false;

                    listCD.Add(c);
                }

            }
            return listCD;
        }

        public static List<CSMarineCommentForDeliveryReceipt> getCommentForDeliveryReceiptTesting(string str)
        {
            //string[] temp = { "1", "5" };
            if (str == null)
            {
                str = string.Empty;
            }
            string[] temp = str.Split(',');
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineCommentForDeliveryReceiptGet");
            List<CSMarineCommentForDeliveryReceipt> listCD = new List<CSMarineCommentForDeliveryReceipt>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineCommentForDeliveryReceipt c = new CSMarineCommentForDeliveryReceipt();
                    c.PK_CD_ID = Convert.ToInt32(row["PK_CD_ID"]);
                    c.CommentForDeliveryReceipt = row["CommentForDeliveryReceipt"].ToString();
                    foreach (var item in temp)
                    {
                        if (Convert.ToString(c.PK_CD_ID) == item)
                        {
                            c.selected = true;
                        }

                    }
                    //c.selected = true;
                    if (c.selected != true)
                    {
                        c.selected = false;
                    }

                    listCD.Add(c);
                }

            }
            return listCD;
        }
        public static List<CSMarineRemarksForNature> getRemarksForNature()
        {

            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineRemarksForNatureGet");
            List<CSMarineRemarksForNature> listRN = new List<CSMarineRemarksForNature>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineRemarksForNature r = new CSMarineRemarksForNature();
                    r.PK_RN_ID = Convert.ToInt32(row["PK_RN_ID"]);
                    r.RemarksForNature = row["RemarksForNature"].ToString();
                    listRN.Add(r);
                }

            }
            return listRN;
        }
        public static List<CSMarineConcurenceConsent> getConcurenceConsent()
        {

            var dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineConcurenceConsentGet");
            List<CSMarineConcurenceConsent> listCC = new List<CSMarineConcurenceConsent>();
            listCC.Insert(0, new CSMarineConcurenceConsent { PK_CC_ID = 0, ConcurenceConsent = "Select" });
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CSMarineConcurenceConsent cc = new CSMarineConcurenceConsent();
                    cc.PK_CC_ID = Convert.ToInt32(row["PK_CC_ID"]);
                    cc.ConcurenceConsent = row["ConcurenceConsent"].ToString();
                    listCC.Add(cc);
                }

            }
            return listCC;
        }
        //public static List<CSMarineModeOfConveyance> getModeOfConveyance()
        //{
        //    //string[] temp = { "1", "5" };
        //    var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineModeOfConveyanceGet");
        //    List<CSMarineModeOfConveyance> listMC = new List<CSMarineModeOfConveyance>();
        //    if (_dt.Rows.Count > 0)
        //    {
        //        foreach (DataRow row in _dt.Rows)
        //        {
        //            CSMarineModeOfConveyance mc = new CSMarineModeOfConveyance();
        //            mc.PK_MC_ID =Convert.ToInt32(row["PK_MC_ID"]);
        //            mc.ModeOfConveyance= row["ModeOfConveyance"].ToString();
        //            mc.Selected = false;
        //            listMC.Add(mc);
        //        }

        //    }
        //    return listMC;
        //}
        public static List<CSMarineModeOfConveyance> getModeOfConveyance(string str)
        {
            if (str == null)
            {
                str = string.Empty;
            }
            string[] temp = str.Split(',');
            var _dt = new SqlFunctions().returnDTWithProc_executeReader("uspMarineModeOfConveyanceGet");
            List<CSMarineModeOfConveyance> listMC = new List<CSMarineModeOfConveyance>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineModeOfConveyance mc = new CSMarineModeOfConveyance();
                    mc.PK_MC_ID = Convert.ToInt32(row["PK_MC_ID"]);
                    mc.ModeOfConveyance = row["ModeOfConveyance"].ToString();
                    foreach (var item in temp)
                    {
                        if (Convert.ToString(mc.PK_MC_ID) == item)
                        {
                            mc.Selected = true;
                        }

                    }
                    //c.selected = true;
                    if (mc.Selected != true)
                    {
                        mc.Selected = false;
                    }

                    listMC.Add(mc);
                }

            }
            return listMC;
        }
        public static List<CSRegionMaster> getRegion()
        {
            var dt = new SqlFunctions().returnDTWithQuery_executeReader("select PK_R_ID,Region from tblRegionMaster");
            List<CSRegionMaster> listRegion = new List<CSRegionMaster>();
            listRegion.Insert(0, new CSRegionMaster { PK_R_ID = 0, Region = "Select" });
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    CSRegionMaster r = new CSRegionMaster();
                    r.PK_R_ID = Convert.ToInt32(row["PK_R_ID"]);
                    r.Region = Convert.ToString(row["Region"]);
                    listRegion.Add(r);
                }
            }
            return listRegion;
        }
        public static List<CSState> getState_byRegion(int r_Id)
        {
            List<CSState> listState = new List<CSState>();
            DataTable dt = new DataTable();
            listState.Insert(0,new CSState { StateCode=0,StateName="Select"});
            dt = new SqlFunctions().returnDTWithQuery_executeReader("SELECT StateCode,StateName FROM tblStates WHERE IsActive='1' and R_ID=" + r_Id + " ORDER BY StateName");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    CSState s = new CSState();
                    s.StateCode = Convert.ToInt32(row["StateCode"]);
                    s.StateName = Convert.ToString(row["StateName"]);
                    listState.Add(s);
                }
            }
            return listState;


        }
        public static List<CSState> getState()
        {
            List<CSState> listState = new List<CSState>();
            DataTable dt = new DataTable();
            listState.Insert(0, new CSState { StateCode = 0, StateName = "Select" });
            dt = new SqlFunctions().returnDTWithQuery_executeReader("SELECT StateCode,StateName FROM tblStates WHERE IsActive='1' ORDER BY StateName");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow row in dt.Rows)
                {
                    CSState s = new CSState();
                    s.StateCode = Convert.ToInt32(row["StateCode"]);
                    s.StateName = Convert.ToString(row["StateName"]);
                    listState.Add(s);
                }
            }
            return listState;


        }
        public static List<CSDistricts> getDistricts_byStateId(int FkState)
        {
                List<CSDistricts> listDistricts = new List<CSDistricts>();
                 DataTable dt = new DataTable();
                listDistricts.Insert(0, new CSDistricts { PKDistrictsId = 0, DistrictName = "Select" });
                dt = new SqlFunctions().returnDTWithQuery_executeReader("SELECT PkDistrictId,DistrictName FROM tblDistricts WHERE Statecode='" + FkState + "' AND IsActive='1' ORDER BY DistrictName");
                if (dt.Rows.Count > 0)
                {

                    foreach (DataRow row in dt.Rows)
                    {
                    CSDistricts d = new CSDistricts();
                        d.PKDistrictsId = Convert.ToInt32(row["PkDistrictId"]);
                        d.DistrictName= Convert.ToString(row["DistrictName"]);
                    listDistricts.Add(d);
                    }
                }
            return listDistricts;
        }
        public static List<T> BindList<T>(DataTable dt)
        {
            // Example 1:
            // Get private fields + non properties
            //var fields = typeof(T).GetFields(BindingFlags.NonPublic | BindingFlags.Instance);

            // Example 2: Your case
            // Get all public fields
            var fields = typeof(T).GetFields();

            List<T> lst = new List<T>();

            foreach (DataRow dr in dt.Rows)
            {
                // Create the object of T
                var ob = Activator.CreateInstance<T>();

                foreach (var fieldInfo in fields)
                {
                    foreach (DataColumn dc in dt.Columns)
                    {
                        // Matching the columns with fields
                        if (fieldInfo.Name == dc.ColumnName)
                        {
                            // Get the value from the datatable cell
                            object value = dr[dc.ColumnName];

                            // Set the value into the object
                            fieldInfo.SetValue(ob, value);
                            break;
                        }
                    }
                }

                lst.Add(ob);
            }

            return lst;
        }

        public static DataTable getMembershipType()
        {
            
            DataTable dt = new DataTable();
            dt = new SqlFunctions().returnDTWithQuery_executeReader("SELECT * FROM tblMasterMembershipType WHERE IsActive='1' ORDER BY TypeName");
            return dt;
        }

    }

}
