﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSSalvage_Query_Offer
    {
        public int PK_SQ_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int AS_ID { get; set; }
        public string Query { get; set; }
        public int PK_SO_ID { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public string GST { get; set; }
        public string EMD_Amount { get; set; }
        public string Full_Amount { get; set; }


    }
}
