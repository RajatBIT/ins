﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSInsuredRegPolicyNo_Insurers
    {
        public int PK_PI_ID { get; set; }
        public int InsdID { get; set; }
        public string PolicyNo { get; set; }
        public int InsrID { get; set; }

    }
}
