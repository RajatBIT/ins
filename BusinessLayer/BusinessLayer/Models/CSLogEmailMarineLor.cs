﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSLogEmailMarineLor
    {
        public int PK_Log_ID { get; set; }
        public int LE_ID { get; set; }
        public string ToEmailIds { get; set; }
        public string CCEmailIds { get; set; }
        public string Subject { get; set; }
        public string MailMsg { get; set; }


    }
}
