﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCFSRNatureExtentDamage
    {
        public int PK_MISCFSRNEDID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string DescAffectedItem { get; set; }
        public decimal Qty { get; set; }
        public string UOM { get; set; }
        public string BlockNo { get; set; }
        public string BuildingNo { get; set; }
        public string FloorNo { get; set; }
        public string NameArea { get; set; }
        public string MaterialPlacing { get; set; }
        public string HeightGround { get; set; }
        public string PackingMaterial { get; set; }
        public string PackingSize { get; set; }
        public string LengthAffectedArea { get; set; }
        public string Breadth { get; set; }
        public string Height { get; set; }
        public string Functions { get; set; }
        public string Stage { get; set; }
        public string WIPStage { get; set; }
        public string BatchSize { get; set; }
        public string OccupancyAffectedArea { get; set; }
        public string NatureDamageLoss { get; set; }
        public string ExtentDamageLoss { get; set; }
        public string Model { get; set; }
        public string Make { get; set; }
        public string SrNo { get; set; }
        public string Capacity { get; set; }
        public string CountryOrigin { get; set; }
        public string MfgYear { get; set; }
        public string WorkingHours { get; set; }
        public string Properties { get; set; }
        public string MOC { get; set; }
        public bool ChkbxPS { get; set; }
        public string SrNoPS { get; set; }

        public int PSI_ID { get; set; }
    }
}
