﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCOperatedPeril
    {
        public int PK_OP_ID { get; set; }
        public string NameOperatedPeril { get; set; }
    }
}
