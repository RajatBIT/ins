﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMISCRecoverySubrogation
    {
        public int PK_RS_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string AspectsofRecovery { get; set; }
        public string SubrogationRights { get; set; }
    }
}
