﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMarineLorEmailIdsNew
    {
        public int PK_LE_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int LH_ID { get; set; }
        public string ToEmailIds { get; set; }
        public string CCEmailIds { get; set; }
        public Boolean ToInsured { get; set; }
        public Boolean CCInsured { get; set; }
        public Boolean ToInsurers { get; set; }
        public Boolean CCInsurers { get; set; }
        public Boolean ToBrokers { get; set; }
        public Boolean CCBrokers { get; set; }
        public int FrequencyOfReminder { get; set; }

        public Boolean FinalNotice { get; set; }
    }
}
