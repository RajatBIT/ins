﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSRole
    {
        public int PKRoleID { get; set; }
        public string RoleName { get; set; }
    }
}
