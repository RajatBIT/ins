﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSAllTaskWithPendingTask
    {
        public CSTaskMaster task { get; set; }
        public List<CSPendingTask> listPendingTask { get; set; }
    }
}
