﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineNoClaim
    {
        public int PK_NC_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int RC_ID { get; set; }
        public string SurveyorsRemarks { get; set; }

    }
}
