﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCRemarkAdv
    {
        public int RemarkAdvID { get; set; }
        public string RemarkAdv { get; set; }
    }
}
