﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSMarineExConditionPacking
    {
        public int PK_EC_ID  { get; set; }
        public string ExConditionPacking { get; set; }
        public bool Selected { get; set; }
    }
}
