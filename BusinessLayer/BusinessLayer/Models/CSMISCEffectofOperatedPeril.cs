﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSMISCEffectofOperatedPeril
    {
        public int Pk_EOP_ID { get; set; }
        public string EffectofOperatedPeril { get; set; }
    }
}
