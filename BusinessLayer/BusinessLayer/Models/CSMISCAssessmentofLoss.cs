﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public class CSMISCAssessmentofLoss
    {
        public int PK_AL_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string DeterQty { get; set; }
        public string VerificationRates { get; set; }
        public string AssesmentSummary { get; set; }
        public string DepriReinst { get; set; }
        public string DeductHBD { get; set; }
        public string ObsolSlowDead { get; set; }
        public string UpgradeModification { get; set; }
        public string Salvage { get; set; }
        public string AdequacySumInsured { get; set; }
        public int PSI_ID { get; set; }

    }
}
