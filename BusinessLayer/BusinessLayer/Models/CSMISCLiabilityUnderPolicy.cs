﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCLiabilityUnderPolicy
    {
        public int PK_MISCLUPID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string TypeOfPolicy { get; set; }
        public int OP_ID { get; set; }
        public string CaptionedFalling { get; set; }
        public string CaptionedFallingText { get; set; }
        public string BreachWarranty { get; set; }
        public string LossOccuredPolicy { get; set; }
        public string LossLocationFound { get; set; }
        public string InsuredReasonablyAdopted { get; set; }
        public string AccidentGrad { get; set; }
        public string ForeUnfore { get; set; }
        public string PhyImm { get; set; }
        public string AggravationLoss { get; set; }
        public string AggravationLossText { get; set; }
        public string HeatingBurning { get; set; }
        public string AffectedProperty { get; set; }
        public string ConclusionInsurers { get; set; }
        public string BlankText { get; set; }

    }
}
