﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSUserMaster
    {
        private int _PkUserId;

        public int PkUserId
        {
            get { return _PkUserId; }
            set { _PkUserId = value; }
        }
        private int _FkUserId;

        public int FkUserId
        {
            get { return _FkUserId; }
            set { _FkUserId = value; }
        }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }

        private string _fullName;

        public string FullName
        {
            get { return _fullName; }
            set { _fullName = value; }
        }

        private string _Designation;

        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private int _StateCode;

        public int StateCode
        {
            get { return _StateCode; }
            set { _StateCode = value; }
        }

        private string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
        private int _DistrictCode;

        public int DistrictCode
        {
            get { return _DistrictCode; }
            set { _DistrictCode = value; }
        }
        private string _District;

        public string District
        {
            get { return _District; }
            set { _District = value; }
        }
        private int _FkRoleID;

        public int FkRoleID
        {
            get { return _FkRoleID; }
            set { _FkRoleID = value; }
        }
        private DateTime _CreateDate;

        public DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }
        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        private DateTime? _DateFrom;

        public DateTime? DateFrom
        {
            get { return _DateFrom; }
            set { _DateFrom = value; }
        }
        private DateTime? _DateTo;

        public DateTime? DateTo
        {
            get { return _DateTo; }
            set { _DateTo = value; }
        }
        private string _RoleName;

        public string RoleName
        {
            get { return _RoleName; }
            set { _RoleName = value; }
        }
        private string _BtnSubmit;

        public string BtnSubmit
        {
            get { return _BtnSubmit; }
            set { _BtnSubmit = value; }
        }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int RoleID { get; set; }
        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
    }
}
