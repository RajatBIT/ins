﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSMISC_ACL_NatureofLoss
    {
        public int PK_NL_ID { get; set; }
        public string NatureofLoss { get; set; }
        public string ShortName { get; set; }

    }
}
