﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLayer.Models
{
    public class CSMotorSurvey
    {
        private int _PK_ID;

        public int PK_ID
        {
            get { return _PK_ID; }
            set { _PK_ID = value; }
        }
        private string _FK_ClaimID;

        public string FK_ClaimID
        {
            get { return _FK_ClaimID; }
            set { _FK_ClaimID = value; }
        }
        private string _RCNO;

        public string RCNO
        {
            get { return _RCNO; }
            set { _RCNO = value; }
        }
        private string _VehicleType;

        public string VehicleType
        {
            get { return _VehicleType; }
            set { _VehicleType = value; }
        }
        private string _RCDate;

        public string RCDate
        {
            get { return _RCDate; }
            set { _RCDate = value; }
        }
        private string _ChasisNo;

        public string ChasisNo
        {
            get { return _ChasisNo; }
            set { _ChasisNo = value; }
        }
        private string _EngineNo;

        public string EngineNo
        {
            get { return _EngineNo; }
            set { _EngineNo = value; }
        }
        private string _Make;

        public string Make
        {
            get { return _Make; }
            set { _Make = value; }
        }
        private string _Model;

        public string Model
        {
            get { return _Model; }
            set { _Model = value; }
        }
        private string _Class;

        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }
        private string _BodyType;

        public string BodyType
        {
            get { return _BodyType; }
            set { _BodyType = value; }
        }
        private string _Seating;

        public string Seating
        {
            get { return _Seating; }
            set { _Seating = value; }
        }
        private string _Fuel;

        public string Fuel
        {
            get { return _Fuel; }
            set { _Fuel = value; }
        }
        private string _Color;

        public string Color
        {
            get { return _Color; }
            set { _Color = value; }
        }
        private string _FitnessUpto;

        public string FitnessUpto
        {
            get { return _FitnessUpto; }
            set { _FitnessUpto = value; }
        }
        private string _PermitType;

        public string PermitType
        {
            get { return _PermitType; }
            set { _PermitType = value; }
        }
        private string _PermitUpto;

        public string PermitUpto
        {
            get { return _PermitUpto; }
            set { _PermitUpto = value; }
        }
        private string _OperationArea;

        public string OperationArea
        {
            get { return _OperationArea; }
            set { _OperationArea = value; }
        }
        private string _LadenWeight;

        public string LadenWeight
        {
            get { return _LadenWeight; }
            set { _LadenWeight = value; }
        }
        private string _UnLadenWeight;

        public string UnLadenWeight
        {
            get { return _UnLadenWeight; }
            set { _UnLadenWeight = value; }
        }
        private string _TaxOTT;

        public string TaxOTT
        {
            get { return _TaxOTT; }
            set { _TaxOTT = value; }
        }
        private string _ATFDevice;

        public string ATFDevice
        {
            get { return _ATFDevice; }
            set { _ATFDevice = value; }
        }
        private string _PACondition;

        public string OdometerReading { get; set; }
        public  string Template { get; set; }
        public string Insured { get; set; }
        public Boolean Petrol { get; set; }
        public bool CNG { get; set; }
        public bool Diesel { get; set; }
        public bool LPG { get; set; }

        public string PACondition
        {
            get { return _PACondition; }
            set { _PACondition = value; }
        }
        private string _FInterest;

        public string FInterest
        {
            get { return _FInterest; }
            set { _FInterest = value; }
        }
        private string _CubCapcity;

        public string CubCapcity
        {
            get { return _CubCapcity; }
            set { _CubCapcity = value; }
        }
        private string _Remark;

        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }
        private string _RCFile;

        public string RCFile
        {
            get { return _RCFile; }
            set { _RCFile = value; }
        }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }

        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private DateTime _UpdatedDate;

        public DateTime UpdatedDate
        {
            get { return _UpdatedDate; }
            set { _UpdatedDate = value; }
        }

        private string _DriverName;

        public string DriverName
        {
            get { return _DriverName; }
            set { _DriverName = value; }
        }
        private string _DLNo;

        public string DLNo
        {
            get { return _DLNo; }
            set { _DLNo = value; }
        }

        private string _IssueDate;

        public string IssueDate
        {
            get { return _IssueDate; }
            set { _IssueDate = value; }
        }

        private string _ValidUpTo;

        public string ValidUpTo
        {
            get { return _ValidUpTo; }
            set { _ValidUpTo = value; }
        }

        private string _DLAuthority;

        public string DLAuthority
        {
            get { return _DLAuthority; }
            set { _DLAuthority = value; }
        }

        private string _Endorsment;

        public string Endorsment
        {
            get { return _Endorsment; }
            set { _Endorsment = value; }
        }

        private string _Involve;

        public string Involve
        {
            get { return _Involve; }
            set { _Involve = value; }
        }

        private string _LicenseType;

        public string LicenseType
        {
            get { return _LicenseType; }
            set { _LicenseType = value; }
        }

        private string _DLRemarks;

        public string DLRemarks
        {
            get { return _DLRemarks; }
            set { _DLRemarks = value; }
        }

        private string _DLFile;

        public string DLFile
        {
            get { return _DLFile; }
            set { _DLFile = value; }
        }
        private string _AccDate;

        public string AccDate
        {
            get { return _AccDate; }
            set { _AccDate = value; }
        }
        private string _AccPlace;

        public string AccPlace
        {
            get { return _AccPlace; }
            set { _AccPlace = value; }
        }
        private string _SpotSurvey;

        public string SpotSurvey
        {
            get { return _SpotSurvey; }
            set { _SpotSurvey = value; }
        }
        private string _FIR;

        public string FIR
        {
            get { return _FIR; }
            set { _FIR = value; }
        }
        private string _FireReport;

        public string FireReport
        {
            get { return _FireReport; }
            set { _FireReport = value; }
        }
        private string _TPPLoss;

        public string TPPLoss
        {
            get { return _TPPLoss; }
            set { _TPPLoss = value; }
        }
        private string _TPBInjury;

        public string TPBInjury
        {
            get { return _TPBInjury; }
            set { _TPBInjury = value; }
        }
        private string _DamageSides;

        public string DamageSides
        {
            get { return _DamageSides; }
            set { _DamageSides = value; }
        }
        private string _OccRemarks;

        public string OccRemarks
        {
            get { return _OccRemarks; }
            set { _OccRemarks = value; }
        }
        private string _LossDesc;

        public string LossDesc
        {
            get { return _LossDesc; }
            set { _LossDesc = value; }
        }

        private string _Photo1;

        public string Photo1
        {
            get { return _Photo1; }
            set { _Photo1 = value; }
        }
        private string _Photo2;

        public string Photo2
        {
            get { return _Photo2; }
            set { _Photo2 = value; }
        }

        private HttpPostedFileBase _docFile;

        public HttpPostedFileBase DocFile
        {
            get { return _docFile; }
            set { _docFile = value; }
        }

        private HttpPostedFileBase _docFile1;

        public HttpPostedFileBase DocFile1
        {
            get { return _docFile1; }
            set { _docFile1 = value; }
        }

        private string _EstimatedLoss;

        public string EstimatedLoss
        {
            get { return _EstimatedLoss; }
            set { _EstimatedLoss = value; }
        }
        private string _RecommendedLoss;

        public string RecommendedLoss
        {
            get { return _RecommendedLoss; }
            set { _RecommendedLoss = value; }
        }
        private string _RemarksISVR;

        public string RemarksISVR
        {
            get { return _RemarksISVR; }
            set { _RemarksISVR = value; }
        }

        private string[] _checkBoxList;

        public string[] CheckBoxList
        {
            get { return _checkBoxList; }
            set { _checkBoxList = value; }
        }
        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }

        private string _UploadEstimate;

        public string UploadEstimate
        {
            get { return _UploadEstimate; }
            set { _UploadEstimate = value; }
        }
        private string _Repairer;

        public string Repairer
        {
            get { return _Repairer; }
            set { _Repairer = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }

        private string _ConDetails;

        public string ConDetails
        {
            get { return _ConDetails; }
            set { _ConDetails = value; }
        }

        private string _RAddress;

        public string RAddress
        {
            get { return _RAddress; }
            set { _RAddress = value; }
        }

        private string _TotalCost;

        public string TotalCost
        {
            get { return _TotalCost; }
            set { _TotalCost = value; }
        }


        private string _RCRemarks;

        public string RCRemarks
        {
            get { return _RCRemarks; }
            set { _RCRemarks = value; }
        }
        private string _RCDRemarks;

        public string RCDRemarks
        {
            get { return _RCDRemarks; }
            set { _RCDRemarks = value; }
        }
        private string _ChRemarks;

        public string ChRemarks
        {
            get { return _ChRemarks; }
            set { _ChRemarks = value; }
        }
        private string _EngineRemarks;

        public string EngineRemarks
        {
            get { return _EngineRemarks; }
            set { _EngineRemarks = value; }
        }
        private string _MakeRemarks;

        public string MakeRemarks
        {
            get { return _MakeRemarks; }
            set { _MakeRemarks = value; }
        }
        private string _ModelRemarks;

        public string ModelRemarks
        {
            get { return _ModelRemarks; }
            set { _ModelRemarks = value; }
        }
        private string _ClassRemarks;

        public string ClassRemarks
        {
            get { return _ClassRemarks; }
            set { _ClassRemarks = value; }
        }
        private string _BodyTypeRemarks;

        public string BodyTypeRemarks
        {
            get { return _BodyTypeRemarks; }
            set { _BodyTypeRemarks = value; }
        }
        private string _SeatingRemarks;

        public string SeatingRemarks
        {
            get { return _SeatingRemarks; }
            set { _SeatingRemarks = value; }
        }
        private string _FuelRemarks;

        public string FuelRemarks
        {
            get { return _FuelRemarks; }
            set { _FuelRemarks = value; }
        }
        private string _ColorRemarks;

        public string ColorRemarks
        {
            get { return _ColorRemarks; }
            set { _ColorRemarks = value; }
        }
        private string _FitnessRemarks;

        public string FitnessRemarks
        {
            get { return _FitnessRemarks; }
            set { _FitnessRemarks = value; }
        }
        private string _PermitRemarks;

        public string PermitRemarks
        {
            get { return _PermitRemarks; }
            set { _PermitRemarks = value; }
        }
        private string _PermitUpRemarks;

        public string PermitUpRemarks
        {
            get { return _PermitUpRemarks; }
            set { _PermitUpRemarks = value; }
        }
        private string _OperationRemarks;

        public string OperationRemarks
        {
            get { return _OperationRemarks; }
            set { _OperationRemarks = value; }
        }
        private string _TaxOTTRemarks;

        public string TaxOTTRemarks
        {
            get { return _TaxOTTRemarks; }
            set { _TaxOTTRemarks = value; }
        }
        private string _LadenRemarks;

        public string LadenRemarks
        {
            get { return _LadenRemarks; }
            set { _LadenRemarks = value; }
        }
        private string _UnLadenRemarks;

        public string UnLadenRemarks
        {
            get { return _UnLadenRemarks; }
            set { _UnLadenRemarks = value; }
        }
        private string _ATFRemarks;

        public string ATFRemarks
        {
            get { return _ATFRemarks; }
            set { _ATFRemarks = value; }
        }
        private string _PARemarks;

        public string PARemarks
        {
            get { return _PARemarks; }
            set { _PARemarks = value; }
        }
        private string _AntiTheftRemarks;

        public string AntiTheftRemarks
        {
            get { return _AntiTheftRemarks; }
            set { _AntiTheftRemarks = value; }
        }
        private string _PreRemarks;

        public string PreRemarks
        {
            get { return _PreRemarks; }
            set { _PreRemarks = value; }
        }
    }

    //public class CSMotorAssessment
    //{
    //    private string _Task;

    //    public string Task
    //    {
    //        get { return _Task; }
    //        set { _Task = value; }
    //    }
    //    private int _PkMotorAssessment;

    //    public int PkMotorAssessment
    //    {
    //        get { return _PkMotorAssessment; }
    //        set { _PkMotorAssessment = value; }
    //    }
    //    private int _FkClaimId;

    //    public int FkClaimId
    //    {
    //        get { return _FkClaimId; }
    //        set { _FkClaimId = value; }
    //    }
    //    private string _CalMode;

    //    public string CalMode
    //    {
    //        get { return _CalMode; }
    //        set { _CalMode = value; }
    //    }
    //    private string _ParticularsItem;

    //    public string ParticularsItem
    //    {
    //        get { return _ParticularsItem; }
    //        set { _ParticularsItem = value; }
    //    }
    //    private string _NatureofItem;

    //    public string NatureofItem
    //    {
    //        get { return _NatureofItem; }
    //        set { _NatureofItem = value; }
    //    }
    //    private string _RateofDep;

    //    public string RateofDep
    //    {
    //        get { return _RateofDep; }
    //        set { _RateofDep = value; }
    //    }
    //    private string _Quantity;

    //    public string Quantity
    //    {
    //        get { return _Quantity; }
    //        set { _Quantity = value; }
    //    }
    //    private string _Rate;

    //    public string Rate
    //    {
    //        get { return _Rate; }
    //        set { _Rate = value; }
    //    }
    //    private string _Ammount;

    //    public string Ammount
    //    {
    //        get { return _Ammount; }
    //        set { _Ammount = value; }
    //    }
    //    private string _CalDescription;

    //    public string CalDescription
    //    {
    //        get { return _CalDescription; }
    //        set { _CalDescription = value; }
    //    }
    //    private string _EntryBy;

    //    public string EntryBy
    //    {
    //        get { return _EntryBy; }
    //        set { _EntryBy = value; }
    //    }
    //    private string _IPAddress;

    //    public string IPAddress
    //    {
    //        get { return _IPAddress; }
    //        set { _IPAddress = value; }
    //    }
    //    private string _xml;

    //    public string Xml
    //    {
    //        get { return _xml; }
    //        set { _xml = value; }
    //    }
    //    private string _TotalAmount;

    //    public string TotalAmount
    //    {
    //        get { return _TotalAmount; }
    //        set { _TotalAmount = value; }
    //    }
    //    private string _GrandTota;

    //    public string GrandTota
    //    {
    //        get { return _GrandTota; }
    //        set { _GrandTota = value; }
    //    }

    //}

    //public class CSPreviousClaim
    //{
    //    private int _PkId;

    //    public int PkId
    //    {
    //        get { return _PkId; }
    //        set { _PkId = value; }
    //    }
    //    private int _FkClaimId;

    //    public int FkClaimId
    //    {
    //        get { return _FkClaimId; }
    //        set { _FkClaimId = value; }
    //    }
    //    private string _PolicyNo;

    //    public string PolicyNo
    //    {
    //        get { return _PolicyNo; }
    //        set { _PolicyNo = value; }
    //    }
    //    private string _Insurer;

    //    public string Insurer
    //    {
    //        get { return _Insurer; }
    //        set { _Insurer = value; }
    //    }
    //    private string _NoOfAcc;

    //    public string NoOfAcc
    //    {
    //        get { return _NoOfAcc; }
    //        set { _NoOfAcc = value; }
    //    }
    //    private string _ValidUpTO;

    //    public string ValidUpTO
    //    {
    //        get { return _ValidUpTO; }
    //        set { _ValidUpTO = value; }
    //    }
    //    private string _DateOfAcc;

    //    public string DateOfAcc
    //    {
    //        get { return _DateOfAcc; }
    //        set { _DateOfAcc = value; }
    //    }
    //    private string _ClaimedAmt;

    //    public string ClaimedAmt
    //    {
    //        get { return _ClaimedAmt; }
    //        set { _ClaimedAmt = value; }
    //    }
    //    private string _CompensationAmt;

    //    public string CompensationAmt
    //    {
    //        get { return _CompensationAmt; }
    //        set { _CompensationAmt = value; }
    //    }
    //    private string _Remark;

    //    public string Remark
    //    {
    //        get { return _Remark; }
    //        set { _Remark = value; }
    //    }
    //    private string _EntryBy;

    //    public string EntryBy
    //    {
    //        get { return _EntryBy; }
    //        set { _EntryBy = value; }
    //    }
    //    private string _IPAddress;

    //    public string IPAddress
    //    {
    //        get { return _IPAddress; }
    //        set { _IPAddress = value; }
    //    }
    //}

    //public class CSMotorOtherLegalAspects
    //{
    //    private int _PkId;

    //    public int PkId
    //    {
    //        get { return _PkId; }
    //        set { _PkId = value; }
    //    }
    //    private int _FkClaimId;

    //    public int FkClaimId
    //    {
    //        get { return _FkClaimId; }
    //        set { _FkClaimId = value; }
    //    }
    //    private string _FIRNo;

    //    public string FIRNo
    //    {
    //        get { return _FIRNo; }
    //        set { _FIRNo = value; }
    //    }
    //    private string _FIRDate;

    //    public string FIRDate
    //    {
    //        get { return _FIRDate; }
    //        set { _FIRDate = value; }
    //    }
    //    private string _PoliceStation;

    //    public string PoliceStation
    //    {
    //        get { return _PoliceStation; }
    //        set { _PoliceStation = value; }
    //    }
    //    private string _IPC;

    //    public string IPC
    //    {
    //        get { return _IPC; }
    //        set { _IPC = value; }
    //    }
    //    private string _SubjectMatter;

    //    public string SubjectMatter
    //    {
    //        get { return _SubjectMatter; }
    //        set { _SubjectMatter = value; }
    //    }
    //    private string _FReport;

    //    public string FReport
    //    {
    //        get { return _FReport; }
    //        set { _FReport = value; }
    //    }
    //    private string _Remark;

    //    public string Remark
    //    {
    //        get { return _Remark; }
    //        set { _Remark = value; }
    //    }
    //    private string _EntryBy;

    //    public string EntryBy
    //    {
    //        get { return _EntryBy; }
    //        set { _EntryBy = value; }
    //    }
    //    private string _IPAddress;

    //    public string IPAddress
    //    {
    //        get { return _IPAddress; }
    //        set { _IPAddress = value; }
    //    }




    //    /**/
    //}

    public class CSPolicyParticulars
    {
        private int _PK_ID;
        public int PK_ID
        {
            get { return _PK_ID; }
            set { _PK_ID = value; }
        }
        private string _FK_ClaimID;
        public string FK_ClaimID
        {
            get { return _FK_ClaimID; }
            set { _FK_ClaimID = value; }
        }
        private string _Period;
        public string Period
        {
            get { return _Period; }
            set { _Period = value; }
        }
        public string PeriodFromDT { get; set; }
        public string PeriodToDT { get; set; }
        public string VehicleRegistrationNo { get; set; }
        public decimal SumInsured { get; set; }
        public string InsuredName { get; set; }
        public string InsuredAddress { get; set; }
        private string _RegNo;
        public string RegNo
        {
            get { return _RegNo; }
            set { _RegNo = value; }
        }
        private string _HPAgreement;
        public string HPAgreement
        {
            get { return _HPAgreement; }
            set { _HPAgreement = value; }
        }
        private string _Excess;
        public string Excess
        {
            get { return _Excess; }
            set { _Excess = value; }
        }
        private string _IPAddress;
        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private string _InstructorName;
        public string InstructorName
        {
            get { return _InstructorName; }
            set { _InstructorName = value; }
        }
        private string _InsrAddress;
        public string InsrAddress
        {
            get { return _InsrAddress; }
            set { _InsrAddress = value; }
        }
        private string _InsrClaimNo;
        public string InsrClaimNo
        {
            get { return _InsrClaimNo; }
            set { _InsrClaimNo = value; }
        }
        private string _InsdType;
        public string InsdType
        {
            get { return _InsdType; }
            set { _InsdType = value; }
        }
        private string _PolicyType;
        public string PolicyType
        {
            get { return _PolicyType; }
            set { _PolicyType = value; }
        }
        private string _PolicyNo;
        public string PolicyNo
        {
            get { return _PolicyNo; }
            set { _PolicyNo = value; }
        }

    }

    public class CSMotorRouteParticulars
    {
        private int _PK_ID;
        public int PK_ID
        {
            get { return _PK_ID; }
            set { _PK_ID = value; }
        }
        private string _FK_ClaimID;
        public string FK_ClaimID
        {
            get { return _FK_ClaimID; }
            set { _FK_ClaimID = value; }
        }
        private string _OrignOfJourney;
        public string OrignOfJourney
        {
            get { return _OrignOfJourney; }
            set { _OrignOfJourney = value; }
        }
        private string _EndOfJourney;
        public string EndOfJourney
        {
            get { return _EndOfJourney; }
            set { _EndOfJourney = value; }
        }
        private string _TransportCompany;
        public string TransportCompany
        {
            get { return _TransportCompany; }
            set { _TransportCompany = value; }
        }
        private string _ConsNoteNo;
        public string ConsNoteNo
        {
            get { return _ConsNoteNo; }
            set { _ConsNoteNo = value; }
        }
        private string _DateOfConsNoteNo;
        public string DateOfConsNoteNo
        {
            get { return _DateOfConsNoteNo; }
            set { _DateOfConsNoteNo = value; }
        }
        private string _TransportItem;
        public string TransportItem
        {
            get { return _TransportItem; }
            set { _TransportItem = value; }
        }
        private string _WeightCons;
        public string WeightCons
        {
            get { return _WeightCons; }
            set { _WeightCons = value; }
        }

        private string _EntryBy;
        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }

    }

    public class CSMotorPartClaim
    {
        private int _PK_ID;
        public int PK_ID
        {
            get { return _PK_ID; }
            set { _PK_ID = value; }
        }
        private string _FK_ClaimID;
        public string FK_ClaimID
        {
            get { return _FK_ClaimID; }
            set { _FK_ClaimID = value; }
        }
        private string _Class;
        public string Class
        {
            get { return _Class; }
            set { _Class = value; }
        }

        private string _PartName;
        public string PartName
        {
            get { return _PartName; }
            set { _PartName = value; }
        }
        private int _PartID;
        public int PartID
        {
            get { return _PartID; }
            set { _PartID = value; }
        }


        private string _Affect;
        public string Affect
        {
            get { return _Affect; }
            set { _Affect = value; }
        }
        private int _AffectID;
        public int AffectID
        {
            get { return _AffectID; }
            set { _AffectID = value; }
        }
    }
    public class CSMotorParticularRemarks
    {
        private string _FK_ClaimID;
        public string FK_ClaimID
        {
            get { return _FK_ClaimID; }
            set { _FK_ClaimID = value; }
        }
        private string _remark;
        public string Remark
        {
            get { return _remark; }
            set { _remark = value; }
        }
        private string _value;
        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
        private string _htmlTable;
        public string HtmlTable
        {
            get { return _htmlTable; }
            set { _htmlTable = value; }
        }
        private int _PK_ID;
        public int PK_ID
        {
            get { return _PK_ID; }
            set { _PK_ID = value; }
        }

    }

    public class SurveryOtherRemark
    {
        public List<SurveryRemarkValue> objlstrem;
        public string Oparticular { get; set; }
        public string Pparticular { get; set; }
        public string Rparticular { get; set; }
        public string Vparticular { get; set; }
        public string Dparticular { get; set; }
    }
    public class SurveryRemarkValue
    {
        public string RemID { get; set; }
        public string RValue { get; set; }
    }

}