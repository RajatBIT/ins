﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSConcerned
    {
        public int PK_Conc_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string PolicyNo { get; set; }
        public int Broker_ID { get; set; }
        public int B_R_ID { get; set; }
        public int B_StateCode { get; set; }
        public int B_Location_ID { get; set; }
        public string B_RegUserEmail { get; set; }
        public string B_ToEmailId { get; set; }
        public string B_CCEmailId { get; set; }
        public int I_R_ID { get; set; }
        public int I_StateCode { get; set; }
        public int I_Location_ID { get; set; }
        public string I_RegUserEmail { get; set; }
        public string I_ToEmailId { get; set; }
        public string I_CCEmailId { get; set; }
        public string Is_ToEmailId { get; set; }
        public string Is_CCEmailId { get; set; }

    }
}
