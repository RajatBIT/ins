﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMISCModeofCommunication
    {
        public int PK_MC_ID { get; set; }
        public String ModeofCommunication  { get; set; }
    }
}
