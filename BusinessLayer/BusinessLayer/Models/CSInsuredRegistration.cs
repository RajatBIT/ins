﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSInsuredRegistration
    {
        private int _PkInsdId;

        public int PkInsdId
        {
            get { return _PkInsdId; }
            set { _PkInsdId = value; }
        }
        private int _FkState;

        public int FkState
        {
            get { return _FkState; }
            set { _FkState = value; }
        }
        private int _FkDistrict;

        public int FkDistrict
        {
            get { return _FkDistrict; }
            set { _FkDistrict = value; }
        }
        private int _FKInsrId;

        public int FKInsrId
        {
            get { return _FKInsrId; }
            set { _FKInsrId = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _AltMobile;

        public string AltMobile
        {
            get { return _AltMobile; }
            set { _AltMobile = value; }
        }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _PolicyNo;

        public string PolicyNo
        {
            get { return _PolicyNo; }
            set { _PolicyNo = value; }
        }
        private int _Pincode;

        public int Pincode
        {
            get { return _Pincode; }
            set { _Pincode = value; }
        }
        private string _Experience;

        public string Experience
        {
            get { return _Experience; }
            set { _Experience = value; }
        }
        private string _ApproveStatus;

        public string ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }
        private int _PageSize;

        public int PageSize
        {
            get { return _PageSize; }
            set { _PageSize = value; }
        }
        private int _PageIndex;

        public int PageIndex
        {
            get { return _PageIndex; }
            set { _PageIndex = value; }
        }
        private int _RecordCount;

        public int RecordCount
        {
            get { return _RecordCount; }
            set { _RecordCount = value; }
        }

        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public string PANNO { get; set; }
        public string GSTIN { get; set; }
        public string InsuredType { get; set; }
        public string AltEmail { get; set; }
        public string Country { get; set; }
        public string AboutInsured { get; set; }
    }
}
