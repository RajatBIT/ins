﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMotorAssParticular
    {
        public int PK_P_ID { get; set; }
        public string Particular { get; set; }
    }
}
