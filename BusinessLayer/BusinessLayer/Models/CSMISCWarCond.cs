﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSMISCWarCond
    {
        public int PK_WC_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int RWC_ID { get; set; }
        public string StatusImplement { get; set; }
        public string RelWarCond { get; set; }
    }
}
