﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public class CSMISCAboutCauseOfLoss
    {
        public int PK_MISCACL { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string Time { get; set; }
        public string Day { get; set; }
        public String Date { get; set; }
        //public string LocationLoss { get; set; }
        //public string NatureLoss { get; set; }
        public  int NL_ID { get; set; }
        public string RootCauseLoss { get; set; }
        public string OriginOperatedPeril { get; set; }
        public int EffectOperatedPeril_ID { get; set; }
        public string StatutoryAuthorities_ID { get; set; }
        public string InsuredSubmission { get; set; }
        public string PublishedPrintMedia { get; set; }
        public string CapturedCCTVCamera { get; set; }
        public string ItemWisePhotoAlbum { get; set; }
        public string TravellingFire { get; set; }
        public string FireContinuedHowManyHours { get; set; }
        public string ImmediateActionControlFire { get; set; }
        public string FurtherActionControlFire { get; set; }
        //public string WasFireBrigadeInformed { get; set; }
        public string WhoInformedFireBrigade { get; set; }
        public string TimeCallingFileBrigade { get; set; }
        public decimal DistanceSiteAndFireStation { get; set; }
        public string NameFireStation { get; set; }
        public string TimeFirstFireTenderArriving { get; set; }
        public decimal HowManyFireTendersUsed { get; set; }
        public string TimeWhenLastFireTender { get; set; }
        public string MeasureWaterLevelOutside { get; set; }
        public string MeasureWaterLevelInside { get; set; }
        public string WasInsuredPropSubmergre { get; set; }
        public string RecordRainfall { get; set; }
        public string HoursInsuredPremises { get; set; }
        public string LongInsuredAreaAffected { get; set; }
        public string WindSpeedRecordedMetDept { get; set; }
        public string EntryExitUsedCulprits { get; set; }
        public string ForcibleEntryAvailable { get; set; }
        public string TypeMarksObserved { get; set; }
        public string SizeEntryExitCulprits { get; set; }

    }
}
