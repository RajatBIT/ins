﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineAssessmentRevisionRsn
    {
        public int PK_AR_ID { get; set; }
        public int AH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string RevisionRsn { get; set; }
        
    }
}
