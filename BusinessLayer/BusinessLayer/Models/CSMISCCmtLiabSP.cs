﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCCmtLiabSP
    {
        public int CmtLiabSPID { get; set; }
        public string CmtLiabSPData { get; set; }
    }
}
