﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSMarineMOMData
    {
        public int PK_MD_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public int SV_ID { get; set; }
        public string Date { get; set; }
        public string Agenda { get; set; }
        public string Venue { get; set; }

    }
}
