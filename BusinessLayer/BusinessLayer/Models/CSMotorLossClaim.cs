﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMotorLossClaim
    {
        public int LM_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string Loss { get; set; }
        public Boolean IsChecked { get; set; }
        
    }
}
