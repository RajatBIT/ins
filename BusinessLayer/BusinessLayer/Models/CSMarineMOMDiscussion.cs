﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineMOMDiscussion
    {
        public int PK_Ds_ID { get; set; }
        public int MD_ID { get; set; }
        public string Discussion { get; set; }

    }
}
