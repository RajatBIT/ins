﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCNatureDmgls
    {
        public int NatureOfDmgLsID { get; set; }
        public string NatureOfDmgLs { get; set; }
    }
}
