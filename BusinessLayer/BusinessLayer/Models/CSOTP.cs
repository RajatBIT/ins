﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSOTP
    {
        private string _Mode;

        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _OTP;

        public string OTP
        {
            get { return _OTP; }
            set { _OTP = value; }
        }
        private string _flag;

        public string Flag
        {
            get { return _flag; }
            set { _flag = value; }
        }
    }
}
