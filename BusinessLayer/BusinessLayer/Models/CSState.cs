﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSState
    {
        public int StateCode { get; set; }
        public string StateName { get; set; }
    }
}
