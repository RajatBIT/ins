﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public class CSMotorParticularOfSurvey
    {
        public int PK_PS_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string ApplicationOfSurveyDT { get; set; }
        public string SurveyDT { get; set; }
        public String PlaceOfSurvey { get; set; }
        public string SurveyTime { get; set; }

    }
}
