﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMarineClaimedCalculation
    {
        public int PK_CC_ID { get; set; }
        public int ClaimID { get; set; }
        public int FK_CalID { get; set; }
        public string CalDescription { get; set; }
        public string CalMode { get; set; }
        public string CalType { get; set; }
        public decimal Value { get; set; }
        public int UserID { get; set; }
        public string IPAddress { get; set; }
        public decimal Rate { get; set; }
        public int AH_ID { get; set; }
        public decimal SubTotal { get; set; }

    }
}
