﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMarineMOMAttendees
    {
        public int PK_At_ID { get; set; }
        public int MD_ID { get; set; }
        public string MR_OR_MRS { get; set; }
        public string NameOfAttendee { get; set; }
        public string Designation { get; set; }
        public string NameOfOrganization { get; set; }
        public string Representing { get; set; }
        public string MobileNo { get; set; }
        public string EmailId { get; set; }

    }
}
