﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCFSRSurveyParticulars
    {
        public int PK_MISCFSRSURPRTID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string FirstSurveyVisitInitDate { get; set; }
        public string FirstSurveyVisitInitTime { get; set; }
        public string FirstSurveyVisitCompDate { get; set; }
        public string FirstSurveyVisitCompTime { get; set; }
        public string ReasonDelaySurveyVisit { get; set; }
        public DateTime InstructionDT { get; set; }
        public string SourceInsInstructorSurvey { get; set; }
    }
}
