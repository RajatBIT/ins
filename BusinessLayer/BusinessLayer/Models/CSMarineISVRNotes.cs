﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineISVRNotes
    {
        public int PK_N_ID { get; set; }
        public int PK_FsrN_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string Notes { get; set; }
    }
}
