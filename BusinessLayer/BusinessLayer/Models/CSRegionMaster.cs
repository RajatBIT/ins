﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSRegionMaster
    {
        public int PK_R_ID { get; set; }
        public string Region { get; set; }
    }
}
