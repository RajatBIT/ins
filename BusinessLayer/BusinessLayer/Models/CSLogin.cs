﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSLogin
    {
        private int _id;

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        private string _loginId;

        public string LoginId
        {
            get { return _loginId; }
            set { _loginId = value; }
        }
        private string _password;

        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        private int _fkRoleId;

        public int FkRoleId
        {
            get { return _fkRoleId; }
            set { _fkRoleId = value; }
        }
        private int _fkUserId;

        public int FkUserId
        {
            get { return _fkUserId; }
            set { _fkUserId = value; }
        }
        private bool _IsPassChanged;

        public bool IsPassChanged
        {
            get { return _IsPassChanged; }
            set { _IsPassChanged = value; }
        }
        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
    }
}
