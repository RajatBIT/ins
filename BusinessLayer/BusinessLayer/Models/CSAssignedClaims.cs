﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSAssignedClaims
    {
        private int _PkTaskId;

        public int PkTaskId
        {
            get { return _PkTaskId; }
            set { _PkTaskId = value; }
        }
        private int _FKClaimId;

        public int FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }
        private int _FKDepartment;

        public int FKDepartment
        {
            get { return _FKDepartment; }
            set { _FKDepartment = value; }
        }
        private int _FKCInsdId;

        public int FKCInsdId
        {
            get { return _FKCInsdId; }
            set { _FKCInsdId = value; }
        }
        private int _FKInsdId;

        public int FKInsdId
        {
            get { return _FKInsdId; }
            set { _FKInsdId = value; }
        }
        private int _SurId;

        public int SurId
        {
            get { return _SurId; }
            set { _SurId = value; }
        }

        private string _Mode;

        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }


        private DateTime? _AssignDate;

        public DateTime? AssignDate
        {
            get { return _AssignDate; }
            set { _AssignDate = value; }
        }
        private char _IsCompleted;

        public char IsCompleted
        {
            get { return _IsCompleted; }
            set { _IsCompleted = value; }
        }
        private string _xml;

        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }
    }
}
