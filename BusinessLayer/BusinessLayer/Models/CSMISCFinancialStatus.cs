﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCFinancialStatus
    {
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public int PK_MISCFINSTID { get; set; }
        public string FinancialYear { get; set; }
        public decimal SalesTurnover { get; set; }
        public decimal GrossProfitRs { get; set; }
        public decimal GrossProfitPerc { get; set; }
        public decimal NetProfitRs { get; set; }
        public decimal NetProfitPerc { get; set; }
        public decimal ClosingStock { get; set; }
        public string RowName { get; set; }

    }
}
