﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public class CSMISCDescAmount
    {
        public int PK_DL_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
        public int PSI_ID { get; set; }
        public int PK_PSI_ID { get; set; }

    }
}
