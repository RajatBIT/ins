﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineCommentForDeliveryReceipt
    {
        public int PK_CD_ID { get; set; }
        public string CommentForDeliveryReceipt { get; set; }
        public bool selected { get; set; }
    }
}
