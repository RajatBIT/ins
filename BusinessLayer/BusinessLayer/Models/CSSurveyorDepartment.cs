﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSSurveyorDepartment
    {
        public int PK_Id { get; set; }
        public int DeptId { get; set; }
        public string DeptName { get; set; }
        public Boolean IsChecked { get; set; }
        public int FK_SurId { get; set; }
        public Boolean IsActive { get; set; }
    }
}
