﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMarineReasonForNoClaim
    {
        public int PK_RC_ID { get; set; }
        public string ReasonForNoClaim { get; set; }
    }
}
