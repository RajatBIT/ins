﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMISCISVRAspectLiab
    {
        public int PK_AL_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }

        public int CmtLiabSPID { get; set; }
        public decimal EstmateLossIns { get; set; }
        public decimal LossReserveRecom { get; set; }
        public string BscLossReserveRecom { get; set; }
        public string ObsrvSiteLoss { get; set; }

        public string RemarkAdvID { get; set; }
        public string BLankText { get; set; }
        public string CmtRelPolCond { get; set; }
        public string CircumLoss { get; set; }
        public string Noteoccrsteye { get; set; }
        //public string tempNoteoccrsteye { get; set; }

    }
}
