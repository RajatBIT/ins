﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public  class CSBrokerRegisteration
    {
        public int PK_BR_ID { get; set; }
        public int Broker_ID { get; set; }
        public string PricipalOfficerName { get; set; }
        public string EmailId { get; set; }
        public string MobileNo { get; set; }
        public string AltMobileNo { get; set; }
        public string Address { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        public int BR_ID { get; set; }
        public string Branch { get; set; }
        public string AltEmailId { get; set; }
        public int PK_BB_ID { get; set; }
        public string BrokerName { get; set; }
        public string ApprovedStatus { get; set; }
    }
}
