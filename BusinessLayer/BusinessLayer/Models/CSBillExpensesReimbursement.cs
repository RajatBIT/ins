﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSBillExpensesReimbursement
    {
        public int PK_ER_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int BH_ID  { get; set; }
        public string ExpenseDT { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

    }
}
