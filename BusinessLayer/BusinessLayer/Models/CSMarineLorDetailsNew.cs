﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMarineLorDetailsNew
    {
        public int PK_L_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int LM_ID { get; set; }
        public string Rec_NotRec_NotRel { get; set; }
        public string Name { get; set; }
        public string FileName { get; set; }
        public string LM_Name { get; set; }

    }
}
