﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace BusinessLayer.Models
{

    public class CSSurveyorsRegistration
    {
        private int _PkSurId;

        public int PkSurId
        {
            get { return _PkSurId; }
            set { _PkSurId = value; }
        }
        private string _FkMemberShip;

        public string FkMemberShip
        {
            get { return _FkMemberShip; }
            set { _FkMemberShip = value; }
        }
        private int _FkState;

        public int FkState
        {
            get { return _FkState; }
            set { _FkState = value; }
        }
        private int _FKDistricts;


        public int FKDistricts
        {
            get { return _FKDistricts; }
            set { _FKDistricts = value; }
        }

        private string _State;

        public string State
        {
            get { return _State; }
            set { _State = value; }
        }
        private string _Districs;

        public string Districs
        {
            get { return _Districs; }
            set { _Districs = value; }
        }

        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        private string _FullName;

        public string FullName
        {
            get { return _FullName; }
            set { _FullName = value; }
        }
        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _AltMobile;

        public string AltMobile
        {
            get { return _AltMobile; }
            set { _AltMobile = value; }
        }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        private string _Pincode;

        public string Pincode
        {
            get { return _Pincode; }
            set { _Pincode = value; }
        }
        private string _PanNo;

        public string PanNo
        {
            get { return _PanNo; }
            set { _PanNo = value; }
        }
        private string _StaxNo;

        public string StaxNo
        {
            get { return _StaxNo; }
            set { _StaxNo = value; }
        }
        private string _SlanNo;

        public string SlanNo
        {
            get { return _SlanNo; }
            set { _SlanNo = value; }
        }
        private string _ValidUpto;

        public string ValidUpto
        {
            get { return _ValidUpto; }
            set { _ValidUpto = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _AltEmail;

        public string AltEmail
        {
            get { return _AltEmail; }
            set { _AltEmail = value; }
        }
        private string _IslaNo;

        public string IslaNo
        {
            get { return _IslaNo; }
            set { _IslaNo = value; }
        }
        private string _IniRef;

        public string IniRef
        {
            get { return _IniRef; }
            set { _IniRef = value; }
        }
        private string _AnyOther;

        public string AnyOther
        {
            get { return _AnyOther; }
            set { _AnyOther = value; }
        }
        private string _Experience;

        public string Experience
        {
            get { return _Experience; }
            set { _Experience = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        private DateTime _UpdateDate;

        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }
        private int _ApprovedBy;

        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        private string _ApproveStatus;

        public string ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }
        private string _UserType;

        public string UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }
        private string[] chDept;

        public string[] ChDept
        {
            get { return chDept; }
            set { chDept = value; }
        }
        public int R_ID { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }

    public class CSSurveyorsDocument
    {
        private int _pkId;

        public int PkId
        {
            get { return _pkId; }
            set { _pkId = value; }
        }
        private int _fkSurId;

        public int FkSurId
        {
            get { return _fkSurId; }
            set { _fkSurId = value; }
        }
        private HttpPostedFileBase _passportPhoto;

        public HttpPostedFileBase PassportPhoto
        {
            get { return _passportPhoto; }
            set { _passportPhoto = value; }
        }

        private string _passportPhotoPath;

        public string PassportPhotoPath
        {
            get { return _passportPhotoPath; }
            set { _passportPhotoPath = value; }
        }

        private HttpPostedFileBase _digitalSign;

        public HttpPostedFileBase DigitalSign
        {
            get { return _digitalSign; }
            set { _digitalSign = value; }
        }

        private string _digitalSignPath;

        public string DigitalSignPath
        {
            get { return _digitalSignPath; }
            set { _digitalSignPath = value; }
        }

        private HttpPostedFileBase _stampSign;

        public HttpPostedFileBase StampSign
        {
            get { return _stampSign; }
            set { _stampSign = value; }
        }

        private string _stampSignPath;

        public string StampSignPath
        {
            get { return _stampSignPath; }
            set { _stampSignPath = value; }
        }

        private HttpPostedFileBase _senderSign;

        public HttpPostedFileBase SenderSign
        {
            get { return _senderSign; }
            set { _senderSign = value; }
        }

        private string _senderSignPath;

        public string SenderSignPath
        {
            get { return _senderSignPath; }
            set { _senderSignPath = value; }
        }

        private HttpPostedFileBase _letterHead;

        public HttpPostedFileBase LetterHead
        {
            get { return _letterHead; }
            set { _letterHead = value; }
        }

        private string _letterHeadPath;

        public string LetterHeadPath
        {
            get { return _letterHeadPath; }
            set { _letterHeadPath = value; }
        }

        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
}
