﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCISVRNatureExtentDamage
    {

        public int PK_NED_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string DescOfAffProp { get; set; }
        public string IdtyOfAffProp { get; set; }
        public int NatureOfDmgLsID { get; set; }
        public int CovUndMenID { get; set; }
        public string NatureOfDmgLs { get; set; }
        public string CovUndMen { get; set; }
        public decimal SumInsured { get; set; }
    }
}
