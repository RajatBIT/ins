﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCISVRIntroClaim
    {
        public int PK_MISCClaim_ID  { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string InsurersClaimNo  { get; set; }
        public string PolicyNo  { get; set; }
        public string PolicyType  { get; set; }
        public DateTime IntimationLossDT { get; set; }
        public DateTime AllotmentSurveyDT  { get; set; }
        public string SourceAllotmentSurvey { get; set; }
        public string Insurers { get; set; }
        public string Address { get; set; }
        public DateTime SurveyVisitInitiatedDT { get; set; }
        public DateTime SurveyVisitCompletedDT { get; set; }
        public string ReasonDelaySurvey { get; set; }
        public string InsuredName  { get; set; }
        public string InsuredAddress  { get; set; }
        public string PS_ALL { get; set; }
        public string PS_ALL_Place { get; set; }
        public string Loss { get; set; }
        public DateTime LossDT { get; set; }
        public string CL_OP { get; set; }
        public string CL_OP_ID { get; set; }

    }
}
