﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSTraineeSurveyorRegistration
    {
        private int _PkTsurId;

        public int PkTsurId
        {
            get { return _PkTsurId; }
            set { _PkTsurId = value; }
        }
        private string _FkClaimId;

        public string FkClaimId
        {
            get { return _FkClaimId; }
            set { _FkClaimId = value; }
        }
        private string _FKDepartment;

        public string FKDepartment
        {
            get { return _FKDepartment; }
            set { _FKDepartment = value; }
        }

        private string _UserType;

        public string UserType
        {
            get { return _UserType; }
            set { _UserType = value; }
        }
        private int _FkStateId;

        public int FkStateId
        {
            get { return _FkStateId; }
            set { _FkStateId = value; }
        }
        private int _FkDistrictsId;

        public int FkDistrictsId
        {
            get { return _FkDistrictsId; }
            set { _FkDistrictsId = value; }
        }
        private string _FirstName;

        public string FirstName
        {
            get { return _FirstName; }
            set { _FirstName = value; }
        }
        private string _LastName;

        public string LastName
        {
            get { return _LastName; }
            set { _LastName = value; }
        }
        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _EmailId;

        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        private string _Pincode;

        public string Pincode
        {
            get { return _Pincode; }
            set { _Pincode = value; }
        }
        private string _SLANo;

        public string SLANo
        {
            get { return _SLANo; }
            set { _SLANo = value; }
        }
        private string _IRDANo;

        public string IRDANo
        {
            get { return _IRDANo; }
            set { _IRDANo = value; }
        }
        private string _ISLANO;

        public string ISLANO
        {
            get { return _ISLANO; }
            set { _ISLANO = value; }
        }
        private string _Education;

        public string Education
        {
            get { return _Education; }
            set { _Education = value; }
        }
        private string _InsQualification;

        public string InsQualification
        {
            get { return _InsQualification; }
            set { _InsQualification = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        private DateTime _UpdateDate;

        public DateTime UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }
        private int _ApprovedBy;

        public int ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        private string _ApproveStatus;

        public string ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }
        private string[] chDept;

        public string[] ChDept
        {
            get { return chDept; }
            set { chDept = value; }
        }

        private int _FkSurId;

        public int FkSurId
        {
            get { return _FkSurId; }
            set { _FkSurId = value; }
        }

        private string _Mode;

        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }

        private int _FkReqId;

        public int FkReqId
        {
            get { return _FkReqId; }
            set { _FkReqId = value; }
        }

        private char _IsLinked;

        public char IsLinked
        {
            get { return _IsLinked; }
            set { _IsLinked = value; }
        }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
    }
}
