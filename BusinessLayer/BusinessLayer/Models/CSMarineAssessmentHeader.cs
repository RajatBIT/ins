﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMarineAssessmentHeader
    {
        public int PK_AH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public decimal SubTotalC { get; set; }
        public decimal SubTotalA { get; set; }
        public decimal TotalAmountC { get; set; }
        public decimal TotalAmountA { get; set; }

    }
}
