﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCFSRIntroClaim
    {
        public int PK_MISCFSRIntroID { get; set; }
        public int PK_MISCFSRIPRID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public String PreviousReports { get; set; }
        public string InsurersClaimNo { get; set; }
        public string IntimationLossDT { get; set; }
        public string AllotmentSurveyDT { get; set; }
        public string SourceAllotmentSurvey { get; set; }
        public string SurveyVisitInitiatedDT { get; set; }
        public string SurveyVisitCompletedDT { get; set; }
        public string ReasonDelaySurvey { get; set; }
        public string PolicyNo { get; set; }
        public string PolicyType { get; set; }
        public string PeriodofPolicyFromDT { get; set; }
        public string PeriodofPolicyToDT { get; set; }
        public string Insurers { get; set; }
        public string InsurersAddress { get; set;}
        public String PolicyIssueofficeAddress { get; set; }
        public string InsuredName { get; set; }
        public String InsuredAddress { get; set; }
        public string FinancierInterest { get; set; }
        public String AddressofLossLocation { get; set; }
        public String Loss { get; set; }
        public string LossDT { get; set; }
        public int CmtLiabSPID { get; set; }
        public decimal FinalClaimInsured { get; set; }
       
        public decimal ReportedLoss { get; set; }
        public string NatureOfLoss{ get; set; }

    }
}
