﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMISCNatureofLoss
    {
        public int PK_NL_ID { get; set; }
        public string NatureOfLoss { get; set; }
    }
}
