﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMotorAssessmentHeader
    {
        public int PK_AH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string ShortName { get; set; }
        public decimal SubTotalC { get; set; }
        public decimal SubTotalA { get; set; }
        public decimal SalvageRate { get; set; }
        public decimal SalvageAmount { get; set; }
        public decimal DepriciationRate { get; set; }
        public decimal DepriciationAmount { get; set; }
        public decimal AddDepriciationRate { get; set; }
        public decimal AddDepriciationAmount { get; set; }
        public decimal DiscountRate { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal GrandTotal { get; set; }

    }
}
