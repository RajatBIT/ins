﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCClaimAssAdjustSummary
    {
        public int PK_CAAS_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public decimal Value { get; set; }
        public string TableName { get; set; }
        public string ShortName { get; set; }


    }
}
