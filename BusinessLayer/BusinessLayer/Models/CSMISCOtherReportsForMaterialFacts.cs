﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCOtherReportsForMaterialFacts
    {
        public int PK_MISCORMFID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        
        public string FireBrigadeReportPhoto { get; set; }
        public string MeteorologicalReportPhoto { get; set; }
        public bool C_PR_Tran { get; set; }
        public bool C_Pr_IPC { get; set; }
        public bool C_Pr_PRC { get; set; }
        public bool C_Pr_Remarks { get; set; }
        public string PoliceReportPhoto { get; set; }
        public string Pr_Tran { get; set; }
        public string Pr_IPC_ID { get; set; }
        public string Pr_PRC { get; set; }
        public string Pr_Remarks { get; set; }

        public bool C_Fr_Tran { get; set; }
        public bool C_Fr_sumry { get; set; }
        public bool C_Fr_Remarks { get; set; }
        public string Fr_Tran { get; set; }
        public string Fr_Sumry { get; set; }
        public string Fr_Remarks { get; set; }
        public bool C_Mr_Tran { get; set; }
        public bool C_Mr_Sumry { get; set; }
        public bool C_Mr_Remarks { get; set; }
        public string Mr_Tran { get; set; }
        public string Mr_Sumry  { get; set; }
        public string Mr_Remarks { get; set; }
        public bool C_Fslr_Tran { get; set; }
        public bool C_Fslr_Sumry { get; set; }
        public bool C_Fslr_Remarks { get; set; }
        public string Fslr_Tran { get; set; }
        public string Fslr_Sumry { get; set; }
        public string Fslr_Remarks { get; set; }

    }
}
