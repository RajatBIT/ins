﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMarineRemarksForNature
    {
        public int PK_RN_ID { get; set; }
        public string RemarksForNature { get; set; }
    }
}
