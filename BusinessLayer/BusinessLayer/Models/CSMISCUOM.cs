﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCUOM
    {
        public int PK_UOM_ID { get; set; }
        public String UOM { get; set; }
    }
}
