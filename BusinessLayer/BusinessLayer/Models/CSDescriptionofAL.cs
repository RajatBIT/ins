﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSDescriptionofAL
    {
        public int PK_DescAL_ID { get; set; }
        public string Description { get; set; }
    }
}
