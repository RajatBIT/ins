﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCPolicySumInsured    {
        public int PK_PSI_ID { get; set; }
        public int PK_Tb_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public int Tb_ID { get; set; }
        public decimal SumInsured { get; set; }
        public string Description { get; set; }
        public string ShortName { get; set; }
    }
}
