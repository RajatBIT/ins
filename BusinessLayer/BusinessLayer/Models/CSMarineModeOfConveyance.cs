﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineModeOfConveyance
    {
        public int PK_MC_ID { get; set; }
        public string ModeOfConveyance { get; set; }
        public bool Selected { get; set; }
    }
}
