﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSEmailGroup
    {
        public int PK_EG_ID { get; set; }
        public int EGM_ID { get; set; }
        public string EmailId { get; set; }
        public string GroupName { get; set; }
    }
}
