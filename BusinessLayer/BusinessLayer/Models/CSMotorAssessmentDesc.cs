﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMotorAssessmentDesc
    {
        public int PK_AD_ID { get; set; }
        public int AH_ID { get; set; }
        public string Items { get; set; }
        public decimal QtyC { get; set; }
        public decimal RateC { get; set; }
        public decimal AmountC { get; set; }
        public decimal QtyA { get; set; }
        public decimal RateA { get; set; }
        public decimal AmountA { get; set; }

    }
}
