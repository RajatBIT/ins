﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSMarinePointOfDeliveryToConsignee
    {
        public int PK_PD_ID { get; set; }
        public string PointOfDeliveryToConsignee { get; set; }

    }
}
