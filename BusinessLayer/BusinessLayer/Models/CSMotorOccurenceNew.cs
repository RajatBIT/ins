﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMotorOccurenceNew
    {
        public int PK_ID { get; set; }
        public int FK_ClaimID { get; set; }
        public string AccDate { get; set; }
        public string AccPlace { get; set; }
        public string SpotSurvey { get; set; }
        public string FIR { get; set; }
        public string FireReport { get; set; }
        public string TPPLoss { get; set; }
        public string TPBInjury { get; set; }
        public string DamageSides { get; set; }
        public string Remarks { get; set; }
        public int EntryBy { get; set; }
        public string IPAddress { get; set; }
        public string NameSpotSurveyor { get; set; }
        public string SpotSurveyDT { get; set; }
        public string FireBrigadeReportNo { get; set; }
        public string FireReportDT { get; set; }
        public string NameFireStation { get; set; }
        public string FireTenderNo { get; set; }
        public string TotalTimeConsumeFireFight { get; set; }
        public string NameOfOwner { get; set; }
        public string LossDesc { get; set; }
        public string Photo1 { get; set; }
        public string Photo2 { get; set; }

    }
}
