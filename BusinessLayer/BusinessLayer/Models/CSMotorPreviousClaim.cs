﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMotorPreviousClaim
    {
        public int PK_ID { get; set; }
        public int FK_ClaimID { get; set; }
        public string PolicyNo { get; set; }
        public string Insurer { get; set; }
        public string NoOfAcc { get; set; }
        public string DateOfAcc { get; set; }
        public decimal ClaimedAmt { get; set; }
        public decimal CompensationAmt { get; set; }
        public string Remark { get; set; }
        public string EntryBy { get; set; }
        public string IPAddress { get; set; }

    }
}
