﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCAssessmentDescription
    {
        public int PK_MISCFSRNEDID { get; set; }
        public int PK_AD_ID { get; set; }
        public string DescAffectedItem { get; set; }
        public decimal Qty { get; set; }
        public string UOM { get; set; }
        public int AH_ID { get; set; }
        public int ClaimID { get; set; }
        public int NED_ID { get; set; }
        public decimal ICRate { get; set; }
        public decimal ICAmount { get; set; }
        public decimal FCQty { get; set; }
        public decimal FCRate { get; set; }
        public decimal FCAmount { get; set; }
        public decimal ALQty { get; set; }
        public decimal ALRate { get; set; }
        public decimal ALAmount { get; set; }
        public decimal DeductAABNA { get; set; }
        public decimal AdmissibleAmount { get; set; }
        public string Remarks { get; set; }

    }
}
