﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public class CSEmailGroupMst
    {
        public int PK_EGM_ID { get; set; }
        public string GroupName { get; set; }
    }
}
