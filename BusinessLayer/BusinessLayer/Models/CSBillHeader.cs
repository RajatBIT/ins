﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSBillHeader
    {
        public int PK_BH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string ClaimNo { get; set; }
        public string InvoiceNo { get; set; }
        public decimal InvoiceAmount { get; set; }
        public string CrtDT { get; set; }

    }
}
