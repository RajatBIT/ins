﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCFSRObservationsCircumstances
    {
        public int PK_OBSCIRID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string CLQuoteText { get; set; }
        public string CLQuotePhotoDoc { get; set; }
        public string UnQuoteText { get; set; }
        public Boolean ChkboxCLTrans { get; set; }
        public string CLTrans { get; set; }
        public Boolean ChkboxNewsPaper { get; set; }
        public string NameNewsPaper { get; set; }
        public string NewsPubDT { get; set; }
        public string NewsPhotoDoc { get; set; }
        public Boolean ChkbxNPTrans { get; set; }
        public string NewsTrans { get; set; }

    }
}
