﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSMotorReInspection
    {
        public int PK_RI_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string SurveyVisitDT { get; set; }
        public string PlaceOfSurveyVisit { get; set; }
        public string PurposeOfVisit { get; set; }
        public string ContactDetail { get; set; }
        public string MobileNo { get; set; }
        public string Email { get; set; }
        public string ProofOfPayment { get; set; }
        public string SalvageDestruction { get; set; }
        public string SurveyorRemarks { get; set; }

    }
}
