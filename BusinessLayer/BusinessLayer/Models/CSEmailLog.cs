﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSEmailLog
    {
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string ToEmailIds { get; set; }
        public string CCEmailIds { get; set; }
        public string Subject { get; set; }
        public string MailMsg { get; set; }
        public string Task { get; set; }
        public int FrequencyOfReminder { get; set; }
        public string InsdType { get; set; }
        public string InsdID { get; set; }
        public String ShortName { get; set; }
    }
}
