﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineIncotermOfTheSale
    {
        public int PK_IS_ID { get; set; }
        public string IncotermSale { get; set; }
    }
}
