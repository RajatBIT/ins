﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMISCCoInsurers
    {
        public int PK_CoIns_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public int CoInsurersID { get; set; }
        public int Percentage { get; set; }
 
    }
}
