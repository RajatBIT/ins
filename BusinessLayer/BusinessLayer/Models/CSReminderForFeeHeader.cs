﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSReminderForFeeHeader
    {
        public int PK_RFH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int Insurer_ID { get; set; }
        public string OfficeCode { get; set; }
        public string Address { get; set; }
        public string CrtDT { get; set; }
        public string InsurerName { get; set; }
    }
}
