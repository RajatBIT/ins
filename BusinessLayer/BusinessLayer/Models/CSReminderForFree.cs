﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{

public class CSReminderForFree
    {
        public string RefNo { get; set; }
        public string InstructionDate { get; set; }
        public string InsurerClaimNo { get; set; }
        public string InsurerName { get; set; }
        public string PolicyNo { get; set; }
        public string DepartmentName { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDT { get; set; }
        public decimal? InvoiceAmount { get; set; }
        public int AgeingOfBills { get; set; }
        public int PK_BH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int BH_ID { get; set; }
        public int RFH_ID { get; set; }
        public int Reminder_Green { get; set; }
        public int Reminder_Yellow { get; set; }
        public int Reminder_Red { get; set; }

    }
}
