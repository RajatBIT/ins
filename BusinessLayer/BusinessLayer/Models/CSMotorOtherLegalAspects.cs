﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSMotorOtherLegalAspects
    {
        public int PK_OL_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string FIRNo { get; set; }
        public string FIRDT { get; set; }
        public string PoliceStation { get; set; }
        public string SectionsOfIPC { get; set; }
        public string SubjectMatterFIR { get; set; }
        public string FinalReport_Untraceable { get; set; }
        public string Remarks { get; set; }

    }
}
