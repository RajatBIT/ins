﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public  class CSBillVisitDetail
    {
        public int PK_ID { get; set; }
        public int PK_VD_ID { get; set; }
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int BH_ID  { get; set; }
        public int SV_ID { get; set; }
        public decimal KMRun { get; set; }
        public int MOC_ID { get; set; }
        public decimal RatePerKM { get; set; }
        public decimal DA { get; set; }
        public int NoOfPhotos { get; set; }
        public string NoOfHours { get; set; }
        public decimal ProfFee { get; set; }
        public decimal RowWiseAmont { get; set; }
    }
}
