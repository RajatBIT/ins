﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMetologyOfItems
    {
        public int PK_MI_ID { get; set; }
        public string MetologyOfItems { get; set; }
        public string ShortName { get; set; }
        public decimal Percentage { get; set; }

    }

}
