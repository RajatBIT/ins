﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMarineLorChattingNew
    {
        public int PK_LC_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string UserType { get; set; }
        public string ChattingText { get; set; }
        public string DocumentName { get; set; }
        public Boolean IsMyData { get; set; }
        public DateTime CrtDT { get; set; }


    }
}
