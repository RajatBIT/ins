﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMISCInsuredClaim
    {
        public int PK_IC_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string RevisionofInsClaim { get; set; }
        public decimal TotalAmount { get; set; }

    }
}
