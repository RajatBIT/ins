﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMasterIPCSection
    {
        public int PK_IPC_ID { get; set; }
        public string NameIPCSection { get; set; }
    }
}
