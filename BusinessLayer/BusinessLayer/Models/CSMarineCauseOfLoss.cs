﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
public    class CSMarineCauseOfLoss
    {
        public int PK_CL_ID { get; set; }
        public string CauseOfLoss { get; set; }
    }
}
