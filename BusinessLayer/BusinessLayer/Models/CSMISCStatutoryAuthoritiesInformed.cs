﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCStatutoryAuthoritiesInformed
    {
        public int PK_SA_ID { get; set; }
        public String StatutoryAuthoritiesInformed { get; set; }
    }
}
