﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public class CSMISCRelWarCond
    {
        public int PK_RWC_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public String RelWarCond { get; set; }
      
    }
}
