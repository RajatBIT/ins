﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCRecommendationsLossRemarks
    {
        public int PK_RL_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string Recommendations { get; set; }
        public string LossMinMeas { get; set; }
        public string Remarks { get; set; }

    }
}
