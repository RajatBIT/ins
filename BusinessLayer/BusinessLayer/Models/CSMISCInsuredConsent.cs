﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMISCInsuredConsent
    {
        public int PK_IC_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string ChkbxDesc { get; set; }
        public int MC_ID { get; set; }
        public string DateofComm { get; set; }
        public string SnapShotofConsent { get; set; }

    }
}
