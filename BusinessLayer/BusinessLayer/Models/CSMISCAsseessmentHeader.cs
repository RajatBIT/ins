﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMISCAsseessmentHeader
    {
        public int PK_AH_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public int PSI_ID { get; set; }
        public decimal TotalAssLoss { get; set; }
        public decimal GrossLossAss { get; set; }
        public decimal TotalDeductAABNA { get; set; }
        public decimal GrossAdmLoss { get; set; }
        public decimal DeductofGFIP { get; set; }
        public decimal SalvageRate { get; set; }
        public decimal SalvageAmount { get; set; }
        public decimal DeprRate { get; set; }
        public decimal DeprAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalInitClaim  { get; set; }
        public decimal TotalFinalClaim { get; set; }

    }
}
