﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMarineAssessmentDetailDesc
    {
        public int PK_AD_ID { get; set; }
        public string CalMode { get; set; }
        public int ClaimID { get; set; }
        public int FK_DesID { get; set; }
        public decimal AffectedQuantityC { get; set; }
        public decimal UnitRateC { get; set; }
        public decimal AmountC { get; set; }
        public decimal AffectedQuantityA { get; set; }
        public decimal UnitRateA { get; set; }
        public decimal AmountA { get; set; }
        public string UserID { get; set; }
        public int MD_ID { get; set; }
        public int AH_ID { get; set; }
        public string Description { get; set; }
        public string InvNo { get; set; }
        public string InvDate { get; set; }
        public string InvQty { get; set; }
        public string UOM { get; set; }
        public string AffectedQuantity { get; set; }
        public string IPAddress { get; set; }

    }
}

