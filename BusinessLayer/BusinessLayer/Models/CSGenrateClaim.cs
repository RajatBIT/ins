﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BusinessLayer.Models
{
    public class CSGenrate
    {
        private CSGenrateClaim _GenrateClaim;

        public CSGenrateClaim GenrateClaim
        {
            get { return _GenrateClaim; }
            set { _GenrateClaim = value; }
        }

        private CSMarineJIR _MarineJIR;

        public CSMarineJIR MarineJIR
        {
            get { return _MarineJIR; }
            set { _MarineJIR = value; }
        }

        private CSMarineConsignmentDocs _MarineConsignmentDocs;

        public CSMarineConsignmentDocs MarineConsignmentDocs
        {
            get { return _MarineConsignmentDocs; }
            set { _MarineConsignmentDocs = value; }
        }

        private CSMarineDamageDetails _MarineDamageDetails;

        public CSMarineDamageDetails MarineDamageDetails
        {
            get { return _MarineDamageDetails; }
            set { _MarineDamageDetails = value; }
        }

        private CSMarineISVR _MarineISVR;

        public CSMarineISVR MarineISVR
        {
            get { return _MarineISVR; }
            set { _MarineISVR = value; }
        }

        private CSMarineTranshipment _MarineTranshipment;

        public CSMarineTranshipment MarineTranshipment
        {
            get { return _MarineTranshipment; }
            set { _MarineTranshipment = value; }
        }

        private CSSurveyorVisits _SurveyorVisits;

        public CSSurveyorVisits SurveyorVisits
        {
            get { return _SurveyorVisits; }
            set { _SurveyorVisits = value; }
        }

        


    }

    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSGenrateClaim
    {
        private string _fkClaimId;

        public string FkClaimId
        {
            get { return _fkClaimId; }
            set { _fkClaimId = value; }
        }

        private string _InsdType;

        public string InsdType
        {
            get { return _InsdType; }
            set { _InsdType = value; }
        }

        private string _PolicyType;

        public string PolicyType
        {
            get { return _PolicyType; }
            set { _PolicyType = value; }
        }
        public string PolicyTypeText { get; set; }
        private string _IndType;

        public string IndType
        {
            get { return _IndType; }
            set { _IndType = value; }
        }

        private string _SurType;

        public string SurveyType
        {
            get { return _SurType; }
            set { _SurType = value; }
        }


        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _PolicyNo;

        public string PolicyNo
        {
            get { return _PolicyNo; }
            set { _PolicyNo = value; }
        }
        private int _FkDepartment;

        public int FkDepartment
        {
            get { return _FkDepartment; }
            set { _FkDepartment = value; }
        }
        private int _FkSurId;

        public int FkSurId
        {
            get { return _FkSurId; }
            set { _FkSurId = value; }
        }
        private int _FkInsrId;

        public int FkInsrId
        {
            get { return _FkInsrId; }
            set { _FkInsrId = value; }
        }
        private string _InsrAddress;

        public string InsrAddress
        {
            get { return _InsrAddress; }
            set { _InsrAddress = value; }
        }
        private string _InsrClaimNo;

        public string InsrClaimNo
        {
            get { return _InsrClaimNo; }
            set { _InsrClaimNo = value; }
        }

        private string _VRegNo;

        public string VRegNo
        {
            get { return _VRegNo; }
            set { _VRegNo = value; }
        }
        private string _InstructorName;

        public string InstructorName
        {
            get { return _InstructorName; }
            set { _InstructorName = value; }
        }
        private string _InstructionDate;

        public string InstructionDate
        {
            get { return _InstructionDate; }
            set { _InstructionDate = value; }
        }
        private string _IEmail;

        public string IEmail
        {
            get { return _IEmail; }
            set { _IEmail = value; }
        }
        private string _ReportedLoss;

        public string ReportedLoss
        {
            get { return _ReportedLoss; }
            set { _ReportedLoss = value; }
        }
        private string _btnSubmit;

        public string BtnSubmit
        {
            get { return _btnSubmit; }
            set { _btnSubmit = value; }
        }

        private string _ClaimDateFrom;

        public string ClaimDateFrom
        {
            get { return _ClaimDateFrom; }
            set { _ClaimDateFrom = value; }
        }
        private string _ClaimDateTo;

        public string ClaimDateTo
        {
            get { return _ClaimDateTo; }
            set { _ClaimDateTo = value; }
        }

        private int _PkPolicyId;

        public int PkPolicyId
        {
            get { return _PkPolicyId; }
            set { _PkPolicyId = value; }
        }

        private string _Period;

        public string Period
        {
            get { return _Period; }
            set { _Period = value; }
        }
        private string _TransitInsured;

        public string TransitInsured
        {
            get { return _TransitInsured; }
            set { _TransitInsured = value; }
        }
        private string _ModeConveyance;
        public string ModeConveyanceOther { get; set; }
        public string ModeConveyance
        {
            get { return _ModeConveyance; }
            set { _ModeConveyance = value; }
        }
        private string _Coverage;

        public string Coverage
        {
            get { return _Coverage; }
            set { _Coverage = value; }
        }
        private string _SubjectMatter;

        public string SubjectMatter
        {
            get { return _SubjectMatter; }
            set { _SubjectMatter = value; }
        }
        private string _BasisValution;

        public string BasisValution
        {
            get { return _BasisValution; }
            set { _BasisValution = value; }
        }
        private string _PolicyExcess;

        public string PolicyExcess
        {
            get { return _PolicyExcess; }
            set { _PolicyExcess = value; }
        }
        public List<CSMarineModeOfConveyance> ListModeOfConveyance { get; set; }
        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }
        private string _CheckOpenTable;

        public string CheckOpenTable
        {
            get { return _CheckOpenTable; }
            set { _CheckOpenTable = value; }
        }
        private string _IpAddress;

        public string IpAddress
        {
            get { return _IpAddress; }
            set { _IpAddress = value; }
        }

        private int _FK_DesID;

        public int FK_DesID
        {
            get { return _FK_DesID; }
            set { _FK_DesID = value; }
        }
        private int _AffectedQuantityC;

        public int AffectedQuantityC
        {
            get { return _AffectedQuantityC; }
            set { _AffectedQuantityC = value; }
        }
        private float _UnitRateC;

        public float UnitRateC
        {
            get { return _UnitRateC; }
            set { _UnitRateC = value; }
        }
        private float _AmountC;

        public float AmountC
        {
            get { return _AmountC; }
            set { _AmountC = value; }
        }
        private int _AffectedQuantityA;

        public int AffectedQuantityA
        {
            get { return _AffectedQuantityA; }
            set { _AffectedQuantityA = value; }
        }
        private float _UnitRateA;

        public float UnitRateA
        {
            get { return _UnitRateA; }
            set { _UnitRateA = value; }
        }
        private float _AmountA;

        public float AmountA
        {
            get { return _AmountA; }
            set { _AmountA = value; }
        }

        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }

        private string _CalDescription;

        public string CalDescription
        {
            get { return _CalDescription; }
            set { _CalDescription = value; }
        }

        private string _XMLFile;

        public string XMLFile
        {
            get { return _XMLFile; }
            set { _XMLFile = value; }
        }

        private string _fKCalId;

        public string FKCalId
        {
            get { return _fKCalId; }
            set { _fKCalId = value; }
        }
        private string _CalMode;

        public string CalMode
        {
            get { return _CalMode; }
            set { _CalMode = value; }
        }
        private string _CalType;

        public string CalType
        {
            get { return _CalType; }
            set { _CalType = value; }
        }
        private string _Result;

        public string Result
        {
            get { return _Result; }
            set { _Result = value; }
        }
        private float _Value;

        public float Value
        {
            get { return _Value; }
            set { _Value = value; }
        }

        private float _GrandTotal;

        public float GrandTotal
        {
            get { return _GrandTotal; }
            set { _GrandTotal = value; }
        }

        private string _AssessmentType;

        public string AssessmentType
        {
            get { return _AssessmentType; }
            set { _AssessmentType = value; }
        }
        public string LocationOfSurvey { get; set; }
        public int NatureOfLossId { get; set; }



        public int MD_ID { get; set; }
        public int AD_ID { get; set; }
        public decimal Rate { get; set; }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSMarineJIR
    {
        private int _PkJIRId;

        public int PkJIRId
        {
            get { return _PkJIRId; }
            set { _PkJIRId = value; }
        }
        private string _FKClaimId;

        public string FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }
        private string _InsdType;

        public string InsdType
        {
            get { return _InsdType; }
            set { _InsdType = value; }
        }
        private int _InsdId;

        public int InsdId
        {
            get { return _InsdId; }
            set { _InsdId = value; }
        }

        private string _Insured;

        public string Insured
        {
            get { return _Insured; }
            set { _Insured = value; }
        }
        private string _InsuredAddress;

        public string InsuredAddress
        {
            get { return _InsuredAddress; }
            set { _InsuredAddress = value; }
        }
        private string _Consiment;

        public string Consiment
        {
            get { return _Consiment; }
            set { _Consiment = value; }
        }

        private string _FkConsignmentType;

        public string FkConsignmentType
        {
            get { return _FkConsignmentType; }
            set { _FkConsignmentType = value; }
        }
        private string _DateOfSurvey;

        public string DateOfSurvey
        {
            get { return _DateOfSurvey; }
            set { _DateOfSurvey = value; }
        }

        private string _NatureOfTransit;

        public string NatureOfTransit
        {
            get { return _NatureOfTransit; }
            set { _NatureOfTransit = value; }
        }
        public string CauseOfLossOther{ get; set; }
        private string _Consignor;

        public string Consignor
        {
            get { return _Consignor; }
            set { _Consignor = value; }
        }
        private string _Consignee;

        public string Consignee
        {
            get { return _Consignee; }
            set { _Consignee = value; }
        }
        private string _ArrivalDateDestination;

        public string ArrivalDateDestination
        {
            get { return _ArrivalDateDestination; }
            set { _ArrivalDateDestination = value; }
        }
        private string _DeliveryDateConsignee;

        public string DeliveryDateConsignee
        {
            get { return _DeliveryDateConsignee; }
            set { _DeliveryDateConsignee = value; }
        }
        private string _DispatchedTo;

        public string DispatchedTo
        {
            get { return _DispatchedTo; }
            set { _DispatchedTo = value; }
        }
        private string _DispatchedFrom;

        public string DispatchedFrom
        {
            get { return _DispatchedFrom; }
            set { _DispatchedFrom = value; }
        }
        private string _CarryingLorryRegNo;

        public string CarryingLorryRegNo
        {
            get { return _CarryingLorryRegNo; }
            set { _CarryingLorryRegNo = value; }
        }
        private string _ConsignmentWeight;

        public string ConsignmentWeight
        {
            get { return _ConsignmentWeight; }
            set { _ConsignmentWeight = value; }
        }
        public string ConsignmentTransshipedTo { get; set; }
        public string LoadCapacityOfCarryVehicle { get; set; }
        public string LoadOnCarryingVehicle { get; set; }
        public string WhoIsInsured { get; set; }
        public string TypeOfContract { get; set; }
        public string IncotermSale { get; set; }
        public string PointDeliveryConsignee { get; set; }
        private string _DescriptionPacking;
        public string FromTable { get; set; }
        public string InsuredClaimNo { get; set; }
        public string DescriptionPacking
        {
            get { return _DescriptionPacking; }
            set { _DescriptionPacking = value; }
        }
        private string _ExConditionPacking;
        
        public string ExConditionPacking
        {
            get { return _ExConditionPacking; }
            set { _ExConditionPacking = value; }
        }
        public List<CSMarineExConditionPacking> ListExConditionPacking { get; set; }
        private string _CauseOfLoss;

        public string CauseOfLoss
        {
            get { return _CauseOfLoss; }
            set { _CauseOfLoss = value; }
        }
        private string _ResoneForDelay;
        public string ExConditionPackingOther { get; set; }
        public string ResoneForDelay
        {
            get { return _ResoneForDelay; }
            set { _ResoneForDelay = value; }
        }

        private string _DelayInIntimation;

        public string DelayInIntimation
        {
            get { return _DelayInIntimation; }
            set { _DelayInIntimation = value; }
        }
        private string _DelayIntakingDelivery;

        public string DelayIntakingDelivery
        {
            get { return _DelayIntakingDelivery; }
            set { _DelayIntakingDelivery = value; }
        }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string[] _checkBoxList;

        public string[] CheckBoxList
        {
            get { return _checkBoxList; }
            set { _checkBoxList = value; }
        }

        private string _ConsignorAddress = "";

        public string ConsignorAddress
        {
            get { return _ConsignorAddress; }
            set { _ConsignorAddress = value; }
        }
        private string _ConsigeeAddress = "";

        public string ConsigeeAddress
        {
            get { return _ConsigeeAddress; }
            set { _ConsigeeAddress = value; }
        }
        private string _CarryingLorryGVW = "";

        public string CarryingLorryGVW
        {
            get { return _CarryingLorryGVW; }
            set { _CarryingLorryGVW = value; }
        }
        private string _CarryingLorryULW = "";

        public string CarryingLorryULW
        {
            get { return _CarryingLorryULW; }
            set { _CarryingLorryULW = value; }
        }

        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }

        private string _PlaceofSurvey;

        public string PlaceofSurvey
        {
            get { return _PlaceofSurvey; }
            set { _PlaceofSurvey = value; }
        }

        private string _PolicyNumber;

        public string PolicyNumber
        {
            get { return _PolicyNumber; }
            set { _PolicyNumber = value; }
        }

        private string _InsrClaimNo;

        public string InsrClaimNo
        {
            get { return _InsrClaimNo; }
            set { _InsrClaimNo = value; }
        }
        private string _dynamicClaimId;

        public string DynamicClaimId
        {
            get { return _dynamicClaimId; }
            set { _dynamicClaimId = value; }
        }
        private string _InvNo;

        public string InvNo
        {
            get { return _InvNo; }
            set { _InvNo = value; }
        }
        private string _InvDate;

        public string InvDate
        {
            get { return _InvDate; }
            set { _InvDate = value; }
        }
        private string _TypeNumber;

        public string TypeNumber
        {
            get { return _TypeNumber; }
            set { _TypeNumber = value; }
        }
        private string _TypeDate;

        public string TypeDate
        {
            get { return _TypeDate; }
            set { _TypeDate = value; }
        }
        private string _TypeNumberValue;

        public string TypeNumberValue
        {
            get { return _TypeNumberValue; }
            set { _TypeNumberValue = value; }
        }

        private string _LOR13;

        public string LOR13
        {
            get { return _LOR13; }
            set { _LOR13 = value; }
        }
        private string _LOR14;

        public string LOR14
        {
            get { return _LOR14; }
            set { _LOR14 = value; }
        }

        private string _ContactPerson;

        public string ContactPerson
        {
            get { return _ContactPerson; }
            set { _ContactPerson = value; }
        }
        private string _EmailId;

        public string EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }
        private string _MobileNumber;

        public string MobileNumber
        {
            get { return _MobileNumber; }
            set { _MobileNumber = value; }
        }
        private string _xml;

        public string Xml
        {
            get { return _xml; }
            set { _xml = value; }
        }
        public string PlaceOfAccident { get; set; }
        public string SpotSurvey { get; set; }
        public string PoliceReport { get; set; }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSMarineConsignmentDocs
    {
        private int _PKId;

        public int PKId
        {
            get { return _PKId; }
            set { _PKId = value; }
        }
        private int _FKClaimId;

        public int FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }

        private string _task;

        public string Task
        {
            get { return _task; }
            set { _task = value; }
        }

        private string _Description;

        public string Description
        {
            get { return _Description; }
            set { _Description = value; }
        }
        private string _InvNo;

        public string InvNo
        {
            get { return _InvNo; }
            set { _InvNo = value; }
        }
        private string _InvDate;

        public string InvDate
        {
            get { return _InvDate; }
            set { _InvDate = value; }
        }
        private string _InvQty;

        public string InvQty
        {
            get { return _InvQty; }
            set { _InvQty = value; }
        }
        private string _Uom;

        public string Uom
        {
            get { return _Uom; }
            set { _Uom = value; }
        }

        private string _GRNo;

        public string GRNo
        {
            get { return _GRNo; }
            set { _GRNo = value; }
        }
        private string _GRDate;

        public string GRDate
        {
            get { return _GRDate; }
            set { _GRDate = value; }
        }
        private string _BLNo;

        public string BLNo
        {
            get { return _BLNo; }
            set { _BLNo = value; }
        }
        private string _BLDate;

        public string BLDate
        {
            get { return _BLDate; }
            set { _BLDate = value; }
        }
        private string _BENo;

        public string BENo
        {
            get { return _BENo; }
            set { _BENo = value; }
        }
        private string _BEDate;

        public string BEDate
        {
            get { return _BEDate; }
            set { _BEDate = value; }
        }
        private string _AWBNo;

        public string AWBNo
        {
            get { return _AWBNo; }
            set { _AWBNo = value; }
        }

        private string _AWBDate;

        public string AWBDate
        {
            get { return _AWBDate; }
            set { _AWBDate = value; }
        }

        private string _RRNo;

        public string RRNo
        {
            get { return _RRNo; }
            set { _RRNo = value; }
        }

        private string _RRDate;

        public string RRDate
        {
            get { return _RRDate; }
            set { _RRDate = value; }
        }

        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }

        private string _ConsimentType;

        public string ConsimentType
        {
            get { return _ConsimentType; }
            set { _ConsimentType = value; }
        }

        private DateTime? _CreatedDate;

        public DateTime? CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private bool _IsSubmitted;

        public bool IsSubmitted
        {
            get { return _IsSubmitted; }
            set { _IsSubmitted = value; }
        }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSMarineDamageDetails
    {
        private int _PkId;

        public int PkId
        {
            get { return _PkId; }
            set { _PkId = value; }
        }

        private int _FKDesId;

        public int FKDesId
        {
            get { return _FKDesId; }
            set { _FKDesId = value; }
        }
        private int _FKClaimId;

        public int FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }
        private string _AffectedQuantity;

        public string AffectedQuantity
        {
            get { return _AffectedQuantity; }
            set { _AffectedQuantity = value; }
        }
        private string _Remark;

        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }
        public string RemarkOther { get; set; }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }

        private string _task;

        public string Task
        {
            get { return _task; }
            set { _task = value; }
        }

        private string _ConsimentType;

        public string ConsimentType
        {
            get { return _ConsimentType; }
            set { _ConsimentType = value; }
        }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSMarineISVR
    {
        private string _CallType;

        public string CallType
        {
            get { return _CallType; }
            set { _CallType = value; }
        }
        private int _PkId;

        public int PkId
        {
            get { return _PkId; }
            set { _PkId = value; }
        }

        private int _FkClaimId;

        public int FkClaimId
        {
            get { return _FkClaimId; }
            set { _FkClaimId = value; }
        }

        private string _PhtotoType;

        public string PhtotoType
        {
            get { return _PhtotoType; }
            set { _PhtotoType = value; }
        }
        private string _PhtotoNam;

        public string PhtotoNam
        {
            get { return _PhtotoNam; }
            set { _PhtotoNam = value; }
        }
        private string _PhtotoPath;

        public string PhtotoPath
        {
            get { return _PhtotoPath; }
            set { _PhtotoPath = value; }
        }

        private string _EstimationOfLoss;

        public string EstimationOfLoss
        {
            get { return _EstimationOfLoss; }
            set { _EstimationOfLoss = value; }
        }
        public List<CSMarineISVRNotes> listNt { get; set; }
        public List<CSMarineISVRNotes> listFSRNt { get; set; }
        private string _RecomendedReserve;

        public string RecomendedReserve
        {
            get { return _RecomendedReserve; }
            set { _RecomendedReserve = value; }
        }
        private string _BasisOfReserve;

        public string BasisOfReserve
        {
            get { return _BasisOfReserve; }
            set { _BasisOfReserve = value; }
        }
        public string SurveyorsFinalRemarks { get; set; }
        private string _Remarks;

        public string Remarks
        {
            get { return _Remarks; }
            set { _Remarks = value; }
        }

        private string _PhotoPath1;

        public string PhotoPath1
        {
            get { return _PhotoPath1; }
            set { _PhotoPath1 = value; }
        }
        private string _PhotoName1;

        public string PhotoName1
        {
            get { return _PhotoName1; }
            set { _PhotoName1 = value; }
        }
        private HttpPostedFileBase _UploadPhoto1;

        public HttpPostedFileBase UploadPhoto1
        {
            get { return _UploadPhoto1; }
            set { _UploadPhoto1 = value; }
        }
        private string _PhotoPath2;

        public string PhotoPath2
        {
            get { return _PhotoPath2; }
            set { _PhotoPath2 = value; }
        }
        private string _PhotoName2;

        public string PhotoName2
        {
            get { return _PhotoName2; }
            set { _PhotoName2 = value; }
        }
        private HttpPostedFileBase _UploadPhoto2;

        public HttpPostedFileBase UploadPhoto2
        {
            get { return _UploadPhoto2; }
            set { _UploadPhoto2 = value; }
        }
        private string _PhotoPath3;

        public string PhotoPath3
        {
            get { return _PhotoPath3; }
            set { _PhotoPath3 = value; }
        }
        private string _PhotoName3;

        public string PhotoName3
        {
            get { return _PhotoName3; }
            set { _PhotoName3 = value; }
        }
        private HttpPostedFileBase _UploadPhoto3;

        public HttpPostedFileBase UploadPhoto3
        {
            get { return _UploadPhoto3; }
            set { _UploadPhoto3 = value; }
        }
        private string _PhotoPath4;

        public string PhotoPath4
        {
            get { return _PhotoPath4; }
            set { _PhotoPath4 = value; }
        }
        private string _PhotoName4;

        public string PhotoName4
        {
            get { return _PhotoName4; }
            set { _PhotoName4 = value; }
        }
        private HttpPostedFileBase _UploadPhoto4;

        public HttpPostedFileBase UploadPhoto4
        {
            get { return _UploadPhoto4; }
            set { _UploadPhoto4 = value; }
        }
        private string _PhotoDocumentPath;

        public string PhotoDocumentPath
        {
            get { return _PhotoDocumentPath; }
            set { _PhotoDocumentPath = value; }
        }
        private string _PhotoDocumentName;

        public string PhotoDocumentName
        {
            get { return _PhotoDocumentName; }
            set { _PhotoDocumentName = value; }
        }
        private HttpPostedFileBase _UploadPhotoDocument;

        public HttpPostedFileBase UploadPhotoDocument
        {
            get { return _UploadPhotoDocument; }
            set { _UploadPhotoDocument = value; }
        }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }

        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private string _XML;

        public string XML
        {
            get { return _XML; }
            set { _XML = value; }
        }

        private string _task;

        public string Task
        {
            get { return _task; }
            set { _task = value; }
        }

        private string _ImgType;

        public string ImgType
        {
            get { return _ImgType; }
            set { _ImgType = value; }
        }

        private string _InsdType;

        public string InsdType
        {
            get { return _InsdType; }
            set { _InsdType = value; }
        }
        private int _InsdId;

        public int InsdId
        {
            get { return _InsdId; }
            set { _InsdId = value; }
        }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSMarineTranshipment
    {
        private int _PkId;

        public int PkId
        {
            get { return _PkId; }
            set { _PkId = value; }
        }
        private string _FKClaimId;

        public string FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }
        private string _Mode;

        public string Mode
        {
            get { return _Mode; }
            set { _Mode = value; }
        }
        private string _LoadingPlace;

        public string LoadingPlace
        {
            get { return _LoadingPlace; }
            set { _LoadingPlace = value; }
        }
        private string _UnLoadingPlace;

        public string UnLoadingPlace
        {
            get { return _UnLoadingPlace; }
            set { _UnLoadingPlace = value; }
        }
        private string _TypeNo;

        public string TypeNo
        {
            get { return _TypeNo; }
            set { _TypeNo = value; }
        }
        private string _TypeDate;

        public string TypeDate
        {
            get { return _TypeDate; }
            set { _TypeDate = value; }
        }
        private string _DateArrival;

        public string DateArrival
        {
            get { return _DateArrival; }
            set { _DateArrival = value; }
        }
        private string _DateDischarge;

        public string DateDischarge
        {
            get { return _DateDischarge; }
            set { _DateDischarge = value; }
        }
        private string _NameOfCarrier;

        public string NameOfCarrier
        {
            get { return _NameOfCarrier; }
            set { _NameOfCarrier = value; }
        }
        private string _Comment;

        public string Comment
        {
            get { return _Comment; }
            set { _Comment = value; }
        }
        public string CommentPhotoOrDoc { get; set; }
        private string _Remark;

        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private DateTime _EntryDate;

        public DateTime EntryDate
        {
            get { return _EntryDate; }
            set { _EntryDate = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }

        private string _task;

        public string Task
        {
            get { return _task; }
            set { _task = value; }
        }
        private string _singleTable;

        public string SingleTable
        {
            get { return _singleTable; }
            set { _singleTable = value; }
        }
        private string _mulipleTable;

        public string MulipleTable
        {
            get { return _mulipleTable; }
            set { _mulipleTable = value; }
        }

        private string _Period;
        public string LegOfJourney { get; set; }

        public List<CSMarineCommentForDeliveryReceipt> listComment { get; set; }
        public List <CSMarineRemarksForNature> listRN { get; set; }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSSurveyorVisits
    {
        private int _PkId;

        public int PkId
        {
            get { return _PkId; }
            set { _PkId = value; }
        }
        private string _FkClaimId;

        public string FkClaimId
        {
            get { return _FkClaimId; }
            set { _FkClaimId = value; }
        }
        private string _StartDate;

        public string StartDate
        {
            get { return _StartDate; }
            set { _StartDate = value; }
        }
        private string _EndDate;

        public string EndDate
        {
            get { return _EndDate; }
            set { _EndDate = value; }
        }
        private string _PlaceVisit;

        public string PlaceVisit
        {
            get { return _PlaceVisit; }
            set { _PlaceVisit = value; }
        }
        private string _PurposeVisit;

        public string PurposeVisit
        {
            get { return _PurposeVisit; }
            set { _PurposeVisit = value; }
        }
        private string _Remark;

        public string Remark
        {
            get { return _Remark; }
            set { _Remark = value; }
        }
        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private DateTime _CreatedDate;

        public DateTime CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }
        public string FromTableName { get; set; }
    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    public class CSFSR
    {
        private string _FKClaimId;

        public string FKClaimId
        {
            get { return _FKClaimId; }
            set { _FKClaimId = value; }
        }
        private string _Salvage;

        public string Salvage
        {
            get { return _Salvage; }
            set { _Salvage = value; }
        }
        private string _CommentPolicy;

        public string CommentPolicy
        {
            get { return _CommentPolicy; }
            set { _CommentPolicy = value; }
        }
        private string _Concurrence;

        public string Concurrence
        {
            get { return _Concurrence; }
            set { _Concurrence = value; }
        }
        private string _OtherRemark;

        public string OtherRemark
        {
            get { return _OtherRemark; }
            set { _OtherRemark = value; }
        }

        private string _EntryBy;

        public string EntryBy
        {
            get { return _EntryBy; }
            set { _EntryBy = value; }
        }

        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        public string CommentPolicy_Com_NotCom { get; set; }
        private string _Task;

        public string Task
        {
            get { return _Task; }
            set { _Task = value; }
        }
        private string _InsdType;

        public string InsdType
        {
            get { return _InsdType; }
            set { _InsdType = value; }
        }
        private int _InsdId;

        public int InsdId
        {
            get { return _InsdId; }
            set { _InsdId = value; }
        }

    }
    /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
}
