﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
 public   class CSCommonMotor
    {
        public int PK_S_ID { get; set; }
        public string Salvage { get; set; }
        public int PK_RO_ID { get; set; }
        public string RemarksObservations { get; set; }
        public int PK_PL_ID { get; set; }
        public string ParticularsOfLossDamage { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
    }
}
