﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMISCFSRInsuranceParticulars
    {
        public int PK_MISCFSRIPRID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string AddressCommunication { get; set; }
        public string FinancierInterest { get; set; }
        public bool AddCovers { get; set; }
        public bool ClausesAttached { get; set; }
        public string ExcessApplicable { get; set; }
        public String CINYN { get; set; }
        public string PSIYN { get; set; }
        

    }
}
