﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSMISCClaimAssAdjust
    {
        public int PK_CAA_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string LossMinmization { get; set; }
        public string DebrisRemoval { get; set; }
        public string SurveyorArcCon { get; set; }
        public string Excess { get; set; }
        public string OnAccountPayment { get; set; }

    }
}
