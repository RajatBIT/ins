﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMarineLorHeaderNew
    {
        public int PK_LH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string NameContactPerson { get; set; }
        public string MobileNoContactPerson { get; set; }
        public string EmailIdContactPerson { get; set; }
        public string ToEmailIds { get; set; }
        public string CCEmailIds { get; set; }
        public int LH_ID { get; set; }
        public int PK_LE_ID { get; set; }
    }
}
