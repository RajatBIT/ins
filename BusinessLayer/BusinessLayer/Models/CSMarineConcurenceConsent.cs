﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMarineConcurenceConsent
    {

        public int PK_CC_ID { get; set; }
        public string ConcurenceConsent { get; set; }
    }
}
