﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
   public class CSBroker
    {
        public int PK_Broker_ID { get; set; }
        public string BrokerName { get; set; }
        public string IrdaRegNo { get; set; }
    }
}
