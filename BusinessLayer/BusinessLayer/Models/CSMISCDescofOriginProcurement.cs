﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public class CSMISCDescofOriginProcurement
    {
        public int PK_DOP_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string DescofItem { get; set; }
        public string SupplierName { get; set; }
        public string CountryofOrigin { get; set; }
        public string InvNo { get; set; }
        public string InvDate { get; set; }
        public decimal InvoiceValue { get; set; }
        public string UOM { get; set; }
        public decimal CapitalizedValue { get; set; }
        public int PSI_ID { get; set; }

    }
}
