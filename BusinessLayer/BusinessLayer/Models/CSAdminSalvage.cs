﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public class CSAdminSalvage
    {
        public int PK_AS_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string LocationOfAffectedMaterial { get; set; }
        public string ItemDesc { get; set; }
        public decimal AffectedQuantity { get; set; }
        public decimal InvoiceRate { get; set; }
        public string Photo1{ get; set; }
        public string Photo2 { get; set; }
        public string Photo3 { get; set; }
        public string Photo4 { get; set; }

    }
}
