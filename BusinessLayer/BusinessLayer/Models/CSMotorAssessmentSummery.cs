﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMotorAssessmentSummery
    {
        public int PK_AS_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public decimal TowingCharge { get; set; }
        public decimal Excess { get; set; }
        public decimal AfterTowingChargeTotal { get; set; }
        public decimal GrandTotalofAssSummery { get; set; }
        public decimal GrandTotalofAssHeader { get; set; }
    }
}
