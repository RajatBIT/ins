﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{

    public class CSPaymentReceive
    {
        public int PK_PR_ID { get; set; }
        public int PK_BH_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public int BH_ID { get; set; }
        public string ReceiveDT { get; set; }
        public string UTRN { get; set; }
        public decimal FeeAmount { get; set; }
        public string ReasonForDifference { get; set; }
        public string Status { get; set; }
        public string InvoiceNo { get; set; }
        public string InvoiceDT  { get; set; }
        public decimal InvoiceAmount { get; set; }


    }
}
