﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    class CSMasterAccess
    {
        private int _PKAccessID;

        public int PKAccessID
        {
            get { return _PKAccessID; }
            set { _PKAccessID = value; }
        }
        private string _AccessName;

        public string AccessName
        {
            get { return _AccessName; }
            set { _AccessName = value; }
        }
        private DateTime _CreateDate;

        public DateTime CreateDate
        {
            get { return _CreateDate; }
            set { _CreateDate = value; }
        }
        private bool _IsActive;

        public bool IsActive
        {
            get { return _IsActive; }
            set { _IsActive = value; }
        }
        private int _FKRoleID;

        public int FKRoleID
        {
            get { return _FKRoleID; }
            set { _FKRoleID = value; }
        }
        private string _Role;

        public string Role
        {
            get { return _Role; }
            set { _Role = value; }
        }

        private int _FKAccessID;

        public int FKAccessID
        {
            get { return _FKAccessID; }
            set { _FKAccessID = value; }
        }
    }
}
 