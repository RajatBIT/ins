﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public   class CSMotorOccThirdPartyDetails
    {
        public int PK_TPB_ID { get; set; }
        public int ClaimID { get; set; }
        public int UserID { get; set; }
        public string NameOfVictim { get; set; }
        public string ParticularOfInjury { get; set; }
        public string Hospitalization { get; set; }
        public int PK_TP_ID { get; set; }
        public string NameOfOwner { get; set; }
        public string ParticularOfProperty { get; set; }
        public decimal EstimatedValue { get; set; }

    }
}
