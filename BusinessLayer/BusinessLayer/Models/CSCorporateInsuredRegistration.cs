﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSCorporateInsuredRegistration
    {
        private int _PkCInsdId;

        public int PkCInsdId
        {
            get { return _PkCInsdId; }
            set { _PkCInsdId = value; }
        }
        private int _FKState;

        public int FKState
        {
            get { return _FKState; }
            set { _FKState = value; }
        }
        private int _FkDistricts;

        public int FkDistricts
        {
            get { return _FkDistricts; }
            set { _FkDistricts = value; }
        }
        private int _FkInsrID;

        public int FkInsrID
        {
            get { return _FkInsrID; }
            set { _FkInsrID = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private string _RegOffice;

        public string RegOffice
        {
            get { return _RegOffice; }
            set { _RegOffice = value; }
        }
        private string _UserName;

        public string UserName
        {
            get { return _UserName; }
            set { _UserName = value; }
        }
        private string _Email;

        public string Email
        {
            get { return _Email; }
            set { _Email = value; }
        }
        private string _Mobile;

        public string Mobile
        {
            get { return _Mobile; }
            set { _Mobile = value; }
        }
        private string _AltMobile;

        public string AltMobile
        {
            get { return _AltMobile; }
            set { _AltMobile = value; }
        }
        public int R_ID { get; set; }
        public string CinNo { get; set; }
        public string Password { get; set; }
        public string ConfirmPassword { get; set; }
        private string _Address;

        public string Address
        {
            get { return _Address; }
            set { _Address = value; }
        }
        private string _Country;

        public string Country
        {
            get { return _Country; }
            set { _Country = value; }
        }
        private string _Pincode;

        public string Pincode
        {
            get { return _Pincode; }
            set { _Pincode = value; }
        }
        private string _Experience;

        public string Experience
        {
            get { return _Experience; }
            set { _Experience = value; }
        }
        private string _IPAddress;

        public string IPAddress
        {
            get { return _IPAddress; }
            set { _IPAddress = value; }
        }
        private string _CreatedDate;

        public string CreatedDate
        {
            get { return _CreatedDate; }
            set { _CreatedDate = value; }
        }
        private string _UpdateDate;

        public string UpdateDate
        {
            get { return _UpdateDate; }
            set { _UpdateDate = value; }
        }
        private string _ApprovedBy;

        public string ApprovedBy
        {
            get { return _ApprovedBy; }
            set { _ApprovedBy = value; }
        }
        private string _ApproveStatus;

        public string ApproveStatus
        {
            get { return _ApproveStatus; }
            set { _ApproveStatus = value; }
        }
    }
}
