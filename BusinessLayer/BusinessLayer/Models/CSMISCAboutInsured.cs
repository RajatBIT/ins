﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSMISCAboutInsured
    {
        public int PK_MISCABTINSID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
        public string NoteBusinessProfile { get; set; }
        public Boolean ChkbxFS { get; set; }
        public string InsuredBalText { get; set; }
        public string BusinessGrowthText { get; set; }
        public string DescriptionsSite { get; set; }
        public Boolean ChkbxLayoutSite { get; set; }
        public string LayoutSitePhotoDoc { get; set; }

    }
}
