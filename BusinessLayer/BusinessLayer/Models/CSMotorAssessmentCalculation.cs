﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
public    class CSMotorAssessmentCalculation
    {
        public int PK_AC_ID { get; set; }
        public int AH_ID { get; set; }
        public string Sign { get; set; }
        public int P_ID { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }

    }
}
