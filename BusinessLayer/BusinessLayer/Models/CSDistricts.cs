﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
  public  class CSDistricts
    {
        public int PKDistrictsId { get; set; }
        public string DistrictName { get; set; }
    }
}
