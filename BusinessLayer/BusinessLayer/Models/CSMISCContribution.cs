﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class CSMISCContribution
    {
        public int PK_Con_ID { get; set; }
        public int UserID { get; set; }
        public int ClaimID { get; set; }
         public int PK_CoIns_ID { get; set; }
        public int CoIns_ID { get; set; }
        public string Name { get; set; }
        public decimal Percentage { get; set; }
        public decimal ShareInAmount { get; set; }

    }
}
