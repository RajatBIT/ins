﻿using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public static class BLOTP
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        //public BLOTP()
        //{
        //    dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        //}

        static BLOTP()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public static String OTPWork(CSOTP objProperty)
        {
            string reult = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Mode", objProperty.Mode);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@OTP", objProperty.OTP);
                reult = dbManager.ExecuteNonQuery(DBSP.uspGenerateOTP, "@flag", CommandType.StoredProcedure);
            }
            catch
            {
                reult = "Invalid OTP entered";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return reult;
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
    }
}
