﻿using BusinessLayer.Models;
using DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    class BLMasterAccess
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public Int32 AddAccess(CSMasterAccess objProperty) 
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("", objProperty.FKRoleID);
                dbManager.AddParameters("", objProperty.AccessName);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public Int32 UpdateAccess(CSMasterAccess objProperty)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("", objProperty.PKAccessID);
                dbManager.AddParameters("", objProperty.AccessName);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public Int32 DeleteAccess(CSMasterAccess objProperty)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("", objProperty.PKAccessID);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public List<CSMasterAccess> BindMasterAccess(string PKAccessID, string FKRoleID)
        {
            List<CSMasterAccess> objProperty = new List<CSMasterAccess>();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("", 1);
                if (!string.IsNullOrEmpty(PKAccessID))
                    dbManager.AddParameters("", PKAccessID);
                if (!string.IsNullOrEmpty(FKRoleID))
                    dbManager.AddParameters("", FKRoleID);
                IDataReader datareader = dbManager.ExecuteReader("", CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    CSMasterAccess obj = new CSMasterAccess();
                }
            }
            catch
            {
                objProperty = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
    }
}
