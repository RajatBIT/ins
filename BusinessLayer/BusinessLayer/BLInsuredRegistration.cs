﻿using BusinessLayer.DAL;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.Models
{
    public class BLInsuredRegistration
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion
        SqlParameter[] _param;
        public BLInsuredRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        static BLInsuredRegistration()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }


        public DataTable AddInsured(CSInsuredRegistration objProperty)
        {
            //string result = "";
            //try
            //{
            //    dbManager.ConnectionOpen();
            //    dbManager.AddParameters("@PK_InsdID", objProperty.PkInsdId);
            //    dbManager.AddParameters("@Fk_State", objProperty.FkState);
            //    dbManager.AddParameters("@Fk_District", objProperty.FkDistrict);
            //    dbManager.AddParameters("@FK_InsrID", objProperty.FKInsrId);
            //    dbManager.AddParameters("@Name", objProperty.Name);
            //    dbManager.AddParameters("@UserName", objProperty.UserName);
            //    dbManager.AddParameters("@Email", objProperty.Email);
            //    dbManager.AddParameters("@Mobile", objProperty.Mobile);
            //    dbManager.AddParameters("@AltMobile", objProperty.AltMobile);
            //    dbManager.AddParameters("@Address", objProperty.Address);
            //    dbManager.AddParameters("@PolicyNo", objProperty.PolicyNo);
            //    dbManager.AddParameters("@Pincode", objProperty.Pincode);
            //    dbManager.AddParameters("@Experience", objProperty.Experience);
            //    dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
            //    dbManager.AddParameters("@Password", objProperty.Password);
            //    dbManager.AddParameters("@ConfirmPassword", objProperty.ConfirmPassword);
            //    dbManager.AddParameters("@PANNO", objProperty.PANNO);
            //    dbManager.AddParameters("@GSTIN", objProperty.GSTIN);
            //    dbManager.AddParameters("@InsuredType", objProperty.InsuredType);
            //    dbManager.AddParameters("@AltEmail", objProperty.AltEmail);
            //    dbManager.AddParameters("@Country", objProperty.Country);
            //    dbManager.AddParameters("@AboutInsured", objProperty.AboutInsured);

            //    result = dbManager.ExecuteNonQuery(DBSP.uspInsuredRegistration, "@Flag", System.Data.CommandType.StoredProcedure);
            //    if (result == "Y")
            //        result = "Your request for Insured Subscription has been successfully received. We will contact you soon.";
            //    else if (result == "E")
            //        result = "Email Already Exists.";
            //    else
            //        result = "User name Already Exists.";
            //}
            //catch
            //{
            //    result = "error";
            //}
            //finally
            //{
            //    dbManager.ConnectionClose();
            //}
            //return result;
            DataTable dt = new DataTable();
            _param = new SqlParameter[] {
            new SqlParameter("@PK_InsdID", objProperty.PkInsdId),
            new SqlParameter("@Fk_State", objProperty.FkState),
            new SqlParameter("@Fk_District", objProperty.FkDistrict),
            new SqlParameter("@FK_InsrID", objProperty.FKInsrId),
            new SqlParameter("@Name", objProperty.Name),
            new SqlParameter("@UserName", objProperty.UserName),
            new SqlParameter("@Email", objProperty.Email),
            new SqlParameter("@Mobile", objProperty.Mobile),
            new SqlParameter("@AltMobile", objProperty.AltMobile),
            new SqlParameter("@Address", objProperty.Address),
            new SqlParameter("@PolicyNo", objProperty.PolicyNo),
            new SqlParameter("@Pincode", objProperty.Pincode),
            new SqlParameter("@Experience", objProperty.Experience),
            new SqlParameter("@IPAddress", BusinessHelper.GetIpAdress()),
            new SqlParameter("@Password", objProperty.Password),
            new SqlParameter("@ConfirmPassword", objProperty.ConfirmPassword),
            new SqlParameter("@PANNO", objProperty.PANNO),
            new SqlParameter("@GSTIN", objProperty.GSTIN),
            new SqlParameter("@InsuredType", objProperty.InsuredType),
            new SqlParameter("@AltEmail", objProperty.AltEmail),
            new SqlParameter("@Country", objProperty.Country),
            new SqlParameter("@AboutInsured", objProperty.AboutInsured)
        };
            dt = new SqlFunctions().returnDTWithProc_executeReader("uspInsuredRegistration",_param);
            return dt;
        }

        public DataTable GetAllInsured(CSInsuredRegistration objProperty)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_InsdID", objProperty.PkInsdId);
                dbManager.AddParameters("@FK_State", objProperty.FkState);
                dbManager.AddParameters("@FK_Districts", objProperty.FkDistrict);
                dbManager.AddParameters("@Name", objProperty.Name);
                dbManager.AddParameters("@UserName", objProperty.UserName);
                dbManager.AddParameters("@ApproveStatus", objProperty.ApproveStatus);
                dbManager.AddParameters("@PageSize", objProperty.PageSize);
                dbManager.AddParameters("@PageIndex", objProperty.PageIndex);
                dbManager.AddParameters("@RecordCount", objProperty.RecordCount);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspViewAllInsured, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static string InsuredApproveOrDiscardInsured(string PkInsdId,string status)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_InsdID", PkInsdId);
                dbManager.AddParameters("@ApproveStatus", status);
                result = dbManager.ExecuteNonQuery(DBSP.uspApproveOrDiscardInsured, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable InsuedLogin(string UserName, string password)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@UserName", UserName);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspInsuredLogin, CommandType.StoredProcedure);

                if (dtResult.Rows.Count > 0)
                {
                    if (Convert.ToString(dtResult.Rows[0]["Flag"]).ToLower() == "Y".ToLower())
                    {
                        if (Convert.ToString(dtResult.Rows[0]["Password"]) != password)
                        {
                            foreach (System.Data.DataColumn col in dtResult.Columns)
                                col.ReadOnly = false;

                            dtResult.Rows[0]["Flag"] = "N";
                            dtResult.Rows[0]["Password"] = "wwq90#%&(*&FGSDFSAT&T)%#@FSDSGFGHW213";
                        }
                    }
                }
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        #region check Insured validation
        public DataTable InsuredRegisterValidation(CSInsuredRegistration i)
        {

            _param = new SqlParameter[] { new SqlParameter("@Email", i.Email), new SqlParameter("@Mobile", i.Mobile), new SqlParameter("@UserName", i.UserName), new SqlParameter("@PANNO", i.PANNO),new SqlParameter("@GSTIN", i.GSTIN) };
            return new SqlFunctions().returnDTWithProc_executeReader("uspInsuredRegisterValidation", _param);

        }
        public int saveInsuredRegistrationPhoto_Doc(int InsdID,string PolicyPhoto_Doc)
        {
            _param = new SqlParameter[] {new SqlParameter("@InsdID", InsdID),new SqlParameter("@PolicyPhoto_Doc", PolicyPhoto_Doc) };
            return new SqlFunctions().executeNonQueryWithProc("usptblInsuredRegPolicyPhoto_DocSave",_param);
        }


        public DataTable SaveInsuredRegistration(CSInsuredRegistration i)
        {
            DataTable dt = new DataTable();
            _param = new SqlParameter[] {
            new SqlParameter("@PK_InsdID", i.PkInsdId),
            new SqlParameter("@Fk_State", i.FkState),
            new SqlParameter("@Fk_District", i.FkDistrict),
            new SqlParameter("@FK_InsrID", i.FKInsrId),
            new SqlParameter("@Name", i.Name),
            new SqlParameter("@UserName", i.UserName),
            new SqlParameter("@Email", i.Email),
            new SqlParameter("@Mobile", i.Mobile),
            new SqlParameter("@AltMobile", i.AltMobile),
            new SqlParameter("@Address", i.Address),
            new SqlParameter("@PolicyNo", i.PolicyNo),
            new SqlParameter("@Pincode", i.Pincode),
            new SqlParameter("@Experience", i.Experience),
            new SqlParameter("@IPAddress", BusinessHelper.GetIpAdress()),
            new SqlParameter("@Password", i.Password),
            new SqlParameter("@ConfirmPassword", i.ConfirmPassword),
            new SqlParameter("@PANNO", i.PANNO),
            new SqlParameter("@GSTIN", i.GSTIN),
            new SqlParameter("@InsuredType", i.InsuredType),
            new SqlParameter("@AltEmail", i.AltEmail),
            new SqlParameter("@Country", i.Country),
            new SqlParameter("@AboutInsured", i.AboutInsured)
        };
            dt = new SqlFunctions().returnDTWithProc_executeReader("uspInsuredRegistration", _param);
            return dt;
        }

        public int saveInsuredRegPolicyNo_Insurers(CSInsuredRegPolicyNo_Insurers pi)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_PI_ID", pi.PK_PI_ID), new SqlParameter("@InsdID", pi.InsdID), new SqlParameter("@PolicyNo", pi.PolicyNo), new SqlParameter("@InsrID", pi.InsrID) };
            return new SqlFunctions().executeNonQueryWithProc("uspInsuredRegPolicyNo_InsurersSave", _param);
        }

        public DataTable getInsuredDetails_byPK_InsdID(int pk_InsdID)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_InsdID", pk_InsdID) };
            return new SqlFunctions().returnDTWithProc_executeReader("uspInsuredRegistration_get_byPK_InsdID",_param);
        }

        #endregion

    }
}


