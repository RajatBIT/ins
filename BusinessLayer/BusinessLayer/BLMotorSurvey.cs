﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System.Data.SqlClient;
using BusinessLayer.DAL;

namespace BusinessLayer
{
    public class BLMotorSurvey
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public BLMotorSurvey()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, InsuranceSurvey.DBSP.ConnectionString);
            _sf = new SqlFunctions();
        }

        static BLMotorSurvey()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, InsuranceSurvey.DBSP.ConnectionString);
        }

        public static CSMotorSurvey BindVehicleParticulars(string FkClaimId)
        {
            CSMotorSurvey objCSMotorSurvey = new CSMotorSurvey();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspMotorGetVehicleParticulars, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    objCSMotorSurvey.PK_ID = (int)datareader["PK_ID"];
                    objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
                    objCSMotorSurvey.VehicleType = datareader["VehicleType"].ToString();
                    objCSMotorSurvey.RCNO = datareader["RCNO"].ToString();
                    objCSMotorSurvey.RCDate = datareader["RCDate"].ToString();
                    objCSMotorSurvey.ChasisNo = datareader["ChasisNo"].ToString();
                    objCSMotorSurvey.EngineNo = datareader["EngineNo"].ToString();
                    objCSMotorSurvey.Make = datareader["Make"].ToString();
                    objCSMotorSurvey.Model = datareader["Model"].ToString();
                    objCSMotorSurvey.Class = datareader["Class"].ToString();
                    objCSMotorSurvey.BodyType = datareader["BodyType"].ToString();
                    objCSMotorSurvey.Seating = datareader["Seating"].ToString();
                    objCSMotorSurvey.Fuel = datareader["Fuel"].ToString();
                    objCSMotorSurvey.Color = datareader["Color"].ToString();
                    objCSMotorSurvey.FitnessUpto = datareader["FitnessUpto"].ToString();
                    objCSMotorSurvey.PermitType = datareader["PermitType"].ToString();
                    objCSMotorSurvey.PermitUpto = datareader["PermitUpto"].ToString();
                    objCSMotorSurvey.OperationArea = datareader["OperationArea"].ToString();
                    objCSMotorSurvey.LadenWeight = datareader["LadenWeight"].ToString();
                    objCSMotorSurvey.UnLadenWeight = datareader["UnLadenWeight"].ToString();
                    objCSMotorSurvey.TaxOTT = datareader["TaxOTT"].ToString();
                    objCSMotorSurvey.ATFDevice = datareader["ATFDevice"].ToString();
                    objCSMotorSurvey.PACondition = datareader["PACondition"].ToString();
                    objCSMotorSurvey.Remark = datareader["Remark"].ToString();
                    objCSMotorSurvey.RCFile = datareader["RCFile"].ToString();
                    objCSMotorSurvey.FInterest = datareader["FInterest"].ToString();
                    objCSMotorSurvey.CubCapcity = datareader["CubCapicity"].ToString();
                    objCSMotorSurvey.OdometerReading = datareader["OdometerReading"].ToString();
                    objCSMotorSurvey.Template = datareader["Template"].ToString();
                    objCSMotorSurvey.Petrol = Convert.ToBoolean(datareader["Petrol"]);
                    objCSMotorSurvey.CNG = Convert.ToBoolean(datareader["CNG"]);
                    objCSMotorSurvey.Diesel = Convert.ToBoolean(datareader["Diesel"]);
                    objCSMotorSurvey.LPG = Convert.ToBoolean(datareader["LPG"]);

                }
            }
            catch
            {
                objCSMotorSurvey = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objCSMotorSurvey;
        }

        public String SubmitVehicleParticulars(CSMotorSurvey objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                dbManager.AddParameters("@VehicleType", objCSMotorSurvey.VehicleType);
                dbManager.AddParameters("@RCNO", objCSMotorSurvey.RCNO);
                dbManager.AddParameters("@RCDate", objCSMotorSurvey.RCDate);
                dbManager.AddParameters("@ChasisNo", objCSMotorSurvey.ChasisNo);
                dbManager.AddParameters("@EngineNo", objCSMotorSurvey.EngineNo);
                dbManager.AddParameters("@Make", objCSMotorSurvey.Make);
                dbManager.AddParameters("@Model", objCSMotorSurvey.Model);
                dbManager.AddParameters("@Class", objCSMotorSurvey.Class);
                dbManager.AddParameters("@BodyType", objCSMotorSurvey.BodyType);
                dbManager.AddParameters("@Seating", objCSMotorSurvey.Seating);
                dbManager.AddParameters("@Fuel", objCSMotorSurvey.Fuel);
                dbManager.AddParameters("@Color", objCSMotorSurvey.Color);
                dbManager.AddParameters("@FitnessUpto", objCSMotorSurvey.FitnessUpto);
                dbManager.AddParameters("@PermitType", objCSMotorSurvey.PermitType);
                dbManager.AddParameters("@PermitUpto", objCSMotorSurvey.PermitUpto);
                dbManager.AddParameters("@OperationArea", objCSMotorSurvey.OperationArea);
                dbManager.AddParameters("@LadenWeight", objCSMotorSurvey.LadenWeight);
                dbManager.AddParameters("@UnLadenWeight", objCSMotorSurvey.UnLadenWeight);
                dbManager.AddParameters("@TaxOTT", objCSMotorSurvey.TaxOTT);
                dbManager.AddParameters("@ATFDevice", objCSMotorSurvey.ATFDevice);
                dbManager.AddParameters("@PACondition", objCSMotorSurvey.PACondition);
                dbManager.AddParameters("@Remark", objCSMotorSurvey.Remark);
                dbManager.AddParameters("@FInterest", objCSMotorSurvey.FInterest);
                dbManager.AddParameters("@CubCapicity", objCSMotorSurvey.CubCapcity);
                dbManager.AddParameters("@RCFile", objCSMotorSurvey.RCFile);
                dbManager.AddParameters("@EntryBy", objCSMotorSurvey.EntryBy);
                dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                dbManager.AddParameters("@OdometerReading", objCSMotorSurvey.OdometerReading);
                dbManager.AddParameters("@Template", objCSMotorSurvey.Template);

                dbManager.AddParameters("@Petrol", objCSMotorSurvey.Petrol);
                dbManager.AddParameters("@CNG", objCSMotorSurvey.CNG);
                dbManager.AddParameters("@Diesel", objCSMotorSurvey.Diesel);
                dbManager.AddParameters("@LPG", objCSMotorSurvey.LPG);


                result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitVehicleParticulars, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static CSMotorSurvey BindDLParticulars(string FkClaimId)
        {
            CSMotorSurvey objCSMotorSurvey = new CSMotorSurvey();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspMotorGETDLParticulars, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    objCSMotorSurvey.PK_ID = (int)datareader["PK_ID"];
                    objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
                    objCSMotorSurvey.DriverName = datareader["DriverName"].ToString();
                    objCSMotorSurvey.DLNo = datareader["DLNo"].ToString();
                    objCSMotorSurvey.IssueDate = datareader["IssueDate"].ToString();
                    objCSMotorSurvey.ValidUpTo = datareader["ValidUpTO"].ToString();
                    objCSMotorSurvey.DLAuthority = datareader["DLAuthority"].ToString();
                    objCSMotorSurvey.Endorsment = datareader["Endorsment"].ToString();
                    objCSMotorSurvey.DLRemarks = datareader["Remarks"].ToString();
                    objCSMotorSurvey.DLFile = datareader["DLFile"].ToString();
                    objCSMotorSurvey.Involve = datareader["Involve"].ToString();
                }
            }
            catch
            {
                objCSMotorSurvey = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objCSMotorSurvey;
        }

        public String SubmitDLParticulars(CSMotorSurvey objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                //dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                //dbManager.AddParameters("@DriverName", objCSMotorSurvey.DriverName);
                //dbManager.AddParameters("@DLNo", objCSMotorSurvey.DLNo);
                //dbManager.AddParameters("@IssueDate", objCSMotorSurvey.IssueDate);
                //dbManager.AddParameters("@ValidUpTO", objCSMotorSurvey.ValidUpTo);
                //dbManager.AddParameters("@DLAuthority", objCSMotorSurvey.DLAuthority);
                //dbManager.AddParameters("@Endorsment", objCSMotorSurvey.Endorsment);
                //dbManager.AddParameters("@Remarks", objCSMotorSurvey.DLRemarks);
                //dbManager.AddParameters("@DLFile", objCSMotorSurvey.DLFile);
                //dbManager.AddParameters("@LicenseType", objCSMotorSurvey.LicenseType.TrimEnd(','));
                //dbManager.AddParameters("@Involve", objCSMotorSurvey.Involve);
                //dbManager.AddParameters("@EntryBy", objCSMotorSurvey.EntryBy);
                //dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                //result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitDLParticulars, "@Flag", CommandType.StoredProcedure);
                _param = new SqlParameter[] {
                new SqlParameter("@PK_ID", objCSMotorSurvey.PK_ID),
                new SqlParameter("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID),
                new SqlParameter("@DriverName", objCSMotorSurvey.DriverName),
                new SqlParameter("@DLNo", objCSMotorSurvey.DLNo),
                new SqlParameter("@IssueDate", objCSMotorSurvey.IssueDate),
                new SqlParameter("@ValidUpTO", objCSMotorSurvey.ValidUpTo),
                new SqlParameter("@DLAuthority", objCSMotorSurvey.DLAuthority),
                new SqlParameter("@Endorsment", objCSMotorSurvey.Endorsment),
                new SqlParameter("@Remarks", objCSMotorSurvey.DLRemarks),
                new SqlParameter("@DLFile", objCSMotorSurvey.DLFile),
                new SqlParameter("@LicenseType", objCSMotorSurvey.LicenseType.TrimEnd(',')),
                new SqlParameter("@Involve", objCSMotorSurvey.Involve),
                new SqlParameter("@EntryBy", objCSMotorSurvey.EntryBy),
                new SqlParameter("@IPAddress", BusinessHelper.GetIpAdress())
                
                //result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitDLParticulars, "@Flag", CommandType.StoredProcedure);
            };
                result = Convert.ToString(_sf.executeScalerWithProc(DBSP.uspMotorSubmitDLParticulars, _param));
            }
            catch (Exception ex)
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static CSMotorSurvey BindOccurence(string FkClaimId)
        {
            CSMotorSurvey objCSMotorSurvey = new CSMotorSurvey();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspMotorGetOccurence, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    objCSMotorSurvey.PK_ID = (int)datareader["PK_ID"];
                    objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
                    objCSMotorSurvey.AccDate = datareader["AccDate"].ToString();
                    objCSMotorSurvey.AccPlace = datareader["AccPlace"].ToString();
                    objCSMotorSurvey.SpotSurvey = datareader["SpotSurvey"].ToString();
                    objCSMotorSurvey.FIR = datareader["FIR"].ToString();
                    objCSMotorSurvey.FireReport = datareader["FireReport"].ToString();
                    objCSMotorSurvey.TPPLoss = datareader["TPPLoss"].ToString();
                    objCSMotorSurvey.TPBInjury = datareader["TPBInjury"].ToString();
                    objCSMotorSurvey.Remark = datareader["DamageSides"].ToString();
                    objCSMotorSurvey.LossDesc = datareader["LossDesc"].ToString();
                    objCSMotorSurvey.LicenseType = datareader["LossType"].ToString();
                    objCSMotorSurvey.OccRemarks = datareader["Remarks"].ToString();
                    objCSMotorSurvey.Photo1 = datareader["Photo1"].ToString();
                    objCSMotorSurvey.Photo2 = datareader["Photo2"].ToString();
                }
            }
            catch
            {
                objCSMotorSurvey = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objCSMotorSurvey;
        }

        public String SubmitOccurence(CSMotorSurvey objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                dbManager.AddParameters("@AccDate", objCSMotorSurvey.AccDate);
                dbManager.AddParameters("@AccPlace", objCSMotorSurvey.AccPlace);
                dbManager.AddParameters("@SpotSurvey", objCSMotorSurvey.SpotSurvey);
                dbManager.AddParameters("@FIR", objCSMotorSurvey.FIR);
                dbManager.AddParameters("@FireReport", objCSMotorSurvey.FireReport);
                dbManager.AddParameters("@TPPLoss", objCSMotorSurvey.TPPLoss);
                dbManager.AddParameters("@TPBInjury", objCSMotorSurvey.TPBInjury);
                dbManager.AddParameters("@DamageSides", objCSMotorSurvey.DamageSides);
                dbManager.AddParameters("@Remarks", objCSMotorSurvey.OccRemarks);
                dbManager.AddParameters("@Photo1", objCSMotorSurvey.Photo1);
                dbManager.AddParameters("@Photo2", objCSMotorSurvey.Photo2);
                dbManager.AddParameters("@EntryBy", objCSMotorSurvey.EntryBy);
                dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitOccurence, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }


        public String MotorSubmitEstimateLoss(CSMotorSurvey objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                //dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                //dbManager.AddParameters("@UploadEstimate", objCSMotorSurvey.UploadEstimate);
                //dbManager.AddParameters("@Repairer", objCSMotorSurvey.Repairer);
                //dbManager.AddParameters("@Mobile", objCSMotorSurvey.Mobile);
                //dbManager.AddParameters("@Email", objCSMotorSurvey.Email);
                //dbManager.AddParameters("@TotalCost", objCSMotorSurvey.TotalCost);
                //dbManager.AddParameters("@ConDetails", objCSMotorSurvey.ConDetails);
                //dbManager.AddParameters("@RAddress", objCSMotorSurvey.RAddress);
                //dbManager.AddParameters("@DamageSides", objCSMotorSurvey.DamageSides);
                //dbManager.AddParameters("@EntryBy", objCSMotorSurvey.EntryBy);
                //dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                //result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitEstimateLoss, "@Flag", CommandType.StoredProcedure);
                _param = new SqlParameter[] {
                new SqlParameter("@PK_ID", objCSMotorSurvey.PK_ID),
                new SqlParameter("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID),
                new SqlParameter("@UploadEstimate", objCSMotorSurvey.UploadEstimate),
                new SqlParameter("@Repairer", objCSMotorSurvey.Repairer),
                new SqlParameter("@Mobile", objCSMotorSurvey.Mobile),
                new SqlParameter("@Email", objCSMotorSurvey.Email),
                new SqlParameter("@TotalCost", objCSMotorSurvey.TotalCost),
                new SqlParameter("@ConDetails", objCSMotorSurvey.ConDetails),
                new SqlParameter("@RAddress", objCSMotorSurvey.RAddress),
                //new SqlParameter("@DamageSides", objCSMotorSurvey.DamageSidess),
                new SqlParameter("@EntryBy", objCSMotorSurvey.EntryBy),
                new SqlParameter("@IPAddress", BusinessHelper.GetIpAdress())
            };
                result = Convert.ToString(_sf.executeScalerWithProc(DBSP.uspMotorSubmitEstimateLoss, _param));
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }


        /*~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=*/

        // Added by dhiraj
        public static CSPolicyParticulars BindPolicyParticulars(string FkClaimId)
        {
            CSPolicyParticulars objCSMotorSurvey = new CSPolicyParticulars();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspMotorGetPolicyParticulars, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    objCSMotorSurvey.PK_ID = (int)datareader["PK_PolicyID"];
                    objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
                    //objCSMotorSurvey.Period = datareader["Period"].ToString();
                    objCSMotorSurvey.HPAgreement = datareader["HPAgreement"].ToString();
                    objCSMotorSurvey.RegNo = datareader["Regno"].ToString();
                    objCSMotorSurvey.PolicyNo = datareader["PolicyNo"].ToString();
                    objCSMotorSurvey.Excess = datareader["Excess"].ToString();
                    objCSMotorSurvey.InstructorName = datareader["InstructorName"].ToString();
                    objCSMotorSurvey.InsrAddress = datareader["InsrAddress"].ToString();
                    objCSMotorSurvey.InsrClaimNo = datareader["InsrClaimNo"].ToString();
                    objCSMotorSurvey.InsdType = datareader["InsdType"].ToString();
                    objCSMotorSurvey.PolicyType = datareader["PolicyType"].ToString();

                    objCSMotorSurvey.InsuredName = datareader["InsdName"].ToString();
                    objCSMotorSurvey.InsuredAddress = datareader["InsdAddress"].ToString();
                    objCSMotorSurvey.PeriodFromDT = datareader["PeriodFromDT"].ToString();
                    objCSMotorSurvey.PeriodToDT = datareader["PeriodToDT"].ToString();
                    objCSMotorSurvey.SumInsured =Convert.ToDecimal(datareader["SumInsured"]);
                    objCSMotorSurvey.VehicleRegistrationNo = datareader["VehicleRegistrationNo"].ToString();

                }
            }
            catch
            {
                objCSMotorSurvey = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objCSMotorSurvey;
        }

        public String MotorSubmitPolicyParticulars(CSPolicyParticulars objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                // dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                dbManager.AddParameters("@PolicyNo", objCSMotorSurvey.PolicyNo);
                //dbManager.AddParameters("@Period", objCSMotorSurvey.Period);
                dbManager.AddParameters("@RegNo", objCSMotorSurvey.RegNo);
                dbManager.AddParameters("@HPAgreement", objCSMotorSurvey.HPAgreement);
                dbManager.AddParameters("@Excess", objCSMotorSurvey.Excess);
                dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                dbManager.AddParameters("@PeriodFromDT", objCSMotorSurvey.PeriodFromDT);
                dbManager.AddParameters("@PeriodToDT", objCSMotorSurvey.PeriodToDT);
                dbManager.AddParameters("@SumInsured", objCSMotorSurvey.SumInsured);
                
                result = dbManager.ExecuteNonQuery(DBSP.uspMotorUpdatePolicyParticulars, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static CSMotorRouteParticulars BindMotorRouteParticulars(string FkClaimId)
        {
            CSMotorRouteParticulars objCSMotorSurvey = new CSMotorRouteParticulars();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                IDataReader datareader = dbManager.ExecuteReader(DBSP.uspMotorGetRouteParticulars, CommandType.StoredProcedure);
                while (datareader.Read())
                {
                    objCSMotorSurvey.PK_ID = (int)datareader["PK_PolicyID"];
                    objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
                    objCSMotorSurvey.OrignOfJourney = datareader["OrignOfJourney"].ToString();
                    objCSMotorSurvey.EndOfJourney = datareader["EndOfJourney"].ToString();
                    objCSMotorSurvey.TransportCompany = datareader["TransportCompany"].ToString();
                    objCSMotorSurvey.ConsNoteNo = datareader["ConsNoteNo"].ToString();
                    objCSMotorSurvey.DateOfConsNoteNo = datareader["DateOfConsNoteNo"].ToString();
                    objCSMotorSurvey.TransportItem = datareader["TransportItem"].ToString();
                    objCSMotorSurvey.WeightCons = datareader["WeightCons"].ToString();
                }
            }
            catch
            {
                objCSMotorSurvey = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objCSMotorSurvey;
        }

        public String MotorSubmitRouteParticulars(CSMotorRouteParticulars objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                dbManager.AddParameters("@OrignOfJourney", objCSMotorSurvey.OrignOfJourney);
                dbManager.AddParameters("@EndOfJourney", objCSMotorSurvey.EndOfJourney);
                dbManager.AddParameters("@TransportCompany", objCSMotorSurvey.TransportCompany);
                dbManager.AddParameters("@ConsNoteNo", objCSMotorSurvey.ConsNoteNo);
                dbManager.AddParameters("@DateOfConsNoteNo", objCSMotorSurvey.DateOfConsNoteNo);
                dbManager.AddParameters("@TransportItem", objCSMotorSurvey.TransportItem);
                dbManager.AddParameters("@WeightCons", objCSMotorSurvey.WeightCons);
                dbManager.AddParameters("@EntryBy", objCSMotorSurvey.EntryBy);
                dbManager.AddParameters("@IPAddress", BusinessHelper.GetIpAdress());
                result = dbManager.ExecuteNonQuery(DBSP.uspMotorSubmitRouteParticulars, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        //public static CSMotorPartClaim BindMotorPartsClaim(string FkClaimId)
        //{
        //    CSMotorPartClaim objCSMotorSurvey = new CSMotorPartClaim();
        //    try
        //    {
        //        dbManager.ConnectionOpen();
        //        dbManager.AddParameters("@FK_ClaimID", FkClaimId);
        //        IDataReader datareader = dbManager.ExecuteReader(DBSP.usp_GetMotorPartsClaim, CommandType.StoredProcedure);
        //        while (datareader.Read())
        //        {
        //            objCSMotorSurvey.PK_ID = (int)datareader["PK_PolicyID"];
        //            objCSMotorSurvey.FK_ClaimID = datareader["FK_ClaimID"].ToString();
        //            objCSMotorSurvey.PartName = datareader["PartName"].ToString();
        //            objCSMotorSurvey.Affect = datareader["Affect"].ToString();

        //        }
        //    }
        //    catch
        //    {
        //        objCSMotorSurvey = null;
        //    }
        //    finally
        //    {
        //        dbManager.ConnectionClose();
        //    }
        //    return objCSMotorSurvey;
        //}

        public String MotorSubmitMotorPartClaim(CSMotorPartClaim objCSMotorSurvey)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                // dbManager.AddParameters("@PK_ID", objCSMotorSurvey.PK_ID);
                dbManager.AddParameters("@FK_ClaimID", objCSMotorSurvey.FK_ClaimID);
                dbManager.AddParameters("@FK_PartID", objCSMotorSurvey.PartID);
                dbManager.AddParameters("@FK_AffectID", objCSMotorSurvey.AffectID);
                result = dbManager.ExecuteNonQuery(DBSP.usp_SubmitMotorPartsClaim, CommandType.StoredProcedure).ToString();
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable BindMotorPartsClaims(int FK_ClaimID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FK_ClaimID);
                dtReult = dbManager.ExecuteDataTable(DBSP.usp_GetMotorPartsClaim, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }
        public static DataSet BindMotorParticularRemarks(int FK_ClaimID)
        {
            DataSet dtReult = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FK_ClaimID);
                dtReult = dbManager.ExecuteDataSet(DBSP.usp_GetMotorParticularsRemarksClaim, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }
        public static String MotorParticularRemarkClaim(int claimId, DataTable dt, string VParticular, string RParticular, string OParticular, string DParticular, string PParticular, int? times, decimal? ratio)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dbManager.AddParameters("@typeMotorParticularsRemarksIDs", dt);
                dbManager.AddParameters("@VParticular", VParticular);
                dbManager.AddParameters("@DParticular", DParticular);
                dbManager.AddParameters("@OParticular", OParticular);
                dbManager.AddParameters("@RParticular", RParticular);
                dbManager.AddParameters("@PParticular", PParticular);
                dbManager.AddParameters("@Times", times);
                dbManager.AddParameters("@Ratio", ratio);

                result = dbManager.ExecuteNonQuery(DBSP.usp_InsertMotorParticularsRemarksClaim, "@Flag", CommandType.StoredProcedure).ToString();
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        // Ended by dhiraj
        #region Particular of Survey


        public int saveParticularOfSurvey(CSMotorParticularOfSurvey ps)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_PS_ID", ps.PK_PS_ID), new SqlParameter("@ClaimID", ps.ClaimID), new SqlParameter("@UserID", ps.UserID), new SqlParameter("@ApplicationOfSurveyDT", ps.ApplicationOfSurveyDT), new SqlParameter("@SurveyDT", ps.SurveyDT),new SqlParameter("@SurveyTime", ps.SurveyTime) };
            return _sf.executeNonQueryWithProc("uspMotorParticularOfSurveySave", _param);
        }
        public CSMotorParticularOfSurvey getParticularOfSurvey(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorParticularOfSurveyGet", _param);
            CSMotorParticularOfSurvey ps = new CSMotorParticularOfSurvey();
            if (_dt.Rows.Count > 0)
            {
                ps.PK_PS_ID = Convert.ToInt32(_dt.Rows[0]["PK_PS_ID"]);
                ps.ApplicationOfSurveyDT = _dt.Rows[0]["ApplicationOfSurveyDT"].ToString();
                ps.SurveyDT = _dt.Rows[0]["SurveyDT"].ToString();
                ps.SurveyTime= _dt.Rows[0]["SurveyTime"].ToString();
            }
            return ps;
        }
        public string getInstructorName_byClaimID(int claimId, int userId)
        {
            return Convert.ToString(_sf.executeScalerWithQuery("select InstructorName from tblGeneratedClaims where FK_SurID='" + userId + "' and PK_ID='" + claimId + "'"));

        }
        #endregion
        #region Particulars Of Loss Damage
        public int saveParticularsOfLossDamage(CSCommonMotor pl)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_PL_ID", pl.PK_PL_ID), new SqlParameter("@ClaimID", pl.ClaimID), new SqlParameter("@UserID", pl.UserID), new SqlParameter("@ParticularsOfLossDamage", pl.ParticularsOfLossDamage) };
            return _sf.executeNonQueryWithProc("uspMotorParticularsOfLossDamageSave", _param);
        }
        public CSCommonMotor getParticularsOfLossDamage(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorParticularsOfLossDamageGet", _param);
            CSCommonMotor pl = new CSCommonMotor();
            if (_dt.Rows.Count > 0)
            {
                pl.PK_PL_ID = Convert.ToInt32(_dt.Rows[0]["PK_PL_ID"]);
                pl.ParticularsOfLossDamage = _dt.Rows[0]["ParticularsOfLossDamage"].ToString();
            }
            return pl;
        }
        #endregion
        #region Remarks/Observations
        public int saveRemarksObservations(CSCommonMotor ro)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_RO_ID", ro.PK_RO_ID), new SqlParameter("@ClaimID", ro.ClaimID), new SqlParameter("@UserID", ro.UserID), new SqlParameter("@RemarksObservations", ro.RemarksObservations) };
            return _sf.executeNonQueryWithProc("uspMotorRemarksObservationsSave", _param);
        }
        public CSCommonMotor getRemarksObservations(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorRemarksObservationsGet", _param);
            CSCommonMotor ro = new CSCommonMotor();
            if (_dt.Rows.Count > 0)
            {
                ro.PK_RO_ID = Convert.ToInt32(_dt.Rows[0]["PK_RO_ID"]);
                ro.RemarksObservations = _dt.Rows[0]["RemarksObservations"].ToString();
            }
            return ro;
        }
        #endregion
        #region MotorSalvage
        public int saveSalvage(CSCommonMotor s)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_S_ID", s.PK_S_ID), new SqlParameter("@ClaimID", s.ClaimID), new SqlParameter("@UserID", s.UserID), new SqlParameter("@Salvage", s.Salvage) };
            return _sf.executeNonQueryWithProc("uspMotorSalvageSave", _param);
        }
        public CSCommonMotor getSalvage(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorSalvageGet", _param);
            CSCommonMotor s = new CSCommonMotor();
            if (_dt.Rows.Count > 0)
            {
                s.PK_S_ID = Convert.ToInt32(_dt.Rows[0]["PK_S_ID"]);
                s.Salvage = _dt.Rows[0]["Salvage"].ToString();
            }
            return s;
        }
        #endregion
        #region Re -Inspection
        public int saveReInspection(CSMotorReInspection r)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_RI_ID", r.PK_RI_ID), new SqlParameter("@ClaimID", r.ClaimID), new SqlParameter("@UserID", r.UserID), new SqlParameter("@SurveyVisitDT", r.SurveyVisitDT), new SqlParameter("@PlaceOfSurveyVisit", r.PlaceOfSurveyVisit), new SqlParameter("@PurposeOfVisit", r.PurposeOfVisit), new SqlParameter("@ContactDetail", r.ContactDetail), new SqlParameter("@MobileNo", r.MobileNo), new SqlParameter("@Email", r.Email), new SqlParameter("@ProofOfPayment", r.ProofOfPayment), new SqlParameter("@SalvageDestruction", r.SalvageDestruction), new SqlParameter("@SurveyorRemarks", r.SurveyorRemarks) };
            return _sf.executeNonQueryWithProc("uspMotorReInspectionSave", _param);
        }
        public CSMotorReInspection getReInspection(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorReInspectionGet", _param);
            CSMotorReInspection r = new CSMotorReInspection();
            if (_dt.Rows.Count > 0)
            {
                r.PK_RI_ID = Convert.ToInt32(_dt.Rows[0]["PK_RI_ID"]);
                r.SurveyVisitDT = Convert.ToString(_dt.Rows[0]["SurveyVisitDT"]);
                r.PlaceOfSurveyVisit = Convert.ToString(_dt.Rows[0]["PlaceOfSurveyVisit"]);
                r.PurposeOfVisit = Convert.ToString(_dt.Rows[0]["PurposeOfVisit"]);
                r.ContactDetail = Convert.ToString(_dt.Rows[0]["ContactDetail"]);
                r.MobileNo = Convert.ToString(_dt.Rows[0]["MobileNo"]);
                r.Email = Convert.ToString(_dt.Rows[0]["Email"]);
                r.ProofOfPayment = Convert.ToString(_dt.Rows[0]["ProofOfPayment"]);
                r.SalvageDestruction = Convert.ToString(_dt.Rows[0]["SalvageDestruction"]).Trim();
                r.SurveyorRemarks = Convert.ToString(_dt.Rows[0]["SurveyorRemarks"]);
            }
            return r;
        }
        #endregion
        #region Other legal Aspect
        public int saveOtherLegalAspects(CSMotorOtherLegalAspects ol)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_OL_ID", ol.PK_OL_ID), new SqlParameter("@ClaimID", ol.ClaimID), new SqlParameter("@UserID", ol.UserID), new SqlParameter("@FIRNo", ol.FIRNo), new SqlParameter("@FIRDT", ol.FIRDT), new SqlParameter("@PoliceStation", ol.PoliceStation), new SqlParameter("@SectionsOfIPC", ol.SectionsOfIPC), new SqlParameter("@SubjectMatterFIR", ol.SubjectMatterFIR), new SqlParameter("@FinalReport_Untraceable", ol.FinalReport_Untraceable), new SqlParameter("@Remarks", ol.Remarks) };
            return _sf.executeNonQueryWithProc("uspMotorOtherLegalAspectsSave", _param);
        }
        public CSMotorOtherLegalAspects getOtherLegalAspects(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorOtherLegalAspectsGet", _param);
            CSMotorOtherLegalAspects ol = new CSMotorOtherLegalAspects();
            if (_dt.Rows.Count > 0)
            {
                ol.PK_OL_ID = Convert.ToInt32(_dt.Rows[0]["PK_OL_ID"]);
                ol.FIRNo = Convert.ToString(_dt.Rows[0]["FIRNo"]);
                ol.FIRDT = Convert.ToString(_dt.Rows[0]["FIRDT"]);
                ol.PoliceStation = Convert.ToString(_dt.Rows[0]["PoliceStation"]);
                ol.SectionsOfIPC = Convert.ToString(_dt.Rows[0]["SectionsOfIPC"]);
                ol.SubjectMatterFIR = Convert.ToString(_dt.Rows[0]["SubjectMatterFIR"]);
                ol.FinalReport_Untraceable = Convert.ToString(_dt.Rows[0]["FinalReport_Untraceable"]);
                ol.Remarks = Convert.ToString(_dt.Rows[0]["Remarks"]);

            }
            return ol;
        }
        #endregion
        #region Previous Claim
        public int savePreviousClaim(CSMotorPreviousClaim p)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_ID",p.PK_ID), new SqlParameter("@FK_ClaimID", p.FK_ClaimID), new SqlParameter("@PolicyNo", p.PolicyNo), new SqlParameter("@Insurer", p.Insurer), new SqlParameter("@NoOfAcc", p.NoOfAcc), new SqlParameter("@DateOfAcc", p.DateOfAcc), new SqlParameter("@ClaimedAmt", p.ClaimedAmt), new SqlParameter("@CompensationAmt", p.CompensationAmt), new SqlParameter("@Remark", p.Remark), new SqlParameter("@EntryBy", p.EntryBy), new SqlParameter("@IPAddress", p.IPAddress)
            };
            return _sf.executeNonQueryWithProc("uspMotorPreviousClaimSave", _param);
        }
        public CSMotorPreviousClaim getPreviousClaim(int claimId, string userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@FK_ClaimID", claimId), new SqlParameter("@EntryBy", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorPreviousClaimGet", _param);
            CSMotorPreviousClaim p = new CSMotorPreviousClaim();
            if (_dt.Rows.Count > 0)
            {
                p.PK_ID = Convert.ToInt32(_dt.Rows[0]["PK_ID"]);
                p.PolicyNo = Convert.ToString(_dt.Rows[0]["PolicyNo"]);
                p.Insurer = Convert.ToString(_dt.Rows[0]["Insurer"]);
                p.NoOfAcc = Convert.ToString(_dt.Rows[0]["NoOfAcc"]);
                p.DateOfAcc = Convert.ToString(_dt.Rows[0]["DateOfAcc"]);
                p.ClaimedAmt = Convert.ToDecimal(_dt.Rows[0]["ClaimedAmt"]);
                p.CompensationAmt = Convert.ToDecimal(_dt.Rows[0]["CompensationAmt"]);
                p.Remark = Convert.ToString(_dt.Rows[0]["Remark"]);
            }
            return p;
        }

        #endregion
        #region Assessment

        public List<CSMetologyOfItems> getMetologyOfItems()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMetologyOfItemsGet");
            List<CSMetologyOfItems> listMI = new List<CSMetologyOfItems>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMetologyOfItems mi = new CSMetologyOfItems();
                    mi.PK_MI_ID = Convert.ToInt32(row["PK_MI_ID"]);
                    mi.MetologyOfItems = Convert.ToString(row["MetologyOfItems"]);
                    mi.ShortName = Convert.ToString(row["ShortName"]);
                    mi.Percentage = Convert.ToDecimal(row["Percentage"]);
                    listMI.Add(mi);

                }

            }
            return listMI;
        }
        public List<CSMotorAssParticular> getMotorParticular()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssParticularGet");
            List<CSMotorAssParticular> listP = new List<CSMotorAssParticular>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorAssParticular p = new CSMotorAssParticular();
                    p.PK_P_ID = Convert.ToInt32(row["PK_P_ID"]);
                    p.Particular = Convert.ToString(row["Particular"]);
                    listP.Add(p);

                }

            }
            return listP;
        }
        public decimal? getDepriciation_byMonth(string claimId, string userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return Convert.ToDecimal(_sf.executeScalerWithProc("usp_tblMotorMetalPerc_getDepriciation_byMonth", _param));
        }
        public int saveAssessmentHeader(CSMotorAssessmentHeader ah)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AH_ID", ah.PK_AH_ID), new SqlParameter("@ClaimID", ah.ClaimID), new SqlParameter("@UserID", ah.UserID), new SqlParameter("@ShortName", ah.ShortName), new SqlParameter("@SubTotalC", ah.SubTotalC), new SqlParameter("@SubTotalA", ah.SubTotalA), new SqlParameter("@SalvageRate", ah.SalvageRate), new SqlParameter("@SalvageAmount", ah.SalvageAmount), new SqlParameter("@DepriciationRate", ah.DepriciationRate), new SqlParameter("@DepriciationAmount", ah.DepriciationAmount), new SqlParameter("@AddDepriciationRate", ah.AddDepriciationRate), new SqlParameter("@AddDepriciationAmount", ah.AddDepriciationAmount), new SqlParameter("@DiscountRate", ah.DiscountRate), new SqlParameter("@DiscountAmount", ah.DiscountAmount), new SqlParameter("@GrandTotal", ah.GrandTotal) };
            return Convert.ToInt32(_sf.executeScalerWithProc("usptblMotorAssessmentHeaderSave", _param));
        }

        public int saveAssessmentDesc(CSMotorAssessmentDesc ad)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AD_ID", ad.PK_AD_ID), new SqlParameter("@AH_ID", ad.AH_ID), new SqlParameter("@Items", ad.Items), new SqlParameter("@QtyC", ad.QtyC), new SqlParameter("@RateC", ad.RateC), new SqlParameter("@AmountC", ad.AmountC), new SqlParameter("@QtyA", ad.QtyA), new SqlParameter("@RateA", ad.RateA), new SqlParameter("@AmountA", ad.AmountA) };
            return _sf.executeNonQueryWithProc("uspMotorAssessmentDescSave", _param);
        }


        public int saveAssessmentCalculation(CSMotorAssessmentCalculation ac)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AC_ID", ac.PK_AC_ID), new SqlParameter("@AH_ID", ac.AH_ID), new SqlParameter("@Sign", ac.Sign), new SqlParameter("@P_ID", ac.P_ID), new SqlParameter("@Rate", ac.Rate), new SqlParameter("@Amount", ac.Amount) }; return _sf.executeNonQueryWithProc("uspMotorAssessmentCalculationSave", _param);
        }
        public CSMotorAssessmentHeader getAssessmentHeader(int claimId, int userId, string shortName)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@ShortName", shortName) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssessmentHeaderGet", _param);
            CSMotorAssessmentHeader ah = new CSMotorAssessmentHeader();
            if (_dt.Rows.Count > 0)
            {
                ah.PK_AH_ID = Convert.ToInt32(_dt.Rows[0]["PK_AH_ID"]);
                ah.SubTotalC = Convert.ToDecimal(_dt.Rows[0]["SubTotalC"]);
                ah.SubTotalA = Convert.ToDecimal(_dt.Rows[0]["SubTotalA"]);
                ah.SalvageRate = Convert.ToDecimal(_dt.Rows[0]["SalvageRate"]);
                ah.SalvageAmount = Convert.ToDecimal(_dt.Rows[0]["SalvageAmount"]);
                ah.DepriciationRate = Convert.ToDecimal(_dt.Rows[0]["DepriciationRate"]);
                ah.DepriciationAmount = Convert.ToDecimal(_dt.Rows[0]["DepriciationAmount"]);
                ah.AddDepriciationRate = Convert.ToDecimal(_dt.Rows[0]["AddDepriciationRate"]);
                ah.AddDepriciationAmount = Convert.ToDecimal(_dt.Rows[0]["AddDepriciationAmount"]);
                ah.DiscountRate = Convert.ToDecimal(_dt.Rows[0]["DiscountRate"]);
                ah.DiscountAmount = Convert.ToDecimal(_dt.Rows[0]["DiscountAmount"]);
                ah.GrandTotal = Convert.ToDecimal(_dt.Rows[0]["GrandTotal"]);
            }
            return ah;
        }
        public List<CSMotorAssessmentDesc> getAssessmentDesc(int ah_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@AH_ID", ah_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssessmentDescGet", _param);
            List<CSMotorAssessmentDesc> listAssDesc = new List<CSMotorAssessmentDesc>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorAssessmentDesc ad = new CSMotorAssessmentDesc();
                    ad.PK_AD_ID = Convert.ToInt32(row["PK_AD_ID"]);
                    ad.Items = Convert.ToString(row["Items"]);
                    ad.QtyC = Convert.ToDecimal(row["QtyC"]);
                    ad.RateC = Convert.ToDecimal(row["RateC"]);
                    ad.AmountC = Convert.ToDecimal(row["AmountC"]);
                    ad.QtyA = Convert.ToDecimal(row["QtyA"]);
                    ad.RateA = Convert.ToDecimal(row["RateA"]);
                    ad.AmountA = Convert.ToDecimal(row["AmountA"]);
                    listAssDesc.Add(ad);
                }

            }
            return listAssDesc;
        }
        public List<CSMotorAssessmentCalculation> getAssessmentCalculation(int ah_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@AH_ID", ah_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssessmentCalculationGet", _param);
            List<CSMotorAssessmentCalculation> listAssCalc = new List<CSMotorAssessmentCalculation>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorAssessmentCalculation ac = new CSMotorAssessmentCalculation();
                    ac.PK_AC_ID = Convert.ToInt32(row["PK_AC_ID"]);
                    ac.Sign = Convert.ToString(row["Sign"]);
                    ac.P_ID = Convert.ToInt32(row["P_ID"]);
                    ac.Rate = Convert.ToDecimal(row["Rate"]);
                    ac.Amount = Convert.ToDecimal(row["Amount"]);
                    listAssCalc.Add(ac);

                }
            }
            return listAssCalc;
        }
        public int deleteMotorAssessmentCalculation(int pk_AC_ID)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AC_ID", pk_AC_ID) };
            return _sf.executeNonQueryWithProc("uspMotorAssessmentCalculationDelete", _param);
        }
        public List<CSMotorAssessmentHeader> getAssessmentHeader_usingFor_AssessmentSummery(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssessmentHeaderGet_usingFor_AssessmentSummery", _param);
            List<CSMotorAssessmentHeader> listAssHeader = new List<CSMotorAssessmentHeader>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorAssessmentHeader ah = new CSMotorAssessmentHeader();
                    ah.PK_AH_ID = Convert.ToInt32(row["PK_AH_ID"]);
                    ah.ShortName = Convert.ToString(row["ShortName"]);
                    ah.SubTotalC = Convert.ToDecimal(row["SubTotalC"]);
                    ah.SubTotalA = Convert.ToDecimal(row["SubTotalA"]);
                    ah.SalvageRate = Convert.ToDecimal(row["SalvageRate"]);
                    ah.SalvageAmount = Convert.ToDecimal(row["SalvageAmount"]);
                    ah.DepriciationRate = Convert.ToDecimal(row["DepriciationRate"]);
                    ah.DepriciationAmount = Convert.ToDecimal(row["DepriciationAmount"]);
                    ah.AddDepriciationRate = Convert.ToDecimal(row["AddDepriciationRate"]);
                    ah.AddDepriciationAmount = Convert.ToDecimal(row["AddDepriciationAmount"]);
                    ah.DiscountRate = Convert.ToDecimal(row["DiscountRate"]);
                    ah.DiscountAmount = Convert.ToDecimal(row["DiscountAmount"]);
                    ah.GrandTotal = Convert.ToDecimal(row["GrandTotal"]);
                    listAssHeader.Add(ah);
                }
            }
            return listAssHeader;

        }

        #endregion
        #region Motor Assessment Summery
        public int saveAssessmentSummery(CSMotorAssessmentSummery mas)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AS_ID", mas.PK_AS_ID), new SqlParameter("@ClaimID", mas.ClaimID), new SqlParameter("@UserID", mas.UserID), new SqlParameter("@TowingCharge", mas.TowingCharge), new SqlParameter("@Excess", mas.Excess) };
            return _sf.executeNonQueryWithProc("uspMotorAssessmentSummerySave", _param);
        }
        public CSMotorAssessmentSummery getAssessmentSummery(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorAssessmentSummeryGet", _param);
            CSMotorAssessmentSummery mas = new CSMotorAssessmentSummery();
            if (_dt.Rows.Count > 0)
            {
                mas.PK_AS_ID = Convert.ToInt32(_dt.Rows[0]["PK_AS_ID"]);
                mas.TowingCharge = Convert.ToDecimal(_dt.Rows[0]["TowingCharge"]);
                mas.Excess = Convert.ToDecimal(_dt.Rows[0]["Excess"]);
                mas.GrandTotalofAssHeader = Convert.ToDecimal(_dt.Rows[0]["GrandTotalofAssHeader"]);
                mas.GrandTotalofAssSummery = Convert.ToDecimal(_dt.Rows[0]["GrandTotalofAssSummery"]);
                mas.AfterTowingChargeTotal = Convert.ToDecimal(_dt.Rows[0]["AfterTowingChargeTotal"]);
            }
            return mas;
        }
        #endregion
        #region Motor Occurence
        public int saveOccurenceNew(CSMotorOccurenceNew o)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_ID", o.PK_ID), new SqlParameter("@FK_ClaimID", o.FK_ClaimID), new SqlParameter("@AccDate", o.AccDate), new SqlParameter("@AccPlace", o.AccPlace), new SqlParameter("@SpotSurvey", o.SpotSurvey), new SqlParameter("@FIR", o.FIR), new SqlParameter("@FireReport", o.FireReport), new SqlParameter("@TPPLoss", o.TPPLoss), new SqlParameter("@TPBInjury", o.TPBInjury), new SqlParameter("@DamageSides", o.DamageSides), new SqlParameter("@Remarks", o.Remarks), new SqlParameter("@EntryBy", o.EntryBy), new SqlParameter("@IPAddress", o.IPAddress), new SqlParameter("@NameSpotSurveyor", o.NameSpotSurveyor), new SqlParameter("@SpotSurveyDT", o.SpotSurveyDT), new SqlParameter("@FireBrigadeReportNo", o.FireBrigadeReportNo), new SqlParameter("@FireReportDT", o.FireReportDT), new SqlParameter("@NameFireStation", o.NameFireStation), new SqlParameter("@FireTenderNo", o.FireTenderNo), new SqlParameter("@TotalTimeConsumeFireFight", o.TotalTimeConsumeFireFight),new SqlParameter("@LossDesc",o.LossDesc)
            };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspMotorOccurenceSaveNew", _param));
        }

        public int saveThirdPartyPropDetails(CSMotorOccThirdPartyDetails tpp)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_TP_ID", tpp.PK_TP_ID), new SqlParameter("@ClaimID", tpp.ClaimID), new SqlParameter("@UserID", tpp.UserID), new SqlParameter("@NameOfOwner", tpp.NameOfOwner), new SqlParameter("@ParticularOfProperty", tpp.ParticularOfProperty), new SqlParameter("@EstimatedValue", tpp.EstimatedValue) };
            return _sf.executeNonQueryWithProc("uspMotorOccThirdPartyPropDetailsSave", _param);
        }

        public int saveThirdPartyBodily(CSMotorOccThirdPartyDetails tpb)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_TPB_ID", tpb.PK_TPB_ID), new SqlParameter("@ClaimID", tpb.ClaimID), new SqlParameter("@UserID", tpb.UserID), new SqlParameter("@NameOfVictim", tpb.NameOfVictim), new SqlParameter("@ParticularOfInjury", tpb.ParticularOfInjury), new SqlParameter("@Hospitalization", tpb.Hospitalization) };
            return _sf.executeNonQueryWithProc("uspMotorOccThirdPartyBodilySave", _param);
        }
        public CSMotorOccurenceNew getOccurenceNew(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            CSMotorOccurenceNew o = new CSMotorOccurenceNew();
            _dt = _sf.returnDTWithProc_executeReader("uspMotorOccurenceGetNew", _param);
            if (_dt.Rows.Count > 0)
            {
                o.PK_ID = Convert.ToInt32(_dt.Rows[0]["PK_ID"]);
                o.AccDate= Convert.ToString(_dt.Rows[0]["AccDate"]);
                o.AccPlace = Convert.ToString(_dt.Rows[0]["AccPlace"]);
                o.SpotSurvey = Convert.ToString(_dt.Rows[0]["SpotSurvey"]);
                o.FIR = Convert.ToString(_dt.Rows[0]["FIR"]);
                o.FireReport = Convert.ToString(_dt.Rows[0]["FireReport"]);
                o.TPPLoss = Convert.ToString(_dt.Rows[0]["TPPLoss"]);
                o.TPBInjury = Convert.ToString(_dt.Rows[0]["TPBInjury"]);
                o.DamageSides = Convert.ToString(_dt.Rows[0]["DamageSides"]);
                o.Remarks = Convert.ToString(_dt.Rows[0]["Remarks"]);
                o.EntryBy = Convert.ToInt32(_dt.Rows[0]["EntryBy"]);
                o.IPAddress = Convert.ToString(_dt.Rows[0]["IPAddress"]);
                o.NameSpotSurveyor = Convert.ToString(_dt.Rows[0]["NameSpotSurveyor"]);
                o.SpotSurveyDT = Convert.ToString(_dt.Rows[0]["SpotSurveyDT"]);
                o.FireBrigadeReportNo = Convert.ToString(_dt.Rows[0]["FireBrigadeReportNo"]);
                o.FireReportDT = Convert.ToString(_dt.Rows[0]["FireReportDT"]);
                o.NameFireStation = Convert.ToString(_dt.Rows[0]["NameFireStation"]);
                o.FireTenderNo = Convert.ToString(_dt.Rows[0]["FireTenderNo"]);
                o.TotalTimeConsumeFireFight = Convert.ToString(_dt.Rows[0]["TotalTimeConsumeFireFight"]);
                o.LossDesc = Convert.ToString(_dt.Rows[0]["LossDesc"]);
                  o.Photo1 = Convert.ToString(_dt.Rows[0]["Photo1"]);
                o.Photo2 = Convert.ToString(_dt.Rows[0]["Photo2"]);

            }
            return o;
        }
        public List<CSMotorOccThirdPartyDetails> getThirdPartyPropDetails(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorOccThirdPartyPropDetailsGet", _param);
            List<CSMotorOccThirdPartyDetails> listTPP = new List<CSMotorOccThirdPartyDetails>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorOccThirdPartyDetails tpp = new CSMotorOccThirdPartyDetails();
                    tpp.PK_TP_ID = Convert.ToInt32(row["PK_TP_ID"]);
                    tpp.NameOfOwner = Convert.ToString(row["NameOfOwner"]);
                    tpp.ParticularOfProperty = Convert.ToString(row["ParticularOfProperty"]);
                    tpp.EstimatedValue = Convert.ToDecimal(row["EstimatedValue"]);
                    listTPP.Add(tpp);
                }
            }
            return listTPP;
        }

        public List<CSMotorOccThirdPartyDetails> getThirdPartyPropBodily(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorOccThirdPartyBodilyGet", _param);
            List<CSMotorOccThirdPartyDetails> listTPB = new List<CSMotorOccThirdPartyDetails>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorOccThirdPartyDetails tpb = new CSMotorOccThirdPartyDetails();
                    tpb.PK_TPB_ID = Convert.ToInt32(row["PK_TPB_ID"]);
                    tpb.NameOfVictim = Convert.ToString(row["NameOfVictim"]);
                    tpb.ParticularOfInjury = Convert.ToString(row["ParticularOfInjury"]);
                    tpb.Hospitalization = Convert.ToString(row["Hospitalization"]);
                    listTPB.Add(tpb);
                }
            }
            return listTPB;
        }

        public List<CSMotorLossClaim> getLossClaim(int claimId, int userId)
        {
            List<CSMotorLossClaim> listLossClaim = new List<CSMotorLossClaim>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorLossClaimGet", _param);
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorLossClaim lc = new CSMotorLossClaim();
                    lc.LM_ID =Convert.ToInt32(row["LM_ID"]);
                    lc.Loss =Convert.ToString(row["Loss"]);
                    lc.IsChecked =Convert.ToBoolean(row["IsChecked"]);
                    listLossClaim.Add(lc);
                }
            }
            return listLossClaim;
        }


        public int saveLossClaim(CSMotorLossClaim lc)
        {
            _param = new SqlParameter[] {new SqlParameter("@LM_ID",lc.LM_ID),new SqlParameter("@ClaimID",lc.ClaimID),new SqlParameter("@UserID",lc.UserID) };
            return _sf.executeNonQueryWithProc("uspMotorLossClaimSave",_param);
        }

       


        public List<CSMotorDamageClaim> getDamageClaim(int claimId, int userId)
        {
            List<CSMotorDamageClaim> listDamageClaim = new List<CSMotorDamageClaim>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMotorDamageClaimGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMotorDamageClaim dm = new CSMotorDamageClaim();
                    dm.DS_ID = Convert.ToInt32(row["DS_ID"]);
                    dm.Damage = Convert.ToString(row["Damage"]);
                    dm.IsChecked = Convert.ToBoolean(row["IsChecked"]);
                    listDamageClaim.Add(dm);
                }
            }
            return listDamageClaim;
        }

        public int saveDamageClaim(CSMotorDamageClaim dm)
        {
            _param = new SqlParameter[] { new SqlParameter("@DS_ID", dm.DS_ID), new SqlParameter("@ClaimID", dm.ClaimID), new SqlParameter("@UserID", dm.UserID) };
            return _sf.executeNonQueryWithProc("uspMotorDamageClaimSave", _param);
        }

        public int deleteOccThirdPartyPropDetails(int pk_TP_Id,int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_TP_ID", pk_TP_Id),new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMotorOccThirdPartyPropDetailsDelete", _param);
        }


        public int deleteOccThirdPartyBodily(int pk_TPB_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_TPB_ID", pk_TPB_Id), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMotorOccThirdPartyBodilyDelete", _param);
        }



        #endregion

    }
}
