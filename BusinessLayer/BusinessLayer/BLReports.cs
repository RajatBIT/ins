﻿using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BusinessLayer
{
    public class BLReports
    {
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public BLReports()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        static BLReports()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public static DataSet GetMotorJIR(string ClaimID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                //dbManager.AddParameters("@InsdType", InsdType);
                //dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMotorJIR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        public static DataSet GetMotorClaimForm(string ClaimID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMotorClaimForm, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }


    }
}
