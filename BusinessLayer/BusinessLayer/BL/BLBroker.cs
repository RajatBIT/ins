﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
public  class BLBroker
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLBroker()
        {
            _sf = new SqlFunctions();
        }

        public int saveBrokerRegisteration(CSBrokerRegisteration br)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_BR_ID", br.PK_BR_ID), new SqlParameter("@Broker_ID", br.Broker_ID), new SqlParameter("@PricipalOfficerName", br.PricipalOfficerName), new SqlParameter("@EmailId", br.EmailId), new SqlParameter("@MobileNo", br.MobileNo), new SqlParameter("@AltMobileNo", br.AltMobileNo), new SqlParameter("@Address", br.Address), new SqlParameter("@UserName", br.UserName), new SqlParameter("@Password", br.Password), new SqlParameter("@ConfirmPassword", br.ConfirmPassword) };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspBrokerRegisterationSave", _param));
        }

        public int saveBrokerBranch_And_EmailId(CSBrokerRegisteration bb)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_BB_ID", bb.PK_BB_ID), new SqlParameter("@BR_ID", bb.BR_ID), new SqlParameter("@Branch", bb.Branch), new SqlParameter("@AltEmailId", bb.AltEmailId) };
            return _sf.executeNonQueryWithProc("uspBrokerBranch_And_EmailIdSave", _param);
        }


        public DataTable BrokerRegisterValidation(CSBrokerRegisteration br)
        {

            _param = new SqlParameter[] { new SqlParameter("@EmailId", br.EmailId), new SqlParameter("@MobileNo", br.MobileNo), new SqlParameter("@UserName", br.UserName)
                };
            return new SqlFunctions().returnDTWithProc_executeReader("uspBrokerRegisterValidation", _param);

        }
       public int deleteBrokerBranch_And_EmailId(int PK_BB_ID)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_BB_ID",PK_BB_ID) };
            return _sf.executeNonQueryWithProc("uspBrokerBranch_And_EmailIdDelete",_param);
        }
       public List<CSBrokerRegisteration> getBrokerRegisteration()
        {
            List<CSBrokerRegisteration> listBrokerDetails = new List<CSBrokerRegisteration>();
            _dt = _sf.returnDTWithProc_executeReader("uspBrokerRegisterationGet");
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBrokerRegisteration b = new CSBrokerRegisteration();
                    b.PK_BR_ID = Convert.ToInt32(row["PK_BR_ID"]);
                    b.Broker_ID = Convert.ToInt32(row["Broker_ID"]);
                    b.PricipalOfficerName = Convert.ToString(row["PricipalOfficerName"]);
                    b.EmailId = Convert.ToString(row["EmailId"]);
                    b.MobileNo = Convert.ToString(row["MobileNo"]);
                    b.AltMobileNo = Convert.ToString(row["AltMobileNo"]);
                    b.Address = Convert.ToString(row["Address"]);
                    b.UserName = Convert.ToString(row["UserName"]);
                    b.Password = Convert.ToString(row["Password"]);
                    b.ConfirmPassword = Convert.ToString(row["ConfirmPassword"]);
                    b.BrokerName= Convert.ToString(row["BrokerName"]);
                    b.ApprovedStatus = Convert.ToString(row["ApprovedStatus"]);
                    listBrokerDetails.Add(b);
                }
            }
            return listBrokerDetails;
        }
        public string approveOrDiscardBroker(int PK_BR_ID,string ApprovedStatus)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_BR_ID",PK_BR_ID),new SqlParameter("@ApprovedStatus",ApprovedStatus) };
            return Convert.ToString(_sf.executeScalerWithProc("uspApproveOrDiscardBroker",_param));
        }
    }
}
