﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
public    class BLSalvage
    {

        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLSalvage()
        {
            _sf = new SqlFunctions();
        }
        #region Admin Salvage
        public int saveAdminSalvage(CSAdminSalvage a)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AS_ID", a.PK_AS_ID), new SqlParameter("@ClaimID", a.ClaimID), new SqlParameter("@UserID", a.UserID), new SqlParameter("@LocationOfAffectedMaterial", a.LocationOfAffectedMaterial), new SqlParameter("@ItemDesc", a.ItemDesc), new SqlParameter("@AffectedQuantity", a.AffectedQuantity), new SqlParameter("@InvoiceRate", a.InvoiceRate) };
            return Convert.ToInt32( _sf.executeScalerWithProc("uspAdminSalvageSave", _param));
        }
        public List<CSAdminSalvage> getAdminSalvage(int claimId,int userId)
        {
            List<CSAdminSalvage> listAS = new List<CSAdminSalvage>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID",claimId),new SqlParameter("@UserID",userId)};
            _dt = _sf.returnDTWithProc_executeReader("uspAdminSalvageGet",_param);
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSAdminSalvage a = new CSAdminSalvage();
                    a.PK_AS_ID = Convert.ToInt32(row["PK_AS_ID"]);
                    a.LocationOfAffectedMaterial = Convert.ToString(row["LocationOfAffectedMaterial"]);
                    a.ItemDesc = Convert.ToString(row["ItemDesc"]);
                    a.AffectedQuantity = Convert.ToDecimal(row["AffectedQuantity"]);
                    a.InvoiceRate = Convert.ToDecimal(row["InvoiceRate"]);
                    a.Photo1 = Convert.ToString("/files/MISC/"+ row["Photo1"]);
                    a.Photo2 = Convert.ToString("/files/MISC/" + row["Photo2"]);
                    a.Photo3 = Convert.ToString("/files/MISC/" + row["Photo3"]);
                    a.Photo4 = Convert.ToString("/files/MISC/" + row["Photo4"]);
                    listAS.Add(a);
                }
            }
            return listAS;
        }
        #endregion
        #region Buyer Salvage
        public int saveSalvageQuery(CSSalvage_Query_Offer q)
        {
          
                _param = new SqlParameter[] { new SqlParameter("@PK_SQ_ID", q.PK_SQ_ID), new SqlParameter("@ClaimID", q.ClaimID), new SqlParameter("@UserID", q.UserID), new SqlParameter("@AS_ID", q.AS_ID), new SqlParameter("@Query", q.Query) };

                return _sf.executeNonQueryWithProc("uspSalvageQuerySave", _param);
          
        }
        public int saveSalvageOffer(CSSalvage_Query_Offer o)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_SO_ID", o.PK_SO_ID), new SqlParameter("@ClaimID", o.ClaimID), new SqlParameter("@UserID", o.UserID), new SqlParameter("@AS_ID", o.AS_ID), new SqlParameter("@Rate", o.Rate), new SqlParameter("@Amount", o.Amount), new SqlParameter("@GST", o.GST), new SqlParameter("@EMD_Amount", o.EMD_Amount), new SqlParameter("@Full_Amount", o.Full_Amount) };
            return _sf.executeNonQueryWithProc("uspSalvageOfferSave", _param);
        }
        public CSSalvage_Query_Offer getSalvageQuery(int claimId,int userId)
        {
            _param = new SqlParameter[] {new SqlParameter("@ClaimID",claimId),new SqlParameter("@UserID",userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspSalvageQueryGet", _param);
            CSSalvage_Query_Offer q = new CSSalvage_Query_Offer();
            if(_dt.Rows.Count>0)
            {
                q.PK_SQ_ID =Convert.ToInt32(_dt.Rows[0]["PK_SQ_ID"]);
                q.Query = _dt.Rows[0]["Query"].ToString();
            }
            return q;
        }
        public CSSalvage_Query_Offer getSalvageOffer(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspSalvageOfferGet", _param);
            CSSalvage_Query_Offer o = new CSSalvage_Query_Offer();
            if (_dt.Rows.Count > 0)
            {
                o.PK_SO_ID = Convert.ToInt32(_dt.Rows[0]["PK_SO_ID"]);
                o.Rate = Convert.ToDecimal(_dt.Rows[0]["Rate"]);
                o.Amount = Convert.ToDecimal(_dt.Rows[0]["Amount"]);
                o.GST = _dt.Rows[0]["GST"].ToString().Trim();
                o.EMD_Amount = _dt.Rows[0]["EMD_Amount"].ToString();
                o.Full_Amount = _dt.Rows[0]["Full_Amount"].ToString();
            }
            return o;
        }
        #endregion
    }
}
