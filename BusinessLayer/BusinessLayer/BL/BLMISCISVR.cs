﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BLMISCISVR
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLMISCISVR()
        {
            _sf = new SqlFunctions();
        }
        #region Intro Claim
        public int SaveMISCIntroClaim(CSMISCISVRIntroClaim ic)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", ic.ClaimID),new SqlParameter("@UserID",ic.UserID), new SqlParameter("@InsurerClaimNo", ic.InsurersClaimNo), new SqlParameter("@PolicyNo", ic.PolicyNo), new SqlParameter("@PolicyType", ic.PolicyType), new SqlParameter("@IntimationLossDT", ic.IntimationLossDT), new SqlParameter("@AllotmentSurveyDT", ic.AllotmentSurveyDT), new SqlParameter("@SourceAllotmentSurvey", ic.SourceAllotmentSurvey), new SqlParameter("@Insurers", ic.Insurers), new SqlParameter("@Address", ic.Address), new SqlParameter("@SurveyVisitInitiatedDT", ic.SurveyVisitInitiatedDT), new SqlParameter("@SurveyVisitCompletedDT", ic.SurveyVisitCompletedDT), new SqlParameter("@ReasonDelaySurvey", ic.ReasonDelaySurvey), new SqlParameter("@InsuredName", ic.InsuredName), new SqlParameter("@InsuredAddress", ic.InsuredAddress), new SqlParameter("@PS_ALL", ic.PS_ALL), new SqlParameter("@PS_ALL_Place", ic.PS_ALL_Place), new SqlParameter("@Loss", ic.Loss), new SqlParameter("@LossDT", ic.LossDT), new SqlParameter("@CL_OP", ic.CL_OP), new SqlParameter("@CL_OP_ID", ic.CL_OP_ID), };
            return _sf.executeNonQueryWithProc("uspMISCISVRIntroClaimSave", _param);
        }
        public int UpdateMISCIntroClaim(CSMISCISVRIntroClaim ic)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_MISCClaim_ID",ic.PK_MISCClaim_ID), new SqlParameter("@UserID",ic.UserID), new SqlParameter("@InsurerClaimNo", ic.InsurersClaimNo), new SqlParameter("@PolicyNo", ic.PolicyNo), new SqlParameter("@PolicyType", ic.PolicyType), new SqlParameter("@IntimationLossDT", ic.IntimationLossDT), new SqlParameter("@AllotmentSurveyDT", ic.AllotmentSurveyDT), new SqlParameter("@SourceAllotmentSurvey", ic.SourceAllotmentSurvey), new SqlParameter("@Insurers", ic.Insurers), new SqlParameter("@Address", ic.Address), new SqlParameter("@SurveyVisitInitiatedDT", ic.SurveyVisitInitiatedDT), new SqlParameter("@SurveyVisitCompletedDT", ic.SurveyVisitCompletedDT), new SqlParameter("@ReasonDelaySurvey", ic.ReasonDelaySurvey), new SqlParameter("@InsuredName", ic.InsuredName), new SqlParameter("@InsuredAddress", ic.InsuredAddress), new SqlParameter("@PS_ALL", ic.PS_ALL), new SqlParameter("@PS_ALL_Place", ic.PS_ALL_Place), new SqlParameter("@Loss", ic.Loss), new SqlParameter("@LossDT", ic.LossDT), new SqlParameter("@CL_OP", ic.CL_OP), new SqlParameter("@CL_OP_ID", ic.CL_OP_ID), };
            return _sf.executeNonQueryWithProc("uspMISCISVRIntroClaimUpdate", _param);
        }

        public List<CSMISCCL_OP> getCL_OP(DataTable _dt)
        {
            throw new NotImplementedException();
        }

        public List<CSMISCCL_OP> getCL_OP()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCL_OPGet");
            List<CSMISCCL_OP> list = new List<CSMISCCL_OP>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCL_OP co = new CSMISCCL_OP();
                    co.CL_OP_ID = Convert.ToInt32(row["CL_OP_ID"]);
                    co.CL_OP = Convert.ToString(row["CL_OP"]);
                    list.Add(co);
                }
            }
            return list;
        }
        public CSMISCISVRIntroClaim ShowMISCIntroClaim(int ClaimID,int UserID)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", ClaimID),new SqlParameter("@UserID",UserID)};
            _dt = _sf.returnDTWithProc_executeReader("uspMISCISVRIntroClaimGet", _param);
            List<CSMISCISVRIntroClaim> list = new List<CSMISCISVRIntroClaim>();

            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCISVRIntroClaim obj = new CSMISCISVRIntroClaim();
                    obj.PK_MISCClaim_ID = Convert.ToInt32(row["PK_MISCClaim_ID"]);
                    //obj.ClaimID = Convert.ToInt32(row["ClaimID"]);
                    //obj.UserID = Convert.ToInt32(row["UserID"]);
                    obj.InsurersClaimNo = Convert.ToString(row["InsurerClaimNo"]);
                    obj.PolicyNo = Convert.ToString(row["PolicyNo"]);
                    obj.PolicyType = row["PolicyType"].ToString();
                    obj.IntimationLossDT = Convert.ToDateTime(row["IntimationLossDT"]);
                    obj.AllotmentSurveyDT = Convert.ToDateTime(row["AllotmentSurveyDT"]);
                    obj.SourceAllotmentSurvey = row["SourceAllotmentSurvey"].ToString();
                    obj.Insurers = row["Insurers"].ToString();
                    obj.Address = row["Address"].ToString();
                    obj.SurveyVisitInitiatedDT = Convert.ToDateTime(row["SurveyVisitInitiatedDT"]);
                    obj.SurveyVisitCompletedDT = Convert.ToDateTime(row["SurveyVisitCompletedDT"]);
                    obj.ReasonDelaySurvey = row["ReasonDelaySurvey"].ToString();
                    obj.InsuredName = row["InsuredName"].ToString();
                    obj.InsuredAddress = row["InsuredAddress"].ToString();
                    obj.PS_ALL = row["PS_ALL"].ToString();
                    obj.PS_ALL_Place = row["PS_ALL_Place"].ToString();
                    obj.Loss = row["Loss"].ToString();
                    obj.LossDT =Convert.ToDateTime(row["LossDT"]);
                    obj.CL_OP = row["CL_OP"].ToString();
                    obj.CL_OP_ID = row["CL_OP_ID"].ToString();
                    list.Add(obj);
                }
            }

            return list.FirstOrDefault();
        }
        #endregion
        #region Aspect Liability
        public int saveAspectLiab(CSMISCISVRAspectLiab al)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AL_ID", al.PK_AL_ID), new SqlParameter("@ClaimID", al.ClaimID), new SqlParameter("@UserID", al.UserID), new SqlParameter("@CmtLiabSPID", al.CmtLiabSPID), new SqlParameter("@EstmateLossIns", al.EstmateLossIns), new SqlParameter("@LossReserveRecom", al.LossReserveRecom), new SqlParameter("@BscLossReserveRecom", al.BscLossReserveRecom), new SqlParameter("@ObsrvSiteLoss", al.ObsrvSiteLoss), new SqlParameter("@RemarkAdvID", al.RemarkAdvID), new SqlParameter("@BLankText", al.BLankText), new SqlParameter("@CmtRelPolCond", al.CmtRelPolCond), new SqlParameter("@CircumLoss", al.CircumLoss), new SqlParameter("@NoteOccrStEye ", al.Noteoccrsteye), };
            return _sf.executeNonQueryWithProc("uspMISCISVRAspectsLiabSave", _param);
        }
      
        public List<CSMISCCmtLiabSP> getCmtLiabSP()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCmtLiabSPGet");
            List<CSMISCCmtLiabSP> list = new List<CSMISCCmtLiabSP>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCmtLiabSP cmt = new CSMISCCmtLiabSP();
                    cmt.CmtLiabSPID = Convert.ToInt32(row["CmtLiabSPID"]);
                    cmt.CmtLiabSPData = Convert.ToString(row["CmtLiabSP"]);
                    list.Add(cmt);
                }
            }
            return list;
        }
      
        public List<CSMISCRemarkAdv> getRemarkAdv()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCRemarkAdvDDGet");
            List<CSMISCRemarkAdv> listRA = new List<CSMISCRemarkAdv>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCRemarkAdv ra = new CSMISCRemarkAdv();
                    ra.RemarkAdvID = Convert.ToInt32(row["RemarkAdvID"]);
                    ra.RemarkAdv = Convert.ToString(row["RemarkAdv"]);
                    listRA.Add(ra);
                }
            }
            return listRA;
        }
        public CSMISCISVRAspectLiab getAspectsLiab(int ClaimID, int UserID)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", ClaimID), new SqlParameter("@UserID", UserID) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCISVRAspectsLiabGet", _param);

            CSMISCISVRAspectLiab AL = new CSMISCISVRAspectLiab();
            if (_dt.Rows.Count > 0)
            {
                AL.PK_AL_ID = Convert.ToInt32(_dt.Rows[0]["PK_AL_ID"]);
                AL.CmtLiabSPID = Convert.ToInt32(_dt.Rows[0]["CmtLiabSPID"]);
                AL.EstmateLossIns = Convert.ToDecimal(_dt.Rows[0]["EstmateLossIns"]);
                AL.LossReserveRecom = Convert.ToDecimal(_dt.Rows[0]["LossReserveRecom"]);
                AL.BscLossReserveRecom = _dt.Rows[0]["BscLossReserveRecom"].ToString();
                AL.ObsrvSiteLoss = _dt.Rows[0]["ObsrvSiteLoss"].ToString();
                AL.RemarkAdvID = _dt.Rows[0]["RemarkAdvID"].ToString();
                AL.BLankText = _dt.Rows[0]["BLankText"].ToString();
                AL.CmtRelPolCond = _dt.Rows[0]["CmtRelPolCond"].ToString();
                AL.CircumLoss = _dt.Rows[0]["CircumLoss"].ToString();
                AL.Noteoccrsteye = _dt.Rows[0]["Noteoccrsteye"].ToString();
            }
            return AL;
        }
        #endregion
        #region Nature Extent Damage
        public int saveNatureExtentDamage(CSMISCISVRNatureExtentDamage ned)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_NED_ID", ned.PK_NED_ID), new SqlParameter("@ClaimID", ned.ClaimID), new SqlParameter("UserID", ned.UserID), new SqlParameter("@DescOfAffProp", ned.DescOfAffProp), new SqlParameter("@IdtyOfAffProp", ned.IdtyOfAffProp), new SqlParameter("@NatureOfDmgLsID", ned.NatureOfDmgLsID), new SqlParameter("@CovUndMenID", ned.CovUndMenID), new SqlParameter("@SumInsured", ned.SumInsured) };
            return _sf.executeNonQueryWithProc("uspMISCISVRNatureExtentDamageSave", _param);
        }
       
        
        public List<CSMISCNatureDmgls> getDmgls()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCNatureDmglsGet");

            List<CSMISCNatureDmgls> list = new List<CSMISCNatureDmgls>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCNatureDmgls obj = new CSMISCNatureDmgls();
                    obj.NatureOfDmgLsID = Convert.ToInt32(row["NatureOfDmgLsID"]);
                    obj.NatureOfDmgLs = Convert.ToString(row["NatureOfDmgLs"]);
                    list.Add(obj);
                }
            }
            return list;
        }
        public List<CSMISCCovMen> getCovMen()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCovMenGet");
           
            List<CSMISCCovMen> listCM = new List<CSMISCCovMen>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCovMen cm = new CSMISCCovMen();
                    cm.CovUndMenID = Convert.ToInt32(row["CovUndMenID"]);
                    cm.CovUndMen = Convert.ToString(row["CovUndMen"]);
                    listCM.Add(cm);
                }
            }
            return listCM;
        }
        public List<CSMISCISVRNatureExtentDamage> getNatureExtentDamage(int claimId,int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCISVRNatureExtentDamageGet", _param);
            
            List<CSMISCISVRNatureExtentDamage> listNED = new List<CSMISCISVRNatureExtentDamage>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCISVRNatureExtentDamage ned = new CSMISCISVRNatureExtentDamage();
                    ned.PK_NED_ID = Convert.ToInt32(row["PK_NED_ID"]);
                    ned.DescOfAffProp = Convert.ToString(row["DescOfAffProp"]);
                    ned.IdtyOfAffProp = Convert.ToString(row["IdtyOfAffProp"]);
                    ned.NatureOfDmgLsID = Convert.ToInt32(row["NatureOfDmgLsID"]);
                    ned.NatureOfDmgLs = Convert.ToString(row["NatureOfDmgLs"]);
                    ned.CovUndMenID = Convert.ToInt32(row["CovUndMenID"]);
                    ned.CovUndMen = Convert.ToString(row["CovUndMen"]);
                    ned.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    listNED.Add(ned);
                }
            }
            return listNED;
        }
        #endregion
    }
}

