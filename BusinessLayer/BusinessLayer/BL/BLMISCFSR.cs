﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{

    public class BLMISCFSR
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;

        public BLMISCFSR()
        {
            _sf = new SqlFunctions();
        }

        #region Insurance Particular

        public List<CSInsurers> getInsurers()
        {
            List<CSInsurers> listIns = new List<CSInsurers>();
            _dt = _sf.returnDTWithProc_executeReader("getInsurers");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSInsurers i = new CSInsurers();
                    i.PK_InsrID = Convert.ToInt32(row["PK_InsrID"]);
                    i.InsurerName = row["Name"].ToString();
                    listIns.Add(i);
                }
            }
            return listIns;
        }
        public int saveInsuranceParticulars(CSMISCFSRInsuranceParticulars ip)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCFSRIPRID", ip.PK_MISCFSRIPRID), new SqlParameter("@UserID", ip.UserID), new SqlParameter("@ClaimID", ip.ClaimID), new SqlParameter("@AddressCommunication", ip.AddressCommunication), new SqlParameter("@FinancierInterest", ip.FinancierInterest), new SqlParameter("@AddCovers", ip.AddCovers), new SqlParameter("@ClausesAttached", ip.ClausesAttached), new SqlParameter("@ExcessApplicable", ip.ExcessApplicable), new SqlParameter("@CINYN", ip.CINYN), new SqlParameter("@PSIYN", ip.PSIYN) };
            return _sf.executeNonQueryWithProc("uspMISCFSRInsuranceParticularsSave", _param);
        }



        public CSMISCFSRInsuranceParticulars getInsuranceParticulars(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFSRInsuranceParticularsGet", _param);
            CSMISCFSRInsuranceParticulars ip = new CSMISCFSRInsuranceParticulars();
            if (_dt.Rows.Count > 0)
            {

                ip.PK_MISCFSRIPRID = Convert.ToInt32(_dt.Rows[0]["PK_MISCFSRIPRID"]);
                ip.AddressCommunication = Convert.ToString(_dt.Rows[0]["AddressCommunication"]);
                ip.FinancierInterest = _dt.Rows[0]["FinancierInterest"].ToString();
                ip.AddCovers = Convert.ToBoolean(_dt.Rows[0]["AddCovers"]);
                ip.ClausesAttached = Convert.ToBoolean(_dt.Rows[0]["ClausesAttached"]);
                ip.ExcessApplicable = Convert.ToString(_dt.Rows[0]["ExcessApplicable"]);
                ip.CINYN = _dt.Rows[0]["CINYN"].ToString();
                ip.PSIYN = _dt.Rows[0]["PSIYN"].ToString();
            }
            return ip;
        }
        public int saveCoInsurers(CSMISCCoInsurers cin)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", cin.UserID), new SqlParameter("@ClaimID", cin.ClaimID), new SqlParameter("@CoInsurersID", cin.CoInsurersID), new SqlParameter("@Percentage", cin.Percentage) };
            return _sf.executeNonQueryWithProc("uspMISCCoInsurersSave", _param);
        }
        public int savePolicySumInsured(CSMISCPolicySumInsured p)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_PSI_ID", p.PK_PSI_ID), new SqlParameter("@UserID", p.UserID), new SqlParameter("@ClaimID", p.ClaimID), new SqlParameter("@Tb_ID", p.PK_Tb_ID), new SqlParameter("@SumInsured", p.SumInsured), new SqlParameter("@Description", p.Description) };
            return _sf.executeNonQueryWithProc("uspMISCPolicySumInsuredSave", _param);
        }
        public int saveRelWarCond(CSMISCRelWarCond r)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_RWC_ID", r.PK_RWC_ID), new SqlParameter("@UserID", r.UserID), new SqlParameter("@ClaimID", r.ClaimID), new SqlParameter("@RelWarCond", r.RelWarCond) };
            return _sf.executeNonQueryWithProc("uspMISCRelWarCondSave", _param);
        }
        public List<CSMISCCoInsurers> getCoInsurers(int claimId, int userId)
        {
            List<CSMISCCoInsurers> listCIn = new List<CSMISCCoInsurers>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCoInsurersGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCoInsurers c = new CSMISCCoInsurers();
                    c.PK_CoIns_ID = Convert.ToInt32(row["PK_CoIns_ID"]);
                    c.CoInsurersID = Convert.ToInt32(row["CoInsurersID"]);
                    c.Percentage = Convert.ToInt32(row["Percentage"]);
                    listCIn.Add(c);
                }
            }
            return listCIn;
        }
        public List<CSMISCPolicySumInsured> getPolicySumInsured(int claimId, int userId)
        {
            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("usp_get_MISCPolicySumInsured_byClaimID", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    listPSI.Add(p);
                }
            }
            return listPSI;
        }
        public List<CSMISCRelWarCond> getRelWarCond(int claimId, int userId)
        {
            List<CSMISCRelWarCond> listRWC = new List<CSMISCRelWarCond>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCRelWarCondGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCRelWarCond r = new CSMISCRelWarCond();
                    r.PK_RWC_ID = Convert.ToInt32(row["PK_RWC_ID"]);
                    r.RelWarCond = row["RelWarCond"].ToString();
                    listRWC.Add(r);
                }
            }
            return listRWC;
        }
        public int deleteCoInsurers(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMISCCoInsurersDelete", _param);
        }
        public int deletePolicySumInsured(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMISCPolicySumInsuredDelete", _param);
        }
        public int deleteRelWarCond(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMISCRelWarCondDelete", _param);
        }
        public List<CSMISCPolicySumInsured> getPolicySumInsuredAndTableName(int claimId, int userId)
        {

            List<CSMISCPolicySumInsured> listPT = new List<CSMISCPolicySumInsured>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCPolicySumInsuredAndTableNameGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.PK_Tb_ID = Convert.ToInt32(row["PK_Tb_ID"]);
                    //p.Tb_ID= Convert.ToInt32(row["Tb_ID"]);
                    p.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    p.Description = Convert.ToString(row["Description"]);
                    //p.ShortName = Convert.ToString(row["ShortName"]);

                    listPT.Add(p);
                }
            }
            return listPT;
        }
        #endregion

        #region Observation Circumstances
        //public int saveObservationsCircumstances(CSMISCFSRObservationsCircumstances oc)
        //{
        //    _param = new SqlParameter[] {new SqlParameter("@PK_OBSCIRID",oc.PK_OBSCIRID),
        //        new SqlParameter("@UserID", oc.UserID), new SqlParameter("@ClaimID", oc.ClaimID), new SqlParameter("@ObsFirstVisit", oc.ObsFirstVisit), new SqlParameter("@ObsSecVisit", oc.ObsSecVisit), new SqlParameter("@Quote", oc.Quote), new SqlParameter("@UnQuoteTrans", oc.UnQuoteTrans), new SqlParameter("@Photo", oc.Photo), new SqlParameter("@NewsTrans", oc.NewsTrans) };
        //    return _sf.executeNonQueryWithProc("uspMISCFSRObservationsCircumstancesSave", _param);
        //}
        public int saveObservationsCircumstances(CSMISCFSRObservationsCircumstances oc)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_OBSCIRID", oc.PK_OBSCIRID), new SqlParameter("@UserID", oc.UserID), new SqlParameter("@ClaimID", oc.ClaimID), new SqlParameter("@CLQuoteText", oc.CLQuoteText), new SqlParameter("@CLQuotePhotoDoc", oc.CLQuotePhotoDoc), new SqlParameter("@UnQuoteText", oc.UnQuoteText), new SqlParameter("@ChkboxCLTrans", oc.ChkboxCLTrans), new SqlParameter("@CLTrans", oc.CLTrans), new SqlParameter("@ChkboxNewsPaper", oc.ChkboxNewsPaper), new SqlParameter("@NameNewsPaper", oc.NameNewsPaper), new SqlParameter("@NewsPubDT", oc.NewsPubDT), new SqlParameter("@NewsPhotoDoc", oc.NewsPhotoDoc), new SqlParameter("@ChkbxNPTrans", oc.ChkbxNPTrans), new SqlParameter("@NewsTrans", oc.NewsTrans), }; return _sf.executeNonQueryWithProc("uspMISCFSRObservationsCircumstancesSave", _param);
        }
        public CSMISCFSRObservationsCircumstances getObservationsCircumstances(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFSRObservationsCircumstancesGet", _param);
            CSMISCFSRObservationsCircumstances oc = new CSMISCFSRObservationsCircumstances();
            if (_dt.Rows.Count > 0)
            {
                oc.PK_OBSCIRID = Convert.ToInt32(_dt.Rows[0]["PK_OBSCIRID"]);
                oc.CLQuoteText = Convert.ToString(_dt.Rows[0]["CLQuoteText"]);
                oc.CLQuotePhotoDoc = Convert.ToString(_dt.Rows[0]["CLQuotePhotoDoc"]);
                oc.UnQuoteText = Convert.ToString(_dt.Rows[0]["UnQuoteText"]);
                oc.ChkboxCLTrans = Convert.ToBoolean(_dt.Rows[0]["ChkboxCLTrans"]);
                oc.CLTrans = Convert.ToString(_dt.Rows[0]["CLTrans"]);
                oc.ChkboxNewsPaper = Convert.ToBoolean(_dt.Rows[0]["ChkboxNewsPaper"]);
                oc.NameNewsPaper = Convert.ToString(_dt.Rows[0]["NameNewsPaper"]);
                oc.NewsPubDT = Convert.ToString(_dt.Rows[0]["NewsPubDT"]);
                oc.NewsPhotoDoc = Convert.ToString(_dt.Rows[0]["NewsPhotoDoc"]);
                oc.ChkbxNPTrans = Convert.ToBoolean(_dt.Rows[0]["ChkbxNPTrans"]);
                oc.NewsTrans = Convert.ToString(_dt.Rows[0]["NewsTrans"]);

            }
            return oc;
        }
        #endregion
        #region SurveyParticulars
        public int saveSurveyParticulars(CSMISCFSRSurveyParticulars sp)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCFSRSURPRTID", sp.PK_MISCFSRSURPRTID), new SqlParameter("@UserID", sp.UserID), new SqlParameter("@ClaimID", sp.ClaimID), new SqlParameter("@FirstSurveyVisitInitDate", sp.FirstSurveyVisitInitDate), new SqlParameter("@FirstSurveyVisitInitTime", sp.FirstSurveyVisitInitTime), new SqlParameter("@FirstSurveyVisitCompDate", sp.FirstSurveyVisitCompDate), new SqlParameter("@FirstSurveyVisitCompTime", sp.FirstSurveyVisitCompTime), new SqlParameter("@ReasonDelaySurveyVisit", sp.ReasonDelaySurveyVisit) };
            return _sf.executeNonQueryWithProc("uspMISCFSRSurveyParticularsSave", _param);
        }
        //public string getInstructorNameByClaimID(int claimId)
        //{
        //    _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
        //    return (string)_sf.executeScalerWithProc("uspGeneratedClaimsGetInstructorNameByClaimID", _param);
        //}
        public CSMISCFSRSurveyParticulars getSurveyParticulars(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFSRSurveyParticularsGet", _param);
            CSMISCFSRSurveyParticulars sp = new CSMISCFSRSurveyParticulars();
            if (_dt.Rows.Count > 0)
            {
                sp.PK_MISCFSRSURPRTID = Convert.ToInt32(_dt.Rows[0]["PK_MISCFSRSURPRTID"]);
                sp.FirstSurveyVisitInitDate = _dt.Rows[0]["FirstSurveyVisitInitDate"].ToString();
                sp.FirstSurveyVisitInitTime = _dt.Rows[0]["FirstSurveyVisitInitTime"].ToString();
                sp.FirstSurveyVisitCompDate = _dt.Rows[0]["FirstSurveyVisitCompDate"].ToString();
                sp.FirstSurveyVisitCompTime = _dt.Rows[0]["FirstSurveyVisitCompTime"].ToString();
                sp.ReasonDelaySurveyVisit = Convert.ToString(_dt.Rows[0]["ReasonDelaySurveyVisit"]);
            }
            return sp;
        }
        public CSMISCFSRSurveyParticulars getInstructorNameInstructionDateByClaimID(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspGeneratedClaimsGetInstNameInsDateByClaimID", _param);
            CSMISCFSRSurveyParticulars gc = new CSMISCFSRSurveyParticulars();
            if (_dt.Rows.Count > 0)
            {
                gc.InstructionDT = Convert.ToDateTime(_dt.Rows[0]["InstructionDate"]);
                gc.SourceInsInstructorSurvey = _dt.Rows[0]["InstructorName"].ToString();
            }
            return gc;
        }
        #endregion
        #region Survey Visit
        public int saveSurveyVisit(CSSurveyorVisits sv)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_ID", sv.PkId), new SqlParameter("@FK_ClaimID", sv.FkClaimId), new SqlParameter("@StartDate", sv.StartDate), new SqlParameter("@EndDate", sv.EndDate), new SqlParameter("@PlaceVisit", sv.PlaceVisit), new SqlParameter("@PurposeVisit", sv.PurposeVisit), new SqlParameter("@Remark", sv.Remark), new SqlParameter("@EntryBy", sv.EntryBy), new SqlParameter("@IPAddress", sv.IPAddress), new SqlParameter("@FromTableName", sv.FromTableName) }; return _sf.executeNonQueryWithProc("uspMarineSurveyorVisitsSave", _param);
        }

        public List<CSSurveyorVisits> getSurveyVisit(int userId, int claimId, string tableName)
        {
            _param = new SqlParameter[] { new SqlParameter("@EntryBy", userId), new SqlParameter("@FK_ClaimID", claimId), new SqlParameter("@FromTableName", tableName) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineSurveyorVisitsGet", _param);
            List<CSSurveyorVisits> list = new List<CSSurveyorVisits>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSSurveyorVisits sv = new CSSurveyorVisits();
                    sv.PkId = Convert.ToInt32(row["PK_Id"]);
                    sv.StartDate = row["StartDate"].ToString();
                    sv.EndDate = row["EndDate"].ToString();
                    sv.PlaceVisit = row["PlaceVisit"].ToString();
                    sv.PurposeVisit = row["PurposeVisit"].ToString();
                    sv.Remark = row["Remark"].ToString();
                    list.Add(sv);
                }
            }
            return list;
        }
        public int deleteSurveyVisit(int pkId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_ID", pkId), new SqlParameter("@EntryBy", userId) };
            return _sf.executeNonQueryWithProc("uspMarineSurveyorVisitsDelete", _param);

        }
        #endregion


        #region IntroClaim

        public List<CSMISCCL_OP> getCL_OP()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCL_OPGet");
            List<CSMISCCL_OP> list = new List<CSMISCCL_OP>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCL_OP obj = new CSMISCCL_OP();
                    obj.CL_OP_ID = Convert.ToInt32(row["CL_OP_ID"]);
                    obj.CL_OP = Convert.ToString(row["CL_OP"]);
                    list.Add(obj);
                }
            }
            return list;
        }
        //public int saveIntroClaim(CSMISCFSRIntroClaim ic)
        //{
        //    _param = new SqlParameter[] { new SqlParameter("@PK_MISCFSRIntroID", ic.PK_MISCFSRIntroID), new SqlParameter("@PK_MISCFSRIPRID", ic.PK_MISCFSRIPRID), new SqlParameter("@UserID", ic.UserID), new SqlParameter("@ClaimID", ic.ClaimID), new SqlParameter("@Loss", ic.Loss), new SqlParameter("@LossDT", ic.LossDT), new SqlParameter("@CmtLiabSPID", ic.CmtLiabSPID), new SqlParameter("@FinalClaimInsured", ic.FinalClaimInsured), new SqlParameter("@PeriodofPolicyFromDT", ic.PeriodofPolicyFromDT), new SqlParameter("@PeriodofPolicyToDT", ic.PeriodofPolicyToDT) };
        //    return _sf.executeNonQueryWithProc("uspMISCFSRIntroClaimSave", _param);
        //}
        public int saveIntroClaim(CSMISCFSRIntroClaim ic)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCFSRIntroID", ic.PK_MISCFSRIntroID), new SqlParameter("@PK_MISCFSRIPRID", ic.PK_MISCFSRIPRID), new SqlParameter("@UserID", ic.UserID), new SqlParameter("@ClaimID", ic.ClaimID), new SqlParameter("@PreviousReports", ic.PreviousReports), new SqlParameter("@IntimationLossDT", ic.IntimationLossDT), new SqlParameter("@AllotmentSurveyDT", ic.AllotmentSurveyDT), new SqlParameter("@SourceAllotmentSurvey", ic.SourceAllotmentSurvey), new SqlParameter("@SurveyVisitInitiatedDT", ic.SurveyVisitInitiatedDT), new SqlParameter("@SurveyVisitCompletedDT", ic.SurveyVisitCompletedDT), new SqlParameter("@ReasonDelaySurvey", ic.ReasonDelaySurvey), new SqlParameter("@PolicyIssueofficeAddress", ic.PolicyIssueofficeAddress), new SqlParameter("@Loss", ic.Loss), new SqlParameter("@LossDT", ic.LossDT), new SqlParameter("@CmtLiabSPID", ic.CmtLiabSPID), new SqlParameter("@FinalClaimInsured", ic.FinalClaimInsured), new SqlParameter("@PeriodofPolicyFromDT", ic.PeriodofPolicyFromDT), new SqlParameter("@PeriodofPolicyToDT", ic.PeriodofPolicyToDT) }; return _sf.executeNonQueryWithProc("uspMISCFSRIntroClaimSave", _param);
        }

        public CSMISCFSRIntroClaim getIntroClaim(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFSRIntroClaimGet", _param);
            CSMISCFSRIntroClaim ic = new CSMISCFSRIntroClaim();
            if (_dt.Rows.Count > 0)
            {

                ic.PK_MISCFSRIntroID = Convert.ToInt32(_dt.Rows[0]["PK_MISCFSRIntroID"]);
                //ic.UserID = Convert.ToInt32(_dt.Rows[0]["UserID"]);
                //ic.ClaimID = Convert.ToInt32(_dt.Rows[0]["ClaimID"]);
                ic.PreviousReports = Convert.ToString(_dt.Rows[0]["PreviousReports"]);
                ic.IntimationLossDT = _dt.Rows[0]["IntimationLossDT"].ToString();
                ic.AllotmentSurveyDT = _dt.Rows[0]["AllotmentSurveyDT"].ToString();
                ic.SourceAllotmentSurvey = _dt.Rows[0]["SourceAllotmentSurvey"].ToString();
                ic.SurveyVisitInitiatedDT = _dt.Rows[0]["SurveyVisitInitiatedDT"].ToString();
                ic.SurveyVisitCompletedDT = _dt.Rows[0]["SurveyVisitCompletedDT"].ToString();
                ic.ReasonDelaySurvey = Convert.ToString(_dt.Rows[0]["ReasonDelaySurvey"]);
                ic.PolicyIssueofficeAddress = Convert.ToString(_dt.Rows[0]["PolicyIssueofficeAddress"]);
                ic.Loss = Convert.ToString(_dt.Rows[0]["Loss"]);
                ic.LossDT = _dt.Rows[0]["LossDT"].ToString();
                //ic.CL_OP = Convert.ToString(_dt.Rows[0]["CL_OP"]);
                //ic.CL_OPID = Convert.ToInt32(_dt.Rows[0]["CL_OPID"]);
                ic.CmtLiabSPID = Convert.ToInt32(_dt.Rows[0]["CmtLiabSPID"]);
                ic.FinalClaimInsured = Convert.ToDecimal(_dt.Rows[0]["FinalClaimInsured"]);
                //ic.GrossLossAssessed = Convert.ToString(_dt.Rows[0]["GrossLossAssessed"]);
                //ic.GrossLossAdmissible = Convert.ToString(_dt.Rows[0]["GrossLossAdmissible"]);
                //ic.NetLossAssessed = Convert.ToString(_dt.Rows[0]["NetLossAssessed"]);
                //ic.NetAdjustedLoss = Convert.ToString(_dt.Rows[0]["NetAdjustedLoss"]);
                //ic.AccPayRecEarReports = Convert.ToString(_dt.Rows[0]["AccPayRecEarReports"]);
                //ic.NetSetAmtRecPay = Convert.ToString(_dt.Rows[0]["NetSetAmtRecPay"]);
                //ic.AddressofLossLocation = _dt.Rows[0]["AddressofLossLocation"].ToString();
                //ic.CL_OP_Name = _dt.Rows[0]["CL_OP_Name"].ToString();
                //ic.CL_OP_ShortName = _dt.Rows[0]["CL_OP_ShortName"].ToString();
                //ic.PeriodofPolicyFromDT = Convert.ToDateTime(_dt.Rows[0]["PeriodofPolicyFromDT"]);
                //ic.PeriodofPolicyToDate = Convert.ToDateTime(_dt.Rows[0]["PeriodofPolicyToDate"]);

            }
            return ic;
        }

        public List<CSMISCCmtLiabSP> getCmtLiabSP()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCmtLiabSPGet");

            List<CSMISCCmtLiabSP> list = new List<CSMISCCmtLiabSP>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCCmtLiabSP obj = new CSMISCCmtLiabSP();
                    obj.CmtLiabSPID = Convert.ToInt32(row["CmtLiabSPID"]);
                    obj.CmtLiabSPData = Convert.ToString(row["CmtLiabSP"]);
                    list.Add(obj);
                }
            }
            return list;
        }
        public CSMISCFSRIntroClaim getMISCCommonDataGet(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCCommonDataGet", _param);
            CSMISCFSRIntroClaim ic = new CSMISCFSRIntroClaim();
            if (_dt.Rows.Count > 0)
            {
                ic.ClaimID = claimId;

                ic.PK_MISCFSRIPRID = Convert.ToInt32(_dt.Rows[0]["PK_MISCFSRIPRID"]);
                ic.InsurersClaimNo = Convert.ToString(_dt.Rows[0]["InsurersClaimNo"]);
                ic.PolicyNo = Convert.ToString(_dt.Rows[0]["PolicyNo"]);
                ic.PolicyType = Convert.ToString(_dt.Rows[0]["PolicyType"]);
                ic.PeriodofPolicyFromDT = _dt.Rows[0]["PeriodofPolicyFromDT"].ToString();
                ic.PeriodofPolicyToDT = _dt.Rows[0]["PeriodofPolicyToDT"].ToString();

                ic.Insurers = Convert.ToString(_dt.Rows[0]["InsurerName"]);
                ic.InsurersAddress = Convert.ToString(_dt.Rows[0]["InsurersAddress"]);

                ic.InsuredName = Convert.ToString(_dt.Rows[0]["InsuredName"]);
                ic.InsuredAddress = Convert.ToString(_dt.Rows[0]["InsuredAddress"]);
                ic.ReportedLoss = Convert.ToDecimal(_dt.Rows[0]["ReportedLoss"]);
                ic.FinancierInterest = _dt.Rows[0]["FinancierInterest"].ToString();
                ic.NatureOfLoss = _dt.Rows[0]["NatureOfLoss"].ToString();
                ic.AddressofLossLocation = _dt.Rows[0]["LocationOfSurvey"].ToString();
            }
            return ic;
        }
        #endregion
        #region About Insured

        public int saveAboutInsured(CSMISCAboutInsured ai)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCABTINSID", ai.PK_MISCABTINSID), new SqlParameter("@UserID", ai.UserID), new SqlParameter("@ClaimID", ai.ClaimID), new SqlParameter("@NoteBusinessProfile", ai.NoteBusinessProfile), new SqlParameter("@ChkbxFS", ai.ChkbxFS), new SqlParameter("@InsuredBalText", ai.InsuredBalText), new SqlParameter("@BusinessGrowthText", ai.BusinessGrowthText), new SqlParameter("@DescriptionsSite", ai.DescriptionsSite), new SqlParameter("@ChkbxLayoutSite", ai.ChkbxLayoutSite), new SqlParameter("@LayoutSitePhotoDoc", ai.LayoutSitePhotoDoc), }; return _sf.executeNonQueryWithProc("uspMISCAboutInsuredSave", _param);
        }
        public int saveFinancialStatus(CSMISCFinancialStatus fs)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCFINSTID", fs.PK_MISCFINSTID), new SqlParameter("@UserID", fs.UserID), new SqlParameter("@ClaimID", fs.ClaimID), new SqlParameter("@FinancialYear", fs.FinancialYear), new SqlParameter("@SalesTurnover", fs.SalesTurnover), new SqlParameter("@GrossProfitRs", fs.GrossProfitRs), new SqlParameter("@GrossProfitPerc", fs.GrossProfitPerc), new SqlParameter("@NetProfitRs", fs.NetProfitRs), new SqlParameter("@NetProfitPerc", fs.NetProfitPerc), new SqlParameter("@ClosingStock", fs.ClosingStock), new SqlParameter("@RowName", fs.RowName), };
            return _sf.executeNonQueryWithProc("uspMISCFinancialStatusSave", _param);
        }
        public CSMISCAboutInsured getAboutInsured(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAboutInsuredGet", _param);
            CSMISCAboutInsured ai = new CSMISCAboutInsured();
            if (_dt.Rows.Count > 0)
            {
                ai.PK_MISCABTINSID = Convert.ToInt32(_dt.Rows[0]["PK_MISCABTINSID"]);
                ai.NoteBusinessProfile = Convert.ToString(_dt.Rows[0]["NoteBusinessProfile"]);
                ai.ChkbxFS = Convert.ToBoolean(_dt.Rows[0]["ChkbxFS"]);
                ai.InsuredBalText = Convert.ToString(_dt.Rows[0]["InsuredBalText"]);
                ai.BusinessGrowthText = Convert.ToString(_dt.Rows[0]["BusinessGrowthText"]);
                ai.DescriptionsSite = Convert.ToString(_dt.Rows[0]["DescriptionsSite"]);
                ai.ChkbxLayoutSite = Convert.ToBoolean(_dt.Rows[0]["ChkbxLayoutSite"]);
                ai.LayoutSitePhotoDoc = Convert.ToString(_dt.Rows[0]["LayoutSitePhotoDoc"]);
            }
            return ai;
        }
        public List<CSMISCFinancialStatus> getFinancialStatus(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFinancialStatusGet", _param);
            List<CSMISCFinancialStatus> list = new List<CSMISCFinancialStatus>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow rows in _dt.Rows)
                {
                    CSMISCFinancialStatus fs = new CSMISCFinancialStatus();
                    // fs.UserID = Convert.ToInt32(rows["UserID"]);
                    //fs.ClaimID = Convert.ToInt32(rows["ClaimID"]);
                    fs.PK_MISCFINSTID = Convert.ToInt32(rows["PK_MISCFINSTID"]);
                    fs.FinancialYear = Convert.ToString(rows["FinancialYear"]);
                    fs.SalesTurnover = Convert.ToDecimal(rows["SalesTurnover"]);
                    fs.GrossProfitRs = Convert.ToDecimal(rows["GrossProfitRs"]);
                    fs.GrossProfitPerc = Convert.ToDecimal(rows["GrossProfitPerc"]);
                    fs.NetProfitRs = Convert.ToDecimal(rows["NetProfitRs"]);
                    fs.NetProfitPerc = Convert.ToDecimal(rows["NetProfitPerc"]);
                    fs.ClosingStock = Convert.ToDecimal(rows["ClosingStock"]);
                    fs.RowName = Convert.ToString(rows["RowName"]);
                    list.Add(fs);

                }
            }
            return list;
        }
        public int deleteFinancialStatus(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMISCFinancialStatusDelete", _param);
        }
        #endregion
        #region About Cause Of Loss
        public int saveAboutCauseOfLoss(CSMISCAboutCauseOfLoss acl)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCACL", acl.PK_MISCACL), new SqlParameter("@UserID", acl.UserID), new SqlParameter("@ClaimID", acl.ClaimID), new SqlParameter("@Time", acl.Time), new SqlParameter("@Day", acl.Day), new SqlParameter("@Date", acl.Date), new SqlParameter("@NL_ID", acl.NL_ID), new SqlParameter("@RootCauseLoss", acl.RootCauseLoss), new SqlParameter("@OriginOperatedPeril", acl.OriginOperatedPeril), new SqlParameter("@EffectOperatedPeril_ID", acl.EffectOperatedPeril_ID), new SqlParameter("@StatutoryAuthorities_ID", acl.StatutoryAuthorities_ID), new SqlParameter("@InsuredSubmission", acl.InsuredSubmission), new SqlParameter("@PublishedPrintMedia", acl.PublishedPrintMedia), new SqlParameter("@CapturedCCTVCamera", acl.CapturedCCTVCamera), new SqlParameter("@ItemWisePhotoAlbum", acl.ItemWisePhotoAlbum), new SqlParameter("@TravellingFire", acl.TravellingFire), new SqlParameter("@FireContinuedHowManyHours", acl.FireContinuedHowManyHours), new SqlParameter("@ImmediateActionControlFire", acl.ImmediateActionControlFire), new SqlParameter("@FurtherActionControlFire", acl.FurtherActionControlFire), new SqlParameter("@WhoInformedFireBrigade", acl.WhoInformedFireBrigade), new SqlParameter("@TimeCallingFileBrigade", acl.TimeCallingFileBrigade), new SqlParameter("@DistanceSiteAndFireStation", acl.DistanceSiteAndFireStation), new SqlParameter("@NameFireStation", acl.NameFireStation), new SqlParameter("@TimeFirstFireTenderArriving", acl.TimeFirstFireTenderArriving), new SqlParameter("@HowManyFireTendersUsed", acl.HowManyFireTendersUsed), new SqlParameter("@TimeWhenLastFireTender", acl.TimeWhenLastFireTender), new SqlParameter("@MeasureWaterLevelOutside", acl.MeasureWaterLevelOutside), new SqlParameter("@MeasureWaterLevelInside", acl.MeasureWaterLevelInside), new SqlParameter("@WasInsuredPropSubmergre", acl.WasInsuredPropSubmergre), new SqlParameter("@RecordRainfall", acl.RecordRainfall), new SqlParameter("@HoursInsuredPremises", acl.HoursInsuredPremises), new SqlParameter("@LongInsuredAreaAffected", acl.LongInsuredAreaAffected), new SqlParameter("@WindSpeedRecordedMetDept", acl.WindSpeedRecordedMetDept), new SqlParameter("@EntryExitUsedCulprits", acl.EntryExitUsedCulprits), new SqlParameter("@ForcibleEntryAvailable", acl.ForcibleEntryAvailable), new SqlParameter("@TypeMarksObserved", acl.TypeMarksObserved), new SqlParameter("@SizeEntryExitCulprits", acl.SizeEntryExitCulprits) };
            return _sf.executeNonQueryWithProc("uspMISCAboutCauseOfLossSave", _param);
        }
        public CSMISCAboutCauseOfLoss getAboutCauseOfLoss(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@claimId", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAboutCauseOfLossGet", _param);
            CSMISCAboutCauseOfLoss acl = new CSMISCAboutCauseOfLoss();
            if (_dt.Rows.Count > 0)
            {
                acl.PK_MISCACL = Convert.ToInt32(_dt.Rows[0]["PK_MISCACL"]);
                acl.Time = Convert.ToString(_dt.Rows[0]["Time"]);
                acl.Day = Convert.ToString(_dt.Rows[0]["Day"]);
                acl.Date = Convert.ToString(_dt.Rows[0]["Date"]);
                //acl.LocationLoss = Convert.ToString(_dt.Rows[0]["LocationLoss"]);
                //acl.NatureLoss = Convert.ToString(_dt.Rows[0]["NatureLoss"]);
                acl.NL_ID = Convert.ToInt32(_dt.Rows[0]["NL_ID"]);
                acl.RootCauseLoss = Convert.ToString(_dt.Rows[0]["RootCauseLoss"]);
                acl.OriginOperatedPeril = Convert.ToString(_dt.Rows[0]["OriginOperatedPeril"]);
                acl.EffectOperatedPeril_ID = Convert.ToInt32(_dt.Rows[0]["EffectOperatedPeril_ID"]);
                acl.StatutoryAuthorities_ID = Convert.ToString(_dt.Rows[0]["StatutoryAuthorities_ID"]);
                acl.InsuredSubmission = Convert.ToString(_dt.Rows[0]["InsuredSubmission"]);
                acl.PublishedPrintMedia = Convert.ToString(_dt.Rows[0]["PublishedPrintMedia"]);
                acl.CapturedCCTVCamera = Convert.ToString(_dt.Rows[0]["CapturedCCTVCamera"]);
                acl.ItemWisePhotoAlbum = Convert.ToString(_dt.Rows[0]["ItemWisePhotoAlbum"]);
                acl.TravellingFire = Convert.ToString(_dt.Rows[0]["TravellingFire"]);
                acl.FireContinuedHowManyHours = Convert.ToString(_dt.Rows[0]["FireContinuedHowManyHours"]);
                acl.ImmediateActionControlFire = Convert.ToString(_dt.Rows[0]["ImmediateActionControlFire"]);
                acl.FurtherActionControlFire = Convert.ToString(_dt.Rows[0]["FurtherActionControlFire"]);
                //acl.WasFireBrigadeInformed = Convert.ToString(_dt.Rows[0]["WasFireBrigadeInformed"]);
                acl.WhoInformedFireBrigade = Convert.ToString(_dt.Rows[0]["WhoInformedFireBrigade"]);
                acl.TimeCallingFileBrigade = Convert.ToString(_dt.Rows[0]["TimeCallingFileBrigade"]);
                acl.DistanceSiteAndFireStation = Convert.ToDecimal(_dt.Rows[0]["DistanceSiteAndFireStation"]);
                acl.NameFireStation = Convert.ToString(_dt.Rows[0]["NameFireStation"]);
                acl.TimeFirstFireTenderArriving = Convert.ToString(_dt.Rows[0]["TimeFirstFireTenderArriving"]);
                acl.HowManyFireTendersUsed = Convert.ToDecimal(_dt.Rows[0]["HowManyFireTendersUsed"]);
                acl.TimeWhenLastFireTender = Convert.ToString(_dt.Rows[0]["TimeWhenLastFireTender"]);
                acl.MeasureWaterLevelOutside = Convert.ToString(_dt.Rows[0]["MeasureWaterLevelOutside"]);
                acl.MeasureWaterLevelInside = Convert.ToString(_dt.Rows[0]["MeasureWaterLevelInside"]);
                acl.WasInsuredPropSubmergre = Convert.ToString(_dt.Rows[0]["WasInsuredPropSubmergre"]);
                acl.RecordRainfall = Convert.ToString(_dt.Rows[0]["RecordRainfall"]);
                acl.HoursInsuredPremises = Convert.ToString(_dt.Rows[0]["HoursInsuredPremises"]);
                acl.LongInsuredAreaAffected = Convert.ToString(_dt.Rows[0]["LongInsuredAreaAffected"]);
                acl.WindSpeedRecordedMetDept = Convert.ToString(_dt.Rows[0]["WindSpeedRecordedMetDept"]);
                acl.EntryExitUsedCulprits = Convert.ToString(_dt.Rows[0]["EntryExitUsedCulprits"]);
                acl.ForcibleEntryAvailable = Convert.ToString(_dt.Rows[0]["ForcibleEntryAvailable"]);
                acl.TypeMarksObserved = Convert.ToString(_dt.Rows[0]["TypeMarksObserved"]);
                acl.SizeEntryExitCulprits = Convert.ToString(_dt.Rows[0]["SizeEntryExitCulprits"]);

            }
            return acl;
        }
        public List<CSMISCEffectofOperatedPeril> getEffectofOperatedPeril()
        {
            List<CSMISCEffectofOperatedPeril> listEOP = new List<CSMISCEffectofOperatedPeril>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCEffectofOperatedPerilGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCEffectofOperatedPeril cop = new CSMISCEffectofOperatedPeril();
                    cop.Pk_EOP_ID = Convert.ToInt32(row["Pk_EOP_ID"]);
                    cop.EffectofOperatedPeril = row["EffectofOperatedPeril"].ToString();
                    listEOP.Add(cop);
                }

            }
            return listEOP;
        }
        public List<CSMISCStatutoryAuthoritiesInformed> getStatutoryAuthoritiesInformed()
        {
            List<CSMISCStatutoryAuthoritiesInformed> listSAI = new List<CSMISCStatutoryAuthoritiesInformed>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCStatutoryAuthoritiesInformedGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCStatutoryAuthoritiesInformed sai = new CSMISCStatutoryAuthoritiesInformed();
                    sai.PK_SA_ID = Convert.ToInt32(row["PK_SA_ID"]);
                    sai.StatutoryAuthoritiesInformed = row["StatutoryAuthoritiesInformed"].ToString();
                    listSAI.Add(sai);
                }

            }
            return listSAI;
        }
        public List<CSMISC_ACL_NatureofLoss> getACL_NatureofLoss()
        {

            _dt = _sf.returnDTWithProc_executeReader("uspMISC_ACL_NatureofLossGet");
            List<CSMISC_ACL_NatureofLoss> listNL = new List<CSMISC_ACL_NatureofLoss>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISC_ACL_NatureofLoss nl = new CSMISC_ACL_NatureofLoss();
                    nl.PK_NL_ID = Convert.ToInt32(row["PK_NL_ID"]);
                    nl.NatureofLoss = row["NatureofLoss"].ToString();
                    nl.ShortName = row["ShortName"].ToString();
                    listNL.Add(nl);
                }

            }
            return listNL;
        }
        #endregion
        #region OtherReportsForMaterialFacts
        //public int saveOtherReportsForMaterialFacts(CSMISCOtherReportsForMaterialFacts ormf)
        //{
        //    _param = new SqlParameter[] { new SqlParameter("@PK_MISCORMFID", ormf.PK_MISCORMFID), new SqlParameter("@UserID", ormf.UserID), new SqlParameter("@ClaimID", ormf.ClaimID), 
        //    return _sf.executeNonQueryWithProc("uspMISCOtherReportsForMaterialFactsSave", _param);
        //}
        public int saveOtherReportsForMaterialFacts(CSMISCOtherReportsForMaterialFacts ormf)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", ormf.UserID), new SqlParameter("@ClaimID", ormf.ClaimID), new SqlParameter("@C_PR_Tran", ormf.C_PR_Tran), new SqlParameter("@C_Pr_IPC", ormf.C_Pr_IPC), new SqlParameter("@C_Pr_PRC", ormf.C_Pr_PRC), new SqlParameter("@C_Pr_Remarks", ormf.C_Pr_Remarks), new SqlParameter("@PoliceReportPhoto", ormf.PoliceReportPhoto), new SqlParameter("@Pr_Tran", ormf.Pr_Tran), new SqlParameter("@Pr_IPC_ID", ormf.Pr_IPC_ID), new SqlParameter("@Pr_PRC", ormf.Pr_PRC), new SqlParameter("@Pr_Remarks", ormf.Pr_Remarks), new SqlParameter("@C_Fr_Tran", ormf.C_Fr_Tran), new SqlParameter("@C_Fr_sumry", ormf.C_Fr_sumry), new SqlParameter("@C_Fr_Remarks", ormf.C_Fr_Remarks), new SqlParameter("@FireBrigadeReportPhoto", ormf.FireBrigadeReportPhoto), new SqlParameter("@Fr_Tran", ormf.Fr_Tran), new SqlParameter("@Fr_Sumry", ormf.Fr_Sumry), new SqlParameter("@Fr_Remarks", ormf.Fr_Remarks), new SqlParameter("@C_Mr_Tran", ormf.C_Mr_Tran), new SqlParameter("@C_Mr_Sumry", ormf.C_Mr_Sumry), new SqlParameter("@C_Mr_Remarks", ormf.C_Mr_Remarks), new SqlParameter("@MeteorologicalReportPhoto", ormf.MeteorologicalReportPhoto), new SqlParameter("@Mr_Tran", ormf.Mr_Tran), new SqlParameter("@Mr_Sumry", ormf.Mr_Sumry), new SqlParameter("@Mr_Remarks", ormf.Mr_Remarks), new SqlParameter("@C_Fslr_Tran", ormf.C_Fslr_Tran), new SqlParameter("@C_Fslr_Sumry", ormf.C_Fslr_Sumry), new SqlParameter("@C_Fslr_Remarks", ormf.C_Fslr_Remarks), new SqlParameter("@Fslr_Tran", ormf.Fslr_Tran), new SqlParameter("@Fslr_Sumry", ormf.Fslr_Sumry), new SqlParameter("@Fslr_Remarks", ormf.Fslr_Remarks), }; return _sf.executeNonQueryWithProc("uspMISCOtherReportsForMaterialFactsSave", _param);
        }
        public CSMISCOtherReportsForMaterialFacts getOtherReportsForMaterialFacts(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@claimId", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCOtherReportsForMaterialFactsGet", _param);
            CSMISCOtherReportsForMaterialFacts o = new CSMISCOtherReportsForMaterialFacts();
            if (_dt.Rows.Count > 0)
            {
                o.PK_MISCORMFID = Convert.ToInt32(_dt.Rows[0]["PK_MISCORMFID"]);
                o.UserID = Convert.ToInt32(_dt.Rows[0]["UserID"]);
                o.ClaimID = Convert.ToInt32(_dt.Rows[0]["ClaimID"]);
                o.C_PR_Tran = Convert.ToBoolean(_dt.Rows[0]["C_PR_Tran"]);
                o.C_Pr_IPC = Convert.ToBoolean(_dt.Rows[0]["C_Pr_IPC"]);
                o.C_Pr_PRC = Convert.ToBoolean(_dt.Rows[0]["C_Pr_PRC"]);
                o.C_Pr_Remarks = Convert.ToBoolean(_dt.Rows[0]["C_Pr_Remarks"]);
                o.PoliceReportPhoto = Convert.ToString(_dt.Rows[0]["PoliceReportPhoto"]);
                o.Pr_Tran = Convert.ToString(_dt.Rows[0]["Pr_Tran"]);
                o.Pr_IPC_ID = Convert.ToString(_dt.Rows[0]["Pr_IPC_ID"]);
                o.Pr_PRC = Convert.ToString(_dt.Rows[0]["Pr_PRC"]);
                o.Pr_Remarks = Convert.ToString(_dt.Rows[0]["Pr_Remarks"]);
                o.C_Fr_Tran = Convert.ToBoolean(_dt.Rows[0]["C_Fr_Tran"]);
                o.C_Fr_sumry = Convert.ToBoolean(_dt.Rows[0]["C_Fr_sumry"]);
                o.C_Fr_Remarks = Convert.ToBoolean(_dt.Rows[0]["C_Fr_Remarks"]);
                o.FireBrigadeReportPhoto = Convert.ToString(_dt.Rows[0]["FireBrigadeReportPhoto"]);
                o.Fr_Tran = Convert.ToString(_dt.Rows[0]["Fr_Tran"]);
                o.Fr_Sumry = Convert.ToString(_dt.Rows[0]["Fr_Sumry"]);
                o.Fr_Remarks = Convert.ToString(_dt.Rows[0]["Fr_Remarks"]);
                o.C_Mr_Tran = Convert.ToBoolean(_dt.Rows[0]["C_Mr_Tran"]);
                o.C_Mr_Sumry = Convert.ToBoolean(_dt.Rows[0]["C_Mr_Sumry"]);
                o.C_Mr_Remarks = Convert.ToBoolean(_dt.Rows[0]["C_Mr_Remarks"]);
                o.MeteorologicalReportPhoto = Convert.ToString(_dt.Rows[0]["MeteorologicalReportPhoto"]);
                o.Mr_Tran = Convert.ToString(_dt.Rows[0]["Mr_Tran"]);
                o.Mr_Sumry = Convert.ToString(_dt.Rows[0]["Mr_Sumry"]);
                o.Mr_Remarks = Convert.ToString(_dt.Rows[0]["Mr_Remarks"]);
                o.C_Fslr_Tran = Convert.ToBoolean(_dt.Rows[0]["C_Fslr_Tran"]);
                o.C_Fslr_Sumry = Convert.ToBoolean(_dt.Rows[0]["C_Fslr_Sumry"]);
                o.C_Fslr_Remarks = Convert.ToBoolean(_dt.Rows[0]["C_Fslr_Remarks"]);
                o.Fslr_Tran = Convert.ToString(_dt.Rows[0]["Fslr_Tran"]);
                o.Fslr_Sumry = Convert.ToString(_dt.Rows[0]["Fslr_Sumry"]);
                o.Fslr_Remarks = Convert.ToString(_dt.Rows[0]["Fslr_Remarks"]);

            }
            return o;
        }
        public List<CSMasterIPCSection> getIPCSection()
        {
            List<CSMasterIPCSection> listIPSec = new List<CSMasterIPCSection>();
            _dt = _sf.returnDTWithProc_executeReader("uspMasterIPCSectionGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {


                    CSMasterIPCSection ips = new CSMasterIPCSection();
                    ips.PK_IPC_ID = Convert.ToInt32(row["PK_IPC_ID"]);
                    ips.NameIPCSection = row["NameIPCSection"].ToString();
                    listIPSec.Add(ips);
                }
            }
            return listIPSec;
        }

        #endregion  
        #region   Liability Under Policy
        //public int saveLiabilityUnderPolicy(CSMISCLiabilityUnderPolicy lup)
        //{
        //    _param = new SqlParameter[] { new SqlParameter("")};
        //}
        public int saveLiabilityUnderPolicy(CSMISCLiabilityUnderPolicy lup)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCLUPID", lup.PK_MISCLUPID), new SqlParameter("@UserID", lup.UserID), new SqlParameter("@ClaimID", lup.ClaimID), new SqlParameter("@TypeOfPolicy", lup.TypeOfPolicy), new SqlParameter("@OP_ID", lup.OP_ID), new SqlParameter("@CaptionedFalling", lup.CaptionedFalling), new SqlParameter("@CaptionedFallingText", lup.CaptionedFallingText), new SqlParameter("@BreachWarranty", lup.BreachWarranty), new SqlParameter("@LossOccuredPolicy", lup.LossOccuredPolicy), new SqlParameter("@LossLocationFound", lup.LossLocationFound), new SqlParameter("@InsuredReasonablyAdopted", lup.InsuredReasonablyAdopted), new SqlParameter("@AccidentGrad", lup.AccidentGrad), new SqlParameter("@ForeUnfore", lup.ForeUnfore), new SqlParameter("@PhyImm", lup.PhyImm), new SqlParameter("@AggravationLoss", lup.AggravationLoss), new SqlParameter("@AggravationLossText", lup.AggravationLossText), new SqlParameter("@HeatingBurning", lup.HeatingBurning), new SqlParameter("@AffectedProperty", lup.AffectedProperty), new SqlParameter("@ConclusionInsurers", lup.ConclusionInsurers), new SqlParameter("@BlankText", lup.BlankText), }; return _sf.executeNonQueryWithProc("uspMISCLiabilityUnderPolicySave", _param);
        }
        public CSMISCLiabilityUnderPolicy getLiabilityUnderPolicy(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCLiabilityUnderPolicyGet", _param);
            CSMISCLiabilityUnderPolicy lup = new CSMISCLiabilityUnderPolicy();
            if (_dt.Rows.Count > 0)
            {
                lup.PK_MISCLUPID = Convert.ToInt32(_dt.Rows[0]["PK_MISCLUPID"]);
                lup.UserID = Convert.ToInt32(_dt.Rows[0]["UserID"]);
                lup.ClaimID = Convert.ToInt32(_dt.Rows[0]["ClaimID"]);
                lup.TypeOfPolicy = Convert.ToString(_dt.Rows[0]["TypeOfPolicy"]);
                lup.OP_ID = Convert.ToInt32(_dt.Rows[0]["OP_ID"]);
                lup.CaptionedFalling = Convert.ToString(_dt.Rows[0]["CaptionedFalling"]);
                lup.CaptionedFallingText = Convert.ToString(_dt.Rows[0]["CaptionedFallingText"]);
                lup.BreachWarranty = Convert.ToString(_dt.Rows[0]["BreachWarranty"]);
                lup.LossOccuredPolicy = Convert.ToString(_dt.Rows[0]["LossOccuredPolicy"]);
                lup.LossLocationFound = Convert.ToString(_dt.Rows[0]["LossLocationFound"]);
                lup.InsuredReasonablyAdopted = Convert.ToString(_dt.Rows[0]["InsuredReasonablyAdopted"]);
                lup.AccidentGrad = Convert.ToString(_dt.Rows[0]["AccidentGrad"]);
                lup.ForeUnfore = Convert.ToString(_dt.Rows[0]["ForeUnfore"]);
                lup.PhyImm = Convert.ToString(_dt.Rows[0]["PhyImm"]);
                lup.AggravationLoss = Convert.ToString(_dt.Rows[0]["AggravationLoss"]);
                lup.AggravationLossText = Convert.ToString(_dt.Rows[0]["AggravationLossText"]);
                lup.HeatingBurning = Convert.ToString(_dt.Rows[0]["HeatingBurning"]);
                lup.AffectedProperty = Convert.ToString(_dt.Rows[0]["AffectedProperty"]);
                lup.ConclusionInsurers = Convert.ToString(_dt.Rows[0]["ConclusionInsurers"]);
                lup.BlankText = Convert.ToString(_dt.Rows[0]["BlankText"]);

            }
            return lup;
        }
        public List<CSMISCWarCond> getWarCond(int claimId, int userId)
        {
            List<CSMISCWarCond> listWC = new List<CSMISCWarCond>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCWarCondGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {

                    CSMISCWarCond wc = new CSMISCWarCond();
                    wc.PK_WC_ID = Convert.ToInt32(row["PK_WC_ID"]);
                    wc.ClaimID = Convert.ToInt32(row["ClaimID"]);
                    wc.UserID = Convert.ToInt32(row["UserID"]);
                    wc.RWC_ID = Convert.ToInt32(row["RWC_ID"]);
                    wc.RelWarCond = row["RelWarCond"].ToString();
                    wc.StatusImplement = row["StatusImplement"].ToString();
                    listWC.Add(wc);
                }
            }
            return listWC;
        }
        public List<CSMISCOperatedPeril> getOperatedPeril()
        {
            List<CSMISCOperatedPeril> listOP = new List<CSMISCOperatedPeril>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCOperatedPerilGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {

                    CSMISCOperatedPeril op = new CSMISCOperatedPeril();
                    op.PK_OP_ID = Convert.ToInt32(row["PK_OP_ID"]);
                    op.NameOperatedPeril = row["NameOperatedPeril"].ToString();
                    listOP.Add(op);
                }
            }
            return listOP;
        }
        public int saveWarCond(CSMISCWarCond wc)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_WC_ID",wc.PK_WC_ID),new SqlParameter("@ClaimID",wc.ClaimID),new SqlParameter("@UserID",wc.UserID),new SqlParameter("@RWC_ID",wc.RWC_ID),new SqlParameter("@StatusImplement",wc.StatusImplement)
        };
            return _sf.executeNonQueryWithProc("uspMISCWarCondSave", _param);
        }
        #endregion
        #region Nature Extent Damage

        public List<CSMISCPolicySumInsured> getDesc_And_ShortName_byClaimID(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("UserID", userId) };
            List<CSMISCPolicySumInsured> listDS = new List<CSMISCPolicySumInsured>();
            _dt = _sf.returnDTWithProc_executeReader("usp_getDesc_And_ShortName_byClaimID", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    p.Description = Convert.ToString(row["Description"]).Trim();
                    p.ShortName = Convert.ToString(row["ShortName"]).Trim();
                    listDS.Add(p);
                }
            }
            return listDS;
        }

        public List<CSMISCPolicySumInsured> getPolicySumInsured_AND_ShortName(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("UserID", userId) };
            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCPolicySumInsured_AND_ShortName_Get", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    p.Description = Convert.ToString(row["Description"]).Trim();
                    p.ShortName = Convert.ToString(row["ShortName"]).Trim();
                    listPSI.Add(p);
                }
            }
            return listPSI;
        }
        public int saveNatureExtentDamage(CSMISCFSRNatureExtentDamage n)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_MISCFSRNEDID", n.PK_MISCFSRNEDID), new SqlParameter("@UserID", n.UserID), new SqlParameter("@ClaimID", n.ClaimID), new SqlParameter("@DescAffectedItem", n.DescAffectedItem), new SqlParameter("@Qty", n.Qty), new SqlParameter("@UOM", n.UOM), new SqlParameter("@BlockNo", n.BlockNo), new SqlParameter("@BuildingNo", n.BuildingNo), new SqlParameter("@FloorNo", n.FloorNo), new SqlParameter("@NameArea", n.NameArea), new SqlParameter("@MaterialPlacing", n.MaterialPlacing), new SqlParameter("@HeightGround", n.HeightGround), new SqlParameter("@PackingMaterial", n.PackingMaterial), new SqlParameter("@PackingSize", n.PackingSize), new SqlParameter("@LengthAffectedArea", n.LengthAffectedArea), new SqlParameter("@Breadth", n.Breadth), new SqlParameter("@Height", n.Height), new SqlParameter("@Functions", n.Functions), new SqlParameter("@Stage", n.Stage), new SqlParameter("@WIPStage", n.WIPStage), new SqlParameter("@BatchSize", n.BatchSize), new SqlParameter("@OccupancyAffectedArea", n.OccupancyAffectedArea), new SqlParameter("@NatureDamageLoss", n.NatureDamageLoss), new SqlParameter("@ExtentDamageLoss", n.ExtentDamageLoss), new SqlParameter("@Model", n.Model), new SqlParameter("@Make", n.Make), new SqlParameter("@SrNo", n.SrNo), new SqlParameter("@Capacity", n.Capacity), new SqlParameter("@CountryOrigin", n.CountryOrigin), new SqlParameter("@MfgYear", n.MfgYear), new SqlParameter("@WorkingHours", n.WorkingHours), new SqlParameter("@Properties", n.Properties), new SqlParameter("@MOC", n.MOC), new SqlParameter("@ChkbxPS", n.ChkbxPS), new SqlParameter("@SrNoPS", n.SrNoPS), new SqlParameter("@PSI_ID", n.PSI_ID) };
            return _sf.executeNonQueryWithProc("uspMISCFSRNatureExtentDamageSave", _param);
        }

        public List<CSMISCFSRNatureExtentDamage> getNatureExtentDamage(int userId, int claimId, int psi_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId), new SqlParameter("@PSI_ID", psi_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCFSRNatureExtentDamageGet", _param);
            List<CSMISCFSRNatureExtentDamage> listNED = new List<CSMISCFSRNatureExtentDamage>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCFSRNatureExtentDamage n = new CSMISCFSRNatureExtentDamage();
                    n.PK_MISCFSRNEDID = Convert.ToInt32(row["PK_MISCFSRNEDID"]);
                    n.DescAffectedItem = Convert.ToString(row["DescAffectedItem"]);
                    n.Qty = Convert.ToDecimal(row["Qty"]);
                    n.UOM = Convert.ToString(row["UOM"]);
                    n.BlockNo = Convert.ToString(row["BlockNo"]);
                    n.BuildingNo = Convert.ToString(row["BuildingNo"]);
                    n.FloorNo = Convert.ToString(row["FloorNo"]);
                    n.NameArea = Convert.ToString(row["NameArea"]);
                    n.MaterialPlacing = Convert.ToString(row["MaterialPlacing"]);
                    n.HeightGround = Convert.ToString(row["HeightGround"]);
                    n.PackingMaterial = Convert.ToString(row["PackingMaterial"]);
                    n.PackingSize = Convert.ToString(row["PackingSize"]);
                    n.LengthAffectedArea = Convert.ToString(row["LengthAffectedArea"]);
                    n.Breadth = Convert.ToString(row["Breadth"]);
                    n.Height = Convert.ToString(row["Height"]);
                    n.Functions = Convert.ToString(row["Functions"]);
                    n.Stage = Convert.ToString(row["Stage"]);
                    n.WIPStage = Convert.ToString(row["WIPStage"]);
                    n.BatchSize = Convert.ToString(row["BatchSize"]);
                    n.OccupancyAffectedArea = Convert.ToString(row["OccupancyAffectedArea"]);
                    n.NatureDamageLoss = Convert.ToString(row["NatureDamageLoss"]);
                    n.ExtentDamageLoss = Convert.ToString(row["ExtentDamageLoss"]);
                    n.Model = Convert.ToString(row["Model"]);
                    n.Make = Convert.ToString(row["Make"]);
                    n.SrNo = Convert.ToString(row["SrNo"]);
                    n.Capacity = Convert.ToString(row["Capacity"]);
                    n.CountryOrigin = Convert.ToString(row["CountryOrigin"]);
                    n.MfgYear = Convert.ToString(row["MfgYear"]);
                    n.WorkingHours = Convert.ToString(row["WorkingHours"]);
                    n.Properties = Convert.ToString(row["Properties"]);
                    n.MOC = Convert.ToString(row["MOC"]);
                    n.ChkbxPS = Convert.ToBoolean(row["ChkbxPS"]);
                    n.SrNoPS = Convert.ToString(row["SrNoPS"]);
                    n.PSI_ID = Convert.ToInt32(row["PSI_ID"]);

                    listNED.Add(n);
                }

            }
            return listNED;
        }
        public List<CSMISCPolicySumInsured> getDescription_SumIns_byClaimID(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("UserID", userId) };
            List<CSMISCPolicySumInsured> listDS = new List<CSMISCPolicySumInsured>();
            _dt = _sf.returnDTWithProc_executeReader("uspPolicySumInsured_getDescription_SumIns_byClaimID", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.SumInsured = Convert.ToDecimal(row["SumInsured"]);
                    p.Description = Convert.ToString(row["Description"]).Trim();
                    listDS.Add(p);
                }
            }
            return listDS;
        }
        #endregion
        #region RecoverySubrogationSave
        public int saveRecoverySubrogation(CSMISCRecoverySubrogation rs)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_RS_ID", rs.PK_RS_ID), new SqlParameter("@ClaimID", rs.ClaimID), new SqlParameter("@UserID", rs.UserID), new SqlParameter("@AspectsofRecovery", rs.AspectsofRecovery), new SqlParameter("@SubrogationRights", rs.SubrogationRights) };
            return _sf.executeNonQueryWithProc("uspMISCRecoverySubrogationSave", _param);
        }
        public CSMISCRecoverySubrogation getRecoverySubrogation(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCRecoverySubrogationGet", _param);
            CSMISCRecoverySubrogation rs = new CSMISCRecoverySubrogation();
            if (_dt.Rows.Count > 0)
            {
                rs.PK_RS_ID = Convert.ToInt32(_dt.Rows[0]["PK_RS_ID"]);
                rs.AspectsofRecovery = _dt.Rows[0]["AspectsofRecovery"].ToString();
                rs.SubrogationRights = _dt.Rows[0]["SubrogationRights"].ToString();

            }
            return rs;
        }
        #endregion
        #region Insured's Claim

        public int saveInsuredClaim(CSMISCInsuredClaim i)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_IC_ID", i.PK_IC_ID), new SqlParameter("@UserID", i.UserID), new SqlParameter("@ClaimID", i.ClaimID), new SqlParameter("@RevisionofInsClaim", i.RevisionofInsClaim) ,new SqlParameter("@TotalAmount",i.TotalAmount) };
            return _sf.executeNonQueryWithProc("uspMISCInsuredClaimSave", _param);
        }

        public int saveDescAmount(CSMISCDescAmount d)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_DL_ID", d.PK_DL_ID), new SqlParameter("@UserID", d.UserID), new SqlParameter("@ClaimID", d.ClaimID),new SqlParameter("@PSI_ID", d.PSI_ID), new SqlParameter("@Amount", d.Amount) };
            return _sf.executeNonQueryWithProc("uspMISCDescAmountSave", _param);
        }
        public List<CSMISCDescAmount> getDescAmount(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCDescAmountGet", _param);
            List<CSMISCDescAmount> listDA = new List<CSMISCDescAmount>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {

                    CSMISCDescAmount da = new CSMISCDescAmount();
                    da.PK_DL_ID = Convert.ToInt32(row["PK_DL_ID"]);
                    da.PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    da.Description = row["Description"].ToString();
                    da.Amount = Convert.ToDecimal(row["Amount"]);
                    listDA.Add(da);

                }

            }
            return listDA;
        }
        public string getReportLoss(int claimId)
        {
            string rl = " ";
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            rl = (string)_sf.executeScalerWithProc("usp_getReportLoss_byClaimID", _param);
            return rl;
        }
        public CSMISCInsuredClaim getInsuredClaim(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCInsuredClaimGet", _param);
            CSMISCInsuredClaim i = new CSMISCInsuredClaim();
            if (_dt.Rows.Count > 0)
            {
                i.PK_IC_ID = Convert.ToInt32(_dt.Rows[0]["PK_IC_ID"]);
                i.RevisionofInsClaim = _dt.Rows[0]["RevisionofInsClaim"].ToString();
                i.TotalAmount =Convert.ToDecimal(_dt.Rows[0]["TotalAmount"]);
            }
            return i;
        }
        #endregion
        #region Assessment of Loss


        public int saveAssessmentofLoss(CSMISCAssessmentofLoss al)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AL_ID", al.PK_AL_ID), new SqlParameter("@UserID", al.UserID), new SqlParameter("@ClaimID", al.ClaimID), new SqlParameter("@DeterQty", al.DeterQty), new SqlParameter("@VerificationRates", al.VerificationRates), new SqlParameter("@AssesmentSummary", al.AssesmentSummary), new SqlParameter("@DepriReinst", al.DepriReinst), new SqlParameter("@DeductHBD", al.DeductHBD), new SqlParameter("@ObsolSlowDead", al.ObsolSlowDead), new SqlParameter("@UpgradeModification", al.UpgradeModification), new SqlParameter("@Salvage", al.Salvage), new SqlParameter("@AdequacySumInsured", al.AdequacySumInsured), new SqlParameter("@PSI_ID", al.PSI_ID) };
            return _sf.executeNonQueryWithProc("uspMISCAssessmentofLossSave", _param);
        }
        public int saveDescofOriginProcurement(CSMISCDescofOriginProcurement d)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_DOP_ID", d.PK_DOP_ID), new SqlParameter("@UserID", d.UserID), new SqlParameter("@ClaimID", d.ClaimID), new SqlParameter("@DescofItem", d.DescofItem), new SqlParameter("@SupplierName", d.SupplierName), new SqlParameter("@CountryofOrigin", d.CountryofOrigin), new SqlParameter("@InvNo", d.InvNo), new SqlParameter("@InvDate", d.InvDate), new SqlParameter("@InvoiceValue", d.InvoiceValue), new SqlParameter("@UOM", d.UOM), new SqlParameter("@CapitalizedValue", d.CapitalizedValue), new SqlParameter("@PSI_ID", d.PSI_ID) };
            return _sf.executeNonQueryWithProc("uspMISCDescofOriginProcurementSave", _param);
        }
        public List<string> getDistinctTableName_byClaimID(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("usp_getDistinctTableName_byClaimID", _param);
            List<string> listTableName = new List<string>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    string TableName = row["TableName"].ToString();
                    listTableName.Add(TableName);
                }
            }
            return listTableName;
        }

        public List<CSMISCAssessmentofLoss> getAssessmentofLoss(int claimId, int psi_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@PSI_ID", psi_Id), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAssessmentofLossGet", _param);
            List<CSMISCAssessmentofLoss> listAL = new List<CSMISCAssessmentofLoss>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCAssessmentofLoss al = new CSMISCAssessmentofLoss();
                    al.PK_AL_ID = Convert.ToInt32(row["PK_AL_ID"]);
                    al.DeterQty = Convert.ToString(row["DeterQty"]);
                    al.VerificationRates = Convert.ToString(row["VerificationRates"]);
                    al.AssesmentSummary = Convert.ToString(row["AssesmentSummary"]);
                    al.DepriReinst = Convert.ToString(row["DepriReinst"]);
                    al.DeductHBD = Convert.ToString(row["DeductHBD"]);
                    al.ObsolSlowDead = Convert.ToString(row["ObsolSlowDead"]);
                    al.UpgradeModification = Convert.ToString(row["UpgradeModification"]);
                    al.Salvage = Convert.ToString(row["Salvage"]);
                    al.AdequacySumInsured = Convert.ToString(row["AdequacySumInsured"]);
                    listAL.Add(al);
                }
            }
            return listAL;
        }

        public List<CSMISCDescofOriginProcurement> getDescofOriginProcurement(int claimId,  int psi_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@PSI_ID", psi_Id), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCDescofOriginProcurementGet", _param);
            List<CSMISCDescofOriginProcurement> listDOP = new List<CSMISCDescofOriginProcurement>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCDescofOriginProcurement d = new CSMISCDescofOriginProcurement();
                    d.PK_DOP_ID = Convert.ToInt32(row["PK_DOP_ID"]);
                    d.DescofItem = Convert.ToString(row["DescofItem"]);
                    d.SupplierName = Convert.ToString(row["SupplierName"]);
                    d.CountryofOrigin = Convert.ToString(row["CountryofOrigin"]);
                    d.InvNo = Convert.ToString(row["InvNo"]);
                    d.InvDate = Convert.ToString(row["InvDate"]);
                    d.InvoiceValue = Convert.ToDecimal(row["InvoiceValue"]);
                    d.UOM = Convert.ToString(row["UOM"]);
                    d.CapitalizedValue = Convert.ToDecimal(row["CapitalizedValue"]);
                     listDOP.Add(d);
                }
            }
            return listDOP;
        }

        #endregion
        #region Asssement of loss part2
        public List<CSMISCPolicySumInsured> getDistinct_PSI_ID_byClaimID(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("UserID", userId) };
            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            _dt = _sf.returnDTWithProc_executeReader("usp_getDistinct_PSI_ID_byClaimID", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCPolicySumInsured p = new CSMISCPolicySumInsured();
                    p.PK_PSI_ID = Convert.ToInt32(row["PK_PSI_ID"]);
                    p.Description = Convert.ToString(row["Description"]).Trim();
                    listPSI.Add(p);
                }
            }
            return listPSI;
        }
        public List<CSDescriptionofAL> getDescriptionofAL()
        {
            List<CSDescriptionofAL> listDAL = new List<CSDescriptionofAL>();
            _dt = _sf.returnDTWithProc_executeReader("uspDescriptionofALGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSDescriptionofAL d = new CSDescriptionofAL();
                    d.PK_DescAL_ID = Convert.ToInt32(row["PK_DescAL_ID"]);
                    d.Description = row["Description"].ToString();
                    listDAL.Add(d);
                }


            }
            return listDAL;
        }
        public List<CSMISCUOM> getUOM()
        {
            List<CSMISCUOM> listUOM = new List<CSMISCUOM>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCUOMGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCUOM u = new CSMISCUOM();
                    u.PK_UOM_ID = Convert.ToInt32(row["PK_UOM_ID"]);
                    u.UOM = row["UOM"].ToString();
                    listUOM.Add(u);
                }


            }
            return listUOM;
        }
        public int saveAsseessmentHeader(CSMISCAsseessmentHeader ah)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AH_ID", ah.PK_AH_ID), new SqlParameter("@UserID", ah.UserID), new SqlParameter("@ClaimID", ah.ClaimID), new SqlParameter("@PSI_ID", ah.PSI_ID), new SqlParameter("@TotalAssLoss", ah.TotalAssLoss), new SqlParameter("@GrossLossAss", ah.GrossLossAss), new SqlParameter("@TotalDeductAABNA", ah.TotalDeductAABNA), new SqlParameter("@GrossAdmLoss", ah.GrossAdmLoss), new SqlParameter("@DeductofGFIP", ah.DeductofGFIP), new SqlParameter("@SalvageRate", ah.SalvageRate), new SqlParameter("@SalvageAmount", ah.SalvageAmount), new SqlParameter("@DeprRate", ah.DeprRate), new SqlParameter("@DeprAmount", ah.DeprAmount), new SqlParameter("@GrandTotal", ah.GrandTotal), new SqlParameter("@TotalInitClaim", ah.TotalInitClaim), new SqlParameter("@TotalFinalClaim", ah.TotalFinalClaim) };

            int scopeId = (int)_sf.executeScalerWithProc("uspMISCAsseessmentHeaderSave", _param);
            return scopeId;
        }
        public int saveAssessmentDescription(CSMISCAssessmentDescription ad)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AD_ID", ad.PK_AD_ID), new SqlParameter("@AH_ID", ad.AH_ID), new SqlParameter("@ClaimID", ad.ClaimID), new SqlParameter("NED_ID", ad.NED_ID), new SqlParameter("@ICRate", ad.ICRate), new SqlParameter("@ICAmount", ad.ICAmount), new SqlParameter("@FCQty", ad.FCQty), new SqlParameter("@FCRate", ad.FCRate), new SqlParameter("@FCAmount", ad.FCAmount), new SqlParameter("@ALQty", ad.ALQty), new SqlParameter("@ALRate", ad.ALRate), new SqlParameter("@ALAmount", ad.ALAmount), new SqlParameter("@DeductAABNA", ad.DeductAABNA), new SqlParameter("@AdmissibleAmount", ad.AdmissibleAmount), new SqlParameter("@Remarks", ad.Remarks), }; return _sf.executeNonQueryWithProc("uspMISCAssessmentDescriptionSave", _param);
        }
        public int saveAssessmentCalculation(CSMISCAssessmentCalculation ac)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_AC_ID", ac.PK_AC_ID), new SqlParameter("@AH_ID", ac.AH_ID), new SqlParameter("@Sign", ac.Sign), new SqlParameter("@DescAL_ID", ac.DescAL_ID), new SqlParameter("@Rate", ac.Rate), new SqlParameter("@Amount", ac.Amount), new SqlParameter("@UOM_Id", ac.UOM_Id)
            };
            return _sf.executeNonQueryWithProc("uspMISCAssessmentCalculationSave", _param);
        }
        public List<CSMISCAssessmentDescription> getAssessmentDescription(int psi_Id, int ClaimID, int UserID)
        {
            _param = new SqlParameter[] { new SqlParameter("@PSI_ID", psi_Id), new SqlParameter("@ClaimID", ClaimID), new SqlParameter("@UserID", UserID) };
            //List<CSMISCAssessmentDescription> listAD = new List<CSMISCAssessmentDescription>();
            List<CSMISCAssessmentDescription> listAD = new List<CSMISCAssessmentDescription>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAssessmentDescriptionGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCAssessmentDescription ad = new CSMISCAssessmentDescription();
                    ad.PK_MISCFSRNEDID = Convert.ToInt32(row["PK_MISCFSRNEDID"]);
                    ad.NED_ID = Convert.ToInt32(row["NED_ID"]);
                    ad.PK_AD_ID = Convert.ToInt32(row["PK_AD_ID"]);
                    ad.DescAffectedItem = row["DescAffectedItem"].ToString();
                    ad.Qty = Convert.ToDecimal(row["Qty"]);
                    ad.UOM = row["UOM"].ToString();
                    ad.AH_ID = Convert.ToInt32(row["AH_ID"]);
                    ad.ICRate = Convert.ToDecimal(row["ICRate"]);
                    ad.ICAmount = Convert.ToDecimal(row["ICAmount"]);
                    ad.FCQty = Convert.ToDecimal(row["FCQty"]);
                    ad.FCRate = Convert.ToDecimal(row["FCRate"]);
                    ad.FCAmount = Convert.ToDecimal(row["FCAmount"]);
                    ad.ALQty = Convert.ToDecimal(row["ALQty"]);
                    ad.ALRate = Convert.ToDecimal(row["ALRate"]);
                    ad.ALAmount = Convert.ToDecimal(row["ALAmount"]);
                    ad.DeductAABNA = Convert.ToDecimal(row["DeductAABNA"]);
                    ad.AdmissibleAmount = Convert.ToDecimal(row["AdmissibleAmount"]);
                    ad.Remarks = Convert.ToString(row["Remarks"]);
                    listAD.Add(ad);
                }

            }
            return listAD;
        }
        public CSMISCAsseessmentHeader getAsseessmentHeader(int userId, int claimId, int psi_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId), new SqlParameter("@PSI_ID",psi_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAsseessmentHeaderGet", _param);
            CSMISCAsseessmentHeader ah = new CSMISCAsseessmentHeader();
            if (_dt.Rows.Count > 0)
            {
                ah.PK_AH_ID = Convert.ToInt32(_dt.Rows[0]["PK_AH_ID"]);
                ah.TotalAssLoss = Convert.ToDecimal(_dt.Rows[0]["TotalAssLoss"]);
                ah.GrossLossAss = Convert.ToDecimal(_dt.Rows[0]["GrossLossAss"]);
                ah.TotalDeductAABNA = Convert.ToDecimal(_dt.Rows[0]["TotalDeductAABNA"]);
                ah.GrossAdmLoss = Convert.ToDecimal(_dt.Rows[0]["GrossAdmLoss"]);
                ah.DeductofGFIP = Convert.ToDecimal(_dt.Rows[0]["DeductofGFIP"]);
                ah.SalvageRate = Convert.ToDecimal(_dt.Rows[0]["SalvageRate"]);
                ah.SalvageAmount = Convert.ToDecimal(_dt.Rows[0]["SalvageAmount"]);
                ah.DeprRate = Convert.ToDecimal(_dt.Rows[0]["DeprRate"]);
                ah.DeprAmount = Convert.ToDecimal(_dt.Rows[0]["DeprAmount"]);
                ah.GrandTotal = Convert.ToDecimal(_dt.Rows[0]["GrandTotal"]);
                ah.TotalInitClaim = Convert.ToDecimal(_dt.Rows[0]["TotalInitClaim"]);
                ah.TotalFinalClaim = Convert.ToDecimal(_dt.Rows[0]["TotalFinalClaim"]);
            }
            return ah;
        }
        public List<CSMISCAssessmentCalculation> getAssessmentCalculation(int ah_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@AH_ID", ah_Id) };
            List<CSMISCAssessmentCalculation> listAC = new List<CSMISCAssessmentCalculation>();
            _dt = _sf.returnDTWithProc_executeReader("uspMISCAssessmentCalculationGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCAssessmentCalculation ac = new CSMISCAssessmentCalculation();
                    ac.PK_AC_ID = Convert.ToInt32(row["PK_AC_ID"]);
                    ac.AH_ID = Convert.ToInt32(row["AH_ID"]);
                    ac.Sign = Convert.ToString(row["Sign"]);
                    ac.DescAL_ID = Convert.ToInt32(row["DescAL_ID"]);
                    ac.Rate = Convert.ToDecimal(row["Rate"]);
                    ac.Amount = Convert.ToDecimal(row["Amount"]);
                    ac.UOM_Id = Convert.ToInt32(row["UOM_Id"]);
                    listAC.Add(ac);
                }

            }
            return listAC;
        }
        #endregion


        #region RecommendationsLossRemarks
        public int saveRecommendationsLossRemarks(CSMISCRecommendationsLossRemarks r)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_RL_ID", r.PK_RL_ID), new SqlParameter("@UserID", r.UserID), new SqlParameter("@ClaimID", r.ClaimID), new SqlParameter("@Recommendations", r.Recommendations), new SqlParameter("@LossMinMeas", r.LossMinMeas), new SqlParameter("@Remarks", r.Remarks), }; return _sf.executeNonQueryWithProc("uspMISCRecommendationsLossRemarksSave", _param);

        }

        public CSMISCRecommendationsLossRemarks getRecommendationsLossRemarks(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCRecommendationsLossRemarksGet", _param);
            CSMISCRecommendationsLossRemarks rl = new CSMISCRecommendationsLossRemarks();
            if (_dt.Rows.Count > 0)
            {
                rl.PK_RL_ID = Convert.ToInt32(_dt.Rows[0]["PK_RL_ID"]);
                rl.Recommendations = Convert.ToString(_dt.Rows[0]["Recommendations"]);
                rl.LossMinMeas = Convert.ToString(_dt.Rows[0]["LossMinMeas"]);
                rl.Remarks = Convert.ToString(_dt.Rows[0]["Remarks"]);

            }
            return rl;
        }
        #endregion
        #region InsuredConsent
        public List<CSMISCModeofCommunication> getModeofCommunication()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspMISCModeofCommunicationGet");
            List<CSMISCModeofCommunication> listMC = new List<CSMISCModeofCommunication>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {

                    CSMISCModeofCommunication mc = new CSMISCModeofCommunication();
                    mc.PK_MC_ID = Convert.ToInt32(row["PK_MC_ID"]);
                    mc.ModeofCommunication = row["ModeofCommunication"].ToString();
                    listMC.Add(mc);

                }

            }
            return listMC;
        }
        public int saveInsuredConsent(CSMISCInsuredConsent ic)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_IC_ID", ic.PK_IC_ID), new SqlParameter("@UserID", ic.UserID), new SqlParameter("@ClaimID", ic.ClaimID), new SqlParameter("@ChkbxDesc", ic.ChkbxDesc), new SqlParameter("@MC_ID", ic.MC_ID), new SqlParameter("@DateofComm", ic.DateofComm), new SqlParameter("@SnapShotofConsent", ic.SnapShotofConsent), }; return _sf.executeNonQueryWithProc("uspMISCInsuredConsentSave", _param);
        }
        public CSMISCInsuredConsent getInsuredConsent(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCInsuredConsentGet", _param);
            CSMISCInsuredConsent ic = new CSMISCInsuredConsent();
            if (_dt.Rows.Count > 0)
            {
                ic.PK_IC_ID = Convert.ToInt32(_dt.Rows[0]["PK_IC_ID"]);
                ic.ChkbxDesc = Convert.ToString(_dt.Rows[0]["ChkbxDesc"]);
                ic.MC_ID = Convert.ToInt32(_dt.Rows[0]["MC_ID"]);
                ic.DateofComm = Convert.ToString(_dt.Rows[0]["DateofComm"]);
                ic.SnapShotofConsent = Convert.ToString(_dt.Rows[0]["SnapShotofConsent"]);


            }
            return ic;
        }
        #endregion

        #region Contribution			

        public List<CSMISCContribution> getContribution(int claimId, int userId)
        {
            List<CSMISCContribution> listCon = new List<CSMISCContribution>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };

            _dt = _sf.returnDTWithProc_executeReader("uspMISCContributionGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMISCContribution c = new CSMISCContribution();
                    c.PK_CoIns_ID = Convert.ToInt32(row["PK_CoIns_ID"]);
                    c.PK_Con_ID = Convert.ToInt32(row["PK_Con_ID"]);
                    c.CoIns_ID = Convert.ToInt32(row["CoIns_ID"]);
                    c.Name = row["Name"].ToString();
                    c.Percentage = Convert.ToDecimal(row["Percentage"]);
                    c.ShareInAmount = Convert.ToDecimal(row["ShareInAmount"]);
                    listCon.Add(c);
                }


            }
            return listCon;
        }

        public int saveContribution(CSMISCContribution c)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_Con_ID", c.PK_Con_ID), new SqlParameter("@ClaimID", c.ClaimID), new SqlParameter("@UserID", c.UserID), new SqlParameter("@CoIns_ID", c.PK_CoIns_ID), new SqlParameter("@ShareInAmount", c.ShareInAmount), }; return _sf.executeNonQueryWithProc("uspMISCContributionSave", _param);
        }
        public int deleteContribution(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMISCContributionDelete", _param);
        }
        #endregion
        #region Claim Assessment/Adjustment						        
        public DataTable getClaimAssAdjustSummary(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspClaimAssAdjustSummaryGet", _param);
            return _dt;
        }
        public int saveClaimAssAdjust(CSMISCClaimAssAdjust c)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_CAA_ID", c.PK_CAA_ID), new SqlParameter("@UserID", c.UserID), new SqlParameter("@ClaimID", c.ClaimID), new SqlParameter("@LossMinmization", c.LossMinmization), new SqlParameter("@DebrisRemoval", c.DebrisRemoval), new SqlParameter("@SurveyorArcCon", c.SurveyorArcCon), new SqlParameter("@Excess", c.Excess), new SqlParameter("@OnAccountPayment", c.OnAccountPayment), }; return _sf.executeNonQueryWithProc("uspMISCClaimAssAdjustSave", _param);
        }
        public CSMISCClaimAssAdjust getClaimAssAdjust(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMISCClaimAssAdjustGet", _param);
            CSMISCClaimAssAdjust c = new CSMISCClaimAssAdjust();
            if (_dt.Rows.Count > 0)
            {
                c.PK_CAA_ID = Convert.ToInt32(_dt.Rows[0]["PK_CAA_ID"]);
                c.LossMinmization = Convert.ToString(_dt.Rows[0]["LossMinmization"]);
                c.DebrisRemoval = Convert.ToString(_dt.Rows[0]["DebrisRemoval"]);
                c.SurveyorArcCon = Convert.ToString(_dt.Rows[0]["SurveyorArcCon"]);
                c.Excess = Convert.ToString(_dt.Rows[0]["Excess"]);
                c.OnAccountPayment = Convert.ToString(_dt.Rows[0]["OnAccountPayment"]);
            }
            return c;
        }
        public int saveClaimAssAdjustSummary(CSMISCClaimAssAdjustSummary cs)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_CAAS_ID",cs.PK_CAAS_ID),
new SqlParameter("@UserID", cs.UserID), new SqlParameter("@ClaimID", cs.ClaimID), new SqlParameter("@Value", cs.Value), new SqlParameter("@TableName", cs.TableName), new SqlParameter("@ShortName", cs.ShortName), };
            return _sf.executeNonQueryWithProc("uspMISCClaimAssAdjustSummarySave", _param);
        }
        public decimal getRLP_Total_byClaimID(int userId, int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId), new SqlParameter("@ClaimID", claimId) };
            decimal rlp_Total = (decimal)_sf.executeScalerWithProc("usp_MISCClaimAssAdjustSummary_getRLP_Total_byClaimID", _param);
            return rlp_Total;
        }
        #endregion


    }
}



