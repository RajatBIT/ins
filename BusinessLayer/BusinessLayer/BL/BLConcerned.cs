﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
   public class BLConcerned
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLConcerned()
        {
            _sf = new SqlFunctions();
        }
        public List<CSBroker> getBroker()
        {
            List<CSBroker> listBroker = new List<CSBroker>();
            _dt = _sf.returnDTWithProc_executeReader("uspBrokerGet");
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBroker b = new CSBroker();
                    b.PK_Broker_ID = Convert.ToInt32(row["PK_Broker_ID"]);
                    b.IrdaRegNo =Convert.ToString(row["IrdaRegNo"]);
                    b.BrokerName =Convert.ToString(row["BrokerName"]);
                    listBroker.Add(b);
                        
                }
            }
            return listBroker;
        }
        public int saveConcerned(CSConcerned c)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_Conc_ID",c.PK_Conc_ID), new SqlParameter("@ClaimID", c.ClaimID), new SqlParameter("@UserID", c.UserID), new SqlParameter("@PolicyNo", c.PolicyNo), new SqlParameter("@Broker_ID", c.Broker_ID), new SqlParameter("@B_R_ID", c.B_R_ID), new SqlParameter("@B_StateCode", c.B_StateCode), new SqlParameter("@B_Location_ID", c.B_Location_ID), new SqlParameter("@B_RegUserEmail", c.B_RegUserEmail), new SqlParameter("@B_ToEmailId", c.B_ToEmailId), new SqlParameter("@B_CCEmailId", c.B_CCEmailId), new SqlParameter("@I_R_ID", c.I_R_ID), new SqlParameter("@I_StateCode", c.I_StateCode), new SqlParameter("@I_Location_ID", c.I_Location_ID), new SqlParameter("@I_RegUserEmail", c.I_RegUserEmail), new SqlParameter("@I_ToEmailId", c.I_ToEmailId), new SqlParameter("@I_CCEmailId", c.I_CCEmailId), new SqlParameter("@Is_ToEmailId", c.Is_ToEmailId), new SqlParameter("@Is_CCEmailId", c.Is_CCEmailId)};
            return _sf.executeNonQueryWithProc("uspConcernedSave", _param);
        }
        public CSConcerned getConcerned(int claimId,int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID",claimId),new SqlParameter("@UserID",userId)};
            _dt = _sf.returnDTWithProc_executeReader("uspConcernedGet",_param);
            CSConcerned c = new CSConcerned();
            if(_dt.Rows.Count>0)
            {
                c.PK_Conc_ID = Convert.ToInt32(_dt.Rows[0]["PK_Conc_ID"]);
                c.Broker_ID = Convert.ToInt32(_dt.Rows[0]["Broker_ID"]);
                c.B_R_ID = Convert.ToInt32(_dt.Rows[0]["B_R_ID"]);
                c.B_StateCode = Convert.ToInt32(_dt.Rows[0]["B_StateCode"]);
                c.B_Location_ID = Convert.ToInt32(_dt.Rows[0]["B_Location_ID"]);
                c.B_RegUserEmail = Convert.ToString(_dt.Rows[0]["B_RegUserEmail"]);
                c.B_ToEmailId = Convert.ToString(_dt.Rows[0]["B_ToEmailId"]);
                c.B_CCEmailId = Convert.ToString(_dt.Rows[0]["B_CCEmailId"]);
                c.I_R_ID = Convert.ToInt32(_dt.Rows[0]["I_R_ID"]);
                c.I_StateCode = Convert.ToInt32(_dt.Rows[0]["I_StateCode"]);
                c.I_Location_ID = Convert.ToInt32(_dt.Rows[0]["I_Location_ID"]);
                c.I_RegUserEmail = Convert.ToString(_dt.Rows[0]["I_RegUserEmail"]);
                c.I_ToEmailId = Convert.ToString(_dt.Rows[0]["I_ToEmailId"]);
                c.I_CCEmailId = Convert.ToString(_dt.Rows[0]["I_CCEmailId"]);
                c.Is_ToEmailId = Convert.ToString(_dt.Rows[0]["Is_ToEmailId"]);
                c.Is_CCEmailId = Convert.ToString(_dt.Rows[0]["Is_CCEmailId"]);

            }
            return c;
        }

    }
}
