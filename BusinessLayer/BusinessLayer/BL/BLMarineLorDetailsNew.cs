﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
   public class BLMarineLorDetailsNew
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        DataSet _ds;
        public BLMarineLorDetailsNew()
        {
            _sf = new SqlFunctions();
        }
        public List<CSMarineLorDetailsNew> getLorDetailsNew(int claimId)
        {
            _param = new SqlParameter[] {new SqlParameter("@ClaimID",claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineLorDetailsNewGet",_param);
            List<CSMarineLorDetailsNew> listLorDetailsNew = new List<CSMarineLorDetailsNew>();
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineLorDetailsNew l = new CSMarineLorDetailsNew();
                    l.PK_L_ID =Convert.ToInt32(row["PK_L_ID"]);
                    l.Name =Convert.ToString(row["Name"]);
                    l.LM_ID =Convert.ToInt32(row["LM_ID"]);
                    l.Rec_NotRec_NotRel = Convert.ToString(row["Rec_NotRec_NotRel"]);
                    l.FileName =Convert.ToString(row["FileName"]);
                    l.LM_Name =Convert.ToString(row["LM_Name"]);
                    listLorDetailsNew.Add(l);
                }
            }
            return listLorDetailsNew;
        }

        public DataTable getDetailForLorReminder(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("uspPDFMarineLORReminder", _param);
            return _dt;
        }
        public DataSet getDetailForPdfMotorFSR(int claimId,int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@userID",userId) };
            _ds = _sf.ReturnDsWithProc("uspPDFMotorFSR", _param);
            return _ds;
        }

        public int saveLorDetailsNew(CSMarineLorDetailsNew l)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_L_ID",l.PK_L_ID), new SqlParameter("@ClaimID", l.ClaimID), new SqlParameter("@UserID", l.UserID), new SqlParameter("@LM_ID", l.LM_ID), new SqlParameter("@Rec_NotRec_NotRel", l.Rec_NotRec_NotRel), new SqlParameter("@LM_Name", l.LM_Name) };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspMarineLorDetailsNewSave", _param));
        }
        public int saveLorHeaderNew(CSMarineLorHeaderNew lh)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_LH_ID", lh.PK_LH_ID), new SqlParameter("@ClaimID", lh.ClaimID), new SqlParameter("@UserID", lh.UserID), new SqlParameter("@NameContactPerson", lh.NameContactPerson), new SqlParameter("@MobileNoContactPerson", lh.MobileNoContactPerson), new SqlParameter("@EmailIdContactPerson", lh.EmailIdContactPerson) };
            return _sf.executeNonQueryWithProc("uspMarineLorHeaderNewSave", _param);
        }
        public CSMarineLorHeaderNew getLorHeaderNew(int cliamId,int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID",cliamId),new SqlParameter("@UserID",userId)};
            _dt = _sf.returnDTWithProc_executeReader("uspMarineLorHeaderNewGet",_param);
            CSMarineLorHeaderNew lh = new CSMarineLorHeaderNew();
            if (_dt.Rows.Count>0)
            {
                    lh.PK_LH_ID =Convert.ToInt32(_dt.Rows[0]["PK_LH_ID"]);
                    lh.MobileNoContactPerson =Convert.ToString(_dt.Rows[0]["MobileNoContactPerson"]);
                    lh.EmailIdContactPerson =Convert.ToString(_dt.Rows[0]["EmailIdContactPerson"]);
                    lh.NameContactPerson =Convert.ToString(_dt.Rows[0]["NameContactPerson"]);
            }
            return lh;
        }
        //public int saveMarineLorEmailIdsNew(CSMarineLorHeaderNew le)
        //{
        //    _param = new SqlParameter[] 
        //    { new SqlParameter("@PK_LE_ID", le.PK_LE_ID), new SqlParameter("@ClaimID", le.ClaimID), new SqlParameter("@UserID", le.UserID), new SqlParameter("@LH_ID", le.LH_ID), new SqlParameter("@ToEmailIds", le.ToEmailIds), new SqlParameter("@CCEmailIds", le.CCEmailIds) };
        //    return _sf.executeNonQueryWithProc("uspMarineLorEmailIdsNewSave", _param);
        //}
        public int saveMarineLorEmailIdsNew(CSMarineLorEmailIdsNew e)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_LE_ID", e.PK_LE_ID), new SqlParameter("@ClaimID", e.ClaimID), new SqlParameter("@UserID", e.UserID), new SqlParameter("@ToEmailIds", e.ToEmailIds), new SqlParameter("@CCEmailIds", e.CCEmailIds), new SqlParameter("@ToInsured", e.ToInsured), new SqlParameter("@CCInsured", e.CCInsured), new SqlParameter("@ToInsurers", e.ToInsurers), new SqlParameter("@CCInsurers", e.CCInsurers), new SqlParameter("@ToBrokers", e.ToBrokers), new SqlParameter("@CCBrokers", e.CCBrokers),new SqlParameter("@FrequencyOfReminder",e.FrequencyOfReminder) };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspMarineLorEmailIdsNewSave", _param));
        }
        public CSMarineLorEmailIdsNew getMarineLorEmailIdsNew(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID",claimId)};
            _dt = _sf.returnDTWithProc_executeReader("uspMarineLorEmailIdsNewGet",_param);
            CSMarineLorEmailIdsNew e = new CSMarineLorEmailIdsNew();
            if (_dt.Rows.Count>0)
            {
                e.PK_LE_ID = Convert.ToInt32(_dt.Rows[0]["PK_LE_ID"]);
                e.ToEmailIds =Convert.ToString("["+ _dt.Rows[0]["ToEmailIds"]+"]");
                e.CCEmailIds = Convert.ToString("[" + _dt.Rows[0]["CCEmailIds"] + "]");
                e.ToInsured = Convert.ToBoolean(_dt.Rows[0]["ToInsured"]);
                e.CCInsured = Convert.ToBoolean(_dt.Rows[0]["CCInsured"]);
                e.ToInsurers = Convert.ToBoolean(_dt.Rows[0]["ToInsurers"]);
                e.CCInsurers = Convert.ToBoolean(_dt.Rows[0]["CCInsurers"]);
                e.ToBrokers = Convert.ToBoolean(_dt.Rows[0]["ToBrokers"]);
                e.CCBrokers = Convert.ToBoolean(_dt.Rows[0]["CCBrokers"]);
                e.FrequencyOfReminder = Convert.ToInt32(_dt.Rows[0]["FrequencyOfReminder"]);
            }
            return e;
        }

        public int saveLogEmailMarineLor(CSLogEmailMarineLor l)
        {
            _param = new SqlParameter[] {new SqlParameter("@PK_Log_ID",l.PK_Log_ID), new SqlParameter("@LE_ID", l.LE_ID), new SqlParameter("@ToEmailIds", l.ToEmailIds), new SqlParameter("@CCEmailIds", l.CCEmailIds), new SqlParameter("@Subject", l.Subject), new SqlParameter("@MailMsg", l.MailMsg)};
            return _sf.executeNonQueryWithProc("uspLogEmailMarineLorSave", _param);
        }
        public DataTable getLogEmailMarineLor_using_byDays()
        {
            _dt = _sf.returnDTWithProc_executeReader("uspLogEmailMarineLorGet_using_byDays");
            return _dt;
        }

        public int saveMarineLorChattingNew(CSMarineLorChattingNew lc)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_LC_ID", lc.PK_LC_ID), new SqlParameter("@ClaimID", lc.ClaimID), new SqlParameter("@UserID", lc.UserID), new SqlParameter("@UserType", lc.UserType), new SqlParameter("@ChattingText", lc.ChattingText)};
             return Convert.ToInt32(_sf.executeScalerWithProc("uspMarineLorChattingNewSave", _param));
        }
       public List<CSMarineLorChattingNew> getMarineLorChattingNew(int claimId,int userId)
        {
            _param = new SqlParameter[] {new SqlParameter("@ClaimID",claimId),new SqlParameter("@UserID",userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineLorChattingNewGet",_param);
            List<CSMarineLorChattingNew> listChatting = new List<CSMarineLorChattingNew>();
            if(_dt.Rows.Count>0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineLorChattingNew c = new CSMarineLorChattingNew();
                    c.PK_LC_ID = Convert.ToInt32(row["PK_LC_ID"]);
                    c.ChattingText = Convert.ToString(row["ChattingText"]);
                    c.UserName = Convert.ToString(row["UserName"]);
                    c.IsMyData = Convert.ToBoolean(row["IsMyData"]);
                    c.DocumentName = Convert.ToString(row["DocumentName"]);
                    c.CrtDT = Convert.ToDateTime(row["CrtDT"]);
                    listChatting.Add(c);
                }
                
            }
            return listChatting;
        }
    }
}
