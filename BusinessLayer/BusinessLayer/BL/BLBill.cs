﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BLBill
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLBill()
        {
            _sf = new SqlFunctions();
        }
        #region Bill 
        public List<CSMarineModeOfConveyance> getModeOfConveyance()
        {
            List<CSMarineModeOfConveyance> listMC = new List<CSMarineModeOfConveyance>();
            _dt = _sf.returnDTWithProc_executeReader("uspMarineModeOfConveyanceGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineModeOfConveyance mc = new CSMarineModeOfConveyance();
                    mc.PK_MC_ID = Convert.ToInt32(row["PK_MC_ID"]);
                    mc.ModeOfConveyance = row["ModeOfConveyance"].ToString();
                    listMC.Add(mc);
                }
            }
            return listMC;

        }
        public int saveBillVisitDetail(CSBillVisitDetail vd)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_VD_ID", vd.PK_VD_ID), new SqlParameter("@ClaimID", vd.ClaimID), new SqlParameter("@UserID", vd.UserID), new SqlParameter("@SV_ID", vd.PK_ID), new SqlParameter("@KMRun", vd.KMRun), new SqlParameter("@MOC_ID", vd.MOC_ID), new SqlParameter("@RatePerKM", vd.RatePerKM), new SqlParameter("@DA", vd.DA), new SqlParameter("@NoOfPhotos", vd.NoOfPhotos), new SqlParameter("@NoOfHours", vd.NoOfHours), new SqlParameter("@ProfFee", vd.ProfFee), new SqlParameter("@BH_ID", vd.BH_ID), new SqlParameter("@RowWiseAmont", vd.RowWiseAmont) };
            return _sf.executeNonQueryWithProc("uspBillVisitDetailSave", _param);
        }
        public List<CSBillVisitDetail> getBillVisitDetail(int claimId, int userId, int bh_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@BH_ID", bh_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspBillVisitDetailGet", _param);
            List<CSBillVisitDetail> listVD = new List<CSBillVisitDetail>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBillVisitDetail vd = new CSBillVisitDetail();

                    vd.PK_VD_ID = Convert.ToInt32(row["PK_VD_ID"]);
                    vd.StartDate = row["StartDate"].ToString();
                    vd.EndDate = row["EndDate"].ToString();
                    //vd.SV_ID = Convert.ToInt32(row["SV_ID"]);
                    vd.KMRun = Convert.ToDecimal(row["KMRun"]);
                    vd.MOC_ID = Convert.ToInt32(row["MOC_ID"]);
                    vd.RatePerKM = Convert.ToDecimal(row["RatePerKM"]);
                    vd.DA = Convert.ToDecimal(row["DA"]);
                    vd.NoOfPhotos = Convert.ToInt32(row["NoOfPhotos"]);
                    vd.NoOfHours = Convert.ToString(row["NoOfHours"]);
                    vd.ProfFee = Convert.ToDecimal(row["ProfFee"]);
                    vd.RowWiseAmont = Convert.ToDecimal(row["RowWiseAmont"]);
                    listVD.Add(vd);
                }
            }
            return listVD;
        }
        public List<CSBillVisitDetail> getDate_byClaimID(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("usp_getDate_byClaimID", _param);
            List<CSBillVisitDetail> listSV = new List<CSBillVisitDetail>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBillVisitDetail vd = new CSBillVisitDetail();
                    vd.PK_ID = Convert.ToInt32(row["PK_ID"]);
                    vd.StartDate = row["StartDate"].ToString();
                    vd.EndDate = row["EndDate"].ToString();
                    listSV.Add(vd);
                }
            }
            return listSV;
        }
        public int saveBillExpensesReimbursement(CSBillExpensesReimbursement er)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_ER_ID", er.PK_ER_ID), new SqlParameter("@ClaimID", er.ClaimID), new SqlParameter("@UserID", er.UserID), new SqlParameter("@ExpenseDT", er.ExpenseDT), new SqlParameter("@Description", er.Description), new SqlParameter("@Amount", er.Amount), new SqlParameter("@BH_ID", er.BH_ID) }; return _sf.executeNonQueryWithProc("uspBillExpensesReimbursementSave", _param);
        }
        public List<CSBillExpensesReimbursement> getBillExpensesReimbursement(int claimId, int userId, int bh_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@BH_ID", bh_Id) };
            List<CSBillExpensesReimbursement> listER = new List<CSBillExpensesReimbursement>();
            _dt = _sf.returnDTWithProc_executeReader("uspBillExpensesReimbursementGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBillExpensesReimbursement er = new CSBillExpensesReimbursement();
                    er.PK_ER_ID = Convert.ToInt32(row["PK_ER_ID"]);
                    er.ExpenseDT = row["ExpenseDT"].ToString();
                    er.Description = row["Description"].ToString();
                    er.Amount = Convert.ToDecimal(row["Amount"]);
                    listER.Add(er);
                }
            }
            return listER;

        }
        public int deleteBillExpensesReimbursement(int pk_Er_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_ER_ID", pk_Er_Id), new SqlParameter("@UserID", userId) }; return _sf.executeNonQueryWithProc("uspBillExpensesReimbursementDelete", _param);
        }


        public string getInsrClaimNo_byClaimID(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            return Convert.ToString(_sf.executeScalerWithProc("usp_getInsrClaimNo_byClaimID", _param));

        }
        public List<CSBillHeader> getBillHeader(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspBillHeaderGet", _param);
            List<CSBillHeader> listBH = new List<CSBillHeader>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSBillHeader bh = new CSBillHeader();
                    bh.PK_BH_ID = Convert.ToInt32(row["PK_BH_ID"]);
                    bh.InvoiceNo = row["InvoiceNo"].ToString();
                    bh.InvoiceAmount = Convert.ToDecimal(row["InvoiceAmount"]);
                    bh.CrtDT = row["CrtDT"].ToString();
                    listBH.Add(bh);
                }


            }
            return listBH;

        }

        public int saveBillHeader(CSBillHeader bh)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_BH_ID",bh.PK_BH_ID), new SqlParameter("@ClaimID", bh.ClaimID), new SqlParameter("@UserID", bh.UserID), new SqlParameter("@ClaimNo", bh.ClaimNo),new SqlParameter("@InvoiceAmount",bh.InvoiceAmount)
            };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspBillHeaderSave", _param));
        }


        #endregion
        #region Payment Receive
        public string getJobNo_byClaimID(int claimId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            return Convert.ToString(_sf.executeScalerWithProc("usp_getJobNo_byClaimID", _param));

        }
        public List<CSPaymentReceive> getPaymentReceive(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            List<CSPaymentReceive> listPR = new List<CSPaymentReceive>();
            _dt = _sf.returnDTWithProc_executeReader("uspPaymentReceiveGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSPaymentReceive pr = new CSPaymentReceive();
                    pr.PK_PR_ID = Convert.ToInt32(row["PK_PR_ID"]);
                    pr.BH_ID = Convert.ToInt32(row["BH_ID"]);
                    pr.ReceiveDT = Convert.ToString(row["ReceiveDT"]);
                    pr.UTRN = Convert.ToString(row["UTRN"]);
                    pr.FeeAmount = Convert.ToDecimal(row["FeeAmount"]);
                    pr.ReasonForDifference = Convert.ToString(row["ReasonForDifference"]);
                    pr.Status = Convert.ToString(row["Status"]);
                    pr.PK_BH_ID = Convert.ToInt32(row["PK_BH_ID"]);
                    pr.InvoiceNo = row["InvoiceNo"].ToString();
                    pr.InvoiceDT = row["CrtDT"].ToString();
                    pr.InvoiceAmount = Convert.ToDecimal(row["InvoiceAmount"]);
                    listPR.Add(pr);
                }
            }
            return listPR;

        }
        public int savePaymentReceive(CSPaymentReceive pr)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_PR_ID", pr.PK_PR_ID), new SqlParameter("@ClaimID", pr.ClaimID), new SqlParameter("@UserID", pr.UserID), new SqlParameter("@BH_ID", pr.PK_BH_ID), new SqlParameter("@ReceiveDT", pr.ReceiveDT), new SqlParameter("@UTRN", pr.UTRN), new SqlParameter("@FeeAmount", pr.FeeAmount), new SqlParameter("@ReasonForDifference", pr.ReasonForDifference), new SqlParameter("@Status", pr.Status) };
            return _sf.executeNonQueryWithProc("uspPaymentReceiveSave", _param);
        }
        #endregion
        #region Reminder For Fee
        public List<CSReminderForFree> getReminderForFree(int claimId, int userId, int days)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@Days", days) };
            _dt = _sf.returnDTWithProc_executeReader("uspReminderForFreeGet", _param);
            List<CSReminderForFree> listRF = new List<CSReminderForFree>();
            if (_dt.Rows.Count > 0)
            {

                foreach (DataRow row in _dt.Rows)
                {
                    CSReminderForFree rf = new CSReminderForFree();
                    rf.RefNo = row["ClaimID"].ToString();
                    rf.InstructionDate = row["InstructionDate"].ToString();
                    rf.InsurerClaimNo = row["InsrClaimNo"].ToString();
                    rf.InvoiceNo = row["InvoiceNo"].ToString();
                    rf.PolicyNo = row["PolicyNo"].ToString();
                    rf.DepartmentName = row["DeptName"].ToString();
                    rf.InvoiceDT = row["CrtDT"].ToString();
                    rf.InvoiceAmount = Convert.ToDecimal(row["InvoiceAmount"]);
                    rf.InsurerName = row["Name"].ToString();
                    rf.AgeingOfBills = Convert.ToInt32(row["AgeingOfBills"]);
                    rf.PK_BH_ID = Convert.ToInt32(row["PK_BH_ID"]);
                    rf.Reminder_Green = Convert.ToInt32(row["Reminder_Green"]);
                    rf.Reminder_Yellow = Convert.ToInt32(row["Reminder_Yellow"]);
                    rf.Reminder_Red = Convert.ToInt32(row["Reminder_Red"]);

                    listRF.Add(rf);
                }
            }
            return listRF;
        }
        public int saveReminderForFeeHeader(CSReminderForFeeHeader rh)
        {
            _param = new SqlParameter[]
            {
                new SqlParameter("@ClaimID", rh.ClaimID), new SqlParameter("@UserID", rh.UserID), new SqlParameter("@Insurer_ID", rh.Insurer_ID), new SqlParameter("@OfficeCode", rh.OfficeCode), new SqlParameter("@Address", rh.Address) };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspReminderForFeeHeaderSave", _param));
        }
        public int saveReminderForFree(CSReminderForFree rf) { _param = new SqlParameter[] { new SqlParameter("@ClaimID", rf.ClaimID), new SqlParameter("@UserID", rf.UserID), new SqlParameter("@BH_ID", rf.PK_BH_ID), new SqlParameter("@RFH_ID", rf.RFH_ID), new SqlParameter("@AgeingOfBills", rf.AgeingOfBills), new SqlParameter("@Reminder_Green", rf.Reminder_Green), new SqlParameter("@Reminder_Yellow", rf.Reminder_Yellow), new SqlParameter("@Reminder_Red", rf.Reminder_Red), }; return _sf.executeNonQueryWithProc("uspReminderForFeeSave", _param); }

        public List<CSReminderForFree> getReminderForFree_byRF_ID(int claimId, int userId, int rfh_Id)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@RFH_ID", rfh_Id) };
            _dt = _sf.returnDTWithProc_executeReader("usp_getReminderForFree_byRF_ID", _param);
            List<CSReminderForFree> listRF = new List<CSReminderForFree>();
            if (_dt.Rows.Count > 0)
            {

                foreach (DataRow row in _dt.Rows)
                {
                    CSReminderForFree rf = new CSReminderForFree();
                    rf.RefNo = row["ClaimID"].ToString();
                    rf.InstructionDate = row["InstructionDate"].ToString();
                    rf.InsurerClaimNo = row["InsrClaimNo"].ToString();
                    rf.InvoiceNo = row["InvoiceNo"].ToString();
                    rf.PolicyNo = row["PolicyNo"].ToString();
                    rf.DepartmentName = row["DeptName"].ToString();
                    rf.InvoiceDT = row["CrtDT"].ToString();
                    rf.InvoiceAmount = Convert.ToDecimal(row["InvoiceAmount"]);
                    rf.InsurerName = row["Name"].ToString();
                    rf.AgeingOfBills = Convert.ToInt32(row["AgeingOfBills"]);
                    rf.PK_BH_ID = Convert.ToInt32(row["PK_BH_ID"]);
                    rf.Reminder_Green = Convert.ToInt32(row["Reminder_Green"]);
                    rf.Reminder_Yellow = Convert.ToInt32(row["Reminder_Yellow"]);
                    rf.Reminder_Red = Convert.ToInt32(row["Reminder_Red"]);

                    listRF.Add(rf);
                }
            }
            return listRF;
        }


        public List<CSReminderForFeeHeader> getReminderForFeeHeader(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspReminderForFeeHeaderGet", _param);
            List<CSReminderForFeeHeader> listRH = new List<CSReminderForFeeHeader>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSReminderForFeeHeader rh = new CSReminderForFeeHeader();
                    rh.PK_RFH_ID = Convert.ToInt32(row["PK_RFH_ID"]);
                    rh.Insurer_ID = Convert.ToInt32(row["Insurer_ID"]);
                    rh.OfficeCode = Convert.ToString(row["OfficeCode"]);
                    rh.Address = Convert.ToString(row["Address"]);
                    rh.InsurerName = Convert.ToString(row["Name"]);
                    rh.CrtDT = Convert.ToString(row["CrtDT"]);
                    listRH.Add(rh);
                }
            }
            return listRH;

        }

        #endregion

    }
}
