﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BLEmailLog
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLEmailLog()
        {
            _sf = new SqlFunctions();
        }

        public int saveEmailLog(CSEmailLog e)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", e.ClaimID), new SqlParameter("@UserID", e.UserID), new SqlParameter("@ToEmailIds", e.ToEmailIds), new SqlParameter("@CCEmailIds", e.CCEmailIds), new SqlParameter("@Subject", e.Subject), new SqlParameter("@MailMsg", e.MailMsg), new SqlParameter("@Task", e.Task)};
            return _sf.executeNonQueryWithProc("uspEmailLogSave", _param); }
        public DataTable getEmailIdsForAotomaticMarineLorReminder()
        {
            _dt = _sf.returnDTWithProc_executeReader("usp_getEmailIdsForAotomaticMarineLorReminder");
            return _dt;
        }

    }
}
