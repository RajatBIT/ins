﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BLEmailGroupMst
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLEmailGroupMst()
        {
            _sf = new SqlFunctions();
        }
        public int saveEmailGroupMst(CSEmailGroupMst egm)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_EGM_ID", egm.PK_EGM_ID), new SqlParameter("@GroupName", egm.GroupName) };
            return _sf.executeNonQueryWithProc("uspEmailGroupMstASave", _param);
        }
        public List<CSEmailGroupMst> getEmailGroupMst()
        {
            List<CSEmailGroupMst> listEGM = new List<CSEmailGroupMst>();
            _dt = _sf.returnDTWithProc_executeReader("uspEmailGroupMstGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSEmailGroupMst egm = new CSEmailGroupMst();
                    egm.PK_EGM_ID = Convert.ToInt32(row["PK_EGM_ID"]);
                    egm.GroupName =row["GroupName"].ToString();
                    listEGM.Add(egm);
                }
                
            }
            return listEGM;
        }
        public int saveEmailGroup(CSEmailGroup eg)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_EG_ID", eg.PK_EG_ID), new SqlParameter("@EGM_ID", eg.EGM_ID),new SqlParameter("@EmailId", eg.EmailId) };
            return _sf.executeNonQueryWithProc("uspEmailGroupSave", _param);
        }
        public List<CSEmailGroup> getEmailGroup()
        {
            List<CSEmailGroup> listEG = new List<CSEmailGroup>();
            _dt = _sf.returnDTWithProc_executeReader("uspEmailGroupGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSEmailGroup eg = new CSEmailGroup();
                    eg.PK_EG_ID =Convert.ToInt32(row["PK_EG_ID"]);
                    eg.EGM_ID = Convert.ToInt32(row["EGM_ID"]);
                    eg.EmailId = row["EmailId"].ToString();
                    eg.GroupName = row["GroupName"].ToString();
                    listEG.Add(eg);
                }

            }
            return listEG;
        }
        public DataTable getEmailId_byGroupId(int groupId)
        {
            _dt = _sf.returnDTWithQuery_executeReader("select EmailId from tblemailgroup where EGM_ID="+groupId+"");
            return _dt;
        }
    }
}
