﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class BLMarineNoClaim
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public BLMarineNoClaim()
        {
            _sf = new SqlFunctions();
        }

        #region No Claim
        public List<CSMarineReasonForNoClaim> getReasonForNoClaim()
        {
            List<CSMarineReasonForNoClaim> listRC = new List<CSMarineReasonForNoClaim>();
            _dt = _sf.returnDTWithProc_executeReader("uspMarineReasonForNoClaimGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineReasonForNoClaim rc = new CSMarineReasonForNoClaim();
                    rc.PK_RC_ID = Convert.ToInt32(row["PK_RC_ID"]);
                    rc.ReasonForNoClaim = row["ReasonForNoClaim"].ToString();
                    listRC.Add(rc);
                }
            }
            return listRC;
        }
       
       
        public int saveNoClaim(CSMarineNoClaim nc)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_NC_ID", nc.PK_NC_ID), new SqlParameter("@ClaimID", nc.ClaimID), new SqlParameter("@UserID", nc.UserID), new SqlParameter("@RC_ID", nc.RC_ID), new SqlParameter("@SurveyorsRemarks", nc.SurveyorsRemarks), }; return _sf.executeNonQueryWithProc("uspMarineNoClaimSave", _param);
        }
        public CSMarineNoClaim getNoClaim(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineNoClaimGet",_param);
            CSMarineNoClaim nc = new CSMarineNoClaim();
            if (_dt.Rows.Count > 0)
            {
                
                nc.PK_NC_ID = Convert.ToInt32(_dt.Rows[0]["PK_NC_ID"]);
                nc.RC_ID = Convert.ToInt32(_dt.Rows[0]["RC_ID"]);
                nc.SurveyorsRemarks = Convert.ToString(_dt.Rows[0]["SurveyorsRemarks"]);
            }
            return nc;
        }
        #endregion
    }
}

