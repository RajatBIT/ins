﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer.BL
{
    public class CommonService
    {

        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        public CommonService()
        {
            _sf = new SqlFunctions();
        }

        public object executeScalarWithQuery(string query)
        {
            return _sf.executeScalerWithQuery(query);
        }
        public int executeNonQueryWithQuery(string query)
        {
            return _sf.executeNonQueryWithQuery(query);
        }
        public DataTable returnDTWithQuery_executeReader(string query)
        {
            return _sf.returnDTWithQuery_executeReader(query);
        }

        public int saveTaskManager(int claimId, int userId, string shortName)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@ShortName", shortName) };
            return _sf.executeNonQueryWithProc("uspTaskManagerSave", _param);
        }
        public List<CSPendingTask> getPendingTask(int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("usp_getPendingTask", _param);
            List<CSPendingTask> listPendingTask = new List<CSPendingTask>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSPendingTask p = new CSPendingTask();
                    p.ClaimID = Convert.ToString(row["ClaimID"]);
                    p.TM_ID = Convert.ToInt32(row["TM_ID"]);

                    listPendingTask.Add(p);
                }
            }
            return listPendingTask;
        }
        public List<CSTaskMaster> getTaskMaster()
        {
            List<CSTaskMaster> listTaskMaster = new List<CSTaskMaster>();
            _dt = _sf.returnDTWithProc_executeReader("uspTaskMasterGet");
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSTaskMaster tm = new CSTaskMaster();
                    tm.PK_TM_ID = Convert.ToInt32(row["PK_TM_ID"]);
                    tm.Task = Convert.ToString(row["Task"]);
                    listTaskMaster.Add(tm);
                }
            }
            return listTaskMaster;
        }

        public List<CSAllTaskWithPendingTask> getAllTaskWithPendingTask(int userId)
        {
            List<CSAllTaskWithPendingTask> listAllTaskWithPendingTask = new List<CSAllTaskWithPendingTask>();

            var listTaskM = getTaskMaster();
            var listAllPTask = getPendingTask(userId);

            listTaskM.ForEach(x =>
            {
                CSAllTaskWithPendingTask o = new CSAllTaskWithPendingTask();

                o.task = x;
                o.listPendingTask = listAllPTask.Where(y => y.TM_ID == x.PK_TM_ID).ToList();

                listAllTaskWithPendingTask.Add(o);
 
            });

            return listAllTaskWithPendingTask;
        }

    }
}
