﻿using BusinessLayer.DAL;
using BusinessLayer.Models;
using DataAccessLayer;
using InsuranceSurvey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace BusinessLayer
{
    public class BLGenrateClaim
    {
        SqlParameter[] _param;
        DataTable _dt;
        SqlFunctions _sf;
        #region Local Variables
        static DataBaseLayer dbManager;
        #endregion

        public BLGenrateClaim()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
            _sf = new SqlFunctions();
        }

        static BLGenrateClaim()
        {
            dbManager = new DataBaseLayer(DataManagerFactory.DataProvider.SqlServer, DBSP.ConnectionString);
        }

        public String GenrateClaimOneTime(CSGenrateClaim objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                dbManager.AddParameters("@InsdType", objProperty.InsdType);
                dbManager.AddParameters("@Name", objProperty.Name);
                dbManager.AddParameters("@Email", objProperty.Email);
                dbManager.AddParameters("@Mobile", objProperty.Mobile);
                dbManager.AddParameters("@Address", objProperty.Address);
                dbManager.AddParameters("@PolicyNo", objProperty.PolicyNo);
                dbManager.AddParameters("@FK_Department", objProperty.FkDepartment);
                dbManager.AddParameters("@FK_SurID", objProperty.FkSurId);
                dbManager.AddParameters("@FK_InsrID", objProperty.FkInsrId);
                dbManager.AddParameters("@InsrAddress", objProperty.InsrAddress);
                dbManager.AddParameters("@InsrClaimNo", objProperty.InsrClaimNo);
                dbManager.AddParameters("@InstructorName", objProperty.InstructorName);
                dbManager.AddParameters("@InstructionDate", objProperty.InstructionDate);
                dbManager.AddParameters("@IEmail", objProperty.IEmail);
                dbManager.AddParameters("@ReportedLoss", objProperty.ReportedLoss);
                dbManager.AddParameters("@SurveyType", objProperty.SurveyType);
                dbManager.AddParameters("@VRegNo", objProperty.VRegNo);

                dbManager.AddParameters("@LocationOfSurvey", objProperty.LocationOfSurvey);
                dbManager.AddParameters("@NatureOfLossId", objProperty.NatureOfLossId);

                result = dbManager.ExecuteNonQuery(DBSP.uspGenrateClaim, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable ViewMyClaims(CSGenrateClaim objProperty)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_SurID", objProperty.FkSurId);
                dbManager.AddParameters("@ClaimID", objProperty.InsrClaimNo);
                dbManager.AddParameters("@FromClaimDate", objProperty.ClaimDateFrom);
                dbManager.AddParameters("@ToClaimDate", objProperty.ClaimDateTo);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewMyClaims, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static CSGenrate GetInsuredDetails(string ClaimID)
        {
            CSGenrate dtReult = new CSGenrate();
            dtReult.GenrateClaim = new CSGenrateClaim();
            dtReult.MarineJIR = new CSMarineJIR();
            try
            {
                DataTable Result = new DataTable();
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                Result = dbManager.ExecuteDataTable(DBSP.uspGetInsuredDetails, CommandType.StoredProcedure);
                dtReult.GenrateClaim.InstructorName = Convert.ToString(Result.Rows[0]["InsdName"]);
                dtReult.GenrateClaim.Address = Convert.ToString(Result.Rows[0]["InsdAddress"]);

                dtReult.MarineJIR.InsdId = Convert.ToInt32(Result.Rows[0]["Fk_InsdID"]);

                dtReult.MarineJIR.InsdType = Convert.ToString(Result.Rows[0]["InsdType"]);
                dtReult.MarineJIR.Insured = Convert.ToString(Result.Rows[0]["InsdName"]);
                dtReult.MarineJIR.InsuredAddress = Convert.ToString(Result.Rows[0]["InsdAddress"]);

            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;

        }

        public static CSGenrate GetViewMyClaims(string ClaimID, string FK_SurID)
        {
            CSGenrate dtReult = new CSGenrate();
            dtReult.GenrateClaim = new CSGenrateClaim();
            dtReult.MarineJIR = new CSMarineJIR();
            try
            {
                DataTable Result = new DataTable();
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_SurID", FK_SurID);
                dbManager.AddParameters("@ClaimID", ClaimID);
                Result = dbManager.ExecuteDataTable(DBSP.uspViewMyClaims, CommandType.StoredProcedure);

                foreach (DataRow row in Result.Rows)
                {
                    dtReult.GenrateClaim.FkInsrId = Convert.ToInt32(row["Fk_InsdID"]);
                    if (Convert.ToString(row["InsdType"]) == "One Time")
                        dtReult.GenrateClaim.InsdType = "O";

                    dtReult.GenrateClaim.InstructorName = Convert.ToString(row["InsdName"]);
                    dtReult.GenrateClaim.Address = Convert.ToString(row["InsdAddress"]);

                    dtReult.MarineJIR.InsdId = Convert.ToInt32(row["Fk_InsdID"]);

                    dtReult.MarineJIR.InsdType = Convert.ToString(row["InsdType"]);


                    dtReult.MarineJIR.Insured = Convert.ToString(row["InsdName"]);
                    dtReult.MarineJIR.InsuredAddress = Convert.ToString(row["InsdAddress"]);
                }

            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }


        /*===================================Old Code Write By Nvn Malik=====================================================*/
        //public String MarineSubmitJIR(CSMarineJIR objProperty)
        //{
        //    string result = "";
        //    try
        //    {
        //        dbManager.ConnectionOpen();
        //        dbManager.AddParameters("@PK_ID", objProperty.PkJIRId);
        //        dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
        //        dbManager.AddParameters("@FK_ConsignmentType", objProperty.FkConsignmentType.TrimEnd(','));
        //        dbManager.AddParameters("@InsuredName", objProperty.Insured);
        //        dbManager.AddParameters("@InsuredAddress", objProperty.InsuredAddress);
        //        dbManager.AddParameters("@DateOfSurvey", objProperty.DateOfSurvey);
        //        dbManager.AddParameters("@Consignment", objProperty.Consiment);
        //        dbManager.AddParameters("@NatureOfTransit", objProperty.NatureOfTransit);
        //        dbManager.AddParameters("@Consignor", objProperty.Consignor);
        //        dbManager.AddParameters("@Consignee", objProperty.Consignee);
        //        dbManager.AddParameters("@ArrivalDateDestination", objProperty.ArrivalDateDestination);
        //        dbManager.AddParameters("@DeliveryDateConsignee", objProperty.DeliveryDateConsignee);
        //        dbManager.AddParameters("@DispatchedTo", objProperty.DispatchedTo);
        //        dbManager.AddParameters("@DispatchedFrom", objProperty.DispatchedFrom);
        //        dbManager.AddParameters("@CarryingLorryRegNo", objProperty.CarryingLorryRegNo);
        //        dbManager.AddParameters("@ConsignmentWeight", objProperty.ConsignmentWeight);
        //        dbManager.AddParameters("@DescriptionPacking", objProperty.DescriptionPacking);
        //        dbManager.AddParameters("@ExConditionPacking", objProperty.ExConditionPacking);
        //        dbManager.AddParameters("@ResoneForDelay", objProperty.ResoneForDelay);
        //        dbManager.AddParameters("@InsdType", objProperty.InsdType);
        //        dbManager.AddParameters("@InsdID", objProperty.InsdId);
        //        dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
        //        dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
        //        result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitJIR, "@Flag", CommandType.StoredProcedure);
        //    }
        //    catch
        //    {
        //        result = "error";
        //    }
        //    finally
        //    {
        //        dbManager.ConnectionClose();
        //    }
        //    return result;
        //}

        /*===================================New Code send by Ashish=====================================================*/
        public String MarineSubmitJIR(CSMarineJIR objProperty)
        {
            string result = "";
            try
            {
                //dbManager.ConnectionOpen();
                SqlParameter[] param = new SqlParameter[] {

                new SqlParameter("@PK_ID", objProperty.PkJIRId),
                new SqlParameter("@FK_ClaimID", objProperty.FKClaimId),
                new SqlParameter("@FK_ConsignmentType", objProperty.FkConsignmentType.TrimEnd(',')),
                new SqlParameter("@InsuredName", objProperty.Insured),
                new SqlParameter("@InsuredAddress", objProperty.InsuredAddress),
                new SqlParameter("@DateOfSurvey", objProperty.DateOfSurvey),
                new SqlParameter("@PlaceofSurvey", objProperty.PlaceofSurvey),
                new SqlParameter("@Consignment", objProperty.Consiment),
                new SqlParameter("@NatureOfTransit", objProperty.NatureOfTransit),
                new SqlParameter("@Consignor", objProperty.Consignor),
                new SqlParameter("@Consignee", objProperty.Consignee),
                new SqlParameter("@ArrivalDateDestination", objProperty.ArrivalDateDestination),
                new SqlParameter("@DeliveryDateConsignee", objProperty.DeliveryDateConsignee),
                new SqlParameter("@DispatchedTo", objProperty.DispatchedTo),
                new SqlParameter("@DispatchedFrom", objProperty.DispatchedFrom),
                new SqlParameter("@CarryingLorryRegNo", objProperty.CarryingLorryRegNo),
                new SqlParameter("@ConsignmentWeight", objProperty.ConsignmentWeight),
                new SqlParameter("@DescriptionPacking", objProperty.DescriptionPacking),
                new SqlParameter("@ExConditionPacking", objProperty.ExConditionPacking),
                new SqlParameter("@ResoneForDelay", objProperty.ResoneForDelay),
                new SqlParameter("@ConsignorAddress", objProperty.ConsignorAddress),
                new SqlParameter("@ConsigeeAddress", objProperty.ConsigeeAddress),
                new SqlParameter("@CarryingLorryGVW", objProperty.CarryingLorryGVW),
                new SqlParameter("@CarryingLorryULW", objProperty.CarryingLorryULW),
                new SqlParameter("@InsdType", objProperty.InsdType),
                new SqlParameter("@InsdID", objProperty.InsdId),
                new SqlParameter("@EntryBy", objProperty.EntryBy),
                new SqlParameter("@IPAddress", objProperty.IPAddress),
                new SqlParameter("@CauseofLoss", objProperty.CauseOfLoss),
                new SqlParameter("@DelayInINTimation", objProperty.DelayInIntimation),
                new SqlParameter("@DelayINTakingDelivery", objProperty.DelayIntakingDelivery),
                new SqlParameter("@LoadOnCarryingVehicle", objProperty.LoadOnCarryingVehicle),
                new SqlParameter("@ConsignmentTransshipedTo", objProperty.ConsignmentTransshipedTo),
                new SqlParameter("@LoadCapacityOfCarryVehicle", objProperty.LoadCapacityOfCarryVehicle),
                new SqlParameter("@WhoIsInsured", objProperty.WhoIsInsured),
                new SqlParameter("@TypeOfContract", objProperty.TypeOfContract),
                new SqlParameter("@IncotermSale", objProperty.IncotermSale),
                new SqlParameter("@PointDeliveryConsignee", objProperty.PointDeliveryConsignee),
                new SqlParameter("@CauseOfLossOther",objProperty.CauseOfLossOther),new SqlParameter("@ExConditionPackingOther",objProperty.ExConditionPackingOther),
                new SqlParameter("@PlaceOfAccident",objProperty.PlaceOfAccident),new SqlParameter("@SpotSurvey",objProperty.SpotSurvey),
                new SqlParameter("@PoliceReport",objProperty.PoliceReport),new SqlParameter("@InsuredClaimNo",objProperty.InsuredClaimNo)

            };
                result = Convert.ToString(_sf.executeScalerWithProc(DBSP.uspMarineSubmitJIR, param));

                //result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitJIR, "@Flag", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }
        /*========================================================================================*/

        public String MarineSubmitConsignmentDocs(CSMarineConsignmentDocs objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objProperty.PKId);
                dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
                dbManager.AddParameters("@Description", objProperty.Description);
                dbManager.AddParameters("@InvNo", objProperty.InvNo);
                dbManager.AddParameters("@InvDate", objProperty.InvDate);
                dbManager.AddParameters("@InvQty", objProperty.InvQty);
                dbManager.AddParameters("@UOM", objProperty.Uom);
                dbManager.AddParameters("@GRNo", objProperty.GRNo);
                dbManager.AddParameters("@GRDate", objProperty.GRDate);
                dbManager.AddParameters("@BLNo", objProperty.BLNo);
                dbManager.AddParameters("@BLDate", objProperty.BLDate);
                dbManager.AddParameters("@BENo", objProperty.BENo);
                dbManager.AddParameters("@BEDate", objProperty.BEDate);
                dbManager.AddParameters("@AWBNo", objProperty.AWBNo);
                dbManager.AddParameters("@AWBDate", objProperty.AWBDate);
                dbManager.AddParameters("@RRNo", objProperty.RRNo);
                dbManager.AddParameters("@RRDate", objProperty.RRDate);
                dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitConsignmentDocs, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable MarineGetConsignmentDocs(string FkClaimID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimID);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineGetConsignmentDocs, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static Int32 MarineDeleteConsignmentDocs(int PkId)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", PkId);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineDeleteConsignmentDocs, CommandType.StoredProcedure);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable MarineGetDescriptionConsignmentDocs(int FkClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineGetDescriptionConsignmentDocs, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }
        public String getLocationOfSurvey(int claimId, int userId)
        {
            return Convert.ToString(_sf.executeScalerWithQuery("select LocationOfSurvey from  tblGeneratedClaims where PK_ID=" + claimId + " and FK_SurID=" + userId + ""));

        }
        public int updateLocationOfSurvey(int claimId, int userId, string locationOfSurvey)
        {
            locationOfSurvey = locationOfSurvey.Replace("'", "''");
            return _sf.executeNonQueryWithQuery("update tblGeneratedClaims set LocationOfSurvey='" + locationOfSurvey + "' where PK_ID=" + claimId + " and FK_SurID=" + userId + "");

        }
        public static CSMarineJIR MarineGetJIR(string FK_ClaimID)
        {
            DataSet dsResult = new DataSet();
            CSMarineJIR objProperty = new CSMarineJIR();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FK_ClaimID);
                dsResult = dbManager.ExecuteDataSet(DBSP.uspMarineGetJIR, CommandType.StoredProcedure);
                foreach (DataRow row in dsResult.Tables[0].Rows)
                {
                    objProperty.PkJIRId = (int)row["PK_ID"];
                    objProperty.FKClaimId = Convert.ToString(row["FK_ClaimID"]);
                    objProperty.DateOfSurvey = Convert.ToString(row["DateOfSurvey"]);
                    objProperty.Consiment = Convert.ToString(row["Consignment"]);
                    objProperty.NatureOfTransit = Convert.ToString(row["NatureOfTransit"]);
                    objProperty.ResoneForDelay = Convert.ToString(row["ResoneForDelay"]);
                    objProperty.Consignor = Convert.ToString(row["Consignor"]);
                    objProperty.Consignee = Convert.ToString(row["Consignee"]);
                    objProperty.ArrivalDateDestination = Convert.ToString(row["ArrivalDateDestination"]);
                    objProperty.DeliveryDateConsignee = Convert.ToString(row["DeliveryDateConsignee"]);
                    objProperty.DispatchedFrom = Convert.ToString(row["DispatchedFrom"]);
                    objProperty.DispatchedTo = Convert.ToString(row["DispatchedTo"]);
                    objProperty.CarryingLorryRegNo = Convert.ToString(row["CarryingLorryRegNo"]);
                    objProperty.ConsignmentWeight = Convert.ToString(row["ConsignmentWeight"]);
                    objProperty.DescriptionPacking = Convert.ToString(row["DescriptionPacking"]);
                    objProperty.ExConditionPacking = Convert.ToString(row["ExConditionPacking"]);
                    objProperty.CauseOfLoss = Convert.ToString(row["CauseOfLoss"]);
                    objProperty.DelayInIntimation = Convert.ToString(row["DelayInINTimation"]);
                    objProperty.DelayIntakingDelivery = Convert.ToString(row["DelayINTakingDelivery"]);
                    objProperty.ConsignorAddress = Convert.ToString(row["ConsignorAddress"]);
                    objProperty.ConsigeeAddress = Convert.ToString(row["ConsigeeAddress"]);
                    objProperty.CarryingLorryGVW = Convert.ToString(row["CarryingLorryGVW"]);
                    objProperty.CarryingLorryULW = Convert.ToString(row["CarryingLorryULW"]);
                    objProperty.InsdType = Convert.ToString(row["InsdType"]);
                    objProperty.InsdId = Convert.ToInt32(row["FkInsrId"]);
                    //objProperty.PlaceofSurvey = Convert.ToString(row["PlaceofSurvey"]);
                    objProperty.LoadOnCarryingVehicle = Convert.ToString(row["LoadOnCarryingVehicle"]);
                    objProperty.ConsignmentTransshipedTo = Convert.ToString(row["ConsignmentTransshipedTo"]);
                    objProperty.LoadCapacityOfCarryVehicle = Convert.ToString(row["LoadCapacityOfCarryVehicle"]);
                    objProperty.WhoIsInsured = Convert.ToString(row["WhoIsInsured"]);
                    objProperty.TypeOfContract = Convert.ToString(row["TypeOfContract"]);
                    objProperty.IncotermSale = Convert.ToString(row["IncotermSale"]);
                    objProperty.PointDeliveryConsignee = Convert.ToString(row["PointDeliveryConsignee"]);
                    objProperty.CauseOfLossOther = Convert.ToString(row["CauseOfLossOther"]);
                    objProperty.ExConditionPackingOther = Convert.ToString(row["ExConditionPackingOther"]);
                    objProperty.PlaceOfAccident = Convert.ToString(row["PlaceOfAccident"]);
                    objProperty.SpotSurvey = Convert.ToString(row["SpotSurvey"]);
                    objProperty.PoliceReport = Convert.ToString(row["PoliceReport"]);
                    objProperty.InsuredClaimNo = Convert.ToString(row["InsuredClaimNo"]);
                }

                foreach (DataRow row in dsResult.Tables[1].Rows)
                {
                    objProperty.ContactPerson = Convert.ToString(row["NAME"]);
                    objProperty.MobileNumber = Convert.ToString(row["Mobile"]);
                    objProperty.EmailId = Convert.ToString(row["Email"]);
                }

            }
            catch
            {
                dsResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }

        public static DataTable MarineGetDescriptionConsignmentDocsEdit(string PkId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PkId", PkId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineGetDescriptionConsignmentDocsEdit, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public String MarineSubmitDamageDetails(CSMarineDamageDetails objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objProperty.PkId);
                dbManager.AddParameters("@FK_DesID", objProperty.FKDesId);
                dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
                dbManager.AddParameters("@AffectedQuantity", objProperty.AffectedQuantity);
                dbManager.AddParameters("@Remark", objProperty.Remark);
                dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                dbManager.AddParameters("@RemarkOther", objProperty.RemarkOther);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitDamageDetails, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable MarineGetDamageDetails(int FkClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineGetDamageDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static Int32 MarineDeleteDamageDetails(int PkId)
        {
            int result = 0;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", PkId);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineDeleteDamageDetails, CommandType.StoredProcedure);
            }
            catch
            {
                result = 0;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String InsertMarineRemark(CSMarineJIR objProperty)
        {
            string result = "";
            try
            {
                DataTable dtLOR = new DataTable();
                dtLOR.Columns.Add("FK_ID");
                if (objProperty.CheckBoxList != null)
                {
                    foreach (string item in objProperty.CheckBoxList)
                    {
                        dtLOR.Rows.Add(item);
                    }
                }
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Fk_ClaimId", objProperty.FKClaimId);
                dbManager.AddParameters("@CauseofLoss", objProperty.CauseOfLoss);
                dbManager.AddParameters("@DelayInINTimation", objProperty.DelayInIntimation);
                dbManager.AddParameters("@DelayINTakingDelivery", objProperty.DelayIntakingDelivery);
                dbManager.AddParameters("@DtmarineRemarks", dtLOR);
                //dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IpAddress", objProperty.IPAddress);
                result = dbManager.ExecuteNonQuery(DBSP.uspInsertMarineRemark, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String InsertMarineLOR(CSMarineJIR objProperty)
        {
            string result = "";
            try
            {
                DataTable dtLOR = new DataTable();
                dtLOR.Columns.Add("FK_ID");
                dtLOR.Columns.Add("LorType");
                objProperty.Xml = System.Web.HttpUtility.UrlDecode(objProperty.Xml, System.Text.Encoding.Default);
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.LoadXml(objProperty.Xml);
                XmlNodeList nodeList = xmlDoc.DocumentElement.SelectNodes("/recodes/recode");
                string LorType = "", LorId = "";
                foreach (XmlNode node in nodeList)
                {
                    DataRow row = dtLOR.NewRow();
                    LorType = node.SelectSingleNode("LorType").InnerText;
                    LorId = node.SelectSingleNode("LorId").InnerText;
                    row[0] = LorId;
                    row[1] = LorType;

                    dtLOR.Rows.Add(row);
                    dtLOR.AcceptChanges();
                    //price = node.SelectSingleNode("Product_price").InnerText;
                    //MessageBox.Show(proID + " " + proName + " " + price);
                }


                //foreach (string item in objProperty.CheckBoxList)
                //{
                //    dtLOR.Rows.Add(item);
                //}

                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Fk_ClaimId", objProperty.FKClaimId);
                dbManager.AddParameters("@dtMarineLOR", dtLOR);
                dbManager.AddParameters("@IpAddress", objProperty.IPAddress);

                dbManager.AddParameters("@LOR13", objProperty.LOR13);
                dbManager.AddParameters("@LOR14", objProperty.LOR14);


                dbManager.AddParameters("@Name", objProperty.ContactPerson);
                dbManager.AddParameters("@Mobile", objProperty.MobileNumber);
                dbManager.AddParameters("@Email", objProperty.EmailId);

                int result1 = dbManager.ExecuteNonQuery(DBSP.uspInsertMarineLOR, CommandType.StoredProcedure);
                if (result1 > 0)
                {
                    result = "I";
                }

            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String MarineContactPersonJIR(CSMarineJIR objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@mode", "IN");
                dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
                dbManager.AddParameters("@Name", objProperty.ContactPerson);
                dbManager.AddParameters("@Mobile", objProperty.MobileNumber);
                dbManager.AddParameters("@Email", objProperty.EmailId);
                int result1 = dbManager.ExecuteNonQuery(DBSP.uspMarineContactPersonJIR, CommandType.StoredProcedure);
                if (result1 > 0)
                {
                    result = "I";
                }
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable GetConsignmentType(string fkClaimID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", 1);
                dbManager.AddParameters("@FK_ClaimID", fkClaimID);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetConsignmentType, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public String AddMarineSubmitISVR(CSMarineISVR objProperty)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PK_ID", objProperty.PkId);
                //dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                //dbManager.AddParameters("@EstimationOfLoss", objProperty.EstimationOfLoss);
                //dbManager.AddParameters("@RecomendedReserve", objProperty.RecomendedReserve);
                //dbManager.AddParameters("@BasisOfReserve", objProperty.BasisOfReserve);
                //dbManager.AddParameters("@Remarks", objProperty.Remarks);
                //dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                //dbManager.AddParameters("@Task", objProperty.Task);
                //dbManager.AddParameters("@SurveyorsFinalRemarks", objProperty.SurveyorsFinalRemarks);
                ////dbManager.AddParameters("@XML", objProperty.XML);
                _param = new SqlParameter[] {
                new SqlParameter("@PK_ID", objProperty.PkId),
                new SqlParameter("@FK_ClaimID", objProperty.FkClaimId),
                new SqlParameter("@EstimationOfLoss", objProperty.EstimationOfLoss),
                new SqlParameter("@RecomendedReserve", objProperty.RecomendedReserve),
                new SqlParameter("@BasisOfReserve", objProperty.BasisOfReserve),
                new SqlParameter("@Remarks", objProperty.Remarks),
                new SqlParameter("@EntryBy", objProperty.EntryBy),
                new SqlParameter("@Task", objProperty.Task),
              new SqlParameter("@SurveyorsFinalRemarks", objProperty.SurveyorsFinalRemarks)
            };
                result = Convert.ToString(_sf.executeScalerWithProc(DBSP.uspMarineSubmitISVR, _param));
                //result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitISVR, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }
        public int saveMarineISVRNotes(CSMarineISVRNotes n)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_N_ID", n.PK_N_ID), new SqlParameter("@ClaimID", n.ClaimID), new SqlParameter("@UserID", n.UserID), new SqlParameter("@Notes", n.Notes)
            }; return _sf.executeNonQueryWithProc("uspMarineISVRNotesSave", _param);
        }
        public int saveMarineFSRNotes(CSMarineISVRNotes n)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_FsrN_ID", n.PK_FsrN_ID), new SqlParameter("@ClaimID", n.ClaimID), new SqlParameter("@UserID", n.UserID), new SqlParameter("@Notes", n.Notes)
            }; return _sf.executeNonQueryWithProc("uspMarineFSRNotesSave", _param);
        }
        public int deleteMarineISVRNotes(int pk_N_Id, int userId)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_N_ID",pk_N_Id), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMarineISVRNotesDelete", _param);
        }
        public int deleteMarineFSRNotes(int pk_FsrN_Id, int userId)
        {
            _param = new SqlParameter[] {
                new SqlParameter("@PK_FsrN_ID",pk_FsrN_Id), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMarineFSRNotesDelete", _param);
        }
        public List<CSMarineISVRNotes> getMarineISVRNotes(int claimId, int userId)
        {
            List<CSMarineISVRNotes> listNt = new List<CSMarineISVRNotes>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineISVRNotesGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineISVRNotes nt = new CSMarineISVRNotes();
                    nt.PK_N_ID = Convert.ToInt32(row["PK_N_ID"]);
                    nt.Notes = row["Notes"].ToString();
                    listNt.Add(nt);
                }

            }
            return listNt;
        }
        public List<CSMarineISVRNotes> getMarineFSRNotes(int claimId, int userId)
        {
            List<CSMarineISVRNotes> listNt = new List<CSMarineISVRNotes>();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineFSRNotesGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineISVRNotes nt = new CSMarineISVRNotes();
                    nt.PK_FsrN_ID = Convert.ToInt32(row["PK_FsrN_ID"]);
                    nt.Notes = row["Notes"].ToString();
                    listNt.Add(nt);
                }

            }
            return listNt;
        }


        public static String AddMarineSubmitISVRPhtotoDocument(string FkClaimId, string CallType, string PkId, string PhtotoType, string PhtotoName, string PhtotoPath, string IPAddress, string ImgType)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@CallType", CallType);
                dbManager.AddParameters("@PkId", PkId);
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                dbManager.AddParameters("@PhtotoType", PhtotoType);
                dbManager.AddParameters("@PhtotoName", PhtotoName);
                dbManager.AddParameters("@PhtotoPath", PhtotoPath);
                dbManager.AddParameters("@IPAddress", IPAddress);
                dbManager.AddParameters("@ImgType", ImgType);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitISVRPhotoDocument, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public string UpdatePolicyParticulars(CSGenrateClaim objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                dbManager.AddParameters("@PolicyNo", objProperty.PolicyNo);
                dbManager.AddParameters("@Period", objProperty.Period);
                dbManager.AddParameters("@TransitInsured", objProperty.TransitInsured);
                dbManager.AddParameters("@ModeConveyance", objProperty.ModeConveyance);
                dbManager.AddParameters("@Coverage", objProperty.Coverage);
                dbManager.AddParameters("@SubjectMatter", objProperty.SubjectMatter);
                dbManager.AddParameters("@BasisValution", objProperty.BasisValution);
                dbManager.AddParameters("@PolicyExcess", objProperty.PolicyExcess);
                dbManager.AddParameters("@IPAddress", objProperty.IpAddress);
                dbManager.AddParameters("@ModeConveyanceOther", objProperty.ModeConveyanceOther);
                dbManager.AddParameters("@PolicyTypeText", objProperty.PolicyTypeText);
                result = dbManager.ExecuteNonQuery(DBSP.uspUpdatePolicyParticulars, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static CSGenrateClaim GetPolicyParticulars(string fkClaimid)
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", fkClaimid);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetPolicyParticulars, CommandType.StoredProcedure);
                objProperty.PkPolicyId = 0;
                if (dtResult.Rows.Count > 0)
                {
                    foreach (DataRow row in dtResult.Rows)
                    {
                        objProperty.PkPolicyId = Convert.ToInt32(row["PK_PolicyID"]);
                        objProperty.Period = Convert.ToString(row["Period"]);
                        objProperty.TransitInsured = Convert.ToString(row["TransitInsured"]);
                        objProperty.Coverage = Convert.ToString(row["Coverage"]);
                        objProperty.SubjectMatter = Convert.ToString(row["SubjectMatter"]);
                        objProperty.BasisValution = Convert.ToString(row["BasisValution"]);
                        objProperty.PolicyExcess = Convert.ToString(row["PolicyExcess"]);
                        objProperty.ModeConveyance = Convert.ToString(row["ModeConveyance"]);
                        objProperty.InstructorName = Convert.ToString(row["InsdName"]);
                        objProperty.InsrAddress = Convert.ToString(row["InsdAddress"]);
                        objProperty.InsrClaimNo = Convert.ToString(row["InsrClaimNo"]);
                        objProperty.InsdType = Convert.ToString(row["InsdType"]);
                        objProperty.PolicyType = Convert.ToString(row["PolicyType"]);
                        objProperty.PolicyNo = Convert.ToString(row["PolicyNo"]);
                        objProperty.ModeConveyanceOther = Convert.ToString(row["ModeConveyanceOther"]);
                        objProperty.PolicyTypeText = Convert.ToString(row["PolicyTypeText"]);
                    }
                }
                objProperty.FkClaimId = fkClaimid;
            }
            catch (Exception ex)
            {
                objProperty = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }

        public static List<CSMarineTranshipment> GetMarineTranshipment(string FKClaimId, string task)
        {
            DataTable dtReult = new DataTable();
            List<CSMarineTranshipment> objProperty = new List<CSMarineTranshipment>();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Task", task);
                dbManager.AddParameters("@FK_ClaimID", FKClaimId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspMarineTranshipment, CommandType.StoredProcedure);
                foreach (DataRow row in dtReult.Rows)
                {
                    CSMarineTranshipment obj = new CSMarineTranshipment();
                    obj.PkId = (int)row["PK_ID"];
                    obj.FKClaimId = Convert.ToString(row["FK_ClaimID"]);
                    obj.Mode = (string)row["Mode"];
                    obj.LoadingPlace = (string)row["LoadingPlace"];
                    obj.UnLoadingPlace = (string)row["UnLoadingPlace"];
                    obj.TypeNo = (string)row["TypeNo"];
                    obj.TypeDate = (string)row["TypeDate"];
                    obj.DateArrival = (string)row["DateArrival"];
                    obj.DateDischarge = (string)row["DateDischarge"];
                    obj.NameOfCarrier = (string)row["NameOfCarrier"];
                    obj.Comment = (string)row["Comment"];
                    obj.Remark = (string)row["Remark"];
                    objProperty.Add(obj);
                }
            }
            catch
            {
                objProperty = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }

        public static CSMarineTranshipment GetSingelMarineTranshipment(string pkId)
        {
            DataTable dtReult = new DataTable();
            CSMarineTranshipment objProperty = new CSMarineTranshipment();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Task", "SelectInd");
                dbManager.AddParameters("@PK_ID", pkId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspMarineTranshipment, CommandType.StoredProcedure);
                foreach (DataRow row in dtReult.Rows)
                {
                    objProperty.PkId = (int)row["PK_ID"];
                    objProperty.FKClaimId = Convert.ToString(row["FK_ClaimID"]);
                    objProperty.Mode = (string)row["Mode"];
                    objProperty.LoadingPlace = (string)row["LoadingPlace"];
                    objProperty.UnLoadingPlace = (string)row["UnLoadingPlace"];
                    objProperty.TypeNo = (string)row["TypeNo"];
                    objProperty.TypeDate = (string)row["TypeDate"];
                    objProperty.DateArrival = (string)row["DateArrival"];
                    objProperty.DateDischarge = (string)row["DateDischarge"];
                    objProperty.NameOfCarrier = (string)row["NameOfCarrier"];
                    objProperty.Comment = (string)row["Comment"].ToString();
                    objProperty.Remark = (string)row["Remark"];
                    objProperty.CommentPhotoOrDoc = row["CommentPhotoOrDoc"].ToString();
                    objProperty.LegOfJourney = row["LegOfJourney"].ToString();
                }
            }
            catch (Exception ex)
            {
                objProperty = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return objProperty;
        }

        public DataTable ActionWithMarineTranshipment(CSMarineTranshipment objProperty)
        {
            DataTable _dt = new DataTable();
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@PK_ID", objProperty.PkId);
                //dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
                //dbManager.AddParameters("@Mode", objProperty.Mode);
                //dbManager.AddParameters("@LoadingPlace", objProperty.LoadingPlace);
                //dbManager.AddParameters("@UnLoadingPlace", objProperty.UnLoadingPlace);
                //dbManager.AddParameters("@TypeNo", objProperty.TypeNo);
                //dbManager.AddParameters("@TypeDate", objProperty.TypeDate);
                //dbManager.AddParameters("@DateArrival", objProperty.DateArrival);
                //dbManager.AddParameters("@DateDischarge", objProperty.DateDischarge);
                //dbManager.AddParameters("@NameOfCarrier", objProperty.NameOfCarrier);
                //dbManager.AddParameters("@Comment", objProperty.Comment);
                //dbManager.AddParameters("@Remark", objProperty.Remark);
                //dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                //dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                //dbManager.AddParameters("@Task", objProperty.Task);
                //dbManager.AddParameters("@CommentPhotoOrDoc",objProperty.CommentPhotoOrDoc);

                SqlParameter[] param = new SqlParameter[] {
                    new SqlParameter("@PK_ID", objProperty.PkId),new SqlParameter("@FK_ClaimID", objProperty.FKClaimId),new SqlParameter("@Mode", objProperty.Mode),new SqlParameter("@LoadingPlace", objProperty.LoadingPlace),new SqlParameter("@UnLoadingPlace", objProperty.UnLoadingPlace),new SqlParameter("@TypeNo", objProperty.TypeNo),new SqlParameter("@TypeDate", objProperty.TypeDate),
                    new SqlParameter("@DateArrival", objProperty.DateArrival),new SqlParameter("@DateDischarge", objProperty.DateDischarge),new SqlParameter("@NameOfCarrier", objProperty.NameOfCarrier),new SqlParameter("@Comment", objProperty.Comment),new SqlParameter("@Remark", objProperty.Remark),new SqlParameter("@EntryBy", objProperty.EntryBy),new SqlParameter("@IPAddress", objProperty.IPAddress),new SqlParameter("@Task", objProperty.Task),new SqlParameter("@LegOfJourney",objProperty.LegOfJourney)

                };

                //result = dbManager.ExecuteNonQuery(DBSP.uspMarineTranshipment, "@Flag", CommandType.StoredProcedure);
                //_dt = new SqlFunctions().returnDTWithProc_adapter(DBSP.uspMarineTranshipment, param);
                _dt = new SqlFunctions().returnDTWithProc_executeReader(DBSP.uspMarineTranshipment, param);
            }
            catch (Exception ex)
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return _dt;
        }

        public static DataTable GetMarineAssessmentDetails(CSGenrateClaim objProperty)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Task", objProperty.AssessmentType);
                dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                dbManager.AddParameters("@FK_DesID", objProperty.FK_DesID);
                dbManager.AddParameters("@AffectedQuantityC", objProperty.AffectedQuantityC);
                dbManager.AddParameters("@UnitRateC", objProperty.UnitRateC);
                dbManager.AddParameters("@AmountC", objProperty.AmountC);
                dbManager.AddParameters("@AffectedQuantityA", objProperty.AffectedQuantityA);
                dbManager.AddParameters("@UnitRateA", objProperty.UnitRateA);
                dbManager.AddParameters("@AmountA", objProperty.AmountA);
                dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IPAddress", objProperty.IpAddress);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineAssessmentDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public String ActionSurveyVisits(CSSurveyorVisits objProperty)
        {
            string result = string.Empty;
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@PK_ID", objProperty.PkId);
                dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                dbManager.AddParameters("@StartDate", objProperty.StartDate);
                dbManager.AddParameters("@EndDate", objProperty.EndDate);
                dbManager.AddParameters("@PlaceVisit", objProperty.PlaceVisit);
                dbManager.AddParameters("@PurposeVisit", objProperty.PurposeVisit);
                dbManager.AddParameters("@Remark", objProperty.Remark);
                dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                dbManager.AddParameters("@Task", objProperty.Task);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSurveyorVisits, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable GetSurveyVisits(string claimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Task", "GetVisit");
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineSurveyorVisits, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }
        //uspMarineSurveyorVisits

        public String AddMarineAssesment(CSGenrateClaim objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                //dbManager.AddParameters("@Task", objProperty.Task);
                //dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                //dbManager.AddParameters("@CalMode", objProperty.CalMode);
                //dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                //dbManager.AddParameters("@IPAddress", objProperty.IpAddress);
                //dbManager.AddParameters("@XML", objProperty.XMLFile);
                //result = dbManager.ExecuteNonQuery(DBSP.uspMarineClaimedCalculation, "@Flag", CommandType.StoredProcedure);
                _param = new SqlParameter[] {
                new SqlParameter("@FK_ClaimID", objProperty.FkClaimId),
                new SqlParameter("@CalMode", objProperty.CalMode),
                new SqlParameter("@EntryBy", objProperty.EntryBy),
                new SqlParameter("@IPAddress", objProperty.IpAddress),
                new SqlParameter("@UnitRateC",objProperty.UnitRateC),new SqlParameter("@AffectedQuantityC",objProperty.AffectedQuantityC),new SqlParameter("@AmountC",objProperty.AmountC),new SqlParameter("@MD_ID",objProperty.MD_ID),new SqlParameter("@AD_ID",objProperty.AD_ID),new SqlParameter("@UnitRateA",objProperty.UnitRateA),new SqlParameter("@AffectedQuantityA",objProperty.AffectedQuantityA),new SqlParameter("@AmountA",objProperty.AmountA),new SqlParameter("@FK_DesID",objProperty.FK_DesID)
                //new SqlParameter("@XML", objProperty.XMLFile)
             
            };
                result = _sf.executeScalerWithProc("uspMarineAssesmentDetails_New", _param).ToString();
                //result = __sf DBSP.uspMarineClaimedCalculation, "@Flag", CommandType.StoredProcedure);
            }
            catch (Exception ex)
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }


        public int saveMarineClaimedCalculation(CSGenrateClaim objProperty)
        {
            _param = new SqlParameter[] {new SqlParameter("@EntryBy", objProperty.EntryBy),new SqlParameter("@FK_CalID",objProperty.FKCalId)
                ,new SqlParameter("@CalMode",objProperty.CalMode),new SqlParameter("@CalType",objProperty.CalType),new SqlParameter("@Value",objProperty.Value),new SqlParameter("@Result",objProperty.Result),new SqlParameter("@IPAddress",objProperty.IpAddress),
                new SqlParameter("@Rate",objProperty.Rate),new SqlParameter("@FK_ClaimID",objProperty.FkClaimId)
            };
            return _sf.executeNonQueryWithProc("uspMarineClaimedCalculation_New", _param);
        }


        public static DataTable GetMarineAssesment(string task, string CalMode, string fkCalimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Task", task);
                dbManager.AddParameters("@CalMode", CalMode);
                dbManager.AddParameters("@FK_ClaimID", fkCalimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineClaimedCalculation, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable GetMarineISVR(string claimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetMarineISVR, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable GetMarineISVRPhotos(string claimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetMarineISVRPhotos, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable GetMarineISVRDocument(string claimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetMarineISVRDocument, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataSet GetMarineJIR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineJIR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        public static DataSet GetMarineISVR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineISVR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }


        public static DataTable ViewInsuredClaims(CSGenrateClaim objProperty)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FkInsdId", objProperty.FkInsrId);
                dbManager.AddParameters("@ClaimID", objProperty.FkClaimId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewInsuredClaims, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static CSGenrate GetInsuredClaims(CSGenrateClaim objProperty)
        {
            CSGenrate dtReult = new CSGenrate();
            dtReult.GenrateClaim = new CSGenrateClaim();
            dtReult.MarineJIR = new CSMarineJIR();
            try
            {
                DataTable Result = new DataTable();
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FkInsdId", objProperty.FkInsrId);
                dbManager.AddParameters("@ClaimID", objProperty.FkClaimId);
                Result = dbManager.ExecuteDataTable(DBSP.uspViewInsuredClaims, CommandType.StoredProcedure);

                foreach (DataRow row in Result.Rows)
                {
                    dtReult.GenrateClaim.FkInsrId = Convert.ToInt32(row["Fk_InsdID"]);
                    if (Convert.ToString(row["InsdType"]) == "One Time")
                        dtReult.GenrateClaim.InsdType = "O";

                    dtReult.GenrateClaim.InstructorName = Convert.ToString(row["InsdName"]);
                    dtReult.GenrateClaim.Address = Convert.ToString(row["InsdAddress"]);

                    dtReult.MarineJIR.InsdId = Convert.ToInt32(row["Fk_InsdID"]);

                    dtReult.MarineJIR.InsdType = Convert.ToString(row["InsdType"]);


                    dtReult.MarineJIR.Insured = Convert.ToString(row["InsdName"]);
                    dtReult.MarineJIR.InsuredAddress = Convert.ToString(row["InsdAddress"]);
                }

            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        // By Ashish
        public static DataSet GetMarineLOR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspMarineGetLORDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        // By Ashish
        public static DataSet GetMarineLORReminder(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspUpdateMarineRemindersLOR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        // By Ashish
        public static DataSet GetMarineFSR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineFSR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        // By Ashish
        public static DataSet GetMarineNoClaim(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                //dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineNoClaim, CommandType.StoredProcedure);
                //rajat pdf proc
                dsMarine = dbManager.ExecuteDataSet("uspPDFMarineNoClaim", CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        // By Ashish
        public static DataSet GetMarineSpotSurvey(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineFSR, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        public static DataSet GetMarineClaimForm(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.uspPDFMarineClaimForm, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }

        public String SubmitFSR(CSFSR objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", objProperty.FKClaimId);
                dbManager.AddParameters("@Salvage", objProperty.Salvage);
                dbManager.AddParameters("@CommentPolicy", objProperty.CommentPolicy);
                dbManager.AddParameters("@Concurrence", objProperty.Concurrence);
                dbManager.AddParameters("@OtherRemark", objProperty.OtherRemark);
                dbManager.AddParameters("@EntryBy", objProperty.EntryBy);
                dbManager.AddParameters("@IPAddress", objProperty.IPAddress);
                dbManager.AddParameters("@CommentPolicy_Com_NotCom", objProperty.CommentPolicy_Com_NotCom);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitFSR, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public String LORPlaceOfSurvey(string PlaceOfSurvey, string FkClaimId)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                dbManager.AddParameters("@PlaceofSurvey", PlaceOfSurvey);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineUpdatePlaceOfSurvey, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable GetMarineFSR(string FkClaimId)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@ClaimID", FkClaimId);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetMarineFSR, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public String AssignedTask(CSTraineeSurveyorRegistration objProperty)
        {
            string result = "";
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Mode", objProperty.Mode);
                dbManager.AddParameters("@FK_ClaimID", objProperty.FkClaimId);
                dbManager.AddParameters("@FK_Department", objProperty.FKDepartment);
                dbManager.AddParameters("@FK_SurID", objProperty.FkSurId);
                dbManager.AddParameters("@FK_TSURID", objProperty.PkTsurId);
                result = dbManager.ExecuteNonQuery(DBSP.uspMarineSubmitFSR, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable GetAssignedTask(string Mode, string FkSurID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Mode", Mode);
                dbManager.AddParameters("@FK_SurID", FkSurID);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetMarineFSR, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static DataTable GetTraineeClaims(CSGenrateClaim objProperty)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_SurID", objProperty.FkSurId);
                dbManager.AddParameters("@ClaimID", objProperty.InsrClaimNo);
                //dbManager.AddParameters("@FromClaimDate", objProperty.ClaimDateFrom);
                //dbManager.AddParameters("@ToClaimDate", objProperty.ClaimDateTo);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspViewTraineeClaims, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        public static DataTable GetClaimDetailsForEdit(string claimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", claimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspGetClaimDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }



        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        // By Ashish
        public static DataTable GetMotorLOR(string ClaimID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMotorGetLORDetails, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable MotorGetSurveyorRemarks(int FkClaimId)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", FkClaimId);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMotorGetSurveyorRemarks, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public String InsertMotorRemark(CSMotorSurvey objProperty)
        {
            string result = "";
            try
            {
                DataTable dtLOR = new DataTable();
                dtLOR.Columns.Add("FK_ID");
                foreach (string item in objProperty.CheckBoxList)
                {
                    dtLOR.Rows.Add(item);
                }

                dbManager.ConnectionOpen();
                dbManager.AddParameters("@Fk_ClaimId", objProperty.FK_ClaimID);
                dbManager.AddParameters("@EstimatedLoss", objProperty.EstimatedLoss);
                dbManager.AddParameters("@RecommendedLoss", objProperty.RecommendedLoss);
                dbManager.AddParameters("@RemarksISVR", objProperty.RemarksISVR);
                dbManager.AddParameters("@DtmarineRemarks", dtLOR);
                result = dbManager.ExecuteNonQuery(DBSP.uspInsertMotorRemark, "@Flag", CommandType.StoredProcedure);
            }
            catch
            {
                result = "Error";
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return result;
        }

        public static DataTable MarineGetLORReminders(string ClaimID)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dtResult = dbManager.ExecuteDataTable(DBSP.uspMarineGetLORReminders, CommandType.StoredProcedure);
            }
            catch
            {
                dtResult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtResult;
        }

        public static DataTable GetLicenseType(string fkClaimID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", fkClaimID);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetLicenseType, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }
        public static DataTable GetLossType(string fkClaimID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", fkClaimID);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetLossType, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }
        public static DataTable GetDamageType(string fkClaimID)
        {
            DataTable dtReult = new DataTable();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", fkClaimID);
                dtReult = dbManager.ExecuteDataTable(DBSP.uspGetDamageType, CommandType.StoredProcedure);
            }
            catch
            {
                dtReult = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dtReult;
        }

        #region MISC ISVR
        public static DataSet GetMIS_ISVR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = new DataSet();
            try
            {
                dbManager.ConnectionOpen();
                dbManager.AddParameters("@FK_ClaimID", ClaimID);
                dbManager.AddParameters("@InsdType", InsdType);
                dbManager.AddParameters("@InsdID", InsdID);
                dsMarine = dbManager.ExecuteDataSet(DBSP.usp_GetMIS_ISVR_PDF, CommandType.StoredProcedure);
            }
            catch
            {
                dsMarine = null;
            }
            finally
            {
                dbManager.ConnectionClose();
            }
            return dsMarine;
        }


        #endregion
        #region Marine MOM
        public DataTable getRefNo_And_InsuredName_byClaimID(int claimId)
        {

            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId) };
            _dt = _sf.returnDTWithProc_executeReader("getRefNo_And_InsuredName_byClaimID", _param);
            //if(_dt.Rows.Count>0)
            //{
            //     RefNo = _dt.Rows[0]["RefNo"].ToString();
            //     InsuredName = _dt.Rows[0]["InsuredName"].ToString();
            //}
            return _dt;
        }
        public int saveMOMData(CSMarineMOMData md)
        {
            int md_Id = 0;
            _param = new SqlParameter[] {new SqlParameter("@PK_MD_ID",md.PK_MD_ID),
                new SqlParameter("@UserID", md.UserID), new SqlParameter("@ClaimID", md.ClaimID), new SqlParameter("@SV_ID", md.SV_ID), new SqlParameter("@Date", md.Date), new SqlParameter("@Agenda", md.Agenda), new SqlParameter("@Venue", md.Venue)
            };
            md_Id = Convert.ToInt32(_sf.executeScalerWithProc("uspMarineMOMDataSave", _param));
            return md_Id;
        }
        public int saveMOMAttendees(CSMarineMOMAttendees ma)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_At_ID", ma.PK_At_ID), new SqlParameter("@MD_ID", ma.MD_ID), new SqlParameter("@MR_OR_MRS", ma.MR_OR_MRS), new SqlParameter("@NameOfAttendee", ma.NameOfAttendee), new SqlParameter("@Designation", ma.Designation), new SqlParameter("@NameOfOrganization", ma.NameOfOrganization), new SqlParameter("@Representing", ma.Representing), new SqlParameter("@MobileNo", ma.MobileNo), new SqlParameter("@EmailId", ma.EmailId) };
            return _sf.executeNonQueryWithProc("uspMarineMOMAttendeesSave", _param);
        }
        public int deleteMOMAttendees(int pk_At_ID)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_At_ID", pk_At_ID) };
            return _sf.executeNonQueryWithProc("uspMarineMOMAttendeesDelete", _param);
        }
        public int deleteMOMDiscussion(int pk_Ds_ID)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_Ds_ID", pk_Ds_ID) };
            return _sf.executeNonQueryWithProc("uspMarineMOMDiscussionDelete", _param);
        }
        public int saveMOMDiscussion(CSMarineMOMDiscussion ds)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_Ds_ID", ds.PK_Ds_ID), new SqlParameter("@MD_ID", ds.MD_ID), new SqlParameter("@Discussion", ds.Discussion) };
            return _sf.executeNonQueryWithProc("uspMarineMOMDiscussionSave", _param);
        }
        public CSMarineMOMData getMOMData(int claimId, int userId, int sv_Id)
        {
            CSMarineMOMData md = new CSMarineMOMData();
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@SV_ID", sv_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineMOMDataGet", _param);
            if (_dt.Rows.Count > 0)
            {
                md.PK_MD_ID = Convert.ToInt32(_dt.Rows[0]["PK_MD_ID"]);
                md.Date = _dt.Rows[0]["Date"].ToString();
                md.Agenda = _dt.Rows[0]["Agenda"].ToString();
                md.Venue = _dt.Rows[0]["Venue"].ToString();

            }
            return md;
        }
        public List<CSMarineMOMAttendees> getMOMAttendees(int md_Id)
        {
            List<CSMarineMOMAttendees> listMA = new List<CSMarineMOMAttendees>();
            _param = new SqlParameter[] { new SqlParameter("@MD_ID", md_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineMOMAttendeesGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineMOMAttendees ma = new CSMarineMOMAttendees();
                    ma.PK_At_ID = Convert.ToInt32(row["PK_At_ID"]);
                    ma.MD_ID = Convert.ToInt32(row["MD_ID"]);
                    ma.MR_OR_MRS = Convert.ToString(row["MR_OR_MRS"]);
                    ma.NameOfAttendee = Convert.ToString(row["NameOfAttendee"]);
                    ma.Designation = Convert.ToString(row["Designation"]);
                    ma.NameOfOrganization = Convert.ToString(row["NameOfOrganization"]);
                    ma.Representing = Convert.ToString(row["Representing"]);
                    ma.MobileNo = Convert.ToString(row["MobileNo"]);
                    ma.EmailId = Convert.ToString(row["EmailId"]);
                    listMA.Add(ma);
                }
            }
            return listMA;
        }
        public List<CSMarineMOMDiscussion> getMOMDiscussion(int md_Id)
        {
            List<CSMarineMOMDiscussion> listDS = new List<CSMarineMOMDiscussion>();
            _param = new SqlParameter[] { new SqlParameter("@MD_ID", md_Id) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineMOMDiscussionGet", _param);
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineMOMDiscussion ds = new CSMarineMOMDiscussion();
                    ds.PK_Ds_ID = Convert.ToInt32(row["PK_Ds_ID"]);
                    ds.MD_ID = Convert.ToInt32(row["MD_ID"]);
                    ds.Discussion = row["Discussion"].ToString();
                    listDS.Add(ds);
                }
            }
            return listDS;
        }
        #endregion
        #region Marine Assessment Detail
        public List<CSMarineCalulationMaster> getParticular(string calMode)
        {
            List<CSMarineCalulationMaster> listCM = new List<CSMarineCalulationMaster>();
            _dt = _sf.returnDTWithProc_executeReader("uspMarineCalulationMasterGet");
            listCM.Insert(0, new CSMarineCalulationMaster {PK_ID=0,CalDescription="Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineCalulationMaster cm = new CSMarineCalulationMaster();
                    cm.PK_ID = Convert.ToInt32(row["PK_ID"]);
                    cm.CalDescription = row["CalDescription"].ToString();
                    listCM.Add(cm);
                }
            }
            return listCM;
        }
        public List<CSMarineAssessmentDetailDesc> getMarineAssessmentDetailDesc(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineAssessmentDetails_Desc_Get_New", _param);
            List<CSMarineAssessmentDetailDesc> listAD = new List<CSMarineAssessmentDetailDesc>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineAssessmentDetailDesc ad = new CSMarineAssessmentDetailDesc();
                    ad.PK_AD_ID = Convert.ToInt32(row["PK_AD_ID"]);
                    ad.Description = row["Description"].ToString();
                    ad.InvNo = row["InvNo"].ToString();
                    ad.InvDate = row["InvDate"].ToString();
                    ad.InvQty = row["InvQty"].ToString();
                    ad.UOM = row["UOM"].ToString();
                    ad.AffectedQuantity = row["AffectedQuantity"].ToString();
                    //ad.ClaimID = Convert.ToInt32(row["FK_ClaimID"]);
                    ad.FK_DesID = Convert.ToInt32(row["FK_DesID"]);
                    ad.AffectedQuantityC = Convert.ToInt32(row["AffectedQuantityC"]);
                    ad.UnitRateC = Convert.ToDecimal(row["UnitRateC"]);
                    ad.AmountC = Convert.ToDecimal(row["AmountC"]);
                    ad.AffectedQuantityA = Convert.ToInt32(row["AffectedQuantityA"]);
                    ad.UnitRateA = Convert.ToDecimal(row["UnitRateA"]);
                    ad.AmountA = Convert.ToDecimal(row["AmountA"]);
                    ad.MD_ID = Convert.ToInt32(row["MD_ID"]);
                    ad.AH_ID = Convert.ToInt32(row["AH_ID"]);

                    listAD.Add(ad);
                }
            }
            return listAD;
        }
        public List<CSMarineConsignmentDocs> getDescription_byClaimID(int claimId, int userId)
        {
            List<CSMarineConsignmentDocs> listCD = new List<CSMarineConsignmentDocs>();
            _dt = _sf.returnDTWithQuery_executeReader("select PK_ID,Description from tblMarineConsignmentDocs where FK_ClaimID=" + claimId + " and EntryBy=" + userId + "");
            listCD.Insert(0, new CSMarineConsignmentDocs { PKId = 0, Description = "Select" });
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineConsignmentDocs cd = new CSMarineConsignmentDocs();
                    cd.PKId = Convert.ToInt32(row["PK_Id"]);
                    cd.Description = row["Description"].ToString();
                    listCD.Add(cd);
                }
            }
            return listCD;
        }
        public CSMarineConsignmentDocs getMarineConsignmentDocs_byFK_Des_ID(int claimId, int userId, int fk_Des_ID)
        {
            CSMarineConsignmentDocs c = new CSMarineConsignmentDocs();
            _dt = _sf.returnDTWithQuery_executeReader("select InvNo,InvDate,InvQty,UOM from tblMarineConsignmentDocs where FK_ClaimID=" + claimId + " and EntryBy=" + userId + " and PK_ID=" + fk_Des_ID + "");
            if (_dt.Rows.Count > 0)
            {
                c.InvNo = _dt.Rows[0]["InvNo"].ToString();
                c.InvDate = _dt.Rows[0]["InvDate"].ToString();
                c.InvQty = _dt.Rows[0]["InvQty"].ToString();
                c.Uom = _dt.Rows[0]["UOM"].ToString();

            }
            return c;
        }
        public int saveMarineAssessmentHeader(CSMarineAssessmentHeader ah)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AH_ID",ah.PK_AH_ID), new SqlParameter("@ClaimID", ah.ClaimID), new SqlParameter("@UserID", ah.UserID), new SqlParameter("@SubTotalC", ah.SubTotalC), new SqlParameter("@SubTotalA", ah.SubTotalA), new SqlParameter("@TotalAmountC", ah.TotalAmountC), new SqlParameter("@TotalAmountA", ah.TotalAmountA)
            };
            return Convert.ToInt32(_sf.executeScalerWithProc("uspMarineAssessmentHeaderSave", _param));
        }

        public int saveMarineAssessmentDetailDesc(CSMarineAssessmentDetailDesc ad)
        {
            _param = new SqlParameter[]
            {
                new SqlParameter("@PK_AD_ID",ad.PK_AD_ID),new SqlParameter("@CalMode",ad.CalMode), new SqlParameter("@ClaimID", ad.ClaimID), new SqlParameter("@FK_DesID", ad.FK_DesID), new SqlParameter("@AffectedQuantityC", ad.AffectedQuantityC), new SqlParameter("@UnitRateC", ad.UnitRateC), new SqlParameter("@AmountC", ad.AmountC), new SqlParameter("@UserID", ad.UserID), new SqlParameter("@IPAddress", ad.IPAddress), new SqlParameter("@AffectedQuantityA", ad.AffectedQuantityA), new SqlParameter("@UnitRateA", ad.UnitRateA), new SqlParameter("@AmountA", ad.AmountA),new SqlParameter("@MD_ID",ad.MD_ID),new SqlParameter("@AH_ID",ad.AH_ID)
            };
            return _sf.executeNonQueryWithProc("uspMarineAssesmentDetails_New", _param);
        }
        public int saveMarineClaimedCalculation(CSMarineClaimedCalculation cc)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_CC_ID", cc.PK_CC_ID), new SqlParameter("@ClaimID", cc.ClaimID), new SqlParameter("@FK_CalID", cc.FK_CalID), new SqlParameter("@CalMode", cc.CalMode), new SqlParameter("@CalType", cc.CalType), new SqlParameter("@Value", cc.Value), new SqlParameter("@UserID", cc.UserID), new SqlParameter("@IPAddress", cc.IPAddress), new SqlParameter("@Rate", cc.Rate), new SqlParameter("@AH_ID", cc.AH_ID),new SqlParameter("@SubTotal",cc.SubTotal) }; return _sf.executeNonQueryWithProc("uspMarineClaimedCalculation_New", _param);
        }
        public int deleteMarineClaimedCalculation(int pk_CC_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_CC_ID", pk_CC_Id), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMarineClaimedCalculationDelete", _param);

        }
        public CSMarineAssessmentHeader getMarineAssessmentHeader(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineAssessmentHeaderGet", _param);
            CSMarineAssessmentHeader ah = new CSMarineAssessmentHeader();
            if (_dt.Rows.Count > 0)
            {
                ah.PK_AH_ID = Convert.ToInt32(_dt.Rows[0]["PK_AH_ID"]);
                ah.SubTotalC = Convert.ToDecimal(_dt.Rows[0]["SubTotalC"]);
                ah.SubTotalA = Convert.ToDecimal(_dt.Rows[0]["SubTotalA"]);
                ah.TotalAmountC = Convert.ToDecimal(_dt.Rows[0]["TotalAmountC"]);
                ah.TotalAmountA = Convert.ToDecimal(_dt.Rows[0]["TotalAmountA"]);
            }
            return ah;
        }
        public List<CSMarineClaimedCalculation> getMarineClaimedCalculation(int claimId, int userId, string calMode)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId), new SqlParameter("@CalMode", calMode) };
            _dt = _sf.returnDTWithProc_executeReader("uspMarineClaimedCalculationGet", _param);
            List<CSMarineClaimedCalculation> listCC = new List<CSMarineClaimedCalculation>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineClaimedCalculation cc = new CSMarineClaimedCalculation();
                    cc.PK_CC_ID = Convert.ToInt32(row["PK_ID"]);
                    cc.FK_CalID = Convert.ToInt32(row["FK_CalID"]);
                    cc.CalDescription = row["calDescription"].ToString();
                    cc.CalType = row["CalType"].ToString().Trim();
                    cc.Value = Convert.ToDecimal(row["Value"]);
                    cc.Rate = Convert.ToDecimal(row["Rate"]);
                    cc.AH_ID = Convert.ToInt32(row["AH_ID"]);
                    cc.SubTotal = Convert.ToInt32(row["SubTotal"]);
                    listCC.Add(cc);
                }   
            }
            return listCC;
        }
        public int saveMarineAssessmentRevisionRsn(CSMarineAssessmentRevisionRsn ar)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AR_ID", ar.PK_AR_ID), new SqlParameter("@AH_ID", ar.AH_ID), new SqlParameter("@ClaimID", ar.ClaimID), new SqlParameter("@UserID", ar.UserID), new SqlParameter("@RevisionRsn", ar.RevisionRsn)
            };
            return _sf.executeNonQueryWithProc("uspMarineAssessmentRevisionRsnSave", _param);
        }
        public int deleteMarineAssessmentRevisionRsn(int pk_AR_Id, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@PK_AR_ID", pk_AR_Id), new SqlParameter("@UserID", userId) };
            return _sf.executeNonQueryWithProc("uspMarineAssessmentRevisionRsnDelete", _param);
                
        }
        public List<CSMarineAssessmentRevisionRsn> getMarineAssessmentRevisionRsn(int claimId, int userId)
        {
            _param = new SqlParameter[] { new SqlParameter("@ClaimID", claimId), new SqlParameter("@UserID", userId)};
            _dt = _sf.returnDTWithProc_executeReader("uspMarineAssessmentRevisionRsnGet", _param);
            List<CSMarineAssessmentRevisionRsn> listRevisionRsn = new List<CSMarineAssessmentRevisionRsn>();
            if (_dt.Rows.Count > 0)
            {
                foreach (DataRow row in _dt.Rows)
                {
                    CSMarineAssessmentRevisionRsn ar = new CSMarineAssessmentRevisionRsn();
                    ar.PK_AR_ID =Convert.ToInt32(row["PK_AR_ID"]);
                    ar.RevisionRsn = row["RevisionRsn"].ToString();
                    listRevisionRsn.Add(ar);
                }
            }
            return listRevisionRsn;
        }
        #endregion
        public String getLegOfJourney_byClaimId(int claimId, string userId)
        {

            return Convert.ToString(_sf.executeScalerWithQuery("select top 1 LegOfJourney from tblMarineTranshipment where FK_ClaimID=" + claimId + " and EntryBy='" + userId + "'"));
        }
        public String getTotalAmountA_byClaimId(int claimId, string userId)
        {

            return Convert.ToString(_sf.executeScalerWithQuery("select TotalAmountA from tblMarineAssessmentHeader where ClaimID=" + claimId + " and UserID='" + userId + "'"));
        }
        public String getTotalAmountC_byClaimId(int claimId, string userId)
        {

            return Convert.ToString(_sf.executeScalerWithQuery("select TotalAmountC from tblMarineAssessmentHeader where ClaimID=" + claimId + " and UserID='" + userId + "'"));
        }
    }
}
