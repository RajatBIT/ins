﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace InsuranceSurvey
{
    public class MySession
    {
        public static string userType
        {
            get
            {
                if (HttpContext.Current.Session["UserType"] == null)
                {
                    return null;
                }
                else
                {
                    return HttpContext.Current.Session["UserType"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["UserType"] = value;
            }
        }
        public static int insuranceUserId
        {
            get
            {
                if (HttpContext.Current.Session["InsuranceUserId"] == null)
                {
                    return 0;
                }
                else
                {
                    return Convert.ToInt32(HttpContext.Current.Session["InsuranceUserId"].ToString());
                }
            }
            set
            {
                HttpContext.Current.Session["InsuranceUserId"] = value;
            }
        }
        public static string userName
        {
            get
            {
                if (HttpContext.Current.Session["UserName"] == null)
                {
                    return null;
                }
                else
                {
                    return HttpContext.Current.Session["UserName"].ToString();
                }
            }
            set
            {
                HttpContext.Current.Session["InsuranceUserId"] = value;
            }
        }
    }
}