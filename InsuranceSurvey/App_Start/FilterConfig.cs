﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace InsuranceSurvey
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }



    public class Authorization : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            HttpContext context = HttpContext.Current;

            if (context.Session["InsuranceUserId"] == null)
            {
                FormsAuthentication.SignOut();
                context.Response.Redirect("~/Login/Login");
            }
            else if (string.IsNullOrEmpty(Convert.ToString(context.Session["UserType"])))
            {
                FormsAuthentication.SignOut();
                context.Response.Redirect("~/Login/Login");
            }
            base.OnActionExecuting(filterContext);
        }
    }




}