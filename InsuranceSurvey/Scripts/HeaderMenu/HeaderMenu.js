$(window).load(function () {
    $('#cssmenu').prepend('<div id="menu-button">Menu</div>');
    $('#cssmenu #menu-button').on('click', function () {
        var menu = $(this).next('ul');
        if (menu.hasClass('open')) {
            menu.removeClass('open');
        } else {
            menu.addClass('open');
        }
    });

    UserDropdown();
});

function UserDropdown() {
    $("#notificationLink").click(function () {
        $("#notificationContainer").fadeToggle(300);
        return false;
    });
    //Document Click
    $(document).click(function () {
        $("#notificationContainer").hide();
    });
    //Popup Click
    $("#notificationContainer").click(function () {
        e.preventDefault;
        return false;
    });
}
