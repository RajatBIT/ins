﻿angular.module('myApp').controller('NoClaimCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.nc = {};
    $scope.getReasonForNoClaim = function () {

        $http.get('/MarineNoClaim/getReasonForNoClaim').then(function (response) {
            console.log(response);
            $scope.listRC = response.data.listRC;
        }, function (error) {
        })
    }
    $scope.getNoClaim = function () {

        
        $http.get('/MarineNoClaim/getNoClaim?claimId='+_claimId).then(function (response) {
            console.log(response);
            $scope.nc = response.data.nc;
        }, function (error) {
        })

    }
    $scope.saveNoClaim = function () {

        $scope.nc.ClaimID = angular.copy(_claimId);
        console.log($scope.nc);
       
        $http.post('/MarineNoClaim/saveNoClaim', {nc:$scope.nc}).then(function (response) {
            console.log(response);
            showSuccessToast(response.data);
            $scope.getNoClaim();
            
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        var id = angular.copy($scope.nc.PK_NC_ID);
        $scope.nc = {};
        $scope.nc.PK_NC_ID = angular.copy(id);
    }

    $scope.getReasonForNoClaim();

    $scope.getNoClaim();
})