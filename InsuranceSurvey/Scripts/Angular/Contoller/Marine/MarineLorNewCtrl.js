﻿/// <reference path="MarineLorNewCtrl.js" />
angular.module('myApp').controller('MarineLorNewCtrl', function ($scope, $http, CommonService, $filter, $location, $anchorScroll) {
    var _claimId = $('#cid').val();
    var _task = $('#task').val();
    $scope.lh = {};
    //$scope.e = {};
    $scope.lc = {};
    $scope.getLorDetailsNew = function () {
        $http.get('/Collaborator/getLorDetailsNew?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listLorDetailsNew = response.data.listLorDetailsNew;
        }, function (error) {
        })
    }
$scope.getLorHeaderNew = function () {
        $http.get('/Collaborator/getLorHeaderNew?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.lh = response.data.lh;
        }, function (error) {
        })
    }
$scope.DocumentName = '';
    $scope.saveLorNew = function (p) {
        var x = $('#formMarineLorNew');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.lh.ClaimID =parseFloat(_claimId);
        $http.post('/Collaborator/saveLorNew', { lh: $scope.lh, listLorDetailsNew: $scope.listLorDetailsNew, claimId: _claimId }).then(function (response) {
            console.log(response);
            $scope.getLorHeaderNew();
            $scope.getLorDetailsNew();
            if (p == 'n') {
                window.location = "/Survery/MarineISVR?fkClaimId="+_claimId+"&task="+_task;
            }
            else {
                if (response.data.msg == 's') {
                    showSuccessToast("Saved Successfully");
                }
                else {
                    showSuccessToast("Failed! Try Again");
                }
            }
        }, function (error) {
        })
    }
    $scope.saveLorDetailsNew_SingleRecord = function (file_Id, l, p) {
        if (l.LM_Name == '' && l.LM_Name == null || l.LM_Name == undefined) {
            var valdidationMsg = "<p>Please Fill Details</p>"
            CommonService.showValidationPopup(valdidationMsg);
            return false;
        }
        $scope.a = {};
        l.ClaimID = parseFloat(_claimId);
        $http.post('/Collaborator/saveLorDetailsNew', { l: l }).then(function (response) {
            console.log(response);
            var temp_response_File;
            if(response.data.msg=="s")
            {
                console.log(l);
                var f;
                var fileData = new FormData();
                if (p == 'upload') {
                    if ($("#" + file_Id).val() != '') {
                        fileData.append($("#" + file_Id).get(0).files[0].name, $("#" + file_Id).get(0).files[0]);
                        f = "yes";
                        $http({
                            url: '/Collaborator/saveLorDetailsNew_File?f=' + f + '&pk_L_ID=' + response.data.pk_L_ID,
                            method: 'POST',
                            data: fileData,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity
                        }).then(function (response_File) {
                            console.log("a");
                            console.log(response_File);
                            angular.forEach($scope.listLorDetailsNew, function (element, index) {
                                if(index==file_Id)
                                {
                                    element.Pk_L_ID = response.data.pk_L_ID;
                                    element.FileName = response_File.data.fileName;
                                    element.Rec_NotRec_NotRel = 'Received';
                                    $("#" + file_Id).val('');
                                }
                                $("#" + file_Id).val('');
                            })
                        }, function (error) {
                            $("#" + file_Id).val('');
                        })
                    }
                }
                else if (p == 'save') {
                    l.Pk_L_ID = response.data.pk_L_ID;
                    l.LM_ID = 0;
                    console.log(l); 
                    if ($('#file').val() != '') {
                        fileData.append($('#file').get(0).files[0].name, $('#file').get(0).files[0]);
                        f = "yes";
                        $http({
                            url: '/Collaborator/saveLorDetailsNew_File?f=' + f + '&pk_L_ID=' + response.data.pk_L_ID,
                            method: 'POST',
                            data: fileData,
                            headers: { 'Content-Type': undefined },
                            transformRequest: angular.identity
                        }).then(function (response_File) {
                            l.FileName = response_File.data.fileName;
                            $('#file').val('');
                        }, function (error) {
                            $('#file').val('');
                        })
                    }
                    $scope.listLorDetailsNew.push(l);
                }
                  
                   
            }
        }, function (error) {
        })
    }
    $scope.test = function (i) {
        var a = $('#' + i).val();
        console.log(a);
        
    }

    $scope.a = {};
    $scope.a.LM_ID = 0;
    $scope.addIn_listLorDetailsNew = function () {
        $scope.a.LM_ID = 0;
        debugger;
        alert($scope.a.LM_Name);

        if ($scope.a.LM_Name == '' && $scope.a.LM_Name == null || $scope.a.LM_Name == undefined)
        {
            var valdidationMsg = "<p>Please Fill Details</p>"
            CommonService.showValidationPopup(valdidationMsg);
            return false;
        }
        else if ($scope.a.LM_Name != '' && $scope.a.LM_Name != null && $scope.a.LM_Name != undefined) {
            $scope.listLorDetailsNew.push($scope.a);
            $scope.a = {};
        }
    }
    //$('#aaaa').change(function () {
    //    $scope.aa = $('#aaaa').val();
    //    alert($scope.aa);
    //})
    $scope.show_Modal_SendJIR = function () {
        $('#modal_SendJIR').modal('show');
    }

    //$scope.getEmailId = function () {
    //    $http.get('/Collaborator/getConcerned?claimId=' + _claimId).then(function (response) {
    //        console.log(response);
    //        $scope.c = response.data.c;
    //    }, function (error) {
    //    })
    //}
    $scope.temp;
    //$scope.setEmailId = function () {
    //    $scope.e.ToEmailIds = '';
    //    $scope.e.CCEmailIds = '';
    //    if ($scope.e.ToInsured != true && $scope.e.ToInsurers != true && $scope.e.ToBrokers != true) {
    //        $scope.e.CCInsured = false;
    //        $scope.e.CCInsurers = false;
    //        $scope.e.CCBrokers = false;
    //    }
    //    if ($scope.e.ToInsured == true) {
    //        $scope.e.ToEmailIds += $scope.c.I_ToEmailId;
    //        //$('#ToEmailIds').val($scope.c.I_ToEmailId);
    //        //$('#ToEmailIds_Val').html($scope.c.I_ToEmailId);
    //        //$('#ToEmailIds').next('.multiple_emails-container').remove();
    //        //$('#ToEmailIds').multiple_emails();
    //    }
    //    if ($scope.e.CCInsured == true) {
    //        if ($scope.e.ToInsured == false)
    //        {
    //            $scope.e.CCEmailIds += $scope.c.I_ToEmailId;
    //            //$('#CCEmailIds').val($scope.c.I_ToEmailId);
    //            //$('#CCEmailIds_Val').html($scope.c.I_ToEmailId);
    //            //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //            //$('#CCEmailIds').multiple_emails();
    //        }
    //        $scope.e.CCEmailIds += $scope.c.I_CCEmailId;

    //        //$('#CCEmailIds').val($scope.c.I_CCEmailId);
    //        //$('#CCEmailIds_Val').html($scope.c.I_CCEmailId);
    //        //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //        //$('#CCEmailIds').multiple_emails();
    //    }



    //    if ($scope.e.ToInsurers == true) {
    //        $scope.e.ToEmailIds += $scope.c.Is_ToEmailId;
    //        //$('#ToEmailIds').val($scope.c.Is_ToEmailId);
    //        //$('#ToEmailIds_Val').html($scope.c.Is_ToEmailId);
    //        //$('#ToEmailIds').next('.multiple_emails-container').remove();
    //        //$('#ToEmailIds').multiple_emails();
    //    }
    //    if ($scope.e.CCInsurers == true) {
    //        if ($scope.e.ToInsurers == false) {
    //            $scope.e.CCEmailIds += $scope.c.Is_ToEmailId;

    //            //$('#CCEmailIds').val($scope.c.Is_ToEmailId);
    //            //$('#CCEmailIds_Val').html($scope.c.Is_ToEmailId);
    //            //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //            //$('#CCEmailIds').multiple_emails();
    //        }
    //        $scope.e.CCEmailIds += $scope.c.Is_CCEmailId;
    //        //$('#CCEmailIds').val($scope.c.Is_CCEmailId);
    //        //$('#CCEmailIds_Val').html($scope.c.Is_CCEmailId);
    //        //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //        //$('#CCEmailIds').multiple_emails();

    //    }


    //    if ($scope.e.ToBrokers == true) {
    //        $scope.e.ToEmailIds += $scope.c.B_ToEmailId;
    //        console.log($scope.e.ToEmailIds);
    //        //$('#ToEmailIds').val($scope.c.B_ToEmailId);
    //        //$('#ToEmailIds_Val').html($scope.c.B_ToEmailId);
    //        //$('#ToEmailIds').next('.multiple_emails-container').remove();
    //        //$('#ToEmailIds').multiple_emails();

    //    }
    //    if ($scope.e.CCBrokers == true) {
    //        if ($scope.e.ToBrokers == false) {
    //            $scope.e.CCEmailIds += $scope.c.B_ToEmailId;
    //            //$('#CCEmailIds').val($scope.c.B_ToEmailId);
    //            //$('#CCEmailIds_Val').html($scope.c.B_ToEmailId);
    //            //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //            //$('#CCEmailIds').multiple_emails();
    //        }
    //        $scope.e.CCEmailIds += $scope.c.B_CCEmailId;
    //        //$('#CCEmailIds').val($scope.c.B_CCEmailId);
    //        //$('#CCEmailIds_Val').html($scope.c.B_CCEmailId);
    //        //$('#CCEmailIds').next('.multiple_emails-container').remove();
    //        //$('#CCEmailIds').multiple_emails();

    //    }
    //    $scope.e.ToEmailIds = $scope.e.ToEmailIds.replace("][", ",").replace("][", ",").replace("][", ",").replace("][", ",");
    //    $('#ToEmailIds').val($scope.e.ToEmailIds);
    //    $('#ToEmailIds_Val').html($scope.e.ToEmailIds);
    //    $('#ToEmailIds').next('.multiple_emails-container').remove();
    //    $('#ToEmailIds').multiple_emails();
    //    $scope.e.CCEmailIds = $scope.e.CCEmailIds.replace("][", ",").replace("][", ",").replace("][", ",").replace("][", ",");
    //    console.log($scope.e.CCEmailIds);
        
        
    //    $('#CCEmailIds').val($scope.e.CCEmailIds);
    //    $('#CCEmailIds_Val').html($scope.e.CCEmailIds);
    //    $('#CCEmailIds').next('.multiple_emails-container').remove();
    //    $('#CCEmailIds').multiple_emails();
    //    $('.multiple_emails-input').attr("readonly", true);
    //    $("a").removeClass("multiple_emails-close");
    //    $("span").removeClass("glyphicon glyphicon-remove");
    //    }
    $scope.le = {};

    $scope.saveMarineLorEmailIdsNew = function () {
        //var x = $('#formMarineLorNew');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        if ($scope.e.ToEmailIds == '' || $scope.e.ToEmailIds == null || $scope.e.ToEmailIds == undefined)
        {
            alert("No Email Found");
            return false;
        }
        if ($scope.e.CCEmailIds == '' || $scope.e.CCEmailIds == null || $scope.e.CCEmailIds == undefined) {
            alert("No Email Found");
            return false;
        }
        $scope.e.ClaimID = parseFloat(_claimId);
        $http.post('/Collaborator/saveMarineLorEmailIdsNew', { e: $scope.e }).then(function (response) {
            console.log(response);
                if (response.data.msg == 's') {
                    showSuccessToast("Send Mail Successfully");
                    $scope.e = {};
                    $scope.getMarineLorEmailIdsNew();
                }
                else {
                    showSuccessToast("Failed! Try Again");
                    $scope.e = {};
                    $scope.getMarineLorEmailIdsNew();
                }
               
        }, function (error) {
        })
    }
    
    $scope.getMarineLorEmailIdsNew = function () {
        $http.get('/Collaborator/getMarineLorEmailIdsNew?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.e = response.data.e;
            
            $('#ToEmailIds').val($scope.e.ToEmailIds);
            $('#ToEmailIds_Val').html($scope.e.ToEmailIds);
            $('#ToEmailIds').next('.multiple_emails-container').remove();
            $('#ToEmailIds').multiple_emails();


            $('#CCEmailIds').val($scope.e.CCEmailIds);
            $('#CCEmailIds_Val').html($scope.e.CCEmailIds);
           $('#CCEmailIds').next('.multiple_emails-container').remove();
           $('#CCEmailIds').multiple_emails();

           $('.multiple_emails-input').attr("readonly", true);
           setTimeout(function () {
               $("a").removeClass("multiple_emails-close");
               $("span").removeClass("glyphicon glyphicon-remove");
           },1000)
           
        }, function (error) {
            $('.multiple_emails-input').attr("readonly", true);
        })
    }

  
    //$scope.sendEmail_Lor = function () {
    //    $http.get('/SuperAdmin/sendEmail_Lor').then(function (response) {
    //        console.log(response);
    //        if(response.data.msg=='s')
    //        {
    //            showSuccessToast("Send mail Successfully");
    //        }
    //        if (response.data.msg == 'n') {
    //            showSuccessToast("No Email Found");
    //        }

    //        else {
    //            showSuccessToast("Failed! Try Again");
    //        }
    //    }, function (error) {
    //    })
    //}

    $scope.getMarineLorChattingNew = function () {
        $http.get('/Collaborator/getMarineLorChattingNew?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listChatting = response.data.listChatting;
            //$location.hash('lastMsg');
            //$anchorScroll();
            //$scope.scroll();
            //$('#scroll').click();
            setTimeout(function () {
                $location.hash('lastMsg');
                $anchorScroll();
            }, 0000);
           
            //setInterval(function () {
            //    $location.hash('lastMsg');
            //    $anchorScroll();
            //    console.log("time");
            //}, 3000)
                
            //$window.scrollTo(0, angular.element('l').offsetTop);
            
            //angular.forEach($scope.listChatting, function (element,index) {
            //    element.CrtDT=
            //})
        }, function (error) {
        })
    }
    $scope.saveMarineLorChattingNew = function () {
        if (($scope.lc.ChattingText == '' || $scope.lc.ChattingText == null || $scope.lc.ChattingText == undefined)
            &&
            ($("#Document").val() == '' || $("#Document").val() == undefined || $("#Document").val() == null))
        {
            console.log($scope.lc.ChattingText);
            console.log($("#Document").val());
            return false;
        }
        $scope.lc.ClaimID = angular.copy(_claimId);
        debugger;
        var $btn = $('#btnChat');
        $btn.button('loading');
        $http.post('/Collaborator/saveMarineLorChattingNew', {lc:$scope.lc}).then(function (response) {
            console.log(response);
            $scope.msg = response.data.msg;
            if (response.data.msg == "s")
            {
                $scope.lc = {};
                var d = '';
                if (window.FormData != undefined) {
                    var fileData = new FormData();
                    if ($("#Document").val() != '') {
                        fileData.append($("#Document").get(0).files[0].name, $("#Document").get(0).files[0]);
                       d = "yes";
                    }
                    $http({
                        url: '/Collaborator/saveLorDetailsNew_Document?PK_LC_ID=' + response.data.PK_LC_ID + '&d=' + d,
                        method: 'POST',
                        data: fileData,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .then(function (resPhoto) {
                        console.log(resPhoto);
                        $btn.button('reset');

                        $scope.lc = {};
                        $("#Document").val('');
                        $scope.getMarineLorChattingNew();
                        if (resPhoto.data.msg == 's') {
                            //showSuccessToast(InsuredMsg);
                        }
                    }, function (errorPhoto) {
                        console.log(errorPhoto);
                        $btn.button('reset');
                        $("#Document").val('');
                        $scope.lc = {};
                            $scope.getMarineLorChattingNew();
                    })
                }
                else {
                    $btn.button('reset');
                    $scope.lc = {};
                    $scope.getMarineLorChattingNew();
                    //showSuccessToast(InsuredMsg);
                }
            }
            else {
                $btn.button('reset');
                $scope.lc = {};
                $scope.getMarineLorChattingNew();
                //showSuccessToast("Failed! Try Again");
            }
 
        }, function (error) {
            console.log(error);
            $btn.button('reset');
            $scope.getMarineLorChattingNew();
        })
    }




    $scope.scroll = function () {
        //alert();
        $location.hash('lastMsg');
        $anchorScroll();

        //$("html, body").animate({ scrollTop: $('#lastMsg').height() }, 1000);
        //$("html, body").animate({ scrollTop: $("#lastMsg").scrollTop() }, 1000);
    }
    $('#addClass').click(function () {
      
      setTimeout(function () {
            $location.hash('lastMsg');
            $anchorScroll();
        }, 0000);
        
    })

    //$("#DocumentIcon").click(function () {
    //    $("#Document").trigger('click');
    // //$scope.DocumentName=  $("#Document").get(0).files[0].name;
    //});

    //$("#Document").on('click',function () {
    //    $scope.DocumentName = $(this).get(0).files[0].name;
    //})


    $scope.generatePDF_MarineJIR = function () {
        $('#Modal_GeneratePDF_MarineJIR').modal('show');
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('iframe').attr('src', "/Survery/GeneratePdf?ClaimID=" + _claimId + "&InsdType=" + $scope.InsdType + "&InsdID=" + $scope.InsdId + "&Task=" + $('#task').val() + "");
        })
    }
    
    //using for true or false for using in reminder_pdf
    $scope.setFinalReminder = function () {

        $http.get('/Collaborator/setFinalReminder?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.msg = response.data.msg;
        }, function (error) {
        })
    }

    $scope.documentChecked = function () {
        $http.post('/Collaborator/saveTaskManager', { claimId: _claimId, shortName: 'AD' }).then(function (response) {
            console.log(response);
            $scope.msg = response.data.msg;
        }, function (error) {
        })
    }

    $scope.getLorDetailsNew();
    $scope.getLorHeaderNew();
    //$scope.getEmailId();
    //$scope.getMarineLorEmailIdsNew();
    $scope.getMarineLorChattingNew();

})