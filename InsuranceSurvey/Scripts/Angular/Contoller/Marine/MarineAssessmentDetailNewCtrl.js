﻿angular.module('myApp').controller('MarineAssessmentDetailNewCtrl', function ($scope, $http) {

    var _claimId = $('#cid').val();
    $scope._calMode = $('#calMode').val();
    var _task = $('#task').val();
    $scope.btnSave = 1;
    $scope.c = {};
    $scope.FK_Des_ID = 0;
    $scope.ah = {};
    $scope.ah.SubTotalC = 0;
    //$scope.ac = {};
    $scope.cc = {};
    $scope.cc.Rate = 0;
    $scope.cc.Value = 0;
    $scope.cc.CalType = "Add";
    $scope.cc.CalDescription = '';
    $scope.cc.FK_CalID = 0;
    $scope.ValueTotal = 0;
    $scope.temp;
    $scope.listAD;
    $scope.listCC;

    $scope.getMarineAssessmentDetailDesc = function () {

        $http.get('/Collaborator/getMarineAssessmentDetailDesc?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listAD = response.data.listAD;
        }, function (error) {
        })
    }
    $scope.getDescription_byClaimID = function () {

        $http.get('/Collaborator/getDescription_byClaimID?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listCD = response.data.listCD;
        }, function (error) {
        })
    }
    $scope.getMarineConsignmentDocs_byFK_Des_ID = function () {
        $http.get('/Collaborator/getMarineConsignmentDocs_byFK_Des_ID?claimId=' + _claimId + '&fk_Des_ID=' + $scope.FK_Des_ID).then(function (response) {

            console.log(response);
            $scope.c = response.data.c;
            $scope.c.AffectedQuantity = 0;
        }, function (error) {

        })
    }
    $scope.saveMarineDamageDetails = function () {
        
        var x = $('#formDesc');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.saveMarineAssessment('d');
        $scope.c.FKDesId = $scope.FK_Des_ID;
        $scope.c.FKClaimId = _claimId;
        $scope.c.AffectedQuantity = 0;
        $http.post('/Collaborator/MarineSubmitDamageDetails', { objProperty: $scope.c }).then(function (response) {
            console.log(response);
            $scope.FK_Des_ID = 0;
            $scope.c = {};
            //$scope.getMarineAssessmentDetailDesc();
            $scope.getMarineAssessment();
        }, function (error) {
            $scope.FK_Des_ID = 0;
        })
    }
    var tempSubTotalC = 0;
    var tempSubTotalA = 0;
    $scope.calAmount = function () {
        $scope.ah.SubTotalC = 0;
        $scope.ah.SubTotalA = 0;

        angular.forEach($scope.listAD, function (element, index) {

            var aqc = (angular.isUndefined(element.AffectedQuantityC) || (element.AffectedQuantityC == '')) ? 0 : parseFloat(element.AffectedQuantityC);
            var urc = (angular.isUndefined(element.UnitRateC) || (element.UnitRateC == '')) ? 0 : parseFloat(element.UnitRateC);;
            console.log(element.UnitRateC);
            element.AmountC = aqc * urc;

            console.log(element.AmountC);

            $scope.ah.SubTotalC += parseFloat(element.AmountC);

            var afa = (angular.isUndefined(element.AffectedQuantityA) || (element.AffectedQuantityA == '')) ? 0 : parseFloat(element.AffectedQuantityA);
            var ura = (angular.isUndefined(element.UnitRateA) || (element.UnitRateA == '')) ? 0 : parseFloat(element.UnitRateA);
            element.AmountA = afa * ura;
            $scope.ah.SubTotalA += parseFloat(element.AmountA);
        })
        //$scope.listAD = angular.copy(templistAD);
        $scope.ah.TotalAmountC = angular.copy(parseFloat($scope.ah.SubTotalC));
        $scope.ah.TotalAmountA = angular.copy(parseFloat($scope.ah.SubTotalA));
        tempSubTotalC = angular.copy(parseFloat($scope.ah.SubTotalC));
        tempSubTotalA = angular.copy(parseFloat($scope.ah.SubTotalA));
        $scope.commonFuncForBindCal();
        $scope.calRateToAmount();
    }

    $scope.calAll = function () {
        if ($scope._calMode == 'CL') {
            $scope.ah.SubTotalC;
        }
        else if ($scope._calMode == 'AS') {

        }
    }
    $scope.listCC = [];
    $scope.addInlistCC = function () {
        console.log($scope.cc.Value);
        var x = $('#formPart');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }

        if ($scope._calMode == 'CL') {
            $scope.listCC.push($scope.cc);
            $scope.tdHideSubTotal = false;
            console.log("cc");
            console.log($scope.cc);
            $scope.cc = {};
            $scope.FK_Des_ID = 0;
            $scope.cc.FK_CalID = 0;
            $scope.cc.Rate = 0;
            $scope.cc.Value = 0;
            $scope.cc.SubTotal = 0;
            $scope.cc.CalType = "Add";
            $scope.cc.CalDescription = "";
           

            $scope.commonFuncForBindCal();
        }
        else if ($scope._calMode == 'AS') {
            $scope.listCC.push($scope.cc);
            $scope.tdHideSubTotal = false;
            $scope.cc = {};
            $scope.FK_Des_ID = 0;
            $scope.cc.FK_CalID = 0;
            $scope.cc.Rate = 0;
            $scope.cc.Value = 0;
            $scope.cc.SubTotal = 0;
            $scope.cc.CalType = "Add";
            $scope.cc.CalDescription = "";

            $scope.commonFuncForBindCal();
        }
        //else {
        //    $scope.cc = {};
        //    $scope.cc.CalType = "Add";
        //    $scope.FK_Des_ID = 0;
        //    $scope.cc.FK_CalID = 0;
        //    $scope.cc.Rate = 0;
        //    $scope.cc.Value = 0;
        //    $scope.cc.SubTotal = 0;
        //    $scope.cc.CalDescription = "";
        //    $scope.commonFuncForBindCal();
        //}
    }
    $scope.calRateToAmount = function () {
        $scope.commonFuncForBindCal();
        if ($scope._calMode == 'CL') {
            $scope.cc.Value = angular.copy((parseFloat(tempSubTotalC) * parseFloat($scope.cc.Rate / 100)).toFixed(2));
        }
        else if ($scope._calMode == 'AS') {
            $scope.cc.Value = angular.copy((parseFloat(tempSubTotalA) * parseFloat($scope.cc.Rate / 100)).toFixed(2));
        }
    }

    $scope.checkIsSubTotalInlistCC = function () {
        var id = this.ac.FK_CalID;
        var $tr = this.ac;
        //console.log(id);
        angular.forEach($scope.listCM, function (element, index) {

            if (id == element.PK_ID) {
                var descName = element.CalDescription;
                if (descName == 'Sub-Total') {

                    //console.log('asfasdf');
                    $tr.hideSubTotal = true;


                    //$scope.CalDescription = "Sub-Total";
                }
            }


        })

        //angular.forEach($scope.listCC, function (element, index) {
        //    if (element.FK_CalID == id) {
        //        element.CalDescription = "Sub-Total";
        //    }
        //})
        

    }
    $scope.temp_CalType='';
    $scope.tdHideSubTotal = false;
    $scope.checkIsSubTotalInCC = function () {
        if ($scope.cc.CalType != "") {
            $scope.temp_CalType = angular.copy($scope.cc.CalType);
        }
        var id = this.cc.FK_CalID;
        var $tr = this.ac;
        //console.log(id);
        //temp_CalType = angular.copy($scope.cc.CalType);
        angular.forEach($scope.listCM, function (element, index) {

            if (id == element.PK_ID) {
                var descName = element.CalDescription;
                if (descName == 'Sub-Total') {

                    //console.log('asfasdf');
                    $scope.tdHideSubTotal = true;
                    if ($scope._calMode == 'CL') {
                        $scope.cc.SubTotal = angular.copy(parseFloat($scope.ah.TotalAmountC));

                    }

                    else if ($scope._calMode == 'AS') {
                        $scope.cc.SubTotal = angular.copy(parseFloat($scope.ah.TotalAmountA));
                    }

                    $scope.cc.CalDescription = "Sub-Total";
                    //temp_CalType =angular.copy($scope.cc.CalType);
                    $scope.cc.CalType = "";
                    return false;
                }
                else {
                    $scope.cc.CalType =angular.copy($scope.temp_CalType);
                    $scope.tdHideSubTotal = false;

                }
            }

        })
        console.log($scope.temp_CalType);

    }

    $scope.commonFuncForBindCal = function () {
        
        if ($scope._calMode == 'CL') {
            $scope.ah.TotalAmountC = 0;
            tempSubTotalC = angular.copy(parseFloat($scope.ah.SubTotalC));

            $scope.ah.TotalAmountC = angular.copy(parseFloat($scope.ah.SubTotalC));

            angular.forEach($scope.listCC, function (element, index) {
                if (element.CalDescription == "Sub-Total") {
                    debugger;
                    tempSubTotalC = angular.copy(parseFloat($scope.ah.TotalAmountC));
                    element.SubTotal = angular.copy(parseFloat($scope.ah.TotalAmountC));
                }
                else {
                    if (element.Rate != 0 && element.Rate != '' && element.Rate != undefined) {
                        element.Value = (parseFloat(tempSubTotalC) * (element.Rate / 100)).toFixed(2);
                    }
                    if (element.CalType == "Add") {
                        $scope.ah.TotalAmountC = (parseFloat($scope.ah.TotalAmountC) + parseFloat(element.Value)).toFixed(2);
                    }
                    else if (element.CalType == "Less") {
                        $scope.ah.TotalAmountC = (parseFloat($scope.ah.TotalAmountC) - parseFloat(element.Value)).toFixed(2);
                    }
                    else if (element.CalType == "Multiply") {
                        $scope.ah.TotalAmountC = (parseFloat($scope.ah.TotalAmountC) * parseFloat(element.Value)).toFixed(2);
                    }
                    else if (element.CalType == "Divide") {
                        $scope.ah.TotalAmountC = (parseFloat($scope.ah.TotalAmountC) / parseFloat(element.Value)).toFixed(2);
                    }

                }

            })
        }
        else if ($scope._calMode == 'AS') {
            $scope.ah.TotalAmountA = 0;
            tempSubTotalA = angular.copy(parseFloat($scope.ah.SubTotalA));
            $scope.ah.TotalAmountA = angular.copy(parseFloat($scope.ah.SubTotalA));
            angular.forEach($scope.listCC, function (element, index) {
                if (element.CalDescription == "Sub-Total") {
                    debugger;
                    tempSubTotalA = angular.copy(parseFloat($scope.ah.TotalAmountA));
                    element.SubTotal = angular.copy(parseFloat($scope.ah.TotalAmountA));
                }
                else {
                    if (element.Rate != 0 && element.Rate != '' && element.Rate != undefined) {
                        debugger;
                        element.Value = (parseFloat(tempSubTotalA) * (element.Rate / 100)).toFixed(2);
                    }

                    console.log(element.Value);
                    if (element.CalType == "Add") {
                        $scope.ah.TotalAmountA = angular.copy((parseFloat($scope.ah.TotalAmountA) + parseFloat(element.Value)).toFixed(2));
                    }
                    else if (element.CalType == "Less") {
                        debugger;
                        $scope.ah.TotalAmountA = angular.copy((parseFloat($scope.ah.TotalAmountA) - parseFloat(element.Value)).toFixed(2));
                    }
                    else if (element.CalType == "Multiply") {
                        $scope.ah.TotalAmountA = angular.copy((parseFloat($scope.ah.TotalAmountA) * parseFloat(element.Value)).toFixed(2));
                    }
                    else if (element.CalType == "Divide") {
                        $scope.ah.TotalAmountA = angular.copy((parseFloat($scope.ah.TotalAmountA) / parseFloat(element.Value)).toFixed(2));
                    }
                }
            })
        }
    }
    $scope.getParticular = function () {
        $http.get('/Collaborator/getParticular?calMode=' + $scope._calMode).then(function (response) {
            console.log(response);
            $scope.listCM = response.data.listCM;
        }, function (error) {
        })
    }
    $scope.saveMarineAssessment = function (p) {
        $scope.btnSave = 0;
        //$scope.commonFuncForBindCal();
        $http.post('/Collaborator/saveMarineAssessment', { claimId: _claimId, calMode: $scope._calMode, ah: $scope.ah, listAD: $scope.listAD, listCC: $scope.listCC, listRevisionRsn: $scope.listRevisionRsn }).then(function (response) {
            console.log(response);

            if (p == 'n') {
                window.location = '/Survery/SubmitFSR?fkClaimId=' + _claimId + '&task=' + _task;
            }
            else if (p == 'd') {
                $scope.getMarineAssessment();
            }
            else {
                showSuccessToast(response.data);
                $scope.getMarineAssessment();
            }
            $scope.btnSave = 1;
        }, function (error) {
            $scope.btnSave = 1;
        })
    }
    $scope.getMarineAssessment = function () {

        $http.get('/Collaborator/getMarineAssessment?claimId=' + _claimId + '&calMode=' + $scope._calMode).then(function (response) {
            console.log(response);
            $scope.ah = response.data.ah;
            //tempSubTotalC = angular.copy($scope.ah.TotalAmountC);
            $scope.listAD = response.data.listAD;
            $scope.listCC = response.data.listCC;
            $scope.listC = response.data.listC;
            $scope.listRevisionRsn = response.data.listRevisionRsn;
            $scope.commonFuncForBindCal();
        }, function (error) {
        })
    }
    $scope.deleteInlistCC = function (pk_CC_Id, index) {
        $scope.listCC.splice(index, 1);
        $scope.commonFuncForBindCal();
        if (pk_CC_Id != undefined) {
            //$scope.commonFuncForBindCal();

            $http.get('/Collaborator/deleteMarineClaimedCalculation?pk_CC_Id=' + pk_CC_Id).then(function (response) {
                console.log(response);
                $scope.saveMarineAssessment('d');
            }, function (error) {
            })
        }
        else {

            $scope.commonFuncForBindCal();
        }
    }
    $scope.getDataFromMarineJIRForAssessment = function () {

        $http.get('/Collaborator/getDataFromMarineJIRForAssessment?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.InsdType = response.data.InsdType;
            $scope.InsdId = response.data.InsdId;
        }, function (error) {
        })
    }
    $scope.ar = {};
    $scope.ar.PK_AR_ID = 0;
    $scope.listRevisionRsn = [];
    $scope.addInlistRevisionRsn = function () {
        $scope.listRevisionRsn.push($scope.ar);
        $scope.ar = {};
    }

    $scope.deleteInlistRevisionRsn = function (pk_AR_Id, index) {
        $scope.listRevisionRsn.splice(index, 1);
        if (pk_AR_Id != undefined) {
       
            $http.get('/Collaborator/deleteMarineAssessmentRevisionRsn?pk_AR_Id=' + pk_AR_Id).then(function (response) {
                console.log(response);
            }, function (error) {
            })
        }
       
    }
    $scope.getParticular();
    $scope.getDataFromMarineJIRForAssessment();
    //$scope.getMarineAssessmentDetailDesc();
    $scope.getDescription_byClaimID();
    $scope.getMarineAssessment();
    $scope.generatePDF_ClaimForm = function () {
        $('#Modal_GeneratePDF_ClaimForm').modal('show');
            $('.modal').on('shown.bs.modal', function () { 
                $(this).find('iframe').attr('src', "/Survery/GeneratePdf?ClaimID=" + _claimId + "&InsdType=" + $scope.InsdType + "&InsdID=" + $scope.InsdId + "&Task=" + $('#task').val() + "");
            })
    }
   

})