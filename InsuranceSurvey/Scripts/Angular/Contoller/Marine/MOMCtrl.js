﻿angular.module('myApp').controller('MOMCtrl', function ($scope, $http) {

    $scope.divMOM = 0;
    $scope.a = {};
    $scope.a.MR_OR_MRS = '';
    $scope.a.NameOfAttendee = '';
    $scope.a.Designation = '';
    $scope.a.NameOfOrganization = '';
    $scope.a.Representing = '';
    $scope.a.MobileNo = '';
    $scope.a.EmailId = '';
    $scope.d = {};
    $scope.d.Discussion = '';
    $scope.md = {};
    $scope.trAt = 1;
    $scope.trDs = 1;
    $scope.pk_At_IDs = '';
    $scope.pk_Ds_IDs = '';

    var _claimId = $('#claimId').val();
    //var pk_SV_ID = $(this).find('.pk_SV_ID').val();
    var pk_SV_ID ;


    $scope.showModalMOM = function (id) {
        
        pk_SV_ID =angular.copy(id);
        console.log(pk_SV_ID);
        $scope.divMOM = 1;
        $('#myModal').modal('show');

        $http.get('/Collaborator/getRefNo_And_InsuredName_byClaimID?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.RefNo = response.data.RefNo;
            $scope.InsuredName = response.data.InsuredName;
            $scope.getMOM();
        }, function (error) {
        })
    }

    $scope.Attendees = [];

    $scope.addInAttendees = function () {
        //var x = $('#formAtten');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        console.log($scope.a.MR_OR_MRS);
        if ($scope.a.MR_OR_MRS == '' || $scope.a.MR_OR_MRS == 'Select')
        {
            alert("Please Select Any One Mr or Mrs");
            return false;
        }
        if ($scope.a.Representing == '' || $scope.a.Representing=='Select')
        {
            alert("Please Select In Representing ");
            return false;
        }
            $scope.Attendees.push($scope.a);
            $scope.a = {};
            console.log($scope.Attendees);
     
     
    }
    $scope.deleteInAttendees = function (index, pk_At_Id) {
        $scope.Attendees.splice(index, 1);
        if (angular.isDefined(pk_At_Id)) {
            $http.get('/Collaborator/deleteMOMAttendees?pk_AT_ID='+pk_At_Id).then(function (response) {
                console.log(response);
            }, function (error) {
            })

        }
     
    }
    $scope.getdata = function () {
        if ($scope.Attendees.lenght == 0) {
            $scope.Attendees.push($scope.a);
            $scope.a = {};
        }

    }
    $scope.ArrDiscussion = [];
    $scope.addInArrDiscussion = function () {
 
            $scope.ArrDiscussion.push($scope.d);
            console.log($scope.ArrDiscussion);
            $scope.d = {};
 

    }
    $scope.deleteInArrDiscussion = function (index, pk_Ds_ID) {
        $scope.ArrDiscussion.splice(index, 1);
        if (angular.isDefined(pk_Ds_ID)) {
            $http.get('/Collaborator/deleteMOMDiscussion?pk_DS_ID=' + pk_Ds_ID).then(function (response) {
                console.log(response);
            }, function (error) {
            })
        }
      
    }
    $scope.saveMOM = function () {
        console.log($scope.ArrDiscussion);
        $scope.a = {};
        $scope.d = {};
        $scope.md.ClaimID = angular.copy(_claimId);
        $scope.md.SV_ID = angular.copy(pk_SV_ID);
        $http.post('/Collaborator/saveMOM', { md: $scope.md, listMA: $scope.Attendees, listDS: $scope.ArrDiscussion}).then(function (response) {
            console.log(response);
            showSuccessToast(response.data);
            $scope.getMOM();

            $('#myModal').modal('hide');
        }, function (error) {
        })

    }

    $scope.getMOM = function () {

        console.log(pk_SV_ID);
        $http.get('/Collaborator/getMOM?claimId=' + _claimId + '&sv_Id=' + pk_SV_ID).then(function (response) {
            console.log(response);
            $scope.md = response.data.md;
            $scope.Attendees = response.data.listMA;
             $scope.ArrDiscussion = response.data.listDS;
           //   console.log($scope.Attendees.length);
           // if ($scope.Attendees.length > 0) {
           //     $scope.trAt = 0;
           // }
           //else if ($scope.Attendees.length == 0) {
           //     $scope.trAt = 1;
           // }

           // $scope.ArrDiscussion = response.data.listDS;
           // if ($scope.ArrDiscussion.length > 0) {
           //     console.log("lenth");
           //     $scope.trDs = 0;
           // }
           //else if ($scope.ArrDiscussion.length == 0) {
           //     $scope.trDs = 1;
           // }
        }, function (error) {
        })
    }

    //$scope.getdata();

})