﻿angular.module('myApp').controller('BillCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.bh = {};
    $scope.er = {};
    $scope.tbER = 1;

    $scope.getModeOfConveyance = function () {

        $http.get('/Bill/getModeOfConveyance').then(function (response) {
            console.log(response);
            var templistMC = response.data.listMC;
            var i = 0;
            angular.forEach(templistMC, function (element, index) {
                if (element.ModeOfConveyance == "Other") {
                    templistMC.splice(i, 1);
                }
                i++;
            })
            $scope.listMC = angular.copy(templistMC);

        }, function (error) {
        })

    }




    $scope.listER = [];

    $scope.addInlistER = function () {
        $scope.listER.push($scope.er);
        $scope.er = {};
    }
    $scope.deleteInlistER = function (index, pk_Er_Id) {
        $scope.listER.splice(index, 1);
        if (angular.isDefined(pk_Er_Id)) {
            $http.get('/Bill/deleteBillExpensesReimbursement?pk_Er_Id=' + pk_Er_Id).then(function (response) {
                console.log(response);
                //showSuccessToast(response.data);
            }, function (error) {
            })
        }

    }
   
    $scope.saveBill = function () {
        var templistVD = [];
        var tempTotalRowWiseAmount = 0;
        angular.forEach($scope.listSV, function (element, index) {
            if (element.KMRun != undefined && element.KMRun != null && element.KMRun != '' && element.RatePerKM != undefined && element.RatePerKM != null && element.RatePerKM != '' && element.DA != undefined && element.DA != null && element.DA != '')
            {
                templistVD.push(element);
                tempTotalRowWiseAmount += parseFloat(element.RowWiseAmont);
            }
        })
        var tempTotalAmount = 0;
        angular.forEach($scope.listER, function (element, index) {
            tempTotalAmount +=parseFloat(element.Amount);
        })
        $scope.bh.InvoiceAmount = parseFloat(tempTotalRowWiseAmount) + parseFloat(tempTotalAmount);
        $http.post('/Bill/saveBill', {bh:$scope.bh,listVD:templistVD,listER:$scope.listER,claimId:_claimId}).then(function (response) {
            console.log(response);
            $scope.getBillHeader();
            showSuccessToast(response.data);
            //$scope.getBill();
        }, function (error) {
        })

    }
    $scope.getBillHeader = function () {
        $http.get('/Bill/getBillHeader?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listBH = response.data.listBH;
        }, function (error) {
        })
    }
    $scope.getBillVisitDetail_And_Expenses = function (bh_Id, invoiceNo) {
        console.log(bh_Id,invoiceNo);
        $scope.tbER = 0;
        $scope.bh.InvoiceNo = invoiceNo;
        $('input').attr('disabled', true);
        $('select').attr('disabled', true);
        $http.get('/Bill/getBillVisitDetail_And_Expenses?claimId=' + _claimId+'&bh_Id='+bh_Id).then(function (response) {
            console.log(response);
            $scope.listSV = response.data.listVD;
            $scope.listER = response.data.listER;
        }, function (error) {
        })
    }

    $scope.testing = function () {
        $scope.tt = $scope.listVD;
        $scope.listVD = [];
        $scope.listVD = $scope.tt;
    }
    $scope.getDate_byClaimID = function () {

        $http.get('/Bill/getDate_byClaimID?claimId='+_claimId).then(function (response) {
            console.log(response);
            $scope.listSV = response.data.listSV;
        }, function (error) {
        })

    }
    $scope.calRowWiseAmont = function () {
        this.v.RowWiseAmont =parseFloat( this.v.KMRun) *parseFloat( this.v.RatePerKM) +parseFloat(this.v.DA);
    }

    $scope.getInsrClaimNo_byClaimID = function () {

        $http.get('/Bill/getInsrClaimNo_byClaimID?claimId=' + _claimId).then(function (response) {
            $scope.bh.ClaimNo = response.data.claimNo;
        }, function (error) {
        })

    }
    $scope.cancel = function () {
        location.reload();
    }
    $scope.getInsrClaimNo_byClaimID();
    $scope.getDate_byClaimID();
    $scope.getModeOfConveyance();

    $scope.getBillHeader();
})