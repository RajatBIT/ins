﻿angular.module('myApp').controller('SalvageCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.a = {};
    $scope.q = {};
    $scope.o = {};
    $scope.divQuery = 0;
    $scope.divOffer = 0;
    var temp_PK_AS_ID = 0;
    $scope.saveAdminSalvage = function () {
        var x = $('#formSalvage');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }

        $scope.a.ClaimID = angular.copy(_claimId);
        $http.post('/Salvage/saveAdminSalvage', { a: $scope.a }).then(function (response) {
            console.log(response);
            $scope.a = {};
            var pk_AS_Id = response.data.pk_AS_Id;
            console.log(pk_AS_Id);
            var p1, p2, p3, p4;
            debugger;
            if (window.FormData != undefined) {
                var fileData = new FormData();
                if ($("#Photo1").val() != '') {
                    fileData.append($("#Photo1").get(0).files[0].name, $("#Photo1").get(0).files[0]);
                    p1 = "yes";
                }
                if ($("#Photo2").val() != '') {
                    fileData.append($("#Photo2").get(0).files[0].name, $("#Photo2").get(0).files[0]);
                      p2 = "yes";
                }
                if ($("#Photo3").val() != '') {
                    fileData.append($("#Photo3").get(0).files[0].name, $("#Photo3").get(0).files[0]);
                    p3 = "yes";
                }
                if ($("#Photo4").val() != '') {
                    fileData.append($("#Photo4").get(0).files[0].name, $("#Photo4").get(0).files[0]);
                    p4 = "yes";
                }



                $http({
                    url: '/Salvage/saveAdminSalvagePhotoORDoc?p1=' + p1 + '&p2=' + p2 + '&p3=' + p3 + '&p4=' + p4 + '&pk_AS_Id=' + pk_AS_Id,
                    method: 'POST',
                    data: fileData,
                    headers: { 'Content-Type': undefined },
                    transformRequest: angular.identity
                });
                //var request = {
                //    method: 'POST',
                //    url: '/Salvage/saveAdminSalvagePhotoORDoc?p1=' + p1 + '&p2=' + p2 + '&p3=' + p3 + '&p4=' + p4 + '&pk_AS_Id=' + pk_AS_Id,
                //    data: fileData,
                //    headers: {
                //        'Content-Type': undefined
                //    }
                //};

                //// SEND THE FILES.
                //$http(request)
                //    .success(function (d) {
                //        alert(d);
                //    })
                //    .error(function () {
                //    });
            }
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully");
                $("#Photo1").val('');
                $("#Photo2").val('');
                $("#Photo3").val('');
                $("#Photo4").val('');
            }
            else {
                showSuccessToast("Failed!Try Again");
                $("#Photo1").val('');
                $("#Photo2").val('');
                $("#Photo3").val('');
                $("#Photo4").val('');
            }
            $scope.getAdminSalvage();
        }, function (error) {
        })

    }
    $scope.cancel = function () {
        $scope.a = {};
        $("#Photo1").val('');
        $("#Photo2").val('');
        $("#Photo3").val('');
        $("#Photo4").val('');
    }
    $scope.getAdminSalvage = function () {

        $http.get('/Salvage/getAdminSalvage?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listAS = response.data.listAS;
        }, function (error) {
        })

    }
    $scope.rowClickAdminSalvage = function () {
        $scope.a = angular.copy(this.a);
    }
    $scope.getRefNo_And_InsuredName_byClaimID = function () {

        $http.get('/Collaborator/getRefNo_And_InsuredName_byClaimID?claimId=' + _claimId).then(function (response) {
            $scope.RefNo = response.data.RefNo;
            $scope.InsuredName = response.data.InsuredName;
        }, function (error) {
        })

    }
    $scope.showModelSalvage = function (p, id) {
        temp_PK_AS_ID = angular.copy(id);
        if (p == 'q') {
            $scope.divOffer = 0;
            $scope.divQuery = 1;
            $scope.getSalvageQuery();
            $('#myModal').modal('show');
        }
        else if (p == 'o') {
            $scope.divQuery = 0;
            $scope.divOffer = 1;
            $scope.getSalvageOffer();
            $('#myModal').modal('show');
        }

    }




    $scope.saveSalvageQuery = function () {
        //var x = $('#formSalvage');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        $scope.q.AS_ID = angular.copy(temp_PK_AS_ID);
        $scope.q.ClaimID = angular.copy(_claimId);
        $http.post('/Salvage/saveSalvageQuery', { q: $scope.q }).then(function (response) {
            console.log(response);

            if (response.data == 's') {
                showSuccessToast("Saved Successfully");
            }
            else {
                showSuccessToast("Failed!Try Again");
            }
        }, function (error) {
        })
    }
    $scope.saveSalvageOffer = function () {
        //var x = $('#formSalvage');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        $scope.o.AS_ID = angular.copy(temp_PK_AS_ID);
        $scope.o.ClaimID = angular.copy(_claimId);
        $http.post('/Salvage/saveSalvageOffer', { o: $scope.o }).then(function (response) {
            console.log(response);

            if (response.data == 's') {
                showSuccessToast("Saved Successfully");
            }
            else {
                showSuccessToast("Failed!Try Again");
            }
        }, function (error) {
        })
    }

    $scope.getSalvageQuery = function () {
        $http.get('/Salvage/getSalvageQuery?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.q = response.data.q;
        }, function (error) {
        })

    }
    $scope.getSalvageOffer = function () {
        $http.get('/Salvage/getSalvageOffer?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.o = response.data.o;
        }, function (error) {
        })

    }
   
    $scope.getRefNo_And_InsuredName_byClaimID();
    $scope.getAdminSalvage();
})



