﻿angular.module('myApp').controller('PaymentReceiveCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.JobNo;
    $scope.getJobNo_byClaimID = function () {

        $http.get('/Bill/getJobNo_byClaimID?claimId='+_claimId).then(function (response) {
            console.log(response);
            $scope.JobNo = response.data.JobNo;
        }, function (error) {
        })

    }
    $scope.savePaymentReceive = function (payRec,cd) {
        if (cd == "Closure")
        {
            payRec.Status = "Closure";
            console.log(payRec.Status);
        }
        else if (cd == "Dispute") {
            payRec.Status = "Dispute";
            console.log(payRec.Status);
        }
        payRec.ClaimID = _claimId;
        $http.post('/Bill/savePaymentReceive', {pr:payRec}).then(function (response) {
            console.log(response);
            $scope.JobNo = response.data.JobNo;
            $scope.getPaymentReceive();
        }, function (error) {
        })

    }
    $scope.getPaymentReceive = function () {
        $http.get('/Bill/getPaymentReceive?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listPR = response.data.listPR;
            angular.forEach($scope.listPR, function (element, index) {
                //if (element.Status != "Closure" || element.Status != "Dispute")
                //{
                //    element.btnCD = 1;
                //}
               


            })
        }, function (error) {
        })
    }
    $scope.check_Closure_OR_Dispute = function () {
        if(parseFloat( this.pr.InvoiceAmount)==parseFloat( this.pr.FeeAmount))
        {
            this.pr.btnCD = 0;
        }
       else if (parseFloat( this.pr.InvoiceAmount) !=parseFloat( this.pr.FeeAmount)) {
            this.pr.btnCD = 1;
        }
    }
    $scope.convertFloat = function (p1) {
        return parseFloat(p1);

    }

    $scope.getJobNo_byClaimID();
    $scope.getPaymentReceive();

})