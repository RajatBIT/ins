﻿angular.module('myApp').controller('ReminderForFeeCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.Days = 0;
    $scope.btnSave = 1;
    $scope.rh = {};
    $scope.RefNo;
    $scope.getRefNo_byClaimID = function () {

        $http.get('/Bill/getJobNo_byClaimID?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.RefNo = response.data.JobNo;
        }, function (error) {
        })

    }
    $scope.getInsurers = function () {

        $http.get('/MISCFSR/getInsurers').then(function (response) {
            $scope.listIns = angular.copy(response.data.listIns);

        }, function (error) {
        })
    }
    $scope.getReminderForFree = function () {

        $http.get('/Bill/getReminderForFree?claimId='+_claimId+'&days='+$scope.Days).then(function (response) {
            $scope.listRF = angular.copy(response.data.listRF);

        }, function (error) {
        })
    }
    $scope.saveReminder = function () {
        alert();
        $http.post('/Bill/saveReminder', { rh: $scope.rh, listRF: $scope.listRF, claimId: _claimId }).then(function (response) {
            console.log(response);

        }, function (error) {
        })
    }
    $scope.getReminderForFeeHeader = function () {

        $http.get('/Bill/getReminderForFeeHeader?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.listRH = angular.copy(response.data.listRH);

        }, function (error) {
            console.log(error);
        })
    }
    $scope.rowClickReminder = function (rh, PK_RFH_ID) {
        $scope.btnSave = 0;
        $scope.rh = angular.copy(rh);
        //alert(PK_RFH_ID);
        $http.get('/Bill/getReminderForFree_byRF_ID?claimId=' + _claimId+'&rfh_Id='+PK_RFH_ID).then(function (response) {
            console.log(response);
            $scope.listRF = angular.copy(response.data.listRF);

        }, function (error) {
            console.log(error);
        })
    }
    $scope.cancel = function () {
        location.reload();

    }
    $scope.getRefNo_byClaimID();
    $scope.getInsurers();
    $scope.getReminderForFeeHeader();


})