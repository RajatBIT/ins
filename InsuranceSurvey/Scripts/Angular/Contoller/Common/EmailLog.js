﻿angular.module('myApp').controller('EmailLogCtrl', function ($scope, $http, CommonService) {
    $scope.e = {};
    var _claimId = $('#cid').val();
     $scope.e.Task = $('#task').val();

    //var _insdType = $('#insdType').val();
    //var _insdId = $('#insdId').val();

    
    $scope.getEmailId = function () {
        $http.get('/Collaborator/getConcerned?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.c = response.data.c;
        }, function (error) {
        })
    }
    $scope.temp;
    $scope.setEmailId = function () {
        $scope.e.ToEmailIds = '';
        $scope.e.CCEmailIds = '';
        if ($scope.e.ToInsured != true && $scope.e.ToInsurers != true && $scope.e.ToBrokers != true) {
            $scope.e.CCInsured = false;
            $scope.e.CCInsurers = false;
            $scope.e.CCBrokers = false;
        }
        if ($scope.e.ToInsured == true) {
            $scope.e.ToEmailIds += $scope.c.I_ToEmailId;
            //$('#ToEmailIds').val($scope.c.I_ToEmailId);
            //$('#ToEmailIds_Val').html($scope.c.I_ToEmailId);
            //$('#ToEmailIds').next('.multiple_emails-container').remove();
            //$('#ToEmailIds').multiple_emails();
        }
        if ($scope.e.CCInsured == true) {
            if ($scope.e.ToInsured == false)
            {
                $scope.e.CCEmailIds += $scope.c.I_ToEmailId;
                //$('#CCEmailIds').val($scope.c.I_ToEmailId);
                //$('#CCEmailIds_Val').html($scope.c.I_ToEmailId);
                //$('#CCEmailIds').next('.multiple_emails-container').remove();
                //$('#CCEmailIds').multiple_emails();
            }
            $scope.e.CCEmailIds += $scope.c.I_CCEmailId;

            //$('#CCEmailIds').val($scope.c.I_CCEmailId);
            //$('#CCEmailIds_Val').html($scope.c.I_CCEmailId);
            //$('#CCEmailIds').next('.multiple_emails-container').remove();
            //$('#CCEmailIds').multiple_emails();
        }



        if ($scope.e.ToInsurers == true) {
            $scope.e.ToEmailIds += $scope.c.Is_ToEmailId;
            //$('#ToEmailIds').val($scope.c.Is_ToEmailId);
            //$('#ToEmailIds_Val').html($scope.c.Is_ToEmailId);
            //$('#ToEmailIds').next('.multiple_emails-container').remove();
            //$('#ToEmailIds').multiple_emails();
        }
        if ($scope.e.CCInsurers == true) {
            if ($scope.e.ToInsurers == false) {
                $scope.e.CCEmailIds += $scope.c.Is_ToEmailId;

                //$('#CCEmailIds').val($scope.c.Is_ToEmailId);
                //$('#CCEmailIds_Val').html($scope.c.Is_ToEmailId);
                //$('#CCEmailIds').next('.multiple_emails-container').remove();
                //$('#CCEmailIds').multiple_emails();
            }
            $scope.e.CCEmailIds += $scope.c.Is_CCEmailId;
            //$('#CCEmailIds').val($scope.c.Is_CCEmailId);
            //$('#CCEmailIds_Val').html($scope.c.Is_CCEmailId);
            //$('#CCEmailIds').next('.multiple_emails-container').remove();
            //$('#CCEmailIds').multiple_emails();

        }


        if ($scope.e.ToBrokers == true) {
            $scope.e.ToEmailIds += $scope.c.B_ToEmailId;
            console.log($scope.e.ToEmailIds);
            //$('#ToEmailIds').val($scope.c.B_ToEmailId);
            //$('#ToEmailIds_Val').html($scope.c.B_ToEmailId);
            //$('#ToEmailIds').next('.multiple_emails-container').remove();
            //$('#ToEmailIds').multiple_emails();

        }
        if ($scope.e.CCBrokers == true) {
            if ($scope.e.ToBrokers == false) {
                $scope.e.CCEmailIds += $scope.c.B_ToEmailId;
                //$('#CCEmailIds').val($scope.c.B_ToEmailId);
                //$('#CCEmailIds_Val').html($scope.c.B_ToEmailId);
                //$('#CCEmailIds').next('.multiple_emails-container').remove();
                //$('#CCEmailIds').multiple_emails();
            }
            $scope.e.CCEmailIds += $scope.c.B_CCEmailId;
            //$('#CCEmailIds').val($scope.c.B_CCEmailId);
            //$('#CCEmailIds_Val').html($scope.c.B_CCEmailId);
            //$('#CCEmailIds').next('.multiple_emails-container').remove();
            //$('#CCEmailIds').multiple_emails();

        }
        $scope.e.ToEmailIds = $scope.e.ToEmailIds.replace("][", ",").replace("][", ",").replace("][", ",").replace("][", ",");
        $('#ToEmailIds').val($scope.e.ToEmailIds);
        $('#ToEmailIds_Val').html($scope.e.ToEmailIds);
        $('#ToEmailIds').next('.multiple_emails-container').remove();
        $('#ToEmailIds').multiple_emails();
        $scope.e.CCEmailIds = $scope.e.CCEmailIds.replace("][", ",").replace("][", ",").replace("][", ",").replace("][", ",");
        console.log($scope.e.CCEmailIds);
        
        
        $('#CCEmailIds').val($scope.e.CCEmailIds);
        $('#CCEmailIds_Val').html($scope.e.CCEmailIds);
        $('#CCEmailIds').next('.multiple_emails-container').remove();
        $('#CCEmailIds').multiple_emails();
        $('.multiple_emails-input').attr("readonly", true);
        $("a").removeClass("multiple_emails-close");
        $("span").removeClass("glyphicon glyphicon-remove");
        }
    $scope.sendEmail_saveEmailLog = function () {
        if ($scope.e.ToEmailIds == '' || $scope.e.ToEmailIds == null || $scope.e.ToEmailIds == undefined)
        {
            alert("No Email Found");
            return false;
        }
        if ($scope.e.CCEmailIds == '' || $scope.e.CCEmailIds == null || $scope.e.CCEmailIds == undefined) {
            alert("No Email Found");
            return false;
        }
        $scope.e.ClaimID = _claimId;
        $scope.e.Task = $scope.e.Task;
        $scope.e.InsdType = $scope.InsdType;
        $scope.e.InsdID = $scope.InsdId;
        $http.post('/Survery/sendEmail_saveEmailLog', { e: $scope.e }).then(function (response) {
            console.log(response);
                if (response.data.msg == 's') {
                    showSuccessToast("Send Mail Successfully");
                    $scope.e = {};
                }
                else {
                    showSuccessToast("Failed! Try Again");
                    $scope.e = {};
                }
               
        }, function (error) {
        })
    }



    $scope.sendEmail_Lor = function () {
        $http.get('/SuperAdmin/sendEmail_Lor').then(function (response) {
            console.log(response);
            if(response.data.msg=='s')
            {
                showSuccessToast("Send mail Successfully");
            }
            if (response.data.msg == 'n') {
                showSuccessToast("No Email Found");
            }

            else {
                showSuccessToast("Failed! Try Again");
            }
        }, function (error) {
        })
    }
    $scope.show_Modal_SendMail = function () {
        $('#modal_GeneratePDF').modal('hide');
        $('#modal_SendMail').modal('show');
    }
    $scope.getDataFromMarineJIRForAssessment = function () {

        $http.get('/Collaborator/getDataFromMarineJIRForAssessment?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.InsdType = response.data.InsdType;
            $scope.InsdId = response.data.InsdId;
        }, function (error) {
        })
    }
    $scope.show_modal_GeneratePDF = function () {
        $('#modal_sendmail').modal('hide');
        $('#modal_GeneratePDF').modal('show');
        $('.modal').on('shown.bs.modal', function () {
            $(this).find('iframe').attr('src', "/Survery/GeneratePdf?ClaimID=" + _claimId + "&InsdType=" + $scope.InsdType + "&InsdID=" + $scope.InsdId + "&Task=" + $('#task').val() + "");
        })
    }

    $scope.automaticSendMarineLorReminder = function () {
        $http.get('/SuperAdmin/automaticSendMarineLorReminder').then(function (response) {
            console.log(response);
            if( response.data.msg=='s')
            {
                showSuccessToast("Send Mail Successfully");
            }
            else {
                showSuccessToast("Not Send Mail!Try Again");
            }

        }, function (error) {
        })
    }

    $scope.getDataFromMarineJIRForAssessment();
    $scope.getEmailId();
})