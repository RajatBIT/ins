﻿angular.module('myApp').controller('ConcernedCtrl', function ($scope, $http) {

    $scope.c = {};
    var _claimId = $('#claimId').val();
    $scope.getRegion = function () {

        $http.get('/Collaborator/getRegion').then(function (response) {
            console.log(response);
            $scope.listRegion = response.data.listRegion;
        }, function (error) {
        })

    }
    $scope.getState_forBroker = function (r_Id) {
        $http.get('/Collaborator/getState_byRegion?r_Id=' + r_Id).then(function (response) {
            console.log(response);
            $scope.listState_forBroker = response.data.listState;

        }, function (error) {
        })
    }

    $scope.getState_forInsured = function (r_Id) {
        $http.get('/Collaborator/getState_byRegion?r_Id=' + r_Id).then(function (response) {
            console.log(response);
            $scope.listState_forInsured = response.data.listState;

        }, function (error) {
        })
    }


    $scope.getBroker = function (r_Id) {

        $http.get('/Collaborator/getBroker').then(function (response) {
            console.log(response);
            $scope.listBroker = response.data.listBroker;
        }, function (error) {
        })

    }

    $scope.showModalConcerned = function () {

        $('#ModalConcerned').modal('show');

    }
    $scope.test = function () {
        alert();
        var emailIDs = [];
        $('#ccEmailBroker .emailAdd').each(function (index, element) {
            emailIDs.push($(element).text());
            console.log(emailIDs);
        })


    }
    $scope.saveConcerned = function () {
        $scope.c.PolicyNo = $('#policyNo').val();
        $scope.c.ClaimID = angular.copy(_claimId);
        //$('#toEmailBroker .emailAdd').each(function (index, element) {
        //    $scope.c.B_ToEmailId = '';
        //    debugger;
        //    if (index == 0) {
        //        $scope.c.B_ToEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.B_ToEmailId += ',' + $(element).text();
        //    }

        //})
        //$('#ccEmailBroker .emailAdd').each(function (index, element) {
        //    $scope.c.B_CCEmailId = '';
        //    if (index == 0) {
        //        $scope.c.B_CCEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.B_CCEmailId += ',' + $(element).text();
        //    }

        //})
        //$('#toEmailInsured .emailAdd').each(function (index, element) {
        //    $scope.c.I_ToEmailId = '';
        //    if (index == 0) {
        //        $scope.c.I_ToEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.I_ToEmailId += ',' + $(element).text();
        //    }

        //})
        //$('#ccEmailInsured .emailAdd').each(function (index, element) {
        //    $scope.c.I_CCEmailId = '';
        //    if (index == 0) {
        //        $scope.c.I_CCEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.I_CCEmailId += ',' + $(element).text();
        //    }

        //})
        //$('#toEmailInsurers .emailAdd').each(function (index, element) {
        //    $scope.c.Is_ToEmailId = '';
        //    if (index == 0) {
        //        $scope.c.Is_ToEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.Is_ToEmailId += ',' + $(element).text();
        //    }

        //})
        //$('#ccEmailInsurers .emailAdd').each(function (index, element) {
        //    $scope.c.Is_CCEmailId = '';
        //    if (index == 0) {
        //        $scope.c.Is_CCEmailId += $(element).text();
        //    }
        //    else {
        //        $scope.c.Is_CCEmailId += ',' + $(element).text();
        //    }

        //})
        $scope.c.B_ToEmailId = $('#toEmailBrokerVal').html();
        $scope.c.B_CCEmailId = $('#ccEmailBrokerVal').html();
        $scope.c.I_ToEmailId = $('#toEmailInsuredVal').html();
        $scope.c.I_CCEmailId = $('#ccEmailInsuredVal').html();
        $scope.c.Is_ToEmailId = $('#toEmailInsurersVal').html();
        $scope.c.Is_CCEmailId = $('#ccEmailInsurersVal').html();
        $http.post('/Collaborator/saveConcerned', { c: $scope.c }).then(function (response) {

            console.log(response);
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
        }, function (error) {
        })

    }

    $scope.getConcerned = function (r_Id) {
        $http.get('/Collaborator/getConcerned?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.c = response.data.c;
            $scope.getState_forBroker($scope.c.B_R_ID);
            $scope.getState_forInsured($scope.c.I_R_ID);
            if ($scope.c.B_ToEmailId != null && $scope.c.B_ToEmailId != undefined && $scope.c.B_ToEmailId != '') {
                $('#toEmailBroker').val($scope.c.B_ToEmailId);
                $('#toEmailBrokerVal').html($scope.c.B_ToEmailId);
                $('#toEmailBroker').next('.multiple_emails-container').remove();
                $('#toEmailBroker').multiple_emails();
            }
            if ($scope.c.B_CCEmailId != null && $scope.c.B_CCEmailId != undefined && $scope.c.B_CCEmailId != '') {
                $('#ccEmailBroker').val($scope.c.B_CCEmailId);
                $('#ccEmailBrokerVal').html($scope.c.B_CCEmailId);
                $('#ccEmailBroker').next('.multiple_emails-container').remove();
                $('#ccEmailBroker').multiple_emails();
            }

            if ($scope.c.I_ToEmailId != null && $scope.c.I_ToEmailId != undefined && $scope.c.I_ToEmailId != '') {
                $('#toEmailInsured').val($scope.c.I_ToEmailId);
                $('#toEmailInsuredVal').html($scope.c.I_ToEmailId);
                $('#toEmailInsured').next('.multiple_emails-container').remove();
                $('#toEmailInsured').multiple_emails();
            }
            if ($scope.c.I_CCEmailId != null && $scope.c.I_CCEmailId != undefined && $scope.c.I_CCEmailId != '') {
                $('#ccEmailInsured').val($scope.c.I_CCEmailId);
                $('#ccEmailInsuredVal').html($scope.c.I_CCEmailId);
                $('#ccEmailInsured').next('.multiple_emails-container').remove();
                $('#ccEmailInsured').multiple_emails();
            }

            if ($scope.c.Is_ToEmailId != null && $scope.c.Is_ToEmailId != undefined && $scope.c.Is_ToEmailId != '') {
                $('#toEmailInsurers').val($scope.c.Is_ToEmailId);
                $('#toEmailInsurersVal').html($scope.c.Is_ToEmailId);
                $('#toEmailInsurers').next('.multiple_emails-container').remove();
                $('#toEmailInsurers').multiple_emails();
            }
            if ($scope.c.Is_CCEmailId != null && $scope.c.Is_CCEmailId != undefined && $scope.c.Is_CCEmailId != '') {
                $('#ccEmailInsurers').val($scope.c.Is_CCEmailId);
                $('#ccEmailInsurersVal').html($scope.c.Is_CCEmailId);
                $('#ccEmailInsurers').next('.multiple_emails-container').remove();
                $('#ccEmailInsurers').multiple_emails();
            }
        }, function (error) {
        })
    }


    
    

    

    $scope.test = function () {

        var arr = '["abc@gmail.com","asdfasdf@gasfa.com","asdfadf@asdf.com"]';

        $('#demo').val(arr);
        $('#demo1Val').html(arr);
        $('#demo').next('.multiple_emails-container').remove();
        $('#demo').multiple_emails();

        $('#toEmailBroker').val(arr);
        $('#toEmailBrokerVal').html(arr);
        $('#toEmailBroker').next('.multiple_emails-container').remove();
        $('#toEmailBroker').multiple_emails();
        alert($('#demo1Val').html());
        alert($('#toEmailBrokerVal').html());
    }
    $scope.getLocation = function () {
        $scope.listLocation = [
           { "LocationId": 1, "LocationName": "Location1" },
                      { "LocationId": 2, "LocationName": "Location2" }
        ];


    }


    $scope.getRegion();
    $scope.getBroker();
    $scope.getConcerned();
    $scope.getLocation();

})