﻿angular.module('myApp').controller('MISCIntroClaimCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.ic = {};
   
    $scope.ClaimID = $('#cid').val();
    $scope.val = "ISVR";
    $scope.ShowCL_OP = function () {

        $http.get('/MISCISVR/ShowCL_OP').then(function (response) {
            console.log(response);
            $scope.ListCL_OP = angular.copy(response.data);
        }, function (error) {
        })

    }
    $scope.ShowMISCIntroClaim = function () {
        $http.get('/MISCISVR/ShowMISCIntroClaim?claimId=' + $scope.ClaimID + ' ').then(function (response) {

            console.log(response);
            //$scope.ic.PolicyNo = angular.copy(response.data.PolicyNo)
            $scope.list_Claim = angular.copy(response.data);
            if ($scope.list_Claim == undefined || $scope.list_Claim == null || $scope.list_Claim == ' ') {
            }
            else {
                $scope.list_Claim.IntimationLossDT = $filter('date')($scope.list_Claim.IntimationLossDT.slice(6, -2), 'dd-MM-yyyy');
                $scope.list_Claim.AllotmentSurveyDT = $filter('date')($scope.list_Claim.AllotmentSurveyDT.slice(6, -2), 'dd-MM-yyyy');
                $scope.list_Claim.SurveyVisitInitiatedDT = $filter('date')($scope.list_Claim.SurveyVisitInitiatedDT.slice(6, -2), 'dd-MM-yyyy');
                $scope.list_Claim.SurveyVisitCompletedDT = $filter('date')($scope.list_Claim.SurveyVisitCompletedDT.slice(6, -2), 'dd-MM-yyyy');
                $scope.list_Claim.LossDT = $filter('date')($scope.list_Claim.LossDT.slice(6, -2), 'dd-MM-yyyy');

                $scope.ic = angular.copy($scope.list_Claim);
            }
            //angular.forEach($scope.list_Claim, function (element, index) {
            //    $scope.ic.InsurersClaimNo = element.InsurersClaimNo;
            //    $scope.ic.PolicyNo=element.PolicyNo;
            //    $scope.ic.PolicyType = element.PolicyType;
            //    $scope.ic.IntimationLossDT = $filter('date')(element.IntimationLossDT.slice(6,-2),'dd-MM-yyyy');
            //    $scope.ic.AllotmentSurveyDT =$filter('date')(element.AllotmentSurveyDT.slice(6,-2),'dd-MM-yyyy');
            //    $scope.ic.SourceAllotmentSurvey = element.SourceAllotmentSurvey;
            //    $scope.ic.Insurers = element.Insurers;
            //    $scope.ic.Address = element.Address;
            //    $scope.ic.SurveyVisitInitiatedDT =$filter('date')( element.SurveyVisitInitiatedDT.slice(6,-2),'dd-MM-yyyy');
            //    $scope.ic.SurveyVisitCompletedDT =$filter('date')(element.SurveyVisitCompletedDT.slice(6,-2),'dd-MM-yyyy');
            //    $scope.ic.ReasonDelaySurvey = element.ReasonDelaySurvey;
            //    $scope.ic.InsuredName = element.InsuredName;
            //    $scope.ic.InsuredAddress = element.InsuredAddress;
            //    $scope.ic.PS_ALL = element.PS_ALL;
            //    $scope.ic.PS_ALL_Place = element.PS_ALL_Place;
            //    $scope.ic.Loss = element.Loss;
            //    $scope.ic.LossDT =$filter('date')(element.LossDT.slice(6,-2),'dd-MM-yyyy');
            //    $scope.ic.CL_OP = element.CL_OP;
            //    $scope.ic.CL_OP_ID = element.CL_OP_ID;
            //})
            // //$scope.ic = angular.copy($scope.list_MISCIntroClaim);
            //console.log($scope.list_MISCIntroClaim);
        }, function (error) {
        })
    }
    $scope.SaveMISCIntroClaim = function (rec) {

        var x = $("#formISVR");
        var Status = $(x).ValidationFunction();
        if (Status == false) {
            return false;
        }
        $scope.ic.ClaimID =angular.copy($scope.ClaimID);
        var sc = angular.copy($scope.ic);
        sc.IntimationLossDT = CommonService.convertDateStringToDateic(sc.IntimationLossDT);
        sc.AllotmentSurveyDT = CommonService.convertDateStringToDateic(sc.AllotmentSurveyDT);
        sc.SurveyVisitInitiatedDT = CommonService.convertDateStringToDateic(sc.SurveyVisitInitiatedDT);
        sc.SurveyVisitCompletedDT = CommonService.convertDateStringToDateic(sc.SurveyVisitCompletedDT);
        sc.LossDT = CommonService.convertDateStringToDateic(sc.LossDT);
       

        $http.post('/MISCISVR/SaveMISCIntroClaim', sc).then(function (SaveResponse) {
            console.log(SaveResponse);
            showSuccessToast("Save Successfully");
            if(rec=='n')
            {
                window.location = "/MISCFSR/SurveyParticulars?cid=";
            }
        },
          function (error) {

          })
    }
    $scope.Cancel = function () {
       var id=angular.copy($scope.ic.PK_MISCClaim_ID);
        $scope.ic = {};
        sc = {};
        $scope.ic.PK_MISCClaim_ID = angular.copy(id);
        console.log($scope.ic.PK_MISCClaim_ID);
        console.log($scope.ClaimID);
    }
    $scope.ShowCL_OP();
     $scope.ShowMISCIntroClaim();
})