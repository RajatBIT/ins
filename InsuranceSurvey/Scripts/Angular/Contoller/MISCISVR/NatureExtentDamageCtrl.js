﻿angular.module('myApp')
    .controller('NatureExtentDamageCtrl', function ($scope, $http, CommonService) {
        $scope.obj = {};
      var claimId = $('#cid').val();
        $scope.getNatureDmgls = function () {

            $http.get('/MISCISVR/getNatureDmgls').then(function (response) {
                console.log(response);
                $scope.list_DmgLs = angular.copy(response.data.listDmgLs);
                console.log($scope.list_DmgLs);
            }, function (error) {
            })
        }
        $scope.getCovMen=function(){
            $http.get('/MISCISVR/getCovMen').then(function (response) {
                console.log(response);
                $scope.list_CovMen = angular.copy(response.data.listCovMen);
                console.log($scope.list_CovMen);
            }, function (error) {
            })

        }
        $scope.getNatureExtentDamage = function () {

            $http.get('/MISCISVR/getNatureExtentDamage?ClaimId='+claimId+'').then(function (response) {
                console.log(response);
                $scope.list_NED = angular.copy(response.data.listNED);
                console.log($scope.list);
            }, function (error) {
            })

        }
        $scope.saveNatureExtentDamage = function () {
            var x = $("#formNatExtDmg");
            var status = $(x).ValidationFunction();

            if (status == false) {
                return false;
            }
            $scope.obj.ClaimID = claimId;
            $scope.obj_copy = angular.copy($scope.obj);

            $http.post('/MISCISVR/saveNatureExtentDamage', $scope.obj_copy).then(function (SaveResponse) {
                console.log(SaveResponse);
                $scope.getNatureExtentDamage();
                showSuccessToast("Save Successfully");
                $scope.cancel();
            },
              function (error) {

              })
        }

        $scope.cancel = function () {
            $scope.obj = {};
            $scope.obj_copy = {};
        }
        $scope.rowClick = function (rowData) {
            $scope.obj = angular.copy(rowData);
            console.log($scope.obj.NatureOfDmgLsID);
            console.log($scope.obj.CovUndMenID);
        }

        $scope.getNatureDmgls();
        $scope.getCovMen();
       $scope.getNatureExtentDamage();
    })