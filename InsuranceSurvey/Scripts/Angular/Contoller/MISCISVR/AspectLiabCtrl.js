﻿angular.module('myApp')
    .controller('AspectLiabCtrl', function ($scope, $http, CommonService) {
        $scope.obj = {};
        $scope.obj.RemarkAdvID = '';
        $scope.Noteoccrsteye = " ";
        $scope.ClaimID = $('#cid').val();
        $scope.rid = [];
        $scope.Path = "/files/MISC/";
        var Temp;
        //$scope.pathcheck = "/files/misc/a0fd0077-f9d1-4075-a729-949d9b5d04d6.png";

        $scope.getCmtLiabSP = function () {

            $http.get('/MISCISVR/getCmtLiabSP').then(function (response) {
                console.log(response);
                $scope.list_CmtLiabSP = angular.copy(response.data.listCmtLiabSP);
                console.log($scope.list_CmtLiabSP);
            }, function (error) {
            })
        }
        $scope.getRemarkAdv = function () {
            $http.get('/MISCISVR/getRemarkAdv').then(function (response) {
                console.log(response);
                $scope.list_RemarkAdv = angular.copy(response.data.listRA);
                console.log($scope.list_RemarkAdv);
            }, function (error) {
            })

        }
        $scope.getAspectsLiab = function () {
            //alert($scope.ClaimID);
            $http.get('/MISCISVR/getAspectsLiab?ClaimId=' + $scope.ClaimID + ' ').then(function (response) {
                console.log(response);
                $scope.list_AL = angular.copy(response.data.AL);
                if ($scope.list_AL == null || $scope.list_AL == " " || $scope.list_AL == undefined) {
                }
                else {
                    rid = $scope.list_AL.RemarkAdvID.split(',');
                    angular.forEach(rid, function (e, i) {
                        Temp = e;
                        console.log(Temp);
                        angular.forEach($scope.list_RemarkAdv, function (element, index) {
                            if (element.RemarkAdvID == Temp) {
                                element.chkbxRemarksAdv = true;
                                console.log();
                            }
                        })

                    })
                    $scope.obj = angular.copy($scope.list_AL);
                }

            }, function (error) {
            })
        }


        $scope.saveAspectLiab = function () {
            var x = $("#formAspectLiab");
            var status = $(x).ValidationFunction();

            if (status == false) {
                return false;
            }
            var RemarkId = '';
            angular.forEach($scope.list_RemarkAdv, function (element, index) {
                if (element.chkbxRemarksAdv) {
                    RemarkId += element.RemarkAdvID + ',';

                }
            })
            RemarkId = RemarkId.slice(0, -1);
            $scope.obj.RemarkAdvID = RemarkId;

            console.log($scope.obj.RemarkAdvID);
            $scope.obj.ClaimID = angular.copy($scope.ClaimID);

            $scope.obj_copy = angular.copy($scope.obj);
            $http.post('/MISCISVR/saveAspectLiab', $scope.obj_copy).then(function (Response) {
                console.log(Response);
               
            },
              function (error) {

              })




            var nos = ' ';
         
            if (window.FormData !== undefined) {
         
                var fileData = new FormData();
                if ($("#CLQuotePhotoDoc").val() != '') {
         
                    fileData.append($("#Noteoccrsteye").get(0).files[0].name, $("#Noteoccrsteye").get(0).files[0]);
                    nos= "yes";
                   
                }

                   $.ajax({
                       url: '/MISCISVR/saveAspectLiabPhotoDoc?nos=' + nos+ '&claimId=' + $scope.ClaimID + ' ',
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fileData,
                    success: function (result) {
                        //alert(result);
                    },
                    error: function (err) {

                        console.log(err.statusText);
                        //alert(err.statusText);
                    }
                });

            }
            $scope.getAspectsLiab();
            showSuccessToast("Save Successfully");
        }
        $scope.cancel = function () {
            var id = angular.copy($scope.obj.PK_AL_ID);
            $scope.obj = {};
            $scope.obj_copy = {};

            //$scope.ra.chkbxRemarksAdv = false;
            $scope.obj.PK_AL_ID = angular.copy(id);
        }

        $scope.getRemarkAdv();
        $scope.getCmtLiabSP();

        $scope.getAspectsLiab();
    })

