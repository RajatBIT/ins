﻿angular.module('myApp').controller('RecoverySubrogationCtrl', function ($scope, $http) {
    $scope.rs = {};

    var _claimId = $('#cid').val();
    $scope.saveRecoverySubrogation = function (rec) {
        var x = $('#formRS');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.rs.ClaimID = angular.copy(_claimId);
        var rs_copy = {};
        rs_copy = angular.copy($scope.rs);
        console.log(rs_copy.AspectsofRecovery);
        console.log(rs_copy.SubrogationRights);

        $http.post('/MISCFSR/saveRecoverySubrogation', { rs: rs_copy }).then(function (response) {
            console.log(response);
            $scope.getRecoverySubrogation();

            if (rec == 'n') {
                window.location = "/MISCFSR/InsuredClaim?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }

    $scope.getRecoverySubrogation = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getRecoverySubrogation?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.rs = angular.copy(response.data.rs);

        }, function (error) {
        })

    }

    $scope.cancel = function () {
        var id = angular.copy($scope.rs.PK_RS_ID);
        $scope.rs = {};
        $scope.rs_copy = {};
        $scope.rs.PK_RS_ID = angular.copy(id);
    }
    $scope.getRecoverySubrogation();

})