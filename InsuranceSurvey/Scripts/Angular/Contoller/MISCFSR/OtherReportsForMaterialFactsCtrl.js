﻿angular.module('myApp').controller('OtherReportsForMaterialFactsCtrl', function ($scope, $http) {
    $scope.ormf = {};
    //var _claimId ;
    //$(window).load(function () {
    //    _claimId = angular.copy($('#cid').val());
    //    console.log(_claimId);
    //})
    //var _claimId;
    $scope.ipc = 0;
  var _claimId = angular.copy($('#cid').val());
  var a = "hello;"
 

  $scope.saveOtherReportsForMaterialFacts = function (rec) {
      var vIPC = '';
      angular.forEach($scope.listIPSec, function (element, index) {
          if (element.chkboxIPC) {
              vIPC += element.PK_IPC_ID + ',';
          }
      })
      vIPC = vIPC.slice(0, -1);
      console.log(vIPC);
      $scope.ormf.ClaimID = angular.copy(_claimId);
      $scope.ormf.Pr_IPC_ID = vIPC;
      var ormf_copy = angular.copy($scope.ormf);
      $http.post('/MISCFSR/saveOtherReportsForMaterialFacts', { ormf: ormf_copy }).then(function (response) {
          var pol = '';
          var fr = '';
          var met = '';
          if (window.FormData !== undefined) {
              //alert($("#PoliceReportPhoto").val());
              var fileData = new FormData();
              if ($("#PoliceReportPhoto").val() != '') {
                  //alert("hello");
                  fileData.append($("#PoliceReportPhoto").get(0).files[0].name, $("#PoliceReportPhoto").get(0).files[0]);
                  pol = "yes";
                  //alert($("#PoliceReportPhoto").val());
                  //alert(pol);
              }
              if ($("#FireBrigadeReportPhoto").val() != '') {
                  fileData.append($("#FireBrigadeReportPhoto").get(0).files[0].name, $("#FireBrigadeReportPhoto").get(0).files[0]);
                  fr = "yes";
                  //alert($("#FireBrigadeReportPhoto").val());
                  //alert(fr);
              }
              if ($("#MeteorologicalReportPhoto").val() != '') {
                  fileData.append($("#MeteorologicalReportPhoto").get(0).files[0].name, $("#MeteorologicalReportPhoto").get(0).files[0]);
                  met = "yes";
                  //alert($("#MeteorologicalReportPhoto").val());
                  //alert(met);
              }


              $.ajax({
                  url: '/MISCFSR/saveOtherReportsForMaterialFactsPhoto?policePhoto=' + pol + '&firePhoto=' + fr + '&metPhoto=' + met + '&claimId=' + _claimId + ' ',
                  type: "POST",
                  contentType: false, // Not to set any content header
                  processData: false, // Not to process data
                  data: fileData,
                  success: function (result) {
                      $scope.getOtherReportsForMaterialFacts();
                     
                      //alert(result);
                  },
                  error: function (err) {
                      //alert(err.statusText);
                  }
              });

          }
          if (rec == 'n') {
              window.location = "/MISCFSR/LiabilityUnderPolicy?cid=" + _claimId + "";
          }
          else {
              showSuccessToast(response.data);
          }

      }, function (error) {
          console.log(error);
          
      })
   
      
  }






  $scope.getOtherReportsForMaterialFacts = function () {
      //alert("get");
        console.log(_claimId);
        $http.get('/MISCFSR/getOtherReportsForMaterialFacts?claimId='+_claimId+'').then(function (response) {
            console.log(response);
            var listORMF = angular.copy(response.data.listORMF);
            $scope.ormf = angular.copy(listORMF);
            var ipc_Id = [];
            var temp=0;
            ipc_Id = angular.copy($scope.ormf.Pr_IPC_ID);
            if (ipc_Id !=null) {
                ipc_Id = ipc_Id.split(',');
            }
            console.log(ipc_Id);
            angular.forEach(ipc_Id, function (element, index) {
                temp = element;
                angular.forEach($scope.listIPSec, function (element, index) {
                    if (element.PK_IPC_ID == temp) {
                       element.chkboxIPC = true;
                    }
                })
            })
        }, function (error) {
        })

        
    }

    $scope.cancel = function () {
        var id = angular.copy($scope.ormf.PK_MISCormf);
        $scope.ormf = {};
        $scope.ormf.PK_MISCormf = angular.copy(id);
    }

    $scope.getIPCSection = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getIPCSection').then(function (response) {
            console.log(response);
            $scope.listIPSec = angular.copy(response.data.listIPSec);
           
        }, function (error) {
        })
    }
    $scope.showIPC = function () {
       
        $('#myModal').modal('show');
        $scope.ipc = 1;
    }

    //$scope.photoCheck = function () {
    //    alert();
    //    var pol=' ';
    //    var fr=' ';
    //    var met=' ';
    //    if (window.FormData !== undefined) {

    //        var fileData = new FormData();
    //        if ($("#PoliceReportPhoto").val() != ' ') {
    //            fileData.append($("#PoliceReportPhoto").get(0).files[0].name, $("#PoliceReportPhoto").get(0).files[0]);
    //            pol = "yes";
    //            //alert($("#PoliceReportPhoto").val());
    //            //alert(pol);
    //        }
    //        if ($("#FireBrigadeReportPhoto").val() != ' ') {
    //            fileData.append($("#FireBrigadeReportPhoto").get(0).files[0].name, $("#FireBrigadeReportPhoto").get(0).files[0]);
    //            fr = "yes";
    //            //alert($("#FireBrigadeReportPhoto").val());
    //            //alert(fr);
    //        }
    //        if ($("#MeteorologicalReportPhoto").val() != ' ') {
    //            fileData.append($("#MeteorologicalReportPhoto").get(0).files[0].name, $("#MeteorologicalReportPhoto").get(0).files[0]);
    //            met = "yes";
    //            //alert($("#MeteorologicalReportPhoto").val());
    //            //alert(met);
    //        }


    //        $.ajax({
    //            url: '/MISCFSR/saveOtherReportsForMaterialFactsPhoto?policePhoto=' + pol + '&firePhoto=' + fr + '&metPhoto=' + met + '&claimId=' + _claimId + ' ',
    //            type: "POST",
    //            contentType: false, // Not to set any content header
    //            processData: false, // Not to process data
    //            data: fileData,
    //            success: function (result) {
    //                alert(result);
    //            },
    //            error: function (err) {
    //                alert(err.statusText);
    //            }
    //        });
    //    }
    
    //}
    $scope.getIPCSection();

    $scope.getOtherReportsForMaterialFacts();
})