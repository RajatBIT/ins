﻿angular.module('myApp').controller('AboutInsuredCtrl', function ($scope, $http) {
    $scope.ai = {};
    $scope.fs = {};
    $scope.sd = {};
    $scope.td = {};
    var _claimId = $('#cid').val();
    $scope.saveAboutInsured = function (rec) {
        var x = $('#formAI');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        var ai_copy = {};
        $scope.ai.ClaimID = angular.copy(_claimId);
        ai_copy = angular.copy($scope.ai);
 
        $http.post('/MISCFSR/saveAboutInsured', ai_copy).then(function (response) {
            console.log(response);
            if ($scope.ai.ChkbxLayoutSite == true) {
             
                if (window.FormData !== undefined) {
                    var lsPD = ' ';
                    //alert($("#PoliceReportPhoto").val());
                    var fileData = new FormData();
                    if ($("#LayoutSitePhotoDoc").val() != '') {
                        //alert("hello");
                        fileData.append($("#LayoutSitePhotoDoc").get(0).files[0].name, $("#LayoutSitePhotoDoc").get(0).files[0]);
                        lsPD = "yes";

                    }

                    $.ajax({
                        url: '/MISCFSR/saveAboutInsuredPhotoDoc?lsPD=' + lsPD + '&claimId=' + _claimId + ' ',
                        type: "POST",
                        contentType: false, // Not to set any content header
                        processData: false, // Not to process data
                        data: fileData,
                        success: function (result) {
                            if ($scope.ai.ChkbxFS == true) {
                                var fs_copy = {};
                                $scope.fs.ClaimID = angular.copy(_claimId);
                                $scope.fs.RowName = "First";
                                fs_copy = angular.copy($scope.fs);

                                $http.post('/MISCFSR/saveFinancialStatus', fs_copy).then(function (response) {
                                    console.log(response);
                                    var sd_copy = {};
                                    $scope.sd.ClaimID = angular.copy(_claimId);
                                    $scope.sd.RowName = "Second";
                                    sd_copy = angular.copy($scope.sd);
                                    $http.post('/MISCFSR/saveFinancialStatus', sd_copy).then(function (response) {
                                        console.log(response);
                                        var td_copy = {};
                                        $scope.td.ClaimID = angular.copy(_claimId);
                                        $scope.td.RowName = "Third";
                                        td_copy = angular.copy($scope.td);
                                        $http.post('/MISCFSR/saveFinancialStatus', td_copy).then(function (response) {
                                            console.log(response);
                                            $scope.getAboutInsured();
                                        }, function (error) {
                                        })
                                    }, function (error) {
                                    })
                                    console.log("f");
                                }, function (error) {
                                })
                               
                            
                            }
                            //alert(result);
                            //$scope.getAboutInsured();
                        },
                        error: function (err) {
                            //alert(err.statusText);
                        }
                    });

                }
            }
            if (rec == 'n') {
                window.location = "/MISCFSR/AboutCauseOfLoss?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
          }, function (error) {
        })
       
     
 


        
        
    }
    $scope.getAboutInsured = function () {
        $http.get('/MISCFSR/getAboutInsured?claimId=' + _claimId + ' ').then(function (response) {
            
            $scope.ai = angular.copy(response.data.listAI);
           
        }, function (error) {

        })
        $http.get('/MISCFSR/getFinancialStatus?claimId=' + _claimId + ' ').then(function (response) {
            var listFS = angular.copy(response.data.listFS);
            angular.forEach(listFS, function (element, index) {
                if (element.RowName == "First") {
                    $scope.fs = angular.copy(element);
                }
                else if (element.RowName == "Second") {
                    $scope.sd = angular.copy(element);
                }
                else if (element.RowName == "Third") {
                    $scope.td = angular.copy(element);
                }
            })
        }, function (error) {
        })
    
    }
    $scope.cancel = function () {
        var aiId = angular.copy($scope.ai.PK_MISCABTINSID);
        var fsId = angular.copy($scope.fs.PK_MISCFINSTID);
        var sdId = angular.copy($scope.sd.PK_MISCFINSTID);
        var tdId = angular.copy($scope.td.PK_MISCFINSTID);
        $scope.ai = {};
        $scope.fs = {};
        $scope.sd = {};
        $scope.td = {};
        $scope.ai.PK_MISCABTINSID = angular.copy(aiId);
        $scope.fs.PK_MISCFINSTID = angular.copy(fsId);
        $scope.sd.PK_MISCFINSTID = angular.copy(sdId);
        $scope.td.PK_MISCFINSTID = angular.copy(tdId);
    }
    $scope.getAboutInsured();
})