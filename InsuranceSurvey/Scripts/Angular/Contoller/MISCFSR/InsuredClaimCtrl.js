﻿angular.module('myApp').controller('InsuredClaimCtrl', function ($scope, $http) {
    $scope.i = {};
    var _claimId = $('#cid').val();
    $scope.TotalAmount = 0;
   

    $scope.getDescAmount = function () {

        $http.get('/MISCFSR/getDescAmount?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.listDA = angular.copy(response.data.listDA);
            //$scope.calTotalAmount();
        }, function (error) {
        })

    }
    

    $scope.getInsuredClaim = function () {

        $http.get('/MISCFSR/getInsuredClaim?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.i = angular.copy(response.data.i);

        }, function (error) {
        })

    }
    $scope.getReportLoss = function () {
        $http.get('/MISCFSR/getReportLoss?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.ReportLoss = angular.copy(response.data.rl);
        }, function (error) {
        })
    }
    $scope.saveInsuredClaim = function (rec) {
        var x = $('#formInC');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.i.ClaimID = angular.copy(_claimId);
        $http.post('/MISCFSR/saveInsuredClaim', { i:$scope.i,listDA:$scope.listDA}).then(function (response) {
            console.log(response);
            $scope.getDescAmount();
            $scope.getReportLoss();
            $scope.getInsuredClaim();
            if (rec == 'n') {
                window.location = "/MISCFSR/AssessmentofLoss?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        var id = angular.copy($scope.i.PK_IC_ID);
        $scope.i = {};
        $scope.i.PK_IC_ID =angular.copy(id);
        angular.forEach($scope.listDA, function (element, index) {
                element.Amount = 0;
        })
        $scope.TotalAmount = 0;
       
    }
    $scope.calTotalAmount = function () {
        $scope.i.TotalAmount = 0;
        angular.forEach($scope.listDA, function (element, index) {
            if (element.Amount == '') {
                element.Amount = 0;
            }
            $scope.i.TotalAmount += parseFloat(element.Amount);
        })
    }
    $scope.getDescAmount();
    $scope.getReportLoss();
    $scope.getInsuredClaim();
})
