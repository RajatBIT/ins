﻿angular.module("myApp").controller('AssessmentofLoss_PartSecondCtrl', function ($scope, $http, $filter) {
    var _claimId = $('#cid').val();
    $scope.TableName = ' ';
    $scope.ah = {};
    $scope.ac = {};
    $scope.listTableName;
    $scope.al = {};
    $scope.d = {};
    $scope.listAD;
    $scope.ah.GrossAdmLoss = 0;
    $scope.ah.TotalDeductAABNA = 0;
    $scope.ah.GrandTotal = 0;
    $scope.ah.SalvageAmount = 0;
    $scope.ah.DeprAmount = 0;
    $scope.ah.DeductofGFIP = 0;
    $scope.ah.SalvageRate = 0;
    $scope.ah.DeprRate = 0;
    $scope.ah.TotalInitClaim = 0;
    $scope.ah.TotalFinalClaim = 0;
    $scope.ac.Rate = 0;
    $scope.ac.Amount = 0;
    $scope.tempReadonly = false;
    $scope.test = 0;
    

    $scope.getDistinctTableName_byClaimID = function () {
        $http.get('/MISCFSR/getDistinctTableName_byClaimID?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listTableName = response.data.listTableName;
     
        }, function (error) {
        })
    }

    //$scope.calGrossAdmLoss = function () {
    //    $scope.ah.GrossAdmLoss = parseFloat($scope.ah.GrossLossAss) - parseFloat($scope.ah.TotalDeductAABNA);
    //    console.log($scope.ah.GrossAdmLoss);
    //}

    $scope.calGrandTotal = function () {
        $scope.ah.GrossAdmLoss = (parseFloat($scope.ah.GrossLossAss) - parseFloat($scope.ah.TotalDeductAABNA)).toFixed(2);
        if ($scope.ah.GrossAdmLoss == 'NaN')
        {
            $scope.ah.GrossAdmLoss = 0;
        }
        $scope.ah.GrandTotal =(parseFloat( $scope.ah.GrossAdmLoss) -parseFloat( $scope.ah.DeductofGFIP) -parseFloat( $scope.ah.SalvageAmount - $scope.ah.DeprAmount)).toFixed(2);
    }
    $scope.commonFuncForBindCal = function () {
        angular.forEach($scope.myArr, function (element, index) {
            if (element.Rate != 0 || element.Rate != '') {
                //alert(element.Amount);

                element.Amount = parseFloat($scope.ah.GrossLossAss) * (element.Rate / 100);

            }
            if (element.Sign == "Add") {
                $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) + parseFloat(element.Amount));
            }
            else if (element.Sign == "Less") {
                $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) - parseFloat(element.Amount));
            }
            else if (element.Sign == "Multiply") {
                $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) * parseFloat(element.Amount));
            }
            else if (element.Sign == "Divide") {
                   $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) / parseFloat(element.Amount));
            }

        })
        $scope.calGrandTotal();
    }
    $scope.calSalvageRateToAmount = function () {
        if ($scope.ah.SalvageRate == 0 || $scope.ah.SalvageRate == '') {

        }
        else {
            $scope.ah.SalvageAmount = (parseFloat($scope.ah.GrossAdmLoss) * (parseFloat($scope.ah.SalvageRate) / 100)).toFixed(2);
        }
        $scope.calGrandTotal();
    }

    $scope.calDepreciationRateToAmount = function () {
        if ($scope.ah.DeprRate == 0 || $scope.ah.DeprRate == '') {

        }
        else {
            $scope.ah.DeprAmount = (parseFloat($scope.ah.GrossAdmLoss) * (parseFloat($scope.ah.DeprRate) / 100)).toFixed(2);
        }
        $scope.calGrandTotal();

    }

    $scope.getAssessmentofLossAhDescCal = function () {
        $scope.getAssessmentDescription();
        $http.get('/MISCFSR/getAssessmentofLossAhDescCal?claimId=' + _claimId + '&psi_Id=' + $scope.pk_PSI_Id).then(function (response) {
            console.log(response);
            $scope.ah = response.data.ah;
            $scope.myArr = response.data.listAC;
            tempArr =angular.copy($scope.myArr);
        }, function (error) {
        })
    }

    var temp = 0;
    $scope.totalvalue = 0;
    $scope.GrossLossAss = 0;
    $scope.calTotalAssLoss = function () {
        tempTotalAssLoss = 0;
        angular.forEach($scope.listAD, function (element, index) {
            //if (element.ALQty != undefined && element.ALRate != undefined) {
            //    if (element.ALQty == '') {
            //        element.ALQty = 0;
            //    }
            //    if (element.ALRate == '') {
            //        element.ALRate = 0;
            //    }
            //    element.ALAmount = (parseFloat(element.ALQty) * parseFloat(element.ALRate));
            //    tempTotalAssLoss += element.ALAmount;



               
            //}
                
                
                element.ALAmount = angular.isUndefined(element.ALQty) || (element.ALQty == '') ? 0 : parseFloat(element.ALQty)
                                *
                                angular.isUndefined(element.ALRate) || (element.ALRate == '') ? 0 : parseFloat(element.ALRate);
                tempTotalAssLoss +=parseFloat(element.ALAmount);

        })

        $scope.ah.TotalAssLoss = angular.copy(tempTotalAssLoss);
        $scope.ah.GrossLossAss = angular.copy($scope.ah.TotalAssLoss);
         $scope.commonFuncForBindCal ();
        $scope.calSalvageRateToAmount();
        $scope.calDepreciationRateToAmount();

    }
    $scope.calTotalInitClaim = function () {
        tempInitClaim = 0;
        angular.forEach($scope.listAD, function (element, index) {
            //if (element.Qty != undefined && element.ICRate != undefined) {
            //    if (element.ICRate == '') {
            //        element.ICRate = 0;
            //    }
            //    element.ICAmount = (parseFloat(element.Qty) * parseFloat(element.ICRate));
            //    tempInitClaim += element.ICAmount;
                
            //}
            element.ICAmount = angular.isUndefined(element.Qty) || (element.Qty == '') ? 0 : parseFloat(element.Qty)
                                *
                                angular.isUndefined(element.ICRate) || (element.ICRate == '') ? 0 : parseFloat(element.ICRate);
                tempInitClaim +=parseFloat(element.ICAmount);

        })
        $scope.ah.TotalInitClaim = angular.copy(tempInitClaim);
    }
    $scope.calTotalFinalClaim = function () {
        tempFinalClaim = 0;
        angular.forEach($scope.listAD, function (element, index) {
            //if (element.FCQty != undefined && element.FCRate != undefined) {
            //    if (element.FCQty == '') {
            //        element.FCQty = 0;
            //    }
            //    if (element.FCRate == '') {
            //        element.FCRate = 0;
            //    }
            //    element.FCAmount = (parseFloat(element.FCQty) * parseFloat(element.FCRate));
            //    tempFinalClaim += element.FCAmount;
            //    //tempFinalClaim += (parseFloat(element.FCQty) * parseFloat(element.FCRate));
            //}
            element.FCAmount = angular.isUndefined(element.FCQty) || (element.FCQty == '') ? 0 : parseFloat(element.FCQty)
                                *
                                angular.isUndefined(element.FCRate) || (element.FCRate == '') ? 0 : parseFloat(element.FCRate);
                tempFinalClaim +=parseFloat(element.FCAmount);
        })

        $scope.ah.TotalFinalClaim = angular.copy(tempFinalClaim);
    }
    $scope.getDescriptionofAL = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getDescriptionofAL').then(function (response) {
            console.log(response);
            $scope.listDAL = angular.copy(response.data.listDAL);

        }, function (error) {
        })
    }

    //$scope.calTotalGrossLossAss = function () {
    //    if ($scope.ac.Sign == "Add") {
    //        $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) + parseFloat($scope.ac.Amount));
    //    }
    //    else if ($scope.ac.Sign == "Less") {
    //        $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) - parseFloat($scope.ac.Amount));
    //    }
    //    else if ($scope.ac.Sign == "Multiply") {
    //        $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) * parseFloat($scope.ac.Amount));
    //    }
    //    else if ($scope.ac.Sign == "Divide") {
    //        $scope.ah.GrossLossAss = (parseFloat($scope.ah.GrossLossAss) / parseFloat($scope.ac.Amount));
    //    }
    //}
    
    $scope.myArr = [];
    var tempArr = [];
    $scope.AddRecord = function () {
        //$scope.tempReadonly = true;
        //$scope.calTotalGrossLossAss();
        //var x = $('#formALP');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    console.log('false');
        //    return false;
        //}
        var tempData = {};
        tempData.Sign = $scope.ac.Sign;
        tempData.DescAL_ID = $scope.ac.DescAL_ID;
        tempData.Rate = $scope.ac.Rate;
        tempData.Amount = $scope.ac.Amount;
        tempData.UOM_Id = $scope.ac.UOM_Id;
        tempArr.push(tempData);
        $scope.myArr =angular.copy(tempArr);
        // $scope.commonFuncForBindCal ();
        // $scope.commonFuncForBindCal ();
        $scope.calTotalAssLoss();


        //$scope.myArr.push(tempData);
        $scope.ac.Sign = ' ';
        $scope.ac.DescAL_ID = 0;
        $scope.ac.Rate = 0;
        $scope.ac.Amount = 0;
        $scope.ac.UOM_Id = 0;
        console.log($scope.myArr);
    }
    //$scope.deleteInmyArr = function (index) {
    //    delete $scope.myArr[index];
    //    $scope.myArr.shift();
    //}
    $scope.deleteInmyArr = function (index) {
        //$scope.myArr.splice(index, 1);
        tempArr.splice(index, 1);
        $scope.myArr = [];
        $scope.myArr =angular.copy(tempArr);
        $scope.calTotalAssLoss();
        // $scope.commonFuncForBindCal ();
    }
    $scope.calTotalDeductAABNA = function () {
        tempAABNA = 0;
        angular.forEach($scope.listAD, function (element, index) {
            if (element.DeductAABNA != undefined) {
                if (element.DeductAABNA == '') {
                    element.DeductAABNA = 0;
                }
                tempAABNA += (parseFloat(element.DeductAABNA));
            }

        })
        $scope.ah.TotalDeductAABNA = angular.copy(tempAABNA);
        $scope.calTotalAssLoss();
    }







    $scope.getUOM = function () {
        $http.get('/MISCFSR/getUOM').then(function (response) {
            console.log(response);
            $scope.listUOM = angular.copy(response.data.listUOM);

        }, function (error) {
        })
    }

    $scope.calRateToAmount = function () {
        if ($scope.ac.Rate != undefined) {
            $scope.ac.Amount = (parseFloat($scope.ah.GrossLossAss) * parseFloat($scope.ac.Rate) / 100).toFixed(2);
            if ($scope.ac.Amount == 'NaN') {
                $scope.ac.Amount = 0;
            }
        }
       }
  
    $scope.$watchGroup(['ah.GrossLossAss', 'ah.TotalDeductAABNA'], function () {
        $scope.calGrossAdmLoss();
    })
    $scope.$watchGroup(['ah.SalvageAmount', 'ah.DeprAmount'], function () {
        $scope.calGrandTotal();
    })

    $scope.saveAssessmentofLossAhDescCal = function () {
        if ($scope.test == 0)
        {
            alert('Please choose any one');
            return false;
        }
        //var x = $('#formALP');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        var ah_copy = {};
        var listAD_copy;
        var myArr_copy;
        $scope.ah.ClaimID = angular.copy(_claimId);
        $scope.ah.PSI_ID = $scope.pk_PSI_Id;
        ah_copy = angular.copy($scope.ah);
        listAD_copy = angular.copy($scope.listAD);
        myArr_copy = angular.copy($scope.myArr);
        $http.post('/MISCFSR/saveAssessmentofLossAhDescCal', { ah: ah_copy, listAD: listAD_copy, listAC: myArr_copy }).then(function (response) {
            console.log(response);
            //if (rec == 'n') {
            //    window.location = "/MISCFSR/ClaimAssAdjust?cid=" + _claimId + "";
            //}
            //else {
                showSuccessToast(response.data);
            //}
 
        }, function (error) {
        })
    }


    $scope.getAssessmentDescription = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getAssessmentDescription?claimId=' + _claimId + '&psi_Id=' + $scope.pk_PSI_Id + '').then(function (response) {
            console.log(response);
            $scope.listAD = angular.copy(response.data.listAD);

        }, function (error) {
        })
    }

    //$scope.getPolicySumInsured_AND_ShortName = function () {

    //    $http.get('/MISCFSR/getPolicySumInsured_AND_ShortName?claimId=' + _claimId + '').then(function (response) {
    //        console.log(response);
    //        $scope.listPSI = angular.copy(response.data.listPSI);

    //    }, function (error) {
    //    })

    //}
    $scope.setData = function (psi_Id, shortName) {
        $scope.pk_PSI_Id = psi_Id;
        $scope.shortName = shortName;

    }
    $scope.getDistinct_PSI_ID_byClaimID = function () {
        $http.get('/MISCFSR/getDistinct_PSI_ID_byClaimID?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listPSI = response.data.listPSI;

        }, function (error) {
        })
    }
    $scope.getDistinct_PSI_ID_byClaimID();

    $scope.getDistinctTableName_byClaimID();
    $scope.getUOM();
    $scope.getDescriptionofAL()
    $scope.getAssessmentDescription();
    $scope.getAssessmentofLossAhDescCal();
    $scope.getAssessmentDescription();
})