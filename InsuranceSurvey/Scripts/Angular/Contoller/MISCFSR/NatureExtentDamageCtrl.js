﻿angular.module('myApp').controller('NatureExtentDamageCtrl', function ($scope, $http) {
    $scope.pm = {};
    $scope.s = {};
    $scope.b = {};
    $scope.o = {};
    $scope.Description;
    $scope.SumInsured;
    $scope.Others;
    $scope.choose;
    $scope.test = 0;
    $scope.pk_PSI_Id = 0;
    var comn_copy = {};
    var _claimId = $('#cid').val();

    $scope.saveNatureExtentDamage = function () {
        if ( $scope.pk_PSI_Id==0) {
            alert("Please Choose any Field");
            return false;
        }
        if ($scope.choose == "PM" || $scope.choose == "Eq" || $scope.choose == "El" || $scope.choose == "C" || $scope.choose == '') {
            alert($scope.choose);
            $scope.pm.ClaimID = angular.copy(_claimId);
            comn_copy = angular.copy($scope.pm);
            console.log("PM");
            console.log(comn_copy);
        }
        else if ($scope.choose == "S") {
            $scope.s.ClaimID = angular.copy(_claimId);
            comn_copy = angular.copy($scope.s);
            console.log("S");
            console.log(comn_copy);
        }
        else if ($scope.choose == "B" || $scope.choose == "F") {
            $scope.b.ClaimID = angular.copy(_claimId);
            $scope.b.TableName = angular.copy($scope.choose);
            comn_copy = angular.copy($scope.b);
            console.log("B");
            console.log(comn_copy);
        }
        comn_copy.PSI_ID = angular.copy($scope.pk_PSI_Id);
        console.log(comn_copy);
        $http.post('/MISCFSR/saveNatureExtentDamage', comn_copy).then(function (response) {
            console.log(response);
            $scope.cancel();
            showSuccessToast(response.data);
            $scope.getNatureExtentDamage();
        }, function (error) {
        })
    }


    $scope.getNatureExtentDamage = function () {

        console.log(_claimId);
        $scope.listPM = [];
        $scope.listS = [];
        $scope.listB = [];
        $scope.listO = [];
        $http.get('/MISCFSR/getNatureExtentDamage?claimId=' + _claimId + '&psi_Id=' + $scope.pk_PSI_Id + '').then(function (response) {
            console.log(response);
            var listNED = angular.copy(response.data.listNED);
            if ($scope.choose == "PM" || $scope.choose == "Eq" || $scope.choose == "El" || $scope.choose == "C" || $scope.choose == '') {
                $scope.listPM = angular.copy(listNED);
            }
            else if ($scope.choose == "S") {
                $scope.listS = angular.copy(listNED);
            }
            else if ($scope.choose == "B" || $scope.choose == "F") {
                $scope.listB = angular.copy(listNED);
            }
            //else if ($scope.choose == "Others") {
            //    $scope.listO = angular.copy(listNED);

            //}
            //$scope.getSumInsured_And_DescofProp(pk_Id);

        }, function (error) {
        })
    }


    $scope.rowClickPM = function (p) {
        $scope.pm = angular.copy(p);
        $scope.getSumInsured_byDescofProp($scope.pm.PSI_ID);
    }
    $scope.rowClickS = function (s) {
        $scope.s = angular.copy(s);
        $scope.getSumInsured_byDescofProp($scope.s.PSI_ID);
    }
    $scope.rowClickB = function (b) {
        $scope.b = angular.copy(b);
        $scope.getSumInsured_byDescofProp($scope.b.PSI_ID);
    }
    $scope.rowClickO = function (o) {
        $scope.o = angular.copy(o);
    }

    //$scope.getSumInsured_And_DescofProp = function (pk_Id) {

    //    $http.get('/MISCFSR/getSumInsured_And_DescofProp?PK_PSI_ID='+pk_Id+'&claimId=' + _claimId + '').then(function (response) {
    //        console.log(response);
    //        $scope.Description = response.data.Description;
    //        $scope.SumInsured = response.data.SumInsured;
    //    })

    //}
    //$scope.getDesc_And_ShortName_byClaimID = function () {

    //    $http.get('/MISCFSR/getDesc_And_ShortName_byClaimID?claimId=' + _claimId + '').then(function (response) {
    //        console.log(response);
    //        $scope.listDS = angular.copy(response.data.listDS);

    //    })

    //}
    //$scope.getDescription_SumIns_byClaimID = function () {

    //    $http.get('/MISCFSR/getDescription_SumIns_byClaimID?claimId=' + _claimId + '').then(function (response) {
    //        console.log(response);
    //        $scope.listDS = angular.copy(response.data.listDS);

    //    })

    //}


    //    }, function (error) {
    //    })
    //}
    $scope.cancel = function () {
        if ($scope.choose == "PM" || $scope.choose == "Eq" || $scope.choose == "El" || $scope.choose == "C" || $scope.choose == '' ) {
            $scope.pm = {};
        }
        else if ($scope.choose == "S") {
            $scope.s = {};
        }
        else if ($scope.choose == "B" || $scope.choose == "F") {
            $scope.b = {};
        }
   }

    //$scope.cancelPlantMachinery = function () {
    //    $scope.pm = {};
    //}
    //$scope.cancelStock = function () {
    //    $scope.s = {};
    //}
    //$scope.cancelBuilding = function () {
    //    $scope.b = {};
    //}


    $scope.getPolicySumInsured_AND_ShortName = function () {

        $http.get('/MISCFSR/getPolicySumInsured_AND_ShortName?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listPSI = angular.copy(response.data.listPSI);

        }, function (error) {
        })

    }
    $scope.setData = function (psi_Id, shortName, desc, sumInsured) {
        console.log(psi_Id, shortName, desc, sumInsured);

        $scope.pk_PSI_Id = psi_Id;
        $scope.choose = shortName;
        $scope.Description = desc;
        $scope.SumInsured = sumInsured;

    }


    $scope.getPolicySumInsured_AND_ShortName();

})