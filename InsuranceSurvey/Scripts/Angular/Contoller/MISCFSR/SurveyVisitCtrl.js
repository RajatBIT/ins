﻿angular.module('myApp').controller('SurveyVisitCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.sv = {};
    var _cLaimId = $('#cid').val();
    var tableName = $('#fromTable').val();
    $scope.claimId = angular.copy(_cLaimId);
    var temp = 0;
    $scope.saveSurveyVisit = function () {
       
        var sv_copy = {};
        $scope.sv.FkClaimId = angular.copy(_cLaimId);
        $scope.sv.FromTableName = angular.copy(tableName);
        sv_copy = angular.copy($scope.sv);
        if (temp == 0) {
            sv_copy.StartDate = $filter('date')(CommonService.convertDateStringToDateObj(sv_copy.StartDate), 'dd-MMM-yyyy');
            sv_copy.EndDate = $filter('date')(CommonService.convertDateStringToDateObj(sv_copy.EndDate), 'dd-MMM-yyyy');
        }
        $http.post('/MISCFSR/saveSurveyVisit', sv_copy).then(function (response) {
            console.log(response);
            $scope.cancel();
            $scope.getSurveyVisit();
            showSuccessToast(response.data);
           
        }, function (error) {
        })
    }
    $scope.getSurveyVisit = function () {

        $http.get('/MISCFSR/getSurveyVisit?claimId=' + _cLaimId + '&tableName='+tableName+' ').then(function (response) {
            $scope.listSV = angular.copy(response.data.listSV);
            //$scope.sv = angular.copy(listsv);
        },function(error){
        })
    }
    $scope.rowClick = function (d) {
        $scope.sv = angular.copy(d);
        temp = 1;
    }
    $scope.rowClickDelete = function (pkId) {
        $http.get('/MISCFSR/deleteSurveyVisit?pkId=' + pkId + ' ').then(function (response) {
            console.log(response);
            $scope.getSurveyVisit();
            //$scope.sv = angular.copy(listsv);
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        $scope.sv = {};
        temp = 0;
    }
    $scope.getSurveyVisit();
})