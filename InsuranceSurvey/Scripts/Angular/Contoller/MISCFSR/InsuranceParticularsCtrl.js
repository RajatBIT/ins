﻿angular.module('myApp').controller('InsuranceParticularsCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.ip = {};
    $scope.ic = {};
    $scope.cd = {};
    $scope.x = {};
    $scope.y = {};
    $scope.z = {};
    $scope.cIns = 0;
    $scope.psi = 0;

    $scope.rwc = 0;
    $scope.deleteDiv = 0;
    var _claimId = $('#cid').val();
    $scope.other = {};
    $scope.other.Description = '';
    $scope.other.SumInsured = 0;
    $scope.getMISCCommonDataGet = function () {

        $http.get('/MISCFSR/getMISCCommonDataGet?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            var listCmnData = angular.copy(response.data);
            $scope.cd = angular.copy(listCmnData);


        }, function (error) {
        })
    }
    $scope.getIntroClaim = function () {

        $http.get('/MISCFSR/getIntroClaim?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            var listIC = response.data.listIC;
            $scope.ic = angular.copy(listIC);
            $scope.ic.PK_MISCFSRIPRID = angular.copy($scope.cd.PK_MISCFSRIPRID);
            $scope.ic.PeriodofPolicyFromDT = angular.copy($scope.cd.PeriodofPolicyFromDT);
            $scope.ic.PeriodofPolicyToDT = angular.copy($scope.cd.PeriodofPolicyToDT);

        }, function (error) {
        })
    }
    $scope.saveIntroClaim = function () {
        var ic_copy = {};
        console.log($scope.ic.IntimationLossDT);
        $scope.ic.ClaimID = angular.copy(_claimId);
        ic_copy = angular.copy($scope.ic);
        $http.post('/MISCFSR/saveIntroClaim', ic_copy).then(function (response) {
            console.log(response);
            $scope.getMISCCommonDataGet();
            $scope.getIntroClaim();
        }, function (error) {
        })
    }

    $scope.saveInsuranceParticulars = function (rec) {
        $scope.saveIntroClaim();
        var x = $('#formip');
        var status = $(x).ValidationFunction();
        if (status == false) {
            console.log('false');
            return false;
        }
   
        if ($scope.ip.CINYN == "Yes") {
            if (!$scope.checkIsHundred()) {
                return false;
            }
        }

        var ip_copy = {};
        $scope.ip.ClaimID = angular.copy(_claimId);
        ip_copy = angular.copy($scope.ip);
        console.log(ip_copy);
        
        $http.post('/MISCFSR/saveInsuranceParticulars', { ip: ip_copy, listCIn: $scope.coInsurers, listPSI: $scope.listPT, listRWC: $scope.relWC }).then(function (response) {
            console.log(response);
            $scope.getInsuranceParticulars();
            $scope.getPolicySumInsuredAndTableName();
            if (rec == 'n') {
                window.location = "/MISCFSR/SurveyParticulars?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }
    $scope.getInsuranceParticulars = function () {

        $http.get('/MISCFSR/getInsuranceParticulars?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
           
            $scope.ip = angular.copy(response.data.listIP);
           
      
        }, function (error) {
        })
    }
    $scope.showModalCoins = function () {
        $scope.psi = 0;
        $scope.rwc = 0;
        $scope.deleteDiv = 0;
        //$('#modalBody').html($('#cIns'));
        $('#myModal').modal('show');
        $scope.cIns = 1;
    }

    $scope.getInsurers = function () {

        $http.get('/MISCFSR/getInsurers').then(function (response) {
            $scope.listIns = angular.copy(response.data.listIns);

        }, function (error) {
        })
    }


    $scope.cancel = function () {
        var id = angular.copy($scope.ip.PK_MISCFSRIPRID);
        $scope.ip = {};
        $scope.ip.PK_MISCFSRIPRID = angular.copy(id);
        $scope.ic.PK_MISCFSRIPRID = angular.copy(id);
    }

    //co -ins start
    var a = 0;
    $scope.x.CoInsurersID = 0;
    $scope.x.Percentage = 0;
    $scope.coInsurers = [];
    var c = 0;
    var tempArr = [];
    $scope.addItem = function () {
        if ($scope.x.CoInsurersID == 0) {
            alert("Please choose any one");
            return false;


        }
        if ($scope.x.Percentage > 100) {
            $scope.x.Percentage = 0;
            alert("Please  enter value below 100");
            return false;
        }
        if ($scope.x.Percentage <= 0) {
            $scope.x.Percentage = 0;
            alert("Please  enter value 1 or more");
            return false;
        }
        c = 0;
        c = $scope.totalCoIns();
        c = parseInt(c) + parseInt($scope.x.Percentage);
        if (c == 100) {
            $scope.btnAdd = 1;
            $scope.trCoIns = 1;
        }
        if (c > 100) {
            $scope.x.Percentage = 0;
            alert("Please Enter value below 100");
            return false;
        }
        //if (c == 100) {
        //    $scope.btnAdd = 1;
        //    $scope.trCoIns = 1;
        //}
        var temp = {};
        temp.Percentage = $scope.x.Percentage;
        temp.CoInsurersID = $scope.x.CoInsurersID;
        //tempArr.push(temp);
        $scope.coInsurers.push(temp);
        $scope.x.Percentage = 0;
        $scope.x.CoInsurersID = " ";
        c = 0;
    }
    $scope.checkIsHundred = function () {
        if ($scope.trCoIns == 1) {
            $('#myModal').modal('hide');
            return true;
        }
        else if ($scope.trCoIns == 0) {

            $('#myModal').modal('show');
            $scope.cIns = 1;
            alert("Please fill percentage till 100");

        }
    }

    $scope.totalCoIns = function () {
        a = 0;
        angular.forEach($scope.coInsurers, function (e, i) {
            console.log(e.Percentage);
            a += parseInt(e.Percentage);

        })
        console.log(a);
        if (a == 100) {
            $scope.trCoIns = 1;
        }
        return a;

    }
    $scope.keyUpCins = function () {
        var check = 0;
        check = $scope.totalCoIns();
        //console.log("check"+check);
        if (check > 100) {
            this.x.Percentage = 0;
        }
    }
    $scope.clearCoIns = function () {
        $scope.coInsurers = [];
        //$scope.x.Percentage=0;
        //$scope.x.CoInsurersID=0;
    }

    //co-ins end

    //co-policy sum ins  start
    $scope.showModalPSI = function () {
        $scope.cIns = 0;
        $scope.rwc = 0;
        $scope.deleteDiv = 0;
        //$('#modalBody').html($('#psi'));
        $('#myModal').modal('show');
        $scope.psi = 1;
    }
    var aa = 0;

    $scope.polSumIns = [];
    $scope.addPSI = function () {
        if ($scope.y.SumInsured <= 0) {
            $scope.y.SumInsured = 0;
            alert("Please  enter value 1 or more");
            return false;
        }
        var tempPSI = {};
        tempPSI.SumInsured = $scope.y.SumInsured;
        tempPSI.DescofProp = $scope.y.DescofProp;
        $scope.polSumIns.push(tempPSI);
        $scope.y.SumInsured = 0;
        $scope.y.DescofProp = " ";
        $scope.totalSumInsured();
        cc = 0;
    }
    $scope.deleteInPSI = function (index) {
        $scope.polSumIns.splice(index, 1);
        $scope.totalSumInsured();
    }


    $scope.clearPSI = function () {
        $scope.y.SumInsured = 0;
        $scope.y.DescofProp = " ";
        $scope.polSumIns = [];
    }
    $scope.totalSI = 0;
    $scope.totalSumInsured = function () {
        $scope.totalSI = 0;
        angular.forEach($scope.polSumIns, function (element, index) {
            $scope.totalSI += parseFloat(element.SumInsured);
        })
    }

    //policy sum ins end

    // rel w c  start
    $scope.showModalRWC = function () {
        //$('#modalBody').html($('#rwc'));
        $scope.cIns = 0;
        $scope.psi = 0;
        $scope.deleteDiv = 0;
        $('#myModal').modal('show');
        $scope.rwc = 1;
    }


    $scope.relWC = [];
    $scope.addRWC = function () {
        var tempRWC = {};
        tempRWC.RelWarCond = $scope.z.RelWarCond;
        $scope.relWC.push(tempRWC);
        $scope.z.RelWarCond = " ";
    }
    $scope.deleteInRWC = function (index) {
        $scope.relWC.splice(index, 1);
    }

    $scope.clearRWC = function () {

        $scope.z.RelWarCond = " ";
        $scope.relWC = [];
    }

    //policy sum ins end

    $scope.showIns = function () {
        console.log($scope.coInsurers);
    }


    $scope.getCoInsurers = function () {

        $http.get('/MISCFSR/getCoInsurers?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.coInsurers = angular.copy(response.data.listCIn);
            $scope.totalCoIns();
            //tempArr = angular.copy($scope.coInsurers);

        }, function (error) {
        })
    }
    $scope.getPolicySumInsured = function () {

        $http.get('/MISCFSR/getPolicySumInsured?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.polSumIns = angular.copy(response.data.listPSI);
            $scope.totalSumInsured();

        }, function (error) {
        })
    }
    $scope.getRelWarCond = function () {

        $http.get('/MISCFSR/getRelWarCond?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.relWC = angular.copy(response.data.listRWC);

        }, function (error) {
        })
    }
   

    $scope.setCoInsYes = function () {
        $('#myModal').modal('hide');
        $scope.ip.CINYN = "No";
    }
    $scope.setCoInsNo = function () {
        $('#myModal').modal('hide');
        $scope.ip.CINYN = "Yes";
    }
    $scope.deleteInCoInssurers = function (index) {

        $scope.coInsurers.splice(index, 1);
        //$scope.coInsurers = [];
        //$scope.coInsurers = angular.copy(tempArr);
        var checkValue = $scope.totalCoIns();
        if (checkValue < 100) {
            $scope.trCoIns = 0;
            $scope.btnAdd = 0;
        }
    }
    $scope.getPolicySumInsuredAndTableName = function () {

        $http.get('/MISCFSR/getPolicySumInsuredAndTableName?claimId='+_claimId).then(function (response) {
            console.log(response);
            $scope.listPT = angular.copy(response.data.listPT);
            
        }, function (error) {
        })
    }
    $scope.addInlistPT = function () {
        if ($scope.other.Description =='')
        {
            alert("Please Enter Desription");
            return false;
        }
        var aa = 0;
        angular.forEach($scope.listPT, function (element, index) {
            debugger;
            if(element.Description.toLowerCase()==$scope.other.Description.toLowerCase())
            {
                aa++;
                return false;
            }
         
        })
        if (aa == 0) {
            $scope.listPT.push($scope.other);
            $scope.other = {};
        }
        else if(aa>0) {
            alert("Description is already Exists. Please Enter Unique Description");
            $scope.other = {};
            
        }
 
    }


    $scope.getInsuranceParticulars();
    $scope.getMISCCommonDataGet();

    $scope.getInsurers();
    $scope.getIntroClaim();
    $scope.getCoInsurers();

    $scope.getPolicySumInsured();
    $scope.getRelWarCond();
    $scope.getPolicySumInsuredAndTableName();
})
