﻿angular.module('myApp').controller('InsuredConsentCtrl', function ($scope, $http) {
    $scope.ic = {};

    var _claimId = $('#cid').val();
    $scope.saveInsuredConsent = function (rec) {
        //var x = $('#formSP');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        $scope.ic.ClaimID = angular.copy(_claimId);
        var ic_copy = {};
        ic_copy = angular.copy($scope.ic);
        $http.post('/MISCFSR/saveInsuredConsent', {ic: ic_copy }).then(function (response) {
            console.log(response);
            var sc = ' ';
            if (window.FormData !== undefined) {
                var fileData = new FormData();
                if ($("#snapShotofCons").val() != '') {
                    
                    fileData.append($("#snapShotofCons").get(0).files[0].name, $("#snapShotofCons").get(0).files[0]);
                    sc = "yes";
                    
                    
                }
            
                $.ajax({
                    url: '/MISCFSR/saveInsuredConsentPhotoDoc?sc=' + sc +'&claimId=' + _claimId + ' ',
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fileData,
                    success: function (result) {
                        //alert(result);
                    },
                    error: function (err) {
                        //alert(err.statusText);
                    }
                });

            }

            $scope.getInsuredConsent();


            if (rec == 'n') {
                window.location = "/MISCFSR/RecommendationsLossRemarks?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })





    }

    $scope.getInsuredConsent = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getInsuredConsent?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.ic = angular.copy(response.data.ic);

        }, function (error) {
        })

    }
    $scope.getModeofCommunication = function () {
        console.log(_claimId);
        $http.get('/MISCFSR/getModeofCommunication?claimId=' + _claimId + ' ').then(function (response) {
            console.log(response);
            $scope.listMC = angular.copy(response.data.listMC);

        }, function (error) {
        })

    }

    $scope.cancel = function () {
        var id = angular.copy($scope.ic.PK_IC_ID);
        $scope.ic = {};
        $scope.ic_copy = {};
        $scope.ic.PK_IC_ID = angular.copy(id);
    }
    $scope.getModeofCommunication();
    $scope.getInsuredConsent();


})