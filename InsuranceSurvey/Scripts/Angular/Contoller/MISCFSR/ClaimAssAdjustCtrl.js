﻿angular.module("myApp").controller('ClaimAssAdjustCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.c = {};
    $scope.getClaimAssAdjust = function () {
        $http.get('/MISCFSR/getClaimAssAdjust?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.c = response.data.c;
        }, function (error) {
        })
        
    }
    $scope.saveClaimAssAdjust = function (rec) {
        var c_copy = {};
        $scope.c.ClaimID = _claimId;
        var cs = {};
        var listCS = [];

        $('#mygridBody input').each(function () {
            cs.Value = $(this).val();
            cs.TableName = $(this).attr('data-tname');
            cs.ShortName = $(this).attr('data-shortname');
            listCS.push(cs);
            cs = {};
                    //console.log($(this).val() + '-' + $(this).attr('data-tname') + '-' + $(this).attr('data-shortname'));

        })
        console.log(listCS);
        c_copy=angular.copy($scope.c);
        $http.post('/MISCFSR/saveClaimAssAdjust', { c: c_copy,listCS:listCS}).then(function (response) {
            console.log(response);
            $scope.getClaimAssAdjust();
            if (rec == 'n') {
                window.location = "/MISCFSR/Contribution?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
            
          }, function (error) {
        })
        
    }
    $scope.cancel = function () {
        //$("input[type=text]").val("");
        $("input[type=text]").val(0);
        $("textarea").val("");
    }
    //$scope.test = function () {
    //    alert();
    //    $('#mygridBody input').each(function () {

    //        console.log($(this).val() + '-' + $(this).attr('data-tname') + '-' + $(this).attr('data-shortname'));

    //    })
    //}
    $scope.getClaimAssAdjust();
})