﻿angular.module('myApp').controller('SurveyParticularsCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.sp = {};
    $scope.gc = {};
    var _claimId = $('#cid').val();
    var tableName = "AddVisit";
    $scope.saveSurveyParticulars = function (rec) {
        var x = $('#formSP');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        var sp_copy = {};
        $scope.sp.ClaimID = angular.copy(_claimId);
        sp_copy = angular.copy($scope.sp);
        $http.post('/MISCFSR/saveSurveyParticulars', sp_copy).then(function (response) {
            console.log(response);
            $scope.getSurveyParticulars();

            if (rec == 'n') {
                window.location = "/MISCFSR/SurveyVisit?cid=" + _claimId + "&fromTable="+tableName;
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }

    $scope.getInstructorNameInstructionDateByClaimID = function () {

        $http.get('/MISCFSR/getInstructorNameInstructionDateByClaimID?claimId=' + _claimId + ' ').then(function (response) {
            var listGC = angular.copy(response.data.gc);
            if (listGC.InstructionDT != null) {
                listGC.InstructionDT = $filter('date')(listGC.InstructionDT.slice(6, -2), 'dd-MM-yyyy');
            }
            $scope.gc = angular.copy(listGC);
        }, function (error) {
        })

    }
    $scope.getSurveyParticulars = function () {

        $http.get('/MISCFSR/getSurveyParticulars?claimID=' + _claimId + ' ').then(function (response) {
            var listSP = angular.copy(response.data.listSP);
            console.log(listSP);
                $scope.sp = angular.copy(listSP);
           
        }, function (error) {
        })

    }
    $scope.cancel = function () {
        var id = angular.copy($scope.sp.PK_MISCFSRSURPRTID);
        var srcIns = angular.copy($scope.sp.SourceInsInstructorSurvey);
        $scope.sp = {};
        $scope.sp.PK_MISCFSRSURPRTID = angular.copy(id);
        $scope.sp.SourceInsInstructorSurvey = angular.copy(srcIns);
    }
    $scope.getIntroClaim = function () {

        $http.get('/MISCFSR/getIntroClaim?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            var listIC = angular.copy(response.data);
            $scope.ic = angular.copy(listIC);
             }, function (error) {
        })
    }
    $scope.getIntroClaim();
    $scope.getSurveyParticulars();
    $scope.getInstructorNameInstructionDateByClaimID();
    
})