﻿angular.module('myApp').controller('LiabilityUnderPolicyCtrl', function ($scope, $http) {
    $scope.lup = {};
    $scope.divWC = 0;
    var _claimId = $('#cid').val();
    //var _cLaimId = 25;
    
    $scope.saveLiabilityUnderPolicy = function (rec) {
        var x = $('#formLUP');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        var lup_copy = {};
        $scope.lup.ClaimID = _claimId;
        lup_copy = angular.copy($scope.lup);
        var myArr=[];
        myArr.push($scope.listWC);
        console.log($scope.listWC);
        $http.post('/MISCFSR/saveLiabilityUnderPolicy', { lup: lup_copy, listWC: $scope.listWC }).then(function (response) {
            console.log(response);
            $scope.getLiabilityUnderPolicy();

            if (rec == 'n') {
                window.location = "/MISCFSR/NatureExtentDamage?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }

    $scope.getLiabilityUnderPolicy = function () {

        $http.get('/MISCFSR/getLiabilityUnderPolicy?claimid=' + _claimId + ' ').then(function (response) {
            $scope.lup = angular.copy(response.data.lup);
        }, function (error) {
        })

    }
    $scope.clearWC = function () {
        $scope.listWC = [];
    }
    $scope.cancel = function () {
        var id = angular.copy($scope.lup.PK_MISCLUPID);
        $scope.lup = {};
        $scope.lup.PK_MISCLUPID = angular.copy(id);
    }
    $scope.getRelWarCond = function () {

        $http.get('/MISCFSR/getRelWarCond?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listRelWC = angular.copy(response.data.listRWC);

        }, function (error) {
        })
    }
    $scope.showWC = function () {
        $scope.divWC = 1;
        $('#myModal').modal('show');
       

    }

    var wc = [];
    $scope.addWC = function () {
        angular.forEach($scope.listRelWC, function (element, index) {

        })
    }

    $scope.getWarCond = function () {

        $http.get('/MISCFSR/getWarCond?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listWC = angular.copy(response.data.listWC);

        }, function (error) {
        })
    }

    $scope.getOperatedPeril = function () {

        $http.get('/MISCFSR/getOperatedPeril').then(function (response) {
            console.log(response);
            $scope.listOP = angular.copy(response.data.listOP);

        }, function (error) {
        })
    }

    $scope.getWarCond();
    $scope.getRelWarCond();
    $scope.getOperatedPeril();
    $scope.getLiabilityUnderPolicy();
})