﻿angular.module('myApp').controller('IntroClaimCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.ic = {};
    $scope.cd = {};
    $scope.tableName = $('#tableName').val();
    console.log($scope.val);
    var _cLaimId = $('#cid').val();
    $scope.saveIntroClaim = function (rec, p) {
        var ic_copy = {};
        $scope.ic.ClaimID = angular.copy(_cLaimId);
        ic_copy = angular.copy($scope.ic);
        console.log(ic_copy);
        $http.post('/MISCFSR/saveIntroClaim', {ic: ic_copy }).then(function (response) {
            console.log(response);
            //showSuccessToast(response.data);
            $scope.getMISCCommonDataGet();
            $scope.getIntroClaim();
            if (rec == 'n') {
                if (p == 'i') {
                    window.location = "/MISCFSR/InsuranceParticulars?cid=" + _cLaimId + "";
                }
                else {
                    window.location = "/MISCISVR/NatureExtentDamage?cid=" + _cLaimId + "";
                }
            }
            else {
                showSuccessToast(response.data);
                $scope.getMISCCommonDataGet();
                $scope.getIntroClaim();
            }
         }, function (error) {
        })
    }

    $scope.getCL_OP = function () {

        $http.get('/MISCFSR/getCL_OP').then(function (response) {
            $scope.listCL_OP = angular.copy(response.data.listCL_OP);
        }, function (error) {
        })

    }
    $scope.getCmtLiabSP = function () {

        $http.get('/MISCFSR/getCmtLiabSP').then(function (response) {
            console.log(response);
            $scope.listCmtLiabSP = angular.copy(response.data.listCmtLiabSP);
        }, function (error) {
        })
    }
    $scope.getIntroClaim = function () {

        $http.get('/MISCFSR/getIntroClaim?claimId=' + _cLaimId + '').then(function (response) {
            console.log(response);
            var listIC = angular.copy(response.data.listIC);
                $scope.ic = angular.copy(listIC);
                $scope.ic.PK_MISCFSRIPRID = angular.copy($scope.cd.PK_MISCFSRIPRID);
                $scope.ic.PeriodofPolicyFromDT = angular.copy($scope.cd.PeriodofPolicyFromDT);
                $scope.ic.PeriodofPolicyToDT =angular.copy( $scope.cd.PeriodofPolicyToDT);
        
        }, function (error) {
        })
    }
    $scope.getMISCCommonDataGet = function () {

        $http.get('/MISCFSR/getMISCCommonDataGet?claimId=' + _cLaimId + '').then(function (response) {
            console.log(response);
            var listCmnData = angular.copy(response.data);
                //fromDate = angular.copy(listCmnData.PeriodofPolicyFromDT);
        
                //toDate = angular.copy(listCmnData.PeriodofPolicyToDT);
       
            $scope.cd = angular.copy(listCmnData);
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        var introPkId = angular.copy($scope.ic.PK_MISCFSRIntroID);
        var insParId = angular.copy($scope.cd.PK_MISCFSRIPRID);
        $scope.ic = {};
        $scope.ic.PK_MISCFSRIntroID = angular.copy(introPkId);
        $scope.ic.PK_MISCFSRIPRID = angular.copy(insParId);
        
    }

    $scope.getMISCCommonDataGet();
    $scope.getIntroClaim();
    $scope.getCmtLiabSP();
    $scope.getCL_OP();

})