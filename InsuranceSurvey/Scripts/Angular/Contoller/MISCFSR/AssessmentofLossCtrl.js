﻿angular.module("myApp").controller('AssessmentofLossCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.listTableName;
    $scope.al = {};
    $scope.d = {};
    $scope.shortName;
    $scope.pk_PSI_Id = 0;
    $scope.test = 0;
    $scope.getDistinctTableName_byClaimID = function () {
        $http.get('/MISCFSR/getDistinctTableName_byClaimID?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listTableName = response.data.listTableName;
        }, function (error) {
        })
    }
    $scope.getAssessmentofLoss = function () {
        $scope.cancelAL();
        $scope.cancelDOP();
        $http.get('/MISCFSR/getAssessmentofLoss?claimId=' + _claimId + '&psi_Id='+$scope.pk_PSI_Id+'').then(function (response) {
            console.log(response);
            $scope.listAL = response.data.listAL;
        }, function (error) {
        })
        
    }
    $scope.showModalDOP = function () {
        $scope.getDescofOriginProcurement();
         $('#myModal').modal('show');
    }

    $scope.saveAssessmentofLoss = function () {
        if ($scope.test == 0) {
            alert("Please Choose any Field");
            return false;
        }
        var x = $('#formAL');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.al.ClaimID = angular.copy(_claimId);
        $scope.al.PSI_ID =angular.copy($scope.pk_PSI_Id);
        $http.post('/MISCFSR/saveAssessmentofLoss', {al:$scope.al}).then(function (response) {
            console.log(response);
            $scope.cancelAL();
            showSuccessToast(response.data);
            $scope.getAssessmentofLoss();
            //$scope.listTableName = response.data.listTableName;
        }, function (error) {
        })
    }
    $scope.saveDescofOriginProcurement = function () {
        if ($scope.test == 0)
        {
            alert("Please Choose any Field");
            return false;
        }
        $scope.d.ClaimID = angular.copy(_claimId);
        $scope.d.PSI_ID = angular.copy($scope.pk_PSI_Id);
        $http.post('/MISCFSR/saveDescofOriginProcurement', { d: $scope.d }).then(function (response) {
            console.log(response);
            
            $scope.cancelDOP();
            showSuccessToast(response.data);
            $scope.getDescofOriginProcurement();

        }, function (error) {
        })

    }
    $scope.getDescofOriginProcurement = function () {
        $http.get('/MISCFSR/getDescofOriginProcurement?claimId=' + _claimId + '&psi_Id=' + $scope.pk_PSI_Id + '').then(function (response) {
            console.log(response);
            $scope.listDOP = response.data.listDOP;
        }, function (error) {
        })
    }
    $scope.rowClickAL=function(a){
        $scope.al = angular.copy(a);
        a = {};
    }
    $scope.rowClickDOP = function (d) {
        $scope.d = angular.copy(d);
        d = {};
    }

    $scope.cancelAL = function () {
        $scope.al = {};
    }
    $scope.cancelDOP = function () {
        $scope.d = {};
    }
    $scope.getPolicySumInsured_AND_ShortName = function () {

        $http.get('/MISCFSR/getPolicySumInsured_AND_ShortName?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.listPSI = angular.copy(response.data.listPSI);

        }, function (error) {
        })

    }
    $scope.setData = function (psi_Id, shortName) {
        $scope.pk_PSI_Id = psi_Id;
        $scope.shortName = shortName;
        
    }
    $scope.getPolicySumInsured_AND_ShortName();
    $scope.getAssessmentofLoss();
    $scope.getDescofOriginProcurement();
    $scope.getDistinctTableName_byClaimID();
    
    
})