﻿angular.module('myApp').controller('RecommendationsLossRemarksCtrl', function ($scope, $http) {
    $scope.rl = {};
    var _claimId = $('#cid').val();
    $scope.saveRecommendationsLossRemarks = function (rec) {
        //var x = $('#formAI');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        var rl_copy = {};
        $scope.rl.ClaimID = angular.copy(_claimId);
        rl_copy = angular.copy($scope.rl);
        $http.post('/MISCFSR/saveRecommendationsLossRemarks', rl_copy).then(function (response) {
            console.log(response);
            $scope.getRecommendationsLossRemarks();
 
        }, function (error) {
        })
 
 
    }
    $scope.getRecommendationsLossRemarks = function () {
        $http.get('/MISCFSR/getRecommendationsLossRemarks?claimId=' + _claimId + ' ').then(function (response) {
           $scope.rl = angular.copy(response.data.rl);
          }, function (error) {

        })

    }
    $scope.cancel = function () {
        var id = angular.copy($scope.rl.PK_RL_ID);
        $scope.rl = {};
        $scope.rl.PK_RL_ID = angular.copy(id);
    }
    $scope.getRecommendationsLossRemarks();
})