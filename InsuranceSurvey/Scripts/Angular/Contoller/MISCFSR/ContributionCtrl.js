﻿angular.module('myApp').controller('ContributionCtrl', function ($scope, $http, $filter, CommonService) {
    var _claimId = $('#cid').val();
    //var hardCodetotalvalue = 10000;
    $scope.getRLP_Total_byClaimID = function () {

        $http.get('/MISCFSR/getRLP_Total_byClaimID?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.rlp_Total = response.data.rlp_Total;

        }, function (error) {
        })
    }



    $scope.getContribution = function () {

        $http.get('/MISCFSR/getContribution?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
        var listCon = angular.copy(response.data.listCon);
        angular.forEach(listCon, function (element, index) {

            element.ShareInAmount = angular.copy(parseFloat($scope.rlp_Total) * (parseFloat(element.Percentage) / 100));
        })
        $scope.listCon = angular.copy(listCon);
           
        }, function (error) {
        })
    }

    $scope.saveContribution = function (rec) {
        var $btn = $('#btnSave');
        $btn.button('loading');
        $http.post('/MISCFSR/saveContribution', { listCon: $scope.listCon, claimId: _claimId }).then(function (response) {
            console.log(response);
            $scope.getRLP_Total_byClaimID();
            $scope.getContribution();
            $btn.button('reset');
            if (rec == 'n') {
                window.location = "/MISCFSR/InsuredConsent?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }

            

        }, function (error) {
        })
    }




    //$scope.getCoInsurers = function () {

    //    $http.get('/MISCFSR/getCoInsurers?claimId=' + _claimId + '').then(function (response) {
    //        console.log(response);
    //        $scope.coInsurers = angular.copy(response.data.listCIn);

    //    }, function (error) {
    //    })
    //}
    //$scope.getInsurers = function () {

    //    $http.get('/MISCFSR/getInsurers').then(function (response) {
    //        $scope.listIns = angular.copy(response.data);

    //    }, function (error) {
    //    })
    //}
    //$scope.getInsurers();
    //$scope.getCoInsurers();
    $scope.getRLP_Total_byClaimID();
    $scope.getContribution();
})