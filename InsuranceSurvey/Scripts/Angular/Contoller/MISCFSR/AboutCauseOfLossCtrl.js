﻿angular.module('myApp').controller('AboutCauseOfLossCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.acl = {};
    var _claimId = $('#cid').val();



    $scope.saveAboutCauseOfLoss = function (rec) {

        var x = $('#formACL');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }

        var acl_copy = {};
        var sa_Id = '';
        angular.forEach($scope.listSAI, function (element, index) {
            if (element.chkboxSAI) {
                console.log(element.PK_SA_ID);
                sa_Id += element.PK_SA_ID + ',';
            }
        })
        sa_Id = sa_Id.slice(0, -1);
        console.log(sa_Id);
        $scope.acl.StatutoryAuthorities_ID = angular.copy(sa_Id);
        $scope.acl.ClaimID = angular.copy(_claimId);
        acl_copy = angular.copy($scope.acl);
        //acl_copy.Date = CommonService.convertDateStringToDateObj(acl_copy.Date);
        $http.post('/MISCFSR/saveAboutCauseOfLoss', acl_copy).then(function (response) {
            console.log(response);
            $scope.getAboutCauseOfLoss();
            if (rec == 'n') {
                window.location = "/MISCFSR/OtherReportsForMaterialFacts?cid=" + _claimId + "";
            }
            else {
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }

    $scope.getAboutCauseOfLoss = function () {

        $http.get('/MISCFSR/getAboutCauseOfLoss?claimId=' + _claimId + '').then(function (response) {
            console.log(response);

            var listACL = angular.copy(response.data.listACL);
            var myArr = listACL.StatutoryAuthorities_ID;
            angular.forEach(myArr, function (e, i) {
                var Temp = parseInt(e);
                angular.forEach($scope.listSAI, function (element, index) {
                    if (element.PK_SA_ID == Temp) {
                         element.chkboxSAI = true;
                    }
                })

            })
            
            $scope.acl = angular.copy(listACL);
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        var id = angular.copy($scope.acl.PK_MISCACL);
        $scope.acl = {};
        $scope.acl.PK_MISCACL = angular.copy(id);
    }
    $scope.getIntroClaim = function () {

        $http.get('/MISCFSR/getIntroClaim?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            var listIC = angular.copy(response.data);
            $scope.ic = angular.copy(listIC);
            //$scope.ic.PK_MISCFSRIPRID = angular.copy($scope.cd.PK_MISCFSRIPRID);
            //$scope.ic.PeriodofPolicyFromDT = angular.copy($scope.cd.PeriodofPolicyFromDT);
            //$scope.ic.PeriodofPolicyToDT = angular.copy($scope.cd.PeriodofPolicyToDT);
            //$scope.ic.PeriodofPolicyFromDT = angular.copy($scope.cd.PeriodofPolicyFromDT);
            //$scope.ic.PeriodofPolicyToDT = angular.copy($scope.cd.PeriodofPolicyToDT);
            //console.log($scope.ic.PeriodofPolicyToDT);
        }, function (error) {
        })
    }
    $scope.getACL_NatureofLoss = function () {

        $http.get('/MISCFSR/getACL_NatureofLoss').then(function (response) {
            console.log(response);
            $scope.listNL = angular.copy(response.data.listNL);

        }, function (error) {
        })
    }
    $scope.getEffectofOperatedPeril = function () {

        $http.get('/MISCFSR/getEffectofOperatedPeril').then(function (response) {
            console.log(response);
            $scope.listEOP = angular.copy(response.data.listEOP);

        }, function (error) {
        })
    }
    $scope.getStatutoryAuthoritiesInformed = function () {

        $http.get('/MISCFSR/getStatutoryAuthoritiesInformed').then(function (response) {
            console.log(response);
            $scope.listSAI = angular.copy(response.data.listSAI);

        }, function (error) {
        })
    }
    $scope.checkboxch = function () {
        var sa_Id = '';
        angular.forEach($scope.listSAI, function (element, index) {
            if (element.chkboxSAI) {
                console.log(element.PK_SA_ID);
                sa_Id += element.PK_SA_ID + ',';
            }
        })
        sa_Id = sa_Id.slice(0, -1);
        console.log(sa_Id);
    }
    $scope.getStatutoryAuthoritiesInformed();
    $scope.getIntroClaim();
    
    $scope.getEffectofOperatedPeril();
    
    $scope.getAboutCauseOfLoss();
    $scope.getACL_NatureofLoss();
})