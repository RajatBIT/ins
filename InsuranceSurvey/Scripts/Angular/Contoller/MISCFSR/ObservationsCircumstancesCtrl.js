﻿angular.module('myApp').controller('ObservationsCircumstancesCtrl', function ($scope, $http, $filter, CommonService) {
    $scope.oc = {};
    var _claimId = $('#cid').val();

    $scope.tableName = "SiteVisit";
    $scope.prevtableName = "AddVisit";



    
    $scope.saveObservationsCircumstances = function (rec) {
        var x = $('#formOC');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        var oc_copy = {};
        $scope.oc.ClaimID = _claimId;
        oc_copy = angular.copy($scope.oc);
        $http.post('/MISCFSR/saveObservationsCircumstances', {oc: oc_copy, listSV:$scope.listSV }).then(function (response) {
            console.log(response);



            var clPD = ' ';
            var nsPD = ' ';
            
            if (window.FormData !== undefined) {
                //alert($("#PoliceReportPhoto").val());
                var fileData = new FormData();
                if ($("#CLQuotePhotoDoc").val() != '') {
                    //alert("hello");
                    fileData.append($("#CLQuotePhotoDoc").get(0).files[0].name, $("#CLQuotePhotoDoc").get(0).files[0]);
                    clPD = "yes";
                    //alert($("#PoliceReportPhoto").val());
                    //alert(pol);
                }
                if ($("#NewsPhotoDoc").val() != '') {
                    fileData.append($("#NewsPhotoDoc").get(0).files[0].name, $("#NewsPhotoDoc").get(0).files[0]);
                    nsPD = "yes";
                    //alert($("#FireBrigadeReportPhoto").val());
                    //alert(fr);
                }
                
                $.ajax({
                    url: '/MISCFSR/saveObservationsCircumstancesPhotoDoc?clPhotoDoc=' + clPD + '&newsPhotoDoc=' + nsPD +'&claimId=' + _claimId + ' ',
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fileData,
                    success: function (result) {
                        //alert(result);
                    },
                    error: function (err) {
                        //alert(err.statusText);
                    }
                });

            }
            $scope.getObservationsCircumstances();
            if (rec == 'n') {
                window.location = "/MISCFSR/AboutInsured?cid=" + _claimId + "";
            }
            else {
                $scope.getObservationsCircumstances();
                showSuccessToast(response.data);
            }
        }, function (error) {
        })
    }


    $scope.getObservationsCircumstances = function () {

        $http.get('/MISCFSR/getObservationsCircumstances?claimid=' + _claimId + ' ').then(function (response) {
            $scope.oc = angular.copy(response.data.listOC);
        }, function (error) {
        })

    }
    $scope.cancel = function () {
        var id = angular.copy($scope.oc.PK_OBSCIRID);
        $scope.oc = {};
        $scope.oc.PK_OBSCIRID = angular.copy(id);
    }
    $scope.getSurveyVisit = function () {

        $http.get('/MISCFSR/getSurveyVisit?claimId=' + _claimId + '&tableName=' +$scope.tableName + ' ').then(function (response) {
            $scope.listSV = angular.copy(response.data.listSV);
            //$scope.sv = angular.copy(listsv);
        }, function (error) {
        })
    }
    $scope.getSurveyVisit();
    $scope.getObservationsCircumstances();
})