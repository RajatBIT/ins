﻿angular.module('myApp').controller('BrokerCtrl', function ($scope, $http, CommonService) {
    
    $scope.br = {};

    $scope.bb = {};
    
    $scope.BrokerRegisterValidation = function (rec) {
  
        var x = $('#formBrokerRegistration');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        if ($scope.br.Password != $scope.br.ConfirmPassword)
        {
            alert("Password is Not Match! Please Enter Correct Password");
            return false;
        }
        var i = 0;
        $http.post('/Home/BrokerRegisterValidation', { br: $scope.br }).then(function (response) {
            console.log(response);
            var validationMsg = '';
            if (response.data.EmailIdStatus == 1) {
                validationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                i++;
            }
            if (response.data.MobileNoStatus == 1) {
                validationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                i++;
            }
            else if (response.data.UserNameStatus == 1) {
                validationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                i++;
            }
            
            
            
            if(i>0)
            {
                CommonService.showValidationPopup(validationMsg);
                return false;
            }
            else {
                //alert();
              otp('IN')
              $("#openPopup").click();
            }

        }, function (error) {
        })

    }
    $scope.saveBrokerRegisteration = function () {

        $http.post('/Home/saveBrokerRegisteration', { br: $scope.br,listBrokerBranch_EmailId:$scope.listBrokerBranch_EmailId }).then(function (response) {
            console.log(response);
            $scope.br = {};
            $scope.listBrokerBranch_EmailId = [];
            if (response.data.msg == "s")
            {
                showSuccessToast("Saved Successfully");
            }
            else {
                showSuccessToast("Failed! Try Again");
            }

        }, function (error) {
        })

    }
    $scope.getBroker = function (r_Id) {

        $http.get('/Collaborator/getBroker').then(function (response) {
            console.log(response);
            $scope.listBroker = response.data.listBroker;
        }, function (error) {
        })

    }
    $scope.setIRDARegisteration = function () {
        $scope.IrdaRegNo;
        angular.forEach($scope.listBroker, function (element, index) {
            if (element.PK_Broker_ID == $scope.br.Broker_ID)
            {
                $scope.IrdaRegNo=element.IrdaRegNo;
            }
        })
    }





    
    $scope.listBrokerBranch_EmailId = [];
    $scope.addInListBrokerBranch_EmailId = function () {
        $scope.listBrokerBranch_EmailId.push($scope.bb);
        $scope.bb = {};
    }

    $scope.deleteBrokerBranch_And_EmailId = function (index, PK_BB_ID) {
        $scope.listBrokerBranch_EmailId.splice(index, 1);
        if (angular.isDefined(PK_BB_ID)) {
            $http.get('/Home/deleteBrokerBranch_And_EmailId?PK_BB_ID=' + PK_BB_ID).then(function (response) {
            }, function (error) {
            })
        }
    }

    $scope.getBrokerRegisteration = function () {

        $http.get('/SuperAdmin/getBrokerRegisteration').then(function (response) {
            console.log(response);
            $scope.listBrokerDetails = response.data.listBrokerDetails;
        }, function (error) {
        })

    }


    $scope.approveOrDiscardBroker = function (PK_BR_ID,ApprovedStatus) {

        $http.get('/SuperAdmin/approveOrDiscardBroker?PK_BR_ID='+PK_BR_ID+'&ApprovedStatus='+ApprovedStatus).then(function (response) {
            console.log(response);
            $scope.getBrokerRegisteration();

            if(response.data.msg=='s')
            {   
                showSuccessToast("User has been  " + response.data.ApprStatus);
            }
        }, function (error) {
        })

    }

    $scope.getBroker();
    $scope.getBrokerRegisteration();
 
})