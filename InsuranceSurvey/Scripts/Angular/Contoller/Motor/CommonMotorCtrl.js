﻿angular.module('myApp').controller('CommonMotorCtrl', function ($scope, $http) {

    var _claimId = $('#cid').val();
    $scope.pl = {};
    $scope.ro = {};
    $scope.s = {};
    $scope.s.Salvage = 'Salvage of the affected/replaced parts of the vehicle had been destructed in our presence, accordingly to be considered as scrap and a notional value of Rs. --------- @ 10% of the claim amount had been deducted from the loss assessment, as agreed with the Insured, which is fair and reasonable in our opinion.';
    $scope.saveParticularsOfLossDamage = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.pl.ClaimID = angular.copy(_claimId);
        $http.post('/Collaborator/saveParticularsOfLossDamage', { pl: $scope.pl }).then(function (response) {
            console.log(response);
            $scope.getParticularsOfLossDamage();

            if (rec == 'n') {
                window.location = "/Survery/EstimateLoss?id=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getParticularsOfLossDamage = function () {

        $http.get('/Collaborator/getParticularsOfLossDamage?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.pl = response.data.pl;
        }, function (error) {
        })
    }
    $scope.saveRemarksObservations = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.ro.ClaimID = angular.copy(_claimId);
        $http.post('/Collaborator/saveRemarksObservations', { ro: $scope.ro }).then(function (response) {
            console.log(response);
            $scope.getRemarksObservations();

            if (rec == 'n') {
                window.location = "/Survery/MotorAssessmentSummery?cid=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getRemarksObservations = function () {

        $http.get('/Collaborator/getRemarksObservations?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            debugger;
            $scope.ro = response.data.ro;
        }, function (error) {
        })
    }
    $scope.saveSalvage = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.s.ClaimID = angular.copy(_claimId);
        $http.post('/Collaborator/saveSalvage', { s: $scope.s }).then(function (response) {
            console.log(response);
            $scope.getSalvage();
            if (rec == 'n') {
                window.location = "/Survery/ReInspection?cid=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
           }
        }, function (error) {
        })
    }
    $scope.getSalvage = function () {

        $http.get('/Collaborator/getSalvage?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            debugger;
            if (response.data.s.PK_S_ID != 0) {
                $scope.s = response.data.s;
            }
        }, function (error) {
        })
    }
    $scope.cancelParticularsOfLossDamage = function () {
        $scope.pl = {};
    }

    $scope.cancelRemarksObservations = function () {
        $scope.ro = {};
    }
    $scope.cancelSalvage = function () {
        $scope.s = {};
    }
    
    $scope.getParticularsOfLossDamage();
    $scope.getRemarksObservations();
    $scope.getSalvage();
})