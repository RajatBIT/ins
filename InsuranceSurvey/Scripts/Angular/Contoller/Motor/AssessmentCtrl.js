﻿angular.module('myApp').controller('AssessmentCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.a = {};
    $scope.ah = {};
    $scope.ac = {};
    $scope.ah.SubTotalC = 0;
    $scope.ah.SubTotalA = 0;
    $scope.test = 0;
    $scope.ah.AddDepriciationRate = 0;
    $scope.ah.AddDepriciationAmount = 0;
    $scope.ah.SalvageRate = 0;
    $scope.ah.SalvageAmount = 0;
    $scope.ah.DiscountRate = 0;
    $scope.ah.DiscountAmount = 0;
    $scope.ah.DepriciationRate = 0;
    $scope.ah.DepriciationAmount = 0;
    $scope.shortName;
    $scope.ac.Sign = "Add";
    $scope.btnSave = 1;

    $scope.getMetologyOfItems = function () {

        $http.get('/Collaborator/getMetologyOfItems').then(function (response) {
            console.log(response);
            $scope.listMI = response.data.listMI;
            //angular.forEach($scope.listMI, function (element,index) {
            //    if(index==1)
            //    {
            //       element.radiobtn = true;
            //    }
            //})
        }, function (error) {
        })
    }
    $scope.getMotorParticular = function () {
        $http.get('/Collaborator/getMotorParticular').then(function (response) {
            console.log(response);
            $scope.listP = response.data.listP;
        }, function (error) {
            console.log(error);
        })
    }
    $scope.listAssDesc = [];
    $scope.addInlistAssDesc = function () {
        if ($scope.test == 0)
        {
            alert("Please Select AnyOne  ");
            return false;
        }
        var x = $('#formAssDesc');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.listAssDesc.push($scope.a);
        $scope.a = {};
        $scope.calAmount();
        $scope.commonFuncForBindCal()   ;
    }
    
    
    $scope.calAmount = function ()
    {
        $scope.ah.SubTotalC = 0;
        $scope.ah.SubTotalA = 0;
        //Amount=Qty*Rate;
        angular.forEach($scope.listAssDesc, function (element, index) {
            var qc = (angular.isUndefined(element.QtyC) || (element.QtyC == '')) ? 0 : parseInt(element.QtyC);
            var rc = (angular.isUndefined(element.RateC) || (element.RateC == '')) ? 0 : parseFloat(element.RateC);;
            element.AmountC =parseFloat(qc)* parseFloat(rc);
               
            
            $scope.ah.SubTotalC += parseFloat(element.AmountC);
            var qa = (angular.isUndefined(element.QtyA) || (element.QtyA == '')) ? 0 : parseInt(element.QtyA);
            var ra = (angular.isUndefined(element.RateA) || (element.RateA == '')) ? 0 : parseFloat(element.RateA);
            element.AmountA =parseFloat(qa) *parseFloat(ra);
         
            $scope.ah.SubTotalA += parseFloat(element.AmountA);
        })
        $scope.ah.GrandTotal =angular.copy($scope.ah.SubTotalA);
        //$scope.commonFuncForBindCal();
        //$scope.calSalvage();

        //$scope.calAddDepriciation();
        //$scope.calDiscount();
        //$scope.calGrandTotal();

        //$scope.listAD = angular.copy(templistAD);
        //$scope.ah.TotalAmountC = angular.copy($scope.ah.SubTotalC);
        //$scope.ah.TotalAmountA = angular.copy($scope.ah.SubTotalA);
        //$scope.commonFuncForBindCal();
        
    }
    $scope.listAssCalc = [];
    $scope.addInlistAssCalc = function () {
        var x = $('#formAssCalc');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $scope.listAssCalc.push($scope.ac);
        $scope.ac = {};
        $scope.ac.Sign = "Add";
        //var x = $('#formPart');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

       
       
        $scope.commonFuncForBindCal();
    }
    $scope.deleteInlistAssCalc = function (pk_AC_ID,index) {
        $scope.listAssCalc.splice(index, 1);
        $scope.commonFuncForBindCal();
        if(angular.isDefined(pk_AC_ID))
        $http.get('/Collaborator/deleteMotorAssessmentCalculation?pk_AC_ID=' + pk_AC_ID).then(function (response) {
            console.log(response);
            $scope.saveAssessment('d');
        }, function (error) {
        })
    }

    $scope.commonFuncForBindCal = function () {
        $scope.calAmount();
            angular.forEach($scope.listAssCalc, function (element, index) {
                if (element.Rate != 0 && element.Rate != '' && element.Rate != undefined) {
                    //debugger;
                    element.Amount = (parseFloat($scope.ah.SubTotalA) * (element.Rate / 100)).toFixed(2);
                }

                if (element.Sign == "Add") {
                    
                    $scope.ah.GrandTotal = (parseFloat($scope.ah.GrandTotal) + parseFloat(element.Amount)).toFixed(2);
                    debugger;
                }
                else if (element.Sign == "Less") {
                    //debugger;
                    $scope.ah.GrandTotal = (parseFloat($scope.ah.GrandTotal) - parseFloat(element.Amount)).toFixed(2);
                }
                else if (element.Sign == "Multiply") {
                    $scope.ah.GrandTotal = (parseFloat($scope.ah.GrandTotal) * parseFloat(element.Amount)).toFixed(2);
                }
                else if (element.Sign == "Divide") {
                    $scope.ah.GrandTotal = (parseFloat($scope.ah.GrandTotal) / parseFloat(element.Amount)).toFixed(2);
                }

            })
            $scope.calSalvage();
            $scope.calDepriciation();
            $scope.calAddDepriciation();
            $scope.calDiscount();
            $scope.calGrandTotal();
     
    }
    $scope.calRateToAmount = function () {
            $scope.ac.Amount = angular.copy((parseFloat($scope.ah.SubTotalA) * parseFloat($scope.ac.Rate / 100)).toFixed(2));
     
    }
    $scope.calAddDepriciation = function () {
    
        var sta_ad = (angular.isUndefined($scope.ah.SubTotalA) || ($scope.ah.SubTotalA == '')) ? 0 : parseFloat($scope.ah.SubTotalA);
        var adr = (angular.isUndefined($scope.ah.AddDepriciationRate) || ($scope.ah.AddDepriciationRate == '')) ? 0 : parseFloat($scope.ah.AddDepriciationRate / 100).toFixed(2);
        $scope.ah.AddDepriciationAmount =parseFloat( sta_ad * adr).toFixed(2);
        //$scope.ah.AddDepriciationAmount = angular.copy((parseFloat($scope.ah.SubTotalA) * parseFloat($scope.ah.AddDepriciationRate / 100)).toFixed(2));
        
    }
    $scope.calSalvage = function () {
        
        var sta_s= (angular.isUndefined($scope.ah.SubTotalA) || ($scope.ah.SubTotalA == '')) ? 0 : parseFloat($scope.ah.SubTotalA);
        var sr = (angular.isUndefined($scope.ah.SalvageRate) || ($scope.ah.SalvageRate == '')) ? 0 : parseFloat($scope.ah.SalvageRate / 100).toFixed(2);
        console.log('sta_s' + sta_s);
        console.log('sr' + sr);
        $scope.ah.SalvageAmount =parseFloat( sta_s * sr).toFixed(2);
        //$scope.ah.SalvageAmount = angular.copy((parseFloat($scope.ah.SubTotalA) * parseFloat($scope.ah.SalvageRate / 100)).toFixed(2));
        
    }

    $scope.calDiscount = function () {
    
        var sta_di = (angular.isUndefined($scope.ah.SubTotalA) || ($scope.ah.SubTotalA == '')) ? 0 : parseFloat($scope.ah.SubTotalA);
        var dr = (angular.isUndefined($scope.ah.DiscountRate) || ($scope.ah.DiscountRate == '')) ? 0 : parseFloat($scope.ah.DiscountRate / 100).toFixed(2);
        $scope.ah.DiscountAmount =parseFloat( sta_di * dr).toFixed(2);
        //$scope.ah.DiscountAmount = angular.copy((parseFloat($scope.ah.SubTotalA) * parseFloat($scope.ah.DiscountRate / 100)).toFixed(2));
        
    }
    $scope.calDepriciation = function () {
    
        $scope.ah.DepriciationAmount = angular.copy((parseFloat($scope.ah.SubTotalA) * parseFloat($scope.ah.DepriciationRate / 100)).toFixed(2));
        
    }
    $scope.calGrandTotal = function () {
        $scope.ah.GrandTotal = (parseFloat($scope.ah.GrandTotal) - parseFloat($scope.ah.SalvageAmount) - parseFloat($scope.ah.DepriciationAmount) - parseFloat($scope.ah.AddDepriciationAmount) - parseFloat($scope.ah.DiscountAmount)).toFixed(2);
        
    }
    $scope.setData = function (shortName, percentage) {
        $scope.cancel();
        console.log(percentage);
        $scope.test = 1;
        $scope.shortName=shortName;
        if(shortName!='M')
        {
            $scope.ah.DepriciationRate = parseFloat(percentage);
            //$scope.calDepriciation();
            //$scope.commonFuncForBindCal();
        }
        else if(shortName=='M')
        {   
            $http.get('/Collaborator/getDepriciation_byMonth?claimId='+_claimId).then(function (response) {
                console.log(response);
                $scope.ah.DepriciationRate = response.data.deprRate;
                //$scope.calDepriciation();
                $scope.commonFuncForBindCal();
            }, function (error) {
                console.log(error);
            })
        }

    }

    $scope.saveAssessment = function (p) {
        //var x = $('#formDesc');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}
        if ($scope.test == 0) {
            alert("Please Select AnyOne  ");
            return false;
        }
        if ($scope.listAssDesc.length == 0)
        {
            alert("Please Fill Data");
            return false;
        }
        $scope.ah.ShortName = $scope.shortName;
        $scope.ah.ClaimID = _claimId;
        //$scope.commonFuncForBindCal();
        $scope.btnSave = 0;
        $http.post('/Collaborator/saveAssessment', { ah: $scope.ah, listAssDesc: $scope.listAssDesc, listAssCalc:$scope.listAssCalc }).then(function (response) {
            console.log(response);
            $scope.cancel();
            if (p != 'd') {
                if (response.data.msg == 's') {
                    showSuccessToast("Saved Successfully");
                }
                else {
                    showSuccessToast("Failed! Try Aagin");
                }
            }
            $scope.getAssessment();
            $scope.btnSave = 1;
            
            
        }, function (error) {
            $scope.btnSave = 1;
        })
    }
   
    $scope.getAssessment = function () {
        
        $http.get('/Collaborator/getAssessment?claimId='+_claimId+'&shortName='+$scope.shortName).then(function (response) {
            console.log(response);
            if (response.data.ah.PK_AH_ID != 0) {
                $scope.ah = response.data.ah;
            }
            if (response.data.listAssDesc.length != 0) {
                $scope.listAssDesc = response.data.listAssDesc;
            }
            if (response.data.listAssCalc.length != 0) {
                $scope.listAssCalc = response.data.listAssCalc;
            }
        }, function (error) {
        })
    }

    $scope.cancel = function () {
        $scope.ah = {};
        $scope.listAssDesc = [];
        $scope.listAssCalc = [];
    }
    $scope.getMetologyOfItems();
    $scope.getMotorParticular();
    $scope.getAssessment();
})