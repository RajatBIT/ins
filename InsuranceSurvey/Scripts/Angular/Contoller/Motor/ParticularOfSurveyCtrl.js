﻿angular.module('myApp').controller('ParticularOfSurveyCtrl', function ($scope, $http, $filter, CommonService) {
    var _claimId = $('#cid').val();
    $scope.ps = {};
    $scope.saveParticularOfSurvey = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.ps.ClaimID = angular.copy(_claimId);
       $scope.ps.SurveyTime = $('#surveyTime').val();
        $http.post('/Collaborator/saveParticularOfSurvey', { ps: $scope.ps }).then(function (response) {
            console.log(response);

            if (rec == 'n') {
                window.location = "/Survery/MotorVehicleParticulars?id=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getParticularOfSurvey = function () {

        $http.get('/Collaborator/getParticularOfSurvey?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.ps = response.data.ps;
            $('#surveyTime').val($scope.ps.SurveyTime);
        }, function (error) {
        })
    }
    $scope.getInstructorName = function () {

        $http.get('/Collaborator/getInstructorName?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.InstructorName = response.data.InstructorName;
        }, function (error) {
        })
    }
   
    
    $scope.getInstructorName();
    $scope.getParticularOfSurvey();

})