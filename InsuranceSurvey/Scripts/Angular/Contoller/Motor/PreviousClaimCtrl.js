﻿angular.module('myApp').controller('PreviousClaimCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.p = {};
    $scope.arrDateOfAcc = [];
    $scope.t = {};
    $scope.t.TempDate = '';
    $scope.savePreviousClaim = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.p.FK_ClaimID = angular.copy(_claimId);

        //$scope.p.DateOfAcc = $scope.arrDateOfAcc;
        $scope.p.DateOfAcc = '';
        angular.forEach($scope.arrDateOfAcc, function (element, index) {
            if (index == 0) {
                $scope.p.DateOfAcc += element.TempDate;
            }
            if (index>0) {
                $scope.p.DateOfAcc +=','+ element.TempDate;
            }
        })
        
        $http.post('/Collaborator/savePreviousClaim', { p: $scope.p }).then(function (response) {
            console.log(response);
            $scope.getPreviousClaim();
            if (rec == 'n') {
                window.location = "/Survery/ParticularsOfDamage?cid=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getPreviousClaim = function () {

        $http.get('/Collaborator/getPreviousClaim?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.p = response.data.p;
            $scope.p.DateOfAcc = $scope.p.DateOfAcc.split(',');
           $scope.arrDateOfAcc=[];
            angular.forEach($scope.p.DateOfAcc, function (element, index) {
              
                $scope.t = {};
                $scope.t.TempDate = '';
                $scope.t.TempDate = element;
                $scope.arrDateOfAcc.push($scope.t);
                $scope.t = {};

            });
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        $scope.p = {};
    }
    
    $scope.createManyColumnfor_DateOfAcc_byNoOfAcc = function () {
        $scope.arrDateOfAcc = [];
        for(var i=1;i<=$scope.p.NoOfAcc;i++)
        {
            $scope.arrDateOfAcc.push($scope.t);
            $scope.t = {};
        }
        
    }
    $scope.test = function () {
        console.log($scope.arrDateOfAcc);
    }



    $scope.getPreviousClaim();
})