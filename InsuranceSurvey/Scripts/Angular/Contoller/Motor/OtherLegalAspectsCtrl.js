﻿angular.module('myApp').controller('OtherLegalAspectsCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.ol = {};

    $scope.saveOtherLegalAspects = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.ol.ClaimID = angular.copy(_claimId);
        $http.post('/Collaborator/saveOtherLegalAspects', { ol: $scope.ol }).then(function (response) {
            console.log(response);
            $scope.getOtherLegalAspects();
            if (rec == 'n') {
                window.location = "/Survery/PreviousClaimHistory?cid=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getOtherLegalAspects = function () {

        $http.get('/Collaborator/getOtherLegalAspects?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.ol = response.data.ol;
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        $scope.ol = {};
    }

    $scope.getOtherLegalAspects();
})