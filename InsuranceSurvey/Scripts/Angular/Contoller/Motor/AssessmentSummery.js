﻿angular.module('myApp').controller('AssessmentSummeryCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    
    $scope.mas = {};
    $scope.mas.GrandTotalofAssSummeryofAssHeader = 0;
    $scope.mas.TowingCharge = 0;
    $scope.mas.Excess = 0;
    $scope.getAssessmentHeader_usingFor_AssessmentSummery = function () {

        $http.get('/Collaborator/getAssessmentHeader_usingFor_AssessmentSummery?claimId='+_claimId).then(function (response) {
            console.log(response);
            $scope.listAssHeader = response.data.listAssHeader;
        }, function (error) {
        })
    }
    $scope.commonFunctionForCal = function () {
        $scope.mas.AfterTowingChargeTotal = (parseFloat($scope.mas.GrandTotalofAssHeader) + parseFloat($scope.mas.TowingCharge)).toFixed(2);
        if ($scope.mas.AfterTowingChargeTotal == 'NaN')
        {
            $scope.mas.AfterTowingChargeTotal = 0;
        }
        $scope.mas.GrandTotalofAssSummery =parseFloat( $scope.mas.AfterTowingChargeTotal) -parseFloat( $scope.mas.Excess);
    }
    $scope.saveAssessmentSummery = function (rec) {
        $scope.mas.ClaimID = angular.copy(_claimId);
        debugger;
        console.log($scope.mas);
        $http.post('/Collaborator/saveAssessmentSummery', { mas :$scope.mas}).then(function (response) {
            console.log(response);
            if (rec == 'n') {
                window.location = "/Survery/MotorSalvage?cid=" + _claimId + "";
            }
            else {
                if (response.data.msg == 's') {
                    showSuccessToast("Saved Successfully");
                }
                else {
                    showSuccessToast("Failed! Try Aagin");
                }
            }
        }, function (error) {
        })

    }
    $scope.getAssessmentSummery = function () {

        $http.get('/Collaborator/getAssessmentSummery?claimId=' + _claimId).then(function (response) {
            console.log(response);
            $scope.mas = response.data.mas;
        }, function (error) {
        })
    }
    $scope.getAssessmentSummery();
    $scope.getAssessmentHeader_usingFor_AssessmentSummery();
})