﻿angular.module('myApp').controller('OccurenceCtrl', function ($scope, $http, CommonService) {
    var _claimId = $('#cid').val();
    $scope.o = {};
    $scope.p = {};
    $scope.b = {};
    $scope.listTPP = [];
    $scope.listTPB = [];
    $scope.listLossClaim = [];
    $scope.listDamageClaim = [];

    $scope.saveOccurenceNew = function (p) {
        $scope.lm_ids = '';
        $scope.ds_Ids = '';
        var x = $('#formOccurence');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }

        angular.forEach($scope.listLossClaim, function (element, index) {
            if (element.IsChecked) {
                $scope.lm_ids += element.LM_ID + ',';
            }
        })
        if ($scope.lm_ids.length > 0) {
            $scope.lm_ids = $scope.lm_ids.slice(0, -1);

        }

        angular.forEach($scope.listDamageClaim, function (element, index) {
            if (element.IsChecked) {
                $scope.ds_Ids += element.DS_ID + ',';
            }
        })
        if ($scope.ds_Ids.length > 0) {
            $scope.ds_Ids = $scope.ds_Ids.slice(0, -1);

        }
        $scope.o.FK_ClaimID = _claimId;
        $http.post('/Collaborator/saveOccurenceNew', {
            o: $scope.o, listTPP: $scope.listTPP, listTPB: $scope.listTPB, lm_ids: $scope.lm_ids, ds_Ids: $scope.ds_Ids
        }).then(function (response) {
            if (response.data.msg == 's') {
                var p1;
                var p2;
                var fileData = new FormData();
                if ($("#photo1").val() != '') {
                    fileData.append($("#photo1").get(0).files[0].name, $("#photo1").get(0).files[0]);
                    p1 = "yes";
                }
                if ($("#photo2").val() != '') {
                    fileData.append($("#photo2").get(0).files[0].name, $("#photo2").get(0).files[0]);
                    p2 = "yes";
                }
                if ($("#photo1").val() != '' || $("#photo2").val() != '') {
                    $http({
                        url: '/Collaborator/saveOccurenceDoc?p1=' + p1 + '&p2=' + p2 + '&pk_Id=' + response.data.pk_Id,
                        method: 'POST',
                        data: fileData,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    }).then(function (resPhoto) {
                        console.log(resPhoto);
                        console.log(response);
                        if (resPhoto.data.msg == 's') {
                            showSuccessToast("Saved Successfully.");
                            $scope.clear();
                            $scope.getOccurenceNew();
                        }
                        else {
                            showSuccessToast("Failed Try Again");
                            $scope.clear();
                            $scope.getOccurenceNew();
                        }

                    }, function (errorPhoto) {

                    })
                }
                else {
                    showSuccessToast("Saved Successfully");
                $scope.clear();
                $scope.getOccurenceNew();
                }
            }
            else {
                showSuccessToast("Failed Try Again");
                $scope.clear();
                $scope.getOccurenceNew();
            }
        }, function (error) {

        })
    }

    $scope.getOccurenceNew = function (p) {
        $http.get('/Collaborator/getOccurenceNew?claimId=' + _claimId).then(function (response) {
            console.log('list');
            console.log(response);
            $scope.o = response.data.o;
            $scope.listTPP = response.data.listTPP;
            $scope.listTPB = response.data.listTPB;
            $scope.listLossClaim = response.data.listLossClaim;
            $scope.listDamageClaim = response.data.listDamageClaim;

        }, function (error) {

        })
    }

    $scope.addIn_listTPP = function () {
        var valditionMsgforTPP = '';
        var i = 0;
        $scope.p.PK_TP_ID = 0;
        if ($scope.p.NameOfOwner == '' || $scope.p.NameOfOwner == null || $scope.p.NameOfOwner == undefined)
        {
            valditionMsgforTPP += '<p>Please Enter Name Of Owner</p>';
            i++;
        }
        if ($scope.p.ParticularOfProperty == '' || $scope.p.ParticularOfProperty == null || $scope.p.ParticularOfProperty == undefined) {
            valditionMsgforTPP += '<p>Please Select Particular Of Property</p>';
            i++;
        }
        if ($scope.p.EstimatedValue == '' || $scope.p.EstimatedValue == null || $scope.p.EstimatedValue == undefined) {
            valditionMsgforTPP += '<p>Please Enter Estimated Value</p>';
            i++;
        }
        if (i > 0) {
            CommonService.showValidationPopup(valditionMsgforTPP);
            return false;
        }
        else {
            $scope.listTPP.push($scope.p);
        }
        $scope.p = {};
    }

    $scope.addIn_listTPB = function () {
        $scope.b.PK_TPB_ID = 0;
        var valditionMsgforTPB = '';
        var i = 0;
        $scope.p.PK_TP_ID = 0;
        if ($scope.b.NameOfVictim == '' || $scope.b.NameOfVictim == null || $scope.b.NameOfVictim == undefined) {
            valditionMsgforTPB += '<p>Please Enter Name Of Victim</p>';
            i++;
        }
        if ($scope.b.ParticularOfInjury == '' || $scope.b.ParticularOfInjury == null || $scope.b.ParticularOfInjury == undefined) {
            valditionMsgforTPB += '<p>Please Choose Particular Of Injury</p>';
            i++;
        }
        if ($scope.b.Hospitalization == '' || $scope.b.Hospitalization == null || $scope.b.Hospitalization == undefined) {
            valditionMsgforTPB += '<p>Please Choose Hospitalization</p>';
            i++;
        }
        if (i > 0) {
            CommonService.showValidationPopup(valditionMsgforTPB);
            return false;
        }
        else {
            $scope.listTPB.push($scope.b);
        }
        $scope.b = {};
    }
    $scope.clear = function () {
        $scope.o = {};
        $scope.listTPP = [];
        $scope.listTPB = [];
        $scope.listLossClaim = [];
        $scope.listDamageClaim = [];
        $scope.lm_ids = [];
        $scope.ds_Ids = [];
        $("#photo1").val('');
        $("#photo2").val('');
    }



    $scope.deleteIn_listTPP = function (pk_TP_Id, index) {
        $scope.listTPP.splice(index,1);
        if(pk_TP_Id!=undefined)
        {
            $http.get('/Collaborator/deleteOccThirdPartyPropDetails?pk_TP_Id=' + pk_TP_Id).then(function (response) {
                console.log(response);
            }, function (error) {

            })
        }
    }


    $scope.deleteIn_listTPB = function (pk_TPB_Id, index) {
        alert(pk_TPB_Id);
        $scope.listTPB.splice(index, 1);
        if (pk_TPB_Id != undefined) {
            $http.get('/Collaborator/deleteOccThirdPartyBodily?pk_TPB_Id=' + pk_TPB_Id).then(function (response) {
                console.log(response);
            }, function (error) {

            })
        }
    }
    $scope.getOccurenceNew();
})