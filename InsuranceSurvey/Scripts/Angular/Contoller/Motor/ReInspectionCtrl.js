﻿angular.module('myApp').controller('ReInspectionCtrl', function ($scope, $http) {
    var _claimId = $('#cid').val();
    $scope.r = {};
    $scope.saveReInspection = function (rec) {

        //var x = $('#formACL');
        //var status = $(x).ValidationFunction();
        //if (status == false) {
        //    return false;
        //}

        $scope.r.ClaimID = angular.copy(_claimId);
        $http.post('/Collaborator/saveReInspection', {r: $scope.r }).then(function (response) {
            console.log(response);
            $scope.getReInspection();

            if (rec == 'n') {
                window.location = "/Survery/MotorAssessment?cid=" + _claimId + "";
            }
            else {
            if (response.data.msg == 's') {
                showSuccessToast("Saved Successfully.");
            }
            else {
                showSuccessToast("Failed!Try Again.");
            }
            }
        }, function (error) {
        })
    }
    $scope.getReInspection = function () {

        $http.get('/Collaborator/getReInspection?claimId=' + _claimId + '').then(function (response) {
            console.log(response);
            $scope.r = response.data.r;
        }, function (error) {
        })
    }
    $scope.cancel = function () {
        $scope.r = {};
    }
    $scope.getReInspection();
})