﻿angular.module('myApp').controller('EmailGroupMstCtrl', function ($scope, $http) {
    $scope.egm = {};
    $scope.eg = {};

    $scope.saveEmailGroupMst = function (rec) {
  
        var x = $('#formEmailGroupMst');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        $http.post('/EmailGroupMst/saveEmailGroupMst', { egm: $scope.egm }).then(function (response) {
            console.log(response);
            showSuccessToast(response.data);
            $scope.getEmailGroupMst();
            
            $scope.egm = {};
        }, function (error) {
        })
    }
    $scope.getEmailGroupMst = function (rec) {

        $http.get('/EmailGroupMst/getEmailGroupMst').then(function (response) {
            console.log(response);
            $scope.listEGM = response.data.listEGM;

        }, function (error) {
        })
    }
    $scope.rowClick_EmailGroupMst = function () {
        $scope.egm =angular.copy(this.e);
    }
    $scope.clearEmailGroupMst = function () {
        $scope.egm = {};
    }
    $scope.saveEmailGroup = function (rec) {
        var x = $('#formEmailGroup');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }

        $http.post('/EmailGroupMst/saveEmailGroup', { eg: $scope.eg }).then(function (response) {
            console.log(response);
            showSuccessToast(response.data);
            $scope.getEmailGroup();

            $scope.eg = {};
        }, function (error) {
        })
    }
    $scope.getEmailGroup = function (rec) {

        $http.get('/EmailGroupMst/getEmailGroup').then(function (response) {
            console.log(response);
            $scope.listEG = response.data.listEG;
        }, function (error) {
        })
    }
    $scope.rowClick_EmailGroup = function () {
        $scope.eg = angular.copy(this.eg);
    }
    $scope.clearEmailGroup = function () {
        $scope.eg = {};
    }
    $scope.aaaa = '';
    $scope.testing = function () {
        alert($scope.aaaa);
    }
    $scope.sendEmail = function (rec) {
        $http.post('/EmailGroupMst/sendEmail', { groupId: $scope.EGM_ID,subject:$scope.subject, mailMsg: $scope.mailMsg }).then(function (response) {
            console.log(response);
            if(response.data=='s')
            {
                showSuccessToast("Mail Successfully Sent");
            }
            else {
                showSuccessToast("Failed !Try Again");
            }
            
        }, function (error) {
        })
    }
    $scope.getEmailGroupMst();
    $scope.getEmailGroup();
})