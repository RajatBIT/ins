﻿angular.module('myApp').controller('SurveyorRegistrationCtrl', function ($scope, $http, CommonService) {
    
    $scope.s = {};
    var validationMsg = '';
    var i = 0;
    
    $scope.SurveyorRegisterValidation = function (rec) {
        validationMsg = '';
        var x = $('#formInsuredRegistration');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        if ($scope.s.Password != $scope.s.ConfirmPassword)
        {
            CommonService.showValidationPopup("<p>Password is Not Match! Please Enter Correct Password</p>")
            return false;
        }
        var i = 0;
        $http.post('/Home/SurveyorRegisterValidation', { sr: $scope.s }).then(function (response) {
            console.log(response);
            
            if (response.data.EmailStatus == 1) {
                validationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                i++;
            }
            if (response.data.ISLANOStatus == 1) {
                validationMsg += "<p>IIISLA NO is already Exist! Please Enter Another IIISLA NO..</p>";
                i++;
            }

            if (response.data.IniRefStatus == 1) {
                validationMsg += "<p>Initial Reference is already Exist! Please Enter Another Initial Reference.</p>";
                i++;
            }
            if (response.data.MobileStatus == 1) {
                validationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                i++;
            }
            if (response.data.PANNOStatus == 1) {
                validationMsg += "<p>Pan No is already Exist! Please Enter Another Pan No..</p>";
                i++;
            }
            if (response.data.SLANoStatus == 1) {
                validationMsg += "<p>SLA No is already Exist! Please Enter Another SLA No.</p>";
                i++;
            }
            if (response.data.StaxNoStatus == 1) {
                validationMsg += "<p>GST IN is already Exist! Please Enter Another GST NO.</p>";
                i++;
            }
            if (response.data.UserNameStatus == 1) {
                validationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                i++;
            }
            if(i>0)
            {
                CommonService.showValidationPopup(validationMsg);
                return false;
            }
            else if(i==0) {
                $scope.saveSurveyorsRegistrationNew();
            }

        }, function (error) {
        })

    }
    var InsuredMsg = '';
    $scope.saveSurveyorsRegistrationNew = function () {
        $scope.s.AltEmail = $('#AltEmail_Val').html();
        $http.post('/Home/saveSurveyorsRegistrationNew', { s: $scope.s, listSurveyorDept: $scope.listSurveyorDept }).then(function (response) {
            console.log(response);
            $scope.clear();
            $scope.getSurveyorRegistration_byPK_SURID();
            $scope.getSurveyorDepartmentNew();
            if (response.data.msg == "s")
            {
                    showSuccessToast("Update Successfully");
            }
            else {
            
                showSuccessToast("Failed! Try Again");
            }

        }, function (error) {
            $scope.clear();
            $scope.getSurveyorRegistration_byPK_SURID();
        })

    }
 





    

    $scope.approveOrDiscardBroker = function (PK_BR_ID,ApprovedStatus) {

        $http.get('/SuperAdmin/approveOrDiscardBroker?PK_BR_ID='+PK_BR_ID+'&ApprovedStatus='+ApprovedStatus).then(function (response) {
            console.log(response);
            $scope.getBrokerRegisteration();

            if(response.data.msg=='s')
            {   
                showSuccessToast("User has been  " + response.data.ApprStatus);
            }
        }, function (error) {
        })

    }

    $scope.getRegion = function (r_Id) {
        $http.get('/Collaborator/getRegion').then(function (response) {
            console.log(response);
            $scope.listRegion = response.data.listRegion;

        }, function (error) {
        })
    }



    $scope.getState_byRegion = function (r_Id) {
        $http.get('/Collaborator/getState_byRegion?r_Id=' + $scope.s.R_ID).then(function (response) {
            console.log(response);
            $scope.listState = response.data.listState;

        }, function (error) {
        })
    }

    $scope.getDistricts_byStateId = function () {
        $http.get('/Collaborator/getDistricts_byStateId?FkState=' +$scope.s.FkState).then(function (response) {
            console.log(response);
            $scope.listDistricts = response.data.listDistricts;
        }, function (error) {
        })
    }
    $scope.getMembershipType = function () {

        $http.get('/Home/getMembershipType').then(function (response) {
            console.log(response);
            $scope.listMembershipType = response.data.listMembershipType;

        }, function (error) {
        })
    }
  

    $scope.getSurveyorRegistration_byPK_SURID = function () {

        $http.get('/Home/getSurveyorRegistration_byPK_SURID').then(function (response) {
            console.log(response);
            $scope.s = response.data.s;
            $scope.getState_byRegion();
            $scope.getDistricts_byStateId();

            $('#AltEmail').val($scope.s.AltEmail);
            $('#AltEmail_Val').html($scope.s.AltEmail);
            $('#AltEmail').next('.multiple_emails-container').remove();
            $('#AltEmail').multiple_emails();
        }, function (error) {
        })
    }
    $scope.clear = function () {
        $scope.s = {};
    }
    $scope.show_modalChangePassword = function () {
        $('#modalChangePassword').modal('show');
        

    }

    $scope.passwordMsg = '';
    $scope.oldPassword = '';
    $scope.newPassword = '';
    $scope.confirmPassword = '';
    $scope.updateSurveyorsPassword = function () {
        
        if ($scope.oldPassword=='' || $scope.newPassword=='' ||  $scope.confirmPassword=='')
        {
            $scope.passwordMsg = "Please Fill All Fields";
            return false;
        }
        if ($scope.newPassword != $scope.confirmPassword)
        {
            $scope.passwordMsg = "New Password And Confirm Password Not Match";
            return false;
        }
        if ($scope.newPassword == $scope.confirmPassword) {
            $http.post('/Home/updateSurveyorsPassword', { oldPassword: $scope.oldPassword, newPassword: $scope.newPassword }).then(function (response) {
                console.log(response);

                if (response.data.passwordCorrect == 0) {
                    $scope.passwordMsg = "Your Old Password Is Not Correct! ";
                    $scope.oldPassword = '';
                    return false;
                }
                if (response.data.msg == "s") {
                    showSuccessToast("Update Successfully");
                    $('#modalChangePassword').modal('hide');
                    $scope.passwordMsg = '';
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.confirmPassword = '';
                }
                else {
                    showSuccessToast("Failed! Try Again");
                    $scope.passwordMsg = '';
                    $scope.oldPassword = '';
                    $scope.newPassword = '';
                    $scope.confirmPassword = '';
                }
            }, function (error) {
                $scope.passwordMsg = '';
                $scope.oldPassword = '';
                $scope.newPassword = '';
                $scope.confirmPassword = '';
            })
        }

    }


    $scope.getSurveyorDepartmentNew = function () {

        $http.get('/Home/getSurveyorDepartmentNew').then(function (response) {
            console.log(response);
            $scope.listSurveyorDept = response.data.listSurveyorDept;
            
        }, function (error) {
        })
    }


    //$scope.tt = [{ a: "Array1", b: true }, { a: "array2", b: false }];
    //console.log($scope.tt);

    $scope.getAllTaskWithPendingTask = function () {
        $http.get('/Collaborator/getAllTaskWithPendingTask').then(function (response) {
            console.log('list');
            console.log(response);
            $scope.listAllTaskWithPendingTask = response.data.listAllTaskWithPendingTask;
            angular.forEach($scope.listAllTaskWithPendingTask, function (element, index) {
                element.task.sign = true;
            })

        }, function (error) {
        })
    }
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
 
    //$scope.getState();
   $scope.openCity =function(evt, cityName) {
       debugger;
       var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
       if(cityName!=undefined)
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";





        // Get the element with id="defaultOpen" and click on it
      var ev= document.getElementById("defaultOpen").click();
   }
   $scope.pendingClaimIds;
   $scope.colorchange = function (p) {
       $scope.pendingClaimIds;
       //$('#divTask').css('background-color', 'aliceblue');
       $('#divTask div').each(function (index, element) {
           $(element).css('background-color', 'aliceblue');
       })

       $('#' + p).closest('div').css('background-color', 'lightskyblue');
       //$('#' + p).css("background-color", "yellow");
       //$('.a' + p).show();
       angular.forEach($scope.listAllTaskWithPendingTask, function (element, index) {
           if (element.task.PK_TM_ID == p)
           {
               $scope.pendingClaimIds = element.listPendingTask;
               console.log($scope.pendingClaimIds);
           }

       })

   }
   $scope.signChange = function () {
       if (this.t.task.sign)
       {
           this.t.task.sign = false;
       }
       else {
           this.t.task.sign = true;
       }
   }

   
   $scope.a;
    $scope.getRegion();
    $scope.getMembershipType();
    $scope.getSurveyorRegistration_byPK_SURID();
    $scope.getSurveyorDepartmentNew();
    $scope.getAllTaskWithPendingTask();

})