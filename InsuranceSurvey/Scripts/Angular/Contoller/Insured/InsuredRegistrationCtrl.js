﻿angular.module('myApp').controller('InsuredRegistrationCtrl', function ($scope, $http, CommonService) {
    
    $scope.i = {};
    $scope.x = {};
    $scope.t = {};
    var validationMsg = '';
    $scope.i.InsuredType = "Single Policy Holder";

    var i = 0;
    $scope.InsuredRegisterValidation = function (rec) {
       
        var x = $('#formInsuredRegistration');
        var status = $(x).ValidationFunction();
        if (status == false) {
            if ($scope.i.InsuredType == 'Multiple Policy Holder' && $scope.listPolicy_Insurers.length==0)
            {
                $("#ErrorMes").append("<p>Please Enter Policy No</p>");
                $("#ErrorMes").append("<p>Please Select Insurer</p>");
                return false;
            }
            return false;
        }
        if ($scope.i.Password != $scope.i.ConfirmPassword)
        {
            //alert();
            CommonService.showValidationPopup("<p>Password is Not Match! Please Enter Correct Password</p>")
            return false;
        }
        var i = 0;
        $http.post('/Home/InsuredRegisterValidation', {i: $scope.i }).then(function (response) {
            console.log(response);
            
            if (response.data.EmailIdStatus == 1) {
                validationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                i++;
            }
            if (response.data.MobileNoStatus == 1) {
                validationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                i++;
            }
            else if (response.data.UserNameStatus == 1) {
                validationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                i++;
            }
            
            if (response.data.PANNOStatus == 1) {
                validationMsg += "<p>Pan No is already Exist! Please Enter Another Pan No..</p>";
                i++;
            }
            if (response.data.GSTINStatus == 1) {
                validationMsg += "<p>GSTIN is already Exist! Please Enter Another GSTIN.</p>";
                i++;
            }
            
            if(i>0)
            {
                CommonService.showValidationPopup(validationMsg);
                return false;
            }
            else {
                //alert();
              otp('IN')
              $("#openPopup").click();
            }

        }, function (error) {
        })

    }
    var InsuredMsg = '';
    $scope.saveInsuredRegistration = function () {
        if ($scope.i.InsuredType == 'Single Policy Holder')
        {
            $scope.listPolicy_Insurers.push($scope.t);
        }
        console.log($scope.listBrokerBranch_EmailId);
        $scope.i.AltEmail = $('#AltEmail_Val').html();
        $http.post('/Home/saveInsuredRegistration', { i: $scope.i, listPolicy_Insurers: $scope.listPolicy_Insurers}).then(function (response) {
            console.log(response);
        
            if (response.data.msg == "s")
            {
                InsuredMsg = response.data.result;
                if (window.FormData != undefined) {
                    var fileData = new FormData();
                    $('#mygridBody1 tr').each(function (index, element) {
                        //console.log($(this).find('input').attr('id'));
                        var id = $(this).find('input').attr('id');
                        if ($('#' + id).val() != null && $('#' + id).val() != undefined && $('#' + id).val() != '') {
                            fileData.append($('#' + id).get(0).files[0].name, $('#' + id).get(0).files[0]);
                        }
                    })
                    $http({
                        url: '/Home/saveInsuredRegistrationPhoto_Doc?PK_InsdID=' + response.data.PK_InsdID,
                        method: 'POST',
                        data: fileData,
                        headers: { 'Content-Type': undefined },
                        transformRequest: angular.identity
                    })
                    .then(function (resPhoto) {
                        console.log(resPhoto);
                        $scope.clear();
                        if (resPhoto.data.msg == 's') {
                            showSuccessToast("Saved Successfully");
                        }
                    }, function (errorPhoto) {
                        $scope.clear();
                    })
                }
                else {
                    $scope.clear();
                    showSuccessToast("Saved Successfully");
                }
            }
            else {
                $scope.clear();
                showSuccessToast("Failed! Try Again");
            }

        }, function (error) {
            $scope.clear();
        })

    }
 





    

    $scope.approveOrDiscardBroker = function (PK_BR_ID,ApprovedStatus) {

        $http.get('/SuperAdmin/approveOrDiscardBroker?PK_BR_ID='+PK_BR_ID+'&ApprovedStatus='+ApprovedStatus).then(function (response) {
            console.log(response);
            $scope.getBrokerRegisteration();

            if(response.data.msg=='s')
            {   
                showSuccessToast("User has been  " + response.data.ApprStatus);
            }
        }, function (error) {
        })

    }

    $scope.getState = function (r_Id) {
        $http.get('/Collaborator/getState').then(function (response) {
            console.log(response);
            $scope.listState = response.data.listState;

        }, function (error) {
        })
    }

    $scope.getDistricts_byStateId = function () {
        $http.get('/Collaborator/getDistricts_byStateId?FkState=' +$scope.i.FkState).then(function (response) {
            console.log(response);
            $scope.listDistricts = response.data.listDistricts;
        }, function (error) {
        })
    }
    $scope.getInsurers = function () {

        $http.get('/MISCFSR/getInsurers').then(function (response) {
            console.log(response);
            $scope.listIns = angular.copy(response.data.listIns);

        }, function (error) {
        })
    }
    
    $scope.listPolicy_Insurers = [];
    $scope.x.InsrID = 0;
    $scope.addIn_listPolicy_Insurers = function () {
        validationMsg = '';
        console.log($scope.x.PolicyNo);
        if ($scope.x.PolicyNo == '' || $scope.x.PolicyNo == undefined || $scope.x.PolicyNo == null || $scope.x.InsrID == 0)
        {
            if ($scope.x.PolicyNo == '' || $scope.x.PolicyNo == undefined || $scope.x.PolicyNo == null) {
                validationMsg += "<p>Please Enter Policy No</p>"
            }
            if ($scope.x.InsrID == 0)
            {
                validationMsg += "<p>Please Select Insurer</p>";
            }
            CommonService.showValidationPopup(validationMsg);
            
            return false;
        }
        else if ($scope.x.PolicyNo != '' && $scope.x.PolicyNo != undefined && $scope.x.PolicyNo != null && $scope.x.InsrID != 0)
        {
            $scope.listPolicy_Insurers.push($scope.x);
            $scope.x = {};
            $scope.x.InsrID = 0;
        }
    }

    $scope.aa = {};
    $scope.temp = [];
    $scope.addIn_temp = function () {
        
        $scope.aa.i = 1;
        $scope.temp.push($scope.aa);
        $scope.aa = {};
    }

    $scope.testing = function () {

        //if (window.FormData != undefined) {
        //    var form = new FormData();
        //    for (var i = 0; i < 3; ++i) {
        //        form.append('PolicyPhoto_Doc[]', $('PolicyPhoto_Doc[]').get(0).files[i]);
        //    }

        if (window.FormData != undefined) {
            var fileData = new FormData();
            $('#mygridBody1 tr').each(function(index,element){
                console.log($(this).find('input').attr('id'));
                var id = $(this).find('input').attr('id');
                if ($('#' + id).val() != null && $('#' + id).val() != undefined && $('#' + id).val() != '') {
                  
                    fileData.append($('#' + id).get(0).files[0].name, $('#' + id).get(0).files[0]);
                }
            })
            $http({
                url: '/Home/saveInsuredRegistrationPhoto_Doc?PK_InsdID=' + 1,
                method: 'POST',
                data: fileData,
                headers: { 'Content-Type': undefined },
                transformRequest: angular.identity
            });
        }
    }

    //var form = new FormData();
    //for (var i = 0; i < $(this).get(0).files.length; ++i) {
    //    form.append('userfiles[]', $(this).get(0).files[i]);
    //}

    //// send form data with ajax
    //$.ajax({
    //    url: 'url',
    //    type: "POST",
    //    data: form
    //});


    
    //$('#btnAddPolicyPhoto_Doc').click(function () {
    $('#mygridBody1').on('click', '#btnAddPolicyPhoto_Doc', function () {

        $(this).attr('id', 'btnDeletePolicyPhoto_Doc');
        $(this).find('img').attr('src', '/Images/Delete_icon.png');
        $(this).closest('tr').find('input').attr('name', 'PolicyPhoto_Doc[]');
        $(this).closest('tr').find('input').attr('id', i);
        i = i + 1;

        $('#mygridBody1').append('<tr> <td style="width:10000px;"><input type="file" /> </td><td  id="btnAddPolicyPhoto_Doc" style="cursor:pointer;"> <img src="/Images/addicon.png" style="width:25px; height:25px;" /></td></tr>');


    })
    $('#mygridBody1').on('click', '#btnDeletePolicyPhoto_Doc', function () {
        $(this).closest('tr').remove();
    })


    $scope.addValidation_onPolicyNo_Insurers = function () {
        if ($scope.i.InsuredType == 'Single Policy Holder') {
            $('#s_Pol').addClass("validate[required,'Please Enter Policy No']");
            $('#s_Ins').addClass("validate[required,'Please Select Insurers']");
        }
        else if ($scope.i.InsuredType == 'Multiple Policy Holder') {
            
            $('#s_Pol').removeClass();
            $('#s_Ins').removeClass();
        }
        //else {
        //    $('#rId').removeClass();
        //    $('#mn').removeClass();
        //}
    }
    $scope.clear = function () {
        $scope.i = {};
        $scope.listPolicy_Insurers = [];
        $scope.t = {};
        $scope.listBrokerBranch_EmailId = [];
        $('.multiple_emails-ul').html('');
        $('#AltEmail_Val').html('');
        i = 0;
        $('#mygridBody1').html('');
        $('#mygridBody1').append('<tr> <td style="width:10000px;"><input type="file" /> </td><td  id="btnAddPolicyPhoto_Doc" style="cursor:pointer;"> <img src="/Images/addicon.png" style="width:25px; height:25px;" /></td></tr>');

    }
    $scope.addValidation_onPolicyNo_Insurers();
    $scope.getState();
    $scope.getInsurers();
    //$scope.getBroker();
    //$scope.getBrokerRegisteration();
 
})