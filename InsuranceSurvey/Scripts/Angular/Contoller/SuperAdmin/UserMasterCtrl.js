﻿angular.module('myApp').controller('UserMasterCtrl', function ($scope, $http, CommonService) {
    
    $scope.u = {};
    var validationMsg = '';
   
    var i = 0;
    $scope.u = {};
    $scope.UserMasterValidation = function () {
        var x = $('#formUserMaster');
        var status = $(x).ValidationFunction();
        if (status == false) {
            return false;
        }
        if ($scope.u.Password != $scope.u.ConfirmPassword)
        {
            CommonService.showValidationPopup("<p>Password is Not Match! Please Enter Correct Password</p>")
            return false;
        }
        var i = 0;
        $http.post('/SuperAdmin/UserMasterValidation', { u: $scope.u }).then(function (response) {
            console.log(response);
            
            if (response.data.EmailStatus == 1) {
                validationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                i++;
            }
            if (response.data.MobileStatus == 1) {
                validationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                i++;
            }
            else if (response.data.UserNameStatus == 1) {
                validationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                i++;
            }
            
            if(i>0)
            {
                CommonService.showValidationPopup(validationMsg);
                return false;
            }
            else {
            
                $http.post('/SuperAdmin/saveUserMaster', { u: $scope.u }).then(function (response) {
                    console.log(response);
                    $scope.clear();
                    $scope.getUserMaster();
                    if(response.data.msg=='s')
                    {
                        showSuccessToast("Saved Successfully");
                    }
                    else
                    {
                        showSuccessToast("Failed!Try Again");
                    }
                }, function (error) {
                })
            }

        }, function (error) {
        })

    }


    $scope.getState = function () {
        $http.get('/Collaborator/getState').then(function (response) {
            console.log(response);
            $scope.listState = response.data.listState;

        }, function (error) {
        })
    }

    $scope.getDistricts_byStateId = function () {
        $http.get('/Collaborator/getDistricts_byStateId?FkState=' + $scope.u.StateCode).then(function (response) {
            console.log(response);
            $scope.listDistricts = response.data.listDistricts;
        }, function (error) {
        })
    }


    $scope.getRole = function () {
        $http.get('/SuperAdmin/getRole').then(function (response) {
            console.log(response);
            $scope.listRole = response.data.listRole;
        }, function (error) {
        })
    }
    $scope.clear = function () {
        $scope.u = {};
    }
    $scope.getUserMaster = function () {
        $http.get('/SuperAdmin/getUserMaster').then(function (response) {
            console.log(response);
            $scope.listUser = response.data.listUser;
        }, function (error) {
        })
    }
    $scope.rowClick=function()
    {
        $scope.u = angular.copy(this.u);
        $scope.getDistricts_byStateId();
    }

    $scope.getState();
    $scope.getRole();
    $scope.getUserMaster();

})