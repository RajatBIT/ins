﻿angular.module("myApp")
.factory('CommonService', function () {
    var fa = {};

    fa.convertDateStringToDateObj = function (dateString) {

        var dateParts = dateString.split("-");

        return new Date(dateParts[2], dateParts[1] - 1, dateParts[0]); // month is 0-based
    }

    fa.convertDateStringToDateObj1 = function (dateString) {
        alert(dateString);
        return DateTime = new Date(Date.parse(dateString));
    }

    fa.showValidationPopup = function (validationMsg) {

        var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ErrorMes\"></div><a class=\"close\" href=\"#close\"></a></div>");
        $(div).appendTo("body");
        $("#ErrorMes").empty();
        $("#ErrorMes").append(validationMsg);

        window.location = "#popup";
    }
    return fa;

})
