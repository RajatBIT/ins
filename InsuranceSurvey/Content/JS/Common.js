﻿

var ajaxLink = {
    DistBind: "/Collaborator/BindDist",
    SurveyUserExistance: "/Collaborator/CheckUserExistance",
    SurveyEmailExistance: "/Collaborator/CheckEmailExistance",
    SurveyCheckSLANO: "/Collaborator/CheckSLANO",
    SurveyCheckIniRefExistance: "/Collaborator/CheckIniRefExistance",
    ViewSurveyProfile: "/Collaborator/ViewSurveyorsDetails",
    ApproveOrDiscardSurveyor: "/Collaborator/ApproveOrDiscardSurveyor",
    MarineSubmitJIR: "/Collaborator/MarineSubmitJIR",
    MarineSubmitDamageDetails: "/Collaborator/MarineSubmitDamageDetails",
    MarineSubmitConsignmentDocs: "/Collaborator/MarineSubmitConsignmentDocs",
    MarineGetConsignmentDocs: "/Collaborator/MarineGetConsignmentDocs",
    MarineGetDescriptionConsignmentDocs: "/Collaborator/MarineGetDescriptionConsignmentDocs",
    MarineGetDamageDetails: "/Collaborator/MarineGetDamageDetails",
    BindMarineRemarksMaster: "/Collaborator/BindMarineRemarksMaster",
    InsertMarineRemark: "/Collaborator/InsertMarineRemark",
    BindMarineLORMaster: "/Collaborator/BindMarineLORMaster",
    InsertMarineLOR: "/Collaborator/InsertMarineLOR",
    GetConsignmentType: "/Collaborator/GetConsignmentType",
    MarineDeleteConsignmentDocs: "/Collaborator/MarineDeleteConsignmentDocs",
    MarineDeleteDamageDetails: "/Collaborator/MarineDeleteDamageDetails",
    MarineGetDescriptionConsignmentDocsEdit: "/Collaborator/MarineGetDescriptionConsignmentDocsEdit",
    MarineTranshipment: "/Collaborator/MarineTranshipment",
    ActionWithMarineTranshipment: "/Collaborator/ActionWithMarineTranshipment",
    GetMarineTranshipment: "/Collaborator/GetMarineTranshipment",
    GetSingelMarineTranshipment: "/Collaborator/GetSingelMarineTranshipment",
    AddClaimForm: "/Collaborator/AddClaimForm",
    ActionSurveyVisits: "/Collaborator/ActionSurveyVisits",
    GetSurveyVisits: "/Collaborator/GetSurveyVisits",
    ApproveOrDiscardInsured: "/Collaborator/ApproveOrDiscardInsured",
    AddMarineAssesment: "/Collaborator/AddMarineAssesment",
    ApproveOrDiscardTrainee: "/Collaborator/ApproveOrDiscardTrainee",
    ViewTraineeProfile: "/Collaborator/ViewTraineeDetails",
    SubmitFSR: "/Collaborator/SubmitFSR",
    TraineeRequestToSurvery: "/Collaborator/TraineeRequestToSurvery",
    ApproveOrDiscardCorporateInsured: "/Collaborator/ApproveOrDiscardCorporateInsured",
    ViewCorporateInsuredProfile: "/Collaborator/ViewCorporateInsuredProfile",
    BindApproveSurveyorsDetails: "/Collaborator/BindApproveSurveyorsDetails",
    TraineeAssginTask: "/Collaborator/TraineeAssginTask",
    //Motor Work
    BindMotorRemarksMaster: "/Collaborator/BindMotorRemarksMaster",
    InsertMotorRemark: "/Collaborator/InsertMotorRemark",
    BindConsignmentType: "/Collaborator/BindConsignmentType",
    InsertMotorParticularRemark: "/Collaborator/InsertMotorParticularRemark",
};

function openNew() {
    window.location = "/Survery/MarineJIR";
}

function createGuid() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        var r = Math.random() * 16 | 0, v = c === 'x' ? r : (r & 0x3 | 0x8);
        return v.toString(16);
    });
}

$(document).on("change", "#drpState", function () {
    var sateCode = $("#drpState option:selected").val();
    var result = new CommonClass()
    result.Bind(sateCode, "#drpDistrict");

});

$(document).on("change", "#drpdepatment", function () {
    if (this.value == "3") {
        $("#lblVRegNo").show();
        $("#txtVRegNo").show();
        $("#divSurveyType").show();
    }
    else {
        $("#lblVRegNo").hide();
        $("#txtVRegNo").hide();
        $("#divSurveyType").hide();
    }

});

/*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*
Author:- Naveen Malik (13-Sep-2015)
This Class help to bind a districts list (It is common class for bind the district list)
This class having one function BindDistricts with two parameters 
1) statecode
2) controlId
*/
var CommonClass = function () {
    this.Bind = function 
         BindDistricts(statecode, controlId) {
        $.ajax({
            type: "POST",
            url: ajaxLink.DistBind,
            data: "{'stateCode':'" + statecode + "'}",
            typeData: "json",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $(controlId).empty();
                $(controlId).append(data);
            }
        });
    }
};

/*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*
Author:- Naveen Malik (13-Sep-2015)
Calander function use for calander
*/
function calander(id) {
    javascript: NewCssCal(id, 'MMddyyyy', '', '', '', '', '');
}

/*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~
This functions are use for searching value in table 
*/

$(document).on("click", "#txtSearch", function () {
    $(this).attr("placeholder", "");
});

$(document).on("blur", "#txtSearch", function () {
    if ($(this).val().replace(/\s/g, '') == "") {
        $(this).attr("placeholder", "Search");
    }
});

$(document).on("keyup", "#txtSearch", function () {
    searchTable($(this).val());
});

$(document).on("click", "#btnReset", function () {
    $("#txtSearch").val("");
    searchTable("");
});

function searchTable(inputVal) {
    var table = $('#mygrid');
    table.find('tr').each(function (index, row) {
        var allCells = $(row).find('td');
        if (allCells.length > 0) {
            var found = false;
            allCells.each(function (index, td) {
                var regExp = new RegExp(inputVal, 'i');
                if (regExp.test($(td).text())) {
                    found = true;
                    return false;
                }
            });
            if (found == true) {
                $(row).show();
            }
            else {
                $(row).hide();
            }
        }
    });
}

/*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~
This functions are use for searching value in table 
*/

//$(document).on("blur", "#txtUserName", function () {
//    CheckUserExistance("SU", this);
//});

//$(document).on("blur", "#txtEmailId", function () {
//    CheckEmailExistance("SU", this);
//});

//$(document).on("blur", "#txtIslaNo", function () {
//    CheckSLANO(this);
//});

//$(document).on("blur", "#txtIniRef", function () {
//    CheckIniRefExistance(this);
//});

function CheckUserExistance(userType, txtUserName, control) {
    var userName = $("#" + txtUserName).val();
    $.ajax({
        type: "POST",
        url: ajaxLink.SurveyUserExistance,
        data: { "UserType": userType, "UserName": userName },
        dataType: "json",
        //  contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result == "Y")
                $(control).attr("src", "/images/right_filter.png");
            else
                $(control).attr("src", "/images/wrong_filter.png");
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function CheckEmailExistance(userType, txtEmailId, control) {
    var emailId = $("#" + txtEmailId).val();
    $.ajax({
        type: "POST",
        url: ajaxLink.SurveyEmailExistance,
        data: { "UserType": userType, "EmailId": emailId },
        dataType: "json",
        //  contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result == "Y")
                $(control).attr("src", "/images/right_filter.png");
            else
                $(control).attr("src", "/images/wrong_filter.png");
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function CheckSLANO(txtSlaNo, control) {
    var SlaNo = $("#" + txtSlaNo).val();
    $.ajax({
        type: "POST",
        url: ajaxLink.SurveyCheckSLANO,
        data: { "SlaNo": SlaNo },
        dataType: "json",
        //  contentType: "application/json; charset=utf-8",
        success: function (result) {
            if (result == "Y")
                $(control).attr("src", "/images/right_filter.png");
            else
                $(control).attr("src", "/images/wrong_filter.png");
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function CheckIniRefExistance(txtIniRef, control) {
    var iniRef = $("#" + txtIniRef).val();
    $.ajax({
        type: "POST",
        url: ajaxLink.SurveyCheckIniRefExistance,
        data: { "IniRef": iniRef },
        dataType: "json",
        success: function (result) {
            if (result == "Y")
                $(control).attr("src", "/images/right_filter.png");
            else
                $(control).attr("src", "/images/wrong_filter.png");
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function SurveryorView(PkSurId) {
    var htmlTable = "";
    $.ajax({
        type: "POST",
        url: ajaxLink.ViewSurveyProfile,
        data: { "FkSurId": PkSurId },
        dataType: "json",
        success: function (result) {
            htmlTable = "<div class=\"grid_body\"><table id='mygrid'><tr><td><b>First Name</b></td><td>" + result[0].FirstName + " </td>";
            htmlTable += "<td><b>Last Name</b></td><td>" + result[0].LastName + "</td></tr><tr><td><b>User Name</b></td><td>" + result[0].UserName + "</td><td><b>Mobile</b></td><td>" + result[0].Mobile + "</td></tr>";
            htmlTable += "<tr><td><b>State</b></td><td>" + result[0].StateName + " </td><td><b>District</b></td><td>" + result[0].DistrictName + "</td></tr>";
            htmlTable += "<tr><td><b>Membership</b></td><td>" + result[0].MemberShip + "</td><td><b>Alternative Mobile</b></td><td>" + result[0].AltMobile + "</td></tr>";
            htmlTable += "<tr><td><b>Address</b></td><td>" + result[0].Address + "</td><td><b>Pin code</b></td><td>" + result[0].Pincode + "</td></tr>";
            htmlTable += "<tr><td><b>Pan No</b></td><td>" + result[0].PANNO + "</td><td><b>Stax No</b></td><td>" + result[0].StaxNo + "</td></tr>";
            htmlTable += "<tr><td><b>SLA No</b></td><td>" + result[0].SLANo + "</td><td><b>Valid Upto</b></td><td>" + result[0].ValidUpto + "</td></tr>";
            htmlTable += "<tr><td><b>Email</b></td><td>" + result[0].Email + "</td><td><b>Alternative Email</b></td><td>" + result[0].AltEmail + "</td></tr>";
            htmlTable += "<tr><td><b>IIISLA NO</b></td><td>" + result[0].ISLANO + "</td><td><b>Ini Ref</b></td><td>" + result[0].IniRef + "</td></tr>";
            htmlTable += "<tr><td><b>Experience</b></td><td>" + result[0].Experience + "</td><td><b>Any Other</b></td><td>" + result[0].AnyOther + "</td></tr></table></div>";
            var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ViewSurveyProfile\"></div><a class=\"close\" href=\"#close\"></a></div>");
            $(div).appendTo("body");
            $("#ViewSurveyProfile").empty();
            $("#ViewSurveyProfile").append(htmlTable);
            window.location = "#popup";
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function getInsuredDetails_byPK_InsdID(pk_InsdID) {
    var htmlTable = "";
    $.ajax({
        type: "POST",
        url: '/Collaborator/getInsuredDetails_byPK_InsdID',
        data: { "pk_InsdID": pk_InsdID },
        dataType: "json",
        success: function (result) {
            console.log(result);
            htmlTable = "<div class=\"grid_body\"><table id='mygrid'><tr><td><b>First Name</b></td><td>" + result[0].Name + " </td><td><b>User Name</b></td><td>" + result[0].UserName + "</td></tr>";
            htmlTable += "<tr><td><b>Mobile</b></td><td>" + result[0].Mobile + "</td><td><b>GST IN</b></td><td>" + result[0].GSTIN + "</td></tr>";
            htmlTable += "<tr><td><b>State</b></td><td>" + result[0].StateName + " </td><td><b>District</b></td><td>" + result[0].DistrictName + "</td></tr>";
            htmlTable += "<tr><td><b>Address</b></td><td>" + result[0].Address + "</td><td><b>Pin code</b></td><td>" + result[0].Pincode + "</td></tr>";
            htmlTable += "<tr><td><b>Pan No</b></td><td>" + result[0].PANNO + "</td><td><b>Stax No</b></td><td>" + result[0].StaxNo + "</td></tr>";
            var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ViewInsuredProfile\"></div><a class=\"close\" href=\"#close\"></a></div>");
            $(div).appendTo("body");
            $("#ViewInsuredProfile").empty();
            $("#ViewInsuredProfile").append(htmlTable);
            window.location = "#popup";
        },
        error: function (result) {
            alert("Error");
        }
    });
}









function ApproveOrDiscardSurveyor(FKSurId, control, imgId) {
    var tr = $(control).closest("tr");
    var ApproveStatus = "";
    if (tr.find("td").eq(6).html() == "Pending") {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }
    else if (tr.find("td").eq(6).html() == "Approved") {
        ApproveStatus = "Discarded";
        $("#" + imgId).attr("src", "/Images/discarded.png");
    }
    else {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }


    $.ajax({
        type: "POST",
        url: ajaxLink.ApproveOrDiscardSurveyor,
        data: { "ApproveStatus": ApproveStatus, "FKSurId": FKSurId },
        dataType: "json",
        success: function (result) {
            if (ApproveStatus == "Approved") {
                showSuccessToast("User has been approved successfully");
                tr.find("td").eq(6).html("Approved");
                // $(control).hide();
                //tr.find("td").find("#aApprove").show();
                //tr.find("td").find("#aDiscarded").hide();
            }
            else if (ApproveStatus == "Discarded") {
                showSuccessToast("User has been discarded successfully");
                //$(control).hide();
                tr.find("td").eq(6).html("Discarded");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
};

$(document).on("click", "#btnNextMaineJir", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var Pk_JIRId = $(this).attr("data-content");
        if (Pk_JIRId == "")
            Pk_JIRId = "0";
        var ex = '';
        $('#divExConditionPacking input:checked').each(function () {
            ex += ($(this).val()) + ',';
        });

        if (ex.length > 0) {
            ex = ex.slice(0, -1);
        }

        var rbFkConsignmentType = "";
        $('input[name=chJIRGroup]:checked').each(function (index, item) {
            rbFkConsignmentType += $(item).val() + ",";
        });

        //debugger;

        var hdFKClaimId = $("#hdFKClaimId").val();
        var txtDateOfSurvey = $("#txtDateOfSurvey").val();
        var txtPlaceOfSurvey = $("#txtPlaceOfSurvey").val();
        //var rbConsiment = $("#Consiment").val();
        var rbConsiment = '';
        $('#divConType input:checked').each(function () {
            rbConsiment = $(this).val();

        });

        var rbSpotSurvey = "";
        $('#divSpSrvy input:checked').each(function () {
            rbSpotSurvey = $(this).val();

        });
        var rbPoliceReport = "";
        $('#divPolRpt input:checked').each(function () {
            rbPoliceReport = $(this).val();
        });
        var txtInsured = $("#Insured").val();
        var txtInsuredAddress = $("#InsuredAddress").val();
        var txtConsignor = $("#Consignor").val();
        var txtConsignee = $("#Consignee").val();
        var txtArrivalDateDestination = $("#txtArrivalDateDestination").val();
        var txtCarryingLorryRegNo = $("#CarryingLorryRegNo").val();
        var txtDispatchedFrom = $("#DispatchedFrom").val();
        var txtDispatchedTo = $("#DispatchedTo").val();
        var txtDeliveryDateConsignee = $("#txtDeliveryDateConsignee").val();
        var txtConsignmentWeight = $("#ConsignmentWeight").val();
        var txtDescriptionPacking = $("#DescriptionPacking").val();
        var txtExConditionPacking = ex;
        var txtNatureOfTransit = $("#NatureOfTransit").val();
        var txtInsdType = $("#hdInsdType").val();
        var txtInsdId = $("#hdFKInsdId").val();
        var txtConsignorAddress = $("#ConsignorAddress").val();
        var txtConsigeeAddress = $("#ConsigeeAddress").val();
        var txtCarryingLorryGVW = $("#CarryingLorryGVW").val();
        var txtCarryingLorryULW = $("#CarryingLorryULW").val();
        var txtCauseofLoss = $("#CauseOfLoss").val();
        debugger;
        var txtDelayInIntimation = $("#DelayInIntimation").val();
        var txtDelayIntakingDelivery = $("#DelayIntakingDelivery").val();
        var txtResoneForDelay = $("#ResoneForDelay").val();
        var txtTask = $("#hdtask").val();
        var txtConsignmentTransshipedTo = $("#ConsignmentTransshipedTo").val();
        var txtLoadCapacityOfCarryVehicle = $("#LoadCapacityOfCarryVehicle").val();
        //var rbLoadCarVeh = $("#LoadCarVeh").val();
        var rbLoadCarVeh = $("input[name=LoadVar]:checked").val();
        var rbWhoIsInsured = $("input[name=CCC]:checked").val();
        var rbTypeOfContract = $("input[name=AR]:checked").val();
        var IncotermSale = $("#IncotermSale").val();
        var PointDeliveryConsignee = $("#PointDeliveryConsignee").val();
        var CauseOfLossOther = $('#CauseOfLossOther').val();
        var ExConditionPackingOther = $('#ExConditionPackingOther').val();
        var txtPlaceOfAccident = $('#PlaceOfAccident').val();
        var txtInsuredClaimNo = $('#InsuredClaimNo').val();
        var objProperty = {
            PkJIRId: Pk_JIRId,
            DateOfSurvey: txtDateOfSurvey,
            Consiment: rbConsiment,
            Insured: txtInsured,
            InsuredAddress: txtInsuredAddress,
            Consignor: txtConsignor,
            Consignee: txtConsignee,
            ArrivalDateDestination: txtArrivalDateDestination,
            CarryingLorryRegNo: txtCarryingLorryRegNo,
            DispatchedFrom: txtDispatchedFrom,
            DispatchedTo: txtDispatchedTo,
            DeliveryDateConsignee: txtDeliveryDateConsignee,
            ConsignmentWeight: txtConsignmentWeight,
            DescriptionPacking: txtDescriptionPacking,
            ExConditionPacking: txtExConditionPacking,
            InsdType: txtInsdType,
            InsdId: txtInsdId,
            FKClaimId: hdFKClaimId,
            FkConsignmentType: rbFkConsignmentType,
            NatureOfTransit: txtNatureOfTransit,
            ConsignorAddress: txtConsignorAddress,
            ConsigeeAddress: txtConsigeeAddress,
            CarryingLorryGVW: txtCarryingLorryGVW,
            CarryingLorryULW: txtCarryingLorryULW,
            CauseOfLoss: txtCauseofLoss,
            DelayInIntimation: txtDelayInIntimation,
            DelayIntakingDelivery: txtDelayIntakingDelivery,
            ResoneForDelay: txtResoneForDelay,
            PlaceofSurvey: txtPlaceOfSurvey,
            ConsignmentTransshipedTo: txtConsignmentTransshipedTo,
            LoadCapacityOfCarryVehicle: txtLoadCapacityOfCarryVehicle,
            LoadOnCarryingVehicle: rbLoadCarVeh,
            WhoIsInsured: rbWhoIsInsured,
            TypeOfContract: rbTypeOfContract,
            IncotermSale: IncotermSale,
            PointDeliveryConsignee: PointDeliveryConsignee,
            CauseOfLossOther: CauseOfLossOther,
            ExConditionPackingOther: ExConditionPackingOther
            , PlaceOfAccident: txtPlaceOfAccident,
            SpotSurvey: rbSpotSurvey,
            PoliceReport: rbPoliceReport,
            InsuredClaimNo: txtInsuredClaimNo
        };

        console.log(objProperty);

        $.ajax({
            type: "POST",
            url: ajaxLink.MarineSubmitJIR,
            data: JSON.stringify(objProperty),// { "objProperty": objProperty },
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                debugger;
                var url = window.location.href;
                if (txtInsdType = "One Time")
                    txtInsdType = "O";
                debugger;
                if (result == "I") {

                    if (url.indexOf("MarineJIRConsignment") > 1) {
                        if (txtTask.toLowerCase() == "FSR".toLowerCase() || txtTask.toLowerCase() == "claim".toLowerCase()) {
                            $("#fkTranshipMentClaimId").val(hdFKClaimId);
                            $("#MarineTranshipmentTask").val(txtTask);
                            //alert('submit but')
                            //$("#form_MarineTranshipment").submit();
                            window.location = '/Survery/MarineTranshipment?fkClaimId=' + hdFKClaimId + '&task=' + txtTask;

                        } else {
                            $("#fkClaimId").val(hdFKClaimId);
                            $("#task").val("FSR");
                            //$("#form_MarineJIR").submit();
                            window.location = '/Survery/MarineJIR?fkClaimId=' + hdFKClaimId + '&task=' + txtTask;
                        }
                    }
                    else {
                        window.location = '/Survery/MarineConsignmentDocs?fkClaimId=' + hdFKClaimId + '&task=' + txtTask + '&ConsimentType=' + rbConsiment;
                        //SendToConsignmentDocs(hdFKClaimId, txtTask, rbConsiment);
                    }
                }
                else if (result == "U") {

                    if (url.indexOf("MarineJIRConsignment") > 1) {
                        if (txtTask.toLowerCase() == "FSR".toLowerCase() || txtTask.toLowerCase() == "claim".toLowerCase()) {
                            $("#fkTranshipMentClaimId").val(hdFKClaimId);
                            $("#MarineTranshipmentTask").val(txtTask);
                            //$("#form_MarineTranshipment").submit();
                            window.location = '/Survery/MarineTranshipment?fkClaimId=' + hdFKClaimId + '&task=' + txtTask;
                        } else {
                            $("#fkClaimId").val(hdFKClaimId);
                            $("#task").val("FSR");
                            window.location = '/Survery/MarineJIR?fkClaimId=' + hdFKClaimId + '&task=' + txtTask;
                            //$("#form_MarineJIR").submit();
                        }
                    } else {
                        window.location = '/Survery/MarineConsignmentDocs?fkClaimId=' + hdFKClaimId + '&task=' + txtTask + '&ConsimentType=' + rbConsiment;
                        //SendToConsignmentDocs(hdFKClaimId, txtTask, rbConsiment);
                    }
                }
                else {
                    alert("Error");

                }
            },
            error: function (result) {
                console.log("Error");
                console.log(result);

                alert("Error");
            }
        });
    } else {
        return false;
    }
});


function SendToConsignmentDocs(fkClaimId, task, rbFkConsignmentType) {
    $("#fkMarineConsignmentDocs_ClaimId").val(fkClaimId);
    $("#fkMarineConsignmentDocs_Task").val(task);
    $("#fkMarineConsignmentDocs_ConsimentType").val(rbFkConsignmentType);
    $("#form_MarineConsignmentDocs").submit();
}


$(document).on("click", "#btnSubmitMarineConsignmentDocs", function () {
    var x = $("#form2");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var PkId = $(this).attr("data-content");
        if (PkId == "")
            PkId = "0";

        var txtFKClaimId = $("#hdFKClaimId").val();
        var txtInvNo = $("#InvNo").val();
        var txtDate = $("#txtInvDate").val();
        var txtInvQty = $("#InvQty").val();
        var txtDescription = $("#Description").val();
        var txtUom = $("#Uom").val();
        var txtGRNo = $("#GRNo").val();
        var txtGRDate = $("#txtGNDate").val();
        var txtBLNo = $("#BLNo").val();
        var txtBLDate = $("#txtBLDate").val();
        var txtBENo = $("#BENo").val();
        var txtBEDate = $("#txtBEDate").val();
        var txtAWBNo = $("#AWBNo").val();
        var txtAWBDate = $("#txtAWBDate").val();
        var txtRRNo = $("#RRNo").val();
        var txtRRDate = $("#txtRRDate").val();

        var objProperty = {
            PKId: PkId,
            FKClaimId: txtFKClaimId,
            InvNo: txtInvNo,
            InvDate: txtDate,
            InvQty: txtInvQty,
            Uom: txtUom,
            Description: txtDescription,
            GRNo: txtGRNo,
            GRDate: txtGRDate,
            BLNo: txtBLNo,
            BLDate: txtBLDate,
            BENo: txtBENo,
            BEDate: txtBEDate,
            AWBNo: txtAWBNo,
            AWBDate: txtAWBDate,
            RRNo: txtRRNo,
            RRDate: txtRRDate
        };
        $.ajax({
            type: "POST",
            url: ajaxLink.MarineSubmitConsignmentDocs,
            data: JSON.stringify(objProperty),
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result == "I") {
                    //alert("Damage Detail Submitted Successfully");
                    MarineGetConsignmentDocs(txtFKClaimId);
                    clearDoc();
                    showSuccessToast("Consignment Information Submitted Successfully.");
                }
                else if (result == "U") {
                    //alert("Damage Detail Updated Successfully")
                    MarineGetConsignmentDocs(txtFKClaimId);
                    clearDoc();
                    showSuccessToast("Damage Detail Updated Successfully.");
                }
                else {
                    alert("Error");
                }

                MarineGetConsignmentDocs(txtFKClaimId);
            },
            error: function (result) {
                alert("Error");
            }
        });
    } else {
        return false;
    }
});

function clearDoc() {
    //$("#InvNo").val("");
    //$("#txtInvDate").val("");
    $("#InvQty").val("");
    //$("#txtTypeDate").val("");
    $("#Description").val("");
    //$("#TypeNo").val("");
    $("#Uom").val("");
    //$("#GRNo").val("");
    //$("#txtGNDate").val("");
    //$("#BLNo").val("");
    //$("#txtBLDate").val("");
    //$("#BENo").val("");
    //$("#txtBEDate").val("");
    //$("#AWBNo").val("");
    //$("#txtAWBDate").val("");
    //$("#RRNo").val("");
    //$("#txtRRDate").val("");
    $("#btnSubmitMarineConsignmentDocs").val("Submit");
    $("#btnSubmitMarineConsignmentDocs").attr("data-content", 0);
}

function MarineGetConsignmentDocs(FKClaimId) {
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineGetConsignmentDocs,
        data: { "FKClaimId": FKClaimId },
        dataType: "json",
        success: function (result) {
            var row = "";// "<thead><tr><th>Sr No.</th><th>Descriptions</th><th>Inv No</th><th>Inv Date</th><th>Inv Qty</th><th>Type Date</th><th>UOM</th><th>Type No</th><th>Action</th></tr></thead>";
            row = result;
            $("#tblDoc").empty();
            $("#tblDoc").append(row);
            if ($("#tblDoc tr").length == 1)
                $("#btnNextDocs").attr("style", "display:none");
            else
                $("#btnNextDocs").attr("style", "display:block");
        },
        error: function (result) {
            alert("Error");
        }
    });

    $("#tr_GNNO").hide();
    $("#tr_BLNo").hide();
    $("#tr_BENo").hide();
    $("#tr_AWBNo").hide();
    $("#tr_RR").hide();

    $.ajax({
        type: "POST",
        url: ajaxLink.GetConsignmentType,
        data: { "FKClaimId": FKClaimId },
        dataType: "json",
        success: function (result) {
            $("#GRNo").removeClass();
            $("#txtGNDate").removeClass();
            $("#BLNo").removeClass();
            $("#txtBLDate").removeClass();
            $("#BENo").removeClass();
            $("#txtBEDate").removeClass();
            $("#AWBNo").removeClass();
            $("#txtAWBDate").removeClass();
            $("#RRNo").removeClass();
            $("#txtRRDate").removeClass();

            for (var k = 0; k < result.length; k++) {
                var ConsignmentType = result[k].ConsignmentType;
                if (result[k].Pk_Id != "0") {
                    switch (ConsignmentType) {
                        case 'GR/LR':
                            $("#tr_GNNO").show();
                            $("#GRNo").addClass("validate[required,'Please Enter gr no'] Txtbox");
                            $("#txtGNDate").addClass("validate[required,'Please Enter gn date'] Txtbox");
                            break;
                        case 'BL':
                            $("#tr_BLNo").show();
                            $("#BLNo").addClass("validate[required,'Please Enter BL no'] Txtbox");
                            $("#txtBLDate").addClass("validate[required,'Please Enter BL date'] Txtbox");
                            break;
                        case 'BE':
                            $("#tr_BENo").show();
                            $("#BENo").addClass("validate[required,'Please Enter BE no'] Txtbox displaytableRow");
                            $("#txtBEDate").addClass("validate[required,'Please Enter BE date'] Txtbox displaytableRow");
                            break;
                        case 'AWB':
                            $("#tr_AWBNo").show();
                            $("#AWBNo").addClass("validate[required,'Please Enter AWB No'] Txtbox");
                            $("#txtAWBDate").addClass("validate[required,'Please Enter AWB Date'] Txtbox");
                            break;
                        case 'RR':
                            $("#tr_RR").show();
                            $("#RRNo").addClass("validate[required,'Please Enter RR'] Txtbox");
                            $("#txtRRDate").addClass("validate[required,'Please Enter RR date'] Txtbox");
                            break;
                    }
                }
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
}

$(document).on("click", ".EditMarineGetConsignmentDocs", function () {
    var id = $(this).attr("data-content");
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineGetDescriptionConsignmentDocsEdit,
        data: { "pkId": id },
        dataType: "json",
        //contentType: "application/json",
        success: function (result) {
            $("#InvNo").val(result[0].InvNo);
            $("#txtInvDate").val(result[0].InvDate);
            $("#InvQty").val(result[0].InvQty);
            $("#txtTypeDate").val(result[0].UOM);
            $("#Description").val(result[0].Description);
            $("#Uom").val(result[0].UOM);
            $("#GRNo").val(result[0].GRNo);
            $("#txtGNDate").val(result[0].GRDate);
            $("#BLNo").val(result[0].BLNo);
            $("#txtBLDate").val(result[0].BLDate);
            $("#BENo").val(result[0].BENo);
            $("#txtBEDate").val(result[0].BEDate);
            $("#AWBNo").val(result[0].AWBNo);
            $("#txtAWBDate").val(result[0].AWBDate);
            $("#RRNo").val(result[0].RRNo);
            $("#txtRRDate").val(result[0].RRDate);
        }, error: function (result) {
            alert("Error");
        }
    });

    $("#btnSubmitMarineConsignmentDocs").val("Update");
    $("#btnSubmitMarineConsignmentDocs").attr("data-content", id);
});

$(document).on("click", "#btnSubmitMarineDamageDetails", function () {
    var x = $("#form3");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var id = $(this).attr("data-content");

        if (id == "" && id == "undefined")
            id = "0";

        var txtFKClaimId = $("#hdFKClaimId").val();
        var drpDestination = $("#drpDestination").val();
        var txtAffectedQuantity = $("#AffectedQuantity").val();
        var txtRemark = $("#Remark").val();
        var txtRemarkOther = $("#RemarkOther").val();
        var objProperty = {
            PkId: id,
            FKClaimId: txtFKClaimId,
            FKDesId: drpDestination,
            AffectedQuantity: txtAffectedQuantity,
            Remark: txtRemark,
            RemarkOther: txtRemarkOther

        };

        $.ajax({
            type: "POST",
            url: ajaxLink.MarineSubmitDamageDetails,
            data: JSON.stringify(objProperty),
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result == "I") {
                    $('#divRO').hide();
                    //   alert("Damage Detail Submitted Successfully");
                    MarineGetDamageDetails(txtFKClaimId);
                    showSuccessToast("Damage Detail Submitted Successfully.<br/>");
                }
                else if (result == "U") {
                    $('#divRO').hide();
                    //alert("Damage Detail Updated Successfully");
                    MarineGetDamageDetails(txtFKClaimId);
                    showSuccessToast("Damage Detail Updated Successfully.<br/>");
                }
                else {
                    $('#divRO').hide();
                    alert("Error");
                }
                $("#btnSubmitMarineDamageDetails").attr("data-content", "0");
                $("#btnSubmitMarineDamageDetails").val("Submit");
                $("#drpDestination").show();
                $("#spDestination").html("");

                $("#drpDestination").addClass("validate[required,'Please select description'] Drp");

                MarineGetDamageDetails(txtFKClaimId);
            },
            error: function (result) {
                alert("Error");
            }
        });
    } else {
        return false;
    }
});

$(document).on("click", ".EditMarineGetConsignmentDetails", function () {
    var id = $(this).attr("data-content");
    var row = $(this).closest("tr");// $("tblDetails")

    var AffectedQuantity = row.children().eq(6).html();
    var Remark = row.children().eq(7).html();
    var Description = row.children().eq(5).html();
    var RemarkOther = row.children().eq(8).html();
    if (Remark == "Other") {
        $('#divRO').show();
    }

    $("#spDestination").html(Description);
    $("#AffectedQuantity").val(AffectedQuantity);
    $("#Remark").val(Remark);
    $("#drpDestination").hide();
    $("#drpDestination").removeAttr("class");
    $("#btnSubmitMarineDamageDetails").val("Update");
    $("#btnSubmitMarineDamageDetails").attr("data-content", id);
    $('#RemarkOther').val(RemarkOther);

})

function BindDestination(FKClaimId) {
    console.log('fkid' + FKClaimId);
    //debugger;
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineGetDescriptionConsignmentDocs,
        data: { "fkClaimId": FKClaimId },
        dataType: "json",
        async: true,
        success: function (result) {
            console.log(result);
            var row = "<option value='Select'>Select</option>";
            row += result;
           
            $("#drpDestination").html("");
            $("#drpDestination").append(row);
            console.log($("#drpDestination").html());
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function MarineGetDamageDetails(FKClaimId) {
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineGetDamageDetails,
        data: { "fkClaimId": FKClaimId },
        dataType: "json",
        success: function (result) {
            var row = "<thead><tr><th>Sr No.</th><th>Inv No</th><th>Inv Date</th><th>Inv Qty</th><th>UOM</th><th>Description</th><th>Affected Quantity</th><th>Remarks</th><th>Action</th></tr></thead>";
            row += result;
            $("#tblDetails").empty();
            $("#tblDetails").append(row);
            clearDamageDetails();
        },
        error: function (result) {
            alert("Error");
        }
    });
    BindDestination(FKClaimId);
}

function clearDamageDetails() {
    console.log('aa');
    //$("#drpDestination").empty();
    $("#drpDestination").val('Select');
    $("#AffectedQuantity").val("");
    $("#Remark").val("");
}

function BindMarineRemarksMaster(fkClaimid) {
    $.ajax({
        type: "POST",
        url: ajaxLink.BindMarineRemarksMaster,
        data: { "fkClaimId": fkClaimid },
        dataType: "json",
        success: function (result) {
            //$(".tbllist #trCh").before().empty();
            $("#trCh").empty();
            $("#trCh").append(result);
        },
        error: function (result) {
            alert("Error");
        }
    });
}


//$(document).on("click", "#btnSaveRemarksClaim", function () {
//    alert("Hello");
//});


$(document).on("click", "#btnSaveRemarksClaim", function () {
    var x = $("#form4");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var listCheck = [];
        $("input[name='chMarineRemarks']:checked").each(function () {
            listCheck.push($(this).val());
        });
        var objProperty = {

            FKClaimId: $("#hdFKClaimId").val(),
            CauseOfLoss: $("#CauseOfLoss").val(),
            DelayInIntimation: $("#DelayInIntimation").val(),
            DelayIntakingDelivery: $("#DelayIntakingDelivery").val(),
            CheckBoxList: listCheck
        };

        $.ajax({
            type: "POST",
            url: ajaxLink.InsertMarineRemark,
            data: JSON.stringify(objProperty),
            dataType: "json",
            contentType: "application/json",
            async: true,
            success: function (result) {
                if (result == "I") {
                    //SendToNextLOR($("#hdFKClaimId").val(), $("#hdTask").val())
                    window.location = "/Survery/lornew?cid=" + $("#hdFKClaimId").val() + "&task=" + $("#hdTask").val();
                }
                else {
                    alert("Error");
                }
            },
            error: function (result) {
                alert(result);
            }
        });
    } else {
        return false;
    }
});
//$(document).on("click", "#btnSubmitLOR", function () {
//});



$(document).on("click", "#btnSubmitLOR", function () {
    var x = $("#form6");
    var status = $(x).ValidationFunction();
    debugger;
    if (status == true) {
        //var listCheck = [];
        //$("input[name='chLOR']:checked").each(function () {
        //    listCheck.push($(this).val());
        //});
        var LorXml = "<recodes>";
        $(".Drp").each(function () {
            LorXml += "<recode>";
            LorXml += "<LorType>" + $(this).val() + "</LorType>";
            LorXml += "<LorId>" + $(this).find('option:selected').attr("data-val") + "</LorId>";
            LorXml += "</recode>";
        });

        LorXml += "</recodes>";

        var txtLOR13 = $("#txtLOR13").val();
        var txtLOR14 = $("#txtLOR14").val();
        var task = $("#hdTask").val();
        var objProperty = {
            FKClaimId: $("#hdFKClaimId").val(),
            //  CheckBoxList: listCheck,
            xml: escape(LorXml),
            PlaceofSurvey: $("#MarineJIR_PlaceofSurvey").val(),
            LOR13: txtLOR13,
            LOR14: txtLOR14,
            ContactPerson: $("#txtContactPerson").val(),
            EmailId: $("#txtEmail").val(),
            MobileNumber: $("#txtMobile").val()
        };

        $.ajax({
            type: "POST",
            url: ajaxLink.InsertMarineLOR,
            data: JSON.stringify(objProperty),
            dataType: "json",
            contentType: "application/json",
            success: function (result) {
                if (result == "I") {
                    if (task == "JIR") {
                        $("#btnGenratePrview").attr("style", "display:block;");
                        showSuccessToast("Joint Inspaction Report Submitted Successfully.");
                    } else if (task == "ISVR") {
                        showSuccessToast("Instant Site Visit Report Submitted Successfully.");
                    } else if (task.toLowerCase() == "LOR".toLowerCase()) {
                        $("#btnGenratePrview").attr("style", "display:block;");
                        showSuccessToast("Letter of Requirment Submitted Successfully.");
                    } else {
                        showSuccessToast("Letter of Requirment Submitted Successfully.");
                    }
                }
                else {
                    alert("Error");
                }
            },
            error: function (result) {
                alert("Error");
            }
        });
    }
    else {
        return false;
    }
});



function BindMarineLORMaster(fkClaimid) {
    $.ajax({
        type: "POST",
        url: ajaxLink.BindMarineLORMaster,
        data: { "fkClaimId": fkClaimid },
        dataType: "json",
        success: function (result) {
            //$(".tbllist #tr_LorList").before().html("");
            // $(".tbllist #tr_LorList").before(result);
            $("#LotTr").empty();
            $("#LotTr").append(result);
        },
        error: function (result) {
            alert("Error");
        }
    });
}

$(document).on("click", ".deleteDetails", function () {
    if (confirm('Are you sure you wish to delete this record?')) {
        return MarineDeleteDamageDetails($(this).attr("data-content"));
    }
    else {
        return false;
    }
});

function MarineDeleteDamageDetails(pkId) {
    //if (confirm('Are you sure you wish to delete this record?')) {
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineDeleteDamageDetails,
        data: { "pkId": pkId },
        dataType: "json",
        success: function (result) {
            MarineGetDamageDetails($("#hdFKClaimId").val());
        },
        error: function (result) {
            alert("Error");
        }
    });
    //}
    //else {
    //    return false;
    //}
}

$(document).on("click", ".deleteDocs", function () {
    if (confirm('Are you sure you wish to delete this record?')) {
        MarineDeleteConsignmentDocs($(this).attr("data-content"));
    }
    else {
        return false;
    }
});

function MarineDeleteConsignmentDocs(pkId) {
    $.ajax({
        type: "POST",
        url: ajaxLink.MarineDeleteConsignmentDocs,
        data: { "pkId": pkId },
        dataType: "json",
        success: function (result) {
            MarineGetConsignmentDocs($("#hdFKClaimId").val());
        },
        error: function (result) {
            alert(result);
        }
    });
}

$(document).on("click", "#btnGenratePrviewISVR", function () {

    var claimId = $(this).attr("data-claimid");
    var InsdType = $(this).attr("data-insdtype");
    var InsdID = $(this).attr("data-insdid");
    var Task = $(this).attr("data-task");

    GenratePrview(claimId, InsdType, InsdID, Task);
});


$(document).on("click", "#btnGenratePrview", function () {
    debugger;
    var claimId = $(this).attr("data-claimid");
    var InsdType = $(this).attr("data-insdtype");
    var InsdID = $(this).attr("data-insdid");
    var Task = $(this).attr("data-task");
    GenratePrview(claimId, InsdType, InsdID, Task);
});

$(document).on("click", "#btnSendLormail", function () {
    debugger;
    var claimId = $(this).attr("data-claimid");
    var InsdType = $(this).attr("data-insdtype");
    var InsdID = $(this).attr("data-insdid");
    var Task = $(this).attr("data-task");
    GenratePrview(claimId, InsdType, InsdID, Task);
});

$(document).on("click", "#btnSendLORISVR", function () {
    debugger;
    var ClaimID = $(this).attr("data-claimid");
    var Task = $(this).attr("data-task");
    var InsdType = $(this).attr("data-insdtype");
    var InsdID = $(this).attr("data-insdid");
    $("#fkLORClaimId").val(ClaimID);
    $("#LORTask").val(Task);

    //$("#FkInsdID").val(InsdID);
    //$("#LORInsdType").val(InsdType);
    $("#form_LOR").submit();
});

function GenratePrview(ClaimID, InsdType, InsdID, Task) {
    //var ClaimID = $("#hdFKClaimId").val();
    //var InsdType = $("#hdInsdType").val();
    //var InsdID = $("#hdFKInsdId").val();
    //var Task = $("#hdtask").val();
    window.open("/Survery/GeneratePdf?ClaimID=" + ClaimID + "&InsdType=" + InsdType + "&InsdID=" + InsdID + "&Task=" + Task);
}


$(document).on("click", "#btnClaimForm", function () {
    debugger;
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var ClaimId = $(this).attr("data-content");
        AddClaimForm(ClaimId);
    } else {
        return false;
    }
});

function AddClaimForm(ClaimId) {
    debugger;
    var dtMarg = $("#ClaimDateFrom").val() + " To " + $("#ClaimDateTo").val();
    var task = $("#Task").val();
    var mc = '';
    $('#divModeConvey input:checked').each(function () {
        mc += ($(this).val()) + ',';

    });
    if (mc.length > 0) {
        mc = mc.slice(0, -1);
    }
    console.log($('#PolicyTypeText').val());
    var objProperty = {
        "FkClaimId": ClaimId,
        "Period": dtMarg,
        "TransitInsured": $("#TransitInsured").val(),
        //"ModeConveyance": $("#ModeConveyance").val(mc),
        "ModeConveyance": mc,
        "Coverage": $("#drpCoverage Option:selected").val(),
        "SubjectMatter": $("#SubjectMatter").val(),
        "BasisValution": $("#BasisValution").val(),
        "PolicyExcess": $("#PolicyExcess").val(),
        "ModeConveyanceOther": $('#ModeConveyanceOther').val(),
        "PolicyTypeText":$('#PolicyTypeText').val()
    }

    $.ajax({
        type: "POST",
        url: ajaxLink.AddClaimForm,
        data: JSON.stringify(objProperty),
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            if (result == "Y") {
                //if (task.toLowerCase() == "FSR".toLowerCase()) {
                //    $("#fkSurveyVisitsClaimId").val(ClaimId);
                //    $("#SurveyVisitsTask").val(task);
                //    $("#form4").submit();
                //} else {
                $("#fkTranshipMentClaimId").val(ClaimId);
                $("#MarineJIRConsignmentTask").val(task);
                //$("#form_MarineJIRConsignment").submit();
                window.location = "/Survery/MarineJIRConsignment?fkClaimId=" + ClaimId + "&task=" + task;
                ////                }
            }
        }, error: function (result) {
            alert(result);
        }
    });
};



$(document).on("click", "#btnTransportSave", function () {
    debugger;
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var PkId = $(this).attr("data-content");
        var task = "Insert";
        if (PkId != 0)
            task = "Update";
        var FKClaimId = $("#FKClaimId").val();
        var rbLegOfJourney = '';
        $('#divLegOfJourney input:checked').each(function () {
            rbLegOfJourney = $(this).val();
        });
        $.ajax({
            type: "Get",
            url: '/Collaborator/getLegOfJourney_byClaimId?claimId=' + FKClaimId,
            data: {},
            dataType: "json",
            async: true,
            success: function (result) {
                console.log(result);
                var legofJorney = result.legOfJourney;
                if (legofJorney != null && legofJorney != "" && legofJorney != undefined) {
                    if(legofJorney!=rbLegOfJourney)        
                    {
                        
                        alert("If You can Change Leg Of Journey ! Please Delete All Journey Details");
                        return false;
                    }
                    else if (legofJorney == rbLegOfJourney) {
                        ActionWithMarineTranshipment(PkId, task);
                    }
                }
                else {
                    ActionWithMarineTranshipment(PkId, task);
                }
            },
            error: function (result) {

            }
        })
        //ActionWithMarineTranshipment(PkId, task);

        $(this).attr("data-content", "0");
        var len = $(".divMultiple").length;
        if (len > 1) {
            //jQuery('input:radio[name="Transhipment"]').filter('[value="Multiple"]').prop('checked', true);
        }

    }
    else {
        return false;
    }
});


function ActionWithMarineTranshipment(pkId, task) {

    var Comment = '';
    $('#divComment input:checked').each(function () {
        Comment += ($(this).val()) + ',';
        console.log(Comment);
    });
    Comment = Comment.slice(0, -1);

    var FKClaimId = $("#FKClaimId").val();
    var mode = $('input[type=radio][name=TransportType]:checked').val();
    var LoadingPlace = $("#LoadingPlace").val();
    var UnLoadingPlace = $("#UnLoadingPlace").val();
    var DateArrival = $("#DateArrival").val();
    var DateDischarge = $("#DateDischarge").val();
    var NameOfCarrier = $("#NameOfCarrier").val();
    //var Comment = $("#Comment").val();
    //var Remark = $("#Remark").val();
    var Remark = $("input[name=rbRN]:checked").val();
    var TypeNo = $("#drpConsignmentType").val(); //$('input[type=radio][name=TransportType]:checked').val();
    var TypeDate = $("#TypeDate").val();
    debugger;

    var rbLegOfJourney = '';
    $('#divLegOfJourney input:checked').each(function () {
        rbLegOfJourney = $(this).val();
    });
    var objProperty = {
        "PkId": pkId,
        "FKClaimId": FKClaimId,
        "Mode": mode,
        "LoadingPlace": LoadingPlace,
        "UnLoadingPlace": UnLoadingPlace,
        "TypeNo": TypeNo,
        "TypeDate": TypeDate,
        "DateArrival": DateArrival,
        "DateDischarge": DateDischarge,
        "NameOfCarrier": NameOfCarrier,
        "Comment": Comment,
        "Remark": Remark,
        "Task": task,
        "LegOfJourney": rbLegOfJourney
    };

    $.ajax({
        type: "POST",
        url: ajaxLink.ActionWithMarineTranshipment,
        data: JSON.stringify(objProperty),
        dataType: "json",
        contentType: "application/json",
        async: true,
        success: function (res) {
            var result = res.flag;
            var pk_id = res.pk_id;
            /////rajat photo save 
            var cmtPhoto = '';
            if (window.FormData !== undefined) {
                //alert($("#PoliceReportPhoto").val());
                var fileData = new FormData();
                if ($("#cmtPhoto").val() != '') {
                    fileData.append($("#cmtPhoto").get(0).files[0].name, $("#cmtPhoto").get(0).files[0]);
                    cmtPhoto = "yes";
                }

                $.ajax({
                    url: '/Collaborator/saveCommentPhotoOrDoc?cmtPhoto=' + cmtPhoto + '&pk_id=' + pk_id + ' ',
                    type: "POST",
                    contentType: false, // Not to set any content header
                    processData: false, // Not to process data
                    data: fileData,
                    success: function (result) {

                        //alert(result);
                    },
                    error: function (err) {

                        //alert(err.statusText);
                    }
                });

            }


            console.log(result.flag);
            $('#divComment input:checked').each(function () {
                $(this).prop('checked', false);

            });
            $('#divRN input:checked').each(function () {
                $(this).prop('checked', false);

            });

            if (result == "I") {
                showSuccessToast("Marine Transhipment Submitted Successfully");
            }
            else if (result == "U") {
                showSuccessToast("Marine Transhipment Updated Successfully");
            }
            else if (result == "D") {
                showSuccessToast("Marine Transhipment Deleted Successfully");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
    GetMarineTranshipment();
    $("#btnTransportSave").val("Submit");
    $("#LoadingPlace").val("");
    $("#UnLoadingPlace").val("");
    $("#DateArrival").val("");
    $("#DateDischarge").val("");
    $("#NameOfCarrier").val("");
    $("#Comment").val("");
    $("#Remark").val("");
    $("#drpConsignmentType").val(0);//$("#TypeNo").val(""); //$('input[type=radio][name=TransportType]:checked').val();
    $("#TypeDate").val("");
    $('#commentPhoto').attr('src', '');
    $('#commentPhoto').val('');
}



function GetMarineTranshipment() {
    $.ajax({
        type: "POST",
        url: ajaxLink.GetMarineTranshipment,
        data: { "FKClaimId": $("#FKClaimId").val() },
        dataType: "json",
        async: true,
        success: function (result) {
            $("#divgrid").html("");
            $("#divgrid").html(result);

            var len = $(".divMultiple").length;
            if (len > 1) {
                //jQuery('input:radio[name="Transhipment"]').filter('[value="Multiple"]').prop('checked', true);
                $("#tr_TransportType").show();
            } else {
                $("#tr_TransportType").hide();
            }
        }, error: function (result) {
            //   alert(result);
        }
    });
}


function GetSingelMarineTranshipment(pkId) {

    $.ajax({
        type: "POST",
        url: ajaxLink.GetSingelMarineTranshipment,
        data: { "pkId": pkId },
        dataType: "json",
        success: function (result) {
            //alert('sdf')
            console.log(result);

            $("#btnTransportSave").attr("data-content", result.PkId);
            $("#LoadingPlace").val(result.LoadingPlace);
            $("#UnLoadingPlace").val(result.UnLoadingPlace);
            $("#DateArrival").val(result.DateArrival);
            $("#DateDischarge").val(result.DateDischarge);
            $("#NameOfCarrier").val(result.NameOfCarrier);
            $("#Comment").val(result.Comment);
            $("#Remark").val(result.Remark);

            //$("#TypeNo").val(result.TypeNo);
            $("#drpConsignmentType").val(result.TypeNo);
            $("#TypeDate").val(result.TypeDate);

            $("#btnTransportSave").val("Update");
            $('input[type=radio][name=TransportType]:checked').val(result.Mode);

            var listComm = result.listComment;
            $('#commentPhoto').attr('src', "/files/MISC/" + result.CommentPhotoOrDoc);
            $('#divComment input').prop('checked', false);

            $('#divComment input').each(function (index, element) {
                var id = $(this).val();

                $(listComm).each(function (index1, element1) {
                    if (id == element1.PK_CD_ID) {
                        if (element1.selected) {
                            $(element).prop('checked', true);
                        }
                    }
                })

            })
            $('#divRN input').each(function (index, element) {
                var id = $(this).val();
                if (id == result.Remark) {
                    console.log(id);
                    $(element).prop('checked', true);
                    return false;
                }

            })

            var lj = result.LegOfJourney;
            $('#divLegOfJourney input[value=' + lj + ']').prop('checked', true);

            //$('input[type=radio][name=TransportType]:checked').val(result[0].BLNo);
        }, error: function (result) {
            alert("Error");
        }
    });
}

function DeleteMarineTranshipment(pkId) {
    ActionWithMarineTranshipment(pkId, "Delete");
}


/////////////////////////////////////////////////////////////////////////////////////////////////

$(document).on("click", "#btnSurveyorVisits", function () {
    var PkId = $(this).attr("data-content")
    //var x = $("#form1");
    //var status = $(x).ValidationFunction();
    //if (status == true) {

    ActionSurveyorVisits(PkId, "InVisit");
    //} else {
    //    return false;
    //}
});

function ActionSurveyorVisits(pkId, task) {
    var objProperty = {
        "PkId": pkId,
        "FkClaimId": $("#hdFkClaimId").val(),
        "StartDate": $("#txtStartDate").val(),
        "EndDate": $("#txtEndDate").val(),
        "PlaceVisit": $("#PlaceVisit").val(),
        "PurposeVisit": $("#PurposeVisit").val(),
        "Remark": $("#Remark").val(),
        "Task": task
    };
    $.ajax({
        type: "POST",
        url: ajaxLink.ActionSurveyVisits,
        data: JSON.stringify(objProperty),
        dataType: "json",
        contentType: 'application/json',
        success: function (Result) {
            BindSurveyorVisits();
        }, error: function (Result) {
            alert("Error ");
        }
    });
    ClearSurveyorVisits();
    if (task == "InVisit") {
        if (pkId == 0) {
            showSuccessToast("surveyor visits details inserted Successfully");
            location.reload();
        }
        else {
            showSuccessToast("surveyor visits details Updated Successfully");
            location.reload();
        }
    }
    else {
        showSuccessToast("surveyor visits details deleted Successfully");
        location.reload();
    }
}

function EditSurveyorVisits(rowId, pkId) {
    $("#txtStartDate").val($("#tableSurveyorVisits tbody #" + rowId + "").find("td").eq(0).html());
    $("#txtEndDate").val($("#tableSurveyorVisits tbody #" + rowId + "").find("td").eq(1).html());
    $("#PlaceVisit").val($("#tableSurveyorVisits tbody #" + rowId + "").find("td").eq(2).html());
    $("#PurposeVisit").val($("#tableSurveyorVisits tbody #" + rowId + "").find("td").eq(3).html());
    $("#Remark").val($("#tableSurveyorVisits tbody #" + rowId + "").find("td").eq(4).html());
    $("#btnSurveyorVisits").attr("data-content", pkId);
    $("#btnSurveyorVisits").val("Update");
}

function DeleteSurveyorVisits(rowId, pkId) {
    ActionSurveyorVisits(pkId, "DelVisit");
    $("#tableSurveyorVisits tbody #" + rowId + "").remove();
}

function BindSurveyorVisits() {
    $.ajax({
        type: "POST",
        url: ajaxLink.GetSurveyVisits,
        data: { "FkClaimId": $("#hdFkClaimId").val() },
        dataType: "json",
        success: function (Result) {
            $("#tableSurveyorVisits tr:not(:first)").remove();
            var row = "";
            for (var k = 0; k < Result.length; k++) {
                row += "<tr id=tr_" + Result[k].PK_ID + ">";
                row += "<td>" + Result[k].StartDate + "</td>"
                row += "<td>" + Result[k].EndDate + "</td>";
                row += "<td>" + Result[k].PlaceVisit + "</td>";
                row += "<td>" + Result[k].PurposeVisit + "</td>";
                row += "<td>" + Result[k].Remark + "</td>"
                row += "<td><a href=\"#\" onclick=\"EditSurveyorVisits('tr_" + Result[k].PK_ID + "','" + Result[k].PK_ID + "')\" class=\"Btn\">Edit</a> | <a href=\"#\" onclick=\"DeleteSurveyorVisits('tr_" + Result[k].PK_ID + "','" + Result[k].PK_ID + "')\" class=\"Btn\">Delete</a></td>";
                row += "</tr>";
            }
            $("#tableSurveyorVisits").append(row);
        }, error: function (Result) {
            alert("Error");
        }
    });
}

function ClearSurveyorVisits() {
    $("#txtStartDate").val("");
    $("#txtEndDate").val("");
    $("#PlaceVisit").val("");
    $("#PurposeVisit").val("");
    $("#Remark").val("");
    $("#btnSurveyorVisits").attr("data-content", 0);
    $("#btnSurveyorVisits").val("Submit");
}

$(document).on("click", "#BackToAssessment", function () {
    $("#fkTranshipMentClaimId").val($("#hdFkClaimId").val());
    $("#MarineTranshipmentTask").val($("#hdTask").val());
    $("#form_MarineTranshipment").submit();
});

function ApproveOrDiscardInsured(FKInsdId, control, imgId) {
    var tr = $(control).closest("tr");
    var ApproveStatus = "";
    if (tr.find("td").eq(5).html() == "Pending") {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }
    else if (tr.find("td").eq(5).html() == "Approved") {
        ApproveStatus = "Discarded";
        $("#" + imgId).attr("src", "/Images/discarded.png");
    }
    else {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }

    $.ajax({
        type: "POST",
        url: ajaxLink.ApproveOrDiscardInsured,
        data: { "ApproveStatus": ApproveStatus, "FKInsdId": FKInsdId },
        dataType: "json",
        success: function (result) {
            if (ApproveStatus == "Approved") {
                showSuccessToast("User has been approved successfully");
                tr.find("td").eq(5).html("Approved");
            }
            else if (ApproveStatus == "Discarded") {
                showSuccessToast("User has been discarded successfully");
                tr.find("td").eq(5).html("Discarded");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
};


//$(document).on("click", "#btnSearch", function () {
//    debugger;
//    var x = $("#form2");
//    var status = $(x).ValidationFunction();
//    alert("hi");
//    if (status == true) {

//        if ($("#txtSearchEmail").val() != "") {
//            $.ajax({
//                type: "POST",
//                url: ajaxLink.GetRegisteredInsuredDetails,
//                data: { "emailId": $("#txtSearchEmail").val() },
//                dataType: "json",
//                success: function (Result) {
//                    if (Result.length > 0) {
//                        $("#Email").val(Result[0].Email);
//                        $("#PolicyNo").val(Result[0].PolicyNo);
//                        $("#Name").val(Result[0].NAME);
//                        $("#Mobile").val(Result[0].Mobile);
//                        $("#InsrAddress").val(Result[0].Address);
//                        $("#divSearch").show().slideUp(500);
//                        $("#divGenrateClaim").hide().slideDown(500);
//                        $("#InsdType").val("R");
//                    }
//                    else {
//                        showSuccessToast("Insured not found");
//                    }
//                }
//            });
//        }
//    }
//    else {
//        return false;
//    }
//});

$(document).on("click", "#btnAddMore", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        var drp = $("#drpCalulation option:selected").val();
        var type = $('input:radio[name="rbType"]').val();
        if ($("#txtRate").val() == '') {
            $("#txtRate").val(0);
        }
        debugger;
        var txtTax = $("#txtTax").val();
        var txtRate = $("#txtRate").val();
        var res = "";

        var rbType = $("input[name='rbType']:checked").val();
        if (rbType == "Add") {
            res = parseFloat(txtTax) + parseFloat($("#GrandTotalVal").html());
            var newRow = "<tr><td>" + drp + "</td><td>Add</td><td>" + txtRate + "</td>   <td>" + txtTax + "</td><td><a class='deleteTemRow' href=\"#\"><img style=\"width:20px;height:20px;\" src=\"/Images/Delete_icon.png\" alt='' /></a></td></tr>";
        }
        else if (rbType == "Less") {
            res = parseFloat($("#GrandTotalVal").html()) - parseFloat(txtTax);
            var newRow = "<tr><td>" + drp + "</td><td>Less</td><td>" + txtRate + "</td><td>" + txtTax + "</td><td><a class='deleteTemRow' href=\"#\"><img style=\"width:20px;height:20px;\" src=\"/Images/Delete_icon.png\" alt='' /></a></td></tr>";
        }
        else if (rbType == "Mul") {
            res = parseFloat($("#GrandTotalVal").html()) * parseFloat(txtTax);
            var newRow = "<tr><td>" + drp + "</td><td>Mul</td><td>" + txtRate + "</td><td>" + txtTax + "</td><td><a class='deleteTemRow' href=\"#\"><img style=\"width:20px;height:20px;\" src=\"/Images/Delete_icon.png\" alt='' /></a></td></tr>";
        }
        else if (rbType == "Div") {
            res = parseFloat($("#GrandTotalVal").html()) / parseFloat(txtTax);
            var newRow = "<tr><td>" + drp + "</td><td>Div</td><td>" + txtRate + "</td><td>" + txtTax + "</td><td><a class='deleteTemRow' href=\"#\"><img style=\"width:20px;height:20px;\" src=\"/Images/Delete_icon.png\" alt='' /></a></td></tr>";
        }

        $("#GrandTotalVal").html("");
        $("#GrandTotalVal").html(res.toFixed(2));
        $("#txtTax").val("");
        $("#txtTax").prop('disabled', false);
        $("#txtRate").val("");
        $("#tblTax tr:last").before(newRow);

        $("#txtAffectedQuantity").attr("readonly", true);
        $("#txtUnitRate").attr("readonly", true);
    } else {
        return false;
    }
});


$(document).on("keyup", "#txtAffectedQuantity", function () {

    var tr = $(this).closest("tr");
    var txtUnitRateC = tr.find("#txtUnitRate");
    var txtAmountC = tr.find("#txtAmount");

    var total = "";
    if (txtUnitRateC.val() == "" && $(this).val() == "")
        total = 0 * 0;
    else
        total = txtUnitRateC.val() * $(this).val();

    txtAmountC.val(total);
    var subTotal = 0;
    $('#tbAssessment').find("tr #txtAmount").each(function (index, element) {

        subTotal += parseFloat($(this).val());
        $('#SubTotal').html(subTotal);
    })

    $('#tbAssessmentFSR').find("tr #txtAmount").each(function (index, element) {

        subTotal += parseFloat($(this).val());
        $('#SubTotal').html(subTotal);
        console.log(subTotal);
    })
    $('#SubTotal').html(subTotal);
    //   TotalAmount();
});

$(document).on("keyup", "#txtUnitRate", function () {

    var tr = $(this).closest("tr");
    var txtAffectedQuantityC = tr.find("#txtAffectedQuantity");
    var txtAmountC = tr.find("#txtAmount");

    var total = 0;
    if (txtAffectedQuantityC.val() == "" && $(this).val() == "")
        total = 0 * 0;
    else
        total = txtAffectedQuantityC.val() * $(this).val();

    txtAmountC.val(total);
    TotalAmount();
    var subTotal = 0;
    $('#tbAssessment tr #txtAmount').not(":last").each(function (index, element) {
        if ($(this).val() != undefined) {
            subTotal += parseFloat($(this).val());
            $('#SubTotal').html(subTotal);
            console.log(subTotal);
        }
    })
    $('#tbAssessmentFSR tr #txtAmount').not(":last").each(function (index, element) {

        subTotal += parseFloat($(this).val());
        $('#SubTotal').html(subTotal);
        console.log(subTotal);
    })
    $('#SubTotal').html(subTotal);
});


function TotalAmount() {

    var total = 0;
    $("#tblGrid tr:not(:first)").each(function () {
        var txtAmount = $(this).find("#txtAmount");
        if (txtAmount.val() == "")
            total = parseFloat(total) + 0;
        else
            total = parseFloat(total) + parseFloat(txtAmount.val());
    });
    $("#totalVal").html("");
    $("#totalVal").html(total.toFixed(2));


    $("#tblTax tr:not(:first):not(:last)").each(function () {

        //rajat total
        //var td2 = $(this).find("td").eq(1).html();
        //var td3 = $(this).find("td").eq(2).html();
        //if (td2.replace(/\s+/g, '') == "Add") {
        //    total = (parseFloat(total) + parseFloat(td3));
        //} else {
        //    total = (parseFloat(total) - parseFloat(td3));
        //}

        var td2 = $(this).find("td").eq(1).html();
        var td4 = $(this).find("td").eq(3).html();
        console.log(td2);

        if (td2.replace(/\s+/g, '') == "Add") {
            total = (parseFloat(total) + parseFloat(td4));
            //$("#GrandTotalVal").html((parseFloat(total) - parseFloat(td4)).toString());
        } else if (td2.replace(/\s+/g, '') == "Less") {
            total = (parseFloat(total) - parseFloat(td4));
            //$("#GrandTotalVal").html((parseFloat(total) + parseFloat(td4)).toString());
        }
        else if (td2.replace(/\s+/g, '') == "Mul") {
            total = (parseFloat(total) * parseFloat(td4));
            //$("#GrandTotalVal").html((parseFloat(total) / parseFloat(td4)).toString());
        }
        else if (td2.replace(/\s+/g, '') == "Div") {
            total = (parseFloat(total) / parseFloat(td4));
            //$("#GrandTotalVal").html((parseFloat(total) * parseFloat(td4)).toString());
        }
    });

    var tr = $("#tblTax tr:not(:first)").length;
    if (tr > 1) {
        $('.TxtBoxReadOnly').each(function () {
            $(this).attr("readonly", true);
        });
    }
    if (total == undefined || total == null)
        total = 0;

    $("#GrandTotalVal").html("");
    if (total > 0)
        $("#GrandTotalVal").html(total.toFixed(2));
    else
        $("#GrandTotalVal").html('0');
}

$(document).on("click", ".deleteTemRow", function () {
    //var total = $("#GrandTotalVal").html();
    //var tr = $(this).closest("tr");
    //var td2 = $(tr).find("td").eq(1).html();
    //var td3 = $(tr).find("td").eq(2).html();
    //if (td2.replace(/\s+/g, '') == "Add") {
    //    $("#GrandTotalVal").html((parseFloat(total) - parseFloat(td3)).toString());
    //} else {
    //    $("#GrandTotalVal").html((parseFloat(total) + parseFloat(td3)).toString());
    //}
    var total = $("#GrandTotalVal").html();
    var tr = $(this).closest("tr");
    var td2 = $(tr).find("td").eq(1).html();
    var td4 = $(tr).find("td").eq(3).html();
    if (td2.replace(/\s+/g, '') == "Add") {
        $("#GrandTotalVal").html((parseFloat(total) - parseFloat(td4)).toString());
    } else if (td2.replace(/\s+/g, '') == "Less") {
        $("#GrandTotalVal").html((parseFloat(total) + parseFloat(td4)).toString());
    }
    else if (td2.replace(/\s+/g, '') == "Mul") {
        $("#GrandTotalVal").html((parseFloat(total) / parseFloat(td4)).toString());
    }
    else if (td2.replace(/\s+/g, '') == "Div") {
        $("#GrandTotalVal").html((parseFloat(total) * parseFloat(td4)).toString());
    }

    tr.remove();
});

$(document).on("click", "#btnGetValue", function () {
    $("#txtTax").val($("#display").val());
});

$(document).on("click", "#btnFinalSubmit", function () {

    //$('#tbAssessment').find('tr .btnRemoveAssessment').each(function (index, element) {

    //    var txtFKClaimId = $('#hdnClaimId').val();
    //    console.log(txtFKClaimId);
    //    debugger;
    //    var drpDestination = $(this).closest('tr').attr('Id');
    //    var txtAffectedQuantity = 0;
    //    var objProperty = {
    //        FKClaimId: txtFKClaimId,
    //        FKDesId: drpDestination,
    //        AffectedQuantity: txtAffectedQuantity

    //    };

    //    $.ajax({
    //        type: "POST",
    //        url: ajaxLink.MarineSubmitDamageDetails,
    //        data: JSON.stringify(objProperty),
    //        dataType: "json",
    //        contentType: "application/json",
    //        success: function (result) {
    //        },
    //        error: function (result) {
    //            alert("Error");
    //        }
    //    });

    //})



    var xml = "<Records>";
    //var tp = $(this).attr("data-content");
    //var tp = $('#AssessmentType').val();
    var tp = $('#tempTask').val();
    //alert(tp);
    //$("#tblTax tr:not(:first):not(:last)").each(function () {
    //$("#tblGrid tr:not(:first)").each(function () {
    $("#tblGrid tr:not(:first):not(:last)").each(function () {
        var txtAffectedQuantityC = $(this).find("#txtAffectedQuantity");
        var txtUnit = $(this).find("#txtUnitRate");
        var txtAmountC = $(this).find("#txtAmount");
        var txtMD_ID = $(this).find('#txtMD_ID').val();
        var txtAD_ID = $(this).find('#txtAD_ID').val();
        var fk_DesId = $(this).attr('Id');
        debugger;
        console.log(txtMD_ID);
        xml += "<Record>";
        xml += "<FK_CalID>" + $(this).attr("Id") + "</FK_CalID>";

        //if (tp == "AS") {
        if (tp == "AS" || tp == "FSR") {
            //alert();
            xml += "<AffectedQuantityA>" + $(txtAffectedQuantityC).val() + "</AffectedQuantityA>";
            xml += "<UnitRateA>" + $(txtUnit).val() + "</UnitRateA>";
            xml += "<AmountA>" + $(txtAmountC).val() + "</AmountA>";
            xml += "<MD_ID>" + txtMD_ID + "</MD_ID>";
            xml += "<AD_ID>" + txtAD_ID + "</AD_ID>";
            xml += "<FK_DesID>" + fk_DesId + "</FK_DesID>";
        } else {
            xml += "<AffectedQuantityC>" + $(txtAffectedQuantityC).val() + "</AffectedQuantityC>";
            xml += "<UnitRateC>" + $(txtUnit).val() + "</UnitRateC>";
            xml += "<AmountC>" + $(txtAmountC).val() + "</AmountC>";
            xml += "<MD_ID>" + txtMD_ID + "</MD_ID>";
            xml += "<AD_ID>" + txtAD_ID + "</AD_ID>";
            xml += "<FK_DesID>" + fk_DesId + "</FK_DesID>";
        }

        xml += "</Record>";
    });
    xml += "</Records>";

    xml += "<AmountRecords>";
    console.log(xml);
    debugger

    $("#tblTax tr:not(:first):not(:last)").each(function () {

        var td1 = $(this).find("td").eq(0).html();
        var td2 = $(this).find("td").eq(1).html();
        var td3 = $(this).find("td").eq(2).html();
        var td4 = $(this).find("td").eq(3).html();
        xml += "<Record>";
        xml += "<FK_CalID>" + td1 + "</FK_CalID>";
        xml += "<CalType>" + td2.replace(/\s+/g, '') + "</CalType>";
        xml += "<Rate>" + td3 + "</Rate>";
        xml += "<Value>" + td4 + "</Value>";
        xml += "<Result>" + $("#totalVal").html() + "</Result>";
        xml += "</Record>";
    });
    xml += "</AmountRecords>";

    var objProperty = {
        Task: "Insert",
        FkClaimId: $("#hdFkClaimId").val(),
        CalMode: $(this).attr("data-content"),
        XMLFile: xml
    };
    debugger;
    $.ajax({
        type: "POST",
        url: ajaxLink.AddMarineAssesment,
        data: JSON.stringify(objProperty),// { "objProperty": objProperty },
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            if (result == "Y") {
                if ($("#btnFinalSubmit").val().toLowerCase() == "Next".toLowerCase()) {
                    $("#fkFSRClaimId").val($("#hdFkClaimId").val());
                    $("#form_SubmitFSR").submit();
                } else {
                    showSuccessToast("Assesment Updated Successfully.");
                    location.reload();
                }
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
});

$(document).on("click", "#btnSubmitFSR", function () {
    var txtSalvage = $("#txtSalvage").val();
    var txtCommentPolicy = $("#txtCommentPolicy").val();
    var txtConcurrence = $("#Concurrence").val();
    //var txtConcurrence = $("#txtConcurrence").val();
    var txtRemark = $("#txtRemark").val();
    var txtCommentPolicy_Com_NotCom = $('#CommentPolicy_Com_NotCom').val();
    if (txtCommentPolicy_Com_NotCom == "Complied") {
        txtCommentPolicy = 'The Insured have complied all the terms and condions of above mention policy.';
    }
    var objProperty = {
        "FkClaimId": $("#hdFkClaimId").val(),
        "Salvage": txtSalvage,
        "CommentPolicy": txtCommentPolicy,
        "Concurrence": txtConcurrence,
        "OtherRemark": txtRemark,
        "CommentPolicy_Com_NotCom": txtCommentPolicy_Com_NotCom

    };

    $.ajax({
        type: "POST",
        url: ajaxLink.SubmitFSR,
        data: JSON.stringify(objProperty),
        dataType: "json",
        contentType: 'application/json',
        success: function (Result) {
            showSuccessToast("FSR details inserted successfully");
            //BindSurveyorVisits();
        }, error: function (Result) {
            alert("Error");
        }
    });

});
$(document).on("change", '#CommentPolicy_Com_NotCom', (function () {

    if ($(this).val() == "Not Complied") {
        $('#txtCommentPolicy').val('');
        $('#divCommentPolicy').show();
    }
    else {
        $('#txtCommentPolicy').val('');
        //$('#txtCommentPolicy').val('');
        $('#divCommentPolicy').hide();
        
    }


}))

function ApproveOrDiscardTrainee(TSURID, control, imgId) {
    var tr = $(control).closest("tr");
    var ApproveStatus = "";
    if (tr.find("td").eq(5).html() == "Pending") {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }
    else if (tr.find("td").eq(5).html() == "Approved") {
        ApproveStatus = "Discarded";
        $("#" + imgId).attr("src", "/Images/discarded.png");
    }
    else {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }

    $.ajax({
        type: "POST",
        url: ajaxLink.ApproveOrDiscardTrainee,
        data: { "ApproveStatus": ApproveStatus, "TSURID": TSURID },
        dataType: "json",
        success: function (result) {
            if (ApproveStatus == "Approved") {
                showSuccessToast("User has been approved successfully");
                tr.find("td").eq(5).html("Approved");
            }
            else if (ApproveStatus == "Discarded") {
                showSuccessToast("User has been discarded successfully");
                tr.find("td").eq(5).html("Discarded");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
};

function ViewTrainee(PkTSurId) {
    var htmlTable = "";
    $.ajax({
        type: "POST",
        url: ajaxLink.ViewTraineeProfile,
        data: { "PkTSurId": PkTSurId },
        dataType: "json",
        success: function (result) {
            htmlTable = "<div class=\"grid_body\"><table id='mygrid'><tr><td><b>First Name</b></td><td>" + result[0].FirstName + " </td>";
            htmlTable += "<td><b>Last Name</b></td><td>" + result[0].LastName + "</td></tr><tr><td><b>User Name</b></td><td>" + result[0].UserName + "</td><td><b>Mobile</b></td><td>" + result[0].Mobile + "</td></tr>";
            htmlTable += "<tr><td><b>State</b></td><td>" + result[0].StateName + " </td><td><b>District</b></td><td>" + result[0].DistrictName + "</td></tr>";
            htmlTable += "<tr><td><b>Membership</b></td><td>" + result[0].UserType + "</td><td><b>Alternative Mobile</b></td><td>" + result[0].Email + "</td></tr>";
            htmlTable += "<tr><td><b>Address</b></td><td>" + result[0].Address + "</td><td><b>Pin code</b></td><td>" + result[0].Pincode + "</td></tr>";
            htmlTable += "<tr><td><b>Pan No</b></td><td>" + result[0].Country + "</td><td><b>SLA No</b></td><td>" + result[0].SLANo + "</td></tr>";
            htmlTable += "<tr><td><b>IRDANo</b></td><td>" + result[0].IRDANo + "</td><td><b>Education</b></td><td>" + result[0].Education + "</td></tr>";
            htmlTable += "<tr><td><b>Ins Qualification</b></td><td>" + result[0].InsQualification + "</td><td>&nbsp;</td><td>&nbsp;</td></tr>";

            htmlTable += "</table></div>";
            var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ViewSurveyProfile\"></div><a class=\"close\" href=\"#close\"></a></div>");
            $(div).appendTo("body");
            $("#ViewSurveyProfile").empty();
            $("#ViewSurveyProfile").append(htmlTable);
            window.location = "#popup";
        },
        error: function (result) {
            alert("Error");
        }
    });
}

$(document).on("click", "#btnSendRequestToSurvery", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        //funTraineeRequestToSurvery($("#txtSearchSurvery").val(), "SendRequest");
        funTraineeRequestToSurvery($("#EmailId").val(), "SendRequest");
    } else {
        return false;
    }
});

function funTraineeRequestToSurvery(emailIdORSLA, mode) {
    $.ajax({
        type: "POST",
        url: ajaxLink.TraineeRequestToSurvery,
        data: { "emailIdORSLA": emailIdORSLA, "mode": mode },
        dataType: "json",
        success: function (result) {
            if (result.toLowerCase() == "Active".toLowerCase()) {
                $("#img_" + emailIdORSLA).attr("src", "/Images/approve.png");
                showSuccessToast("Trainee approved successfully");
            } else if (result.toLowerCase() == "DeActive".toLowerCase()) {
                $("#img_" + emailIdORSLA).attr("src", "/Images/discarded.png");
                showSuccessToast("Trainee discarded successfully");
            } else {
                showSuccessToast(result);
            }
        }, error: function (result) {

        }
    });
}


$(document).on("click", "#btnBackClaim", function () {
    var task = $("#hdtask").val();
    var claimId = $("#hdFKClaimId").val();
    //if (task.toLowerCase() == 'fsr') {
    //    $("#fkSurveyVisitsClaimId").val(claimId);
    //    $("#SurveyVisitsTask").val(task);
    //    $("#form_SurveyVisits").submit();
    //} else {
    var claimId = $(this).attr("data-claimId");
    $("#fkClaimIdFrom").val(claimId);
    $("#ClaimFormTask").val(task);
    //$("#form_Clam").submit();
    window.location = "/Survery/ClaimForm?fkClaimId=" + $("#hdFKClaimId").val() + "&task=" + $("#hdtask").val();
    //}
});


$(document).on("click", "#btnClaimFormBack", function () {
    debugger;
    var task = $("#Task").val();
    if (task.toLowerCase() == 'fsr') {
        var claimId = $(this).attr("data-content");
        $("#fkSurveyVisitsClaimId").val(claimId);
        $("#SurveyVisitsTask").val(task);
        $("#form4").submit();
    } else {
        $("#formManageClaim").submit();
    }

});


function CorporateInsuredView(PKCInsdId) {
    var htmlTable = "";
    $.ajax({
        type: "POST",
        url: ajaxLink.ViewCorporateInsuredProfile,
        data: { "PKCInsdId": PKCInsdId },
        dataType: "json",
        success: function (result) {
            htmlTable = "<div class=\"grid_body\"><table id='mygrid'><tr><td><b>Name</b></td><td>" + result[0].NAME + " </td><td>Insurer Type</td><td>" + result[0].InsType + "</td>";
            htmlTable += "</tr><tr><td><b>User Name</b></td><td>" + result[0].UserName + "</td><td><b>Mobile</b></td><td>" + result[0].Mobile + "</td></tr>";
            htmlTable += "<tr><td><b>State</b></td><td>" + result[0].StateName + " </td><td><b>District</b></td><td>" + result[0].DistrictName + "</td></tr>";
            htmlTable += "<tr><td><b>Email</b></td><td>" + result[0].Email + "</td><td><b>Alternative Mobile</b></td><td>" + result[0].AltMobile + "</td></tr>";
            htmlTable += "<tr><td><b>Address</b></td><td>" + result[0].Address + "</td><td><b>Pin code</b></td><td>" + result[0].Pincode + "</td></tr>";
            htmlTable += "<tr><td><b>Reg Office</b></td><td>" + result[0].RegOffice + "</td><td><b>Experience</b></td><td>" + result[0].Experience + "</td></tr>";
            htmlTable += "</table></div>";
            var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ViewCorporateInsuredProfile\"></div><a class=\"close\" href=\"#close\"></a></div>");
            $(div).appendTo("body");
            $("#ViewCorporateInsuredProfile").empty();
            $("#ViewCorporateInsuredProfile").append(htmlTable);
            window.location = "#popup";
        },
        error: function (result) {
            alert("Error");
        }
    });
}

function ApproveOrDiscardCorporateInsured(PKCInsdId, control, imgId) {
    var tr = $(control).closest("tr");
    var ApproveStatus = "";
    if (tr.find("td").eq(6).html() == "Pending") {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }
    else if (tr.find("td").eq(6).html() == "Approved") {
        ApproveStatus = "Discarded";
        $("#" + imgId).attr("src", "/Images/discarded.png");
    }
    else {
        ApproveStatus = "Approved";
        $("#" + imgId).attr("src", "/Images/approve.png");
    }


    $.ajax({
        type: "POST",
        url: ajaxLink.ApproveOrDiscardCorporateInsured,
        data: { "ApproveStatus": ApproveStatus, "PKCInsdId": PKCInsdId },
        dataType: "json",
        success: function (result) {
            if (ApproveStatus == "Approved") {
                tr.find("td").eq(6).html("Approved");
                showSuccessToast("User has been approved successfully");
            }
            else if (ApproveStatus == "Discarded") {
                tr.find("td").eq(6).html("Discarded");
                showSuccessToast("User has been discarded successfully");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });
};


$(document).on("click", "#btnSearchArroveSurvery", function () {
    var emailId = $("#txtSearchSurvery").val();
    if (emailId != "") {
        $.ajax({
            url: ajaxLink.BindApproveSurveyorsDetails,
            data: "{ 'emailId': '" + emailId + "'}",
            dataType: "json",
            type: "POST",
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $("#tblDetails").append("<tbody><tr><td>" + data[0].FirstName + ' ' + data[0].LastName + "</td><td>" + data[0].Mobile + "</td><td>" + data[0].Address + "</td><td>" + data[0].Pincode + "</td></tr></tbody>");
                $("#EmailId").val(data[0].Email);
                $("#hdFkSurveyor").val(data[0].PK_SURID);
                $(".form_label").removeClass("dispalyNone");
            },
            error: function (response) {
                alert("error");
            }
        });
    }
});

$(document).on("click", "#btnMotorVehicleParticulars", function () {
    var x = $("#form7");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});

$(document).on("click", "#btnMotorDLParticulars", function () {
    var x = $("#formDL");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});


$(document).on("click", "#btnMotorOccurence", function () {
    var x = $("#formMO");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});


$(document).on("click", "#btnAssginTask", function () {
    var xml = "<Records>";
    $("#mygrid tr:not(:first)").each(function () {
        var drp = $(this).find("select");
        //if (drp.val() != "0") {
        xml += "<Record>";
        xml += "<TraneeId>" + drp.val() + "</TraneeId>";
        xml += "<ClaimID>" + $(this).attr("Id") + "</ClaimID>";
        xml += "</Record>";
        //}
    });
    xml += "</Records>";
    var objProperty = {
        Mode: "Assign",
        Xml: xml
    };

    $.ajax({
        type: "POST",
        url: ajaxLink.TraineeAssginTask,
        data: JSON.stringify(objProperty),// { "objProperty": objProperty },
        dataType: "json",
        contentType: "application/json",
        success: function (result) {
            if (result == "Y") {
                showSuccessToast("Task Assign Successfully");
            }
            else {
                alert("Error");
            }
        },
        error: function (result) {
            alert("Error");
        }
    });

});


// Motor Work
$(document).on("change", "#drpConsignmentType", function () {
    var DataDate = $(this).find('option:selected').attr("data-date");// $(this).attr("data-date");
    $("#TypeDate").val(DataDate);
});

function BindConsignmentType(FKClaimId, ConsignmentType) {
    $.ajax({
        type: "POST",
        url: ajaxLink.BindConsignmentType,
        data: { "FKClaimId": FKClaimId, "ConsignmentType": ConsignmentType },
        dataType: "json",
        success: function (result) {
            console.log(result);
            $("#drpConsignmentType").html(result);
        },
        error: function (result) {
            alert("Error");
        }
    });
}
function BindMotorRemarksMaster(fkClaimid) {
    $.ajax({
        type: "POST",
        url: ajaxLink.BindMotorRemarksMaster,
        data: { "fkClaimId": fkClaimid },
        dataType: "json",
        success: function (result) {
            //$(".tbllist #trCh").before().empty();
            $("#trCh").empty();
            $("#trCh").append(result);
        },
        error: function (result) {
            alert("Error");
        }
    });
}

//$(document).on("click", "#btnSaveRemarksClaim", function () {
//    var x = $("#form4");
//    var status = $(x).ValidationFunction();
//    if (status == true) {
//        var listCheck = [];
//        $("input[name='chMarineRemarks']:checked").each(function () {
//            listCheck.push($(this).val());
//        });

//        var objProperty = {
//            FKClaimId: $("#hdFKClaimId").val(),
//            CauseOfLoss: $("#CauseOfLoss").val(),
//            DelayInIntimation: $("#DelayInIntimation").val(),
//            DelayIntakingDelivery: $("#DelayIntakingDelivery").val(),
//            CheckBoxList: listCheck
//        };

//        $.ajax({
//            type: "POST",
//            url: ajaxLink.InsertMarineRemark,
//            data: JSON.stringify(objProperty),
//            dataType: "json",
//            contentType: "application/json",
//            async: false,
//            success: function (result) {
//                if (result == "I") {
//                    SendToNextLOR($("#hdFKClaimId").val(), $("#hdTask").val())
//                }
//                else {
//                    alert("Error");
//                }
//            },
//            error: function (result) {
//                alert(result);
//            }
//        });
//    } else {
//        return false;
//    }
//});

$(document).on("click", "#btnSendEmail", function () {
    debugger;
    var clamId = $(this).attr("data-claimid");
    var task = $(this).attr("data-task");
    var htmlTable = "<div style='width:400px;'><p>Enter email id for CC</p><p><input type=\"text\" id=\"txtCCMail\" /></p></div> </br> <div class='fr'>" +
        "<input type=\"button\" id=\"btnSendCCMail\" value=\"Submit\" data-task='" + task + "' data-claimid='" + clamId + "' /></div>";
    var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popupSendMail\"></a><div class=\"popup\"><div style='margin-top:30px;' id=\"EmailCC\"></div><a class=\"close\" href=\"#close\"></a></div>");
    $(div).appendTo("body");
    $("#divPopup").html("");

    $("#EmailCC").empty();
    $("#EmailCC").append(htmlTable);
    window.location = "#popupSendMail";
});


$(document).on("click", "#btnSendEmailLor", function () {
    window.location = "#popup1";
});


//$(document).on("click", "#btnSendCCMail", function () {

//    var txtCCMail = $("#txtCCMail").val();
//    var clamId = $(this).attr("data-claimid");
//    var task = $(this).attr("data-task");

//    $("#EmailIdCC").val(txtCCMail);
//    $("#fkMailClaimId").val(clamId);
//    $("#EmailTask").val(task);
//    $("#form_SendMaillCC").submit();

//});

$(document).on("click", "#btnSubmitFSR1", function () {
    $("#fkFSRClaimId").val($(this).attr("data-claimid"));
    $("#FSRTask").val($(this).attr("data-content"));
    $("#form_SubmitFSR").submit();

});


//function MoveToNext(type) {
//    var FKClaimId = $("#hdFKClaimId").val();
//    switch (type) {
//        case 1:
//            $("#divMarineJiv").show();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").hide();
//            $("#MarineISVR").hide();
//            break;
//        case 2:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").show();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").hide();
//            $("#MarineISVR").hide();
//            MarineGetConsignmentDocs(FKClaimId);
//            break;
//        case 3:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").show();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").hide();
//            $("#MarineISVR").hide();
//            //BindDestination(FKClaimId);
//            MarineGetDamageDetails(FKClaimId)
//            break;
//        case 4:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").show();
//            $("#MarineLOR").hide();
//            $("#MarineISVR").hide();
//            BindMarineRemarksMaster(FKClaimId);
//            break;
//        case 5:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").show();
//            $("#MarineISVR").hide();
//            if ($("#hdtask").val() == "JIR") {
//                $("#aLorNext").removeClass("Btn").addClass("dispalyNone");
//                $("#btnGenratePrview").removeClass("dispalyNone").addClass("Btn");
//            }
//            else {
//                $("#aLorNext").removeClass("dispalyNone").addClass("Btn");
//                $("#btnGenratePrview").removeClass("Btn").addClass("dispalyNone");
//            }
//            BindMarineLORMaster(FKClaimId);
//            break;
//        case 6:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").hide();
//            if ($("#hdtask").val() == "JIR") {
//                MoveToNext(5);
//            }
//            else {
//                $("#MarineISVR").show();
//            }
//            //BindMarineLORMaster(FKClaimId);
//            break;
//        case 7:
//            $("#fkMarineJIRConsignmentClaimId").val($("#hdFkClaimId").val());
//            $("#form_MarineJIRConsignment").submit();
//            break;
//    }
//}

//function MoveToBack(type) {
//    var FKClaimId = $("#hdFKClaimId").val();
//    switch (type) {
//        case 1:
//            if (sessionStorage.getItem("ActionType").toLowerCase() == "FSR".toLowerCase()) {
//                MoveToNext(7);
//            }
//            else {
//                $("#divMarineJiv").show();
//                $("#divMarineConsignmentDocs").hide();
//                $("#MarineDamageDetails").hide();
//                $("#MarineRemarksClaim").hide();
//                $("#MarineISVR").hide();
//            }
//            //window.location.href= MarineJIRConsignment
//            break;
//        case 2:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").show();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineISVR").hide();
//            MarineGetConsignmentDocs(FKClaimId);
//            break;
//        case 3:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").show();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineISVR").hide();
//            // BindDestination(FKClaimId);
//            MarineGetDamageDetails(FKClaimId)
//            break;
//        case 4:
//            BindMarineRemarksMaster(FKClaimId);
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").show();
//            $("#MarineLOR").hide();
//            $("#MarineISVR").hide();
//            break;
//        case 5:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").show();
//            $("#MarineISVR").hide();
//            BindMarineLORMaster(FKClaimId);
//            break;
//        case 6:
//            $("#divMarineJiv").hide();
//            $("#divMarineConsignmentDocs").hide();
//            $("#MarineDamageDetails").hide();
//            $("#MarineRemarksClaim").hide();
//            $("#MarineLOR").show();
//            $("#MarineISVR").hide();
//            break;
//        case 7:
//            $("#fkClaimIdFrom").val($("#hdFkClaimId").val());
//            $("#form3").submit();
//            break;
//    }
//}

// added by dhiraj
$(document).on("click", "#btnMotorPartClaimSub", function () {
    var x = $("#formPC");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});
$(document).on("click", "#btnMotorRouteParticulars", function () {
    var x = $("#formRP");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});
$(document).on("click", "#btnMotorPolicyParticulars", function () {
    var x = $("#formPP");
    var status = $(x).ValidationFunction();
    if (status == true) {
        return true;
    }
    else {
        return false;
    }
});

function getAllElement(claimid) {
    var the_Rvalue = [];
    var the_txtvalue = [];
    debugger;
    jQuery('input:radio:checked').each(function (index, value) {
        // $('tr.checklisttr').each(function () {
        the_Rvalue.push({ RemId: jQuery(value).closest("tr").find("input:hidden").val(), RValue: jQuery(value).val() });

    });
    debugger;
    var txtDval = $("#txtDOther").val();
    var txtOval = $("#txtOOther").val();
    var txtPval = $("#txtPOther").val();
    var txtRval = $("#txtROther").val();
    var txtVval = $("#txtVOther").val();
    var txtTimes = $("#txtTimes").val();
    var txtRatio = $("#txtRatio").val();


    $.ajax({
        url: ajaxLink.InsertMotorParticularRemark,
        data: JSON.stringify({ objlst: the_Rvalue, Oparticular: txtOval, Pparticular: txtPval, Rparticular: txtRval, Vparticular: txtVval, Dparticular: txtDval, Cid: claimid, times: txtTimes, ratio:txtRatio }),
        dataType: "json",
        type: "POST",
        contentType: "application/json; charset=utf-8",
        success: function (data) {
            debugger;
            window.location.href = "/Survery/MotorParticularRemark/" + claimid;
        }
        //,
        //error: function (response) {
        //    alert("error");
        // }
    });

}

// ended by dhiraj