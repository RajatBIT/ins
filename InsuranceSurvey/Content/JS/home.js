﻿var ajaxHomeLink = {
    GenerateVerifiedOTP: "/Collaborator/GenerateVerifiedOTP"
}


$(document).on("click", "#btnSubmitOtp", function () {
    otp('VF');
    $("#txtOTP").val("");
    $("#timer").html("0");
    //if (CheckOTPStatus == true || CheckOTPStatus == "true") {
    //    alert("1");
    //    return true;
    //} else {
    //    alert("2");
    //    return false;
    //}
});

$(document).on("click", "#txtReSendOTP", function () {
    otp('RE')
});

    
$(document).on("click", "#btnSurveyorRegistrationSubmit", function () {
    otp('IN');
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        if ($('#Password').val() != $('#ConfirmPassword').val())
        {
            alert("Password is not Matched");
            return false;
        }
        var sr = {};
        sr.UserName = $('#txtUserName').val();
        sr.Mobile = $('#Mobile').val();
        sr.Email = $('#txtEmailId').val();
        sr.PanNo = $('#PanNo').val();
        sr.StaxNo = $('#StaxNo').val();
        sr.SlanNo = $('#SlanNo').val();
        sr.IslaNo = $('#txtIslaNo').val();
        sr.IniRef = $('#txtIniRef').val();
        var i = 0;
        $.ajax({
            type: "POST",
            url: '/Home/SurveyorRegisterValidation',
            data: JSON.stringify({ sr:sr }),
            dataType: "json",
            contentType: 'application/json',
            success: function (Result) {
                console.log(Result);
                var vadiationMsg = '';
        
                if( Result.EmailStatus==1)
                {
                    vadiationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                    i++;
                }
                 if (Result.ISLANOStatus == 1) {
                     vadiationMsg += "<p>IIISLA NO is already Exist! Please Enter Another IIISLA NO..</p>";
                     i++;
                }

                if (Result.IniRefStatus == 1) {
                    vadiationMsg += "<p>Initial Reference is already Exist! Please Enter Another Initial Reference.</p>";
                    i++;
                }
                 if (Result.MobileStatus == 1) {
                     vadiationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                     i++;
                }
                if (Result.PANNOStatus == 1) {
                    vadiationMsg += "<p>Pan No is already Exist! Please Enter Another Pan No..</p>";
                    i++;
                }
                if (Result.SLANoStatus == 1) {
                    vadiationMsg += "<p>SLA No is already Exist! Please Enter Another SLA No.</p>";
                    i++;
                }
                if (Result.StaxNoStatus == 1) {
                    vadiationMsg += "<p>GST IN is already Exist! Please Enter Another GST NO.</p>";
                    i++;
                }
                if (Result.UserNameStatus == 1) {
                    vadiationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                    i++;
                }
                 
                 if(i>0)
                 {
                     //showWarningToast(vadiationMsg);
                     console.log(vadiationMsg);
                     var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ErrorMes\"></div><a class=\"close\" href=\"#close\"></a></div>");
                     $(div).appendTo("body");
                     $("#ErrorMes").empty();
                     $("#ErrorMes").append(vadiationMsg);

                     window.location = "#popup";
                     //return false;
                 }
                 else {
                         otp('IN')
                         $("#openPopup").click();
                         //$("#timer").val("10");
                         //myTimer(20);
                         return true;
                 }

            }, error: function (Result) {
                return false;
            }
        });
        //if (i > 0) {
        //    return false;
        //}
        //else {

        //    otp('IN')
        //    $("#openPopup").click();
        //    //$("#timer").val("10");
        //    //myTimer(20);
        //    return true;
        //}
    }
    else {
        return false;
    }
});


function showWarningToast(msg) {
    $().toastmessage('showWarningToast', msg);
}

function showStickyWarningToast(msg) {
    $().toastmessage('showToast', {
        text: 'msg',
        sticky: true,
        position: 'middle-right',
        type: 'warning',
        closeText: '',
        close: function () {
            console.log("toast is closed ...");
        }
    });
}


$(document).on("click", "#btnInsuredRegistrationSubmit", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    //alert(status);
    debugger;
    if (status == true) {
        if ($('#Password').val() != $('#ConfirmPassword').val())
        {
            alert("Password is not Matched");
            return false;
        }
        var ins = {};
        ins.UserName = $('#UserName').val();
        ins.Mobile = $('#Mobile').val();
        ins.Email = $('#txtEmailId').val();
        ins.PANNO = $('#PANNO').val();
        ins.GSTIN = $('#GSTIN').val();
        var i = 0;
        $.ajax({
            type: "POST",
            url: '/Home/InsuredRegisterValidation',
            data: JSON.stringify({ i: ins }),
            dataType: "json",
            contentType: 'application/json',
            success: function (Result) {
                console.log(Result);
                var vadiationMsg = '';
                debugger;
                if( Result.EmailStatus==1)
                {
                    vadiationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                    i++;
                }
                
                if (Result.MobileStatus == 1) {
                    vadiationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                    i++;
                }
               if (Result.PANNOStatus == 1) {
                    vadiationMsg += "<p>Pan No is already Exist! Please Enter Another Pan No..</p>";
                    i++;
                }
                if (Result.UserNameStatus == 1) {
                    vadiationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                    i++;
                }
                 
                if (Result.GSTINStatus == 1) {
                    vadiationMsg += "<p>GSTIN is already Exist! Please Enter Another GSTIN.</p>";
                    i++;
                }

                if(i>0)
                {
                    //showWarningToast(vadiationMsg);
                    console.log(vadiationMsg);
                    var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ErrorMes\"></div><a class=\"close\" href=\"#close\"></a></div>");
                    $(div).appendTo("body");
                    $("#ErrorMes").empty();
                    $("#ErrorMes").append(vadiationMsg);

                    window.location = "#popup";
                    return false;
                }
                else {
                    otp('IN')
                        $("#openPopup").click();
                        //$("#timer").val("10");
                        //myTimer(20);
                        return true;
                }
            }, error: function (Result) {
                return false;
            }
        });
        //if (i > 0) {
        //    return false;
        //}
        //else {
        //    alert(i);
        //    otp('IN')
        //    $("#openPopup").click();
        //    //$("#timer").val("10");
        //    //myTimer(20);
        //    return true;
        //}
    }
    else {
        return false;
    }
});

$(document).on("click", "#btnTraineRegistrationSubmit", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        if ($('#Password').val() != $('#ConfirmPassword').val()) {
            alert("Password is not Matched");
            return false;
        }
        var t = {};
        t.UserName = $('#UserName').val();
        t.Mobile = $('#Mobile').val();
        t.EmailId = $('#txtEmailId').val();
        console.log(t);



        
        var i = 0;
        $.ajax({
            type: "POST",
            url: '/Home/TraineeSurveyorsRegisterValidation',
            data: JSON.stringify({ t: t }),
            dataType: "json",
            contentType: 'application/json',
            success: function (Result) {
                console.log(Result);
                var vadiationMsg = '';

                if (Result.EmailStatus == 1) {
                    vadiationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                    i++;
                }
                
                if (Result.MobileStatus == 1) {
                    vadiationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                    i++;
                }
                
               if (Result.UserNameStatus == 1) {
                    vadiationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                    i++;
                }

                if (i > 0) {
                    //showWarningToast(vadiationMsg);
                    console.log(vadiationMsg);
                    var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ErrorMes\"></div><a class=\"close\" href=\"#close\"></a></div>");
                    $(div).appendTo("body");
                    $("#ErrorMes").empty();
                    $("#ErrorMes").append(vadiationMsg);

                    window.location = "#popup";
                    //return false;
                }
                else {
                    debugger;
                    otp('IN')
                    $("#openPopup").click();
                    //$("#timer").val("10");
                    //myTimer(20);
                    return true;
                }

            }, error: function (Result) {
                return false;
            }
        });
    }
    else {
        return false;
    }
});


//$(document).on("click", "#btnCorporateRegistationSubmit", function () {
//    var x = $("#form1");
//    var status = $(x).ValidationFunction();
//    if (status == true) {
//        otp('IN')
//        $("#openPopup").click();
//        $("#timer").val("10");
//        myTimer(20);
//        return true;
//    }
//    else {
//        return false;
//    }
//});

$(document).on("click", "#btnCorporateRegistationSubmit", function () {
    var x = $("#form1");
    var status = $(x).ValidationFunction();
    if (status == true) {
        if ($('#Password').val() != $('#ConfirmPassword').val()) {
            alert("Password is not Matched");
            return false;
        }
        var c = {};
        c.UserName = $('#UserName').val();
        c.Mobile = $('#Mobile').val();
        c.EmailId = $('#txtEmailId').val();
        

        var i = 0;
        $.ajax({
            type: "POST",
            url: '/Home/CorporateInsuredRegisterValidation',
            data: JSON.stringify({ c: c }),
            dataType: "json",
            contentType: 'application/json',
            success: function (Result) {
                console.log(Result);
                var vadiationMsg = '';

                if (Result.EmailStatus == 1) {
                    vadiationMsg += "<p>Email Id is already Exist! Please Enter Another EmailId.</p>";
                    i++;
                }

                if (Result.MobileStatus == 1) {
                    vadiationMsg += "<p>Mobile No is already Exist! Please Enter Another Mobile.</p>";
                    i++;
                }

                if (Result.UserNameStatus == 1) {
                    vadiationMsg += "<p>User Name is already Exist! Please Enter Another User Name.</p>";
                    i++;
                }

                if (i > 0) {
                    //showWarningToast(vadiationMsg);
                    console.log(vadiationMsg);
                    var div = $("<a href=\"#popup\" id=\"pop\"></a><a href=\"#x\" class=\"overlay\" id=\"popup\"></a><div class=\"popup\"><div id=\"ErrorMes\"></div><a class=\"close\" href=\"#close\"></a></div>");
                    $(div).appendTo("body");
                    $("#ErrorMes").empty();
                    $("#ErrorMes").append(vadiationMsg);

                    window.location = "#popup";
                    //return false;
                }
                else {
                    debugger;
                    otp('IN')
                    $("#openPopup").click();
                    //$("#timer").val("10");
                    //myTimer(20);
                    return true;
                }

            }, error: function (Result) {
                return false;
            }
        });
    }
    else {
        return false;
    }
});


$(document).on("click", "#btnCancel", function () {
    var url = window.location;
    url = window.location.href.split('#')[0];
    window.location.href = "/Login/Login";//url.split('?')[0];
});



function otp(type) {
    var txtMobile = $("#Mobile").val();
    var txtOTP = $("#txtOTP").val();
    var objProperty = {
        "Mode": type,
        "Mobile": txtMobile,
        "OTP": txtOTP
    };
    $.ajax({
        type: "POST",
        url: ajaxHomeLink.GenerateVerifiedOTP,
        data: JSON.stringify(objProperty),
        dataType: "json",
        contentType: 'application/json',
        success: function (Result) {
            if (Result == "Verified") {

                $('.js-modal-close').click();
                $("#btnFinalSubmitOtp").click();
            } else if (Result == "Open PopUp for OTP") {
                //alert(Result);
                return false;
            }
            else {
                //showSuccessToast(Result);
                //alert(Result);
                return false;
            }
        }, error: function (Result) {
            //alert("Error");
            return false;
        }
    });
}

function    myTimer(sec) {
    setTimeout(function () {
        var newSec = parseInt(sec) - parseInt(2);
        $("#timer").html(newSec.toString());
        if (newSec > 0) {
            myTimer(newSec);
        } else {
            $("#timer").html(newSec.toString())
        }
    }, 2000);
}


//function otp_forBrokerReg(type,MobileNo,OTP) {
//    var txtMobile = MobileNo;
//    var txtOTP = OTP;
//    var objProperty = {
//        "Mode": type,
//        "Mobile": txtMobile,
//        "OTP": txtOTP
//    };
//    $.ajax({
//        type: "POST",
//        url: ajaxHomeLink.GenerateVerifiedOTP,
//        data: JSON.stringify(objProperty),
//        dataType: "json",
//        contentType: 'application/json',
//        success: function (Result) {
//            if (Result == "Verified") {

//                $("#btnFinalSubmitOtp").click();
//            } else if (Result == "Open PopUp for OTP") {
//                //alert(Result);
//                return false;
//            }
//            else {
//                showSuccessToast(Result);
//                //alert(Result);
//                return false;
//            }
//        }, error: function (Result) {
//            alert("Error");;
//            return false;
//        }
//    });
//}
