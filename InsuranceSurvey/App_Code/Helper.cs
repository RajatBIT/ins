﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.IO;
using System.Text.RegularExpressions;

namespace InsuranceSurvery
{
    static public class Helper
    {
        public static string MakeThumbnail(string ImgName)
        {
            int wid = 0;
            int hei = 0;
            System.Web.UI.Control Controlurl = new System.Web.UI.Control();
            System.Drawing.Image myThumbnail150;
            System.Drawing.Image.GetThumbnailImageAbort myCallback = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image imagesize = System.Drawing.Image.FromFile(Controlurl.ResolveUrl(HttpContext.Current.Server.MapPath(@"~\CardsRealImages\" + ImgName)));
            Bitmap bitmapNew = new Bitmap(imagesize);

            if (imagesize.Width < 300)
                wid = imagesize.Width;
            else
                wid = 300;

            if (imagesize.Height < 300)
                hei = imagesize.Height;
            else
                hei = 300;

            myThumbnail150 = bitmapNew.GetThumbnailImage(wid, hei, myCallback, IntPtr.Zero);
            myThumbnail150.Save(Controlurl.ResolveUrl(HttpContext.Current.Server.MapPath(@"~\CardsThumbnailImage\")) + ImgName, System.Drawing.Imaging.ImageFormat.Jpeg);

            return ImgName;
        }

        public static bool ThumbnailCallback()
        {
            return true;
        }

        public static bool CheckFilePath(this string path)
        {
            bool result = false;
            result = File.Exists(path);
            return result;
        }
    }
}