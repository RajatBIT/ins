﻿using BusinessLayer.BL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class MISCISVRController : Controller
    {
        DataTable _dt;
        BLMISCISVR _isvr = new BLMISCISVR();

        int _claimId;
        int userId;

        #region Nature Extent Damage
        public ActionResult NatureExtentDamage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }

        }
        public JsonResult saveNatureExtentDamage(CSMISCISVRNatureExtentDamage nd)
        {
            string msg = null;
            int status = 0;
            try
            {
                nd.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _isvr.saveNatureExtentDamage(nd);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getNatureDmgls()
        {
            string msg = null;
            List<CSMISCNatureDmgls> listDmgLs = new List<CSMISCNatureDmgls>();
            try
            {
                listDmgLs = _isvr.getDmgls();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listDmgLs = listDmgLs ,msg=msg}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCovMen()
        {
            string msg = null;
            List<CSMISCCovMen> listCovMen = new List<CSMISCCovMen>();
            try
            {
                listCovMen = _isvr.getCovMen();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json( new { listCovMen = listCovMen ,msg=msg}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getNatureExtentDamage(int claimId)
        {
            string msg = null;
            List<CSMISCISVRNatureExtentDamage> listNED = new List<CSMISCISVRNatureExtentDamage>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
             

                listNED = _isvr.getNatureExtentDamage(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listNED =listNED,msg=msg}, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Intro Claim
        public ActionResult IntroClaim(string cid)
        {
            //UserID = Convert.ToInt32(Session["InsuranceUserId"]);
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        
        public JsonResult SaveMISCIntroClaim(CSMISCISVRIntroClaim ic)
        {

            string Msg = null;
            int Status = 0;
            if (ic.PK_MISCClaim_ID > 0)
            {
                ic.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _isvr.UpdateMISCIntroClaim(ic);
                Msg = Status > 0 ? "Update Successfully!" : "Failed!Try Again";
            }
            else
            {
                ic.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _isvr.SaveMISCIntroClaim(ic);
                Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult ShowMISCIntroClaim(int claimId)
        {
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCISVRIntroClaim list_MISCIntroClaim = new CSMISCISVRIntroClaim();
            list_MISCIntroClaim = _isvr.ShowMISCIntroClaim(claimId, userId);
            return Json(list_MISCIntroClaim, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCL_OP()
        {
            List<CSMISCCL_OP> listCL_OP = new List<CSMISCCL_OP>();

            listCL_OP = _isvr.getCL_OP();
            return Json(listCL_OP, JsonRequestBehavior.AllowGet);

        }

        #endregion


        #region Aspects of liab
        public ActionResult AspectLiab(string cid)
        {


            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {

                return View();
            }

        }
        public JsonResult saveAspectLiab(CSMISCISVRAspectLiab asplb)
        {
            string msg = null;
            int Status = 0;
            try
            {
                asplb.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _isvr.saveAspectLiab(asplb);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }


            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAspectsLiab(int claimId)
        {
            string msg = null;
            CSMISCISVRAspectLiab AL = new CSMISCISVRAspectLiab();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);

                AL = _isvr.getAspectsLiab(claimId, userId);
                TempData["tempNote"] = AL.Noteoccrsteye;
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json( new { AL=AL,msg=msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getCmtLiabSP()
        {
            string msg = null;
            List<CSMISCCmtLiabSP> listCmtLiabSP = new List<CSMISCCmtLiabSP>();
            try
            {
                listCmtLiabSP = _isvr.getCmtLiabSP();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCmtLiabSP = listCmtLiabSP ,msg=msg}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRemarkAdv()
        {
            string msg = null;

            List<CSMISCRemarkAdv> listRA = new List<CSMISCRemarkAdv>();
            try
            {
                listRA = _isvr.getRemarkAdv();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listRA=listRA,msg=msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAspectLiabPhotoDoc(string nos, int claimId)
        {
            string Msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCISVRAspectLiab al = new CSMISCISVRAspectLiab();
            al = _isvr.getAspectsLiab(claimId, userId);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && nos == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + al.Noteoccrsteye);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                al.Noteoccrsteye = fname;
                            }


                        }


                        fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                        file.SaveAs(fname);
                    }
                    al.UserID = userId;
                    Status = _isvr.saveAspectLiab(al);
                    Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
                }
                catch (Exception ex)
                {
                    Msg = "Error:" + ex.Message;
                }
            }


            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public string getMemoryStream_byBased64BinaryString(string Based64BinaryString, string fileName, string fullPath)
        {
            if (string.IsNullOrEmpty(Based64BinaryString))
            {
                return null;
            }

            string format = "";

            if (Based64BinaryString.Contains("data:image/jpeg;base64,"))
            {
                format = "jpeg";
            }
            else if (Based64BinaryString.Contains("data:image/jpg;base64,"))
            {
                format = "jpg";
            }
            else if (Based64BinaryString.Contains("data:image/gif;base64,"))
            {
                format = "gif";
            }
            else if (Based64BinaryString.Contains("data:image/png;base64,"))
            {
                format = "png";
            }
            else if (Based64BinaryString.Contains("data:image/bmp;base64,"))
            {
                format = "bmp";
            }
            //if (Based64BinaryString.Contains("data:text/plain;base64,"))
            //{
            //    format = "txt";
            //}

            string str = Based64BinaryString.Replace("data:image/jpeg;base64,", " ");//jpg check
            str = str.Replace("data:image/png;base64,", " ");//png check
            str = str.Replace("data:text/plain;base64,", " ");//text file check
            str = str.Replace("data:;base64,", " ");//zip file check
            str = str.Replace("data:application/zip;base64,", " ");//zip file check

            byte[] data = Convert.FromBase64String(str);

            MemoryStream ms = new MemoryStream(data, 0, data.Length);
            ms.Write(data, 0, data.Length);

            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

            //  fileName = Guid.NewGuid().ToString() + ".png";
            // fullPath = Server.MapPath("~/files/MISC/" + fileName);

            image.Save(fullPath);
            // result = "image uploaded successfully";
            string a = "hello";
            // return ms;
            return a;
        }































    }
}