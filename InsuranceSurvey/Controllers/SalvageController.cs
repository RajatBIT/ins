﻿using BusinessLayer.BL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class SalvageController : Controller
    {
        BLSalvage _sal = new BLSalvage();
        CommonService _cs = new CommonService();
        //
        // GET: /Salvage/
        #region Admin Salvage
        public ActionResult AdminSalvage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult saveAdminSalvage(CSAdminSalvage a)
        {
            string msg = null;
            int status = 0;
            int pk_AS_Id = 0;
            try
            {
                a.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                pk_AS_Id = _sal.saveAdminSalvage(a);
                msg = pk_AS_Id > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg, pk_AS_Id = pk_AS_Id }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAdminSalvagePhotoORDoc(string p1, string p2, string p3, string p4, int pk_AS_Id)
        {
            DataTable dt = new DataTable();
            string msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSAdminSalvage a = new CSAdminSalvage();
            dt = _cs.returnDTWithQuery_executeReader("select Photo1,Photo2,Photo3,Photo4 from tblAdminSalvage where PK_AS_ID =" + pk_AS_Id + " and UserID=" + userId + "");
            if (dt.Rows.Count > 0)
            {
                a.Photo1 = dt.Rows[0]["Photo1"].ToString();
                a.Photo2 = dt.Rows[0]["Photo2"].ToString();
                a.Photo3 = dt.Rows[0]["Photo3"].ToString();
                a.Photo4 = dt.Rows[0]["Photo4"].ToString();
            }
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFile = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFile;

                            if ( p1 == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + a.Photo1);
                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }
                                _cs.executeNonQueryWithQuery("update tblAdminSalvage set Photo1='" + fname + "' where PK_AS_ID=" + pk_AS_Id + " and UserID=" + userId + "");
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);

                                fname = "";
                                p1 = "no";
                            }
                           else if (p2 == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + a.Photo2);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }
                                _cs.executeNonQueryWithQuery("update tblAdminSalvage set Photo2='" + fname + "'where PK_AS_ID=" + pk_AS_Id + " and UserID=" + userId + "");
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);

                                fname = "";
                                p2 = "no";
                            }
                           else if ( p3 == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + a.Photo3);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }
                                _cs.executeNonQueryWithQuery("update tblAdminSalvage set Photo3='" + fname + "'where PK_AS_ID=" + pk_AS_Id + " and UserID=" + userId + "");
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);

                                fname = "";
                                p3 = "no";
                            }
                           else if ( p4 == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + a.Photo4);
                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }
                                _cs.executeNonQueryWithQuery("update tblAdminSalvage set Photo4='" + fname + "'where PK_AS_ID=" + pk_AS_Id + " and UserID=" + userId + "");
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);

                                fname = "";
                                p4 = "no";
                            }

                        }

                    }

                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAdminSalvage(int claimId)
        {
            string msg = null;
            List<CSAdminSalvage> listAS = new List<CSAdminSalvage>();
            try
            {
                listAS = _sal.getAdminSalvage(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAS = listAS, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Buyer Salvage
        public ActionResult BuyerSalvage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult saveSalvageQuery(CSSalvage_Query_Offer q)
        {
            string msg = null;
            int status = 0;
            try
            {
                q.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _sal.saveSalvageQuery(q);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveSalvageOffer(CSSalvage_Query_Offer o)
        {
            string msg = null;
            int status = 0;
            try
            {
                o.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _sal.saveSalvageOffer(o);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSalvageQuery(int claimId)
        {
            string msg = null;
            int status = 0;
            CSSalvage_Query_Offer q = new CSSalvage_Query_Offer();
            try
            {
                q = _sal.getSalvageQuery(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { q=q,msg=msg}, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSalvageOffer(int claimId)
        {
            string msg = null;
            int status = 0;
            CSSalvage_Query_Offer o = new CSSalvage_Query_Offer();
            try
            {
                o = _sal.getSalvageOffer(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { o = o, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
