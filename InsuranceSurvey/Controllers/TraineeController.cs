﻿using BusinessLayer;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class TraineeController : Controller
    {
        //
        // GET: /Trainee/

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SendRequestToSurvery()
        {
            return View();
        }

        public ActionResult TranieeAssignedTask()
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                ViewBag.ClaimDetails = BLAssignedClaims.BindTraineeAssignedTask(Convert.ToString(Session["InsuranceUserId"])); //BLGenrateClaim.ViewMyClaims(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
    }
}
