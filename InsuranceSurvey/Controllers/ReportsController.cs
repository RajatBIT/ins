﻿using BusinessLayer;
using BusinessLayer.Models;
using InsuranceSurvery;
using InsuranceSurvey.BusinessLayer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class ReportsController : Controller
    {
        //
        // GET: /Reports/

        public ActionResult Index()
        {
            return View();
        }

        public iTextSharp.text.Font NormalFontSmall = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 7, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        public iTextSharp.text.Font NormalFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        public iTextSharp.text.Font BoldFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        public iTextSharp.text.Font BoldFont12 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


        public PdfPCell GetFontWithStyle(string Text, iTextSharp.text.Font Font, int Alignment, string IsBorder = "", int Colspan = 0)
        {
            PdfPCell cell = new PdfPCell(new Phrase(Text, Font));
            cell.HorizontalAlignment = Alignment;
            cell.BorderColor = BaseColor.LIGHT_GRAY;
            if (IsBorder == "N")
            {
                cell.BorderWidth = 0f;
            }
            if (Colspan != 0)
            {
                cell.Colspan = Colspan;
            }
            return cell;
        }

        public PdfPCell GetSeprator(iTextSharp.text.Font Font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(":", Font));
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cell.BorderColor = BaseColor.LIGHT_GRAY;
            return cell;
        }

        public PdfPCell AddBlankRow()
        {
            Phrase phrase = new Phrase(Chunk.NEWLINE);
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderWidth = 0f;
            cell.PaddingTop = 5f;
            return cell;
        }

        public PdfPCell AddBlankRow(int Colspan)
        {
            Phrase phrase = new Phrase(Chunk.NEWLINE);
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderWidth = 0f;
            cell.PaddingTop = 5f;
            cell.Colspan = Colspan;
            return cell;
        }


        private PdfPCell ImageCell(string path, int align, float Height, float Width)
        {
            iTextSharp.text.Image image = null;
            path = Server.MapPath("~/MarineISVRPhotos/" + path);
            if (path.CheckFilePath())
            {
                image = iTextSharp.text.Image.GetInstance(path);
            }

            image.ScaleAbsoluteWidth(Width);
            image.ScaleAbsoluteHeight(Height);
            PdfPCell cell = new PdfPCell(image);
            cell.BorderColor = BaseColor.BLACK;
            cell.HorizontalAlignment = align;
            //cell.PaddingBottom = 0f;
            //cell.PaddingTop = 0f;
            return cell;
        }
        private PdfPCell ImageCellWithFullPath(string path, int align, float Height, float Width)
        {
            iTextSharp.text.Image image = null;
            //path = Server.MapPath("~/MarineISVRPhotos/" + path);
            if (path.CheckFilePath())
            {
                image = iTextSharp.text.Image.GetInstance(path);
            }

            image.ScaleAbsoluteWidth(Width);
            image.ScaleAbsoluteHeight(Height);
            PdfPCell cell = new PdfPCell(image);
            cell.BorderColor = BaseColor.BLACK;
            cell.HorizontalAlignment = align;
            //cell.PaddingBottom = 0f;
            //cell.PaddingTop = 0f;
            return cell;
        }



        [HttpGet]
        public FileResult GeneratePdf(string ClaimID, string InsdType, string InsdID, string Task)
        {
            //ClaimID = "1";
            //InsdType = "O";
            //InsdID = "1";
            if (Task == "JIR")
            {
                return GeneratePdfJIR(ClaimID);
            }
            else if (Task == "LOR")
            {
                return GeneratePdfLOR(ClaimID, InsdType, InsdID);
            }
            else if (Task == "FSR")
            {
                return GeneratePdfFSR(ClaimID, InsdType, InsdID);
            }
            else if (Task == "ISVR")
            {
                return GeneratePdfISVR(ClaimID, InsdType, InsdID);
            }
            else if (Task == "Reminder")
            {
                return GenerateReminderLOR(ClaimID, InsdType, InsdID);
            }
            else
            {
                return GeneratePdfClaimForm(ClaimID);
            }
        }

        public FileResult GeneratePdfMIS_ISVR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMotor = BLGenrateClaim.GetMIS_ISVR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMotor.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 85f, 45f);
                //Document document = new Document();
                //document.SetMargins(10f,10f,40f,20f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                Header objHeader = new Header();
                objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["LetterHead"].ToString());
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                objFooter.RefNo = dsMotor.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Insurers"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Address: " + dsMotor.Tables[0].Rows[0]["InsrAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("REF NO. " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                table.AddCell(GetFontWithStyle("INSURER CLAIM NO. " + dsMotor.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                cell = GetFontWithStyle("INSTANT SITE VISIT REPORT", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                if (dsMotor.Tables[0].Rows.Count > 0)
                {

                    table.AddCell(GetFontWithStyle("POLICY TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Loss"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["LossDT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PS_ALL"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PS_ALL_Place"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CL_OP"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CL_OP_DESC"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsuredName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED ADDRESS", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsuredAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("SOURCE OF ALLOTMENT OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["SourceAllotmentSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF SURVEY VISIT INITIATED", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["IntimationLossDT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF SURVEY VISIT COMPLETED", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AllotmentSurveyDT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }
                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMotor.Tables[1].Columns.Count);
                if (dsMotor.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMotor.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("NATURE & EXTENT OF DAMAGE (Affected Property)", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                //document.NewPage();
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });

                if (dsMotor.Tables[3].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("SURVEYOR’S REMARKS/ ADVICE TO THE INSURED.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[3].Rows.Count; i++)
                    {
                        cell = GetFontWithStyle("     - " + dsMotor.Tables[3].Rows[i]["RemarkAdv"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                    }
                    document.Add(table);
                }



                // Add ISVR Details

                if (dsMotor.Tables[2].Rows.Count > 0)
                {
                    document.NewPage();
                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                    cell = GetFontWithStyle("ASPECTS OF LIABILITY:", BoldFont, PdfPCell.ALIGN_LEFT, "N", 3);
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    table.AddCell(GetFontWithStyle("Comment on Liability under the scope of policy:", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["CmtLiabSP"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Estimation of Loss by Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["EstmateLossIns"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("Loss Reserve Recommendation(Rs.):", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["LossReserveRecom"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Basis of Reserve, Recommended:", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["BscLossReserveRecom"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Observation at site of loss:", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["ObsrvSiteLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Comment on Relevant Policy Conditions", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["CmtRelPolCond"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Circumtances of Loss (As stated by Insured)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["CircumLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Other Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["OthRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    document.Add(table);
                }

                // Starts New Page

                if (dsMotor.Tables[2].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.AddCell(AddBlankRow(1));
                    cell = GetFontWithStyle("Note on Occurence/Statement of eye witness/insured statement", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow(1));

                    string path = Server.MapPath("~/Files/MISC/") + dsMotor.Tables[2].Rows[0]["NoteOccrStEye"].ToString();

                    table.AddCell(ImageCellWithFullPath(path, PdfPCell.ALIGN_CENTER, 220f, 200f));
                    table.AddCell(GetFontWithStyle("Statement of eye witness/insured", BoldFont, PdfPCell.ALIGN_CENTER));
                    table.AddCell(AddBlankRow(1));
                    document.Add(table);
                }
                document.CloseDocument();
            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");

        }


        public FileResult GeneratePdfJIR(string ClaimID)
        {
            DataSet dsMotor = BLReports.GetMotorJIR(ClaimID);
            MemoryStream workStream = new MemoryStream();
            if (dsMotor.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;
                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 10f, 10f, 20f, 45f);

                //Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;

                document.Open();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Ref No. " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                cell = GetFontWithStyle("JOINT INSPECTION REPORT WITH INSURED OR HIS REPRESENTATIVE", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);
                Footer objFooter = new Footer();
                objFooter.RefNo = dsMotor.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Joint Incepection Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                if (dsMotor.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle("Vehicle Particulars (to be filled by the Insured)", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Registration Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["RCNO"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Registration", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["RCDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Chassis Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ChasisNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Engine Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["EngineNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Make", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Make"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Model", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Model"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Class of vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Class"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Type of Body", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["BodyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Seating Capacity", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Seating"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fuel use in vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Fuel"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Colour of the Vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Color"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fitness Valid upto", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FitnessUpto"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Permit type & Valid upto", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PermitType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Area of Operation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["OperationArea"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Registered Laden Weight", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["LadenWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Un-Laden Weight", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["UnLadenWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Tax / OTT paid", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TaxOTT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    table.AddCell(GetFontWithStyle("Anti-theft fitted Device", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ATFDevice"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    table.AddCell(GetFontWithStyle("Pre Accident condition of the vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PACondition"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Upload Registration Certificate", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("Yes", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Surveyor’s Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //D/L Particulars (to be filled by the Insured)
                    table.AddCell(GetFontWithStyle("D/L Particulars (to be filled by the Insured)", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Name of Driver, at the time of accident of the Insured Vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DriverName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Driving License Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Issued", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["IssueDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Valid up to", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ValidUpTO"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("D/L issuing Authority", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLAuthority"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Type of License", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("4 wheeler", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Endorsement (If Any)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Endorsment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Remarks by Surveyors if any", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    //About occurrence (To be filled by the Insured)
                    table.AddCell(GetFontWithStyle("About occurrence (To be filled by the Insured)", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Date & Time of Accident", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AccDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Place of Accident", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AccPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Spot Survey", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Police Report/FIR/Panchnama", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FIR"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fire Brigade Report ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FireReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Third Party Property Loss", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TPPLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Third Party Body Injury", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TPBInjury"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Damaged side", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DamageSides"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Cause of Loss as Described by the Insured or his representative", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["OccRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));


                    // Estimate of Loss (to be filled by the Insured)
                    table.AddCell(GetFontWithStyle("Estimate of Loss (to be filled by the Insured)", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Name of Repairer", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AccDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Repairer", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Repairer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Mobile No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Mobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Email", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Email"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Total Estimated cost for Repair at on this stage", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TotalCost"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Upload Estimate", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["UploadEstimate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Note:- Kindly note additional estimates would be submitted after dismantling of the subject vehicle, if necessary.", BoldFont, PdfPCell.ALIGN_LEFT, "", 3));

                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }

                if (dsMotor.Tables[1].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("SURVEYOR’S REMARKS/ ADVICE TO THE INSURED.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[1].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[1].Rows[i]["RCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[1].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    document.Add(table);
                }


                if (dsMotor.Tables[2].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });

                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[2].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[2].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[2].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[2].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    document.Add(table);
                }

                // Add Contact Person Details

                if (dsMotor.Tables[3].Rows.Count > 0)
                {
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.50f, 0.50f });
                    cell = GetFontWithStyle("For: " + dsMotor.Tables[3].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("For: " + dsMotor.Tables[3].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Contact Person: " + dsMotor.Tables[3].Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Mobile No.: " + dsMotor.Tables[3].Rows[0]["Mobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Mobile No.: " + dsMotor.Tables[3].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMotor.Tables[3].Rows[0]["Email"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMotor.Tables[3].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    document.Add(table);
                }
                document.Close();

                //byte[] byteInfo = workStream.ToArray();
                //workStream.Write(byteInfo, 0, byteInfo.Length);
                //workStream.Position = 0;

            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public FileResult GeneratePdfISVR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMotor = BLGenrateClaim.GetMarineISVR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMotor.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 85f, 45f);
                //Document document = new Document();
                //document.SetMargins(10f,10f,40f,20f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                Header objHeader = new Header();
                objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["LetterHead"].ToString());
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                objFooter.RefNo = dsMotor.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Insurer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Address: " + dsMotor.Tables[0].Rows[0]["InsrAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("REF NO. " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                table.AddCell(GetFontWithStyle("INSURER CLAIM NO. " + dsMotor.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                cell = GetFontWithStyle("INSTANT SITE VISIT REPORT", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                if (dsMotor.Tables[0].Rows.Count > 0)
                {

                    table.AddCell(GetFontWithStyle("POLICY TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DateOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED ADDRESS", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsdAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNOR", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CARRYING LORRY REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }
                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMotor.Tables[1].Columns.Count);
                if (dsMotor.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMotor.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("INVOICE/LORRY RECEIPTS", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The consignment was transit vide following Documents.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                if (dsMotor.Tables[2].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(dsMotor.Tables[2].Columns.Count);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("DAMAGE DETAIL OF CONSIGNMENT RECEIVED", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = 8;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[2].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[2].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[2].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[2].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[2].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = 8;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                }
                if (dsMotor.Tables[0].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("CAUSES OF LOSS AS DESCRIBED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("DELAY IN INTIMATION (IF ANY) AS CLARIFIED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DelayInINTimation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("DELAY IN TAKING DELIVERY (IF ANY) AS DESCRIBED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DelayINTakingDelivery"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    document.Add(table);
                }

                //document.NewPage();
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });

                if (dsMotor.Tables[4].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("SURVEYOR’S REMARKS/ ADVICE TO THE INSURED.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[4].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[4].Rows[i]["RCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[4].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    document.Add(table);
                }


                if (dsMotor.Tables[3].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[3].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[3].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[3].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[3].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    // Add ISVR Details

                    if (dsMotor.Tables[5].Rows.Count > 0)
                    {
                        table = new PdfPTable(3);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                        table.AddCell(GetFontWithStyle("Estimation of Loss by Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[5].Rows[0]["EstimationOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("Recommended Reserve for the Insurers", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[5].Rows[0]["RecomendedReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("Basis of Reserve, Recommended", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[5].Rows[0]["BasisOfReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[5].Rows[0]["Remarks"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        document.Add(table);
                    }

                    // Starts New Page

                    if (dsMotor.Tables[6].Rows.Count > 0)
                    {
                        document.NewPage();
                        table = new PdfPTable(2);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.5f, 0.5f });
                        table.AddCell(AddBlankRow(2));
                        cell = GetFontWithStyle("Photo Sheet", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                        cell.Colspan = 2;
                        table.AddCell(cell);
                        table.AddCell(AddBlankRow(2));

                        table.AddCell(ImageCell(dsMotor.Tables[6].Rows[0]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                        table.AddCell(ImageCell(dsMotor.Tables[6].Rows[1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[0]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));

                        table.AddCell(AddBlankRow(2));

                        table.AddCell(ImageCell(dsMotor.Tables[6].Rows[2]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                        table.AddCell(ImageCell(dsMotor.Tables[6].Rows[3]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[2]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                        table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[3]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                        //document.NewPage();
                        for (int i = 4; i < dsMotor.Tables[6].Rows.Count; i++)
                        {
                            if (dsMotor.Tables[6].Rows[i]["PHOTOTYPE"].ToString() == "5")
                            {
                                cell = ImageCell(dsMotor.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 500f, 430);
                                cell.Colspan = 2;
                                table.AddCell(cell);
                                cell = GetFontWithStyle(dsMotor.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER);
                                cell.Colspan = 2;
                            }
                        }
                        table.AddCell(cell);
                        document.Add(table);
                    }
                }
                document.CloseDocument();

            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public partial class Footer : PdfPageEventHelper
        {
            public iTextSharp.text.Font NormalFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            public string RefNo, ReportName, Signature;
            public PdfPCell GetFontWithStyle(string Text, iTextSharp.text.Font Font, int Alignment, string IsBorder = "", int Colspan = 0)
            {
                PdfPCell cell = new PdfPCell(new Phrase(Text, Font));
                cell.HorizontalAlignment = Alignment;
                if (IsBorder == "N")
                {
                    cell.BorderWidth = 0f;
                }
                if (Colspan != 0)
                {
                    cell.Colspan = Colspan;
                }
                return cell;
            }


            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                PdfPTable footerTbl = new PdfPTable(3);
                footerTbl.TotalWidth = 500f;
                footerTbl.WidthPercentage = 100f;
                footerTbl.LockedWidth = true;
                footerTbl.SetWidths(new float[] { 0.3f, 0.4f, 0.2f });
                PdfPCell cell;
                cell = GetFontWithStyle("Signature", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Format designed by:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle(ReportName, NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                if (Signature.CheckFilePath())
                {
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Signature);
                    jpg.ScaleAbsoluteHeight(20f);
                    jpg.ScaleAbsoluteWidth(20f);
                    cell = new PdfPCell(jpg);
                    cell.Border = 0;
                }
                else
                {
                    cell.Border = 0;
                    cell = GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                }
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Visit us at: www.saisurveyors.com", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Ref No. " + RefNo, NormalFont, PdfPCell.ALIGN_LEFT, "N");
                footerTbl.AddCell(cell);
                footerTbl.WriteSelectedRows(0, -1, 50, 50, writer.DirectContent);
            }

        }

        public partial class Header : PdfPageEventHelper
        {
            public string path;
            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                PdfPTable table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(path);
                jpg.ScaleAbsoluteHeight(80f);
                jpg.ScaleAbsoluteWidth(550f);
                PdfPCell cell = new PdfPCell(jpg);
                cell.BorderWidth = 0f;
                table.AddCell(cell);
                table.WriteSelectedRows(0, -1, 0, 680, writer.DirectContent);
            }

        }

        public FileResult GeneratePdfLOR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsLOR = BLGenrateClaim.GetMarineLOR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsLOR.Tables.Count > 0)
            {
                PdfPTable table = null;
                PdfPCell cell = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                Header objHeader = new Header();
                objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["LetterHead"].ToString());
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                objFooter.RefNo = dsLOR.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("Letter of Requirment", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdAddress"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), BoldFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Ref No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Kindly Attention: " + dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Sub.: Marine claim under transit Policy No. " + dsLOR.Tables[0].Rows[0]["PolicyNo"].ToString() + " on account of damage during the course of transit from " + dsLOR.Tables[0].Rows[0]["DispatchedFROM"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This refer to our visit at your premises " + dsLOR.Tables[0].Rows[0]["PlaceofSurvey"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + ",  when we conducted survey/inspection for reported loss to the consignment vide Invoice No. " + dsLOR.Tables[1].Rows[0]["Inv. No."].ToString() + " Dated " + dsLOR.Tables[1].Rows[0]["Invoice Date"].ToString() + ", & " + dsLOR.Tables[1].Columns[6].ToString() + " " + dsLOR.Tables[1].Rows[0][6].ToString() + " Dated " + dsLOR.Tables[1].Rows[0][7].ToString() + "."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We hope that, you have immediately acted upon our advices in context to minimise the loss.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We have shared the observations of our visit and acknowledged the receipt of part information vide JIR No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("In order to enable us to proceed further, we draw your kind attention on pending information/documents as advised in JIR, for your convenience, list of pending documents/information reproduced below.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);
                if (dsLOR.Tables[2].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());


                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                            //list.SetListSymbol("\u2022");
                            //list.IndentationLeft = 50f;
                            //list.Add(new ListItem(dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    //document.Add(list);
                }
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Please note, we may ask for some further information or documents, if required later, on perusal of information submitted by you.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Your prompt response will be highly appreciated and help us to expedite the proceedings of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("You may also upload the requisite documents on our website www.saisurveyors.com and link with claim ref no. if any difficulty in uploading documents please email request documents helpforuploadingdesk@saisurveyors.com .", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Please feel free to contact the under singed for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                document.Add(table);
                document.CloseDocument();//
            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public FileResult GenerateReminderLOR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsLOR = BLGenrateClaim.GetMarineLORReminder(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsLOR.Tables.Count > 0)
            {
                PdfPTable table = null;
                PdfPCell cell = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                Header objHeader = new Header();
                objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["LetterHead"].ToString());
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                objFooter.RefNo = dsLOR.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                string ReminderSrno = string.Empty;
                if (dsLOR.Tables[3].Rows[0]["ReminderNo"].ToString() == "1")
                {
                    ReminderSrno = "1st ";
                }
                else if (dsLOR.Tables[3].Rows[0]["ReminderNo"].ToString() == "2")
                {
                    ReminderSrno = "2nd ";
                }
                else
                {
                    ReminderSrno = "Final ";
                }
                table.AddCell(GetFontWithStyle(ReminderSrno + "REMINDER VIDE EMAIL", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This is " + ReminderSrno + "reminder for your doing needful for our earlier request letter for pending documents/information of your claim."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("We wish to draw your kind attention for pending requirements requested in our letter no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + " (attached for your ready reference)."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("We will appreciate, if you would give immediate attention to clarify the pendency of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Please feel free to contact the under singed for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);
                document.NewPage();
                // LOR Work

                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("Letter of Requirment", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdAddress"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), BoldFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Ref No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Kindly Attention: " + dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Sub.: Marine claim under transit Policy No. " + dsLOR.Tables[0].Rows[0]["PolicyNo"].ToString() + " on account of damage during the course of transit from " + dsLOR.Tables[0].Rows[0]["DispatchedFROM"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This refer to our visit at your premises " + dsLOR.Tables[0].Rows[0]["PlaceofSurvey"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + ",  when we conducted survey/inspection for reported loss to the consignment vide Invoice No. " + dsLOR.Tables[1].Rows[0]["Inv. No."].ToString() + " Dated " + dsLOR.Tables[1].Rows[0]["Invoice Date"].ToString() + ", & " + dsLOR.Tables[1].Columns[6].ToString() + " " + dsLOR.Tables[1].Rows[0][6].ToString() + " Dated " + dsLOR.Tables[1].Rows[0][7].ToString() + "."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We hope that, you have immediately acted upon our advices in context to minimise the loss.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We have shared the observations of our visit and acknowledged the receipt of part information vide JIR No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("In order to enable us to proceed further, we draw your kind attention on pending information/documents as advised in JIR, for your convenience, list of pending documents/information reproduced below.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);

                if (dsLOR.Tables[2].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());


                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                            //list.SetListSymbol("\u2022");
                            //list.IndentationLeft = 50f;
                            //list.Add(new ListItem(dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    //document.Add(list);
                }
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Please note, we may ask for some further information or documents, if required later, on perusal of information submitted by you.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Your prompt response will be highly appreciated and help us to expedite the proceedings of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("You may also upload the requisite documents on our website www.saisurveyors.com and link with claim ref no. if any difficulty in uploading documents please email request documents helpforuploadingdesk@saisurveyors.com .", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Please feel free to contact the under singed for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                document.Add(table);
                document.CloseDocument();//
            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public FileResult GeneratePdfFSR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMotor = BLGenrateClaim.GetMarineFSR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMotor.Tables.Count > 0)
            {

                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                document.OpenDocument();
                int Table1Count = dsMotor.Tables[0].Rows.Count;
                if (Table1Count > 0)
                {
                    Header objHeader = new Header();
                    objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["LetterHead"].ToString());
                    writer.PageEvent = objHeader;
                    Footer objFooter = new Footer();
                    objFooter.RefNo = dsMotor.Tables[0].Rows[0]["ClaimID"].ToString();
                    objFooter.ReportName = "Final Survey Report";
                    objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["DSign"].ToString());
                    writer.PageEvent = objFooter;
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Ref No. " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. " + dsMotor.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    cell = GetFontWithStyle("Final Survey Report", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.36f, 0.02f, 0.8f });

                    table.AddCell(GetFontWithStyle("Previous Reports", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PreviousReports"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("1. Policy Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Policy Type", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsuredName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["InsuredAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Period of Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Period"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Coverage", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Coverage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Basis of Evaluation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["BasisValution"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("2. Survey Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Source & Date of Instruction", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("Competent Authority had deputed the undersign as Independent Surveyor & Loss Assessor on " + dsMotor.Tables[0].Rows[0]["InstructionDate"].ToString() + " for the captioned loss", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date/Time of First Visit", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DateOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Place of Survey", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PlaceofSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Delay in intimation  (If any)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DelayInINTimation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Detail of other Survey Visits", BoldFont, PdfPCell.ALIGN_CENTER, "Y", 3));
                    if (dsMotor.Tables[7].Rows.Count > 0)
                    {
                        PdfPTable PdfTable1 = new PdfPTable(4);
                        PdfTable1.TotalWidth = 500f;
                        PdfTable1.WidthPercentage = 100f;
                        PdfTable1.LockedWidth = true;
                        PdfTable1.SetWidths(new float[] { 0.15f, 0.15f, 0.3f, 0.4f });
                        PdfTable1.AddCell(GetFontWithStyle("Date of Visit", BoldFont, PdfPCell.ALIGN_CENTER, "", 2));
                        PdfTable1.AddCell(GetFontWithStyle("Place of Visit", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("Purpose of Visit", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("Start Journey", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("End Journey", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER, "N"));
                        PdfTable1.AddCell(GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER, "N"));
                        for (int rows = 0; rows < dsMotor.Tables[7].Rows.Count; rows++)
                        {
                            for (int column = 0; column < dsMotor.Tables[7].Columns.Count; column++)
                            {
                                cell = new PdfPCell(GetFontWithStyle(dsMotor.Tables[7].Rows[rows][column].ToString(), NormalFont, PdfPCell.ALIGN_CENTER));
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable1.AddCell(cell);
                            }
                        }
                        cell = new PdfPCell(PdfTable1);
                        cell.Colspan = 3;
                        table.AddCell(cell);
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));//Nature of Tra
                    }
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("3. Consignment Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Nature of Transit", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Consignor", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Consignor", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsignorAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Consignee", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Consignee", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsigeeAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Arrival at Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsigeeAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Delivery to Consignee", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsigeeAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Delay in taking Delivery (If any)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DelayINTakingDelivery"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Transit From & To", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Transit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Carrying Lorry Regn. No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Consignment Weight ( in Kg's)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Carrying Lorry GVW (Kg.)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Carrying Lorry ULW (Kg.)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Description of Packing", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("External Condition of Packing when delivered to consignee", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(AddBlankRow(3));
                    document.Add(table);
                }

                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMotor.Tables[1].Columns.Count);
                if (dsMotor.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMotor.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("4. Consignment Document Particulars", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The Insured consignment was dispatched vide following invoice/s and LR/s detailed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                if (dsMotor.Tables[2].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(4);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    cell = GetFontWithStyle("5. CONSIGNMENT JOURENEY PARTICULARS", BoldFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("All the events of the Insured consignment journey are summarized as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    if (dsMotor.Tables[2].Rows.Count == 1 && dsMotor.Tables[2].Rows[0]["Mode"].ToString() == "Road")
                    {
                        PdfTable.AddCell(GetFontWithStyle("Single Transhipment by Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                        PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[0]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        for (int rows = 0; rows < dsMotor.Tables[2].Rows.Count; rows++)
                        {
                            string mode = "";
                            switch (rows)
                            {
                                case 0:
                                    mode = "First leg of journey by";
                                    break;
                                case 1:
                                    mode = "Second leg of journey by";
                                    break;
                                case 2:
                                    mode = "Third leg of journey by";
                                    break;
                                case 3:
                                    mode = "Fourth leg of journey by";
                                    break;

                            }
                            if (dsMotor.Tables[2].Rows[rows]["Mode"].ToString() == "Road")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMotor.Tables[2].Rows[rows]["Mode"].ToString() == "Air")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Air", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Airway Bill Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMotor.Tables[2].Rows[rows]["Mode"].ToString() == "Sea")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Sea", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMotor.Tables[2].Rows[rows]["Mode"].ToString() == "Rail")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Rail", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Destination Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMotor.Tables[2].Rows[rows]["Mode"].ToString() == "Other")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Courier/Others", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Booking Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Delivery Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMotor.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                        }
                        document.Add(PdfTable);
                    }
                }


                if (dsMotor.Tables[3].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(dsMotor.Tables[3].Columns.Count);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    int Table3Count = dsMotor.Tables[3].Columns.Count;
                    cell = GetFontWithStyle("6. NATURE & EXTENT OF DAMAGE TO CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The Insured consignment was dispatched vide following invoice/s and LR/s detailed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[3].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[3].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[3].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[3].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[3].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow(dsMotor.Tables[3].Columns.Count);
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                }

                if (dsMotor.Tables[0].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("7. CAUSES OF LOSS AS DESCRIBED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    document.Add(table);
                }

                // Assessment Details

                PdfTable = new PdfPTable(dsMotor.Tables[4].Columns.Count);
                if (dsMotor.Tables[4].Rows.Count > 0)
                {
                    int Colspan = dsMotor.Tables[4].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("8. Invoice Value of the Loss", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The Invoice Value Loss is Computed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Assessment Sheet", BoldFont, PdfPCell.ALIGN_CENTER, "", 11);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_CENTER, "", 5);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Claimed", BoldFont, PdfPCell.ALIGN_CENTER, "", 3);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Assessed", BoldFont, PdfPCell.ALIGN_CENTER, "", 3);
                    PdfTable.AddCell(cell);


                    for (int rows = 0; rows < dsMotor.Tables[4].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMotor.Tables[4].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[4].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMotor.Tables[4].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[4].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                // Calculation Details

                PdfTable = new PdfPTable(2);
                if (dsMotor.Tables[9].Rows.Count > 0)
                {
                    int Colspan = dsMotor.Tables[9].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("9. Adjustment of Loss,as per policy", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    cell = GetFontWithStyle("Adjustment of Loss,as per Policy Terms", BoldFont, PdfPCell.ALIGN_CENTER, "", 2);
                    cell.Colspan = 2;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMotor.Tables[9].Rows.Count; rows++)
                    {
                        //if (rows == 0)
                        //{
                        //    for (int column = 0; column < dsMotor.Tables[4].Columns.Count; column++)
                        //    {
                        //        cell = new PdfPCell(new Phrase(new Chunk(dsMotor.Tables[4].Columns[column].ColumnName, BoldFont)));
                        //        cell.BackgroundColor = BaseColor.CYAN;
                        //        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        //        PdfTable.AddCell(cell);
                        //    }
                        //}
                        for (int column = 0; column < dsMotor.Tables[9].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMotor.Tables[9].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                // FSR Details

                if (dsMotor.Tables[8].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("10. Salvage", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[8].Rows[0]["Salvage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("11. Comment on Policy Conditions", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[8].Rows[0]["CommentPolicy"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("12. Concurrence", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[8].Rows[0]["Concurrence"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());

                    //table = new PdfPTable(1);
                    //table.TotalWidth = 500f;
                    //table.WidthPercentage = 100f;
                    //table.LockedWidth = true;
                    //table.SetWidths(new float[] { 0.10f });
                    //table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("13. Remark Pending Documents", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Insured are requested to obtain following documents, at the time of settlement of the claim. ", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMotor.Tables[5].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[5].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("     - " + dsMotor.Tables[5].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());

                    //List list = new List(List.UNORDERED, 20f);
                    //for (int i = 0; i < dsMotor.Tables[5].Rows.Count; i++)
                    //{
                    //    if (dsMotor.Tables[5].Rows[i]["LCID"].ToString() == "")
                    //    {
                    //        list.SetListSymbol("\u2022");
                    //        list.IndentationLeft = 50f;
                    //        list.Add(new ListItem(dsMotor.Tables[5].Rows[i]["Name"].ToString(), NormalFont));
                    //    }
                    //}
                    //document.Add(table);
                    //document.Add(list);

                    cell = GetFontWithStyle("14. Other Remarks if any", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMotor.Tables[8].Rows[0]["OtherRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    document.Add(table);
                }


                if (dsMotor.Tables[5].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("ENCLOSURES", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsMotor.Tables[5].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[5].Rows[i]["LCID"].ToString() != "")
                        {
                            list.SetListSymbol("\u2022");
                            list.IndentationLeft = 50f;
                            list.Add(new ListItem(dsMotor.Tables[5].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    document.Add(list);


                }


                // Add ISVR Details


                if (dsMotor.Tables[6].Rows.Count > 0)
                {
                    document.NewPage();
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(AddBlankRow(2));
                    cell = GetFontWithStyle("Photo Sheet", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow(2));

                    table.AddCell(ImageCell(dsMotor.Tables[6].Rows[0]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    table.AddCell(ImageCell(dsMotor.Tables[6].Rows[1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[0]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));

                    table.AddCell(AddBlankRow(2));

                    table.AddCell(ImageCell(dsMotor.Tables[6].Rows[2]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    table.AddCell(ImageCell(dsMotor.Tables[6].Rows[3]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[2]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[6].Rows[3]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    //document.NewPage();
                    for (int i = 4; i < dsMotor.Tables[6].Rows.Count; i++)
                    {
                        if (dsMotor.Tables[6].Rows[i]["PHOTOTYPE"].ToString() == "5")
                        {
                            cell = ImageCell(dsMotor.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 500f, 430);
                            cell.Colspan = 2;
                            table.AddCell(cell);
                            cell = GetFontWithStyle(dsMotor.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER);
                            cell.Colspan = 2;
                        }
                    }
                    table.AddCell(cell);
                    document.Add(table);
                }

                document.CloseDocument();
            }
            //return new FileStreamResult(workStream, "application/pdf");
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public FileResult GeneratePdfClaimForm(string ClaimID)
        {
            DataSet dsMotor = BLReports.GetMotorClaimForm(ClaimID);
            MemoryStream workStream = new MemoryStream();
            if (dsMotor.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;
                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 10f, 10f, 20f, 45f);

                //Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;

                document.Open();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("ClaimID: " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("No. " + dsMotor.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Claim Form", BoldFont12, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("(This is electronic claim form to be submitted by the Insured only, submission of this form does not comply the requirement of claim form prescribed by Insurers. This form had been design for help of surveyor to expedite the process of submission of Survey Report)", BoldFont12, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                table.AddCell(cell);
                document.Add(table);
                //Footer objFooter = new Footer();
                //objFooter.RefNo = dsMotor.Tables[0].Rows[0]["ClaimID"].ToString();
                //objFooter.ReportName = "Joint Incepection Report";
                //objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMotor.Tables[0].Rows[0]["DSign"].ToString());
                //writer.PageEvent = objFooter;

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                if (dsMotor.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle("Policy Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Policy No. /Cover Note No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Period of the Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Period"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of the Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[1].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of the Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[1].Rows[0]["InsdAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Registration No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["RCNO"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Hire Purchase Agreement/ Hypothecation Clause", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("?", NormalFont, PdfPCell.ALIGN_LEFT));
                    ////////////// Vehicle Particulars
                    table.AddCell(GetFontWithStyle("Vehicle Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Registration Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["RCNO"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Registration", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["RCDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Chassis Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ChasisNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Engine Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["EngineNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Make", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Make"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Model", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Model"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Class of vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Class"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Type of Body", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["BodyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Seating Capacity", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Seating"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fuel use in vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Fuel"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Colour of the Vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Color"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fitness Valid upto", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FitnessUpto"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Permit type & Valid upto", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PermitType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Area of Operation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["OperationArea"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Registered Laden Weight", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["LadenWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Un-Laden Weight", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["UnLadenWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Tax / OTT paid", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TaxOTT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    table.AddCell(GetFontWithStyle("Anti-theft fitted Device", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ATFDevice"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    table.AddCell(GetFontWithStyle("Pre Accident condition of the vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PACondition"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Upload Registration Certificate", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("Yes", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Surveyor’s Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //D/L Particulars (to be filled by the Insured)
                    table.AddCell(GetFontWithStyle("D/L Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Name of Driver, at the time of accident of the Insured Vehicle", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DriverName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Driving License Number", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date of Issued", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["IssueDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Valid up to", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ValidUpTO"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("D/L issuing Authority", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLAuthority"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Type of License", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("4 wheeler", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Endorsement (If Any)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Endorsment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Remarks by Surveyors if any", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DLRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                    //About occurrence (To be filled by the Insured)
                    table.AddCell(GetFontWithStyle("Accident Particuars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Date & Time of Accident", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AccDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Place of Accident", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["AccPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Spot Survey", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Police Report/FIR/Panchnama", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FIR"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Fire Brigade Report ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["FireReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Third Party Property Loss", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TPPLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Third Party Body Injury", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["TPBInjury"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Damaged side", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DamageSides"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Cause of Loss as Described by the Insured or his representative", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["OccRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));


                    // Previous Claim History If any PC.PolicyNo as PPolicyNo,Insurer,NoOfAcc,DateOfAcc,ClaimedAmt,CompensationAmt,PC.Remark as PCRemark
                    table.AddCell(GetFontWithStyle("Previous Claim History If any", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Previous Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["PPolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Insurers ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["Insurer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("No. of Accident in Previous Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["NoOfAcc"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date/s of Accident", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["DateOfAcc"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Claimed Amount/s", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["ClaimedAmt"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Compensation Amount Received (Total)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMotor.Tables[0].Rows[0]["CompensationAmt"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    //document.Add(table);
                    table.AddCell(GetFontWithStyle("I ------------------- hereby declared, that the above information given by me is absolutely true and correct as per best of my knowledge. I have not hide any fact of material in above, I will be solely responsible to produce evidence in support of above material facts in, future if required by any authorized person.", NormalFontSmall, PdfPCell.ALIGN_LEFT, "N", 3));

                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    table.AddCell(GetFontWithStyle("Signature", NormalFont, PdfPCell.ALIGN_LEFT, "N", 3));
                    document.Add(table);
                }


                document.Close();

                //byte[] byteInfo = workStream.ToArray();
                //workStream.Write(byteInfo, 0, byteInfo.Length);
                //workStream.Position = 0;

            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        private MemoryStream getPageCountOnFooter(MemoryStream workStream)
        {
            byte[] finalBytes;
            PdfReader reader = new PdfReader(workStream.ToArray());
            //Document document1 = new Document(new iTextSharp.text.Rectangle(550f, 680f), 22f, 0f, 0f, 0f);
            Document document1 = new Document(new iTextSharp.text.Rectangle(550f, 680f), 22f, 0f, 0f, 0f);
            MemoryStream ms = new MemoryStream();
            PdfWriter writer1 = PdfWriter.GetInstance(document1, ms);
            writer1.CloseStream = false;
            document1.Open();
            for (var i = 1; i <= reader.NumberOfPages; i++)
            {
                document1.NewPage();
                var baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                var importedPage = writer1.GetImportedPage(reader, i);
                var contentByte = writer1.DirectContent;
                contentByte.BeginText();
                contentByte.SetFontAndSize(baseFont, 8);
                string TotalPages = reader.NumberOfPages.ToString();
                var multiLineString = "Page: " + i.ToString() + " of " + TotalPages;
                contentByte.ShowTextAligned(PdfContentByte.ALIGN_LEFT, multiLineString, document1.PageSize.GetRight(50), 10, 0);
                contentByte.EndText();
                contentByte.AddTemplate(importedPage, 0, 0);
            }

            document1.Close();
            finalBytes = ms.ToArray();
            ms.Write(finalBytes, 0, finalBytes.Length);
            ms.Position = 0;
            return ms;
        }

        //public ActionResult SendEmail(MemoryStream workStream)
        //{

        //    byte[] byteInfo = workStream.ToArray();
        //    workStream.Write(byteInfo, 0, byteInfo.Length);
        //    workStream.Position = 0;

        //    MailMessage mm = new MailMessage("ashish.rockstaar@gmail.com", "ashisharora1012@gmail.com");
        //    mm.Subject = "iTextSharp PDF";
        //    mm.Body = "iTextSharp PDF Attachment";
        //    mm.Attachments.Add(new Attachment(new MemoryStream(byteInfo), "iTextSharpPDF.pdf"));
        //    mm.IsBodyHtml = true;
        //    SmtpClient smtp = new SmtpClient();
        //    smtp.Host = "smtp.gmail.com";
        //    smtp.EnableSsl = false;
        //    NetworkCredential NetworkCred = new NetworkCredential();
        //    NetworkCred.UserName = "------";
        //    NetworkCred.Password = "------";
        //    smtp.UseDefaultCredentials = true;
        //    smtp.Credentials = NetworkCred;
        //    smtp.Port = 587;
        //    smtp.Send(mm);
        //    return View();
        //}


    }
}
