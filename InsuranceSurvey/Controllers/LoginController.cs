﻿using BusinessLayer;
using BusinessLayer.Models;
using InsuranceSurvey;
using InsuranceSurvey.BusinessLayer;
using System;
using System.Data;
using System.Web.Mvc;

namespace AniClean.Controllers
{
    public class LoginController : Controller
    {
        //
        // GET: /Login/

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(CSLogin objModels, string Command)
        {
            try
            {
                DataTable dtResult = new DataTable();
                if (objModels.LoginId.ToLower() == "admin" && objModels.Password.ToLower() == "1234")
                {
                    Session["InsuranceUserId"] = "1";
                    return RedirectToAction("Index", "SuperAdmin");

                }
                else if (objModels.LoginId.ToLower() != "admin")
                {
                    dtResult = BLSurveyorsRegistration.SurveyorLogin(objModels.LoginId, objModels.Password);
                    if (dtResult.Rows.Count > 0)
                    {
                        if (dtResult.Rows[0]["Flag"].ToString() == "Y")
                        {
                            Session["InsuranceUserId"] = Convert.ToString(dtResult.Rows[0]["FK_SurId"]);
                            Session["UserName"] = Convert.ToString(dtResult.Rows[0]["UserName"]);
                            if (dtResult.Rows[0]["UserType"].ToString().ToLower() == "t".ToLower())
                            {
                                Session["UserType"] = "Trainee";
                                return RedirectToAction("Index", "Survery");
                                //return RedirectToAction("Index", "Trainee");
                            }
                            else
                            {
                                Session["UserType"] = "Survery";
                                return RedirectToAction("Index", "Survery");
                            }

                        }
                        else if (dtResult.Rows[0]["Flag"].ToString() == "E")
                        {
                            TempData["msg"] = "Please confirm your account by login to your email account";
                            return RedirectToAction("Login", "Login");
                        }
                        else if (dtResult.Rows[0]["Flag"].ToString() == "B")
                        {
                            TempData["msg"] = "Your account has been blocked by admin";
                            return RedirectToAction("Login", "Login");
                        }
                        else// if (dtResult.Rows[0]["Flag"].ToString() == "N")
                        {
                            TempData["msg"] = "Your account is not verified by admin yet.";
                            return RedirectToAction("Login", "Login");
                        }
                    }
                    else
                    {
                        TempData["msg"] = "Invalid User Id & Password";
                        return RedirectToAction("Login", "Login");
                    }
                }
                //else //if (objModels.LoginId == "superadmin" && objModels.Password == "12345")
                //{
                //    Session["InsuranceUserId"] = "1";
                //    return RedirectToAction("Index", "SuperAdmin");
                //}

            }
            catch (Exception ex)
            {
                ViewData["msg"] = "Error " + ex.Message;

            }

            return RedirectToAction("login", "login");
        }

        public ActionResult InsuredLogin()
        {
            return View();
        }

        [HttpPost]
        public ActionResult InsuredLogin(CSLogin objModels, string Command)
        {
            DataTable dtResult = new DataTable();
            try
            {
                dtResult = BLInsuredRegistration.InsuedLogin(objModels.LoginId, objModels.Password);

                if (dtResult.Rows.Count > 0)
                {
                    Session["UserName"] = Convert.ToString(dtResult.Rows[0]["UserName"]);
                    if (dtResult.Rows[0]["Flag"].ToString() == "Y")
                    {
                        Session["InsuranceUserId"] = Convert.ToString(dtResult.Rows[0]["FK_InsdId"]);
                        if (dtResult.Rows[0]["UserType"].ToString().ToLower() == "r" || dtResult.Rows[0]["UserType"].ToString().ToLower() == "o")
                        {
                            Session["UserType"] = "Insured";
                        }
                        else if (dtResult.Rows[0]["UserType"].ToString().ToLower() == "c")
                        {
                            Session["UserType"] = "CopInsured";
                        }
                        else if (dtResult.Rows[0]["UserType"].ToString().ToLower() == "b")
                        {
                            Session["UserType"] = "Broker";
                        }
                        return RedirectToAction("ManageClaims", "Survery");
                    }
                    else if (dtResult.Rows[0]["Flag"].ToString() == "E")
                    {
                        TempData["msg"] = "Please confirm your account by login to your email account";
                        return RedirectToAction("InsuredLogin", "Login");
                    }
                    else if (dtResult.Rows[0]["Flag"].ToString() == "B")
                    {
                        TempData["msg"] = "Your account has been blocked by admin";
                        return RedirectToAction("InsuredLogin", "Login");
                    }
                    else if (dtResult.Rows[0]["Flag"].ToString() == "N")
                    {
                        TempData["msg"] = "Your account is not verified by admin yet.";
                        return RedirectToAction("InsuredLogin", "Login");
                    }
                    else
                    {
                        TempData["msg"] = "Invalid User Id & Password";
                        return RedirectToAction("InsuredLogin", "Login");
                    }
                }
                else //if (objModels.LoginId == "superadmin" && objModels.Password == "12345")
                {
                    Session["InsuranceUserId"] = "1";
                    return RedirectToAction("Index", "SuperAdmin");
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SuperAdminLogin(CSLogin objModels)
        {
            return RedirectToAction("Index", "SuperAdmin");
        }

        public ActionResult Logout()
        {
            Session.Abandon();
            return RedirectToAction("Login", "Login");
        }
        public ActionResult UserLogin()
        {
            return View();
        }
        public ActionResult authenticateUser(string userName,string password)
        {

            DataTable dt = new DataTable();
            BLUserMaster _um = new BLUserMaster();
            try
            {
                dt = _um.authenticateUser(userName, password);
                if (dt.Rows.Count > 0)
                {
                    MySession.insuranceUserId = Convert.ToInt32(dt.Rows[0]["PkUserId"]);
                    MySession.userName = Convert.ToString(dt.Rows[0]["UserName"]);
                    return RedirectToAction("Index", "SuperAdmin");
                }
                else
                {
                    return RedirectToAction("UserLogin", "Login");
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("UserLogin", "Login");
        }
    }
}

