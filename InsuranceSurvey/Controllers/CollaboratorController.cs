﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Text;
using BusinessLayer;
using InsuranceSurvey.BusinessLayer;
using Newtonsoft.Json;
using BusinessLayer.Models;
using InsuranceSurvery;
using System.Data;
using System.IO;
using System.Configuration;
using System.Data.SqlClient;
using BusinessLayer.BL;
using System.Globalization;
using System.Data.Objects.SqlClient;
using System.Net.Mail;

namespace InsuranceSurvey.Controllers
{
    public class CollaboratorController : Controller
    {
        BLGenrateClaim _gc = new BLGenrateClaim();
        BLMotorSurvey _ms = new BLMotorSurvey();
        BLConcerned _conc = new BLConcerned();
        BLMarineLorDetailsNew _lor = new BLMarineLorDetailsNew();
        CommonService _cs = new CommonService();
        string conStr = ConfigurationManager.ConnectionStrings["connectionstring"].ToString();
        //
        // GET: /Collaborator/

        //public ActionResult Index()
        //{
        //    return View();
        //}

        public JsonResult BindDist(string stateCode)
        {
            StringBuilder sb = new StringBuilder();
            JsonResult result = new JsonResult();
            Dictionary<string, string> _dictionary = new Dictionary<string, string>();
            _dictionary = BLCommon.BindDistricts(stateCode);
            foreach (KeyValuePair<string, string> dictionary in _dictionary)
            {
                sb.Append("<option value=" + dictionary.Key + ">" + dictionary.Value + "</option>");
            }
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = Convert.ToString(sb);
            return result;
        }

        public JsonResult CheckUserExistance(string UserType, string UserName)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            if (!string.IsNullOrEmpty(UserType) && !string.IsNullOrEmpty(UserName))
                result.Data = BLSurveyorsRegistration.CheckUserExistance(UserType, UserName);
            else
                result.Data = "N";
            return result;
        }

        public JsonResult CheckEmailExistance(string UserType, string EmailId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            if (!string.IsNullOrEmpty(UserType) && !string.IsNullOrEmpty(EmailId))
                result.Data = BLSurveyorsRegistration.CheckEmailExistance(UserType, EmailId);
            else
                result.Data = "N";
            return result;
        }

        public JsonResult CheckSLANO(string SlaNo)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            if (!string.IsNullOrEmpty(SlaNo))
                result.Data = BLSurveyorsRegistration.CheckSLANO(SlaNo);
            else
                result.Data = "N";

            return result;
        }

        public JsonResult CheckIniRefExistance(string IniRef)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            if (!string.IsNullOrEmpty(IniRef))
                result.Data = BLSurveyorsRegistration.CheckIniRefExistance(IniRef);
            else
                result.Data = "N";

            return result;
        }

        public string ViewSurveyorsDetails(int? FkSurId)
        {
            BLSurveyorsRegistration obj = new BLSurveyorsRegistration();
            var Result = JsonConvert.SerializeObject(obj.BindSurveyorsDetails(0, "", FkSurId), Newtonsoft.Json.Formatting.None);
            return Result;
        }


        public string getInsuredDetails_byPK_InsdID(int pk_InsdID)
        {
            DataTable dt = new DataTable();
            BLInsuredRegistration i = new BLInsuredRegistration();
            var Result = JsonConvert.SerializeObject(i.getInsuredDetails_byPK_InsdID(pk_InsdID), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public JsonResult ApproveOrDiscardSurveyor(string ApproveStatus, string FKSurId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLSurveyorsRegistration.ApproveOrDiscardSurveyor(ApproveStatus, "1", FKSurId);
            return result;
        }

        public JsonResult MarineSubmitJIR(CSMarineJIR objProperty)
        {
            BLGenrateClaim obj = new BLGenrateClaim();

            objProperty.EntryBy = Convert.ToString(MySession.insuranceUserId);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            obj.updateLocationOfSurvey(Convert.ToInt32(objProperty.FKClaimId), MySession.insuranceUserId, objProperty.PlaceofSurvey);
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //if (TempData["FromTable"] != null)
            //{
            //    objProperty.FromTable = TempData["FromTable"].ToString();
            //}
            result.Data = obj.MarineSubmitJIR(objProperty);
            return result;
        }

        public JsonResult MarineSubmitDamageDetails(CSMarineDamageDetails objProperty)
        {
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = obj.MarineSubmitDamageDetails(objProperty);
            return result;
        }

        public JsonResult MarineSubmitDamageDetails_(CSMarineDamageDetails MD)
        {
            string msg = null;
            int pk_MDId = 0;
            try
            {
                string entryBy = Convert.ToString(Session["InsuranceUserId"]);
                string IPAddress = BusinessHelper.GetIpAdress();

                string query = "insert into tblMarineDamageDetails(FK_DesID, FK_ClaimID, AffectedQuantity, Remark, EntryBy, IPAddress, CreatedDate, RemarkOther) values(" + MD.FKDesId + "," + MD.FKClaimId + "," + MD.AffectedQuantity + ",'" + MD.Remark + "','" + entryBy + "','" + IPAddress + "',getdate()) select scope_identity()";

                pk_MDId = Convert.ToInt32(new CommonService().executeScalarWithQuery(query));

            }
            catch (Exception ex)
            {
                msg = "Error : " + ex.Message;
            }
            return Json(new { msg = msg, pk_MDId = pk_MDId }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult MarineSubmitConsignmentDocs(CSMarineConsignmentDocs objProperty)
        {
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = obj.MarineSubmitConsignmentDocs(objProperty);
            return result;
        }

        public JsonResult MarineDeleteConsignmentDocs(string pkId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLGenrateClaim.MarineDeleteConsignmentDocs(Convert.ToInt32(pkId));
            return result;
        }

        public JsonResult MarineGetConsignmentDocs(string FKClaimId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtResult = new DataTable();
            StringBuilder sb = new StringBuilder();
            dtResult = BLGenrateClaim.MarineGetConsignmentDocs(FKClaimId);
            int row = 0;

            //
            sb.Append("<thead><tr><th>Sr No.</th><th>Descriptions</th><th>Inv No</th><th>Inv Date</th><th>Inv Qty</th><th>UOM</th>");

            DataTable dtConsignment = BLGenrateClaim.GetConsignmentType(FKClaimId);
            string[] _arr = new string[5];
            foreach (DataRow dr in dtConsignment.Rows)
            {
                if (Convert.ToString(dr["Pk_Id"]) != "0")
                {
                    switch (dr["ConsignmentType"].ToString())
                    {
                        case "GR/LR":
                            sb.Append("<th id='thGL'>GL/LR</th><th>GL/LR Date</th>");
                            _arr[0] = "GR";
                            break;
                        case "BL":
                            sb.Append("<th id='thBL'>BL</th><th>BL Date</th>");
                            _arr[1] = "BL";
                            break;
                        case "BE":
                            sb.Append("<th id='thBE'>BE</th><th>BE Date</th>");
                            _arr[2] = "BE";
                            break;
                        case "AWB":
                            sb.Append("<th id='thAWB'>AWB</th><th>AWB Date</th>");
                            _arr[3] = "AWB";
                            break;
                        case "RR":
                            sb.Append("<th id='thRR'>RR</th><th>RR Date</th>");
                            _arr[4] = "RR";
                            break;

                    }
                }
            }
            sb.Append("<th>Action</th></tr></thead>");

            foreach (DataRow dr in dtResult.Rows)
            {
                row++;
                sb.Append("<tr>");
                sb.Append("<td>" + row.ToString() + "</td>");
                sb.Append("<td>" + dr["Description"] + "</td>");
                sb.Append("<td>" + dr["InvNo"] + "</td>");
                sb.Append("<td>" + dr["InvDate"] + "</td>");
                sb.Append("<td>" + dr["InvQty"] + "</td>");
                //sb.Append("<td>" + dr["TypeDate"] + "</td>");
                sb.Append("<td>" + dr["UOM"] + "</td>");

                //foreach (DataRow drcon in dtConsignment.Rows)
                //{

                if (_arr.Contains("GR"))
                {
                    sb.Append("<td>" + dr["GRNo"] + "</td>");
                    sb.Append("<td>" + dr["GRDate"] + "</td>");
                }
                if (_arr.Contains("BL"))
                {
                    sb.Append("<td>" + dr["BLNo"] + "</td>");
                    sb.Append("<td>" + dr["BLDate"] + "</td>");
                }
                if (_arr.Contains("BE"))
                {
                    sb.Append("<td>" + dr["BENo"] + "</td>");
                    sb.Append("<td>" + dr["BEDate"] + "</td>");
                }
                if (_arr.Contains("AWB"))
                {
                    sb.Append("<td>" + dr["AWBNo"] + "</td>");
                    sb.Append("<td>" + dr["AWBDate"] + "</td>");
                }
                if (_arr.Contains("RR"))
                {
                    sb.Append("<td>" + dr["RRNo"] + "</td>");
                    sb.Append("<td>" + dr["RRDate"] + "</td>");
                }
                sb.Append("<td>");
                sb.Append("<a href='#' data-content=\"" + dr["PK_ID"] + "\" class=\"EditMarineGetConsignmentDocs\" title=\"Edit\"><img src=" + @Url.Content("~/Images/edit-128.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Edit\" /></a>");
                sb.Append(" | <a href='#' data-content=\"" + dr["PK_ID"] + "\" class=\"deleteDocs\"><img src=" + @Url.Content("~/Images/Delete_icon.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Delete\" /></a>");
                sb.Append("</td></tr>");
            }
            result.Data = Convert.ToString(sb);
            return result;
        }

        public string MarineGetDescriptionConsignmentDocsEdit(string pkId)
        {
            var Result = JsonConvert.SerializeObject(BLGenrateClaim.MarineGetDescriptionConsignmentDocsEdit(pkId), Newtonsoft.Json.Formatting.None);
            return Result;
        }


        public JsonResult MarineGetDescriptionConsignmentDocs(string fkClaimId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtResult = BLGenrateClaim.MarineGetDescriptionConsignmentDocs(Convert.ToInt32(fkClaimId));
            StringBuilder sb = new StringBuilder();

            foreach (DataRow dr in dtResult.Rows)
                sb.Append("<option value=\"" + dr["PK_ID"] + "\" data-invno='" + dr["InvNo"] + "' data-invdt='" + dr["InvDate"] + "' data-invqty='" + dr["InvQty"] + "' data-uom='" + dr["uom"] + "'   >" + dr["Description"] + "</option>");

            result.Data = Convert.ToString(sb);
            return result;
        }

        public string GetConsignmentType(string fkClaimID)
        {
            var Result = JsonConvert.SerializeObject(BLGenrateClaim.GetConsignmentType(fkClaimID), Newtonsoft.Json.Formatting.None);
            return Result;
        }


        public JsonResult MarineGetDamageDetails(string fkClaimId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtresult = new DataTable();
            dtresult = BLGenrateClaim.MarineGetDamageDetails(Convert.ToInt32(fkClaimId));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < dtresult.Rows.Count; i++)
            {
                if (Convert.ToString(dtresult.Rows[i]["PK_ID"]) != "0")
                {
                    sb.Append("<tr>");
                    sb.Append("<td>" + (i + 1).ToString() + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["InvNo"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["InvDate"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["InvQty"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["UOM"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["Description"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["AffectedQuantity"] + "</td>");
                    sb.Append("<td>" + dtresult.Rows[i]["Remark"] + "</td>");
                    sb.Append("<td style='display:none;'>" + dtresult.Rows[i]["RemarkOther"] + "</td>");
                    sb.Append("<td>");
                    sb.Append("<a href='#' data-content=\"" + dtresult.Rows[i]["PK_ID"] + "\" class=\"Btn EditMarineGetConsignmentDetails\" title=\"Edit\"><img src=" + @Url.Content("~/Images/edit-128.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Edit\" /></a>");
                    //sb.Append(" | <a href='#' data-content=\"" + dtresult.Rows[i]["FK_DesID"] + "\" onclick = \"MarineDeleteDamageDetails('" + dtresult.Rows[i]["FK_DesID"] + "')\" class=\"Btn deleteDetails\"><img src=" + @Url.Content("~/Images/Delete_icon.png") + " style=\"width:13px;height:13px;\" alt='' title=\"Delete\" /></a>");
                    //sb.Append(" | <a href='#' data-content=\"" + dtresult.Rows[i]["FK_DesID"] + "\" class=\"Btn deleteDetails\"><img src=" + @Url.Content("~/Images/Delete_icon.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Delete\" /></a>");
                    sb.Append(" | <a href='#' data-content=\"" + dtresult.Rows[i]["PK_ID"] + "\" class=\"Btn deleteDetails\"><img src=" + @Url.Content("~/Images/Delete_icon.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Delete\" /></a>");
                    sb.Append("</td></tr>");
                }
            }
            if (dtresult.Rows.Count == 0)
                sb.Append("");

            result.Data = Convert.ToString(sb);
            return result;
        }

        public JsonResult MarineDeleteDamageDetails(string pkId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLGenrateClaim.MarineDeleteDamageDetails(Convert.ToInt32(pkId));
            return result;
        }

        public JsonResult BindMarineRemarksMaster(string fkClaimId)
        {
            StringBuilder sb = new StringBuilder();
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtResult = BLCommon.BindMarineRemarksMaster(fkClaimId);
            int count = 0;
            //sb.Append("<table>");
            foreach (DataRow row in dtResult.Rows)
            {
                count++;
                if (Convert.ToInt32(row["CheckMarineRemarksClaimExists"]) == 0)
                    sb.Append("<tr><td style='text-align: right;'><input type='checkbox' id=\"claim" + count.ToString() + "\" value=\"" + row["PK_ID"] + "\" name='chMarineRemarks' title=\"" + row["Name"] + "\" /><label><span></span></label></td><td>" + row["Name"] + "</td></tr>");
                else
                    sb.Append("<tr><td style='text-align: right;'><input type='checkbox' id=\"claim" + count.ToString() + "\" checked='true' value=\"" + row["PK_ID"] + "\" name='chMarineRemarks' title=\"" + row["Name"] + "\" /><label><span></span></label></td><td>" + row["Name"] + "</td></tr>");
            }
            //sb.Append("</table>");
            result.Data = Convert.ToString(sb);
            return result;
        }

        public JsonResult InsertMarineRemark(CSMarineJIR objProperty)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            result.Data = obj.InsertMarineRemark(objProperty);
            return result;
        }

        public JsonResult BindMarineLORMaster(string fkClaimId)
        {
            StringBuilder sb = new StringBuilder();
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtResult = BLCommon.BindMarineLORMaster(fkClaimId);
            int count = 0;
            //sb.Append("<table>");
            int TotalRow = dtResult.Rows.Count;
            foreach (DataRow row in dtResult.Rows)
            {
                count++;
                string drp = "<select id=\"drp" + row["PK_ID"] + "\" style=\"width:170px\" class = \"Drp\">";
                switch (Convert.ToString(row["LorType"]))
                {
                    case "R":
                        //drp += "<option values=\"0\">Select</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" selected=\"true\" value=\"" + row["LorType"] + "\">" + row["LorTypeText"] + "</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"N\">Not Received</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"A\">Not Relevent</option>";
                        break;
                    case "N":
                        //drp += "<option values=\"0\">Select</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"R\">Received</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" selected=\"true\" value=\"" + row["LorType"] + "\">" + row["LorTypeText"] + "</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"A\">Not Relevent</option>";
                        break;
                    case "A":
                        //drp += "<option values=\"0\">Select</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"R\">Received</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"N\">Not Received</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" selected=\"true\" value=\"" + row["LorType"] + "\">" + row["LorTypeText"] + "</option>";
                        break;
                    default:
                        drp += "<option selected=\"true\" data-val=\"" + row["PK_ID"] + "\" value=\"A\">Not Relevent</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"R\">Received</option>";
                        drp += "<option data-val=\"" + row["PK_ID"] + "\" value=\"N\">Not Received</option>";
                        break;
                }



                // if (row["PK_ID"].ToString() == "13" || row["PK_ID"].ToString() == "14")
                //{
                //  if (Convert.ToInt32(row["CheckLOR"]) == 0)
                //  {
                //                        sb.Append("<tr><td style='width:2px;'><input type='checkbox' id=\"lor" + count.ToString() + "\" class=\"validate[required,chLOR,'Please select']\" value=\"" + row["PK_ID"] + "\" name='chLOR' title=\"" + row["Name"] + "\" /></td><td> <input type='text' id='txtLOR" + row["PK_ID"] + "' value='" + row["Name"] + "'/></td></tr>");
                sb.Append("<tr><td style=\"width:190px\" >" + drp + "</td><td> <input type='text' id='txtLOR" + row["PK_ID"] + "' value='" + row["Name"] + "'/></td></tr>");

                //  }
                //else
                //{
                //    sb.Append("<tr><td style='width:2px;'><input type='checkbox' id=\"lor" + count.ToString() + "\" class=\"validate[required,chLOR,'Please select']\" checked='true' value=\"" + row["PK_ID"] + "\" name='chLOR' title=\"" + row["Name"] + "\" /></td><td> <input type='text' id='txtLOR" + row["PK_ID"] + "' value='" + row["Name"] + "'/></td></tr>");
                //}
                //}
                //else
                //{
                //    if (Convert.ToInt32(row["CheckLOR"]) == 0)
                //        sb.Append("<tr><td style='width:2px;'><input type='checkbox' id=\"lor" + count.ToString() + "\" class=\"validate[required,chLOR,'Please select']\" value=\"" + row["PK_ID"] + "\" name='chLOR' title=\"" + row["Name"] + "\" /></td><td>" + row["Name"] + "</td></tr>");
                //    else
                //        sb.Append("<tr><td style='width:2px;'><input type='checkbox' id=\"lor" + count.ToString() + "\" class=\"validate[required,chLOR,'Please select']\" value=\"" + row["PK_ID"] + "\" name='chLOR' title=\"" + row["Name"] + "\" /></td><td>" + row["Name"] + "</td></tr>");
                //  }
                TotalRow--;
            }
            //sb.Append("</table>");
            result.Data = Convert.ToString(sb);
            return result;
        }

        public JsonResult InsertMarineLOR(CSMarineJIR objProperty)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            result.Data = obj.InsertMarineLOR(objProperty);
            obj.LORPlaceOfSurvey(objProperty.PlaceofSurvey, objProperty.FKClaimId);

            obj.MarineContactPersonJIR(objProperty);




            return result;
        }


        public string AddClaimForm(CSGenrateClaim objProperty)
        {
            BLGenrateClaim objResult = new BLGenrateClaim();
            //objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IpAddress = BusinessHelper.GetIpAdress();
            var Result = JsonConvert.SerializeObject(objResult.UpdatePolicyParticulars(objProperty), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        //checl thos fuction
        public JsonResult ActionWithMarineTranshipment(CSMarineTranshipment objProperty)
        {
            JsonResult result = new JsonResult();
            try
            {
                result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                BLGenrateClaim obj = new BLGenrateClaim();
                objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                objProperty.IPAddress = BusinessHelper.GetIpAdress();
                var dt = obj.ActionWithMarineTranshipment(objProperty);

                result.Data = new { flag = dt.Rows[0]["flag"], pk_id = dt.Rows[0]["pk_id"] };
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        //rajat
        public JsonResult saveCommentPhotoOrDoc(string cmtPhoto, int pk_id)
        {
            string Msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {


                        HttpPostedFileBase file = files[i];
                        string fname;


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && cmtPhoto == "yes")
                            {


                                SqlConnection con = new SqlConnection(conStr);
                                SqlCommand cmd = new SqlCommand("select CommentPhotoOrDoc from tblmarinetranshipment  where PK_ID=" + pk_id + " ", con);
                                con.Open();
                                string prevPhoto = Convert.ToString(cmd.ExecuteScalar());
                                con.Close();
                                SqlCommand cmd1 = new SqlCommand("update tblmarinetranshipment set CommentPhotoOrDoc='" + fname + "' where PK_ID=" + pk_id + " ", con);
                                con.Open();
                                cmd1.ExecuteNonQuery();
                                con.Close();

                                string tempPath = Server.MapPath("~/files/MISC/" + prevPhoto);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);
                            }

                            //fname = file.FileName;
                        }



                    }
                    //Status = _fsr.saveOtherReportsForMaterialFacts(listORMF);
                    // Returns message that successfully uploaded  
                    //return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }

            //ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
            //Status = _fsr.saveOtherReportsForMaterialFacts(ormf);
            Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult GetMarineTranshipment(string FKClaimId)
        {
            List<CSMarineTranshipment> objResult = BLGenrateClaim.GetMarineTranshipment(FKClaimId, "SelectAll");

            string[] _arr = new string[] { "First", "Second", "Third", "Fourth", "Fifth", "Sixth", "Seventh", "Eighth", "Ninth", "Tenth" };
            string Singel = "";
            string Multiple = "";
            int count = 0;

            for (int k = 0; k < objResult.Count; k++)
            {
                Multiple += "<div class=\"divMultiple\" style=\"width: 100%;height:auto;\">";
                Multiple += "<div style=\"background-color: #0069b5;text-align: center;border-radius: 5px;font-size: large;color: #fff;font-family: -webkit-pictograph;\">";

                if (objResult[k].Mode == "Road")
                    Multiple += _arr[k].ToString() + " Leg of Journey by Road</div>";
                else if (objResult[k].Mode == "Sea")
                    Multiple += _arr[k].ToString() + " Leg of Journey by Sea</div>";
                else if (objResult[k].Mode == "Air")
                    Multiple += _arr[k].ToString() + " Leg of Journey by Air</div>";
                else if (objResult[k].Mode == "Rail")
                    Multiple += _arr[k].ToString() + " Leg of Journey by Rail</div>";
                else
                    Multiple += _arr[k].ToString() + " Leg of Journey by Courier/Others</div>";

                Multiple += "<table class=\"tbllist tem mygrid\" style=\"width: 100%;\"><tr><td><b>Loading Place</b>:</td>";
                Multiple += "<td>" + objResult[k].LoadingPlace + "</td><td><b>UnLoading Place</b>:</td><td>" + objResult[k].UnLoadingPlace + "</td>";

                if (objResult[k].Mode == "Road")
                    Multiple += "<td style=\"width: 150px;\"><b>GR/LR No</b>:</td><td>" + objResult[k].TypeNo + "</td></tr><tr><td><b>GR/LR Dar</b>:</td><td>" + objResult[k].TypeDate + "</td>";
                else if (objResult[k].Mode == "Sea")
                    Multiple += "<td style=\"width: 150px;\"><b>Bill of Lading No</b>:</td><td>" + objResult[k].TypeNo + "</td></tr><tr><td><b>Bill of Lading Date</b>:</td><td>" + objResult[k].TypeDate + "</td>";
                else if (objResult[k].Mode == "Air")
                    Multiple += "<td style=\"width: 150px;\"><b>Airway Bill No</b>:</td><td>" + objResult[k].TypeNo + "</td></tr><tr><td><b>Airway Bill Date</b>:</td><td>" + objResult[k].TypeDate + "</td>";
                else if (objResult[k].Mode == "Rail")
                    Multiple += "<td style=\"width: 150px;\"><b>Railway Receipt No</b>:</td><td>" + objResult[k].TypeNo + "</td></tr><tr><td><b>Railway Receipt Date</b>:</td><td>" + objResult[k].TypeDate + "</td>";
                else
                    Multiple += "<td style=\"width: 150px;\"><b>Docket No</b>:</td><td>" + objResult[k].TypeNo + "</td></tr><tr><td><b>Docket Date</b>:</td><td>" + objResult[k].TypeDate + "</td>";


                Multiple += "<td><b>Date of Arival</b>:</td><td>" + objResult[k].DateArrival + "</td>";
                Multiple += "<td><b>Date of Deliving</b>:</td><td>" + objResult[k].DateDischarge + "</td><tr>";
                Multiple += "<td><b>Name Of Carrier</></td><td>" + objResult[k].NameOfCarrier + "</td>";
                Multiple += "<td><b>Comment</b>:</td><td>" + objResult[k].Comment + "</td><td><b>Remarks</b>:</td><td>" + objResult[k].Remark + "</td></tr>";
                Multiple += "<tr><td colspan='6'><div class=\"fr\">";
                Multiple += "<a href=\"#\" onclick=\"GetSingelMarineTranshipment('" + objResult[k].PkId + "')\" class=\"Btn\"<a><img src=" + @Url.Content("~/Images/edit-128.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Edit\" /><a/>&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"#\" onclick=\"DeleteMarineTranshipment('" + objResult[k].PkId + "')\" class=\"Btn\"><img src=" + @Url.Content("~/Images/Delete_icon.png") + " style=\"width:20px;height:20px;\" alt='' title=\"Delete\" /><a/>";
                Multiple += "</div></td></tr></table></div></div>";
                Multiple += "<div style=\"clear:both\"></div>";
                count++;
            }

            //CSMarineTranshipment obj = new CSMarineTranshipment();
            //obj.SingleTable = count.ToString();
            //obj.MulipleTable = Multiple;

            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = Multiple;


            //var Result = JsonConvert.SerializeObject(obj, Newtonsoft.Json.Formatting.None);
            //return Result;
            return result;
        }

        public string GetSingelMarineTranshipment(string pkId)
        {

            CSMarineTranshipment objResult = new CSMarineTranshipment();

            objResult = BLGenrateClaim.GetSingelMarineTranshipment(pkId);
            var listC = BLCommon.getCommentForDeliveryReceiptTesting(objResult.Comment);
            objResult.listComment = listC;
            //TempData["photo"] = objResult.CommentPhotoOrDoc;

            var Result = JsonConvert.SerializeObject(objResult, Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public string ActionSurveyVisits(CSSurveyorVisits objProperty)
        {
            BLGenrateClaim objResult = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            var Result = JsonConvert.SerializeObject(objResult.ActionSurveyVisits(objProperty), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public string GetSurveyVisits(string FkClaimId)
        {
            var Result = JsonConvert.SerializeObject(BLGenrateClaim.GetSurveyVisits(FkClaimId), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public JsonResult ApproveOrDiscardInsured(string ApproveStatus, string FKInsdId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLInsuredRegistration.InsuredApproveOrDiscardInsured(FKInsdId, ApproveStatus);
            return result;
        }

        /// <summary>
        /// Get string value after [last] a.
        /// </summary>
        public string After(string value, string a)
        {
            int posA = value.LastIndexOf(a);
            if (posA == -1)
            {
                return "";
            }
            int adjustedPosA = posA + a.Length;
            if (adjustedPosA >= value.Length)
            {
                return "";
            }
            return value.Substring(adjustedPosA);
        }
        public JsonResult AddMarineAssesment(CSGenrateClaim objProperty)
        {
            //rajat uppal
            JsonResult result = new JsonResult();
            BLGenrateClaim obj = new BLGenrateClaim();
            string calMode = objProperty.CalMode;
            string claimId = objProperty.FkClaimId;
            string xml = objProperty.XMLFile.Substring(0, objProperty.XMLFile.IndexOf("<AmountRecords>"));


            StringReader theReader = new StringReader(xml);
            DataSet theDataSet = new DataSet();
            theDataSet.ReadXml(theReader);

            DataTable dtRecord = theDataSet.Tables[0];

            foreach (DataRow row in dtRecord.Rows)
            {
                objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                //objProperty.FKCalId = row["FK_CalId"].ToString();

                if (objProperty.CalMode == "FSR" || objProperty.CalMode == "AS")
                {
                    objProperty.UnitRateA = float.Parse(row["UnitRateA"].ToString());
                    objProperty.AffectedQuantityA = Convert.ToInt32(row["AffectedQuantityA"]);
                    objProperty.AmountA = float.Parse(row["AmountA"].ToString());
                }
                else if (objProperty.CalMode == "Claim")
                {
                    objProperty.UnitRateC = float.Parse(row["UnitRateC"].ToString());

                    objProperty.AffectedQuantityC = Convert.ToInt32(row["AffectedQuantityC"]);
                    objProperty.AmountC = float.Parse(row["AmountC"].ToString());
                }
                objProperty.MD_ID = Convert.ToInt32(row["MD_ID"]);
                objProperty.AD_ID = Convert.ToInt32(row["AD_ID"]);
                objProperty.FK_DesID = Convert.ToInt32(row["FK_DesID"]);
                objProperty.IpAddress = BusinessHelper.GetIpAdress();

                obj.AddMarineAssesment(objProperty);
            }
            //result.Data = obj.AddMarineAssesment(objProperty);
            //string xmlAmtRct = objProperty.XMLFile.Substring(objProperty.XMLFile.IndexOf("</Records>"), objProperty.XMLFile.Length-1);

            string xmlAmtRct = After(objProperty.XMLFile, "</Records>");



            StringReader theReader1 = new StringReader(xmlAmtRct);
            DataSet theDataSet1 = new DataSet();
            theDataSet1.ReadXml(theReader1);

            if (theDataSet1.Tables.Count > 0)
            {
                if (theDataSet1.Tables[0].Rows.Count > 0)
                {
                    DataTable dtAmount = theDataSet1.Tables[0];
                    foreach (DataRow row in dtAmount.Rows)
                    {
                        objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                        objProperty.FKCalId = row["FK_CalID"].ToString();
                        //objProperty.CalMode = row["CalMode"].ToString();
                        objProperty.CalMode = calMode;
                        objProperty.CalType = row["CalType"].ToString();
                        objProperty.Value = float.Parse(row["Value"].ToString());
                        objProperty.Result = row["Result"].ToString();
                        objProperty.IpAddress = BusinessHelper.GetIpAdress();
                        objProperty.Rate = Convert.ToDecimal(row["Rate"]);
                        //objProperty.FkClaimId=
                        obj.saveMarineClaimedCalculation(objProperty);
                    }
                }
            }


            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IpAddress = BusinessHelper.GetIpAdress();
            //result.Data = obj.AddMarineAssesment(objProperty);
            return result;
        }

        public JsonResult ISVRPhotoDocumentRemove(string PkId, string FkClaimId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(FkClaimId), "2", PkId, "5", "", "", "", "");
            return result;
        }
        public JsonResult saveMarineISVRNotes(List<CSMarineISVRNotes> listNt, string claimId)
        {
            string msg = null;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                foreach (CSMarineISVRNotes n in listNt)
                {
                    n.ClaimID = Convert.ToInt32(claimId);
                    n.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                    gc.saveMarineISVRNotes(n);
                }

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public string GetRegisteredInsuredDetails(string emailId)
        {
            var Result = JsonConvert.SerializeObject(BLSurveyorsRegistration.GetRegisteredInsuredDetails(emailId), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public JsonResult ApproveOrDiscardTrainee(string ApproveStatus, string TSURID)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLTraineeSurveyorRegistration.ApproveOrDiscardTrainee(ApproveStatus, Convert.ToString(Session["InsuranceUserId"]), TSURID);  //BLInsuredRegistration.InsuredApproveOrDiscardInsured(FKInsdId, ApproveStatus);
            return result;
        }

        public string ViewTraineeDetails(int? PkTSurId)
        {
            BLTraineeSurveyorRegistration obj = new BLTraineeSurveyorRegistration();
            var Result = JsonConvert.SerializeObject(obj.BindTraineeDetails("", PkTSurId), Newtonsoft.Json.Formatting.None);
            return Result;
        }


        public JsonResult SubmitFSR(CSFSR objProperty)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            result.Data = obj.SubmitFSR(objProperty);
            return result;
        }

        public JsonResult TraineeRequestToSurvery(string emailIdORSLA, string mode)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            CSTraineeSurveyorRegistration objProperty = new CSTraineeSurveyorRegistration();
            objProperty.SLANo = emailIdORSLA;
            objProperty.Mode = mode;
            objProperty.PkTsurId = Convert.ToInt32(Session["InsuranceUserId"]);
            objProperty.IsLinked = 'N';
            DataTable dtResult = BLTraineeSurveyorRegistration.SurveyorTraineeInfo(objProperty);
            if (dtResult.Rows.Count != 0)
            {
                if (dtResult.Rows[0][0].ToString().Contains("Y"))
                {
                    result.Data = "Request has been sent successfully to this Surveyor! Please wait for his response, Thankyou.";
                }
                else if (dtResult.Rows[0][0].ToString().Contains("T"))
                {
                    result.Data = "You are already connected with this Surveyor";
                }
                else if (dtResult.Rows[0][0].ToString().Contains("S"))
                {
                    result.Data = "Surveyor does not exists";
                }
                else if (dtResult.Rows[0][0].ToString().Contains("A"))
                {
                    result.Data = "Active";
                }
                else if (dtResult.Rows[0][0].ToString().Contains("D"))
                {
                    result.Data = "DeActive";
                }
                else
                {
                    result.Data = "Error";
                }
            }
            return result;
        }

        public JsonResult ApproveOrDiscardCorporateInsured(string ApproveStatus, string PKCInsdId)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = BLCorporateInsuredRegistration.ApproveOrDiscardCorporateInsured(ApproveStatus, Convert.ToString(Session["InsuranceUserId"]), PKCInsdId);   //BLSurveyorsRegistration.ApproveOrDiscardSurveyor(ApproveStatus, "1", FKSurId);
            return result;
        }

        public string ViewCorporateInsuredProfile(string PKCInsdId)
        {
            BLCorporateInsuredRegistration obj = new BLCorporateInsuredRegistration();
            var Result = JsonConvert.SerializeObject(obj.BindCorporateInsuredRegistration(PKCInsdId, "", "", ""), Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public string BindApproveSurveyorsDetails(string emailId)
        {
            DataTable dtResult = BLSurveyorsRegistration.BindApproveSurveyorsDetails(emailId);
            var Result = JsonConvert.SerializeObject(dtResult, Newtonsoft.Json.Formatting.None);
            return Result;
        }

        public JsonResult TraineeAssginTask(CSAssignedClaims objProperty)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            BLAssignedClaims obj = new BLAssignedClaims();
            objProperty.SurId = Convert.ToInt32(Session["InsuranceUserId"]);
            result.Data = obj.TaskAssign(objProperty);
            return result;
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/
        public JsonResult GenerateVerifiedOTP(CSOTP objProperty)
        {
            JsonResult result = new JsonResult();
            string sResult = string.Empty;
            if (objProperty.Mode == "IN" || objProperty.Mode == "RE")
            {
                //Random rd = new Random();
                //string r = rd.Next(1000,9999).ToString();
                objProperty.OTP = "1234";
            }

            sResult = BLOTP.OTPWork(objProperty);

            if (sResult == "I")
                sResult = "Open PopUp for OTP";
            else if (sResult == "Y")
                sResult = "Verified";
            else if (sResult == "E")
                sResult = "OTP Expired, Please Click on Resend";
            else
                sResult = "Invalid OTP entered";

            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            result.Data = sResult;
            return result;
        }

        /*=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~=~*/

        public JsonResult BindMotorRemarksMaster(string fkClaimId)
        {
            StringBuilder sb = new StringBuilder();
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            DataTable dtResult = BLCommon.BindMarineRemarksMaster(fkClaimId);
            int count = 0;
            //sb.Append("<table>");
            foreach (DataRow row in dtResult.Rows)
            {
                count++;
                if (Convert.ToInt32(row["CheckMarineRemarksClaimExists"]) == 0)
                    sb.Append("<tr><td style='text-align: right;'><input type='checkbox' id=\"claim" + count.ToString() + "\" class=\"validate[required,chMarineRemarks,'Please select']\" value=\"" + row["PK_ID"] + "\" name='chMarineRemarks' title=\"" + row["Name"] + "\" /><label><span></span></label></td><td>" + row["Name"] + "</td></tr>");
                else
                    sb.Append("<tr><td style='text-align: right;'><input type='checkbox' id=\"claim" + count.ToString() + "\" class=\"validate[required,chMarineRemarks,'Please select']\" checked='true' value=\"" + row["PK_ID"] + "\" name='chMarineRemarks' title=\"" + row["Name"] + "\" /><label><span></span></label></td><td>" + row["Name"] + "</td></tr>");
            }
            //sb.Append("</table>");
            result.Data = Convert.ToString(sb);
            return result;
        }

        public JsonResult InsertMotorRemark(CSMotorSurvey objProperty)
        {
            JsonResult result = new JsonResult();
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            BLGenrateClaim obj = new BLGenrateClaim();
            objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
            objProperty.IPAddress = BusinessHelper.GetIpAdress();
            result.Data = obj.InsertMotorRemark(objProperty);
            return result;
        }



        public JsonResult BindConsignmentType(string FKClaimId, string ConsignmentType)
        {

            JsonResult result = new JsonResult();
            try
            {
                result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
                DataTable dtResult = new DataTable();
                StringBuilder sb = new StringBuilder();
                dtResult = BLGenrateClaim.MarineGetConsignmentDocs(FKClaimId);
                DataTable dtConsignment = BLGenrateClaim.GetConsignmentType(FKClaimId);
                string[] _arr = new string[5];
                foreach (DataRow dr in dtConsignment.Rows)
                {
                    if (Convert.ToString(dr["Pk_Id"]) != "0")
                    {
                        switch (dr["ConsignmentType"].ToString())
                        {
                            case "GR/LR":
                                _arr[0] = "GR";
                                break;
                            case "BL":
                                _arr[1] = "BL";
                                break;
                            case "BE":
                                _arr[2] = "BE";
                                break;
                            case "AWB":
                                _arr[3] = "AWB";
                                break;
                            case "RR":
                                _arr[4] = "RR";
                                break;
                        }
                    }
                }

                sb.Append("<option value='0'>Select</option>");
                foreach (DataRow dr in dtResult.Rows)
                {
                    if (_arr.Contains("GR") && ConsignmentType.ToLower() == "gr")
                    {
                        sb.Append("<option data-number='" + dr["GRNo"] + "' data-date='" + dr["GRDate"] + "' value='" + dr["GRNo"] + "'>" + dr["GRNo"] + "</option>");
                    }
                    else if (_arr.Contains("BL") && ConsignmentType.ToLower() == "bl")
                    {
                        sb.Append("<option data-number='" + dr["BLNo"] + "' data-date='" + dr["BLDate"] + "' value='" + dr["BLNo"] + "'>" + dr["BLNo"] + "</option>");
                    }
                    else if (_arr.Contains("BE") && ConsignmentType.ToLower() == "be")
                    {
                        sb.Append("<option data-number='" + dr["BENo"] + "' data-date='" + dr["BEDate"] + "' value='" + dr["BENo"] + "'>" + dr["BENo"] + "</option>");
                    }
                    else if (_arr.Contains("AWB") && ConsignmentType.ToLower() == "awb")
                    {
                        sb.Append("<option data-number='" + dr["AWBNo"] + "' data-date='" + dr["AWBDate"] + "' value='" + dr["AWBNo"] + "'>" + dr["AWBNo"] + "</option>");
                    }
                    else// (_arr.Contains("RR"))
                    {
                        sb.Append("<option data-number='" + dr["RRNo"] + "' data-date='" + dr["RRDate"] + "' value='" + dr["RRNo"] + "'>" + dr["RRNo"] + "</option>");
                    }
                }
                result.Data = Convert.ToString(sb);
            }
            catch (Exception ex)
            {

            }
            return result;
        }

        public JsonResult InsertMotorParticularRemark(List<SurveryRemarkValue> objlst, string Oparticular, string Pparticular, string Rparticular, string Vparticular, string Dparticular, string Cid, int? times,decimal? ratio)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("FK_ID");
            dt.Columns.Add("LorType");
            dt = BusinessHelper.ConvertToDataTable(objlst);
            JsonResult result = new JsonResult();
            SurveryOtherRemark objorem = new SurveryOtherRemark();
            string status = BLMotorSurvey.MotorParticularRemarkClaim(Convert.ToInt32(Cid), dt, Vparticular, Rparticular, Oparticular, Dparticular, Pparticular, times, ratio);
            // objorem.objlstrem = remval
            // result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            //   result.Data = .MarineDeleteConsignmentDocs(Convert.ToInt32(pkId));
            return Json(result, null, null);
        }
        public JsonResult deleteMarineISVRNotes(int pk_N_Id)
        {

            string msg = null;
            int status = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                status = gc.deleteMarineISVRNotes(pk_N_Id, userId);
                msg = status > 0 ? "Delete Successfully" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteMarineFSRNotes(int pk_FsrN_Id)
        {

            string msg = null;
            int status = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                status = gc.deleteMarineFSRNotes(pk_FsrN_Id, userId);
                msg = status > 0 ? "Delete Successfully" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        #region Marine MOM
        public JsonResult deleteMOMAttendees(int pk_AT_ID)
        {
            string msg = null;
            int status = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                status = gc.deleteMOMAttendees(pk_AT_ID);
                msg = status > 0 ? "Delete Successfully!" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteMOMDiscussion(int pk_DS_ID)
        {
            string msg = null;
            int status = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                status = gc.deleteMOMDiscussion(pk_DS_ID);
                msg = status > 0 ? "Delete Successfully!" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRefNo_And_InsuredName_byClaimID(int claimId)
        {
            string RefNo = "";
            string InsuredName = "";
            DataTable dt = new DataTable();
            string msg = null;
            try
            {
                dt = new BLGenrateClaim().getRefNo_And_InsuredName_byClaimID(claimId);

                if (dt.Rows.Count > 0)
                {
                    RefNo = dt.Rows[0]["RefNo"].ToString();
                    InsuredName = dt.Rows[0]["InsuredName"].ToString();
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { RefNo = RefNo, InsuredName = InsuredName, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveMOM(CSMarineMOMData md, List<CSMarineMOMAttendees> listMA, List<CSMarineMOMDiscussion> listDS)
        {
            int status = 0; string msg = null;
            int md_Id = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                md.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                md_Id = gc.saveMOMData(md);
                status = md_Id;
                msg = status > 0 ? "Saved Successfully!" : "Failed ! Try Again";
                //List<CSMarineMOMAttendees> templistMA = new List<CSMarineMOMAttendees>();
                //templistMA = gc.getMOMAttendees(md.PK_MD_ID);
                //if (templistMA.Count>0)
                //{

                //    foreach (CSMarineMOMAttendees tm in templistMA)
                //    {
                //        int t = tm.PK_At_ID;
                //        foreach (CSMarineMOMAttendees ma in listMA)
                //        {
                //            if (t == 0 || t == ma.PK_At_ID)
                //            {
                //                ma.MD_ID = md_Id;
                //                gc.saveMOMAttendees(ma);
                //            }
                //            else if (t != 0)
                //            {
                //                if (t != ma.PK_At_ID)
                //                {
                //                    gc.deleteMOMAttendees(ma.PK_At_ID);
                //                }
                //            }

                //        }

                //    }
                //}
                foreach (CSMarineMOMAttendees ma in listMA)
                {
                    ma.MD_ID = md_Id;
                    gc.saveMOMAttendees(ma);

                }
                foreach (CSMarineMOMDiscussion ds in listDS)
                {
                    ds.MD_ID = md_Id;
                    gc.saveMOMDiscussion(ds);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMOM(int claimId, int sv_Id)
        {
            string msg = null;
            BLGenrateClaim gc = new BLGenrateClaim();
            CSMarineMOMData md = new CSMarineMOMData();

            List<CSMarineMOMAttendees> listMA = new List<CSMarineMOMAttendees>();
            List<CSMarineMOMDiscussion> listDS = new List<CSMarineMOMDiscussion>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                md = gc.getMOMData(claimId, userId, sv_Id);
                listMA = gc.getMOMAttendees(md.PK_MD_ID);
                listDS = gc.getMOMDiscussion(md.PK_MD_ID);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { md = md, listMA = listMA, listDS = listDS, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Marine Assessment Detail

        public JsonResult getParticular(string calMode)
        {
            string msg = null;
            List<CSMarineCalulationMaster> listCM = new List<CSMarineCalulationMaster>();
            try
            {
                listCM = _gc.getParticular(calMode);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCM = listCM, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMarineAssessmentDetailDesc(int claimId)
        {
            string msg = null;
            List<CSMarineAssessmentDetailDesc> listAD = new List<CSMarineAssessmentDetailDesc>();
            try
            {
                listAD = _gc.getMarineAssessmentDetailDesc(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAD = listAD, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDescription_byClaimID(int claimId)
        {
            string msg = null;
            List<CSMarineConsignmentDocs> listCD = new List<CSMarineConsignmentDocs>();
            try
            {
                listCD = _gc.getDescription_byClaimID(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCD = listCD, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMarineConsignmentDocs_byFK_Des_ID(int claimId, int fk_Des_ID)
        {
            string msg = null;
            CSMarineConsignmentDocs c = new CSMarineConsignmentDocs();
            try
            {
                c = _gc.getMarineConsignmentDocs_byFK_Des_ID(claimId, Convert.ToInt32(Session["InsuranceUserId"]), fk_Des_ID);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { c = c, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteMarineClaimedCalculation(int pk_CC_Id)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _gc.deleteMarineClaimedCalculation(pk_CC_Id, Convert.ToInt32(Session["InsuranceUserId"]));
                msg = status > 0 ? "Delete Successfully" : "Failed! Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteMarineAssessmentRevisionRsn(int pk_AR_Id)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _gc.deleteMarineAssessmentRevisionRsn(pk_AR_Id, MySession.insuranceUserId);
                msg = status > 0 ? "Delete Successfully" : "Failed! Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveMarineAssessment(int claimId, string calMode, CSMarineAssessmentHeader ah, List<CSMarineAssessmentDetailDesc> listAD, List<CSMarineClaimedCalculation> listCC, List<CSMarineAssessmentRevisionRsn> listRevisionRsn)
        {
            string msg = null;
            int status = 0;
            int pk_AH_Id = 0;
            try
            {
                ah.ClaimID = claimId;
                ah.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                pk_AH_Id = _gc.saveMarineAssessmentHeader(ah);
                if (pk_AH_Id > 0)
                {
                    if (listAD != null)
                    {
                        foreach (CSMarineAssessmentDetailDesc ad in listAD)
                        {
                            ad.ClaimID = claimId;
                            ad.UserID = Session["InsuranceUserId"].ToString();
                            ad.AH_ID = pk_AH_Id;
                            ad.CalMode = calMode;
                            ad.IPAddress = BusinessHelper.GetIpAdress();
                            _gc.saveMarineAssessmentDetailDesc(ad);
                        }
                    }
                    if (listCC != null)
                    {
                        foreach (CSMarineClaimedCalculation cc in listCC)
                        {
                            cc.ClaimID = claimId;
                            cc.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                            cc.AH_ID = pk_AH_Id;
                            cc.CalMode = calMode;
                            cc.IPAddress = BusinessHelper.GetIpAdress();
                            _gc.saveMarineClaimedCalculation(cc);
                        }
                    }
                    if (listRevisionRsn != null)
                    {
                        foreach (CSMarineAssessmentRevisionRsn ar in listRevisionRsn)
                        {
                            ar.ClaimID = claimId;
                            ar.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                            ar.AH_ID = pk_AH_Id;
                            _gc.saveMarineAssessmentRevisionRsn(ar);
                        }
                    }
                    _cs.saveTaskManager(claimId, MySession.insuranceUserId, "AS");

                }

                msg = pk_AH_Id > 0 ? "Saved Successfully!" : "Failed! Try Again.";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMarineAssessment(int claimId, string calMode)
        {
            string msg = null;
            CSMarineAssessmentHeader ah = new CSMarineAssessmentHeader();
            List<CSMarineAssessmentDetailDesc> listAD = new List<CSMarineAssessmentDetailDesc>();
            List<CSMarineClaimedCalculation> listCC = new List<CSMarineClaimedCalculation>();
            List<CSMarineClaimedCalculation> listC = new List<CSMarineClaimedCalculation>();
            List<CSMarineAssessmentRevisionRsn> listRevisionRsn = new List<CSMarineAssessmentRevisionRsn>();
            try
            {
                ah = _gc.getMarineAssessmentHeader(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
                listAD = _gc.getMarineAssessmentDetailDesc(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
                listCC = _gc.getMarineClaimedCalculation(claimId, Convert.ToInt32(Session["InsuranceUserId"]), calMode);
                listRevisionRsn = _gc.getMarineAssessmentRevisionRsn(claimId, MySession.insuranceUserId);
                listC = _gc.getMarineClaimedCalculation(claimId, Convert.ToInt32(Session["InsuranceUserId"]), "CL");

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { ah = ah, listAD = listAD, listCC = listCC, listC = listC, listRevisionRsn = listRevisionRsn, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDataFromMarineJIRForAssessment(int claimId)
        {
            string InsdType = null;
            int InsdId = 0;
            string msg = null;
            CSMarineJIR objPropertyJIR = new CSMarineJIR();
            try
            {
                objPropertyJIR = BLGenrateClaim.MarineGetJIR(claimId.ToString());
                InsdType = Convert.ToString(objPropertyJIR.InsdType);
                InsdId = objPropertyJIR.InsdId;
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { InsdType = InsdType, InsdId = InsdId, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public JsonResult getLegOfJourney_byClaimId(int claimId)
        {

            string msg = null;
            string legOfJourney = null;
            try
            {
                legOfJourney = _gc.getLegOfJourney_byClaimId(claimId, Convert.ToString(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { legOfJourney = legOfJourney, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getTotalAmountA_byClaimId(int claimId)
        {

            string msg = null;
            string totalAmountA = null;
            try
            {
                totalAmountA = _gc.getTotalAmountA_byClaimId(claimId, Convert.ToString(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { totalAmountA = totalAmountA, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult getTotalAmountC_byClaimId(int claimId)
        //{

        //    string msg = null;
        //    string totalAmountC = null;
        //    try
        //    {
        //        totalAmountC = _gc.getTotalAmountC_byClaimId(claimId, Convert.ToString(Session["InsuranceUserId"]));
        //    }
        //    catch (Exception ex)
        //    {
        //        msg = "Error:" + ex.Message;
        //    }
        //    return Json(new { totalAmountC = totalAmountC, msg = msg }, JsonRequestBehavior.AllowGet);
        //}
        #region Particular of Survey

        public JsonResult saveParticularOfSurvey(CSMotorParticularOfSurvey ps)
        {
            string msg = null;
            int status = 0;
            try
            {
                ps.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveParticularOfSurvey(ps);
                _gc.updateLocationOfSurvey(ps.ClaimID, Convert.ToInt32(Session["InsuranceUserId"]), ps.PlaceOfSurvey);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getParticularOfSurvey(int claimId)
        {
            CSMotorParticularOfSurvey ps = new CSMotorParticularOfSurvey();
            string msg = null;
            try
            {
                ps = _ms.getParticularOfSurvey(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
                ps.PlaceOfSurvey = _gc.getLocationOfSurvey(Convert.ToInt32(claimId), Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { ps = ps, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInstructorName(int claimId)
        {
            string msg = null;
            string InstructorName = null;
            try
            {
                InstructorName = _ms.getInstructorName_byClaimID(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { InstructorName = InstructorName, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Particulars Of Loss Damage
        public JsonResult saveParticularsOfLossDamage(CSCommonMotor pl)
        {
            string msg = null;
            int status = 0;
            try
            {
                pl.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveParticularsOfLossDamage(pl);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getParticularsOfLossDamage(int claimId)
        {
            CSCommonMotor pl = new CSCommonMotor();
            string msg = null;
            try
            {
                pl = _ms.getParticularsOfLossDamage(claimId, Convert.ToInt32(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { pl = pl, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Remarks/Observations
        public JsonResult saveRemarksObservations(CSCommonMotor ro)
        {
            string msg = null;
            int status = 0;
            try
            {
                ro.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveRemarksObservations(ro);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRemarksObservations(int claimId)
        {
            CSCommonMotor ro = new CSCommonMotor();
            string msg = null;
            try
            {
                ro = _ms.getRemarksObservations(claimId, Convert.ToInt32(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { ro = ro, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region MotorSalvage
        public JsonResult saveSalvage(CSCommonMotor s)
        {
            string msg = null;
            int status = 0;
            try
            {
                s.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveSalvage(s);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSalvage(int claimId)
        {
            CSCommonMotor s = new CSCommonMotor();
            string msg = null;
            try
            {
                s = _ms.getSalvage(claimId, Convert.ToInt32(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { s = s, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Re -Inspection
        public JsonResult saveReInspection(CSMotorReInspection r)
        {
            string msg = null;
            int status = 0;
            try
            {
                r.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveReInspection(r);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getReInspection(int claimId)
        {
            CSMotorReInspection r = new CSMotorReInspection();
            string msg = null;
            try
            {
                r = _ms.getReInspection(claimId, Convert.ToInt32(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { r = r, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Other Legal Aspects
        public JsonResult saveOtherLegalAspects(CSMotorOtherLegalAspects ol)
        {
            string msg = null;
            int status = 0;
            try
            {
                ol.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveOtherLegalAspects(ol);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getOtherLegalAspects(int claimId)
        {
            CSMotorOtherLegalAspects ol = new CSMotorOtherLegalAspects();
            string msg = null;
            try
            {
                ol = _ms.getOtherLegalAspects(claimId, Convert.ToInt32(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { ol = ol, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Previous Claim
        public JsonResult savePreviousClaim(CSMotorPreviousClaim p)
        {
            string msg = null;
            int status = 0;
            try
            {
                p.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                p.IPAddress = BusinessHelper.GetIpAdress();
                status = _ms.savePreviousClaim(p);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPreviousClaim(int claimId)
        {
            CSMotorPreviousClaim p = new CSMotorPreviousClaim();
            string msg = null;
            try
            {
                p = _ms.getPreviousClaim(claimId, Convert.ToString(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { p = p, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Motor Assessment
        public JsonResult getMetologyOfItems()
        {
            List<CSMetologyOfItems> listMI = new List<CSMetologyOfItems>();
            string msg = null;
            try
            {
                listMI = _ms.getMetologyOfItems();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listMI = listMI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMotorParticular()
        {
            List<CSMotorAssParticular> listP = new List<CSMotorAssParticular>();
            string msg = null;
            try
            {
                listP = _ms.getMotorParticular();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listP = listP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDepriciation_byMonth(string claimId)
        {
            decimal? deprRate = 0;
            string msg = null;
            try
            {
                deprRate = _ms.getDepriciation_byMonth(claimId, Convert.ToString(Session["InsuranceUserId"]));

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { deprRate = deprRate, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAssessment(CSMotorAssessmentHeader ah, List<CSMotorAssessmentDesc> listAssDesc, List<CSMotorAssessmentCalculation> listAssCalc)
        {
            string msg = null;
            int pk_AH_ID = 0;
            try
            {
                ah.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                pk_AH_ID = _ms.saveAssessmentHeader(ah);
                if (pk_AH_ID != 0)
                {
                    if (listAssDesc != null)
                    {
                        foreach (CSMotorAssessmentDesc ad in listAssDesc)
                        {
                            ad.AH_ID = pk_AH_ID;
                            _ms.saveAssessmentDesc(ad);
                        }
                    }
                    if (listAssCalc != null)
                    {
                        foreach (CSMotorAssessmentCalculation ac in listAssCalc)
                        {
                            ac.AH_ID = pk_AH_ID;
                            _ms.saveAssessmentCalculation(ac);
                        }
                    }
                }

                msg = pk_AH_ID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssessment(int claimId, string shortName)
        {
            string msg = null;
            CSMotorAssessmentHeader ah = new CSMotorAssessmentHeader();
            List<CSMotorAssessmentDesc> listAssDesc = new List<CSMotorAssessmentDesc>();
            List<CSMotorAssessmentCalculation> listAssCalc = new List<CSMotorAssessmentCalculation>();
            try
            {
                ah = _ms.getAssessmentHeader(claimId, Convert.ToInt32(Session["InsuranceUserId"]), shortName);
                listAssDesc = _ms.getAssessmentDesc(ah.PK_AH_ID);
                listAssCalc = _ms.getAssessmentCalculation(ah.PK_AH_ID);
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { ah = ah, listAssDesc = listAssDesc, listAssCalc = listAssCalc, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteMotorAssessmentCalculation(int pk_AC_ID)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _ms.deleteMotorAssessmentCalculation(pk_AC_ID);
                msg = status > 0 ? "d" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;

            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Motor AssessmentSummery
        public JsonResult getAssessmentHeader_usingFor_AssessmentSummery(int claimId)
        {
            string msg = null;
            List<CSMotorAssessmentHeader> listAssHeader = new List<CSMotorAssessmentHeader>();
            try
            {
                listAssHeader = _ms.getAssessmentHeader_usingFor_AssessmentSummery(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { listAssHeader = listAssHeader, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAssessmentSummery(CSMotorAssessmentSummery mas)
        {
            string msg = null;
            int status = 0;
            try
            {
                mas.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _ms.saveAssessmentSummery(mas);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssessmentSummery(int claimId)
        {
            string msg = null;
            CSMotorAssessmentSummery mas = new CSMotorAssessmentSummery();
            try
            {
                mas = _ms.getAssessmentSummery(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { mas = mas, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        public JsonResult getNatureofLoss(int deptId)
        {
            string msg = null;
            List<CSMISCNatureofLoss> listNL = new List<CSMISCNatureofLoss>();
            try
            {
                listNL = BLCommon.getNatureofLoss(deptId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listNL = listNL, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #region get State 
        public JsonResult getRegion()
        {
            List<CSRegionMaster> listRegion = new List<CSRegionMaster>();
            string msg = null;
            try
            {
                listRegion = BLCommon.getRegion();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listRegion = listRegion, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getState_byRegion(int r_Id)
        {
            List<CSState> listState = new List<CSState>();
            string msg = null;
            try
            {
                listState = BLCommon.getState_byRegion(r_Id);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listState = listState, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getState()
        {
            List<CSState> listState = new List<CSState>();
            string msg = null;
            try
            {
                listState = BLCommon.getState();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listState = listState, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDistricts_byStateId(int FkState)
        {
            List<CSDistricts> listDistricts = new List<CSDistricts>();
            string msg = null;
            try
            {
                listDistricts = BLCommon.getDistricts_byStateId(FkState);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listDistricts = listDistricts, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Concerned
        public JsonResult getBroker()
        {
            string msg = null;
            List<CSBroker> listBroker = new List<CSBroker>();
            try
            {
                listBroker = _conc.getBroker();
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;

            }
            return Json(new { listBroker = listBroker, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveConcerned(CSConcerned c)
        {
            string msg = null;
            int status = 0;
            try
            {
                c.UserID = MySession.insuranceUserId;
                status = _conc.saveConcerned(c);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getConcerned(int claimId)
        {
            string msg = null;
            CSConcerned c = new CSConcerned();
            try
            {

                c = _conc.getConcerned(claimId, MySession.insuranceUserId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { c = c }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Marine Lor New
        public JsonResult getLorDetailsNew(int claimId)
        {
            string msg = null;
            List<CSMarineLorDetailsNew> listLorDetailsNew = new List<CSMarineLorDetailsNew>();
            try
            {

                listLorDetailsNew = _lor.getLorDetailsNew(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listLorDetailsNew = listLorDetailsNew, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLorHeaderNew(int claimId)
        {
            string msg = null;
            CSMarineLorHeaderNew lh = new CSMarineLorHeaderNew();
            try
            {

                lh = _lor.getLorHeaderNew(claimId, MySession.insuranceUserId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { lh = lh, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveLorNew(CSMarineLorHeaderNew lh, List<CSMarineLorDetailsNew> listLorDetailsNew, int claimId)
        {
            string msg = null;
            int status = 0;
            try
            {
                lh.UserID = MySession.insuranceUserId;
                status = _lor.saveLorHeaderNew(lh);
                if (status > 0)
                {
                    if (listLorDetailsNew != null)
                    {
                        foreach (CSMarineLorDetailsNew l in listLorDetailsNew)
                        {
                            l.ClaimID = claimId;
                            l.UserID = MySession.insuranceUserId;
                            _lor.saveLorDetailsNew(l);
                        }
                    }
                }
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveLorDetailsNew(CSMarineLorDetailsNew l)
        {
            string msg = null;
            int pk_L_ID = 0;
            try
            {
                l.UserID = MySession.insuranceUserId;
                pk_L_ID = _lor.saveLorDetailsNew(l);
                msg = pk_L_ID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { pk_L_ID = pk_L_ID, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveMarineLorEmailIdsNew(CSMarineLorEmailIdsNew e)
        {
            string msg = null;
            int PK_LE_ID = 0;
            try
            {
                e.UserID = MySession.insuranceUserId;
                e.ToEmailIds = e.ToEmailIds.Replace("[", "");
                e.ToEmailIds = e.ToEmailIds.Replace("]", "");
                e.CCEmailIds = e.CCEmailIds.Replace("[", "");
                e.CCEmailIds = e.CCEmailIds.Replace("]", "");
                PK_LE_ID = _lor.saveMarineLorEmailIdsNew(e);
                foreach (var emailId in e.ToEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.ToEmailIds = e.ToEmailIds.Replace('"', ' ');
                    }

                }


                foreach (var emailId in e.CCEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.CCEmailIds = e.CCEmailIds.Replace('"', ' ');
                    }

                }

                CSLogEmailMarineLor l = new CSLogEmailMarineLor();
                l.LE_ID = PK_LE_ID;
                l.ToEmailIds = e.ToEmailIds;
                l.CCEmailIds = e.CCEmailIds;
                l.Subject = "Testing";
                l.MailMsg = "This is for Testing";
                _lor.saveLogEmailMarineLor(l);
                sendEmail("Test", "This is for Testing", e.ToEmailIds, e.CCEmailIds);
                msg = PK_LE_ID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMarineLorEmailIdsNew(int claimId)
        {
            string msg = null;
            CSMarineLorEmailIdsNew e = new CSMarineLorEmailIdsNew();
            try
            {

                e = _lor.getMarineLorEmailIdsNew(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { e = e, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveLorDetailsNew_File(string f, int pk_L_ID)
        {
            int status = 0;
            string msg;
            string sendFileName = "";
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        string tempFileName = file.FileName;
                        string tempFile = Path.GetExtension(tempFileName);
                        fname = Guid.NewGuid().ToString() + tempFile;

                        if (f == "yes")
                        {
                            CommonService _cs = new CommonService();
                            string fileName;
                            fileName = Convert.ToString(_cs.executeScalarWithQuery("select FileName from tblMarineLorDetailsNew where Pk_L_ID =" + pk_L_ID + ""));
                            string tempPath = Server.MapPath("~/Files/" + fileName);
                            if (System.IO.File.Exists(tempPath))
                            {
                                System.IO.File.Delete(tempPath);
                            }
                            status = _cs.executeNonQueryWithQuery("update tblMarineLorDetailsNew set FileName='" + fname + "',Rec_NotRec_NotRel='Received' where Pk_L_ID=" + pk_L_ID + "");
                            fname = Path.Combine(Server.MapPath("~/Files/"), fname);
                            file.SaveAs(fname);
                            sendFileName = Convert.ToString(_cs.executeScalarWithQuery("select FileName from tblMarineLorDetailsNew where Pk_L_ID =" + pk_L_ID + ""));
                            fname = "";
                        }

                    }

                }
                catch (Exception ex)
                {
                    msg = "Error:" + ex.Message;
                }
            }
            msg = status > 0 ? "s" : "f";
            return Json(new { msg = msg, fileName = sendFileName }, JsonRequestBehavior.AllowGet);
        }


        public JsonResult sendEmail(string subject, string mailMsg, string toEmailIds, string ccEmailIds)
        {
            string msg = null;
            DataTable dt = new DataTable();
            //dt = _eg.getEmailId_byGroupId(groupId);
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                foreach (var emailId in toEmailIds.Split(','))
                {
                    mail.To.Add(toEmailIds.ToString());

                }


                //foreach (var emailId in ccEmailIds.Split(','))
                //{
                //    if (emailId.Contains('"'))
                //    {
                //        ccEmailIds = ccEmailIds.Replace('"', ' ');
                //    }

                //}
                foreach (var emailId in ccEmailIds.Split(','))
                {
                    mail.CC.Add(ccEmailIds.ToString());
                }

                //foreach (DataRow row in dt.Rows)
                //{
                //    // using for bcc:-bcc means person ni dekh skta kis kis ko mail gyi h

                //    mail.Bcc.Add(row["EmailId"].ToString());
                //    // using for cc:-cc meand person dekh skta h kis kis ko mail gyi h
                //    //mail.CC.Add(row["EmailId"].ToString());
                //}

                mail.From = new MailAddress("oknfeesoftware@gmail.com");
                mail.Subject = subject;
                string Body = mailMsg;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
                //mail.Attachments.Add(attachment);
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                smtp.EnableSsl = true;
                smtp.Send(mail);
                msg = "s";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getMarineLorChattingNew(int claimId)
        {
            string msg = null;
            List<CSMarineLorChattingNew> listChatting = new List<CSMarineLorChattingNew>();
            try
            {
                listChatting = _lor.getMarineLorChattingNew(claimId, MySession.insuranceUserId);
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { listChatting = listChatting, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveMarineLorChattingNew(CSMarineLorChattingNew lc)
        {
            string msg = null;
            int PK_LC_ID = 0;
            try
            {
                lc.UserID = MySession.insuranceUserId;
                lc.UserType = Session["UserType"].ToString();
                PK_LC_ID = _lor.saveMarineLorChattingNew(lc);
                msg = PK_LC_ID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { PK_LC_ID = PK_LC_ID, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult saveLorDetailsNew_Document(string d, int PK_LC_ID)
        {
            int status = 0;
            string msg;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;
                        string tempFileName = file.FileName;
                        string tempFile = Path.GetExtension(tempFileName);
                        fname = Guid.NewGuid().ToString() + tempFile;

                        if (d == "yes")
                        {
                            CommonService _cs = new CommonService();
                            status = _cs.executeNonQueryWithQuery("update tblMarineLorChattingNew set DocumentName='" + fname + "' where PK_LC_ID=" + PK_LC_ID + "");
                            fname = Path.Combine(Server.MapPath("~/files/"), fname);
                            file.SaveAs(fname);
                            fname = "";
                        }

                    }

                }
                catch (Exception ex)
                {
                    msg = "Error:" + ex.Message;
                }
            }
            msg = status > 0 ? "s" : "f";
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult setFinalReminder(int claimId)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = new CommonService().executeNonQueryWithQuery("update tblMarineLorHeaderNew set FinalReminder=1 where ClaimID=" + claimId + " and UserID=" + MySession.insuranceUserId + "");
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Email Log
        public JsonResult sendEmail_saveEmailLog(CSEmailLog e)
        {
            string msg = null;
            int status = 0;
            DataTable dt = new DataTable();
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                e.ToEmailIds = e.ToEmailIds.Replace("[", "");
                e.ToEmailIds = e.ToEmailIds.Replace("]", "");
                e.CCEmailIds = e.CCEmailIds.Replace("[", "");
                e.CCEmailIds = e.CCEmailIds.Replace("]", "");
                foreach (var emailId in e.ToEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.ToEmailIds = e.ToEmailIds.Replace('"', ' ');
                    }

                }
                foreach (var emailId in e.CCEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.CCEmailIds = e.CCEmailIds.Replace('"', ' ');
                    }

                }
                e.UserID = MySession.insuranceUserId;
                status = new BLEmailLog().saveEmailLog(e);
                foreach (var emailId in e.ToEmailIds.Split(','))
                {
                    mail.To.Add(emailId.ToString());

                }
                foreach (var emailId in e.CCEmailIds.Split(','))
                {
                    mail.CC.Add(emailId.ToString());
                }
                //new SurveryController().GeneratePdf(Convert.ToString(e.ClaimID),e.InsdType,e.InsdID,e.Task);
                //byte[] data = null;
                //if (new SurveryController().GeneratePdf(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID, e.Task) is FileContentResult)
                //{
                //    data = ((FileContentResult)(new SurveryController().GeneratePdf(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID, e.Task))).FileContents;
                //}
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment(new MemoryStream(data),"PDF");
                //mail.Attachments.Add(attachment);

                var contentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);

                var attachmentFile = new SurveryController().GeneratePdf(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID, e.Task);
                var attachmentStream = new MemoryStream((attachmentFile as FileContentResult).FileContents);
                var attachmentTitle = (attachmentFile as FileContentResult).FileDownloadName;
                mail.Attachments.Add(new Attachment(attachmentStream, attachmentTitle, contentType.ToString()));
                mail.From = new MailAddress("oknfeesoftware@gmail.com");
                e.Subject = "Testing";
                e.MailMsg = "This Is Testing";
                mail.Subject = e.Subject;
                string Body = e.MailMsg;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                //System.Net.Mail.Attachment attachment;
                //attachment = new System.Net.Mail.Attachment("c:/textfile.txt");
                //mail.Attachments.Add(attachment);
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                smtp.EnableSsl = true;
                smtp.Send(mail);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Task Manager
        public JsonResult saveTaskManager(int claimId, string shortName)
        {
            int status = 0;
            string msg = null;
            try
            {
                status = new CommonService().saveTaskManager(claimId, MySession.insuranceUserId, shortName);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAllTaskWithPendingTask()
        {
            string msg = null;
            List<CSAllTaskWithPendingTask> listAllTaskWithPendingTask = new List<CSAllTaskWithPendingTask>();
            try
            {
                listAllTaskWithPendingTask = _cs.getAllTaskWithPendingTask(MySession.insuranceUserId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg, listAllTaskWithPendingTask = listAllTaskWithPendingTask }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Motor Occurence
        public JsonResult saveOccurenceNew(CSMotorOccurenceNew o, List<CSMotorOccThirdPartyDetails> listTPP, List<CSMotorOccThirdPartyDetails> listTPB, string lm_ids, string ds_Ids)
        {
            string msg = null;
            int pk_Id = 0;
            try
            {
                o.EntryBy = MySession.insuranceUserId;
                pk_Id = _ms.saveOccurenceNew(o);
                if (pk_Id > 0)
                {
                    if (listTPP != null)
                    {
                        foreach (CSMotorOccThirdPartyDetails tpp in listTPP)
                        {
                            tpp.ClaimID = o.FK_ClaimID;
                            tpp.UserID = MySession.insuranceUserId;
                            _ms.saveThirdPartyPropDetails(tpp);
                        }
                    }
                    if (listTPB != null)
                    {
                        foreach (CSMotorOccThirdPartyDetails tpb in listTPB)
                        {
                            tpb.ClaimID = o.FK_ClaimID;
                            tpb.UserID = MySession.insuranceUserId;
                            _ms.saveThirdPartyBodily(tpb);
                        }
                    }
                    if (lm_ids != null)
                    {
                        _cs.executeNonQueryWithQuery("delete from tblMotorLossClaim where LM_ID not in(" + lm_ids + ") and ClaimID=" + o.FK_ClaimID + "");
                        foreach (string lm_Id in lm_ids.Split(','))
                        {
                            CSMotorLossClaim lc = new CSMotorLossClaim();
                            lc.ClaimID = o.FK_ClaimID;
                            lc.UserID = MySession.insuranceUserId;
                            lc.LM_ID = Convert.ToInt32(lm_Id);
                            _ms.saveLossClaim(lc);
                        }
                    }
                    if (ds_Ids != null)
                    {
                        _cs.executeNonQueryWithQuery("delete from tblMotorDamageClaim where DS_ID not in(" + ds_Ids + ") and ClaimID=" + o.FK_ClaimID + "");
                        foreach (string ds_Id in ds_Ids.Split(','))
                        {
                            CSMotorDamageClaim dm = new CSMotorDamageClaim();
                            dm.ClaimID = o.FK_ClaimID;
                            dm.UserID = MySession.insuranceUserId;
                            dm.DS_ID = Convert.ToInt32(ds_Id);
                            _ms.saveDamageClaim(dm);
                        }
                    }
                }
                msg = pk_Id > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
            }
            return Json(new { pk_Id = pk_Id, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveOccurenceDoc(string p1, string p2, int pk_Id)
        {
            int status = 0;
            string msg=null;
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {
                        HttpPostedFileBase file = files[i];
                        string fname;

                        string tempFileName = file.FileName;
                        string tempFile = Path.GetExtension(tempFileName);
                        fname = Guid.NewGuid().ToString() + tempFile;
                        if (p1 == "yes")
                        {
                            string jobSummery;
                            jobSummery = Convert.ToString(_cs.executeScalarWithQuery("select Photo1 from tblMotorOccurence where PK_ID=" + pk_Id + ""));
                            string tempPath = Server.MapPath("~/Files/Motor/" + jobSummery);
                            if (System.IO.File.Exists(tempPath))
                            {
                                System.IO.File.Delete(tempPath);
                            }
                            status = _cs.executeNonQueryWithQuery("update tblMotorOccurence set Photo1='" + fname + "' where PK_ID=" + pk_Id + "");
                            fname = Path.Combine(Server.MapPath("~/Files/Motor"), fname);
                            file.SaveAs(fname);
                            fname = "";
                            p1 = "no";
                        }

                        else if (p2 == "yes")
                        {
                            string jobSummery;
                            jobSummery = Convert.ToString(_cs.executeScalarWithQuery("select Photo2 from tblMotorOccurence where PK_ID=" + pk_Id + ""));
                            string tempPath = Server.MapPath("~/Files/Motor/" + jobSummery);
                            if (System.IO.File.Exists(tempPath))
                            {
                                System.IO.File.Delete(tempPath);
                            }
                            status = _cs.executeNonQueryWithQuery("update tblMotorOccurence set Photo2='" + fname + "' where PK_ID=" + pk_Id + "");
                            fname = Path.Combine(Server.MapPath("~/Files/Motor/"), fname);
                            file.SaveAs(fname);
                            fname = "";
                            p2 = "no";
                        }

                    }
                    msg = status > 0 ? "s" : "f";

                }

                //}
                catch (Exception ex)
                {
                    msg = "Error :" + ex.Message;
                }
            }
 
            return Json(new { msg =msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getOccurenceNew(int claimId)
        {
            string msg = null;
            CSMotorOccurenceNew o = new CSMotorOccurenceNew();
            List<CSMotorOccThirdPartyDetails> listTPP = new List<CSMotorOccThirdPartyDetails>();
            List<CSMotorOccThirdPartyDetails> listTPB = new List<CSMotorOccThirdPartyDetails>();
            List<CSMotorLossClaim> listLossClaim = new List<CSMotorLossClaim>();
            List<CSMotorDamageClaim> listDamageClaim = new List<CSMotorDamageClaim>();
            try
            {
                o = _ms.getOccurenceNew(claimId, MySession.insuranceUserId);
                listTPP = _ms.getThirdPartyPropDetails(claimId, MySession.insuranceUserId);
                listTPB = _ms.getThirdPartyPropBodily(claimId, MySession.insuranceUserId);
                listLossClaim = _ms.getLossClaim(claimId, MySession.insuranceUserId).OrderByDescending(m => m.Loss).ToList();
                listDamageClaim = _ms.getDamageClaim(claimId, MySession.insuranceUserId).OrderByDescending(m => m.Damage).ToList();
            }
            catch (Exception ex)
            {
                msg = "Error: " + ex.Message;
            }
            return Json(new { o = o, listTPP = listTPP, listTPB = listTPB, listLossClaim = listLossClaim, listDamageClaim = listDamageClaim, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteOccThirdPartyPropDetails(int pk_TP_Id)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _ms.deleteOccThirdPartyPropDetails(pk_TP_Id,MySession.insuranceUserId);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg=msg},JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteOccThirdPartyBodily(int pk_TPB_Id)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _ms.deleteOccThirdPartyBodily(pk_TPB_Id, MySession.insuranceUserId);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}
