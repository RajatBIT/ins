﻿using BusinessLayer;
using BusinessLayer.BL;
using BusinessLayer.Models;
using InsuranceSurvey.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class SuperAdminController : Controller
    {

        BLUserMaster _um = new BLUserMaster();
        public object ToEmailIds { get; private set; }


        //
        // GET: /SuperAdmin/

        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult AddNewUser(CSUserMaster objProperty, string command, string pkUserId)
        {
            string mes = "";
            BLUserMaster obj = new BLUserMaster();
            //try
            //{

            //}
            //catch (Exception ex)
            //{
            //    TempData["errMsg"] = "Error : " + ex.Message;
            //}
            if (!string.IsNullOrEmpty(command))
            {
                if (command == "Submit")
                {
                    mes = obj.AddUsers(objProperty);
                    if (!string.IsNullOrEmpty(mes))
                        mes = "User inserted successfully";
                }
                else
                {
                    mes = obj.UpdateUsers(objProperty).ToString();
                    if (!string.IsNullOrEmpty(mes))
                        mes = "User updated successfully";
                }
                TempData["msg"] = mes;
                return RedirectToAction("AddNewUser", "SuperAdmin");
            }
            else
            {
                if (!string.IsNullOrEmpty(pkUserId))
                {
                    objProperty.PkUserId = Convert.ToInt32(pkUserId);
                    objProperty = BLUserMaster.BindUsers(objProperty).FirstOrDefault();
                    objProperty.BtnSubmit = "Update";
                }
                else
                {
                    objProperty.BtnSubmit = "Submit";
                }
                BindUserDeatils(objProperty);
                return View(objProperty);
            }
        }

        public ActionResult DeleteUser(string pkUserId)
        {
            int result = 0;
            BLUserMaster obj = new BLUserMaster();
            try
            {
                result = obj.DeleteUsers(pkUserId);
                TempData["msg"] = "";
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return RedirectToAction("ManageUser", "SuperAdmin");
        }

        private void BindUserDeatils(CSUserMaster objProperty)
        {
            try
            {
                ViewBag.BindUsers = BLUserMaster.BindUsers(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
        }

        public ActionResult ManageSurveyors()
        {
            try
            {
                //BindSurveyorsDetails(0, "Pending", 0);
                BindSurveyorsDetails(0, "", 0);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }

        private void BindSurveyorsDetails(int? FkUserID, string Status, int? FkSurId)
        {
            try
            {
                BLSurveyorsRegistration obj = new BLSurveyorsRegistration();
                ViewBag.SurveyorsDetails = obj.BindSurveyorsDetails(FkUserID, Status, FkSurId);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ManageInsured(CSInsuredRegistration objProperty, string command)
        {
            try
            {
                BindInsuredDetails(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ManageTrainee(CSTraineeSurveyorRegistration objPropery)
        {
            try
            {
                string status = objPropery.ApproveStatus;
                int FkTSurId = objPropery.PkTsurId;
                BindSurveyorsTrainee(status, FkTSurId);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult ManageCorporateInsured(CSCorporateInsuredRegistration objPropery)
        {
            try
            {
                string name = objPropery.ApproveStatus;
                string regoffice = objPropery.RegOffice;
                string email = objPropery.Email;
                BindCorporateInsured(name, regoffice, email);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }

        private void BindInsuredDetails(CSInsuredRegistration objProperty)
        {
            try
            {
                BLInsuredRegistration obj = new BLInsuredRegistration();
                ViewBag.InsuredDetails = obj.GetAllInsured(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
        }

        private void BindSurveyorsTrainee(string Status, int? FkTSurId)
        {
            try
            {
                BLTraineeSurveyorRegistration obj = new BLTraineeSurveyorRegistration();
                ViewBag.TraineeDetails = obj.BindTraineeDetails(Status, FkTSurId);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            
        }

        private void BindCorporateInsured(string name, string regoffice,string email)
        {
            try
            {
                BLCorporateInsuredRegistration obj = new BLCorporateInsuredRegistration();
                ViewBag.CorporateInsuredDetails = obj.BindCorporateInsuredRegistration("0", name, regoffice, email);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
        }
        public ActionResult ManageBrokers()
        {
            return View();
        }
        public JsonResult getBrokerRegisteration()
        {
            string msg = null;
            List<CSBrokerRegisteration> listBrokerDetails = new List<CSBrokerRegisteration>();
            try
            {
                listBrokerDetails = new BLBroker().getBrokerRegisteration();
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { listBrokerDetails=listBrokerDetails,msg=msg},JsonRequestBehavior.AllowGet);
        }


        public JsonResult approveOrDiscardBroker(int PK_BR_ID, string ApprovedStatus)
        {
            string msg = null;
            int status = 0;
            string ApprStatus = null;
            try
            {
                ApprStatus = new BLBroker().approveOrDiscardBroker(PK_BR_ID,ApprovedStatus);
                msg = ApprStatus!=null ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new {msg = msg ,ApprStatus=ApprStatus}, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Mail()
        {
            return View();
        }


        public JsonResult sendEmail_Lor()
        {
            BLMarineLorDetailsNew _lor = new BLMarineLorDetailsNew();
            string msg = null;
            DataTable dt = new DataTable();
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                dt = _lor.getLogEmailMarineLor_using_byDays();
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        CSLogEmailMarineLor l = new CSLogEmailMarineLor();
                        l.LE_ID = Convert.ToInt32(row["PK_LE_ID"]);
                       l.ToEmailIds = row["ToEmailIds"].ToString();
                       l.CCEmailIds= row["CCEmailIds"].ToString();

                        if (l.ToEmailIds != null)
                        {
                            foreach (var emailId in l.ToEmailIds.Split(','))
                            {
                                if (emailId.Contains('"'))
                                {
                                    l.ToEmailIds = l.ToEmailIds.Replace('"', ' ');
                                }

                            }

                            foreach (var emailId in l.ToEmailIds.Split(','))
                            {
                                mail.To.Add(l.ToEmailIds.ToString());
                            }
                        }

                        if (l.CCEmailIds != null)
                        {
                            foreach (var emailId in l.CCEmailIds.Split(','))
                            {
                                if (emailId.Contains('"'))
                                {
                                    l.CCEmailIds = l.CCEmailIds.Replace('"', ' ');
                                }

                            }
                            foreach (var emailId in l.CCEmailIds.Split(','))
                            {
                                mail.CC.Add(l.CCEmailIds.ToString());
                            }
                        }
                        _lor.saveLogEmailMarineLor(l);
                        mail.From = new MailAddress("oknfeesoftware@gmail.com");
                        mail.Subject = "Testing";
                        string Body = "This is Testing";
                        mail.Body = Body;
                        mail.IsBodyHtml = true;
                        SmtpClient smtp = new SmtpClient();
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                        smtp.EnableSsl = true;
                     
                        smtp.Send(mail);
                        msg = "s";
                    }
                }
                else
                {
                    msg = "n";//email not found
                }
            
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }

            return Json(new { msg=msg }, JsonRequestBehavior.AllowGet);
        }
        #region User Detail 
        public ActionResult UserMasterNew()
        {
            //return View("ManageUserNew", "SuperAdmin");
            return View();
        }
        public JsonResult getRole()
        {
            string msg = null;
            List<CSRole> listRole = new List<CSRole>();
            try
            {
                listRole = _um.getRole();
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { listRole=listRole, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UserMasterValidation(CSUserMaster u)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailStatus = 0, MobileStatus = 0, UserNameStatus = 0;
            try
            {
                dt = _um.UserMasterValidation(u);
                if (dt.Rows.Count > 0)
                {
                    EmailStatus = Convert.ToInt32(dt.Rows[0]["EmailStatus"]);
                    MobileStatus = Convert.ToInt32(dt.Rows[0]["MobileStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailStatus = EmailStatus, MobileStatus = MobileStatus, UserNameStatus = UserNameStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveUserMaster(CSUserMaster u)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _um.saveUserMaster(u);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getUserMaster()
        {
            string msg = null;
            List<CSUserMaster> listUser = new List<CSUserMaster>();
            try
            {
                listUser = _um.getUserMaster();
            }
            catch (Exception ex)
            {

                msg = "Error:" + ex.Message;
            }
            return Json(new {listUser=listUser, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        #region Automatic Send Mail Marine Lor Reminder
        public JsonResult automaticSendMarineLorReminder()
        {
            string msg = null;
            int count = 0;
            string fileName = "Reminder.pdf";
            DataTable dt = new DataTable();
            SurveryController sc = new SurveryController();
            try
            {
                dt = new BLEmailLog().getEmailIdsForAotomaticMarineLorReminder();
                if (dt.Rows.Count > 0)
                {
                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                    foreach (DataRow row in dt.Rows)
                    {
                        try
                        {
                            CSEmailLog e = new CSEmailLog();
                            e.ToEmailIds = Convert.ToString(row["ToEmailIds"]);
                            e.CCEmailIds = Convert.ToString(row["CCEmailIds"]);
                            e.ClaimID = Convert.ToInt32(row["ClaimID"]);
                            e.UserID = MySession.insuranceUserId;
                            foreach (var emailId in e.ToEmailIds.Split(','))
                            {
                                mail.To.Add(emailId.ToString());
                            }
                            foreach (var emailId in e.CCEmailIds.Split(','))
                            {
                                mail.CC.Add(emailId.ToString());
                            }
                            MemoryStream memStream = new MemoryStream();
                            if (Convert.ToBoolean(row["FinalReminder"]) == true)
                            {
                                e.Subject = "Final Reminder";
                                memStream = sc.LorFinalReminderMemoryStream(37);
                            }
                            else if (Convert.ToBoolean(row["FinalReminder"]) == false)
                            {
                                e.Subject = "Reminder";
                                memStream = sc.LorFinalReminderMemoryStream(e.ClaimID);
                            }
                            memStream.Position = 0;
                            var contentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
                            var reportAttachment = new Attachment(memStream, contentType);
                            reportAttachment.ContentDisposition.FileName = fileName;
                            mail.Attachments.Add(reportAttachment);
                            mail.From = new MailAddress("oknfeesoftware@gmail.com");
                            mail.Subject = e.Subject;
                            mail.Body = "This is Reminder";
                            mail.IsBodyHtml = true;
                            SmtpClient smtp = new SmtpClient();
                            smtp.Host = "smtp.gmail.com";
                            smtp.Port = 587;
                            smtp.UseDefaultCredentials = false;
                            smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                            smtp.EnableSsl = true;
                            smtp.Send(mail);
                            e.UserID = MySession.insuranceUserId;
                            count= new BLEmailLog().saveEmailLog(e);
                            count++;
                        }
                        catch (Exception ex)
                        {
                            //msg = "Error:" + ex.Message;
                        }
                    }
                }
                msg = count > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
