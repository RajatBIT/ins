﻿using BusinessLayer;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class InsuredController : Controller
    {
        //
        // GET: /Insured/

        public ActionResult Index()
        {
            //return View();
            return RedirectToAction("ManageClaims", "Insured");
        }

        public ActionResult ManageClaims()
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                objProperty.FkInsrId = Convert.ToInt32(Session["InsuranceUserId"]);
                ViewBag.ClaimDetails = BLGenrateClaim.ViewInsuredClaims(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View();
        }

        public ActionResult ClaimForm(string fkClaimId)
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                objProperty = BLGenrateClaim.GetPolicyParticulars(fkClaimId);
                objProperty.FkClaimId = fkClaimId;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View(objProperty);
        }

        public ActionResult MarineJIRConsignment(string fkClaimId)
        {
            CSGenrate objProperty = new CSGenrate();
            StringBuilder sb = new StringBuilder();
            try
            {
                objProperty.GenrateClaim = new CSGenrateClaim();
                objProperty.MarineJIR = new CSMarineJIR();

                objProperty.GenrateClaim.FkClaimId = fkClaimId;
                objProperty.GenrateClaim.FkInsrId = Convert.ToInt32(MySession.insuranceUserId);
                objProperty = BLGenrateClaim.GetInsuredClaims(objProperty.GenrateClaim);

                objProperty.MarineJIR.DateOfSurvey = null;
                int InsdId = objProperty.MarineJIR.InsdId;
                string InsdType = objProperty.MarineJIR.InsdType;
                string Insured = objProperty.MarineJIR.Insured;
                string InsuredAddress = objProperty.MarineJIR.InsuredAddress;
                objProperty.MarineJIR = BLGenrateClaim.MarineGetJIR(fkClaimId);
                objProperty.MarineJIR.InsdId = InsdId;
                objProperty.MarineJIR.InsdType = InsdType;
                objProperty.MarineJIR.ResoneForDelay = "--";
                objProperty.MarineJIR.FKClaimId = fkClaimId;
                objProperty.MarineJIR.Insured = Insured;
                objProperty.MarineJIR.InsuredAddress = InsuredAddress;

                objProperty.MarineISVR = new CSMarineISVR();
                objProperty.MarineISVR.FkClaimId = Convert.ToInt32(fkClaimId);

                objProperty.MarineConsignmentDocs = new CSMarineConsignmentDocs();
                objProperty.MarineConsignmentDocs.ConsimentType = objProperty.MarineJIR.Consiment;
                DataTable dtResult = new DataTable();
                dtResult = BLGenrateClaim.GetConsignmentType(fkClaimId);
                sb.Append("<tr>");
                for (int k = 0; k < dtResult.Rows.Count; k++)
                {

                    if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                    {
                        sb.Append("<td><input id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span></span></label></td><td>" + dtResult.Rows[k]["ConsignmentType"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td><input checked='true' id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span></span></label></td><td>" + dtResult.Rows[k]["ConsignmentType"] + "</td>");
                    }
                    if (k == 2)
                    {
                        sb.Append("</tr><tr>");
                    }
                }
                sb.Append("</tr>");
                objProperty.MarineJIR.FkConsignmentType = Convert.ToString(sb);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            
            return View(objProperty);
        }

        public ActionResult MarineTranshipment(string fkClaimId)
        {
            CSGenrate objProperty = new CSGenrate();
            try
            {
                objProperty.MarineTranshipment = new CSMarineTranshipment();
                objProperty.MarineTranshipment.FKClaimId = fkClaimId;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        public ActionResult MarineAssessmentDetails(string fkClaimId, string task)
        {
            CSGenrateClaim objproperty = new CSGenrateClaim();
            try
            {
                objproperty.FkClaimId = fkClaimId;
                objproperty.Task = task;
                ViewBag.AssessmentDetails = BLGenrateClaim.GetMarineAssessmentDetails(objproperty);
                ViewBag.GetMarineAssesment = BLGenrateClaim.GetMarineAssesment("Get", task, fkClaimId);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objproperty);
        }
        /////////////////////////////////////////////////////////////////////////////////////
    }
}
