﻿using BusinessLayer.BL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class BillController : Controller
    {
        BLBill _vd = new BLBill();
        //
        // GET: /Bill/

        public ActionResult Bill(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult PaymentReceive(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult ReminderForFee(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #region Bill
        public JsonResult getModeOfConveyance()
        {
            string msg = null;
            List<CSMarineModeOfConveyance> listMC = new List<CSMarineModeOfConveyance>();
            listMC.Insert(0, new CSMarineModeOfConveyance { PK_MC_ID = 0, ModeOfConveyance = "Select" });
            try
            {
                listMC = _vd.getModeOfConveyance();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listMC = listMC, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getDate_byClaimID(int claimId)
        {
            string msg = null;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            List<CSBillVisitDetail> listSV = new List<CSBillVisitDetail>();
            try
            {
            listSV=  _vd.getDate_byClaimID(claimId,userId);
            }
            catch(Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listSV=listSV, msg = msg },JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInsrClaimNo_byClaimID(int claimId)
        {
            string msg = null;
            string claimNo = null;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
             claimNo = _vd.getInsrClaimNo_byClaimID(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new {claimNo=claimNo, msg = msg }, JsonRequestBehavior.AllowGet);

        }


        public JsonResult getBillHeader(int claimId)
        {
            string msg = null;
            List<CSBillHeader> listBH = new List<CSBillHeader>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listBH = _vd.getBillHeader(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new {listBH=listBH, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getBillVisitDetail_And_Expenses(int claimId,int bh_Id)
        {
            string msg = null;
            List<CSBillVisitDetail> listVD = new List<CSBillVisitDetail>();
            List<CSBillExpensesReimbursement> listER = new List<CSBillExpensesReimbursement>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listVD = _vd.getBillVisitDetail(claimId,userId,bh_Id);
                listER = _vd.getBillExpensesReimbursement(claimId, userId,bh_Id);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new {listVD=listVD,listER = listER, msg = msg }, JsonRequestBehavior.AllowGet);

        }

        public JsonResult saveBill(CSBillHeader bh, List<CSBillVisitDetail> listVD, List<CSBillExpensesReimbursement> listER, int claimId)
        {
            string msg = null;
            int status = 0;
            int bh_Id = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                bh.ClaimID = claimId;
                bh.UserID = userId;
                bh_Id = _vd.saveBillHeader(bh);
                foreach (CSBillVisitDetail vd in listVD)
                {
                    vd.UserID = userId;
                    vd.ClaimID = claimId;
                    vd.BH_ID = bh_Id;
                    status = _vd.saveBillVisitDetail(vd);
                }
                foreach (CSBillExpensesReimbursement er in listER)
                {
                    er.ClaimID = claimId;
                    er.UserID = userId;
                    er.BH_ID = bh_Id;
                    //if (bh_Id != 0)
                    //{
                    //    er.BH_ID = bh_Id;
                    //}
                    //else if(bh_Id==0)
                    //{
                    //    er.BH_ID = bh.PK_BH_ID;
                    //}
                   
                    status = _vd.saveBillExpensesReimbursement(er);
                }

                msg = status > 0 ? "Saved Successfully!" : "Failed! Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }


        public JsonResult deleteBillExpensesReimbursement(int pk_Er_Id)
        {
            string msg = null;
            int status = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _vd.deleteBillExpensesReimbursement(pk_Er_Id, userId);
                msg = status > 0 ? "Delete Successfully!" : "Failed! Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }

        #endregion
        #region Payment Receive
        public JsonResult getJobNo_byClaimID(int claimId)
        {
            string msg = null;
            string JobNo = null;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                JobNo = _vd.getJobNo_byClaimID(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { JobNo, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getPaymentReceive(int claimId)
        {
            string msg = null;
            List<CSPaymentReceive> listPR = new List<CSPaymentReceive>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPR = _vd.getPaymentReceive(claimId,userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listPR=listPR, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult savePaymentReceive(CSPaymentReceive pr)
        {
            string msg = null;
            int status = 0;
            try
            {
                pr.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _vd.savePaymentReceive(pr);
                msg = status > 0 ? "Save Success":"Failed";
                
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }

        #endregion
        #region Reminder For Fee
        public JsonResult getReminderForFree(int claimId, int days)
        {
            string msg = null;
            List<CSReminderForFree> listRF = new List<CSReminderForFree>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listRF = _vd.getReminderForFree(claimId,userId,days);
            }
         catch(Exception ex)
            {

            }
            return Json(new { listRF=listRF,msg=msg},JsonRequestBehavior.AllowGet);
        }
        public JsonResult getReminderForFree_byRF_ID(int claimId, int rfh_Id)
        {
            string msg = null;
            List<CSReminderForFree> listRF = new List<CSReminderForFree>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listRF = _vd.getReminderForFree_byRF_ID(claimId,userId,rfh_Id);
            }
            catch (Exception ex)
            {

            }
            return Json(new { listRF = listRF, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveReminder(CSReminderForFeeHeader rh, List<CSReminderForFree> listRF,int claimId)
        {
            string msg = null;
            int status = 0;
            int rfh_Id = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                rh.ClaimID = claimId;
                rh.UserID = userId;
                rfh_Id = _vd.saveReminderForFeeHeader(rh);
                foreach (CSReminderForFree rf in listRF)
                {
                    rf.UserID = userId;
                    rf.ClaimID = claimId;
                    rf.RFH_ID = rfh_Id;
                   status= _vd.saveReminderForFree(rf);

                }
                msg = status > 0 ? "Saved Successfully" : "Failed! Try Again.";
            }
            catch(Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getReminderForFeeHeader(int claimId)
        {
            string msg = null;
            List<CSReminderForFeeHeader> listRH = new List<CSReminderForFeeHeader>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listRH = _vd.getReminderForFeeHeader(claimId,userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listRH = listRH, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
