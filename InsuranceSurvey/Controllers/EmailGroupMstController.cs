﻿using BusinessLayer.BL;
using BusinessLayer.Models;
using Org.BouncyCastle.X509;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Security;
using System.Security.Cryptography.X509Certificates;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class EmailGroupMstController : Controller
    {
        BLEmailGroupMst _eg = new BLEmailGroupMst();

        //
        // GET: /EmailGroupMst/
        #region EmailGroup Master
        public ActionResult EmailGroupMst()
        {
            return View();
        }


        public JsonResult saveEmailGroupMst(CSEmailGroupMst egm)
        {
            string msg = null;
            int status = 0;
            try
            {

                status = _eg.saveEmailGroupMst(egm);
                msg = status > 0 ? "Save Successfully" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getEmailGroupMst()
        {
            string msg = null;
            int status = 0;
            List<CSEmailGroupMst> listEGM = new List<CSEmailGroupMst>();
            try
            {

                listEGM = _eg.getEmailGroupMst();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listEGM = listEGM, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region EmailGroup
        public ActionResult EmailGroup()
        {
            return View();
        }
        public JsonResult saveEmailGroup(CSEmailGroup eg)
        {
            string msg = null;
            int status = 0;
            try
            {

                status = _eg.saveEmailGroup(eg);
                msg = status > 0 ? "Save Successfully" : "Failed! Try Again";

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getEmailGroup()
        {
            string msg = null;
            int status = 0;
            List<CSEmailGroup> listEG = new List<CSEmailGroup>();
            try
            {

                listEG = _eg.getEmailGroup();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listEG = listEG, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        #endregion
        #region Log Email
        public ActionResult LogEmail()
        {
            return View();
        }
        public JsonResult sendEmail(int groupId,string subject, string mailMsg)
        {
            string msg = null;
            DataTable dt = new DataTable();
            dt = _eg.getEmailId_byGroupId(groupId);
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                foreach (DataRow row in dt.Rows)
                {
                    // using for bcc:-bcc means person ni dekh skta kis kis ko mail gyi h

                    mail.Bcc.Add(row["EmailId"].ToString());
                    // using for cc:-cc meand person dekh skta h kis kis ko mail gyi h
                    //mail.CC.Add(row["EmailId"].ToString());
                }

                mail.From = new MailAddress("oknfeesoftware@gmail.com");
                mail.Subject = subject;
                string Body = mailMsg;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                smtp.EnableSsl = true;
                smtp.Send(mail);
                msg = "s";
            }
            catch(Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
