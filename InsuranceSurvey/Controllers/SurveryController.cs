﻿using BusinessLayer;
using BusinessLayer.Models;
using InsuranceSurvery;
using InsuranceSurvey.BusinessLayer;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Net;
using System.Net.Mail;
using BusinessLayer.BL;
using System.Globalization;
using System.Web.Hosting;

namespace InsuranceSurvey.Controllers
{
    public class SurveryController : Controller
    {
        //
        // GET: /Survery/

        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult GenrateClaim(CSGenrateClaim objProperty, string command, string ClaimID)
        {
            string mes = "";
            if (command == "Submit")
            {
                string result = "";
                objProperty.FkSurId = Convert.ToInt32(Session["InsuranceUserId"]);
                BLGenrateClaim obj = new BLGenrateClaim();
                result = obj.GenrateClaimOneTime(objProperty);

                if (result == "Y")
                {
                    mes = "New Claim Generated Successfully";
                    TempData["msg"] = mes;
                    return RedirectToAction("GenrateClaim", "Survery");
                }
                else if (result == "U")
                {
                    mes = "Claim Updated Successfully";
                    TempData["msg"] = mes;
                    return RedirectToAction("manageclaims", "Survery");
                }
                else
                {
                    mes = "Error";
                    TempData["msg"] = mes;
                    return View(objProperty);
                }
            }
            else if (ClaimID != null)
            {
                DataTable objDataTable = BLGenrateClaim.GetClaimDetailsForEdit(ClaimID);
                objProperty = new CSGenrateClaim();
                objProperty.InsdType = objDataTable.Rows[0]["InsdType"].ToString();
                objProperty.Name = objDataTable.Rows[0]["InsdName"].ToString();
                objProperty.Email = objDataTable.Rows[0]["InsdEmail"].ToString();
                objProperty.Mobile = objDataTable.Rows[0]["InsdMobile"].ToString();
                objProperty.Address = objDataTable.Rows[0]["InsdAddress"].ToString();
                objProperty.PolicyNo = objDataTable.Rows[0]["PolicyNo"].ToString();
                objProperty.FkDepartment = Convert.ToInt16(objDataTable.Rows[0]["FK_Department"]);
                objProperty.FkSurId = Convert.ToInt16(objDataTable.Rows[0]["FK_SurID"]);
                objProperty.FkInsrId = Convert.ToInt16(objDataTable.Rows[0]["FK_InsrID"]);
                objProperty.InsrAddress = objDataTable.Rows[0]["InsrAddress"].ToString();
                objProperty.InsrClaimNo = objDataTable.Rows[0]["InsrClaimNo"].ToString();
                objProperty.InstructorName = objDataTable.Rows[0]["InstructorName"].ToString();
                objProperty.InstructionDate = objDataTable.Rows[0]["InstructionDate"].ToString();
                objProperty.IEmail = objDataTable.Rows[0]["IEmail"].ToString();
                objProperty.ReportedLoss = objDataTable.Rows[0]["ReportedLoss"].ToString();
                objProperty.LocationOfSurvey = objDataTable.Rows[0]["LocationOfSurvey"].ToString();
                objProperty.NatureOfLossId = Convert.ToInt32(objDataTable.Rows[0]["NatureOfLossId"]);
                objProperty.FkClaimId = ClaimID;
                // For Motor
                objProperty.VRegNo = objDataTable.Rows[0]["VRegNo"].ToString();
                objProperty.SurveyType = objDataTable.Rows[0]["ReportedLoss"].ToString();
                return View(objProperty);
            }
            else
            {
                objProperty = new CSGenrateClaim();
                return View(objProperty);
            }
        }

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        public ActionResult ManageClaims()
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                if (MySession.userType == null)
                {
                    return RedirectToAction("login", "login");
                }


                if (MySession.userType.ToLower() == "survery")
                {
                    objProperty.FkSurId = MySession.insuranceUserId;
                    ViewBag.ClaimDetails = BLGenrateClaim.ViewMyClaims(objProperty);
                }
                else if (MySession.userType.ToLower() == "insured")
                {
                    objProperty.FkInsrId = MySession.insuranceUserId;
                    ViewBag.ClaimDetails = BLGenrateClaim.ViewInsuredClaims(objProperty);
                }
                else
                {
                    ViewBag.ClaimDetails = BLAssignedClaims.BindTraineeAssignedTask(MySession.insuranceUserId.ToString());
                }

            }
            catch (Exception ex)
            {

                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View();
        }

        public ActionResult AssignTask()
        {
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                objProperty.FkSurId = Convert.ToInt32(MySession.insuranceUserId);
                ViewBag.ClaimDetails = BLGenrateClaim.GetTraineeClaims(objProperty);
                ViewBag.BindTrainees = BLAssignedClaims.BindTaskAssign("GetMyTrainees", Convert.ToString(Session["InsuranceUserId"]));
                //BindTrainees("Y");
                //StringBuilder _option = new StringBuilder();
                //_option.Append("<option value=\"0\">-Select-</option>");
                //foreach (DataRow row in dtResult.Rows)
                //{
                //    _option.Append("<option value=\"" + Convert.ToString(row["PK_TSURID"]) + "\">" + Convert.ToString(row["UserName"]) + "</option>");
                //}
                //ViewBag.BindTrainees = _option.ToString();
            }
            catch (Exception ex)
            {

                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View();
        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult UploadDocument(CSSurveyorsDocument objProperty, string command)
        {
            string fileName = "";
            objProperty.FkSurId = Convert.ToInt32(MySession.insuranceUserId);
            if (!string.IsNullOrEmpty(command))
            {
                if (objProperty.PassportPhoto != null)
                {
                    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.PassportPhoto.FileName);
                    objProperty.PassportPhoto.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
                    objProperty.PassportPhotoPath = fileName;
                }
                if (objProperty.DigitalSign != null)
                {
                    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.DigitalSign.FileName);
                    objProperty.DigitalSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
                    objProperty.DigitalSignPath = fileName;
                }
                if (objProperty.StampSign != null)
                {
                    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.StampSign.FileName);
                    objProperty.StampSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
                    objProperty.StampSignPath = fileName;
                }
                if (objProperty.SenderSign != null)
                {
                    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.SenderSign.FileName);
                    objProperty.SenderSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
                    objProperty.SenderSignPath = fileName;
                }
                if (objProperty.LetterHead != null)
                {
                    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.LetterHead.FileName);
                    objProperty.LetterHead.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
                    objProperty.LetterHeadPath = fileName;
                }

                BLSurveyorsRegistration obj = new BLSurveyorsRegistration();
                obj.AddSurveyorDocs(objProperty);

                TempData["mes"] = "Save successfully";
                return RedirectToAction("UploadDocument", "Survery");
            }
            else
            {
                objProperty = new CSSurveyorsDocument();
                DataTable dtResult = BLSurveyorsRegistration.BindSurveyorsDocsDetails(Convert.ToInt32(Session["InsuranceUserId"]));
                if (dtResult.Rows.Count >= 1)
                {
                    objProperty.PassportPhotoPath = Convert.ToString(dtResult.Rows[0]["PassportPhoto"]);
                    objProperty.DigitalSignPath = Convert.ToString(dtResult.Rows[0]["DigitalSign"]);
                    objProperty.StampSignPath = Convert.ToString(dtResult.Rows[0]["StampSign"]);
                    objProperty.SenderSignPath = Convert.ToString(dtResult.Rows[0]["StampSign"]);
                    objProperty.LetterHeadPath = Convert.ToString(dtResult.Rows[0]["LetterHead"]);
                }
                return View(objProperty);
            }

            //SurveryorDocument

        }


        //[AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        //public ActionResult UploadDocument(CSSurveyorsDocument objProperty, string command)
        //{
        //    string fileName = "";
        //    if (!string.IsNullOrEmpty(command))
        //    {
        //        objProperty.FkSurId = Convert.ToInt32(Session["InsuranceUserId"]);
        //        if (objProperty.PassportPhoto != null)
        //        {
        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.PassportPhoto.FileName);
        //            objProperty.PassportPhoto.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
        //            objProperty.PassportPhotoPath = fileName;
        //        }
        //        if (objProperty.DigitalSign != null)
        //        {
        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.DigitalSign.FileName);
        //            objProperty.DigitalSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
        //            objProperty.DigitalSignPath = fileName;
        //        }
        //        if (objProperty.StampSign != null)
        //        {
        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.StampSign.FileName);
        //            objProperty.StampSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
        //            objProperty.StampSignPath = fileName;
        //        }
        //        if (objProperty.SenderSign != null)
        //        {
        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.SenderSign.FileName);
        //            objProperty.SenderSign.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
        //            objProperty.SenderSignPath = fileName;
        //        }
        //        if (objProperty.LetterHead != null)
        //        {
        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.LetterHead.FileName);
        //            objProperty.LetterHead.SaveAs(Path.Combine(Server.MapPath("~/SurveryorDocument/"), fileName));
        //            objProperty.LetterHeadPath = fileName;
        //        }
        //        BLSurveyorsRegistration obj = new BLSurveyorsRegistration();
        //        obj.AddSurveyorDocs(objProperty);

        //        TempData["mes"] = "Save successfully";
        //        return RedirectToAction("UploadDocument", "Survery");
        //    }
        //    else
        //    {
        //        objProperty = new CSSurveyorsDocument();
        //        DataTable dtResult = BLSurveyorsRegistration.BindSurveyorsDocsDetails(Convert.ToInt32(Session["InsuranceUserId"]));
        //        if (dtResult.Rows.Count >= 1)
        //        {
        //            objProperty.PassportPhotoPath = Convert.ToString(dtResult.Rows[0]["PassportPhoto"]);
        //            objProperty.DigitalSignPath = Convert.ToString(dtResult.Rows[0]["DigitalSign"]);
        //            objProperty.StampSignPath = Convert.ToString(dtResult.Rows[0]["StampSign"]);
        //            objProperty.SenderSignPath = Convert.ToString(dtResult.Rows[0]["StampSign"]);
        //            objProperty.LetterHeadPath = Convert.ToString(dtResult.Rows[0]["LetterHead"]);
        //        }
        //        return View(objProperty);
        //    }

        //    //SurveryorDocument

        //}
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        //public ActionResult MarineJIR(string fkClaimId, string CheckOpenTable, string task, string Fk_InsdID, string InsdType, string InsdName, string InsdAddress)
        public ActionResult MarineJIR(string fkClaimId, string task)
        {
            CSGenrate objProperty = new CSGenrate();

            try
            {

                //TempData["FromTable"] = "MarineJIR";
                CSGenrateClaim objGenrateClaim = new CSGenrateClaim();
                objGenrateClaim.FkClaimId = fkClaimId;
                var dtClaim = BLGenrateClaim.GetViewMyClaims(fkClaimId, Session["InsuranceUserId"].ToString());


                objProperty.GenrateClaim = new CSGenrateClaim();

                //objProperty.GenrateClaim.Task = task;
                //objProperty.GenrateClaim.CheckOpenTable = CheckOpenTable;
                //objProperty.GenrateClaim.FkInsrId = Convert.ToInt32(Fk_InsdID);
                //objProperty.GenrateClaim.InsdType = Convert.ToChar(InsdType);
                //objProperty.GenrateClaim.InstructorName = InsdName;
                //objProperty.GenrateClaim.Address = InsdAddress;
                objProperty.GenrateClaim.FkClaimId = fkClaimId;

                objProperty.MarineJIR = new CSMarineJIR();
                objProperty.MarineJIR.DateOfSurvey = null;
                objProperty.MarineJIR = BLGenrateClaim.MarineGetJIR(fkClaimId);
                var ListExConditionPacking = BLCommon.getMarineExConditionPacking_JIR(objProperty.MarineJIR.ExConditionPacking);
                objProperty.MarineJIR.ListExConditionPacking = ListExConditionPacking;
                objProperty.MarineJIR.Task = task;
                objProperty.MarineJIR.FKClaimId = fkClaimId;
                BLGenrateClaim gc = new BLGenrateClaim();

                objProperty.MarineJIR.PlaceofSurvey = gc.getLocationOfSurvey(Convert.ToInt32(fkClaimId), Convert.ToInt32(Session["InsuranceUserId"]));

                if (dtClaim != null)
                {
                    objProperty.MarineJIR.InsdId = dtClaim.MarineJIR.InsdId;//Convert.ToInt32(dtClaim.Rows[0]["Fk_InsdID"]);
                    objProperty.MarineJIR.InsdType = dtClaim.MarineJIR.InsdType; // dtClaim.Rows[0]["InsdType "].ToString();
                    objProperty.MarineJIR.Insured = dtClaim.MarineJIR.Insured; // dtClaim.Rows[0]["InsdName"].ToString();
                    objProperty.MarineJIR.InsuredAddress = dtClaim.MarineJIR.InsuredAddress; //dtClaim.Rows[0]["InsdAddress"].ToString();
                }

                DataTable dtResult = new DataTable();
                dtResult = BLGenrateClaim.GetConsignmentType(fkClaimId);
                StringBuilder sb = new StringBuilder();
                sb.Append("<tr>");

                for (int k = 0; k < dtResult.Rows.Count; k++)
                {
                    if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                    {
                        sb.Append("<td><input id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span>" + dtResult.Rows[k]["ConsignmentType"] + "</span></label></td>");
                    }
                    else
                    {
                        sb.Append("<td><input checked='true' id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span>" + dtResult.Rows[k]["ConsignmentType"] + "</span></label></td>");
                    }
                    //if (k == 2)
                    //{
                    //    sb.Append("</tr><tr>");
                    //}
                }


                sb.Append("</tr>");
                objProperty.MarineJIR.FkConsignmentType = Convert.ToString(sb);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objProperty);
        }


        public ActionResult MarineConsignmentDocs(string fkClaimId, string task, string ConsimentType)
        {
            CSMarineConsignmentDocs objProperty = new CSMarineConsignmentDocs();
            try
            {
                objProperty.FKClaimId = Convert.ToInt32(fkClaimId);
                objProperty.Task = task;
                objProperty.ConsimentType = ConsimentType;
            }
            catch (Exception ex)
            {

                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        public ActionResult MarineDamageDetails(string fkClaimId, string task)
        {
            CSMarineDamageDetails objProperty = new CSMarineDamageDetails();
            CSMarineJIR objMarineJIR = new CSMarineJIR();
            try
            {

                objMarineJIR = BLGenrateClaim.MarineGetJIR(fkClaimId);
                objProperty.ConsimentType = objMarineJIR.Consiment;
                objProperty.FKClaimId = Convert.ToInt32(fkClaimId);
                objProperty.Task = task;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        public ActionResult MarineRemarksClaim(string fkClaimId, string task)
        {
            CSMarineJIR objProperty = new CSMarineJIR();
            try
            {
                objProperty = BLGenrateClaim.MarineGetJIR(fkClaimId);
                objProperty.FKClaimId = fkClaimId;
                objProperty.Task = task;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        public ActionResult LOR(string fkClaimId, string task)
        {
            CSMarineJIR objProperty = new CSMarineJIR();
            objProperty = BLGenrateClaim.MarineGetJIR(fkClaimId.Trim());
            objProperty.FKClaimId = fkClaimId;
            objProperty.Task = task.Trim();
            ViewBag.MarineGetLORReminders = BLGenrateClaim.MarineGetLORReminders(fkClaimId);
            return View(objProperty);
        }


        public ActionResult MarineJIRConsignment(string fkClaimId, string task)
        {
            //TempData["FromTable"] = "MarineJIRConsignment";
            CSGenrate objProperty = new CSGenrate();
            try
            {
                objProperty.GenrateClaim = new CSGenrateClaim();
                objProperty.MarineJIR = new CSMarineJIR();
                objProperty.GenrateClaim.FkClaimId = fkClaimId.Trim();

                //objProperty = BLGenrateClaim.GetViewMyClaims(fkClaimId.Trim(), Convert.ToString(Session["InsuranceUserId"]));
                objProperty = BLGenrateClaim.GetInsuredDetails(fkClaimId.Trim());

                objProperty.MarineJIR.DateOfSurvey = null;

                int InsdId = objProperty.MarineJIR.InsdId;
                string InsdType = objProperty.MarineJIR.InsdType.Trim();
                string Insured = objProperty.MarineJIR.Insured.Trim();
                string InsuredAddress = objProperty.MarineJIR.InsuredAddress.Trim();
                objProperty.MarineJIR = BLGenrateClaim.MarineGetJIR(fkClaimId.Trim());
                var ListExConditionPacking = BLCommon.getMarineExConditionPacking_JIR(objProperty.MarineJIR.ExConditionPacking);
                objProperty.MarineJIR.ListExConditionPacking = ListExConditionPacking;
                BLGenrateClaim gc = new BLGenrateClaim();
                objProperty.MarineJIR.PlaceofSurvey = gc.getLocationOfSurvey(Convert.ToInt32(fkClaimId), Convert.ToInt32(Session["InsuranceUserId"]));
                objProperty.MarineJIR.InsdId = InsdId;
                objProperty.MarineJIR.InsdType = InsdType;
                objProperty.MarineJIR.ResoneForDelay = "--";
                objProperty.MarineJIR.FKClaimId = fkClaimId;
                objProperty.MarineJIR.Insured = Insured;
                objProperty.MarineJIR.InsuredAddress = InsuredAddress;
                objProperty.MarineJIR.Task = task.Trim();

                objProperty.MarineISVR = new CSMarineISVR();
                objProperty.MarineISVR.FkClaimId = Convert.ToInt32(fkClaimId);

                objProperty.MarineConsignmentDocs = new CSMarineConsignmentDocs();
                objProperty.MarineConsignmentDocs.ConsimentType = objProperty.MarineJIR.Consiment;
                DataTable dtResult = new DataTable();
                dtResult = BLGenrateClaim.GetConsignmentType(fkClaimId);
                StringBuilder sb = new StringBuilder();
                sb.Append("<tr>");
                for (int k = 0; k < dtResult.Rows.Count; k++)
                {

                    if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                    {
                        sb.Append("<td><input id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span></span></label></td><td>" + dtResult.Rows[k]["ConsignmentType"] + "</td>");
                    }
                    else
                    {
                        sb.Append("<td><input checked='true' id=\"chJir" + k.ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["ConsignmentType"] + "\" /><label><span></span></label></td><td>" + dtResult.Rows[k]["ConsignmentType"] + "</td>");
                    }
                    if (k == 2)
                    {
                        sb.Append("</tr><tr>");
                    }
                }
                sb.Append("</tr>");
                objProperty.MarineJIR.FkConsignmentType = Convert.ToString(sb);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objProperty);
        }


        public ActionResult MarineISVR(string task, string fkClaimId)
        {
            CSMarineISVR objProperty = new CSMarineISVR();
            CSMarineJIR objPropertyJIR = new CSMarineJIR();
            try
            {
                objPropertyJIR = BLGenrateClaim.MarineGetJIR(fkClaimId.Trim());

                objProperty.InsdId = objPropertyJIR.InsdId;
                objProperty.InsdType = objPropertyJIR.InsdType;
                task = task.Trim();
                if (task == "JIR_ISVR" || task.ToUpper() == "JIR")
                    task = "ISVR";

                if (task.ToLower() == "ISVR".ToLower() || task.ToLower() == "FSR".ToLower() || task == "SpotSurvey")
                {
                    objProperty.FkClaimId = Convert.ToInt32(fkClaimId);
                    objProperty.Task = task;
                    DataTable dtMarineISVR = BLGenrateClaim.GetMarineISVR(fkClaimId);
                    int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                    BLGenrateClaim gc = new BLGenrateClaim();
                    List<CSMarineISVRNotes> listNt = new List<CSMarineISVRNotes>();
                    listNt = gc.getMarineISVRNotes(Convert.ToInt32(fkClaimId), userId);
                    List<CSMarineISVRNotes> listFSRNt = new List<CSMarineISVRNotes>();
                    listFSRNt = gc.getMarineFSRNotes(Convert.ToInt32(fkClaimId), userId);
                    if (dtMarineISVR.Rows.Count > 0)
                    {
                        if (task.ToLower() == "ISVR".ToLower())
                        {
                            objProperty.EstimationOfLoss = Convert.ToString(dtMarineISVR.Rows[0]["ReportedLoss"]);
                        }
                        else
                        {
                            objProperty.EstimationOfLoss = gc.getTotalAmountC_byClaimId(Convert.ToInt32(fkClaimId), Convert.ToString(Session["InsuranceUserId"]));
                        }
                        objProperty.BasisOfReserve = Convert.ToString(dtMarineISVR.Rows[0]["BasisOfReserve"]);
                        objProperty.RecomendedReserve = Convert.ToString(dtMarineISVR.Rows[0]["RecomendedReserve"]);
                        objProperty.Remarks = Convert.ToString(dtMarineISVR.Rows[0]["Remarks"]);
                        objProperty.SurveyorsFinalRemarks = Convert.ToString(dtMarineISVR.Rows[0]["SurveyorsFinalRemarks"]);
                        objProperty.listNt = listNt;
                        objProperty.listFSRNt = listFSRNt;
                    }
                    //    DataTable dtMarinePhoto = BLGenrateClaim.GetMarineISVRPhotos(fkClaimId);
                    //  if (dtMarinePhoto.Rows.Count > 0)
                    // {
                    //DataRow[] row = dtMarinePhoto.Select("PhotoType='1'");
                    //if (row.Length != 0)
                    //{
                    //    objProperty.PhotoName1 = Convert.ToString(row[0]["PhotoName"]);
                    //    ViewBag.Photo1 = Convert.ToString(row[0]["PhotoPath"]);
                    //}
                    //row = dtMarinePhoto.Select("PhotoType='2'");
                    //if (row.Length != 0)
                    //{
                    //    objProperty.PhotoName2 = Convert.ToString(row[0]["PhotoName"]);
                    //    ViewBag.Photo2 = Convert.ToString(row[0]["PhotoPath"]);
                    //}
                    //row = dtMarinePhoto.Select("PhotoType='3'");
                    //if (row.Length != 0)
                    //{
                    //    objProperty.PhotoName3 = Convert.ToString(row[0]["PhotoName"]);
                    //    ViewBag.Photo3 = Convert.ToString(row[0]["PhotoPath"]);
                    //}
                    //row = dtMarinePhoto.Select("PhotoType='4'");
                    //if (row.Length != 0)
                    //{
                    //    objProperty.PhotoName4 = Convert.ToString(row[0]["PhotoName"]);
                    //    ViewBag.Photo4 = Convert.ToString(row[0]["PhotoPath"]);
                    //}
                    //}
                }

            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objProperty);
        }

        [HttpPost]
        public ActionResult MarineSubmitISVR(CSMarineISVR objProperty, HttpPostedFileBase[] files, FormCollection form, string[] notes, List<string> listNotes, List<int> listNotesId, List<string> listFSRNotes, List<int> listFSRNotesId)
        {
            string fileName = "";
            StringBuilder sb = new StringBuilder();
            try
            {

                objProperty.IPAddress = BusinessHelper.GetIpAdress();

                //var listNotes = form["notes"].ToList();
                //var pkIDs = form["PK_N_ID"].ToList();
                BLGenrateClaim gc = new BLGenrateClaim();
                int claimId = objProperty.FkClaimId;
                if (listNotes != null)
                {
                    for (int i = 0; i < listNotes.Count; i++)
                    {
                        //string notesName = listNotes[i];
                        //string notesId = listNotes[i];
                        CSMarineISVRNotes n = new CSMarineISVRNotes();
                        n.Notes = listNotes[i].ToString();
                        n.PK_N_ID = Convert.ToInt32(listNotesId[i]);
                        n.ClaimID = claimId;
                        n.UserID = Convert.ToInt32(MySession.insuranceUserId);
                        gc.saveMarineISVRNotes(n);

                    }
                }
                if (listFSRNotes != null)
                {
                    for (int i = 0; i < listFSRNotes.Count; i++)
                    {
                        CSMarineISVRNotes n = new CSMarineISVRNotes();
                        n.Notes = listFSRNotes[i].ToString();
                        n.PK_FsrN_ID = Convert.ToInt32(listFSRNotesId[i]);
                        n.ClaimID = claimId;
                        n.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                        gc.saveMarineFSRNotes(n);

                    }
                }


                //if (objProperty.UploadPhoto1 != null)
                //{
                //    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.UploadPhoto1.FileName);
                //    string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                //    objProperty.UploadPhoto1.SaveAs(Imgpath);
                //    BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(objProperty.FkClaimId), "1", "0", "1", objProperty.PhotoName1, fileName, objProperty.IPAddress, "Photo");
                //}
                //if (objProperty.UploadPhoto2 != null)
                //{
                //    fileName = "";
                //    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.UploadPhoto2.FileName);
                //    string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                //    objProperty.UploadPhoto2.SaveAs(Imgpath);
                //    BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(objProperty.FkClaimId), "1", "0", "2", objProperty.PhotoName2, fileName, objProperty.IPAddress, "Photo");
                //}
                //if (objProperty.UploadPhoto3 != null)
                //{
                //    fileName = "";
                //    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.UploadPhoto3.FileName);
                //    string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                //    objProperty.UploadPhoto3.SaveAs(Imgpath);
                //    BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(objProperty.FkClaimId), "1", "0", "3", objProperty.PhotoName3, fileName, objProperty.IPAddress, "Photo");
                //}
                //if (objProperty.UploadPhoto4 != null)
                //{
                //    fileName = "";
                //    fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(objProperty.UploadPhoto4.FileName);
                //    string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                //    objProperty.UploadPhoto4.SaveAs(Imgpath);
                //    BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(objProperty.FkClaimId), "1", "0", "4", objProperty.PhotoName4, fileName, objProperty.IPAddress, "Photo");
                //}

                int k = 0;
                if (objProperty.ImgType != null)
                {
                    string[] txtName = objProperty.PhotoDocumentName.Split('#');
                    string[] imgType = objProperty.ImgType.Split('#');
                    foreach (HttpPostedFileBase file in files)
                    {
                        if (file != null)
                        {
                            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(file.FileName);
                            string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                            file.SaveAs(Imgpath);
                            BLGenrateClaim.AddMarineSubmitISVRPhtotoDocument(Convert.ToString(objProperty.FkClaimId), "1", "0", "5", txtName[k].ToString(), fileName, objProperty.IPAddress, imgType[k]);
                        }
                        k++;
                    }
                }

                BLGenrateClaim obj = new BLGenrateClaim();
                objProperty.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                obj.AddMarineSubmitISVR(objProperty);
                if (objProperty.Task.ToUpper() == "FSR")
                {
                    new CommonService().saveTaskManager(claimId, MySession.insuranceUserId, "FSRC");
                }

                /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~* Open JIR From =~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
                CSGenrateClaim objClaim = new CSGenrateClaim();
                objClaim.InsrClaimNo = Convert.ToString(objProperty.FkClaimId);
                objClaim.FkSurId = Convert.ToInt32(Session["InsuranceUserId"]);
                DataTable dtResult = BLGenrateClaim.ViewMyClaims(objClaim);
                //string url = Request.Url.ToString().Replace("MarineSubmitISVR", "MarineISVR");
                string url = "/Survery/MarineISVR?fkClaimId=" + claimId + "&task=" + objProperty.Task + "";
                string task = objProperty.Task;

                sb.Append("<html>");
                sb.AppendFormat(@"<body onload='document.forms[""form""].submit()'>");
                sb.AppendFormat("<form action='{0}' name='form' enctype='multipart/form-data' id='form1' method='post'>", url);
                sb.AppendFormat("<input type='hidden' id='fkClaimId' name='fkClaimId' value='{0}' />", objProperty.FkClaimId);
                sb.AppendFormat("<input type='hidden' id='task' name='task' value='{0}' />", task);
                sb.AppendFormat("</form>");
                sb.Append("</body>");
                sb.Append("</html>");

            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return Content(sb.ToString());
        }

        public ActionResult MarineTranshipment(string fkClaimId, string task)
        {
            CSGenrate objProperty = new CSGenrate();
            try
            {
                var listC = BLCommon.getCommentForDeliveryReceipt();
                var listRN = BLCommon.getRemarksForNature();
                objProperty.MarineTranshipment = new CSMarineTranshipment();
                objProperty.MarineTranshipment.FKClaimId = fkClaimId;
                objProperty.MarineTranshipment.Task = task;
                objProperty.MarineTranshipment.listComment = listC;
                objProperty.MarineTranshipment.listRN = listRN;

            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }


        public ActionResult LorWork(string fkClaimId, string Task)
        {
            CSGenrate objProperty = new CSGenrate();
            ViewBag.MarineGetLORReminders = BLGenrateClaim.MarineGetLORReminders(fkClaimId);
            var dtClaim = BLGenrateClaim.GetViewMyClaims(fkClaimId, Session["InsuranceUserId"].ToString());
            objProperty.MarineJIR = new CSMarineJIR();
            DataSet dsLOR = BLGenrateClaim.GetMarineLOR(fkClaimId, dtClaim.MarineJIR.InsdType, dtClaim.MarineJIR.InsdId.ToString());

            objProperty.MarineJIR = BLGenrateClaim.MarineGetJIR(fkClaimId.Trim());

            objProperty.MarineJIR.FKClaimId = fkClaimId;
            objProperty.MarineJIR.InsdId = Convert.ToInt32(dtClaim.MarineJIR.InsdId);
            objProperty.MarineJIR.InsdType = dtClaim.MarineJIR.InsdType;
            objProperty.MarineJIR.Task = Task;
            if (dsLOR.Tables.Count > 0)
            {
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    objProperty.MarineJIR.PlaceofSurvey = Convert.ToString(dsLOR.Tables[0].Rows[0]["PlaceofSurvey"]);
                    objProperty.MarineJIR.DateOfSurvey = Convert.ToString(dsLOR.Tables[0].Rows[0]["DateOfSurvey"]);
                    objProperty.MarineJIR.Insured = Convert.ToString(dsLOR.Tables[0].Rows[0]["InsdName"]);
                    objProperty.MarineJIR.InsuredAddress = Convert.ToString(dsLOR.Tables[0].Rows[0]["InsdAddress"]);
                    objProperty.MarineJIR.PolicyNumber = Convert.ToString(dsLOR.Tables[0].Rows[0]["PolicyNo"]);
                    objProperty.MarineJIR.DispatchedFrom = Convert.ToString(dsLOR.Tables[0].Rows[0]["DispatchedFROM"]);
                    objProperty.MarineJIR.InsrClaimNo = Convert.ToString(dsLOR.Tables[0].Rows[0]["InsrClaimNo"]);
                    objProperty.MarineJIR.DynamicClaimId = Convert.ToString(dsLOR.Tables[0].Rows[0]["ClaimID"]);
                }
                if (dsLOR.Tables[1].Rows.Count > 0)
                {
                    objProperty.MarineJIR.InvNo = dsLOR.Tables[1].Rows[0]["Inv. No."].ToString();
                    objProperty.MarineJIR.InvDate = dsLOR.Tables[1].Rows[0]["Invoice Date"].ToString();
                    objProperty.MarineJIR.TypeNumber = dsLOR.Tables[1].Columns[6].ToString();
                    objProperty.MarineJIR.TypeNumberValue = dsLOR.Tables[1].Rows[0][6].ToString();
                    objProperty.MarineJIR.TypeDate = dsLOR.Tables[1].Rows[0][7].ToString();
                }
            }
            return View(objProperty);
        }

        public ActionResult ClaimForm(string fkClaimId, string task)
        {
            //List<CSMarineModeOfConveyance> listMC = new List<CSMarineModeOfConveyance>();
            //var  listMC = BLCommon.getModeOfConveyance();
            CSGenrateClaim objProperty = new CSGenrateClaim();
            try
            {
                objProperty = BLGenrateClaim.GetPolicyParticulars(fkClaimId);
                var listMC = BLCommon.getModeOfConveyance(objProperty.ModeConveyance);
                objProperty.ListModeOfConveyance = listMC;
                objProperty.FkClaimId = fkClaimId;
                objProperty.Task = task;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        public ActionResult MarineAssessmentDetails(string fkClaimId, string AssessmentType, string task)
        {
            CSGenrateClaim objproperty = new CSGenrateClaim();
            try
            {
                CSMarineJIR objPropertyJIR = new CSMarineJIR();
                objPropertyJIR = BLGenrateClaim.MarineGetJIR(fkClaimId);


                objproperty.FkClaimId = fkClaimId;
                objproperty.Task = task;
                objproperty.AssessmentType = AssessmentType;

                objproperty.InsdType = Convert.ToString(objPropertyJIR.InsdType);
                objproperty.FkInsrId = objPropertyJIR.InsdId;

                if (task == "Claim")
                {
                    objproperty.AssessmentType = "CL";
                }
                else if (task == "FSR")
                {
                    objproperty.AssessmentType = "AS";
                }
                ViewBag.AssessmentDetails = BLGenrateClaim.GetMarineAssessmentDetails(objproperty);
                ViewBag.GetMarineAssesment = BLGenrateClaim.GetMarineAssesment("Get", AssessmentType, fkClaimId);
                ViewBag.GetMarineAssesmentPOP = BLGenrateClaim.GetMarineAssesment("Get", "CL", fkClaimId);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objproperty);
        }

        public ActionResult SurveyVisits(string fkClaimId, string task)
        {
            CSSurveyorVisits objProperty = new CSSurveyorVisits();
            try
            {
                ViewBag.SurveyorVisits = BLGenrateClaim.GetSurveyVisits(fkClaimId);
                objProperty.FkClaimId = fkClaimId;
                objProperty.Task = task;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objProperty);
        }

        //public ActionResult SubmitFSR(CSFSR objProperty, string fkClaimId)
        public ActionResult SubmitFSR(string fkClaimId, string task)
        {
            CSFSR objProperty = new CSFSR();
            try
            {
                objProperty.Task = task;

                objProperty.FKClaimId = fkClaimId;
                CSMarineJIR objPropertyJIR = new CSMarineJIR();
                objPropertyJIR = BLGenrateClaim.MarineGetJIR(fkClaimId);

                DataTable dtResult = BLGenrateClaim.GetMarineFSR(fkClaimId);
                if (dtResult.Rows.Count > 0)
                {

                    objProperty.Concurrence = Convert.ToString(dtResult.Rows[0]["Concurrence"]);
                    objProperty.Salvage = Convert.ToString(dtResult.Rows[0]["Salvage"]);
                    objProperty.CommentPolicy = Convert.ToString(dtResult.Rows[0]["CommentPolicy"]);
                    objProperty.OtherRemark = Convert.ToString(dtResult.Rows[0]["OtherRemark"]);
                    objProperty.CommentPolicy_Com_NotCom = Convert.ToString(dtResult.Rows[0]["CommentPolicy_Com_NotCom"]);
                }
                else
                {
                    string remarks = string.Format("The undersigned have following comments on subrogation/recovery rights. The Insured have lodged monetary claim on carrier / s, vide their letter No.______ dated _______ by registered post, to save recovery right for damaged / short delivery, copy of same with acknowledgement is enclosed with this report.The Insured have obtained damaged / shortage certificate from carrier, the same is enclosed with this report.The Insurers are advised to obtained recovery rights on appropriate stamp paper from Insured at the time of final settlement of the claim.");
                    objProperty.OtherRemark = remarks;
                }

                objProperty.InsdId = objPropertyJIR.InsdId;
                objProperty.InsdType = objPropertyJIR.InsdType;
                objProperty.Task = objProperty.Task;

            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }


            return View(objProperty);
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SurveyorTraineeInfo(string IsLinked)
        {
            try
            {
                ViewBag.TraineeDetails = BindTrainees(IsLinked);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View();
        }

        private DataTable BindTrainees(string IsLinked)
        {
            CSTraineeSurveyorRegistration objProperty = new CSTraineeSurveyorRegistration();
            DataTable dtresult = new DataTable();
            try
            {
                objProperty.Mode = "ViewConnections";
                objProperty.FkSurId = Convert.ToInt32(Session["InsuranceUserId"]);

                if (!string.IsNullOrEmpty(IsLinked))
                    objProperty.IsLinked = Convert.ToChar(IsLinked);
                else
                    objProperty.IsLinked = 'N';

                dtresult = BLTraineeSurveyorRegistration.SurveyorTraineeInfo(objProperty);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return dtresult;
        }

        public ActionResult TraineeSendRequest()
        {
            return View();
        }

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/


        public iTextSharp.text.Font NormalFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
        public iTextSharp.text.Font BoldFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 10, iTextSharp.text.Font.BOLD, BaseColor.BLACK);
        public iTextSharp.text.Font BoldFont12 = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 12, iTextSharp.text.Font.BOLD, BaseColor.BLACK);


        public PdfPCell GetFontWithStyle(string Text, iTextSharp.text.Font Font, int Alignment, string IsBorder = "", int Colspan = 0)
        {
            PdfPCell cell = new PdfPCell(new Phrase(Text, Font));
            cell.HorizontalAlignment = Alignment;
            cell.VerticalAlignment = Element.ALIGN_BOTTOM;
            cell.BorderColor = BaseColor.LIGHT_GRAY;
            if (IsBorder == "N")
            {
                cell.BorderWidth = 0f;
            }
            if (Colspan != 0)
            {
                cell.Colspan = Colspan;
            }
            return cell;
        }

        public PdfPCell GetSeprator(iTextSharp.text.Font Font)
        {
            PdfPCell cell = new PdfPCell(new Phrase(":", Font));
            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
            cell.BorderColor = BaseColor.LIGHT_GRAY;
            return cell;
        }

        public PdfPCell AddBlankRow()
        {
            Phrase phrase = new Phrase(Chunk.NEWLINE);
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderWidth = 0f;
            cell.PaddingTop = 5f;
            return cell;
        }

        public PdfPCell AddBlankRow(int Colspan)
        {
            Phrase phrase = new Phrase(Chunk.NEWLINE);
            PdfPCell cell = new PdfPCell(phrase);
            cell.BorderWidth = 0f;
            cell.PaddingTop = 5f;
            cell.Colspan = Colspan;
            return cell;
        }


        private PdfPCell ImageCell(string path, int align, float Height, float Width)
        {

            iTextSharp.text.Image image = null;
            path = Server.MapPath("~/MarineISVRPhotos/" + path);
            if (path.CheckFilePath())
            {
                image = iTextSharp.text.Image.GetInstance(path);
            }
            image.ScaleAbsoluteWidth(Width);
            image.ScaleAbsoluteHeight(Height);
            PdfPCell cell = new PdfPCell(image);
            cell.BorderColor = BaseColor.BLACK;
            cell.HorizontalAlignment = align;


            //cell.PaddingBottom = 0f;
            //cell.PaddingTop = 0f;
            return cell;
        }

        [HttpGet]
        public FileResult GeneratePdf(string ClaimID, string InsdType, string InsdID, string Task)
        {
            try
            {


                //ClaimID = "1";
                //InsdType = "O";
                //InsdID = "1";
                if (Task == "JIR")
                {
                    return GeneratePdfJIR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "LOR")
                {
                    return GeneratePdfLOR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "FSR")
                {
                    return GeneratePdfFSR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "ISVR")
                {
                    return GeneratePdfISVR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "Reminder")
                {
                    return GenerateReminderLOR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "Spot")
                {
                    //return GeneratePdfSpotSurvey(ClaimID, InsdType, InsdID);
                    return GeneratePdfISVR(ClaimID, InsdType, InsdID);
                }
                else if (Task == "NoClaim")
                {
                    return GeneratePdfNoClaim(ClaimID, InsdType, InsdID);
                }
                else
                {
                    return GeneratePdfClaimForm(ClaimID, InsdType, InsdID);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;

                if (msg.Contains("There is no row at position 0."))
                {
                    Response.Write("No Record Found");
                }
                else
                {
                    Response.Write("Some Error Occured. Please check.");
                }
                return null;
            }
        }

        public FileResult GeneratePdfJIR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineJIR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();

            workStream = JIRMemoryStream(ClaimID, InsdType, InsdID);

            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream JIRMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineJIR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMarine.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;
                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 10f, 10f, 20f, 45f);

                //Document document = new Document();
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;

                document.Open();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Ref No. " + dsMarine.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                cell = GetFontWithStyle("JOINT INSPECTION REPORT", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);
                Footer objFooter = new Footer();
                objFooter.RefNo = dsMarine.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Joint Inspection Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                if (dsMarine.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle("POLICY TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("POLICY NO.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF INSTRUCTION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InstructionDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DateOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("PLACE OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LocationOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INSURED ADDRESS", BoldFont, PdfPCell.ALIGN_TOP));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsdAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT-TRANSIT PARTICULARS", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 3));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNOR NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNEE NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CARRYING VEHICLE REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("LOAD ON CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LoadOnCarryingVehicle"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("GVW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("ULW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CAUSE OF LOSS DURING TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Other")
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLossOther"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Road Accident" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Hi-jacking" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Fire")
                    {
                        table.AddCell(GetFontWithStyle("PLACE OF OCCURENCE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceOfAccident"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT TRANSHIPPED TO ", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentTransshipedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("SPOT SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("POLICE REPORT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PoliceReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }
                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMarine.Tables[1].Columns.Count);
                if (dsMarine.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("INVOICE/LORRY RECEIPTS", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The consignment was transit vide following Documents.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                if (dsMarine.Tables[2].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(dsMarine.Tables[2].Columns.Count);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("DAMAGE DETAIL OF CONSIGNMENT RECEIVED TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = 8;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[2].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[2].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[2].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = 8;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                }
                if (dsMarine.Tables[0].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("CAUSES OF LOSS AS DESCRIBED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("DELAY IN INTIMATION TO THE INSURERS (IF ANY) AS CLARIFIED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DelayInINTimation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("DELAY IN TAKING DELIVERY (IF ANY) AS DESCRIBED BY THE CONSIGNEE’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DelayINTakingDelivery"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    document.Add(table);
                }
                // Starts New Page
                //document.NewPage();
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });

                if (dsMarine.Tables[4].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("SURVEYOR’S REMARKS/ ADVICE TO THE INSURED.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMarine.Tables[4].Rows.Count; i++)
                    {
                        if (dsMarine.Tables[4].Rows[i]["RCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMarine.Tables[4].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    document.Add(table);
                }


                if (dsMarine.Tables[3].Rows.Count > 0)
                {

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });

                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMarine.Tables[3].Rows.Count; i++)
                    {
                        if (dsMarine.Tables[3].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("     - " + dsMarine.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF SURVEY VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsMarine.Tables[3].Rows.Count; i++)
                    {
                        if (dsMarine.Tables[3].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("     - " + dsMarine.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    document.Add(table);
                }

                // Add Contact Person Details

                if (dsMarine.Tables[5].Rows.Count > 0)
                {
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.50f, 0.50f });
                    cell = GetFontWithStyle("For: " + dsMarine.Tables[5].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("For: " + dsMarine.Tables[5].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Contact Person: " + dsMarine.Tables[5].Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[5].Rows[0]["Mobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[5].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMarine.Tables[5].Rows[0]["Email"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMarine.Tables[5].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                    table.AddCell(cell);
                    document.Add(table);
                }
                document.Close();

                //byte[] byteInfo = workStream.ToArray();
                //workStream.Write(byteInfo, 0, byteInfo.Length);
                //workStream.Position = 0;

            }
            return workStream;
        }

        public FileResult GeneratePdfISVR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineISVR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            workStream = ISVRMemoryStream(ClaimID, InsdType, InsdID);

            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
            //if(workStream==null)
            //{
            //    workStream = new MemoryStream();
            //}
            //return new FileStreamResult(workStream, "application/pdf");
        }
        public MemoryStream ISVRMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineISVR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();

            if (dsMarine.Tables.Count > 0)
            {
                if (dsMarine.Tables[0].Rows.Count > 0)
                {

                    PdfPCell cell = null;
                    PdfPTable table = null;

                    Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 85f, 45f);
                    //Document document = new Document();
                    //document.SetMargins(10f,10f,40f,20f);
                    PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                    writer.CloseStream = false;

                    if (dsMarine.Tables[0].Rows.Count > 0)
                    {
                        Header objHeader = new Header();
                        objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["LetterHead"].ToString());
                        writer.PageEvent = objHeader;

                        Footer objFooter = new Footer();
                        objFooter.RefNo = dsMarine.Tables[0].Rows[0]["ClaimID"].ToString();
                        objFooter.ReportName = "Instant Site Visit Report";
                        objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["DSign"].ToString());
                        writer.PageEvent = objFooter;

                    }

                    document.OpenDocument();
                    writer.PageEvent = new Footer();
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Insurer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Address: " + dsMarine.Tables[0].Rows[0]["InsrAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("REF NO. " + dsMarine.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("INSURER CLAIM NO. " + dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    cell = GetFontWithStyle("INSTANT SITE VISIT REPORT", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                    if (dsMarine.Tables[0].Rows.Count > 0)
                    {

                        table.AddCell(GetFontWithStyle("POLICY TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("POLICY NO.", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DATE OF INSTRUCTION", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InstructionDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DATE OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DateOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("PLACE OF SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LocationOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("INSURED NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("INSURED ADDRESS", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsdAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT-TRANSIT PARTICULARS", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 3));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                        //table.AddCell(GetSeprator(BoldFont));
                        //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNOR NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNEE NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                        //table.AddCell(GetSeprator(BoldFont));
                        //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CARRYING VEHICLE REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("LOAD ON CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LoadOnCarryingVehicle"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("GVW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("ULW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CAUSE OF LOSS DURING TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Other")
                        {
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLossOther"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        }
                        else
                        {
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        }
                        if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Road Accident" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Hi-jacking" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Fire")
                        {
                            table.AddCell(GetFontWithStyle("PLACE OF OCCURENCE", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceOfAccident"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("CONSIGNMENT TRANSHIPPED TO ", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentTransshipedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("SPOT SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("POLICE REPORT", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PoliceReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        }
                        cell = AddBlankRow();
                        cell.Colspan = 3;
                        table.AddCell(cell);
                        document.Add(table);
                    }
                    // Docs Table

                    PdfPTable PdfTable = new PdfPTable(dsMarine.Tables[1].Columns.Count);
                    if (dsMarine.Tables[1].Rows.Count > 0)
                    {
                        int Colspan = dsMarine.Tables[1].Columns.Count;
                        PdfTable.TotalWidth = 500f;
                        PdfTable.WidthPercentage = 100f;
                        PdfTable.LockedWidth = true;
                        //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                        PdfTable.AddCell(AddBlankRow(Colspan));
                        cell = GetFontWithStyle("CONSIGNMENT TRANSIT DOCUMENTS", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.Colspan = Colspan;
                        cell.PaddingBottom = 5f;
                        PdfTable.AddCell(cell);
                        //cell = GetFontWithStyle("The consignment was transit vide following Documents.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        //cell.Colspan = Colspan;
                        //cell.PaddingBottom = 5f;
                        //PdfTable.AddCell(cell);

                        for (int rows = 0; rows < dsMarine.Tables[1].Rows.Count; rows++)
                        {
                            if (rows == 0)
                            {
                                for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                                {
                                    cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[1].Columns[column].ColumnName, BoldFont)));
                                    cell.BackgroundColor = BaseColor.CYAN;
                                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    PdfTable.AddCell(cell);
                                }
                            }
                            for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        cell = AddBlankRow();
                        cell.Colspan = Colspan;
                        PdfTable.AddCell(cell);
                        document.Add(PdfTable);

                    }

                    if (dsMarine.Tables[2].Rows.Count > 0)
                    {
                        PdfTable = new PdfPTable(dsMarine.Tables[2].Columns.Count);
                        PdfTable.TotalWidth = 500f;
                        PdfTable.WidthPercentage = 100f;
                        PdfTable.LockedWidth = true;
                        PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                        cell = GetFontWithStyle("DAMAGE DETAIL OF CONSIGNMENT RECEIVED TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.Colspan = 8;
                        cell.PaddingBottom = 5f;
                        PdfTable.AddCell(cell);

                        for (int rows = 0; rows < dsMarine.Tables[2].Rows.Count; rows++)
                        {
                            if (rows == 0)
                            {
                                for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                                {
                                    cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[2].Columns[column].ColumnName, BoldFont)));
                                    cell.BackgroundColor = BaseColor.CYAN;
                                    cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                    PdfTable.AddCell(cell);
                                }
                            }
                            for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[2].Rows[rows][column].ToString()), NormalFont)));
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        cell = AddBlankRow();
                        cell.Colspan = 8;
                        PdfTable.AddCell(cell);
                        document.Add(PdfTable);
                    }
                    if (dsMarine.Tables[0].Rows.Count > 0)
                    {
                        table = new PdfPTable(1);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.10f });
                        //cell = GetFontWithStyle("CAUSES OF LOSS AS DESCRIBED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        //cell.PaddingBottom = 5f;
                        //table.AddCell(cell);
                        //cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                        //cell.MinimumHeight = 40f;
                        //table.AddCell(cell);
                        //table.AddCell(AddBlankRow());
                        cell = GetFontWithStyle("DELAY IN INTIMATION TO THE INSURERS (IF ANY) AS CLARIFIED BY THE INSURED’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.PaddingBottom = 5f;
                        table.AddCell(cell);
                        cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DelayInINTimation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                        cell.MinimumHeight = 40f;
                        table.AddCell(cell);
                        table.AddCell(AddBlankRow());
                        cell = GetFontWithStyle("DELAY IN TAKING DELIVERY (IF ANY) AS DESCRIBED BY THE CONSIGNEE’S REPRESENTATIVE", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.PaddingBottom = 5f;
                        table.AddCell(cell);
                        cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DelayINTakingDelivery"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                        cell.MinimumHeight = 40f;
                        table.AddCell(cell);
                        document.Add(table);
                    }

                    //document.NewPage();
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });

                    if (dsMarine.Tables[4].Rows.Count > 0)
                    {

                        table = new PdfPTable(1);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.10f });
                        table.AddCell(AddBlankRow());

                        cell = GetFontWithStyle("SURVEYOR’S REMARKS/ ADVICE TO THE INSURED.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.PaddingBottom = 5f;
                        table.AddCell(cell);
                        for (int i = 0; i < dsMarine.Tables[4].Rows.Count; i++)
                        {
                            if (dsMarine.Tables[4].Rows[i]["RCID"].ToString() != "")
                            {
                                cell = GetFontWithStyle("     - " + dsMarine.Tables[4].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                                table.AddCell(cell);
                            }
                        }
                        document.Add(table);
                    }


                    if (dsMarine.Tables[3].Rows.Count > 0)
                    {

                        table = new PdfPTable(1);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.10f });
                        table.AddCell(AddBlankRow());

                        cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.PaddingBottom = 5f;
                        table.AddCell(cell);
                        for (int i = 0; i < dsMarine.Tables[3].Rows.Count; i++)
                        {
                            if (dsMarine.Tables[3].Rows[i]["LCID"].ToString() == "")
                            {
                                cell = GetFontWithStyle("     - " + dsMarine.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                                table.AddCell(cell);
                            }
                        }
                        table.AddCell(AddBlankRow());
                        cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        cell.PaddingBottom = 5f;
                        table.AddCell(cell);
                        for (int i = 0; i < dsMarine.Tables[3].Rows.Count; i++)
                        {
                            if (dsMarine.Tables[3].Rows[i]["LCID"].ToString() != "")
                            {
                                cell = GetFontWithStyle("     - " + dsMarine.Tables[3].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                                table.AddCell(cell);
                            }
                        }
                        table.AddCell(AddBlankRow());
                        document.Add(table);

                        // Add ISVR Details

                        if (dsMarine.Tables[5].Rows.Count > 0)
                        {
                            table = new PdfPTable(3);
                            table.TotalWidth = 500f;
                            table.WidthPercentage = 100f;
                            table.LockedWidth = true;
                            table.SetWidths(new float[] { 0.5f, 0.02f, 0.66f });
                            table.AddCell(GetFontWithStyle("INITIAL LOSS ESTIMATION BY THE INSURED(₹)", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["EstimationOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("LOSS RESERVE RECOMMENDATION TO THE INSURERS(₹)", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["RecomendedReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("BASIS OF RESERVE LOSS RECOMMENDATION", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["BasisOfReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetFontWithStyle("SURVEYOR REMARKS", BoldFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(GetSeprator(BoldFont));
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["Remarks"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                            table.AddCell(AddBlankRow());
                            document.Add(table);
                        }

                        table = new PdfPTable(1);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        //table.SetWidths(new float[] { 0.50f, 0.50f });
                        table.AddCell(AddBlankRow());
                        cell = GetFontWithStyle("For: " + dsMarine.Tables[0].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[0].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Email: " + dsMarine.Tables[0].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        document.Add(table);

                        // Starts New Page

                        if (dsMarine.Tables[6].Rows.Count > 0)
                        {
                            document.NewPage();
                            table = new PdfPTable(2);
                            table.TotalWidth = 500f;
                            table.WidthPercentage = 100f;
                            table.LockedWidth = true;
                            table.SetWidths(new float[] { 0.5f, 0.5f });
                            table.AddCell(AddBlankRow(2));
                            cell = GetFontWithStyle("Photo Sheet", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                            cell.Colspan = 2;
                            table.AddCell(cell);
                            table.AddCell(AddBlankRow(2));

                            //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[0]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[0]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                            //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));

                            //table.AddCell(AddBlankRow(2));

                            //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[2]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[3]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[2]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                            //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[3]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                            //document.NewPage();
                            for (int i = 0; i < dsMarine.Tables[6].Rows.Count; i++)
                            {
                                if (dsMarine.Tables[6].Rows[i]["ImgType"].ToString() == "Photo")
                                {
                                    table.AddCell(ImageCell(dsMarine.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                                    if (dsMarine.Tables[6].Rows[i + 1]["ImgType"].ToString() == "Photo")
                                    {
                                        table.AddCell(ImageCell(dsMarine.Tables[6].Rows[i + 1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                                    }
                                    else
                                    {
                                        table.AddCell(ImageCell("NoImage.jpg", PdfPCell.ALIGN_CENTER, 220f, 200f));
                                    }
                                    table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                                    if (dsMarine.Tables[6].Rows[i + 1]["ImgType"].ToString() == "Photo")
                                    {
                                        table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[i + 1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                                        i = i + 1;
                                    }
                                    else
                                    {
                                        table.AddCell(GetFontWithStyle("No Image", BoldFont, PdfPCell.ALIGN_CENTER));
                                    }
                                }
                                else
                                {
                                    cell = ImageCell(dsMarine.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 500f, 430);
                                    cell.Colspan = 2;
                                    table.AddCell(cell);
                                    cell = GetFontWithStyle(dsMarine.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER);
                                    cell.Colspan = 2;
                                }
                            }
                            table.AddCell(cell);
                            document.Add(table);

                        }
                    }
                    document.CloseDocument();

                }
                else
                {
                    Response.Write("No Record Found");

                    return null;

                }
            }
            return workStream;
        }

        public partial class Footer : PdfPageEventHelper
        {
            public iTextSharp.text.Font NormalFont = new iTextSharp.text.Font(iTextSharp.text.Font.FontFamily.TIMES_ROMAN, 8, iTextSharp.text.Font.NORMAL, BaseColor.BLACK);
            public string RefNo, ReportName, Signature;
            public PdfPCell GetFontWithStyle(string Text, iTextSharp.text.Font Font, int Alignment, string IsBorder = "", int Colspan = 0)
            {
                PdfPCell cell = new PdfPCell(new Phrase(Text, Font));
                cell.HorizontalAlignment = Alignment;
                if (IsBorder == "N")
                {
                    cell.BorderWidth = 0f;
                }
                if (Colspan != 0)
                {
                    cell.Colspan = Colspan;
                }
                return cell;
            }


            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                PdfPTable footerTbl = new PdfPTable(3);
                footerTbl.TotalWidth = 500f;
                footerTbl.WidthPercentage = 100f;
                footerTbl.LockedWidth = true;
                footerTbl.SetWidths(new float[] { 0.3f, 0.4f, 0.2f });
                PdfPCell cell;
                cell = GetFontWithStyle("Signature", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Format designed by:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle(ReportName, NormalFont, PdfPCell.ALIGN_LEFT, "N");
                cell.PaddingTop = 10;
                footerTbl.AddCell(cell);
                if (Signature.CheckFilePath())
                {
                    iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(Signature);
                    jpg.ScaleAbsoluteHeight(20f);
                    jpg.ScaleAbsoluteWidth(20f);
                    cell = new PdfPCell(jpg);
                    cell.Border = 0;
                }
                else
                {
                    cell.Border = 0;
                    cell = GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                }
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Visit us at: www.saisurveyors.com", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                footerTbl.AddCell(cell);
                cell = GetFontWithStyle("Ref No. " + RefNo, NormalFont, PdfPCell.ALIGN_LEFT, "N");
                footerTbl.AddCell(cell);
                footerTbl.WriteSelectedRows(0, -1, 50, 50, writer.DirectContent);
            }

        }

        public partial class Header : PdfPageEventHelper
        {
            public string path;
            public override void OnEndPage(PdfWriter writer, Document doc)
            {
                PdfPTable table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(path);
                jpg.ScaleAbsoluteHeight(80f);
                jpg.ScaleAbsoluteWidth(550f);
                PdfPCell cell = new PdfPCell(jpg);
                cell.BorderWidth = 0f;
                table.AddCell(cell);
                table.WriteSelectedRows(0, -1, 0, 680, writer.DirectContent);
            }

        }

        public FileResult GeneratePdfLOR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsLOR = BLGenrateClaim.GetMarineLOR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            workStream = LORMemoryStream(ClaimID, InsdType, InsdID);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream LORMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsLOR = BLGenrateClaim.GetMarineLOR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsLOR.Tables.Count > 0)
            {
                PdfPTable table = null;
                PdfPCell cell = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    Header objHeader = new Header();
                    objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["LetterHead"].ToString());
                    writer.PageEvent = objHeader;
                }

                Footer objFooter = new Footer();
                objFooter.RefNo = dsLOR.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("Letter of Requirment", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdAddress"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), BoldFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Ref No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. " + dsLOR.Tables[0].Rows[0]["InsrClaimNo"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Kindly Attention: " + dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Sub.: Marine claim under transit Policy No. " + dsLOR.Tables[0].Rows[0]["PolicyNo"].ToString() + " on account of damage during the course of transit from " + dsLOR.Tables[0].Rows[0]["DispatchedFROM"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This refer to our visit at your premises " + dsLOR.Tables[0].Rows[0]["PlaceofSurvey"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + ",  when we conducted survey/inspection for reported loss to the consignment vide Invoice No. " + dsLOR.Tables[1].Rows[0]["Inv. No."].ToString() + " Dated " + dsLOR.Tables[1].Rows[0]["Invoice Date"].ToString() + ", & " + dsLOR.Tables[1].Columns[6].ToString() + " " + dsLOR.Tables[1].Rows[0][6].ToString() + " Dated " + dsLOR.Tables[1].Rows[0][7].ToString() + "."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We hope that, you have immediately acted upon our advice in context to minimise the loss.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We have shared our observations during the visit ,vide JIR No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("In order to enable us to proceed further, we draw your kind attention on pending information/documents as advised in JIR.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("For your convenience, list of pending documents/information reproduced as below.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);
                if (dsLOR.Tables[2].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());


                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                            //list.SetListSymbol("\u2022");
                            //list.IndentationLeft = 50f;
                            //list.Add(new ListItem(dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    //document.Add(list);
                }
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Please note, we may ask some more information/documents, if required later, on perusal of information submitted by you.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Your prompt response will be highly appreciated and help us to expedite the proceedings of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("You may also upload the requisite documents on our website www.saisurveyors.com and link with claim ref no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                // if any difficulty in uploading documents please email request documents helpforuploadingdesk@saisurveyors.com .
                table.AddCell(GetFontWithStyle("If any difficulty in uploading documents please email request documents at helpforuploadingdesk@saisurveyors.com . Please don't forget to mention the claim refference no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + " in subject line.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Please feel free to contact the under sign for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                cell = GetFontWithStyle("For: " + dsLOR.Tables[0].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("Mobile No.: " + dsLOR.Tables[0].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("Email: " + dsLOR.Tables[0].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                document.Add(table);
                document.CloseDocument();//
            }
            return workStream;
        }

        public FileResult GenerateReminderLOR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsLOR = BLGenrateClaim.GetMarineLORReminder(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsLOR.Tables.Count > 0)
            {
                PdfPTable table = null;
                PdfPCell cell = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 20f, 20f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                Header objHeader = new Header();
                objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["LetterHead"].ToString());
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                objFooter.RefNo = dsLOR.Tables[0].Rows[0]["ClaimID"].ToString();
                objFooter.ReportName = "Instant Site Visit Report";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsLOR.Tables[0].Rows[0]["DSign"].ToString());
                writer.PageEvent = objFooter;
                document.OpenDocument();
                writer.PageEvent = new Footer();
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                string ReminderSrno = string.Empty;
                if (dsLOR.Tables[3].Rows[0]["ReminderNo"].ToString() == "1")
                {
                    ReminderSrno = "1st ";
                }
                else if (dsLOR.Tables[3].Rows[0]["ReminderNo"].ToString() == "2")
                {
                    ReminderSrno = "2nd ";
                }
                else
                {
                    ReminderSrno = "Final ";
                }
                table.AddCell(GetFontWithStyle(ReminderSrno + "REMINDER VIDE EMAIL", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This is " + ReminderSrno + "reminder for your doing needful for our earlier request letter for pending documents/information of your claim."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("We wish to draw your kind attention for pending requirements requested in our letter no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + " (attached for your ready reference)."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("We will appreciate, if you would give immediate attention to clarify the pendency of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Please feel free to contact the under singed for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);
                document.NewPage();
                // LOR Work

                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });
                table.AddCell(GetFontWithStyle("Letter of Requirment", BoldFont12, PdfPCell.ALIGN_CENTER, "N", 2));
                if (dsLOR.Tables[0].Rows.Count > 0)
                {
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsdAddress"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), BoldFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Ref No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle(dsLOR.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Kindly Attention: " + dsLOR.Tables[0].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("Sub.: Marine claim under transit Policy No. " + dsLOR.Tables[0].Rows[0]["PolicyNo"].ToString() + " on account of damage during the course of transit from " + dsLOR.Tables[0].Rows[0]["DispatchedFROM"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(AddBlankRow(2));
                    table.AddCell(GetFontWithStyle("This refer to our visit at your premises " + dsLOR.Tables[0].Rows[0]["PlaceofSurvey"].ToString() + ", on Dated " + dsLOR.Tables[0].Rows[0]["DateOfSurvey"].ToString() + ",  when we conducted survey/inspection for reported loss to the consignment vide Invoice No. " + dsLOR.Tables[1].Rows[0]["Inv. No."].ToString() + " Dated " + dsLOR.Tables[1].Rows[0]["Invoice Date"].ToString() + ", & " + dsLOR.Tables[1].Columns[6].ToString() + " " + dsLOR.Tables[1].Rows[0][6].ToString() + " Dated " + dsLOR.Tables[1].Rows[0][7].ToString() + "."
                    , NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We hope that, you have immediately acted upon our advice in context to minimise the loss.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("We have shared our observations during the visit ,vide JIR No. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("In order to enable us to proceed further, we draw your kind attention on pending information/documents as advised in JIR.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("For your convenience, list of pending documents/information reproduced as below.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                }
                document.Add(table);

                if (dsLOR.Tables[2].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());


                    cell = GetFontWithStyle("THE INSURED ARE REQUESTED TO PROVIDE THE FOLLOWING DOCUMENTS.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() == "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                        }
                    }
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("WE ACKNOWLEDGED THE RECEIPT OF FOLLOWING DOCUMENTS AT THE TIME OF OUR VISIT AT SITE.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsLOR.Tables[2].Rows.Count; i++)
                    {
                        if (dsLOR.Tables[2].Rows[i]["LCID"].ToString() != "")
                        {
                            cell = GetFontWithStyle("- " + dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                            table.AddCell(cell);
                            //list.SetListSymbol("\u2022");
                            //list.IndentationLeft = 50f;
                            //list.Add(new ListItem(dsLOR.Tables[2].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    //document.Add(list);
                }
                table = new PdfPTable(1);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.10f });
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Please note, we may ask some more information/documents, if required later, on perusal of information submitted by you.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Your prompt response will be highly appreciated and help us to expedite the proceedings of your claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("You may also upload the requisite documents on our website www.saisurveyors.com and link with claim ref no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + ".", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                // if any difficulty in uploading documents please email request documents helpforuploadingdesk@saisurveyors.com .
                table.AddCell(GetFontWithStyle("If any difficulty in uploading documents please email request documents at helpforuploadingdesk@saisurveyors.com . Please don't forget to mention the claim refference no. " + dsLOR.Tables[0].Rows[0]["ClaimID"].ToString() + " in subject line.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Please feel free to contact the under sign for required assistance regarding in this claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Yours faithfully,", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                cell = GetFontWithStyle("For: " + dsLOR.Tables[0].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("Mobile No.: " + dsLOR.Tables[0].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                table.AddCell(cell);
                cell = GetFontWithStyle("Email: " + dsLOR.Tables[0].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2);
                document.Add(table);
                document.CloseDocument();//
            }
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public FileResult GeneratePdfFSR(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineFSR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            workStream = FSRMemoryStream(ClaimID, InsdType, InsdID);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream FSRMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineFSR(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMarine.Tables.Count > 0)
            {

                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                document.OpenDocument();
                int Table1Count = dsMarine.Tables[0].Rows.Count;
                if (Table1Count > 0)
                {
                    Header objHeader = new Header();
                    objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["LetterHead"].ToString());
                    writer.PageEvent = objHeader;
                    Footer objFooter = new Footer();
                    objFooter.RefNo = dsMarine.Tables[0].Rows[0]["ClaimID"].ToString();
                    objFooter.ReportName = "Final Survey Report";
                    objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["DSign"].ToString());
                    writer.PageEvent = objFooter;
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Insurer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Address: " + dsMarine.Tables[0].Rows[0]["InsrAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Surveyors Claim No. " + dsMarine.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("Insured Claim No. " + dsMarine.Tables[0].Rows[0]["InsuredClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. " + dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    cell = GetFontWithStyle("Final Survey Report", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.36f, 0.02f, 0.8f });

                    table.AddCell(GetFontWithStyle("Previous Reports", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PreviousReports"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("All Other Communications", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Consent", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Admissibility", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Initial Loss Estimation by the Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["EstimationOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Loss Reserve Recommended in our earlier report(s)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["RecomendedReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Net Assessed/Adjusted Loss", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["BasisOfReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("1. Policy Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Policy Type", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Period of Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Period"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Coverage", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Coverage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Basis of Evaluation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["BasisValution"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Transit Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["TransitInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Mode of Conveyance", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ModeConveyance"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Subject Matter Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SubjectMatter"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy Excess", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyExcess"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("2. Survey Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Source & Date of Instruction", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("Competent Authority had deputed the undersign as Independent Surveyor & Loss Assessor on " + dsMarine.Tables[0].Rows[0]["InstructionDate"].ToString() + " for the captioned loss", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Date/Time of First Visit", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DateOfSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Place of Survey", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceofSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Delay in intimation  (If any)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DelayInINTimation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    if (dsMarine.Tables[7].Rows.Count > 0)
                    {
                        table.AddCell(GetFontWithStyle("Detail of other Survey Visits", BoldFont, PdfPCell.ALIGN_CENTER, "Y", 3));
                        PdfPTable PdfTable1 = new PdfPTable(4);
                        PdfTable1.TotalWidth = 500f;
                        PdfTable1.WidthPercentage = 100f;
                        PdfTable1.LockedWidth = true;
                        PdfTable1.SetWidths(new float[] { 0.15f, 0.15f, 0.3f, 0.4f });
                        PdfTable1.AddCell(GetFontWithStyle("Date of Visit", BoldFont, PdfPCell.ALIGN_CENTER, "", 2));
                        PdfTable1.AddCell(GetFontWithStyle("Place of Visit", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("Purpose of Visit", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("Start Journey", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("End Journey", BoldFont, PdfPCell.ALIGN_CENTER));
                        PdfTable1.AddCell(GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER, "N"));
                        PdfTable1.AddCell(GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER, "N"));
                        for (int rows = 0; rows < dsMarine.Tables[7].Rows.Count; rows++)
                        {
                            for (int column = 0; column < dsMarine.Tables[7].Columns.Count; column++)
                            {
                                cell = new PdfPCell(GetFontWithStyle(dsMarine.Tables[7].Rows[rows][column].ToString(), NormalFont, PdfPCell.ALIGN_CENTER));
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable1.AddCell(cell);
                            }
                        }
                        cell = new PdfPCell(PdfTable1);
                        cell.Colspan = 3;
                        table.AddCell(cell);
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));//Nature of Tra
                    }
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("3. Consignment Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNOR NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNEE NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CARRYING VEHICLE REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("LOAD ON CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LoadOnCarryingVehicle"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("GVW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("ULW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CAUSE OF LOSS DURING TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Other")
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLossOther"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Road Accident" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Hi-jacking" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Fire")
                    {
                        table.AddCell(GetFontWithStyle("PLACE OF OCCURENCE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceOfAccident"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT TRANSHIPPED TO ", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentTransshipedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("SPOT SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("POLICE REPORT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PoliceReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    table.AddCell(GetFontWithStyle("INSURABLE INTEREST", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["WhoIsInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("TYPE OF CONTRACT WITH CARRIER", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["TypeOfContract"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INCOTERM OF SALE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["IncotermSale"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("POINT OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PointDeliveryConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }

                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMarine.Tables[1].Columns.Count);
                if (dsMarine.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("4. Consignment Document Particulars", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The Insured consignment was dispatched vide following invoice/s and LR/s detailed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                if (dsMarine.Tables[2].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(4);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    cell = GetFontWithStyle("5. CONSIGNMENT JOURENEY PARTICULARS", BoldFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("All the events of the Insured consignment journey are summarized as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    if (dsMarine.Tables[2].Rows.Count == 1 && dsMarine.Tables[2].Rows[0]["Mode"].ToString() == "Road")
                    {
                        PdfTable.AddCell(GetFontWithStyle("Single Transhipment by Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                        PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[0]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        for (int rows = 0; rows < dsMarine.Tables[2].Rows.Count; rows++)
                        {
                            string mode = "";
                            switch (rows)
                            {
                                case 0:
                                    mode = "First leg of journey by";
                                    break;
                                case 1:
                                    mode = "Second leg of journey by";
                                    break;
                                case 2:
                                    mode = "Third leg of journey by";
                                    break;
                                case 3:
                                    mode = "Fourth leg of journey by";
                                    break;

                            }
                            if (dsMarine.Tables[2].Rows[rows]["Mode"].ToString() == "Road")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[2].Rows[rows]["Mode"].ToString() == "Air")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Air", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Airway Bill Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[2].Rows[rows]["Mode"].ToString() == "Sea")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Sea", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[2].Rows[rows]["Mode"].ToString() == "Rail")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Rail", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Destination Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[2].Rows[rows]["Mode"].ToString() == "Other")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Courier/Others", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Booking Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Delivery Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[2].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                        }
                    }
                    document.Add(PdfTable);
                }


                if (dsMarine.Tables[3].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(dsMarine.Tables[3].Columns.Count);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    int Table3Count = dsMarine.Tables[3].Columns.Count;
                    cell = GetFontWithStyle("6. NATURE & EXTENT OF DAMAGE TO CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    //cell = GetFontWithStyle("The Insured consignment was dispatched vide following invoice/s and LR/s detailed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell = GetFontWithStyle("Following Damage was noticed to the Insured Consignment tabulated as below:", NormalFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[3].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[3].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[3].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[3].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[3].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow(dsMarine.Tables[3].Columns.Count);
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                }

                if (dsMarine.Tables[0].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("7. ASSESSMENT OF LOSS", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("The Loss assessment computation is elaborated in detail on assessment sheet attached to this report. Notes on assessment are described as bullet points as follows:", NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.BorderWidth = 0f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Notes :-", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("     •	   The assessed quantities are based upon our physical verifications, before and after segregation.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("     •	   The assessed rates are considered and verified from original invoice.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("     •	   As per policy terms, basis of indemnification is agreed value, hence loss amount adjusted accordingly, as basis of valuation mentioned in the policy schedule.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("     •	   Nature of damage to the consignment was found fresh and also collating to the cause of loss elaborated by the Insured.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("     •	   Assessment of loss is subject to declaration submitted by the Insured, Insurers are requested to check the declaration of relevant transit at their end at the time, final settlement of the claim.", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    document.Add(table);
                }

                if (dsMarine.Tables[8].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    cell = GetFontWithStyle("8. Salvage", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[8].Rows[0]["Salvage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("9. Comment on Policy Conditions", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[8].Rows[0]["CommentPolicy"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("10. RECOVERY RIGHT/SUBROGATION", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[8].Rows[0]["Concurrence"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());

                    //table = new PdfPTable(1);
                    //table.TotalWidth = 500f;
                    //table.WidthPercentage = 100f;
                    //table.LockedWidth = true;
                    //table.SetWidths(new float[] { 0.10f });
                    //table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("11. Remarks for Pending Documents", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Insurers are requested to obtain following documents from the insured, at the time of settlement of the claim. ", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    if (dsMarine.Tables[10].Rows.Count > 0)
                    {
                        for (int i = 0; i < dsMarine.Tables[10].Rows.Count; i++)
                        {
                            if (dsMarine.Tables[10].Rows[i]["LCID"].ToString() == "")
                            {
                                cell = GetFontWithStyle("     - " + dsMarine.Tables[10].Rows[i]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                                table.AddCell(cell);
                            }
                        }
                    }
                    table.AddCell(AddBlankRow());

                    //List list = new List(List.UNORDERED, 20f);
                    //for (int i = 0; i < dsMarine.Tables[5].Rows.Count; i++)
                    //{
                    //    if (dsMarine.Tables[5].Rows[i]["LCID"].ToString() == "")
                    //    {
                    //        list.SetListSymbol("\u2022");
                    //        list.IndentationLeft = 50f;
                    //        list.Add(new ListItem(dsMarine.Tables[5].Rows[i]["Name"].ToString(), NormalFont));
                    //    }
                    //}
                    //document.Add(table);
                    //document.Add(list);

                    cell = GetFontWithStyle("12. Comment on Subrogation (Notice to Carrier/Damage Certificate)", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[8].Rows[0]["OtherRemark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("13. Conclusion/ Recommendations", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    cell = GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Recommendation"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT);
                    cell.MinimumHeight = 40f;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    //table.SetWidths(new float[] { 0.50f, 0.50f });
                    table.AddCell(AddBlankRow());
                    cell = GetFontWithStyle("For: " + dsMarine.Tables[0].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[0].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMarine.Tables[0].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    document.Add(table);

                }

                // Assessment Details

                PdfTable = new PdfPTable(dsMarine.Tables[4].Columns.Count);
                if (dsMarine.Tables[4].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[4].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    //cell = GetFontWithStyle("14. Invoice Value of the Loss", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    //cell.Colspan = Colspan;
                    //cell.PaddingBottom = 5f;
                    //PdfTable.AddCell(cell);
                    //cell = GetFontWithStyle("The Invoice Value Loss is tabulated as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    //cell.Colspan = Colspan;
                    //cell.PaddingBottom = 5f;
                    //PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Assessment Sheet", BoldFont, PdfPCell.ALIGN_CENTER, "", 10);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_CENTER, "", 4);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Claimed", BoldFont, PdfPCell.ALIGN_CENTER, "", 3);
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Assessed", BoldFont, PdfPCell.ALIGN_CENTER, "", 3);
                    PdfTable.AddCell(cell);


                    for (int rows = 0; rows < dsMarine.Tables[4].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[4].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[4].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[4].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[4].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.NewPage();
                    document.Add(PdfTable);

                }

                // Calculation Details

                PdfTable = new PdfPTable(2);
                if (dsMarine.Tables[9].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[9].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    PdfTable.SetWidths(new float[] { 0.80f, 0.20f });

                    //cell = GetFontWithStyle("9. Adjustment of the Loss,as per policy", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2);
                    cell = GetFontWithStyle("9. Adjustment of the Loss,as per terms & conditions of the captioned policy adjustment of the invoice value of loss is as follows:", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    cell = GetFontWithStyle("Adjustment of Loss,as per Policy Terms", BoldFont, PdfPCell.ALIGN_CENTER, "", 2);
                    cell.Colspan = 2;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[9].Rows.Count; rows++)
                    {
                        //if (rows == 0)
                        //{
                        //    for (int column = 0; column < dsMarine.Tables[4].Columns.Count; column++)
                        //    {
                        //        cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[4].Columns[column].ColumnName, BoldFont)));
                        //        cell.BackgroundColor = BaseColor.CYAN;
                        //        cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                        //        PdfTable.AddCell(cell);
                        //    }
                        //}
                        for (int column = 0; column < dsMarine.Tables[9].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[9].Rows[rows][column].ToString()), NormalFont)));
                            if ((column + 2) % 2 == 0)
                            {
                                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            }
                            else
                            {
                                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            }
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                // FSR Details




                if (dsMarine.Tables[5].Rows.Count > 0)
                {
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.10f });
                    table.AddCell(AddBlankRow());

                    cell = GetFontWithStyle("ENCLOSURES", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.PaddingBottom = 5f;
                    table.AddCell(cell);
                    List list = new List(List.UNORDERED, 20f);
                    for (int i = 0; i < dsMarine.Tables[5].Rows.Count; i++)
                    {
                        if (dsMarine.Tables[5].Rows[i]["LCID"].ToString() != "")
                        {
                            list.SetListSymbol("\u2022");
                            list.IndentationLeft = 50f;
                            list.Add(new ListItem(dsMarine.Tables[5].Rows[i]["Name"].ToString(), NormalFont));
                        }
                    }
                    document.Add(table);
                    document.Add(list);


                }


                // Add ISVR Details


                if (dsMarine.Tables[6].Rows.Count > 0)
                {
                    document.NewPage();
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(AddBlankRow(2));
                    cell = GetFontWithStyle("Photo Sheet", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow(2));

                    //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[0]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[0]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));

                    //table.AddCell(AddBlankRow(2));

                    //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[2]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    //table.AddCell(ImageCell(dsMarine.Tables[6].Rows[3]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[2]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[3]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                    ////document.NewPage();
                    for (int i = 0; i < dsMarine.Tables[6].Rows.Count; i++)
                    {
                        if (dsMarine.Tables[6].Rows[i]["ImgType"].ToString() == "Photo")
                        {
                            table.AddCell(ImageCell(dsMarine.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            if (dsMarine.Tables[6].Rows[i + 1]["ImgType"].ToString() == "Photo")
                            {
                                table.AddCell(ImageCell(dsMarine.Tables[6].Rows[i + 1]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 220f, 200f));
                            }
                            else
                            {
                                table.AddCell(ImageCell("NoImage.jpg", PdfPCell.ALIGN_CENTER, 220f, 200f));
                            }
                            table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                            if (dsMarine.Tables[6].Rows[i + 1]["ImgType"].ToString() == "Photo")
                            {
                                table.AddCell(GetFontWithStyle(dsMarine.Tables[6].Rows[i + 1]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER));
                                i = i + 1;
                            }
                            else
                            {
                                table.AddCell(GetFontWithStyle("No Image", BoldFont, PdfPCell.ALIGN_CENTER));
                            }
                        }
                        else
                        {
                            cell = ImageCell(dsMarine.Tables[6].Rows[i]["PHOTOPATH"].ToString(), PdfPCell.ALIGN_CENTER, 500f, 430);
                            cell.Colspan = 2;
                            table.AddCell(cell);
                            cell = GetFontWithStyle(dsMarine.Tables[6].Rows[i]["PHOTONAME"].ToString(), BoldFont, PdfPCell.ALIGN_CENTER);
                            cell.Colspan = 2;
                        }
                    }
                    table.AddCell(cell);
                    document.Add(table);
                }

                document.CloseDocument();
            }
            //return new FileStreamResult(workStream, "application/pdf");
            return workStream;
        }

        public FileResult GeneratePdfClaimForm(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineClaimForm(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            workStream = ClaimFormMemoryStream(ClaimID, InsdType, InsdID);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream ClaimFormMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineClaimForm(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMarine.Tables.Count > 0)
            {
                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 20f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                document.OpenDocument();
                int Table1Count = dsMarine.Tables[0].Rows.Count;
                if (Table1Count > 0)
                {
                    //Header objHeader = new Header();
                    //objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["LetterHead"].ToString());
                    //writer.PageEvent = objHeader;
                    Footer objFooter = new Footer();
                    objFooter.RefNo = dsMarine.Tables[0].Rows[0]["ClaimID"].ToString();
                    objFooter.ReportName = "Claim Form";
                    objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["DSign"].ToString());
                    writer.PageEvent = objFooter;
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Surveyors Claim No. " + dsMarine.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("Insured Claim No. " + dsMarine.Tables[0].Rows[0]["InsuredClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. " + dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    cell = GetFontWithStyle("Claim Form", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("(This is electronic claim form to be submitted by the Insured only, submission of this form does not comply the requirement of claimform prescribed by Insurers. This form had been design for help of surveyor to expedite the process of submission of Survey Report)", NormalFont, PdfPCell.ALIGN_JUSTIFIED, "");
                    cell.Colspan = 2;
                    cell.BackgroundColor = BaseColor.YELLOW;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.36f, 0.02f, 0.8f });

                    table.AddCell(GetFontWithStyle("1. Policy Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Policy Type", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Period of Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Period"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Transit Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["TransitInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Mode of Conveyance", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ModeConveyance"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Coverage", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Coverage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Subject Matter Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SubjectMatter"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Basis of Evaluation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["BasisValution"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy Excess", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyExcess"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("2. Consignment Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNOR NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNEE NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CARRYING VEHICLE REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("LOAD ON CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LoadOnCarryingVehicle"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("GVW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("ULW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CAUSE OF LOSS DURING TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Other")
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLossOther"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Road Accident" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Hi-jacking" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Fire")
                    {
                        table.AddCell(GetFontWithStyle("PLACE OF OCCURENCE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceOfAccident"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT TRANSHIPPED TO ", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentTransshipedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("SPOT SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("POLICE REPORT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PoliceReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    table.AddCell(GetFontWithStyle("INSURABLE INTEREST", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["WhoIsInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("TYPE OF CONTRACT WITH CARRIER", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["TypeOfContract"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("INCOTERM OF SALE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["IncotermSale"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("POINT OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PointDeliveryConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }

                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMarine.Tables[1].Columns.Count);
                if (dsMarine.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("3. Consignment Document Particulars", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                if (dsMarine.Tables[2].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(dsMarine.Tables[2].Columns.Count);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    int Table3Count = dsMarine.Tables[2].Columns.Count;
                    cell = GetFontWithStyle("4. Damage Details of Consignment Received", BoldFont, PdfPCell.ALIGN_LEFT, "N", Table3Count);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[2].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[2].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[2].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[2].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow(dsMarine.Tables[2].Columns.Count);
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                }

                if (dsMarine.Tables[5].Rows.Count > 0)
                {
                    PdfTable = new PdfPTable(4);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });
                    cell = GetFontWithStyle("5. CONSIGNMENT JOURENEY PARTICULARS", BoldFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("All the events of the Insured consignment journey are summarized as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N", 4);
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    if (dsMarine.Tables[5].Rows.Count == 1 && dsMarine.Tables[5].Rows[0]["Mode"].ToString() == "Road")
                    {
                        PdfTable.AddCell(GetFontWithStyle("Single Transhipment by Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                        PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                        PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[0]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        for (int rows = 0; rows < dsMarine.Tables[5].Rows.Count; rows++)
                        {
                            string mode = "";
                            switch (rows)
                            {
                                case 0:
                                    mode = "First leg of journey by";
                                    break;
                                case 1:
                                    mode = "Second leg of journey by";
                                    break;
                                case 2:
                                    mode = "Third leg of journey by";
                                    break;
                                case 3:
                                    mode = "Fourth leg of journey by";
                                    break;

                            }
                            if (dsMarine.Tables[5].Rows[rows]["Mode"].ToString() == "Road")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Road", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("GR/LR Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival in Destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[5].Rows[rows]["Mode"].ToString() == "Air")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Air", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Airway Bill Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[5].Rows[rows]["Mode"].ToString() == "Sea")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Sea", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Bill of Lading Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Port", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[5].Rows[rows]["Mode"].ToString() == "Rail")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Rail", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Loading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Unloading Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Railway Receipt Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at Destination Rly. Siding", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                            if (dsMarine.Tables[5].Rows[rows]["Mode"].ToString() == "Other")
                            {
                                PdfTable.AddCell(GetFontWithStyle(mode + " Courier/Others", BoldFont, PdfPCell.ALIGN_CENTER, "", 4));
                                PdfTable.AddCell(GetFontWithStyle("Booking Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["LoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Delivery Place", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["UnLoadingPlace"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket No.", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Docket Date", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["TypeDate"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Arrival at destination Town", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateArrival"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Date of Discharge/ Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["DateDischarge"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Name of Carrier", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["NameOfCarrier"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Comment on Discharge/Delivery", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Comment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle("Remarks", BoldFont, PdfPCell.ALIGN_LEFT));
                                PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[5].Rows[rows]["Remark"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "", 3));
                                PdfTable.AddCell(AddBlankRow(4));
                            }
                        }
                    }
                    document.Add(PdfTable);
                }


                // Assessment Details

                PdfTable = new PdfPTable(dsMarine.Tables[3].Columns.Count);
                if (dsMarine.Tables[3].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[3].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;

                    cell = GetFontWithStyle("Claim Bill", BoldFont, PdfPCell.ALIGN_CENTER, "", Colspan);
                    PdfTable.AddCell(cell);
                    for (int rows = 0; rows < dsMarine.Tables[3].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[3].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[3].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[3].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[3].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }

                // Calculation Details

                PdfTable = new PdfPTable(2);
                if (dsMarine.Tables[4].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[4].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.SetWidths(new float[] { 0.8f, 0.2f });
                    PdfTable.LockedWidth = true;

                    for (int rows = 0; rows < dsMarine.Tables[4].Rows.Count; rows++)
                    {
                        for (int column = 0; column < dsMarine.Tables[4].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[4].Rows[rows][column].ToString()), NormalFont)));
                            if ((column + 2) % 2 == 0)
                            {
                                cell.HorizontalAlignment = PdfPCell.ALIGN_LEFT;
                            }
                            else
                            {
                                cell.HorizontalAlignment = PdfPCell.ALIGN_RIGHT;
                            }
                            PdfTable.AddCell(cell);
                        }
                    }
                    //for (int rows = 0; rows < dsMarine.Tables[4].Rows.Count; rows++)
                    //{
                    //    PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[4].Rows[rows]["CalType"].ToString() + ": " + dsMarine.Tables[4].Rows[rows]["FK_CalID"].ToString(), NormalFont, PdfPCell.ALIGN_CENTER));
                    //    PdfTable.AddCell(GetFontWithStyle(dsMarine.Tables[4].Rows[rows]["Value"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //}
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                    PdfTable = new PdfPTable(2);
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.SetWidths(new float[] { 0.5f, 0.5f });
                    PdfTable.LockedWidth = true;
                    cell = GetFontWithStyle("I " + dsMarine.Tables[0].Rows[0]["InsuredName"].ToString() + " hereby declared, that the above information given by me, is absolutely true and correct as per best of my knowledge. I have not hide any fact of material in above, I will be solely responsible to produce evidence in support of above material facts in, future if required by any authorised person.", NormalFont, PdfPCell.ALIGN_JUSTIFIED, "");
                    cell.BorderWidth = 0f;
                    cell.Colspan = 2;
                    PdfTable.AddCell(cell);
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Date: __________________", BoldFont, PdfPCell.ALIGN_LEFT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Name of Claim form Submitter: __________________", BoldFont, PdfPCell.ALIGN_RIGHT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Place: __________________", BoldFont, PdfPCell.ALIGN_LEFT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Mobile: " + dsMarine.Tables[0].Rows[0]["InsuredMobile"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Signature: __________________", BoldFont, PdfPCell.ALIGN_LEFT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("Email: " + dsMarine.Tables[0].Rows[0]["InsuredEmail"].ToString(), BoldFont, PdfPCell.ALIGN_RIGHT, "", 0);
                    cell.BorderWidth = 0f;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);

                }



                document.CloseDocument();
            }
            return workStream;
        }




        public FileResult GeneratePdfNoClaim(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineNoClaim(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            workStream = NoClaimMemoryStream(ClaimID, InsdType, InsdID);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream NoClaimMemoryStream(string ClaimID, string InsdType, string InsdID)
        {
            DataSet dsMarine = BLGenrateClaim.GetMarineNoClaim(ClaimID, InsdType, InsdID);
            MemoryStream workStream = new MemoryStream();
            if (dsMarine.Tables.Count > 0)
            {

                PdfPCell cell = null;
                PdfPTable table = null;

                Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
                PdfWriter writer = PdfWriter.GetInstance(document, workStream);
                writer.CloseStream = false;
                document.OpenDocument();
                int Table1Count = dsMarine.Tables[0].Rows.Count;
                if (Table1Count > 0)
                {
                    Header objHeader = new Header();
                    objHeader.path = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["LetterHead"].ToString());
                    writer.PageEvent = objHeader;
                    Footer objFooter = new Footer();
                    objFooter.RefNo = dsMarine.Tables[0].Rows[0]["ClaimID"].ToString();
                    objFooter.ReportName = "Final Survey Report";
                    objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + dsMarine.Tables[0].Rows[0]["DSign"].ToString());
                    writer.PageEvent = objFooter;
                    table = new PdfPTable(2);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.5f, 0.5f });
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Insurer"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("Address: " + dsMarine.Tables[0].Rows[0]["InsrAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                    table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                    table.AddCell(GetFontWithStyle("Surveyors Claim No. " + dsMarine.Tables[0].Rows[0]["ClaimID"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N"));
                    table.AddCell(GetFontWithStyle("Insured Claim No. " + dsMarine.Tables[0].Rows[0]["InsuredClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. " + dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                    cell = GetFontWithStyle("Final Survey Report", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                    cell.Colspan = 2;
                    table.AddCell(cell);
                    table.AddCell(AddBlankRow());
                    table.AddCell(AddBlankRow());
                    document.Add(table);

                    table = new PdfPTable(3);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    table.SetWidths(new float[] { 0.36f, 0.02f, 0.8f });

                    table.AddCell(GetFontWithStyle("Previous Reports", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PreviousReports"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("All Other Communications", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Consent", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Admissibility", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Insurers Claim No. ", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Initial Loss Estimation by the Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["EstimationOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Loss Reserve Recommended in our earlier report(s)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["RecomendedReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Net Assessed/Adjusted Loss", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["BasisOfReserve"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("1. Policy Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("Policy Type", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyType"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy No.", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Name of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Address of Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["InsuredAddress"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Period of Policy", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Period"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Coverage", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Coverage"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Basis of Evaluation", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["BasisValution"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Transit Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["TransitInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Mode of Conveyance", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ModeConveyance"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Subject Matter Insured", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SubjectMatter"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("Policy Excess", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PolicyExcess"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(AddBlankRow(3));
                    table.AddCell(GetFontWithStyle("2. Consignment Particulars", BoldFont, PdfPCell.ALIGN_CENTER, "", 3));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT TYPE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("CONSIGNMENT", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignment"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNOR NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignor"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNEE NAME", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["Consignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF ARRIVAL AT DESTINATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ArrivalDateDestination"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DATE OF DELIVERY TO CONSIGNEE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DeliveryDateConsignee"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("NATURE OF TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["NatureOfTransit"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetFontWithStyle("REASON FOR DELAY IN INTIMATION", BoldFont, PdfPCell.ALIGN_LEFT));
                    //table.AddCell(GetSeprator(BoldFont));
                    //table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ResoneForDelay"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED FROM", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedFROM"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DISPATCHED TO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DispatchedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CARRYING VEHICLE REGN. NO", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryRegNo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CONSIGNMENT WEIGHT (IN KG'S)", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentWeight"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("LOAD ON CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["LoadOnCarryingVehicle"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("GVW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryGVW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("ULW OF CARRYING VEHICLE", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CarryingLorryULW"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("DESCRIPTION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["DescriptionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("EXTERNAL CONDITION OF PACKING", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ExConditionPacking"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetFontWithStyle("CAUSE OF LOSS DURING TRANSIT", BoldFont, PdfPCell.ALIGN_LEFT));
                    table.AddCell(GetSeprator(BoldFont));
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Other")
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLossOther"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    else
                    {
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    if (dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Road Accident" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Hi-jacking" || dsMarine.Tables[0].Rows[0]["CauseOfLoss"].ToString() == "Fire")
                    {
                        table.AddCell(GetFontWithStyle("PLACE OF OCCURENCE", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PlaceOfAccident"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("CONSIGNMENT TRANSHIPPED TO ", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["ConsignmentTransshipedTo"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("SPOT SURVEY", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["SpotSurvey"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetFontWithStyle("POLICE REPORT", BoldFont, PdfPCell.ALIGN_LEFT));
                        table.AddCell(GetSeprator(BoldFont));
                        table.AddCell(GetFontWithStyle(dsMarine.Tables[0].Rows[0]["PoliceReport"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                    }
                    cell = AddBlankRow();
                    cell.Colspan = 3;
                    table.AddCell(cell);
                    document.Add(table);
                }

                // Docs Table

                PdfPTable PdfTable = new PdfPTable(dsMarine.Tables[1].Columns.Count);
                if (dsMarine.Tables[1].Rows.Count > 0)
                {
                    int Colspan = dsMarine.Tables[1].Columns.Count;
                    PdfTable.TotalWidth = 500f;
                    PdfTable.WidthPercentage = 100f;
                    PdfTable.LockedWidth = true;
                    //PdfTable.SetWidths(new float[] { 0.06f, 0.25f, 0.08f, 0.13f, 0.10f, 0.12f, 0.13f, 0.13f });

                    cell = GetFontWithStyle("3. Consignment Document Particulars", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);
                    cell = GetFontWithStyle("The Insured consignment was dispatched vide following invoice/s and LR/s detailed as follows:", NormalFont, PdfPCell.ALIGN_LEFT, "N");
                    cell.Colspan = Colspan;
                    cell.PaddingBottom = 5f;
                    PdfTable.AddCell(cell);

                    for (int rows = 0; rows < dsMarine.Tables[1].Rows.Count; rows++)
                    {
                        if (rows == 0)
                        {
                            for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                            {
                                cell = new PdfPCell(new Phrase(new Chunk(dsMarine.Tables[1].Columns[column].ColumnName, BoldFont)));
                                cell.BackgroundColor = BaseColor.CYAN;
                                cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                                PdfTable.AddCell(cell);
                            }
                        }
                        for (int column = 0; column < dsMarine.Tables[1].Columns.Count; column++)
                        {
                            cell = new PdfPCell(new Phrase(new Chunk(Convert.ToString(dsMarine.Tables[1].Rows[rows][column].ToString()), NormalFont)));
                            cell.HorizontalAlignment = PdfPCell.ALIGN_CENTER;
                            PdfTable.AddCell(cell);
                        }
                    }
                    cell = AddBlankRow();
                    cell.Colspan = Colspan;
                    PdfTable.AddCell(cell);
                    document.Add(PdfTable);
                    document.NewPage();
                    table = new PdfPTable(1);
                    table.TotalWidth = 500f;
                    table.WidthPercentage = 100f;
                    table.LockedWidth = true;
                    cell = GetFontWithStyle("Withdrawn By Insured: ", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    cell = GetFontWithStyle("The undersign had conducted minutely inspection of the subject consignment in the presence of Insured representative. The consignment was hardly affected and the Insured was advised to segregation of affected material. Further Insured have informed us, there was minor loss to the consignment, hence they are no interested to proceed further of this claim.  The Insured had withdrawn this claim, vide their email dated ---------------.", BoldFont, PdfPCell.ALIGN_JUSTIFIED, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Accordingly we are submitting our final survey report with recommendation of closure of this file as Nil claim.", BoldFont, PdfPCell.ALIGN_JUSTIFIED, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Non Submission of Documents.", BoldFont, PdfPCell.ALIGN_JUSTIFIED, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Despite of various reminders and phone calls, the Insured have not provided relevant information for proceedings of this claim. Finally the undersigned issued final reminder to the Insured for submission of relevant information or explain the reason for delay, with expected time to completion of necessary requirements of this claim with time line of 7 days from receipt of final reminder dated…………", BoldFont, PdfPCell.ALIGN_JUSTIFIED, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    cell = GetFontWithStyle("Regret to mention here the insured have no responded our request, so its appeared that Insured are unable to complete the necessary information required to finalization of the claim or not interested to proceed further of this claim in present scenario. Hence Insurers are advised to close this file as No claim for present. In case Insured further agree to cooperate and submit necessary information within reasonable time period, relevant to finalization of the claim, Insurers may advise us to reopen the claim at their discretions.", BoldFont, PdfPCell.ALIGN_JUSTIFIED, "N");
                    table.AddCell(cell);
                    cell = AddBlankRow();
                    table.AddCell(cell);
                    document.Add(table);

                    // Add Contact Person Details

                    if (dsMarine.Tables[2].Rows.Count > 0)
                    {
                        table = new PdfPTable(2);
                        table.TotalWidth = 500f;
                        table.WidthPercentage = 100f;
                        table.LockedWidth = true;
                        table.SetWidths(new float[] { 0.50f, 0.50f });
                        cell = GetFontWithStyle("For: " + dsMarine.Tables[2].Rows[0]["InsdName"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("For: " + dsMarine.Tables[2].Rows[0]["SName"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Contact Person: " + dsMarine.Tables[2].Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("(Surveuor & Loss Assessor)", BoldFont, PdfPCell.ALIGN_RIGHT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[2].Rows[0]["Mobile"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Mobile No.: " + dsMarine.Tables[2].Rows[0]["SMobile"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Email: " + dsMarine.Tables[2].Rows[0]["Email"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N");
                        table.AddCell(cell);
                        cell = GetFontWithStyle("Email: " + dsMarine.Tables[2].Rows[0]["SEmail"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N");
                        table.AddCell(cell);
                        document.Add(table);
                    }
                }


                document.CloseDocument();
            }
            //return new FileStreamResult(workStream, "application/pdf");
            return workStream;
        }


        private MemoryStream getPageCountOnFooter(MemoryStream workStream)
        {
            byte[] finalBytes;
            MemoryStream ms = new MemoryStream();
            PdfReader reader = new PdfReader(workStream.ToArray());
            //Document document1 = new Document(new iTextSharp.text.Rectangle(550f, 680f), 22f, 0f, 0f, 0f);
            Document document1 = new Document(new iTextSharp.text.Rectangle(550f, 680f), 22f, 0f, 0f, 0f);
            PdfWriter writer1 = PdfWriter.GetInstance(document1, ms);
            writer1.CloseStream = false;
            document1.Open();
            for (var i = 1; i <= reader.NumberOfPages; i++)
            {
                document1.NewPage();
                var baseFont = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                var importedPage = writer1.GetImportedPage(reader, i);
                var contentByte = writer1.DirectContent;
                contentByte.BeginText();
                contentByte.SetFontAndSize(baseFont, 8);
                string TotalPages = reader.NumberOfPages.ToString();
                var multiLineString = "Page: " + i.ToString() + " of " + TotalPages;
                contentByte.ShowTextAligned(PdfContentByte.ALIGN_LEFT, multiLineString, document1.PageSize.GetRight(50), 10, 0);
                contentByte.EndText();
                contentByte.AddTemplate(importedPage, 0, 0);
            }
            document1.Close();
            finalBytes = ms.ToArray();
            ms.Write(finalBytes, 0, finalBytes.Length);
            ms.Position = 0;
            return ms;
        }

        public ActionResult SendEmail(MemoryStream workStream)
        {

            byte[] byteInfo = workStream.ToArray();
            workStream.Write(byteInfo, 0, byteInfo.Length);
            workStream.Position = 0;

            MailMessage mm = new MailMessage("ashish.rockstaar@gmail.com", "ashisharora1012@gmail.com");
            mm.Subject = "iTextSharp PDF";
            mm.Body = "iTextSharp PDF Attachment";
            mm.Attachments.Add(new Attachment(new MemoryStream(byteInfo), "iTextSharpPDF.pdf"));
            mm.IsBodyHtml = true;
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.EnableSsl = false;
            NetworkCredential NetworkCred = new NetworkCredential();
            NetworkCred.UserName = "------";
            NetworkCred.Password = "------";
            smtp.UseDefaultCredentials = true;
            smtp.Credentials = NetworkCred;
            smtp.Port = 587;
            smtp.Send(mm);
            return View();
        }

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~Motor Work*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        [HttpGet]
        public ActionResult MotorVehicleParticulars(string id)
        {
            CSMotorSurvey objMotorSurvey = new CSMotorSurvey();
            try
            {
                if (string.IsNullOrEmpty(id))
                    id = "0";

                objMotorSurvey.FK_ClaimID = id.ToString();
                objMotorSurvey = BLMotorSurvey.BindVehicleParticulars(objMotorSurvey.FK_ClaimID);
                CSGenrate g = new CSGenrate();
                //g.MarineJIR = new CSMarineJIR();
                g=BLGenrateClaim.GetInsuredDetails(objMotorSurvey.FK_ClaimID);
                objMotorSurvey.Insured = g.MarineJIR.Insured;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objMotorSurvey);
        }

        [HttpPost]
        public ActionResult MotorVehicleParticulars(CSMotorSurvey objMotorSurvey, HttpPostedFileBase[] files)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.SubmitVehicleParticulars(objMotorSurvey);
                //   Session["Type"] = "MotorDLParticulars";
            }
            catch (Exception ex)
            {

                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return RedirectToAction("MotorDLParticulars", new { @id = objMotorSurvey.FK_ClaimID });
            //return View(objMotorSurvey);
        }

        [HttpGet]
        public ActionResult MotorDLParticulars(string id)
        {
            CSMotorSurvey objMotorSurvey = new CSMotorSurvey();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
                DataTable dtResult = new DataTable();
                dtResult = BLGenrateClaim.GetLicenseType(id);
                StringBuilder sb = new StringBuilder();
                sb.Append("<tr>");

                for (int k = 0; k < dtResult.Rows.Count; k++)
                {
                    if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                    {
                        objMotorSurvey.LicenseType += "";
                        sb.Append("<td><input id=\"chk_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["License"] + "\" /><label><span>" + dtResult.Rows[k]["License"] + "</span></label></td>");
                    }
                    else
                    {
                        objMotorSurvey.LicenseType += dtResult.Rows[k]["Id"].ToString() + ",";
                        sb.Append("<td><input checked='true' id=\"chk_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chJIRGroup' class=\"validate[required,chJIRGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["License"] + "\" /><label><span>" + dtResult.Rows[k]["License"] + "</span></label></td>");
                    }
                    //if (k == 2)
                    //{
                    //    sb.Append("</tr><tr>");
                    //}
                }

                sb.Append("</tr>");
                objMotorSurvey = BLMotorSurvey.BindDLParticulars(objMotorSurvey.FK_ClaimID);
                objMotorSurvey.FK_ClaimID = id;
                objMotorSurvey.Remark = Convert.ToString(sb);
                //Session["Type"] = "MotorDLParticulars";
                // Session.Remove("Type");
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objMotorSurvey);
        }
        [HttpPost]
        public ActionResult MotorDLParticulars(CSMotorSurvey objMotorSurvey, HttpPostedFileBase[] files)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.SubmitDLParticulars(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("MotorOccurence", new { @id = objMotorSurvey.FK_ClaimID });
        }
        [HttpGet]
        public ActionResult MotorOccurence(string id)
        {
            CSMotorSurvey objMotorSurvey = new CSMotorSurvey();
            if (String.IsNullOrEmpty(id))
            {
                return RedirectToAction("manageclaims");
            }
            objMotorSurvey.FK_ClaimID = id;
            DataTable dtResult = new DataTable();
            dtResult = BLGenrateClaim.GetLossType(id);
            StringBuilder sb = new StringBuilder();
            sb.Append("<tr>");

            for (int k = 0; k < dtResult.Rows.Count; k++)
            {
                if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                {
                    objMotorSurvey.LicenseType += "";
                    sb.Append("<td><input id=\"Loss_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chLossGroup' class=\"validate[required,chLossGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["Loss"] + "\" /><label><span>" + dtResult.Rows[k]["Loss"] + "</span></label></td>");
                }
                else
                {
                    objMotorSurvey.LicenseType += dtResult.Rows[k]["Id"].ToString() + ",";
                    sb.Append("<td><input checked='true' id=\"Loss_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chLossGroup' class=\"validate[required,chLossGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["Loss"] + "\" /><label><span>" + dtResult.Rows[k]["Loss"] + "</span></label></td>");
                }
                //if (k == 2)
                //{
                //    sb.Append("</tr><tr>");
                //}
            }

            sb.Append("</tr>");


            dtResult = BLGenrateClaim.GetDamageType(id);
            StringBuilder sb1 = new StringBuilder();
            sb1.Append("<tr>");

            for (int k = 0; k < dtResult.Rows.Count; k++)
            {
                if (dtResult.Rows[k]["Pk_Id"].ToString() == "0")
                {
                    objMotorSurvey.DamageSides += "";
                    sb1.Append("<td><input id=\"dmg_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chDmgGroup' class=\"validate[required,chDmgGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["Damage"] + "\" /><label><span>" + dtResult.Rows[k]["Damage"] + "</span></label></td>");
                }
                else
                {
                    objMotorSurvey.DamageSides += dtResult.Rows[k]["Id"].ToString() + ",";
                    sb1.Append("<td><input checked='true' id=\"dmg_" + dtResult.Rows[k]["Id"].ToString() + "\" name='chDmgGroup' class=\"validate[required,chDmgGroup,'Please select']\" type=\"checkbox\" value=\"" + dtResult.Rows[k]["Damage"] + "\" /><label><span>" + dtResult.Rows[k]["Damage"] + "</span></label></td>");
                }
                //if (k == 2)
                //{
                //    sb.Append("</tr><tr>");
                //}
            }

            sb1.Append("</tr>");

            objMotorSurvey = BLMotorSurvey.BindOccurence(objMotorSurvey.FK_ClaimID);
            objMotorSurvey.Remark = sb.ToString();
            objMotorSurvey.RemarksISVR = sb1.ToString();
            objMotorSurvey.FK_ClaimID = id;
            sb = null;
            sb1 = null;
            return View(objMotorSurvey);
        }
        [HttpPost]
        public ActionResult MotorOccurence(CSMotorSurvey objMotorSurvey, HttpPostedFileBase[] files)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.SubmitOccurence(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("OtherLegalAspects", new { @cId = objMotorSurvey.FK_ClaimID });
        }



        public ActionResult LorMotorWork(string fkClaimId)
        {
            CSMotorSurvey objProperty = new CSMotorSurvey();
            DataTable dtLOR = BLGenrateClaim.GetMotorLOR(fkClaimId);
            try
            {
                if (dtLOR.Rows.Count > 0)
                {
                    objProperty.AccDate = dtLOR.Rows[0]["AccDate"].ToString();
                    objProperty.AccPlace = dtLOR.Rows[0]["AccPlace"].ToString();
                    objProperty.DLNo = dtLOR.Rows[0]["PolicyNo"].ToString();
                    objProperty.ChasisNo = dtLOR.Rows[0]["ClaimID"].ToString();
                    objProperty.SpotSurvey = dtLOR.Rows[0]["PlaceVisit"].ToString();
                    objProperty.RCNO = dtLOR.Rows[0]["RCNO"].ToString();
                    objProperty.IssueDate = dtLOR.Rows[0]["StartDate"].ToString();
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objProperty);
        }

        [HttpGet]
        public ActionResult EstimateLoss(string id)
        {
            CSMotorSurvey objMotorSurvey = new CSMotorSurvey();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objMotorSurvey);
        }

        [HttpPost]
        public ActionResult EstimateLoss(CSMotorSurvey objMotorSurvey)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                objMotorSurvey.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                string a = objBLMotorSurvey.MotorSubmitEstimateLoss(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return RedirectToAction("RemarksObservations", new { cId = objMotorSurvey.FK_ClaimID });
        }


        //public void SendMaillCC(string fkClaimId, string emailId, string task)
        //{
        //    //return View();
        //}
        public JsonResult SendMaillCC(List<string> emailIDs)
        {
            string msg = null;
            StringBuilder mailMsg = new StringBuilder();
            mailMsg.Append("Dear sir,Please find let of Request and provide the document as soon as possible.In case, Request any further assistance please fill free to contact the under signed.");
            try
            {
                if (emailIDs != null)
                {
                    System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();

                    foreach (string e in emailIDs)
                    {
                        mail.CC.Add(e);
                    }
                    mail.From = new MailAddress("oknfeesoftware@gmail.com");
                    //mail.Subject = subject;

                    //string Body = mailMsg;
                    mail.Body = Convert.ToString(mailMsg);
                    mail.IsBodyHtml = true;
                    SmtpClient smtp = new SmtpClient();
                    smtp.Host = "smtp.gmail.com";
                    smtp.Port = 587;
                    smtp.UseDefaultCredentials = false;
                    smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                    smtp.EnableSsl = true;
                    smtp.Send(mail);
                    msg = "s";
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }


        [HttpGet]
        public ActionResult MotorRemarksClaim(string id)
        {
            CSMotorSurvey objProperty = new CSMotorSurvey();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                DataTable dt = BLGenrateClaim.MotorGetSurveyorRemarks(Int32.Parse(id));
                if (dt.Rows.Count > 0)
                {
                    objProperty.EstimatedLoss = dt.Rows[0]["EstimatedLoss"].ToString();
                    objProperty.RecommendedLoss = dt.Rows[0]["RecommendedLoss"].ToString();
                    objProperty.RemarksISVR = dt.Rows[0]["RemarksISVR"].ToString();
                }
            }
            catch (Exception ex)
            {

            }

            return View(objProperty);
        }

        [HttpPost]
        public ActionResult MotorRemarksClaim(CSMotorSurvey objMotorSurvey)
        {
            //objMotorSurvey.FK_ClaimID = id;
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            //string a = objBLMotorSurvey.SubmitDLParticulars(objMotorSurvey);
            return RedirectToAction("MotorRemarksClaim");
        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/

        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=   PolicyParticulars  =~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        // Added by dhiraj
        [HttpGet]
        public ActionResult PolicyParticulars(string id)
        {
            CSPolicyParticulars objMotorSurvey = new CSPolicyParticulars();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
                objMotorSurvey = BLMotorSurvey.BindPolicyParticulars(objMotorSurvey.FK_ClaimID);
                objMotorSurvey.FK_ClaimID = id;
                //Session["Type"] = "MotorDLParticulars";
                // Session.Remove("Type");
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objMotorSurvey);
        }

        [HttpPost]
        public ActionResult PolicyParticulars(CSPolicyParticulars objMotorSurvey)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.MotorSubmitPolicyParticulars(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("ParticularOfSurvey", new { @cId = objMotorSurvey.FK_ClaimID });

        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=   Motor Route Particulars  =~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        [HttpGet]
        public ActionResult MotorRouteParticulars(string id)
        {
            CSMotorRouteParticulars objMotorSurvey = new CSMotorRouteParticulars();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
                objMotorSurvey = BLMotorSurvey.BindMotorRouteParticulars(objMotorSurvey.FK_ClaimID);
                objMotorSurvey.FK_ClaimID = id;
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objMotorSurvey);
        }
        [HttpPost]
        public ActionResult MotorRouteParticulars(CSMotorRouteParticulars objMotorSurvey)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.MotorSubmitRouteParticulars(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("MotorPartsClaim", new { @id = objMotorSurvey.FK_ClaimID });

        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~ Motor part Claim   *=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        [HttpGet]
        public ActionResult MotorPartsClaim(string id)
        {
            CSMotorPartClaim objMotorSurvey = new CSMotorPartClaim();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
                ViewBag.PartsClaim = BLMotorSurvey.BindMotorPartsClaims(Convert.ToInt32(id));
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View(objMotorSurvey);
        }
        [HttpPost]
        public ActionResult MotorPartsClaim(CSMotorPartClaim objMotorSurvey)
        {
            BLMotorSurvey objBLMotorSurvey = new BLMotorSurvey();
            try
            {
                string a = objBLMotorSurvey.MotorSubmitMotorPartClaim(objMotorSurvey);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return RedirectToAction("MotorPartsClaim", new { @id = objMotorSurvey.FK_ClaimID });

        }
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~ Motor particular remark *=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        [HttpGet]
        public ActionResult MotorParticularRemark(string id)
        {
            CSMotorParticularRemarks objMotorSurvey = new CSMotorParticularRemarks();
            try
            {
                if (String.IsNullOrEmpty(id))
                {
                    return RedirectToAction("manageclaims");
                }
                objMotorSurvey.FK_ClaimID = id;
                DataSet dt = BLMotorSurvey.BindMotorParticularRemarks(Convert.ToInt32(id));
                StringBuilder sb = new StringBuilder();
                int dcount = 0, rcount = 0, pcount = 0, vcount = 0, ocount = 0;
                int dRowcount = 0, rRowcount = 0, pRowcount = 0, vRowcount = 0, oRowcount = 0;
                int dTcount = 0, rTcount = 0, pTcount = 0, vTcount = 0, oTcount = 0;
                DataRow[] vrows;
                vrows = dt.Tables[0].Select("ParticularType= 'V' ");
                vTcount = vrows.Length;
                DataRow[] drows;
                drows = dt.Tables[0].Select("ParticularType= 'D' ");
                dTcount = drows.Length;
                DataRow[] orows;
                orows = dt.Tables[0].Select("ParticularType= 'O' ");
                oTcount = orows.Length; 
                DataRow[] prows;
                prows = dt.Tables[0].Select("ParticularType= 'P' ");
                pTcount = prows.Length;
                DataRow[] rrows;
                rrows = dt.Tables[0].Select("ParticularType= 'R' ");
                rTcount = rrows.Length;
               


                for (int k = 0; k < dt.Tables[0].Rows.Count; k++)
                {
                    if (dt.Tables[0].Rows[k]["ParticularType"].ToString() == "V")
                    {
                        vRowcount = vRowcount + 1;
                        if (vcount == 0)
                        {
                            vcount = vcount + 1;
                            sb.Append("<tr class='checklisttr'><td colspan='2'><b>Surveyor remarks on vehicle particulars :-</b></td></tr>");
                        }

                        if (dt.Tables[0].Rows[k]["Value"].ToString() == "Y")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Yes&nbsp<input value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Value"].ToString() == "N")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>No</td></tr>");
                        }

                        else if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Projections:") && dt.Tables[0].Rows[k]["Value"].ToString() == "W")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='W' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Well Maintained&nbsp<input value='R' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Roadworthy</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Projections:") && dt.Tables[0].Rows[k]["Value"].ToString() == "R")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='W' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Well Maintained&nbsp<input value='R' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Roadworthy</td></tr>");
                        }

                        else
                        {
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Projections:"))
                            {
                                sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='W' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Well Maintained&nbsp<input value='R' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Roadworthy</td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                            }

                        }
                        if (vTcount == vRowcount)
                        {
                            if (dt.Tables[1].Rows.Count > 0)
                            {
                                sb.Append("<tr><td colspan='2'><input type='text' style='width: 50%;' value='" + dt.Tables[1].Rows[0]["VParticular"] + "' class='txt' id='txtVOther'  </td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr><td colspan='2'><input style='width: 50%;' placeholder='Other Particular' type='text' value='' class='txt' id='txtVOther'  </td></tr>");
                            }

                        }
                    }
                    else if (dt.Tables[0].Rows[k]["ParticularType"].ToString() == "D")
                    {
                        dRowcount = dRowcount + 1;
                        string className = string.Empty;
                        if (dcount == 0)
                        {
                            dcount = dcount + 1;
                            sb.Append("<tr><td colspan='2'><b>Surveyor remarks on DL particulars :-</b></td></tr>");
                        }
                        if (dt.Tables[0].Rows[k]["Value"].ToString() == "Y")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value= " + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("3"))
                            {
                                sb.Append("<input type='text' id='txtTimes' maxlength='2' value='" + dt.Tables[1].Rows[0]["Times"] + "' />");
                                className = "dlparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className+ "\" id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Yes&nbsp<input class=\"" + className + "\" id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                            className = string.Empty;
                        }
                        else if (dt.Tables[0].Rows[k]["Value"].ToString() == "N")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("3"))
                            {
                                sb.Append("<input type='text' id='txtTimes' maxlength='2' value='" + dt.Tables[1].Rows[0]["Times"] + "'  />");
                                className = "dlparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className + "\" id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Yes&nbsp<input class=\"" + className + "\" id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>No</td></tr>");
                            className = string.Empty;
                        }
                        else
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("3"))
                            {
                                sb.Append("<input type='text' id='txtTimes' maxlength='2' value='" + dt.Tables[1].Rows[0]["Times"] + "' />");
                                className = "dlparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className + "\" id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input class=\"" + className + "\" id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                            className = string.Empty;

                        }
                        if (dTcount == dRowcount)
                        {
                            if (dt.Tables[1].Rows.Count > 0)
                            {
                                sb.Append("<tr><td><span>Driver Mobile Number</span> </td><td colspan='2'><input type='text' style='width: 50%;' value='" + dt.Tables[1].Rows[0]["DParticular"] + "' class='txt'  id='txtDOther'maxlength='10'/>  </td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr><td><span>Driver Mobile Number</span> </td> <td colspan='2'><input style='width: 50%;' type='text' value='' class='txt' id='txtDOther' maxlength='10'/>  </td></tr>");
                            }

                        }

                    }
                    else if (dt.Tables[0].Rows[k]["ParticularType"].ToString() == "O")
                    {
                        oRowcount = oRowcount + 1;
                        if (ocount == 0)
                        {
                            ocount = ocount + 1;
                            sb.Append("<tr class='checklisttr'><td colspan='2'><b>Surveyor remarks on O particulars :- </b></td></tr>");
                        }

                        if (dt.Tables[0].Rows[k]["Value"].ToString() == "Y")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Yes&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Value"].ToString() == "N")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input  id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>No</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Mode of Communication") && dt.Tables[0].Rows[k]["Value"].ToString() == "E")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='E' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Email&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='M' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Mobile&nbsp<input id=\"rbtnY" + k.ToString() + "\" value='P' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Personal Meeting</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Mode of Communication") && dt.Tables[0].Rows[k]["Value"].ToString() == "M")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='E' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Email&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='M' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Mobile&nbsp<input id=\"rbtnY" + k.ToString() + "\" value='P' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Personal Meeting</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Mode of Communication") && dt.Tables[0].Rows[k]["Value"].ToString() == "P")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='E' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Email&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='M' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Mobile&nbsp<input id=\"rbtnY" + k.ToString() + "\" value='P' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Personal Meeting</td></tr>");
                        }
                        else
                        {
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Mode of Communication"))
                            {
                                sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='E' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Email&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='M' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />Mobile&nbsp<input id=\"rbtnY" + k.ToString() + "\" value='P' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Personal Meeting</td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input id=\"rbtnY" + k.ToString() + "\" value='Y' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                                }
                            }
                        if (oTcount == oRowcount)
                        {
                            if (dt.Tables[1].Rows.Count > 0)
                            {
                                sb.Append("<tr><td colspan='2'><input type='text' style='width: 50%;' value='" + dt.Tables[1].Rows[0]["OParticular"] + "' class='txt' id='txtOOther'  </td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr><td colspan='2'><input style='width: 50%;' placeholder='Other Particular' type='text' value='' class='txt' id='txtOOther'  </td></tr>");
                            }

                        }
                    }
                    else if (dt.Tables[0].Rows[k]["ParticularType"].ToString() == "P")
                    {
                        pRowcount = pRowcount + 1;
                        if (pcount == 0)
                        {
                            pcount = pcount + 1;
                            sb.Append("<tr><td colspan='2'><b>Surveyor remarks on Policy particulars :-</b></td></tr>");
                        }

                        if (dt.Tables[0].Rows[k]["Value"].ToString() == "Y")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Yes&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                        }
                        else if (dt.Tables[0].Rows[k]["Value"].ToString() == "N")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input  id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>No</td></tr>");
                        }
                        else
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label></td><td><input value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input id=\"rbtnN" + k.ToString() + "\" value='N' name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");

                        }
                        if (pTcount == pRowcount)
                        {
                            if (dt.Tables[1].Rows.Count > 0)
                            {
                                sb.Append("<tr><td colspan='2'><input type='text' style='width: 50%;' value='" + dt.Tables[1].Rows[0]["PParticular"] + "' class='txt' id='txtPOther'  </td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr><td colspan='2'><input style='width: 50%;' placeholder='Other Particular' type='text' value='' class='txt' id='txtPOther'  </td></tr>");
                            }

                        }

                    }
                    else if (dt.Tables[0].Rows[k]["ParticularType"].ToString() == "R")
                    {
                        rRowcount = rRowcount + 1;
                       string className= string.Empty;
                        if (rcount == 0)
                        {
                            rcount = rcount + 1;
                            sb.Append("<tr><td colspan='2'><b>Surveyor remarks on Route particulars :-</b></td></tr>");
                        }

                        if (dt.Tables[0].Rows[k]["Value"].ToString() == "Y")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Was Insured Vehicle over loaded"))
                            {
                                sb.Append("<input type='text' id='txtRatio' maxlength='4' class='number'  value='" + dt.Tables[1].Rows[0]["Ratio"] + "'/>");
                                className = "routeparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className + "\" value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>Yes&nbsp<input  class=\"" + className + "\" value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                            className = string.Empty;
                        }
                        else if (dt.Tables[0].Rows[k]["Value"].ToString() == "N")
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden' value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Was Insured Vehicle over loaded"))
                            {
                                sb.Append("<input type='text' id='txtRatio' maxlength='4' class='number' value='" + dt.Tables[1].Rows[0]["Ratio"] + "'/>");
                                className = "routeparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className + "\"  value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input   class=\"" + className + "\" value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" checked/>No</td></tr>");
                            className = string.Empty;
                        }
                        else
                        {
                            sb.Append("<tr class='checklisttr'><td><label>" + dt.Tables[0].Rows[k]["Remark"] + "<input type='hidden'  value=" + dt.Tables[0].Rows[k]["FK_RemID"] + " id=\"hid_remid_" + k.ToString() + "\" /></label>");
                            if (dt.Tables[0].Rows[k]["Remark"].ToString().Contains("Was Insured Vehicle over loaded"))
                            {
                                sb.Append("<input type='text' class='number' maxlength='4' id='txtRatio' value='" + dt.Tables[1].Rows[0]["Ratio"] + "'/>");
                                className = "routeparticularclass";
                            }
                            sb.Append("</td><td><input class=\"" + className + "\" value='Y' id=\"rbtnY" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\"/>Yes&nbsp<input  class=\"" + className + "\" value='N' id=\"rbtnN" + k.ToString() + "\" name=\"rbtnValue_" + k.ToString() + "\" type=\"Radio\" />No</td></tr>");
                            className = string.Empty;
                        }
                        if (rTcount == rRowcount)
                        {
                            if (dt.Tables[1].Rows.Count > 0)
                            {
                                sb.Append("<tr><td colspan='2'><input type='text' style='width: 50%;' value='" + dt.Tables[1].Rows[0]["RParticular"] + "' class='txt' id='txtROther'  </td></tr>");
                            }
                            else
                            {
                                sb.Append("<tr><td colspan='2'><input style='width: 50%;' placeholder='Other Particular' type='text' value='' class='txt' id='txtROther'  </td></tr>");
                            }

                        }
                    }

                    }
               
                //sb.Append("</tr>");
                objMotorSurvey.HtmlTable = Convert.ToString(sb);
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }

            return View(objMotorSurvey);
        }
        // Ended by dhiraj


        #region Marine Assessment Detail
        public ActionResult MarineAssessmentDetailNew(string cid, string calMode, string task)
        {
            if (string.IsNullOrEmpty(cid) && string.IsNullOrEmpty(calMode) && (string.IsNullOrEmpty(task)))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Particulars of survey
        public ActionResult ParticularOfSurvey(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }

        #endregion
        #region Particulars Of Damage
        public ActionResult ParticularsOfDamage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Remarks/Observations
        public ActionResult RemarksObservations(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region MotorSalvage
        public ActionResult MotorSalvage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Re -Inspection
        public ActionResult ReInspection(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Other Legal Aspects
        public ActionResult OtherLegalAspects(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Prevoios Claim
        public ActionResult PreviousClaimHistory(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Motor Assessment
        public ActionResult MotorAssessment(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult MotorAssessmentSummery(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Lor New
        public ActionResult LorNew(string cid, string task)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region Surveyor Profile
        public ActionResult SurveyorProfile()
        {
            return View();
        }
        #endregion
        #region Email Log
        public JsonResult sendEmail_saveEmailLog(CSEmailLog e)
        {
            string msg = null;
            int status = 0;
            string fileName = "Testing.pdf";
            string shortName = string.Empty;
            DataTable dt = new DataTable();
            CommonService cs = new CommonService();
            try
            {
                System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
                e.ToEmailIds = e.ToEmailIds.Replace("[", "");
                e.ToEmailIds = e.ToEmailIds.Replace("]", "");
                e.CCEmailIds = e.CCEmailIds.Replace("[", "");
                e.CCEmailIds = e.CCEmailIds.Replace("]", "");
                foreach (var emailId in e.ToEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.ToEmailIds = e.ToEmailIds.Replace('"', ' ');
                    }

                }
                foreach (var emailId in e.CCEmailIds.Split(','))
                {
                    if (emailId.Contains('"'))
                    {
                        e.CCEmailIds = e.CCEmailIds.Replace('"', ' ');
                    }

                }

                foreach (var emailId in e.ToEmailIds.Split(','))
                {
                    mail.To.Add(emailId.ToString());

                }
                foreach (var emailId in e.CCEmailIds.Split(','))
                {
                    mail.CC.Add(emailId.ToString());
                }
                MemoryStream memStream = new MemoryStream();
                if (e.Task.ToUpper() == "JIR")
                {
                    shortName = "ISVR";
                    cs.executeNonQueryWithQuery("update tblMarineLorHeaderNew set FrequencyOfReminder=" + e.FrequencyOfReminder + " where ClaimID=" + e.ClaimID + " and UserID=" + e.UserID + "");
                    fileName = "JIR_PDF.pdf";
                    memStream = JIRMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }

                else if (e.Task.ToUpper() == "ISVR" || e.Task.ToUpper() == "SPOT")
                {
                    shortName = "Sub";
                    fileName = "ISVR_PDF.pdf";
                    memStream = ISVRMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }
                else if (e.Task.ToUpper() == "FSR")
                {
                    fileName = "FSR_PDF.pdf";
                    memStream = FSRMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }
                else if (e.Task.ToUpper() == "CLAIM")
                {
                    fileName = "ClaimForm_PDF.pdf";
                    memStream = ClaimFormMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }
                else if (e.Task.ToUpper() == "FSR")
                {
                    shortName = "FSRI";
                    fileName = "FSR_PDF.pdf";
                    memStream = FSRMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }
                else if (e.Task.ToUpper() == "NOCLAIM")
                {
                    fileName = "NoClaim_PDF.pdf";
                    memStream = NoClaimMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                }
                //MemoryStream memStream = ISVRMemoryStream(Convert.ToString(e.ClaimID), e.InsdType, e.InsdID);
                memStream.Position = 0;
                var contentType = new System.Net.Mime.ContentType(System.Net.Mime.MediaTypeNames.Application.Pdf);
                var reportAttachment = new Attachment(memStream, contentType);
                reportAttachment.ContentDisposition.FileName = fileName;
                mail.Attachments.Add(reportAttachment);
                mail.From = new MailAddress("oknfeesoftware@gmail.com");
                e.Subject = "Testing";
                e.MailMsg = "This Is Testing";
                mail.Subject = e.Subject;
                string Body = e.MailMsg;
                mail.Body = Body;
                mail.IsBodyHtml = true;
                SmtpClient smtp = new SmtpClient();
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.UseDefaultCredentials = false;
                smtp.Credentials = new System.Net.NetworkCredential("oknfeesoftware@gmail.com", "9253363902"); // Enter seders User name and password  
                smtp.EnableSsl = true;
                smtp.Send(mail);
                e.UserID = MySession.insuranceUserId;
                status = new BLEmailLog().saveEmailLog(e);
                cs.saveTaskManager(e.ClaimID, MySession.insuranceUserId, shortName);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region PDF LOR Reminder

        public FileResult generatePdfLorReminder(int claimId)
        {
            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);
            MemoryStream workStream = new MemoryStream();
            workStream = LorReminderMemoryStream(claimId);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream LorReminderMemoryStream(int claimId)
        {
            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);
            MemoryStream workStream = new MemoryStream();


            PdfPCell cell = null;
            PdfPTable table = null;

            Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;
            document.OpenDocument();
            int Table1Count = dt.Rows.Count;
            if (dt.Rows.Count > 0)
            {
                Header objHeader = new Header();
                //objHeader.path = Server.MapPath("~/SurveryorDocument/" + "LetterHead");
                objHeader.path = Server.MapPath("~/SurveryorDocument/Header.jpg");

                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                //objFooter.RefNo = (1).ToString(); //"ClaimID";
                //objFooter.ReportName = "Reminder 1";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + "DSign");
                // objFooter.Signature = Server.MapPath("~/images/footer.jpg");
                writer.PageEvent = objFooter;
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });

                cell = GetFontWithStyle("Letter of Requirement", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("(Reminder 1)", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(GetFontWithStyle("Insured: " + dt.Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Address: " + dt.Rows[0]["Address"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                cell = GetFontWithStyle("Without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Our Ref No.         " + dt.Rows[0]["RefNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Insurers Ref. No.   " + dt.Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Insured Ref No. ______", NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                cell = GetFontWithStyle("Kindly Attention:", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Sub: Required documents for your Marine Claim on account loss of consignment, transit through " + dt.Rows[0]["CarryingLorryRegNo"].ToString() + ", on " + Convert.ToDateTime(dt.Rows[0]["GRDate"]).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " from " + dt.Rows[0]["DispatchedFrom"].ToString() + " to " + dt.Rows[0]["DispatchedTo"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("This refers to our letter of requirement (included with JIR) dated " + Convert.ToDateTime(dt.Rows[0]["CrtDT"]).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " for above said pending claim. We wish to draw your kind attention on the requested documents/information pertaining to the claim. Enabling us to proceed further of your claim kindly provide/upload following documents at earliest.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                document.Add(table);


                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.05f, 0.9f });
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of GR/LR/RR with remarks of carrier/s representative " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of Bill of Lading, with remarks. (In case of Import) " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of Bill of Entry. (In case of Import) " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Claim bill, with basis of salvage, and an estimate in case of repairs." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of notice to carrier/s for monetary claim (along with acknowledgement or receipt of registered Post with AD)." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Damage/Short/Non Delivery Certificate issued by the carrier/s." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Letter of Subrogation (Declaration cum Undertaking) on appropriate stamp paper." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());



                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("We appreciate, if all the required documents, will be provided/uploaded on our web portal ( link already sent on your registered email & mobile no.  Incase required further assistance for uploading documents on our web portal you may email, sms or call us to our central team on below mentioned contact details.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Email:- required.assistance@relyonom.com", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("SMS: 744744", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Call: 7404744744", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Kindly, contact the undersigned if any assistance is required for above mentioned case. ", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Thanking You,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("For: Surveyor", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("(Surveuor& Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Mobile No.: 9868704744", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Email: claims.skm@gmail.com", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Cc: to the insurers", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);

            }

            document.CloseDocument();

            return workStream;
        }

        public FileResult generatePdfLorFinalReminder(int claimId)
        {
            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);
            MemoryStream workStream = new MemoryStream();
            workStream = LorFinalReminderMemoryStream(claimId);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }

        public MemoryStream LorFinalReminderMemoryStream(int claimId)
        {
            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);
            MemoryStream workStream = new MemoryStream();


            PdfPCell cell = null;
            PdfPTable table = null;

            Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;
            document.OpenDocument();
            //int Table1Count = 1;
            if (dt.Rows.Count > 0)
            {
                Header objHeader = new Header();
                objHeader.path = HostingEnvironment.MapPath("~/SurveryorDocument/Header.jpg");
                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                //objFooter.RefNo = (1).ToString(); //"ClaimID";
                //objFooter.ReportName = "Reminder 1";
                objFooter.Signature = HostingEnvironment.MapPath("~/SurveryorDocument/" + "DSign");
                // objFooter.Signature = Server.MapPath("~/images/footer.jpg");
                writer.PageEvent = objFooter;
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });

                cell = GetFontWithStyle("Letter of Requirement", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("(Final Reminder)", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(GetFontWithStyle("Insured: " + dt.Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Address: " + dt.Rows[0]["Address"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                cell = GetFontWithStyle("Without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Our Ref No.         " + dt.Rows[0]["RefNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Insurers Ref. No.   " + dt.Rows[0]["InsrClaimNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Insured Ref No. ______", NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                cell = GetFontWithStyle("Kindly Attention:", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Dear Sir/Madam,", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Sub: Required documents for your Marine Claim on account loss of consignment, transit through " + dt.Rows[0]["CarryingLorryRegNo"].ToString() + ", on " + Convert.ToDateTime(dt.Rows[0]["GRDate"]).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " from " + dt.Rows[0]["DispatchedFrom"].ToString() + " to " + dt.Rows[0]["DispatchedTo"].ToString() + ".", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("This refers to our letter of requirement dated " + Convert.ToDateTime(dt.Rows[0]["CrtDT"]).ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture) + " followed by three reminders for above said pending claim. We wish to draw your kind attention on the requested documents/information pertaining to the claim. Enabling us to proceed further of your claim kindly provide/upload following documents at earliest. ", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                document.Add(table);


                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.05f, 0.9f });
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of GR/LR/RR with remarks of carrier/s representative " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of Bill of Lading, with remarks. (In case of Import) " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of Bill of Entry. (In case of Import) " + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Claim bill, with basis of salvage, and an estimate in case of repairs." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Copy of notice to carrier/s for monetary claim (along with acknowledgement or receipt of registered Post with AD)." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Damage/Short/Non Delivery Certificate issued by the carrier/s." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("", NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("- Letter of Subrogation (Declaration cum Undertaking) on appropriate stamp paper." + DateTime.Now.ToShortDateString(), NormalFont, PdfPCell.ALIGN_LEFT, "N"));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());



                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("We appreciate, if all the required documents, will be provided/uploaded on our web portal ( link already sent on your registered email & mobile no.  Incase required further assistance for uploading documents on our web portal you may email, sms or call us to our central team on below mentioned contact details.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Email:- required.assistance@relyonom.com", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("SMS: 744744", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Call: 7404744744", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Kindly note, this is notice to providing/uploading requested document within given timeline of 3 days from receipt of this letter. In case we will not hear from you within given time line we will issue our independent report based upon our observations and available documents with us. It may also be with recommendation for closure of the case-file without any payment for settlement of your claim at on this stage.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                cell = GetFontWithStyle("Kindly also make a note, after given timeline final report of your claim will be issued without any further notice and reopening request will be subject to discretions of the Insurers based upon merits of documents /explanations submit by you within reasonable time period.", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                table.AddCell(GetFontWithStyle("Kindly, contact the undersigned if any assistance is required for above mentioned case. ", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("Thanking You,", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("For: Surveyor", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("(Surveuor& Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Mobile No.: 9868704744", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Email: claims.skm@gmail.com", NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(GetFontWithStyle("  Cc: to the Insurers & Brokers", BoldFont, PdfPCell.ALIGN_LEFT, "N", 2));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);

            }

            document.CloseDocument();
            return workStream;
        }
        #endregion
        #region Task Manager
        public ActionResult TaskManager()
        {
            return View();
        }

        #endregion
        #region MotorOccurence
        public ActionResult MotorOccurenceNew(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims","Suvery");
            }
            else
            {
                return View();
            }
        }
        #endregion
        #region pdf motor FSR
        public FileResult generatePdfMotorFSR(int claimId,int userId)
        {
            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);
            MemoryStream workStream = new MemoryStream();
            workStream = MotorFSRMemoryStream(claimId,userId);
            return new FileStreamResult(getPageCountOnFooter(workStream), "application/pdf");
        }
        public MemoryStream MotorFSRMemoryStream(int claimId,int userId)
        {
            CSGenrate c = new CSGenrate();
            c = BLGenrateClaim.GetInsuredDetails(claimId.ToString());

            //int claimId = 37;
            BLMarineLorDetailsNew BLMarineLorReminder = new BLMarineLorDetailsNew();
            DataSet ds = BLMarineLorReminder.getDetailForPdfMotorFSR(claimId,userId);
            MemoryStream workStream = new MemoryStream();
            //DataTable dt = new DataTable();
            DataTable dt = BLMarineLorReminder.getDetailForLorReminder(claimId);

            PdfPCell cell = null;
            PdfPTable table = null;

            Document document = new Document(new iTextSharp.text.Rectangle(550f, 680f), 0f, 0f, 80f, 45f);
            PdfWriter writer = PdfWriter.GetInstance(document, workStream);
            writer.CloseStream = false;
            document.OpenDocument();
            int Table1Count = ds.Tables[0].Rows.Count;
            if (ds.Tables[0].Rows.Count > 0)
            {
                Header objHeader = new Header();
                //objHeader.path = Server.MapPath("~/SurveryorDocument/" + "LetterHead");
                objHeader.path = Server.MapPath("~/SurveryorDocument/Header.jpg");

                writer.PageEvent = objHeader;
                Footer objFooter = new Footer();
                //objFooter.RefNo = (1).ToString(); //"ClaimID";
                //objFooter.ReportName = "Reminder 1";
                objFooter.Signature = Server.MapPath("~/SurveryorDocument/" + "DSign");
                // objFooter.Signature = Server.MapPath("~/images/footer.jpg");
                writer.PageEvent = objFooter;
                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });

               
                table.AddCell(GetFontWithStyle("Insured: " + dt.Rows[0]["Name"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
                table.AddCell(GetFontWithStyle("Address: " + dt.Rows[0]["Address"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT, "N", 2));
               
                table.AddCell(GetFontWithStyle("DATE: " + DateTime.Now.ToString("dd-MMM-yyyy", CultureInfo.InvariantCulture), NormalFont, PdfPCell.ALIGN_LEFT, "N"));
                table.AddCell(GetFontWithStyle("Our Ref No." + dt.Rows[0]["RefNo"].ToString(), NormalFont, PdfPCell.ALIGN_RIGHT, "N", 2));
                cell = GetFontWithStyle("FINAL SURVEY REPORT", BoldFont12, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);
                cell = GetFontWithStyle("Issued without Prejudice", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 2;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                document.Add(table);

                table = new PdfPTable(2);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] { 0.5f, 0.5f });

                cell = GetFontWithStyle("1.	Over view of the Claim", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 2;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_LEFT));
                document.Add(table);

                table = new PdfPTable(3);
                table.TotalWidth = 500f;
                table.WidthPercentage = 100f;
                table.LockedWidth = true;
                table.SetWidths(new float[] {  0.47f,0.03f ,0.47f });

                table.AddCell(GetFontWithStyle("Insurers claim No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[0].Rows[0]["InsrClaimNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Insured’s claim No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[0].Rows[0]["ClaimID"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Our Previous Report/s", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Estimate of Loss as Reported", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[0].Rows[0]["ReportedLoss"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Final Settlement Amount Recommended", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("2.	Policy Particulars ", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Policy No. /Cover Note No. ", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["PolicyNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Period From", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["PeriodFromDT"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Period To", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["PeriodToDT"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Name of the Insured", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["InsdName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Address of the Insured", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["InsdAddress"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Vehicle Registration No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["VehicleRegistrationNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));



                table.AddCell(GetFontWithStyle("Hire Purchase Agreement/ Hypothecation Clause", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["HPAgreement"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Sum-Insured (IDV)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[1].Rows[0]["SumInsured"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                cell = GetFontWithStyle("1.	The Insurers are advised to check 64VB at the time final settlement of the claim. ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("2.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER|PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);
                cell = GetFontWithStyle("3.	Was Insured vehicle found in use of same trade as registered under MV act and risk covered in the policy, If no please described.  ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER|PdfPCell.BOTTOM_BORDER;
                table.AddCell(cell);
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                
                cell = GetFontWithStyle("3.	Particular of Survey", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Date of Application of Survey", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[2].Rows[0]["ApplicationOfSurveyDT"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Source of Instruction for Survey ", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[0].Rows[0]["InstructorName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Date & Time of Survey", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[2].Rows[0]["SurveyDT"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Place of Survey", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[0].Rows[0]["LocationOfSurvey"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("4.	Vehicle Particulars", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Registration Number", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["RCNO"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Date of Registration", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["RCDate"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Registered Owner of the Vehicle", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(c.MarineJIR.Insured, BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Chassis Number", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[3]["ChasisNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Engine Number", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["EngineNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Make& Variant", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Make"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Model", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Model"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Class of Vehicle", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Class"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Type of Body", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["BodyType"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Seating Capacity", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Seating"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));




                table.AddCell(GetFontWithStyle("Fuel use in vehicle", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Fuel"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Colour of Vehicle", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["Color"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Fitness Valid upto", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["FitnessUpto"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Permit type", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["PermitType"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Permit Valid upto", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["PermitUpto"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Area of Operation", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["OperationArea"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Registered Laden Weight/Gross Vehicle Weight", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["LadenWeight"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Un-Laden Weight", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["UnLadenWeight"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));




                table.AddCell(GetFontWithStyle("Tax / OTT paid", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["TaxOTT"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Odometer Reading", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[3].Rows[0]["OdometerReading"].ToString(), NormalFont, PdfPCell.ALIGN_LEFT));


                cell = GetFontWithStyle("	1.	Above mentioned vehicle particulars verified by the undersign from Original RC ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("2.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("3.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("4.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);
                cell = GetFontWithStyle("5.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);
                cell = GetFontWithStyle("6.	Was Insured vehicle found in use of same trade as registered under MV act and risk covered in the policy, If no please described.  ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("Photo of Original R C Verified by undersign", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);


                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());


                cell = GetFontWithStyle("5.	Driving License Particulars", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Name of driver", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["DriverName"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Driving License Number", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["DLNo"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Date of Issued", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["IssueDate"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Valid up to", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["ValidUpTO"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("License issuing Authority", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["DLAuthority"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Type of License", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Endorsement ( If Any)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["Endorsment"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Badge No. (if Any)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(ds.Tables[4].Rows[0]["Remarks"].ToString(), BoldFont, PdfPCell.ALIGN_LEFT));
                
                cell = GetFontWithStyle("	1.	Above mentioned vehicle particulars verified by the undersign from Original RC ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("2.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("3.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("4.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);
                cell = GetFontWithStyle("5.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);
                cell = GetFontWithStyle("6.	Was Insured vehicle found in use of same trade as registered under MV act and risk covered in the policy, If no please described.  ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("Photo of Original R C Verified by undersign", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);


                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("6.	About Occurrence/Accident", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Date & Time of Accident", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Place of Accident", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Nature of Accident", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Damaged side/s", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Spot Survey", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Police Report/FIR/Panchnama", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Fire Brigade Report", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Third Party Property Loss", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Third Party Body Injury", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                cell = GetFontWithStyle("	1.	Above mentioned vehicle particulars verified by the undersign from Original RC ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("2.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("3.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("4.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER|PdfPCell.BOTTOM_BORDER;
                table.AddCell(cell);


                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("7.	Journey Particulars (Route Particular)", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Origin of Journey (Journey Starting Place)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("End of Journey (Targeted place of Journey )", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Transport Company ( Contract for journey with)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("GR/LR/CN No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Date of GR/LR/CN No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Name or Type of  Transported Item", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Weight of Load/No. of Passengers ", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
               
                cell = GetFontWithStyle("	1.	Above mentioned vehicle particulars verified by the undersign from Original RC ", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.TOP_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("2.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER;
                table.AddCell(cell);

                cell = GetFontWithStyle("3.	The above policy is found issued with zero depreciation clause.", NormalFont, PdfPCell.ALIGN_LEFT);
                cell.Colspan = 3;
                cell.Border = PdfPCell.LEFT_BORDER | PdfPCell.RIGHT_BORDER | PdfPCell.BOTTOM_BORDER;
                table.AddCell(cell);

              


                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("8.	Brief Note on Cause of Loss/Accident, as Described by the Insured", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);
                cell = GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("9.	Other Legal Aspects of the Accident/Documents if applicable", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("FIR No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("FIR Date", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Police Station", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Sections of IPC 1860", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Subject matter of the FIR", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Final Report/Un-traceable Report(Applicable in case if theft only)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Remarks/conclusions", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("10. Previous claim History of the Insured Vehicle", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Previous Policy No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Insurers", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("No. of Accident in Previous Policy", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Date/s of Accident", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Claimed Amount/s", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Compensation Amount Received (Total)", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Surveyor's Remarks/observation", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("11.PARTICULARS OF LOSS/DAMAGE", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("12. INSURED’S  CLAIM", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Name of Repairer", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Address of the Repairer", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Authorized service centre by Manufacturer", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Total Estimated Repair cost", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("13.Summary of Claim/Assessment", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("14. Salvage", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());

                cell = GetFontWithStyle("15. RE-INSPECTION ", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(GetFontWithStyle("Date & time of the survey visit", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Place of the survey visit", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Purpose of visit", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Contact Detail of the Insured’s representative", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));


                table.AddCell(GetFontWithStyle("Mobile No.", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Email:", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Proof of Payment/Cashless Scheme", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));

                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetFontWithStyle("Salvage Destruction", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

                table.AddCell(GetFontWithStyle("Surveyor's Remarks/observation", NormalFont, PdfPCell.ALIGN_LEFT));
                table.AddCell(GetSeprator(BoldFont));
                table.AddCell(GetFontWithStyle(" ", BoldFont, PdfPCell.ALIGN_LEFT));

    



                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());


                cell = GetFontWithStyle("16. FINAL REMARKS/NOTES ", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = GetFontWithStyle("", BoldFont, PdfPCell.ALIGN_CENTER);
                cell.Colspan = 3;
                table.AddCell(cell);

                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());
                table.AddCell(AddBlankRow());


                cell = GetFontWithStyle("The above report is issued without prejudice to the rights of the Insurers. The payment of the claim is subject to the terms and conditions of the policy under which the claim has been preferred.", NormalFont, PdfPCell.ALIGN_LEFT,"N");
                cell.Colspan = 3;
                table.AddCell(cell);


                cell = GetFontWithStyle("For: Surveyor", NormalFont, PdfPCell.ALIGN_LEFT,"N");
                cell.Colspan = 3;
                table.AddCell(cell);


                cell = GetFontWithStyle("(Surveuor& Loss Assessor)", BoldFont, PdfPCell.ALIGN_LEFT, "N");
                cell.Colspan = 3;
                table.AddCell(cell);

                cell = GetFontWithStyle("ASSESSMENT SHEET", BoldFont, PdfPCell.ALIGN_CENTER, "N");
                cell.Colspan = 3;
                table.AddCell(cell);
                
                document.Add(table);

                

            }

            document.CloseDocument();

            return workStream;
        }


        #endregion

    }
}

