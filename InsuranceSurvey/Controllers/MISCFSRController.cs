﻿using BusinessLayer;
using BusinessLayer.BL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class MISCFSRController : Controller
    {

        //static int id;
        BLMISCFSR _fsr = new BLMISCFSR();

        //
        // GET: /MISCFSR/

        public ActionResult IntroClaim(string cid, string tableName)
        {
            if (string.IsNullOrEmpty(cid) && (string.IsNullOrEmpty(tableName)))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }

        public ActionResult InsuranceParticulars(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult InsuredClaim(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult AssessmentofLoss(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult AssessmentofLoss_PartSecond(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SurveyParticulars(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult SurveyVisit(string cid, string fromTable)
        {

            if (string.IsNullOrEmpty(cid) || string.IsNullOrEmpty(fromTable))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult ObservationsCircumstances(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult AboutInsured(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public ActionResult AboutCauseOfLoss(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }

        public ActionResult OtherReportsForMaterialFacts(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }

        }
        public ActionResult LiabilityUnderPolicy(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }

        }
        public ActionResult NatureExtentDamage(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }

        public ActionResult RecoverySubrogation(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }


        //public ActionResult saveOtherReportsForMaterialFacts(HttpPostedFileBase PoliceReportPhoto, HttpPostedFileBase FireBrigadeReportPhoto, HttpPostedFileBase MeteorologicalReportPhoto, string Conclusion, int claimId)
        //{

        //    CSMISCOtherReportsForMaterialFacts ormf = new CSMISCOtherReportsForMaterialFacts();
        //    //ormf.Conclusion = Conclusion;
        //    ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
        //    ormf.ClaimID = claimId;
        //    //ormf.PK_MISCORMFID = id;
        //    string fileName = " ";
        //    string Imgpath = " ";
        //    if (PoliceReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempPolice"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(PoliceReportPhoto.FileName);
        //        ormf.PoliceReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        PoliceReportPhoto.SaveAs(Imgpath);
        //    }
        //    if (FireBrigadeReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempFire"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(FireBrigadeReportPhoto.FileName);
        //        ormf.FireBrigadeReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        FireBrigadeReportPhoto.SaveAs(Imgpath);
        //    }
        //    if (MeteorologicalReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempMeteo"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(MeteorologicalReportPhoto.FileName);
        //        ormf.MeteorologicalReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        MeteorologicalReportPhoto.SaveAs(Imgpath);
        //    }
        //    _fsr.saveOtherReportsForMaterialFacts(ormf);
        //    return RedirectToAction("OtherReportsForMaterialFacts", "MISCFSR", new { cid = claimId });
        //    //return View("OtherReportsForMaterialFacts?cid='" + claimId + "'","MISCFSR");

        //}
        #region IntroClaim
        public JsonResult saveIntroClaim(CSMISCFSRIntroClaim ic)
        {
            string Msg = null;
            int Status = 0;
            BLGenrateClaim gc = new BLGenrateClaim();
            try
            {
                ic.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                gc.updateLocationOfSurvey(Convert.ToInt32(ic.ClaimID), Convert.ToInt32(Session["InsuranceUserId"]), ic.AddressofLossLocation);
                Status = _fsr.saveIntroClaim(ic);
                Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                Msg = "Error:" + ex.Message;
            }
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCmtLiabSP()
        {
            string msg = null;
            List<CSMISCCmtLiabSP> listCmtLiabSP = new List<CSMISCCmtLiabSP>();
            try
            {
                listCmtLiabSP = _fsr.getCmtLiabSP();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCmtLiabSP = listCmtLiabSP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCL_OP()
        {
            string msg = null;
            List<CSMISCCL_OP> listCL_OP = new List<CSMISCCL_OP>();
            try
            {
                listCL_OP = _fsr.getCL_OP();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCL_OP = listCL_OP, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult getIntroClaim(int claimId)
        {
            string msg = null;
            BLGenrateClaim gc = new BLGenrateClaim();
            CSMISCFSRIntroClaim listIC = new CSMISCFSRIntroClaim();

            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listIC = _fsr.getIntroClaim(claimId, userId);
                listIC.AddressofLossLocation = gc.getLocationOfSurvey(claimId, Convert.ToInt32(Session["InsuranceUserId"]));
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listIC = listIC, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Insurance Particular
        public JsonResult saveInsuranceParticulars(CSMISCFSRInsuranceParticulars ip, List<CSMISCCoInsurers> listCIn, List<CSMISCPolicySumInsured> listPSI, List<CSMISCRelWarCond> listRWC)
        {
            string Msg = null;
            int Status = 0;
            int claimId = ip.ClaimID;
            try
            {
                ip.UserID = Convert.ToInt32(Session["InsuranceUserId"]);

                Status = _fsr.saveInsuranceParticulars(ip);
                if (ip.CINYN == "No")
                {
                    _fsr.deleteCoInsurers(ip.ClaimID, ip.UserID);
                    _fsr.deleteContribution(ip.ClaimID, ip.UserID);
                }
                else if (ip.CINYN == "Yes")
                {
                    _fsr.deleteCoInsurers(ip.ClaimID, ip.UserID);
                    _fsr.deleteContribution(ip.ClaimID, ip.UserID);
                    if ((listCIn != null))
                    {
                        foreach (CSMISCCoInsurers row in listCIn)
                        {

                            CSMISCCoInsurers ci = new CSMISCCoInsurers();
                            ci.PK_CoIns_ID = row.PK_CoIns_ID;
                            ci.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                            ci.ClaimID = claimId;
                            ci.CoInsurersID = row.CoInsurersID;
                            ci.Percentage = row.Percentage;
                            _fsr.saveCoInsurers(ci);

                        }
                    }
                }
                if (ip.PSIYN == "No")
                {
                    //_fsr.deletePolicySumInsured(ip.ClaimID, ip.UserID);
                }
                else if (ip.PSIYN == "Yes")
                {
                    if (listPSI != null)
                    {
                        foreach (CSMISCPolicySumInsured row in listPSI)
                        {
                            row.ClaimID = ip.ClaimID;
                            row.UserID = ip.UserID;
                            row.Tb_ID = row.PK_Tb_ID;
                            _fsr.savePolicySumInsured(row);
                        }
                    }

                }
                if ((listRWC != null) && (!listRWC.Any()))
                {
                    foreach (CSMISCRelWarCond row in listRWC)
                    {
                        CSMISCRelWarCond rwc = new CSMISCRelWarCond();
                        rwc.PK_RWC_ID = row.PK_RWC_ID;
                        rwc.ClaimID = claimId;
                        rwc.UserID = ip.UserID;
                        rwc.RelWarCond = row.RelWarCond;
                        _fsr.saveRelWarCond(rwc);
                    }
                }
                else if (listRWC == null)
                {
                    _fsr.deleteRelWarCond(ip.ClaimID, ip.UserID);
                }
                Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                Msg = "Error:" + ex.Message;
            }
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInsuranceParticulars(int claimId)
        {
            string msg = null;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCFSRInsuranceParticulars listIP = new CSMISCFSRInsuranceParticulars();
            try
            {
                listIP = _fsr.getInsuranceParticulars(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listIP = listIP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInsurers()
        {
            string msg = null;
            List<CSInsurers> listIns = new List<CSInsurers>();
            try
            {
                listIns = _fsr.getInsurers();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listIns = listIns, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getCoInsurers(int claimId)
        {
            string msg = null;

            List<CSMISCCoInsurers> listCIn = new List<CSMISCCoInsurers>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listCIn = _fsr.getCoInsurers(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCIn = listCIn, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPolicySumInsured(int claimId)
        {
            string msg = null;

            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPSI = _fsr.getPolicySumInsured(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listPSI = listPSI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRelWarCond(int claimId)
        {
            string msg = null;
            List<CSMISCRelWarCond> listRWC = new List<CSMISCRelWarCond>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listRWC = _fsr.getRelWarCond(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listRWC = listRWC, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPolicySumInsuredAndTableName(int claimId)
        {
            string msg = null;
            List<CSMISCPolicySumInsured> listPT = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPT = _fsr.getPolicySumInsuredAndTableName(claimId,userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listPT = listPT, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region SurveyParticulars
        public JsonResult getInstructorNameInstructionDateByClaimID(int claimId)
        {
            string msg = null;
            CSMISCFSRSurveyParticulars gc = new CSMISCFSRSurveyParticulars();
            try
            {
                gc = _fsr.getInstructorNameInstructionDateByClaimID(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { gc = gc, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveSurveyParticulars(CSMISCFSRSurveyParticulars sp)
        {
            string msg = null;
            int status = 0;
            try
            {
                sp.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _fsr.saveSurveyParticulars(sp);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSurveyParticulars(int claimId)
        {
            string msg = null;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]); ;
            CSMISCFSRSurveyParticulars listSP = new CSMISCFSRSurveyParticulars();
            try
            {
                listSP = _fsr.getSurveyParticulars(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listSP = listSP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion  
        #region Survey Visit
        public JsonResult saveSurveyVisit(CSSurveyorVisits sv)
        {
            string msg = null;
            int status = 0;
            try
            {
                sv.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                sv.IPAddress = BusinessHelper.GetIpAdress();
                status = _fsr.saveSurveyVisit(sv);


                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveSurveyVisit_byObservationsCicum(List<CSSurveyorVisits> listSV)
        {
            string msg = null;
            int status = 0;
            try
            {
                foreach (CSSurveyorVisits row in listSV)
                {
                    CSSurveyorVisits sv = new CSSurveyorVisits();
                    sv.PkId = row.PkId;
                    sv.StartDate = row.StartDate;
                    sv.EndDate = row.EndDate;
                    sv.PlaceVisit = row.PlaceVisit;
                    sv.PurposeVisit = row.PurposeVisit;
                    sv.Remark = row.Remark;
                    sv.IPAddress = BusinessHelper.GetIpAdress();
                    sv.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                    status = _fsr.saveSurveyVisit(sv);
                }

                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        //public JsonResult saveSurveyVisit(CSSurveyorVisits sv)
        //{


        //    string Msg = null;
        //    int Status = 0;

        //    Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
        //    return Json(Msg, JsonRequestBehavior.AllowGet);
        //}


        public JsonResult getSurveyVisit(int claimId, string tableName)
        {
            string msg = null;
            List<CSSurveyorVisits> listSV = new List<CSSurveyorVisits>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);

                listSV = _fsr.getSurveyVisit(userId, claimId, tableName);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listSV = listSV, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult deleteSurveyVisit(int pkId)
        {
            string msg = null;
            int status = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _fsr.deleteSurveyVisit(pkId, userId);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Observation Circumstances
        public JsonResult saveObservationsCircumstances(CSMISCFSRObservationsCircumstances oc, List<CSSurveyorVisits> listSV)
        {
            string msg = null;
            int status = 0;
            try
            {
                if ((listSV != null) && (!listSV.Any()))
                {
                    foreach (CSSurveyorVisits row in listSV)
                    {
                        CSSurveyorVisits sv = new CSSurveyorVisits();
                        sv.PkId = row.PkId;
                        sv.StartDate = row.StartDate;
                        sv.EndDate = row.EndDate;
                        sv.PlaceVisit = row.PlaceVisit;
                        sv.PurposeVisit = row.PurposeVisit;
                        sv.Remark = row.Remark;
                        sv.IPAddress = BusinessHelper.GetIpAdress();
                        sv.FromTableName = "SiteVisit";
                        sv.EntryBy = Convert.ToString(Session["InsuranceUserId"]);
                        _fsr.saveSurveyVisit(sv);
                    }
                }

                oc.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                if (oc.ChkboxCLTrans == false)
                {
                    oc.CLTrans = string.Empty;

                }
                if (oc.ChkboxNewsPaper == false)
                {
                    oc.NameNewsPaper = string.Empty;
                    oc.NewsPubDT = string.Empty;
                }
                if (oc.ChkbxNPTrans == false)
                {
                    oc.NewsTrans = string.Empty;
                }
                status = _fsr.saveObservationsCircumstances(oc);
                msg = status > 0 ? "Save Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }

            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveObservationsCircumstancesPhotoDoc(string clPhotoDoc, string newsPhotoDoc, int claimId)
        {
            string msg = null;
            int status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCFSRObservationsCircumstances oc = new CSMISCFSRObservationsCircumstances();
            oc = _fsr.getObservationsCircumstances(userId, claimId);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {


                        HttpPostedFileBase file = files[i];
                        string fname;


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && clPhotoDoc == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + oc.CLQuotePhotoDoc);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                oc.CLQuotePhotoDoc = fname;
                            }
                            else if (i == 1 && newsPhotoDoc == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + oc.NewsPhotoDoc);
                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                oc.NewsPhotoDoc = fname;
                            }

                        }


                        fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                        file.SaveAs(fname);
                    }
                    oc.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                    status = _fsr.saveObservationsCircumstances(oc);
                    // Returns message that successfully uploaded  
                    //return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getObservationsCircumstances(int claimId)
        {
            string msg = null;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCFSRObservationsCircumstances listOC = new CSMISCFSRObservationsCircumstances();
            try
            {
                listOC = _fsr.getObservationsCircumstances(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listOC = listOC, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region About Insured
        public JsonResult saveAboutInsured(CSMISCAboutInsured ai)
        {

            string msg = null;
            int status = 0;
            try
            {
                ai.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                if (ai.ChkbxFS == false)
                {
                    ai.InsuredBalText = string.Empty;
                    _fsr.deleteFinancialStatus(ai.ClaimID, ai.UserID);
                }
                if (ai.ChkbxLayoutSite == false)
                {
                    string tempPath = Server.MapPath("~/files/MISC/" + ai.LayoutSitePhotoDoc);

                    if (System.IO.File.Exists(tempPath))
                    {
                        System.IO.File.Delete(tempPath);
                    }
                    ai.LayoutSitePhotoDoc = string.Empty;
                }
                status = _fsr.saveAboutInsured(ai);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAboutInsuredPhotoDoc(string lsPD, int claimId)
        {
            string msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCAboutInsured ai = new CSMISCAboutInsured();
            ai = _fsr.getAboutInsured(userId, claimId);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {

                        HttpPostedFileBase file = files[i];
                        string fname;

                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && lsPD == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + ai.LayoutSitePhotoDoc);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                ai.LayoutSitePhotoDoc = fname;
                                fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                                file.SaveAs(fname);
                                ai.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                                Status = _fsr.saveAboutInsured(ai);
                            }
                        }

                    }

                    // Returns message that successfully uploaded  
                    //return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }

            msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveFinancialStatus(CSMISCFinancialStatus fs)
        {
            string msg = null;
            int status = 0;
            try
            {

                fs.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _fsr.saveFinancialStatus(fs);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAboutInsured(int claimId)
        {
            string msg = null;
            CSMISCAboutInsured listAI = new CSMISCAboutInsured();
            try
            {

                int userId = Convert.ToInt32(Session["InsuranceUserId"]); ;

                listAI = _fsr.getAboutInsured(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAI = listAI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getFinancialStatus(int claimId)
        {
            string msg = null;
            List<CSMISCFinancialStatus> listFS = new List<CSMISCFinancialStatus>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]); ;

                listFS = _fsr.getFinancialStatus(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listFS = listFS, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region About Cause Of Loss
        public JsonResult saveAboutCauseOfLoss(CSMISCAboutCauseOfLoss acl)
        {
            string msg = null;
            int status = 0;
            try
            {

                acl.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _fsr.saveAboutCauseOfLoss(acl);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAboutCauseOfLoss(int claimId)
        {
            string msg = null;
            CSMISCAboutCauseOfLoss listACL = new CSMISCAboutCauseOfLoss();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]); ;

                listACL = _fsr.getAboutCauseOfLoss(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listACL = listACL, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getStatutoryAuthoritiesInformed()
        {
            string msg = null;
            List<CSMISCStatutoryAuthoritiesInformed> listSAI = new List<CSMISCStatutoryAuthoritiesInformed>();
            try
            {
                listSAI = _fsr.getStatutoryAuthoritiesInformed();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listSAI = listSAI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getEffectofOperatedPeril()
        {
            string msg = null;
            List<CSMISCEffectofOperatedPeril> listEOP = new List<CSMISCEffectofOperatedPeril>();
            try
            {
                listEOP = _fsr.getEffectofOperatedPeril();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listEOP = listEOP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getACL_NatureofLoss()
        {
            string msg = null;
            List<CSMISC_ACL_NatureofLoss> listNL = new List<CSMISC_ACL_NatureofLoss>();
            try
            {
                listNL = _fsr.getACL_NatureofLoss();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listNL = listNL, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region OtherReportsForMaterialFacts
        public JsonResult saveOtherReportsForMaterialFacts(CSMISCOtherReportsForMaterialFacts ormf)
        {
            string msg = null;
            int status = 0;
            try
            {
                ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                if (ormf.C_PR_Tran == false)
                {
                    ormf.Pr_Tran = string.Empty;
                }
                if (ormf.C_Pr_IPC == false)
                {
                    ormf.Pr_IPC_ID = string.Empty;
                }
                if (ormf.C_Pr_PRC == false)
                {
                    ormf.Pr_PRC = string.Empty;
                }
                if (ormf.C_Pr_Remarks == false)
                {
                    ormf.Pr_Remarks = string.Empty;
                }
                if (ormf.C_Fr_Tran == false)
                {
                    ormf.Fr_Tran = string.Empty;
                }

                if (ormf.C_Fr_sumry == false)
                {
                    ormf.Fr_Sumry = string.Empty;
                }
                if (ormf.C_Fr_Remarks == false)
                {
                    ormf.Fr_Remarks = string.Empty;
                }
                if (ormf.C_Mr_Tran == false)
                {
                    ormf.Mr_Tran = string.Empty;
                }

                if (ormf.C_Mr_Sumry == false)
                {
                    ormf.Mr_Sumry = string.Empty;
                }
                if (ormf.C_Mr_Remarks == false)
                {
                    ormf.Mr_Remarks = string.Empty;
                }

                if (ormf.C_Fslr_Tran == false)
                {
                    ormf.Fslr_Tran = string.Empty;
                }

                if (ormf.C_Fslr_Sumry == false)
                {
                    ormf.Fslr_Sumry = string.Empty;
                }
                if (ormf.C_Fslr_Remarks == false)
                {
                    ormf.Fslr_Remarks = string.Empty;
                }

                status = _fsr.saveOtherReportsForMaterialFacts(ormf);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveOtherReportsForMaterialFactsPhoto(string policePhoto, string firePhoto, string metPhoto, int claimId)
        {
            string Msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCOtherReportsForMaterialFacts listORMF = new CSMISCOtherReportsForMaterialFacts();
            listORMF = _fsr.getOtherReportsForMaterialFacts(userId, claimId);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {


                        HttpPostedFileBase file = files[i];
                        string fname;


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && policePhoto == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + listORMF.PoliceReportPhoto);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                listORMF.PoliceReportPhoto = fname;
                            }
                            else if (i == 1 && firePhoto == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + listORMF.FireBrigadeReportPhoto);
                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                listORMF.FireBrigadeReportPhoto = fname;
                            }
                            else if (i == 2 && metPhoto == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + listORMF.MeteorologicalReportPhoto);
                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                listORMF.MeteorologicalReportPhoto = fname;
                            }
                            //fname = file.FileName;
                        }


                        fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                        file.SaveAs(fname);
                    }
                    Status = _fsr.saveOtherReportsForMaterialFacts(listORMF);
                    // Returns message that successfully uploaded  
                    //return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }

            //ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
            //Status = _fsr.saveOtherReportsForMaterialFacts(ormf);
            Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }

        //public JsonResult saveOtherReportsForMaterialFacts(CSMISCOtherReportsForMaterialFacts ormf)
        //{
        //    string Msg = null;
        //    int Status = 0;

        //    ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
        //    Status = _fsr.saveOtherReportsForMaterialFacts(ormf);
        //    Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
        //    return Json(Msg, JsonRequestBehavior.AllowGet);
        //}
        public JsonResult getOtherReportsForMaterialFacts(int claimId)
        {
            string msg = null;
            CSMISCOtherReportsForMaterialFacts listORMF = new CSMISCOtherReportsForMaterialFacts();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]); ;
                listORMF = _fsr.getOtherReportsForMaterialFacts(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listORMF = listORMF, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getIPCSection()
        {
            string msg = null;
            List<CSMasterIPCSection> listIPSec = new List<CSMasterIPCSection>();
            try
            {
                listIPSec = _fsr.getIPCSection();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listIPSec = listIPSec, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region   Liability Under Policy
        public JsonResult saveLiabilityUnderPolicy(CSMISCLiabilityUnderPolicy lup, List<CSMISCWarCond> listWC)
        {
            string msg = null;
            int status = 0;
            try
            {
                int claimId = lup.ClaimID;
                lup.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                if (lup.TypeOfPolicy == "Specific Named peril Policy")
                {
                    lup.CaptionedFalling = string.Empty;
                    lup.CaptionedFallingText = string.Empty;

                }
                if (lup.TypeOfPolicy == "Exclusion driven or All Risk Policy")
                {
                    lup.OP_ID = 0;

                }
                if (lup.ConclusionInsurers != "Others")
                {
                    lup.BlankText = string.Empty;
                }
                status = _fsr.saveLiabilityUnderPolicy(lup);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
                if ((listWC != null) && (!listWC.Any()))
                {

                    foreach (CSMISCWarCond row in listWC)
                    {
                        CSMISCWarCond wc = new CSMISCWarCond();
                        wc.PK_WC_ID = row.PK_WC_ID;
                        wc.ClaimID = claimId;
                        wc.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                        wc.RWC_ID = row.RWC_ID;
                        wc.StatusImplement = row.StatusImplement;
                        _fsr.saveWarCond(wc);

                    }
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getLiabilityUnderPolicy(int claimId)
        {

            string msg = null;
            CSMISCLiabilityUnderPolicy lup = new CSMISCLiabilityUnderPolicy();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                lup = _fsr.getLiabilityUnderPolicy(userId, claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { lup = lup, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getWarCond(int claimId)
        {
            string msg = null;
            List<CSMISCWarCond> listWC = new List<CSMISCWarCond>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listWC = _fsr.getWarCond(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listWC = listWC, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getOperatedPeril()
        {
            string msg = null;
            List<CSMISCOperatedPeril> listOP = new List<CSMISCOperatedPeril>();
            try
            {
                listOP = _fsr.getOperatedPeril();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listOP = listOP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Nature Extent Damage
        public JsonResult saveNatureExtentDamage(CSMISCFSRNatureExtentDamage n)
        {
            string msg = null;
            int status = 0;
            try
            {
                if (n.ChkbxPS == false)
                {
                    n.SrNoPS = " ";
                }
                if (n.Stage == "RM" || n.Stage == "FG")
                {
                    n.WIPStage = " ";
                    n.BatchSize = " ";
                }
                n.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                status = _fsr.saveNatureExtentDamage(n);
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getNatureExtentDamage(int claimId, int psi_Id)
        {
            string msg = null;
            List<CSMISCFSRNatureExtentDamage> listNED = new List<CSMISCFSRNatureExtentDamage>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);


                listNED = _fsr.getNatureExtentDamage(userId, claimId, psi_Id);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listNED = listNED, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDesc_And_ShortName_byClaimID(int claimId)
        {
            string msg = null;
            List<CSMISCPolicySumInsured> listDS = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);

                listDS = _fsr.getDesc_And_ShortName_byClaimID(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listDS = listDS, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSumInsured_And_DescofProp(int PK_PSI_ID, int claimId)
        {
            string msg = null;
            decimal SumInsured = 0;
            string Description = null;

            List<CSMISCPolicySumInsured> listPS = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPS = _fsr.getDesc_And_ShortName_byClaimID(claimId, userId);
                Description = listPS.Where(x => x.PK_PSI_ID == PK_PSI_ID).Select(x => x.Description).FirstOrDefault();
                SumInsured = listPS.Where(x => x.PK_PSI_ID == PK_PSI_ID).Select(x => x.SumInsured).FirstOrDefault();
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { Description = Description, SumInsured = SumInsured, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDescription_SumIns_byClaimID(int claimId)
        {
            string msg = null;
            List<CSMISCPolicySumInsured> listDS = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listDS = _fsr.getDescription_SumIns_byClaimID (claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listDS = listDS, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getPolicySumInsured_AND_ShortName(int claimId)
        {
            string msg = null;
            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPSI = _fsr.getPolicySumInsured_AND_ShortName(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }
            return Json(new { listPSI = listPSI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        //public JsonResult saveOtherReportsForMaterialFacts(HttpPostedFileBase PoliceReportPhoto, HttpPostedFileBase FireBrigadeReportPhoto, HttpPostedFileBase MeteorologicalReportPhoto, string Conclusion, int claimId, string abc)
        //{

        //    CSMISCOtherReportsForMaterialFacts ormf = new CSMISCOtherReportsForMaterialFacts();
        //    ormf.Conclusion = Conclusion;
        //    ormf.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
        //    ormf.ClaimID = claimId;
        //    //ormf.PK_MISCORMFID = id;
        //    string fileName = " ";
        //    string Imgpath = " ";
        //    if (PoliceReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempPolice"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(PoliceReportPhoto.FileName);
        //        ormf.PoliceReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        PoliceReportPhoto.SaveAs(Imgpath);
        //    }
        //    if (FireBrigadeReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempFire"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(FireBrigadeReportPhoto.FileName);
        //        ormf.FireBrigadeReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        FireBrigadeReportPhoto.SaveAs(Imgpath);
        //    }
        //    if (MeteorologicalReportPhoto != null)
        //    {
        //        string tempPath = Path.Combine(Server.MapPath("~/files/MISC/"), TempData["tempMeteo"].ToString());
        //        if (System.IO.File.Exists(tempPath))
        //        {
        //            System.IO.File.Delete(tempPath);
        //        }

        //        fileName = Guid.NewGuid().ToString() + Path.GetExtension(MeteorologicalReportPhoto.FileName);
        //        ormf.MeteorologicalReportPhoto = fileName;
        //        Imgpath = Path.Combine(Server.MapPath("~/files/MISC/"), fileName);
        //        MeteorologicalReportPhoto.SaveAs(Imgpath);
        //    }
        //    _fsr.saveOtherReportsForMaterialFacts(ormf);
        //    string a = "hello";
        //    return Json(a,JsonRequestBehavior.AllowGet);
        //    //return View("OtherReportsForMaterialFacts?cid='" + claimId + "'","MISCFSR");

        //}

        #region RecoverySubrogationSave
        public JsonResult saveRecoverySubrogation(CSMISCRecoverySubrogation rs)
        {
            string msg = null;
            int Status = 0;
            try
            {
                rs.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveRecoverySubrogation(rs);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRecoverySubrogation(int claimId)
        {
            string msg = null;
            CSMISCRecoverySubrogation rs = new CSMISCRecoverySubrogation();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                rs = _fsr.getRecoverySubrogation(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { rs = rs, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Insured's Claim
        public JsonResult saveInsuredClaim(CSMISCInsuredClaim i, List<CSMISCDescAmount> listDA)
        {
            string msg = null;
            int Status = 0;
            int claimId = 0;
            try
            {
                claimId = i.ClaimID;
                i.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveInsuredClaim(i);
                foreach (CSMISCDescAmount da in listDA)
                {
                   da.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                    da.ClaimID = claimId;
                  _fsr.saveDescAmount(da);
                }
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getDescAmount(int claimId)
        {
            string msg = null;

            List<CSMISCDescAmount> listDA = new List<CSMISCDescAmount>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listDA = _fsr.getDescAmount(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listDA = listDA, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getReportLoss(int claimId)
        {
            string msg = null;
            string rl = " ";
            try
            {
                rl = _fsr.getReportLoss(claimId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { rl = rl, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInsuredClaim(int claimId)
        {
            string msg = null;
            CSMISCInsuredClaim i = new CSMISCInsuredClaim();
            try
            {
                i = _fsr.getInsuredClaim(claimId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { i = i, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Assessment of Loss
        public JsonResult saveAssessmentofLoss(CSMISCAssessmentofLoss al)
        {
            string msg = null;
            int Status = 0;
            try
            {
                al.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveAssessmentofLoss(al);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveDescofOriginProcurement(CSMISCDescofOriginProcurement d)
        {
            string msg = null;
            int Status = 0;
            try
            {
                d.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveDescofOriginProcurement(d);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDistinctTableName_byClaimID(int claimId)
        {
            string msg = null;
            List<string> listTableName = new List<string>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listTableName = _fsr.getDistinctTableName_byClaimID(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listTableName = listTableName, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssessmentofLoss(int claimId, int psi_Id)
        {
            string msg = null;
            List<CSMISCAssessmentofLoss> listAL = new List<CSMISCAssessmentofLoss>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listAL = _fsr.getAssessmentofLoss(claimId, psi_Id, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAL = listAL, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDescofOriginProcurement(int claimId, int psi_Id)
        {
            string msg = null;
            List<CSMISCDescofOriginProcurement> listDOP = new List<CSMISCDescofOriginProcurement>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listDOP = _fsr.getDescofOriginProcurement(claimId, psi_Id, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listDOP = listDOP, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Assement of loss part Second
        public JsonResult getDescriptionofAL()
        {
            string msg = null;
            List<CSDescriptionofAL> listDAL = new List<CSDescriptionofAL>();
            try
            {
                listDAL = _fsr.getDescriptionofAL();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listDAL = listDAL, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getUOM()
        {
            string msg = null;
            List<CSMISCUOM> listUOM = new List<CSMISCUOM>();
            try
            {
                listUOM = _fsr.getUOM();
                //listUOM.Insert(0,new CSMISCUOM { PK_UOM_ID=0  ,     UOM="Select"});

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listUOM = listUOM, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssessmentDescription(int psi_Id, int claimId)
        {
            string msg = null;
            List<CSMISCAssessmentDescription> listAD = new List<CSMISCAssessmentDescription>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listAD = _fsr.getAssessmentDescription(psi_Id, claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAD = listAD, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getDistinct_PSI_ID_byClaimID(int claimId)
        {
            string msg = null;
            List<CSMISCPolicySumInsured> listPSI = new List<CSMISCPolicySumInsured>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listPSI = _fsr.getDistinct_PSI_ID_byClaimID(claimId, userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listPSI = listPSI, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveAssessmentofLossAhDescCal(CSMISCAsseessmentHeader ah, List<CSMISCAssessmentDescription> listAD, List<CSMISCAssessmentCalculation> listAC)
        {
            string msg = null;
            int claimId = 0;
            int scopeId = 0;
            int status = 0;
            try
            {
                ah.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                claimId = ah.ClaimID;
                scopeId = _fsr.saveAsseessmentHeader(ah);

                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";

                foreach (CSMISCAssessmentDescription ad in listAD)
                {
                    ad.NED_ID = ad.PK_MISCFSRNEDID;
                    ad.ClaimID = claimId;
                    ad.AH_ID = scopeId;
                    status = _fsr.saveAssessmentDescription(ad);
                }
                foreach (CSMISCAssessmentCalculation ac in listAC)
                {
                    ac.AH_ID = scopeId;
                    status = _fsr.saveAssessmentCalculation(ac);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getAssessmentofLossAhDescCal(int claimId, int psi_Id)
        {
            string msg = null;
            CSMISCAsseessmentHeader ah = new CSMISCAsseessmentHeader();
            List<CSMISCAssessmentCalculation> listAC = new List<CSMISCAssessmentCalculation>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                ah = _fsr.getAsseessmentHeader(userId, claimId, psi_Id);
                int ah_Id = ah.PK_AH_ID;
                listAC = _fsr.getAssessmentCalculation(ah_Id);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listAC = listAC, ah = ah, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region RecommendationsLossRemarks
        public ActionResult RecommendationsLossRemarks(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult saveRecommendationsLossRemarks(CSMISCRecommendationsLossRemarks r)
        {
            string msg = null;
            int Status = 0;
            try
            {
                r.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveRecommendationsLossRemarks(r);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRecommendationsLossRemarks(int claimId)
        {
            string msg = null;
            CSMISCRecommendationsLossRemarks rl = new CSMISCRecommendationsLossRemarks();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                rl = _fsr.getRecommendationsLossRemarks(claimId, userId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { rl = rl, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region InsuredConsent
        public ActionResult InsuredConsent(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult saveInsuredConsent(CSMISCInsuredConsent ic)
        {
            string msg = null;
            int Status = 0;
            try
            {
                ic.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                Status = _fsr.saveInsuredConsent(ic);
                msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getInsuredConsent(int claimId)
        {
            string msg = null;
            CSMISCInsuredConsent ic = new CSMISCInsuredConsent();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                ic = _fsr.getInsuredConsent(claimId, userId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { ic = ic, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getModeofCommunication()
        {
            string msg = null;
            List<CSMISCModeofCommunication> listMC = new List<CSMISCModeofCommunication>();
            try
            {
                listMC = _fsr.getModeofCommunication();

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listMC = listMC, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveInsuredConsentPhotoDoc(string sc, int claimId)
        {
            string Msg = null;
            int Status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            CSMISCInsuredConsent ic = new CSMISCInsuredConsent();
            ic = _fsr.getInsuredConsent(claimId, userId);
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {


                        HttpPostedFileBase file = files[i];
                        string fname;


                        if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                        {
                            string[] testfiles = file.FileName.Split(new char[] { '\\' });
                            fname = testfiles[testfiles.Length - 1];
                        }
                        else
                        {
                            string tempFileName = file.FileName;
                            string tempFileEx = Path.GetExtension(tempFileName);
                            fname = Guid.NewGuid().ToString() + tempFileEx;
                            if (i == 0 && sc == "yes")
                            {
                                string tempPath = Server.MapPath("~/files/MISC/" + ic.SnapShotofConsent);

                                if (System.IO.File.Exists(tempPath))
                                {
                                    System.IO.File.Delete(tempPath);
                                }

                                ic.SnapShotofConsent = fname;
                            }

                        }


                        fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                        file.SaveAs(fname);
                    }
                    ic.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                    Status = _fsr.saveInsuredConsent(ic);
                    // Returns message that successfully uploaded  
                    //return Json("File Uploaded Successfully!");
                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            else
            {
                //return Json("No files selected.");
            }
            Msg = Status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            return Json(Msg, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region Contribution			
        public ActionResult Contribution(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult getContribution(int claimId)
        {
            string msg = null;
            List<CSMISCContribution> listCon = new List<CSMISCContribution>();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                listCon = _fsr.getContribution(claimId, userId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listCon = listCon, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveContribution(List<CSMISCContribution> listCon, int claimId)
        {
            string msg = null;
            int status = 0;
            try
            {
                foreach (CSMISCContribution c in listCon)
                {
                    c.ClaimID = claimId;
                    c.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
                    status += _fsr.saveContribution(c);
                }
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Claim Assessment/Adjustment						
        public ActionResult ClaimAssAdjust(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                DataTable dt = new DataTable();
                try
                {
                    int claimId = Convert.ToInt32(cid);
                    int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                    dt = _fsr.getClaimAssAdjustSummary(claimId, userId);
                }
                catch (Exception ex)
                {
                    dt = new DataTable();
                }

                return View(dt);
            }
        }

        public JsonResult getClaimAssAdjust(int claimId)
        {
            string msg = null;
            CSMISCClaimAssAdjust c = new CSMISCClaimAssAdjust();
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                c = _fsr.getClaimAssAdjust(claimId, userId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { c = c, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getRLP_Total_byClaimID(int claimId)
        {
            string msg = null;
            decimal rlp_Total = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                rlp_Total = _fsr.getRLP_Total_byClaimID(userId, claimId);

            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { rlp_Total = rlp_Total, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult saveClaimAssAdjust(CSMISCClaimAssAdjust c, List<CSMISCClaimAssAdjustSummary> listCS)
        {
            string msg = null;
            int status = 0;
            try
            {
                int userId = Convert.ToInt32(Session["InsuranceUserId"]);
                c.UserID = userId;
                int claimId = c.ClaimID;

                status = _fsr.saveClaimAssAdjust(c);
                foreach (CSMISCClaimAssAdjustSummary cs in listCS)
                {
                    cs.UserID = userId;
                    cs.ClaimID = claimId;
                    _fsr.saveClaimAssAdjustSummary(cs);
                }
                msg = status > 0 ? "Saved Successfully!" : "Failed!Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }



        #endregion
        #region CommonData
        public JsonResult getMISCCommonDataGet(int claimId)
        {
            string msg = null;
            CSMISCFSRIntroClaim listCmnData = new CSMISCFSRIntroClaim();
            try
            {
                listCmnData = _fsr.getMISCCommonDataGet(claimId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(listCmnData, JsonRequestBehavior.AllowGet);
        }
        public string getMemoryStream_byBased64BinaryString(string Based64BinaryString, string fileName, string fullPath)
        {
            if (string.IsNullOrEmpty(Based64BinaryString))
            {
                return null;
            }

            string format = "";

            if (Based64BinaryString.Contains("data:image/jpeg;base64,"))
            {
                format = "jpeg";
            }
            else if (Based64BinaryString.Contains("data:image/jpg;base64,"))
            {
                format = "jpg";
            }
            else if (Based64BinaryString.Contains("data:image/gif;base64,"))
            {
                format = "gif";
            }
            else if (Based64BinaryString.Contains("data:image/png;base64,"))
            {
                format = "png";
            }
            else if (Based64BinaryString.Contains("data:image/bmp;base64,"))
            {
                format = "bmp";
            }
            //if (Based64BinaryString.Contains("data:text/plain;base64,"))
            //{
            //    format = "txt";
            //}

            string str = Based64BinaryString.Replace("data:image/jpeg;base64,", " ");//jpg check
            str = str.Replace("data:image/png;base64,", " ");//png check
            str = str.Replace("data:text/plain;base64,", " ");//text file check
            str = str.Replace("data:;base64,", " ");//zip file check
            str = str.Replace("data:application/zip;base64,", " ");//zip file check

            byte[] data = Convert.FromBase64String(str);

            MemoryStream ms = new MemoryStream(data, 0, data.Length);
            ms.Write(data, 0, data.Length);

            System.Drawing.Image image = System.Drawing.Image.FromStream(ms, true);

            //  fileName = Guid.NewGuid().ToString() + ".png";
            // fullPath = Server.MapPath("~/files/MISC/" + fileName);

            image.Save(fullPath);
            // result = "image uploaded successfully";
            string a = "hello";
            // return ms;
            return a;
        }
        //public JsonResult getSumInsured_byDescofProp(int PK_PSI_ID, int claimId)
        //{
        //    string msg = null;
        //    decimal sumInsured = 0;
        //    List<CSMISCPolicySumInsured> listPS = new List<CSMISCPolicySumInsured>();
        //    try
        //    {
        //        int userId = Convert.ToInt32(Session["InsuranceUserId"]);
        //        listPS = _fsr.getPolicySumInsured(claimId, userId);
        //        sumInsured = listPS.Where(x => x.PK_PSI_ID == PK_PSI_ID).Select(x => x.SumInsured).FirstOrDefault();
        //    }
        //    catch (Exception ex)
        //    {
        //        msg = "Error:" + ex.Message;
        //    }
        //    return Json(new { sumInsured = sumInsured, msg = msg }, JsonRequestBehavior.AllowGet);
        //}
        #endregion
    }   
}
