﻿using BusinessLayer.BL;
using BusinessLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class MarineNoClaimController : Controller
    {
        BLMarineNoClaim _noClm = new BLMarineNoClaim();
        //
        // GET: /MarineNoClaim/

        public ActionResult NoClaim(string cid)
        {
            if (string.IsNullOrEmpty(cid))
            {
                return RedirectToAction("ManageClaims", "Survery");
            }
            else
            {
                return View();
            }
        }
        public JsonResult getReasonForNoClaim()
        {
            string msg = null;
            List<CSMarineReasonForNoClaim> listRC = new List<CSMarineReasonForNoClaim>();
            listRC.Insert(0,new CSMarineReasonForNoClaim {PK_RC_ID=0,ReasonForNoClaim="Select" });
            try
            {
                listRC = _noClm.getReasonForNoClaim();
            }
            catch(Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new {listRC=listRC,msg=msg },JsonRequestBehavior.AllowGet);

        }
        public JsonResult getNoClaim(int claimId)
        {
            string msg = null;
            CSMarineNoClaim nc = new CSMarineNoClaim();
            try
            {
                int userId= Convert.ToInt32(Session["InsuranceUserId"]);
                nc = _noClm.getNoClaim(claimId,userId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { nc=nc, msg = msg }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult saveNoClaim(CSMarineNoClaim nc)
        {
            string msg = null;
            int status = 0;
            try
            {
            nc.UserID = Convert.ToInt32(Session["InsuranceUserId"]);
             status = _noClm.saveNoClaim(nc);
                msg = status > 0 ? "Saved Successfully!" : "Failed! Try Again";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(msg , JsonRequestBehavior.AllowGet);

        }

    }
}
