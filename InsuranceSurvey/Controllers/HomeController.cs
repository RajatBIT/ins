﻿using BusinessLayer;
using BusinessLayer.BL;
using BusinessLayer.Models;
using InsuranceSurvey.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace InsuranceSurvey.Controllers
{
    public class HomeController : Controller
    {
        BLBroker _b = new BLBroker();

        //
        // GET: /Home/
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult SurveyorRegistration(string command, CSSurveyorsRegistration objProperty)//, string[] chDept)
        {
            string mes = "";
            try
            {
                if (command == "Submit")
                {
                    string result = "";
                    BLSurveyorsRegistration obj = new BLSurveyorsRegistration();
                    result = obj.AddSurveyors(objProperty);

                    if (result == "I")
                        mes = "Initial Reference Already Exists";
                    else if (result == "E")
                        mes = "Email Already Exists";
                    else if (result == "")
                        mes = "SLA No Already Exists";
                    else
                        mes = "Inserted Successfully";
                    //ViewBag.Mes = mes;
                    TempData["SurvMsg"] = mes;
                    return RedirectToAction("SurveyorRegistration", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;

            }
            return View();
        }

        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult InsuredRegistration(string command, CSInsuredRegistration objProperty, HttpPostedFileBase[] PolicyPhoto_Doc)
        {
            string mes = "";
            DataTable dt = new DataTable();
            try
            {
                if (command == "Submit")
                {
                    string result = "";
                    BLInsuredRegistration obj = new BLInsuredRegistration();
                    dt = obj.AddInsured(objProperty);
                    if (dt.Rows.Count > 0)
                    {
                        int PK_InsdID = Convert.ToInt32(dt.Rows[0]["PK_InsdID"]);
                        if (PK_InsdID > 0)
                        {
                            string fileName = "";
                            if (PolicyPhoto_Doc != null)
                            {
                                foreach (HttpPostedFileBase file in PolicyPhoto_Doc)
                                {
                                    if (file != null)
                                    {
                                        fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(file.FileName);
                                        string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                                        file.SaveAs(Imgpath);
                                        obj.saveInsuredRegistrationPhoto_Doc(PK_InsdID, Imgpath);
                                    }
                                }
                            }
                        }


                        result = Convert.ToString(dt.Rows[0]["Flag"]);
                        if (result == "Y")
                            result = "Your request for Insured Subscription has been successfully received. We will contact you soon.";
                        else if (result == "E")
                            result = "Email Already Exists.";
                        else
                            result = "User name Already Exists.";
                    }
                    ViewBag.Mes = mes;
                    return RedirectToAction("InsuredRegistration", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {

                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View();
        }


        [AcceptVerbs(HttpVerbs.Get | HttpVerbs.Post)]
        public ActionResult TraineeSurveyorsRegistration(string command, CSTraineeSurveyorRegistration objProperty)
        {
            string mes = "";
            try
            {
                if (command == "Submit")
                {
                    string result = "";
                    BLTraineeSurveyorRegistration obj = new BLTraineeSurveyorRegistration();
                    objProperty.IPAddress = BusinessHelper.GetIpAdress();
                    result = obj.AddTraineeSurveyorRegistration(objProperty);
                    ViewBag.Mes = result;
                    TempData["TraineeMsg"] = result;
                    if (result != "Your request for registering as a Trainee Surveyor received successfully. We will contact you shortly, Thank you.")
                        return View(objProperty);
                    else
                        return RedirectToAction("TraineeSurveyorsRegistration", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View();
        }

        public ActionResult CorporateInsuredRegistration(string command, CSCorporateInsuredRegistration objProperty)
        {
            string mes = "";
            try
            {
                if (command == "Submit")
                {
                    string result = "";
                    BLCorporateInsuredRegistration obj = new BLCorporateInsuredRegistration();
                    objProperty.IPAddress = BusinessHelper.GetIpAdress();
                    result = obj.AddCorporateInsuredRegistration(objProperty);
                    ViewBag.Mes = mes;
                    TempData["Co-InsMsg"] = result;
                    if (result != "Your request for Insured Subscription has been successfully received. We will contact you soon.")
                        return View(objProperty);
                    else
                        return RedirectToAction("CorporateInsuredRegistration", "Home");
                }
                else
                {
                    return View();
                }
            }
            catch (Exception ex)
            {
                TempData["errMsg"] = "Error : " + ex.Message;
            }
            return View();
        }

        #region check surveyor validation
        public JsonResult SurveyorRegisterValidation(CSSurveyorsRegistration sr)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailStatus = 0, MobileStatus = 0, UserNameStatus = 0, PANNOStatus = 0, StaxNoStatus = 0, SLANoStatus = 0,
ISLANOStatus = 0, IniRefStatus = 0;
            try
            {
                BLSurveyorsRegistration bs = new BLSurveyorsRegistration();
                dt = bs.SurveyorRegisterValidation(sr);
                if (dt.Rows.Count > 0)
                {
                    EmailStatus = Convert.ToInt32(dt.Rows[0]["EmailStatus"]);
                    MobileStatus = Convert.ToInt32(dt.Rows[0]["MobileStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                    PANNOStatus = Convert.ToInt32(dt.Rows[0]["PANNOStatus"]);
                    StaxNoStatus = Convert.ToInt32(dt.Rows[0]["StaxNoStatus"]);
                    SLANoStatus = Convert.ToInt32(dt.Rows[0]["SLANoStatus"]);
                    ISLANOStatus = Convert.ToInt32(dt.Rows[0]["ISLANOStatus"]);
                    IniRefStatus = Convert.ToInt32(dt.Rows[0]["IniRefStatus"]);

                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailStatus = EmailStatus, MobileStatus = MobileStatus, UserNameStatus = UserNameStatus, PANNOStatus = PANNOStatus, StaxNoStatus = StaxNoStatus, SLANoStatus = SLANoStatus, ISLANOStatus = ISLANOStatus, IniRefStatus = IniRefStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion
        #region check Insured validation
        public JsonResult InsuredRegisterValidation(CSInsuredRegistration i)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailStatus = 0, MobileStatus = 0, UserNameStatus = 0, PANNOStatus = 0, GSTINStatus = 0;
            try
            {
                BLInsuredRegistration bi = new BLInsuredRegistration();
                dt = bi.InsuredRegisterValidation(i);
                if (dt.Rows.Count > 0)
                {
                    EmailStatus = Convert.ToInt32(dt.Rows[0]["EmailStatus"]);
                    MobileStatus = Convert.ToInt32(dt.Rows[0]["MobileStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                    PANNOStatus = Convert.ToInt32(dt.Rows[0]["PANNOStatus"]);
                    GSTINStatus = Convert.ToInt32(dt.Rows[0]["GSTINStatus"]);

                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailStatus = EmailStatus, MobileStatus = MobileStatus, UserNameStatus = UserNameStatus, PANNOStatus = PANNOStatus, GSTINStatus = GSTINStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion


        #region check Broker validation
        public JsonResult BrokerRegisterValidation(CSBrokerRegisteration br)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailIdStatus = 0, MobileNoStatus = 0, UserNameStatus = 0;
            try
            {
                BLInsuredRegistration bi = new BLInsuredRegistration();
                dt = _b.BrokerRegisterValidation(br);
                if (dt.Rows.Count > 0)
                {
                    EmailIdStatus = Convert.ToInt32(dt.Rows[0]["EmailIdStatus"]);
                    MobileNoStatus = Convert.ToInt32(dt.Rows[0]["MobileNoStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailIdStatus = EmailIdStatus, MobileStatus = MobileNoStatus, UserNameStatus = UserNameStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        public ActionResult BrokerRegisteration()
        {
            return View();
        }
        public JsonResult saveBrokerRegisteration(CSBrokerRegisteration br, List<CSBrokerRegisteration> listBrokerBranch_EmailId)
        {
            string msg = null;
            int status = 0;
            int pk_BR_ID = 0;
            try
            {
                pk_BR_ID = _b.saveBrokerRegisteration(br);
                if (pk_BR_ID > 0)
                {
                    if (listBrokerBranch_EmailId != null)
                    {
                        foreach (CSBrokerRegisteration bb in listBrokerBranch_EmailId)
                        {
                            bb.BR_ID = pk_BR_ID;
                            _b.saveBrokerBranch_And_EmailId(bb);
                        }
                    }
                }
                msg = pk_BR_ID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }



        public JsonResult deleteBrokerBranch_And_EmailId(int PK_BB_ID)
        {
            string msg = null;
            int status = 0;
            try
            {
                status = _b.deleteBrokerBranch_And_EmailId(PK_BB_ID);
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }



        #region check Trainee validation
        public JsonResult TraineeSurveyorsRegisterValidation(CSTraineeSurveyorRegistration t)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailStatus = 0, MobileStatus = 0, UserNameStatus = 0;
            try
            {
                BLTraineeSurveyorRegistration bt = new BLTraineeSurveyorRegistration();
                dt = bt.TraineeSurveyorsRegisterValidation(t);
                if (dt.Rows.Count > 0)
                {
                    EmailStatus = Convert.ToInt32(dt.Rows[0]["EmailStatus"]);
                    MobileStatus = Convert.ToInt32(dt.Rows[0]["MobileStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailStatus = EmailStatus, MobileStatus = MobileStatus, UserNameStatus = UserNameStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion


        #region check Corporate Insured validation
        public JsonResult CorporateInsuredRegisterValidation(CSCorporateInsuredRegistration c)
        {
            string msg = null;
            DataTable dt = new DataTable();

            int EmailStatus = 0, MobileStatus = 0, UserNameStatus = 0;
            try
            {
                BLCorporateInsuredRegistration ci = new BLCorporateInsuredRegistration();
                dt = ci.CorporateInsuredRegisterValidation(c);
                if (dt.Rows.Count > 0)
                {
                    EmailStatus = Convert.ToInt32(dt.Rows[0]["EmailStatus"]);
                    MobileStatus = Convert.ToInt32(dt.Rows[0]["MobileStatus"]);
                    UserNameStatus = Convert.ToInt32(dt.Rows[0]["UserNameStatus"]);
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { EmailStatus = EmailStatus, MobileStatus = MobileStatus, UserNameStatus = UserNameStatus, msg = msg }, JsonRequestBehavior.AllowGet);
        }

        #endregion

        public ActionResult InsuredRegistrationNew()
        {
            return View();
        }


        public JsonResult saveInsuredRegistration(CSInsuredRegistration i, List<CSInsuredRegPolicyNo_Insurers> listPolicy_Insurers /*, HttpPostedFileBase[] PolicyPhoto_Doc*/)
        {
            string mes = "";
            DataTable dt = new DataTable();
            string result = "";
            int PK_InsdID = 0;
            BLInsuredRegistration obj = new BLInsuredRegistration();
            string msg = null;
            try
            {

                dt = obj.SaveInsuredRegistration(i);
                if (dt.Rows.Count > 0)
                {
                    PK_InsdID = Convert.ToInt32(dt.Rows[0]["PK_InsdID"]);
                    result = Convert.ToString(dt.Rows[0]["Flag"]);
                    if (PK_InsdID > 0)
                    {

                        //string fileName = "";
                        //if (PolicyPhoto_Doc != null)
                        //{
                        //    foreach (HttpPostedFileBase file in PolicyPhoto_Doc)
                        //    {
                        //        if (file != null)
                        //        {
                        //            fileName = Guid.NewGuid().ToString() + DateTime.Now.ToString("ddMMyyyyhhmmssfff") + Path.GetExtension(file.FileName);
                        //            string Imgpath = Path.Combine(Server.MapPath("~/MarineISVRPhotos/"), fileName);
                        //            file.SaveAs(Imgpath);
                        //            obj.saveInsuredRegistrationPhoto_Doc(PK_InsdID, Imgpath);
                        //        }
                        //    }
                        //}
                        if (listPolicy_Insurers != null)
                        {
                            foreach (CSInsuredRegPolicyNo_Insurers pi in listPolicy_Insurers)
                            {
                                pi.InsdID = PK_InsdID;
                                obj.saveInsuredRegPolicyNo_Insurers(pi);
                            }
                        }

                        if (result == "Y")
                        {
                            msg = "s";
                            result = "Your request for Insured Subscription has been successfully received. We will contact you soon.";
                        }
                        else if (result == "E")
                        {
                            result = "Email Already Exists.";
                        }
                        else
                        {
                            result = "User name Already Exists.";
                        }
                    }
                    ViewBag.Mes = result;
                    TempData["tt"] = result;
                }
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;

            }

            return Json(new { msg = msg, PK_InsdID = PK_InsdID, result = result }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult saveInsuredRegistrationPhoto_Doc(int PK_InsdID)
        {
            string msg = null;
            int status = 0;
            int userId = Convert.ToInt32(Session["InsuranceUserId"]);
            //CSMISCFSRObservationsCircumstances oc = new CSMISCFSRObservationsCircumstances();
            //oc = _fsr.getObservationsCircumstances(userId, claimId);
            BLInsuredRegistration obj = new BLInsuredRegistration();
            if (Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    for (int i = 0; i < files.Count; i++)
                    {


                        HttpPostedFileBase file = files[i];
                        string fname;
                        string tempFileName = file.FileName;
                        string tempFileEx = Path.GetExtension(tempFileName);
                        fname = Guid.NewGuid().ToString() + tempFileEx;
                        fname = Path.Combine(Server.MapPath("~/files/MISC/"), fname);
                        file.SaveAs(fname);
                        status += obj.saveInsuredRegistrationPhoto_Doc(PK_InsdID, fname);
                    }


                }
                catch (Exception ex)
                {
                    //return Json("Error occurred. Error details: " + ex.Message);
                }
            }
            msg = status > 0 ? "s" : "f";
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }
        #region SurveyReg New
        public JsonResult saveSurveyorsRegistrationNew(CSSurveyorsRegistration s, List<CSSurveyorDepartment> listSurveyorDept)
        {
            string msg = null;
            int pk_SURID = 0;
            int count = 0;
            try
            {
                pk_SURID = new BLSurveyorsRegistration().saveSurveyorsRegistrationNew(s);
                foreach (CSSurveyorDepartment sd in listSurveyorDept)
                {
                    //if (sd.IsActive == true && sd.FK_SurId>0)
                    //{
                        sd.FK_SurId = pk_SURID;
                        count += new BLSurveyorsRegistration().saveSurveyorDepartmentNew(sd);
                    //}
                }
                msg = pk_SURID > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { msg = msg }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult getMembershipType()
        {
            string msg = null;
            int status = 0;
            //var listMembershipType
            DataTable dt = new DataTable();
            dt = BLCommon.getMembershipType();
            var listMembershipType = (from row in dt.AsEnumerable()
                                      select new { TypeID = row.Field<int>("TypeID"), TypeName = row.Field<string>("TypeName") }).ToList();
            return Json(new { listMembershipType = listMembershipType, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSurveyorRegistration_byPK_SURID()
        {
            string msg = null;
            CSSurveyorsRegistration s = new CSSurveyorsRegistration();
            try
            {
                s = new BLSurveyorsRegistration().getSurveyorRegistration_byPK_SURID(MySession.insuranceUserId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { s = s, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult updateSurveyorsPassword(string oldPassword, string newPassword)
        {
            string msg = null;
            int passwordCorrect = 0;
            int status = 0;
            try
            {
                passwordCorrect = Convert.ToInt32(new CommonService().executeScalarWithQuery("if exists(select PK_SURID from tblSurveyorRegistration where PK_SURID=" + MySession.insuranceUserId + " and Password='" + oldPassword + "') select 1 else select 0"));
                if (passwordCorrect == 1)
                {
                    status = new BLSurveyorsRegistration().updateSurveyorsPassword(MySession.insuranceUserId, newPassword);
                }
                msg = status > 0 ? "s" : "f";
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { passwordCorrect = passwordCorrect, msg = msg }, JsonRequestBehavior.AllowGet);
        }
        public JsonResult getSurveyorDepartmentNew()
        {
            string msg = null;
            List<CSSurveyorDepartment> listSurveyorDept = new List<CSSurveyorDepartment>();
            try
            {
                listSurveyorDept = new BLSurveyorsRegistration().getSurveyorDepartmentNew(MySession.insuranceUserId);
            }
            catch (Exception ex)
            {
                msg = "Error:" + ex.Message;
            }
            return Json(new { listSurveyorDept=listSurveyorDept,msg=msg},JsonRequestBehavior.AllowGet);
        }
        #endregion
        /*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*=~*/
    }
}
